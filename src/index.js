/**
 * Create React App entry point. This and `public/index.html` files can not be
 * changed or moved.
 */
import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import React from "react";
import ReactDOM from "react-dom";
import store, { persistor } from "./app/store/store";
import App from "./App";
import "./index.scss"; // Standard version
import "socicon/css/socicon.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "select2/dist/css/select2.css"
import { I18nextProvider } from "react-i18next";
import i18next from "i18next";

import common_es from "./translations/es/common.json";
import common_en from "./translations/en/common.json";

i18next.init({
  interpolation: { escapeValue: false },
  lng: 'es',
  resources: {
    es: {
      common: common_es
    },
    en: {
      common: common_en
    },
  },
});

window.process = {
  env: {
      NODE_ENV: 'development'
  }
}    



/**
 * Base URL of the website.
 *
 * @see https://facebook.github.io/create-react-app/docs/using-the-public-folder
 */
const { PUBLIC_URL } = process.env;

/**
 * Inject metronic interceptors for axios.
 *
 * @see https://github.com/axios/axios#interceptors
 */

ReactDOM.render(
  <I18nextProvider i18n={i18next}>
    <App
      store={store}
      persistor={persistor}
      basename={PUBLIC_URL}
    />
  </I18nextProvider>,
  document.getElementById("root")
);
