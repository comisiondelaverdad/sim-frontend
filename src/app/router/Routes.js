import React from "react";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import { useLastLocation } from "react-router-last-location";
import HomePage from "../pages/home/HomePage";
import MuseoHome from "../pages/home/MuseoHome";
import ErrorsPage1 from "../pages/errors/ErrorPage1";
import LogoutPage from "../pages/auth/Logout";
import { LayoutContextProvider } from "../../theme/LayoutContext";
import * as routerHelpers from "../router/RouterHelpers";
import AuthPage from "../pages/auth/AuthPage";
import UserCompromise from "../components/organisms/UserCompromise";
import { me } from "../crud/auth.crud";

export const Routes = withRouter(({ history }) => {
  const lastLocation = useLastLocation();
  const compromise = false;

  const validateCompromise = ()  => {   
   return <UserCompromise />;
  };
  routerHelpers.saveLastLocation(lastLocation);
  const { isAuthorized, userLastLocation } = useSelector(
    ({ auth, urls}) => ({
      isAuthorized: auth.user != null,
      userLastLocation: routerHelpers.getLastLocation(),
    }),
    shallowEqual
  );

  return (
    /* Create `LayoutContext` from current `history` and `menuConfig`. */

    <LayoutContextProvider history={history} >
      <Switch>
        {!isAuthorized ? (
          /* Render auth page when user at `/auth` and not authorized. */
          <AuthPage />
        ) : (
          /* Otherwise redirect to root page (`/`) */
          <Redirect from="/auth" to={userLastLocation} />
        )}

        <Route path="/error" component={ErrorsPage1} />
        <Route path="/logout" component={LogoutPage} />

        {!isAuthorized ? (
          /* Redirect to `/auth` when user is not authorized */
          <Redirect to="/auth/login" />
        ) : (
          <>
            {validateCompromise()}
            {history.location.pathname.indexOf("/museo") === 0 ? (
              <MuseoHome />
            ) : (
                <HomePage userLastLocation={userLastLocation} />
            )}
          </>
        )}
      </Switch>
    </LayoutContextProvider>
  );
});
