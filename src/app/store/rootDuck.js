import {
  all
} from "redux-saga/effects";
import {
  combineReducers
} from "redux";

import * as auth from "./ducks/auth.duck";
import * as app from "./ducks/app.duck";
import * as museo from "./ducks/museo.duck";

export const rootReducer = combineReducers({
  app: app.reducer,
  auth: auth.reducer,
  museo: museo.reducer
});

export function* rootSaga() {
  yield all([auth.saga()]);
}