import { URL_API } from "../config/const"
import { getToken } from "../crud/auth.crud";


export function serviceImage(filename) {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  const path = "/api/records/image/" + filename;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.blob();
    }
  });
}

export function serviceImageResize(id, size = 'large') {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  let path = "/api/records/imageservice/" + id

  size !== 'large' ? path += '/' + size : path += '/large'

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.blob();
    }
  });
}

export function serviceStream(id, format, support) {
  let path = URL_API + "/api/records/streamservice/" + Date.now() + "/" + id + "/" + format + "/" + support

  // let loadedBytes = 0
  // let isFileLoaded = false
  // let chunkSize = 5000
  // let sourceBuffer = null

  // var miInit = {
  //   method: "GET",
  //   mode: "cors",
  //   cache: "default"
  // };

  // let mediaSource = new MediaSource();
  // mediaSource.addEventListener('sourceopen', sourceOpen);

  // const removeEventListeners = () => {
  //   mediaSource && mediaSource.removeEventListener('sourceopen', sourceOpen)
  //   sourceBuffer && sourceBuffer.removeEventListener('updateend', appendBuffer)
  // };

  // const sourceOpen = () => {
  //   sourceBuffer = mediaSource.addSourceBuffer('video/ogg');
  //   appendBuffer();
  //   sourceBuffer.addEventListener('updateend', appendBuffer);

  //   console.log('open stream')
  // };

  // const appendBuffer = () => {
  //   const start = loadedBytes ? loadedBytes + 1 : loadedBytes;
  //   const end = loadedBytes ? loadedBytes + chunkSize : chunkSize;

  //   if (!isFileLoaded) {
  //     console.log('hola')
  //     rangeContent(start, end)
  //       .then(([data, contentRange]) => {
  //         sourceBuffer.appendBuffer(data);
  //         loadedBytes = (end);
  //         isFileLoaded = (data.byteLength < chunkSize);

  //         if (chunkSize === 5000) {
  //           chunkSize = (Math.ceil(contentRange.substring(contentRange.indexOf('/') + 1) / 5));
  //         }
  //       })
  //       .catch((err) => console.log(err));
  //   } else {
  //     removeEventListeners();
  //     mediaSource.endOfStream();
  //   }
  // }

  // const rangeContent = (start = 0, end = null) => {
  //   const fetchSettings = {
  //     headers: new Headers({
  //       Authorization: getToken(),
  //       Range: `bytes=${start}-${end ? end : ''}`
  //     })
  //   };

  //   const fetchMethod = fetch(path, fetchSettings);
  //   const data = fetchMethod.then((res) => res.arrayBuffer());
  //   const header = fetchMethod.then((res) => res.headers.get('Content-Range'));

  //   return Promise.all([data, header]).then(([data, contentRange]) => ({
  //     data,
  //     contentRange
  //   }));
  // }


  return path

}


export function serviceContentDocumentByIdent(ident) {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  const path = "/api/search-museo/record/content/document/" + ident;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function serviceFile(id) {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  const path = "/api/records/file/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.blob();
    }
  });
}

export function serviceZipFile(data) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/records/zip-file";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    }
    else {
      return response.blob();
    }
  });
}

export function servicePreProcess(ident) {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  const path = "/api/records/pre_process/" + ident;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function getInfoRecordById(id) {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  const path = "/api/records/infoRecord/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}