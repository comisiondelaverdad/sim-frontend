import { BehaviorSubject } from 'rxjs';
import $, { type } from 'jquery';
import { dateToAAAAMMDD, getMetadata } from './utils';

const subject$ = new BehaviorSubject({});

let DcSchema = {
    title: "DC.title",
    ident: "DC.identifier",
    ResourceGroupId: "DC.source",
    description: "DC.description",
    type: "DC.type",
    topics: "DC.subject",
    relation: "DC.relation",
    ident: "DC.coverage",
    creator: "DC:creator",
    authors: "DC:creator",
    rights: 'DC.rights',
    language: "DC.language",
    date: "DC.date",
    temporalCoverage: "DC.date",
    creationDate: "DC.date",
    collaborators: "DC.contributor"
}



export const clarMetadata = () => {
    const metaItems = document.getElementsByTagName("meta");
    for (let i = metaItems.length - 1; i > 0; i--) {
        (metaItems[i]).remove();
    }
}

subject$.subscribe((metadata) => {
    clarMetadata();
    var typeZotero = false;  

    for (let meta in metadata) {
        if (DcSchema.hasOwnProperty(`${meta}`)) {
            if (metadata[meta] && typeof metadata[meta] !== 'undefined') {
                if(meta === 'type') {
                    typeZotero = true;
                    let typeZotero = ''
                    if((metadata[meta].toLowerCase()).includes('entrevista')) {
                        typeZotero = 'interview'
                    }else if ((metadata[meta].toLowerCase()).includes('informe') || (metadata[meta].toLowerCase()).includes('caso')){
                        typeZotero = 'report'
                    }else {
                        typeZotero = 'document'
                    }
                    $("title").before(`<meta property="${DcSchema[meta]}" content="${typeZotero}">`)
                }else if (Object.prototype.toString.call(metadata[meta]) === "[object Array]") {
                    metadata[meta].map((element) => {                    
                        if(Array.isArray(element))
                            
                        element = element.join()
                        if (element != null){
                            element = element.replace('[', '')
                            element = element.replace(']','')
                            element = element.replace('"','')
                        }
                            
                        $("title").before(`<meta property="${DcSchema[meta]}" content="${element}">`)})
                } else if (Object.prototype.toString.call(metadata[meta]) === "[object String]") {
                    if(meta === 'creator'){
                        let cre = [];
                        let creator = metadata[meta];
                        creator = creator.replace('[', '');
                        creator = creator.replace(']', '');
                        creator = creator.replace('"', '');
                        creator = creator + ", ";
                        $("title").before(`<meta property="${DcSchema[meta]}" content="${creator}">`)
                    }else{
                        $("title").before(`<meta property="${DcSchema[meta]}" content="${metadata[meta]}">`)
                    }
                } else if (Object.prototype.toString.call(metadata[meta]) === "[object Object]") {
                    if (metadata[meta].start) {
                        $("title").before(`<meta property="${DcSchema[meta]}" content="${dateToAAAAMMDD(metadata[meta].start)}">`);
                    } else if (metadata[meta].end) {
                        $("title").before(`<meta property="${DcSchema[meta]}" content="${dateToAAAAMMDD(metadata[meta].end)}">`);
                    }
                }
            }
        }
    }
    if(!typeZotero){
        $("title").before(`<meta property="DC:type" content="document">`);
    }
    document.dispatchEvent(new Event('ZoteroItemUpdated', {
        bubbles: true,
        cancelable: true
    }))
});

export const updateMetadata = (resource) => {
    if (resource === null) {
        clarMetadata()
    } else {
        let finalMetadata = getMetadata(resource);
        subject$.next(finalMetadata);
    }
}

