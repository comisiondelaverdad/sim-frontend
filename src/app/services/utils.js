import _, { forEach } from "lodash";
import Swal from "sweetalert2";
import moment from "moment";

export const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    let r = (Math.random() * 16 | 0), v = c === 'x' ? r : ((r & 0x3) | 0x8);
    return v.toString(16);
  });
}

export const is_array = (value) => {
  return Object.prototype.toString.call(value) === '[object Array]';
}

export const is_string = (value) => {
  return Object.prototype.toString.call(value) === '[object String]';
}

export const findField = (fields, id) => {
  return _.find(fields, (obj) => {
    return obj.id === id;
  });
}

const addPropertyToObject = (object, propertyObject) => {
  for (let property in propertyObject) {
    if (object.hasOwnProperty(property) && propertyObject.hasOwnProperty(property)) {
      const mergeChild = addPropertyToObject(object[property], propertyObject[property])
      return { ...object, ...{ [property]: mergeChild } };
    } else {
      return { ...object, ...propertyObject }
    }
  }
}

export const getMetadata = (object) => {
  const metadata = {
    ...object.ident ? { ident: object.ident } : {},
    ...object.ResourceGroupId ? { ResourceGroupId: object.ResourceGroupId } : {},
    ...object.type ? { type: object.type } : {},
    ...object.origin ? { origin: object.origin } : {},
    ...object.metadata.firstLevel ? object.metadata.firstLevel : {},
    ...object.metadata.missionLevel ? object.metadata.missionLevel.humanRights ? object.metadata.missionLevel.humanRights : {} : {},
    ...object.metadata.missionLevel ? object.metadata.missionLevel.target ? object.metadata.missionLevel.target : {} : {},
    ...object.metadata.missionLevel ? object.metadata.missionLevel.source ? object.metadata.missionLevel.source : {} : {},
  }
  return metadata;
}

export const objectTransformation = (object, statment) => {
  let newObject = null;
  for (let i = 0; i < statment.length; i++) {
    let { destiny, origin, defaultValue } = statment[i];
    destiny = `"${(destiny.split(".")).join('"."')}"`;
    const obj = JSON.parse(`{${(destiny.split('.')).join(':{')}:${object.hasOwnProperty(origin) ? JSON.stringify(object[origin]) === undefined ? JSON.stringify(defaultValue) : JSON.stringify(object[origin]) : JSON.stringify(defaultValue)}${'}'.repeat((destiny.split('.')).length)}`);
    if (newObject === null) {
      newObject = obj
    } else {
      newObject = addPropertyToObject(newObject, obj);
    }
  }
  return newObject;
}

export const getValueFromPath = (path, obj) => {
  for (var i = 0, path = path.split('.'), len = path.length; i < len; i++) {
    // console.log(path, obj)
    if (obj === null || typeof obj[path[i]] === 'undefined') {
      i = path.length;
      obj = null
    } else {
      obj = obj[path[i]];
    }
  };
  return obj;
}

export const objectTransformationInverse = (object, statment) => {
  let newObject = null;
  for (let i = 0; i < statment.length; i++) {
    const { destiny, origin, defaultValue, type } = statment[i];
    let objectTrasform = defaultValue;
    if (JSON.stringify(object) === "{}") {
      console.log(JSON.stringify(object));
    } else {
      let dataContent = getValueFromPath(destiny, object);
      switch (type) {
        case 'author':
          objectTrasform = dataContent ?
            is_array(dataContent) ? (dataContent).sort() : [dataContent] : defaultValue;
          break;
        default:
          objectTrasform = dataContent ? dataContent : defaultValue;
          break;
      }
    }
    newObject = { ...newObject, ...{ [origin]: objectTrasform } };
  }
  return newObject;
}

export const resourceGroupsValidation = (metadata) => {
  const error = {
    error: false,
    message: `<div class="meta-container">__content__</div>`,
  }
  let listErrors = '';
  if (metadata.firstLevel.urlRecords) {
    const urlRegex = new RegExp(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/, 'i')
    if (is_array(metadata.firstLevel.urlRecords)) {
      const errorsUrl = metadata.firstLevel.urlRecords.filter((url) => !(urlRegex.test(url)));
      if (errorsUrl.length > 0) {
        error.error = true;
        listErrors = `${listErrors}<div class="meta-list"> 
                          <span class="form-label">Url de recursos </span>
                          <div class="meta-value">Error de url ${errorsUrl.toString()} </div>
                        </div>`;
      }
    }
  }
  if (metadata.firstLevel.temporalCoverage) {
    if (metadata.firstLevel.temporalCoverage.start && metadata.firstLevel.temporalCoverage.end)
    if (new Date(metadata.firstLevel.temporalCoverage.start) > new Date(metadata.firstLevel.temporalCoverage.end)) {
      error.error = true;
      listErrors = `${listErrors}<div class="meta-list"> 
                  <span class="form-label">Cobertura temporal </span>
                  <div class="meta-value"> La fecha inicial es mayor que la fecha final </div>
                </div>`;
    }
  }
  if (metadata.firstLevel.creationDate) {
    if (metadata.firstLevel.creationDate.start && metadata.firstLevel.creationDate.end)
    if (new Date(metadata.firstLevel.creationDate.start) > new Date(metadata.firstLevel.creationDate.end)) {
      error.error = true;
      listErrors = `${listErrors}<div class="meta-list"> 
                  <span class="form-label">Fecha de creación </span>
                  <div class="meta-value"> La fecha inicial es mayor que la fecha final </div>
                </div>`;
    }
  }

  error.message = error.message.replace('__content__', listErrors);
  return error;
}

export const resourcesValidation = (metadata) => {
  const error = {
    error: false,
    message: `<div class="meta-container">__content__</div>`,
  }
  let listErrors = '';
  if (metadata.firstLevel.urlRecords) {
    const urlRegex = new RegExp(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/, 'i')
    if (is_array(metadata.firstLevel.urlRecords)) {
      const errorsUrl = metadata.firstLevel.urlRecords.filter((url) => !(urlRegex.test(url)));
      if (errorsUrl.length > 0) {
        error.error = true;
        listErrors = `${listErrors}<div class="meta-list"> 
                          <span class="form-label">Url de recursos </span>
                          <div class="meta-value">Error de url ${errorsUrl.toString()} </div>
                        </div>`;
      }
    }
  }
  if (metadata.firstLevel.temporalCoverage) {
    if (metadata.firstLevel.temporalCoverage.start && metadata.firstLevel.temporalCoverage.end)
    if (new Date(metadata.firstLevel.temporalCoverage.start) > new Date(metadata.firstLevel.temporalCoverage.end)) {
      error.error = true;
      listErrors = `${listErrors}<div class="meta-list"> 
                  <span class="form-label">Cobertura temporal </span>
                  <div class="meta-value"> La fecha inicial es mayor que la fecha final </div>
                </div>`;
    }
  }
  error.message = error.message.replace('__content__', listErrors);
  return error;
}

export const dateToAAAAMMDD = (d) => {
  if (d !== null) {
    const date = new Date(d);
    const dd = (date.getUTCDate() < 10) ? `0${date.getUTCDate()}` : date.getUTCDate();
    const mm = (date.getUTCMonth() + 1) < 10 ? `0${date.getUTCMonth() + 1}` : date.getUTCMonth() + 1;
    const aaaa = date.getUTCFullYear();
    return `${aaaa}-${mm}-${dd}`
  } else {
    return d;
  }
};

export const sleep = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

export const errorAlert = ({ error, text, confirmButtonText, action }) => {
  if (error == 401) {
    console.log(error)
  } else {
    Swal.fire({
      title: error,
      text: text,
      icon: 'error',
      confirmButtonText: confirmButtonText,
    })
      .then((result) => {
        action();
      })
  }
}

export const submitAlert = ({ option, type, fn, data, info, gen, fnFinally }) => {
  Swal.fire({
    title: `Se ${option === 'update' ? 'actualizará' : 'creará'} ${gen ? gen : 'el'} ${type}`,
    text: info,
    icon: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonText: `${option === 'update' ? 'Actualizar' : 'Crear'} ${type}`
  })
    .then((result) => {
      if (result.value) {
        Swal.fire({
          title: '¡Por favor espere!',
          html: `${option === 'update' ? 'Actualizando' : 'Creando'}  ${gen ? gen : 'el'} ${type}`,
          allowOutsideClick: false,
          onBeforeOpen: () => {
            Swal.showLoading()
          },
        });
        fn(data)
          .then((response) => {
            Swal.close();
            Swal.fire(
              `${option === 'update' ? 'Actualizado' : 'Creado'}`,
              `Se ha ${option === 'update' ? 'actualizado' : 'Creado'}  ${gen ? gen : 'el'} ${type} ${response.ident ? response.ident : response._id ? response._id : ''} de forma exitosa`,
              'success'
            ).then((data2) => {
              const buttonReturn = document.getElementById('return');
              if (buttonReturn) {
                buttonReturn.click();
              }
              if (fnFinally) {
                fnFinally();
              }
            })
          })
          .catch(err => {
            Swal.close();
            Swal.fire(
              `No se ha podido ${option === 'update' ? 'Actualizar' : 'Crear'}  ${gen ? gen : 'el'} ${type}`,
              `error: ${err}`,
              'error'
            )
          })
      }
    })
}