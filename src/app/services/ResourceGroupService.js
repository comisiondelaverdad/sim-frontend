import { URL_API } from "../config/const"
import { getToken } from "../crud/auth.crud";
import $ from 'jquery';

export function serviceListFirstLevel(initial = null) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/resource-groups/first-level-cache";

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function serviceListFirstLevelLazy(initial = null) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  let path = "/api/resource-groups/lazy?head=0&path=all";

  if(initial !== "")
    path = "/api/resource-groups/lazy?head=1&path=" + initial

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function getAllChildren(ident) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  let path = "/api/resource-groups/get-all-children?ident=" + ident

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function getRecursiveMetadataRG(id) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/resource-groups/recursive-metadata/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}


export function getMetadataRG(id) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/resource-groups/metadata/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function updateResourceGroupMetadata(body) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/resource-groups/update-metadata";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(body)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function toMoveResourceGroup(body) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/resource-groups/logic-move";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(body)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function updateResources(body) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/resource-groups/update-resources";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(body)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function deleteResourceGroup(id) {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  const path = "/api/resource-groups/"+id;

  var miInit = {
    method: "DELETE",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path , miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function getMetadata(body) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/resource-groups/update-metadata";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(body)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}


export function newResourceGroup(body) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/resource-groups";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(body)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

// add headers into interceptor ajaxRequest
$(document).ajaxSend(function (event, request, settings) {
  request.setRequestHeader("Content-Type", "application/json");
  request.setRequestHeader("Authorization", getToken());
});

export const url = (node) => {
  if (node.id === '#') {
    const id = window.location.pathname.split("/")[(window.location.pathname.split("/").length) - 1]
    return URL_API + "/api/resource-groups/lazy?head=0&path=" + id
  } else {
    return URL_API + "/api/resource-groups/lazy?head=1&path=" + node.id;
  }
}

export function serviceListInternal(initial = null) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/resource-groups/internal";

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function serviceDetail(id) {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  const path = "/api/resource-groups/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (!([200, 304].includes(response.status))) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}


