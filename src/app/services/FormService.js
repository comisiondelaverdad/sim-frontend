import {URL_API} from "../config/const"
import { getToken } from "../crud/auth.crud";

export function getAdvancedSearchConfig() {
  var myHeaders = new Headers({'Authorization': getToken()});

  const path = "/api/forms/advanced-search";

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function getOptions(field){
  var myHeaders = new Headers({'Authorization': getToken()});

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };
  let url = URL_API + "/api/forms/options/" + field
  return fetch(url, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function getAsyncOptions(form, query){
  var myHeaders = new Headers({'Authorization': getToken()});

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };
  let url = URL_API + form.schema.url+query
  //let url = "https://api.github.com/search/users?q="+query
  return fetch(url, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}




