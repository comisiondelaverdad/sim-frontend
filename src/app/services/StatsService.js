import axios from 'axios';

import {TIMEOUT, URL_API} from '../config/const';
import {getToken} from '../crud/auth.crud';


export function resourceViews(ids) {
    const headers = {'Authorization': getToken()};

    return axios.post(`${URL_API}/api/stats/resource/views`, {
        ids: ids
    }, {
        headers: headers,
        timeout: TIMEOUT
    }).then((response) => {
        return response.data
    }).catch((error) => {
        return Promise.reject(error);
    });
}
