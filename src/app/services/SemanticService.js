import axios from "axios";

import { URL_API, /*PAGE_SIZE,*/ TIMEOUT } from "../config/const";
import { getToken } from "../crud/auth.crud";

export function list(search, from) {
  let myHeaders = new Headers({ Authorization: getToken() });
  const page_size = 1000; //PAGE_SIZE

  search = search ? "q=" + search : "";
  const path =
    "/api/semantic/findTable?" +
    search +
    "&skip=" +
    (from - 1) * page_size +
    "&limit=" +
    page_size;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function terms() {
  let myHeaders = new Headers({ Authorization: getToken() });
  
  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(`${URL_API}/api/term/relations`, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function semantics(inputValue) {
  let myHeaders = new Headers({ Authorization: getToken() });
  
  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(`${URL_API}/api/semantic/relations/${inputValue}`, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function selectsemantic() {
  let myHeaders = new Headers({ Authorization: getToken() });
  
  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(`${URL_API}/api/semantic/selectsemantic`, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}


export function records(id) {
  let myHeaders = new Headers({ Authorization: getToken() });
  
  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  const path = "/api/semantic/records/" + id;  

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}


export function get(id) {  
  var myHeaders = new Headers({ Authorization: getToken() });

  const path = "/api/semantic/findOne/" + id;  
  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function diccionarioAbcFind(like) {
  var myHeaders = new Headers({ Authorization: getToken() });

  const path = "/api/semantic/diccionarioAbcFind/" + like;  
  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function diccionarioAbc(like) {
  var myHeaders = new Headers({ Authorization: getToken() });

  const path = "/api/semantic/diccionarioAbc/" + like;  
  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function diccionarioLike(like) {
  var myHeaders = new Headers({ Authorization: getToken() });

  const path = "/api/semantic/diccionarioLike/" + like;  
  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function getCatalogadores() {
  var myHeaders = new Headers({ Authorization: getToken() });

  const path = "/api/users/catalogador";

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function create(semantic) {
  const headers = { Authorization: getToken() };

  return axios
    .post(`${URL_API}/api/semantic/createbuscador/`, semantic, {
      headers: headers,
      timeout: TIMEOUT,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function update(id, semantic) {
  const path = "/api/semantic/updatebuscador/" + id;
  const headers = { Authorization: getToken() };

  return axios
    .put(URL_API + path, semantic, {
      headers: headers,
      // timeout: TIMEOUT,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

  export function resources(inputValue) {
    let myHeaders = new Headers({ Authorization: getToken() });
    
    var miInit = {
      method: "GET",
      headers: myHeaders,
      mode: "cors",
      cache: "default",
    };
  
    return fetch(`${URL_API}/api/semantic/resources/${inputValue}`, miInit).then(function(response) {
      if (response.status !== 200) {
        return Promise.reject(response.status);
      } else {
        return response.json();
      }
    });
  }

  export function findByResorce(id) {
    var myHeaders = new Headers({ Authorization: getToken() });
  
    const path = "/api/semantic/findByResorce/" + id;  
    var miInit = {
      method: "GET",
      headers: myHeaders,
      mode: "cors",
      cache: "default",
    };
  
    return fetch(URL_API + path, miInit).then(function(response) {
      if (response.status !== 200) {
        return Promise.reject(response.status);
      } else {
        return response.json();
      }
    });
  }