import { URL_API } from "../config/const";
import axios from "axios";

import { getToken, getUserId } from "../crud/auth.crud";
export function create(collection) {
  var myHeaders = new Headers({
    "Content-Type": "application/json",
    Authorization: getToken(),
  });
  const path = "/api/collection";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(collection),
  };

  return fetch(URL_API + path, miInit).then((response) => {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function getCollectionById(id) {
  var myHeaders = new Headers({
    "Content-Type": "application/json",
    Authorization: getToken(),
  });
  const path = "/api/collection/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then((response) => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function getCollectionBySlug(slug) {
  var myHeaders = new Headers({
    "Content-Type": "application/json",
    Authorization: getToken(),
  });
  const path = "/api/collection/slug/" + slug;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then((response) => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function getResumeCollectionByUser() {
  var myHeaders = new Headers({
    "Content-Type": "application/json",
    Authorization: getToken(),
  });
  const path = "/api/collection/resume/byuser/" + getUserId();

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then((response) => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function deleteCollection(id) {
  var myHeaders = new Headers({ Authorization: getToken() });
  const path = "/api/collection/" + id;

  var miInit = {
    method: "DELETE",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then((response) => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function update(id, data) {
  var myHeaders = new Headers({
    "Content-Type": "application/json",
    Authorization: getToken(),
  });

  const path = "/api/collection/" + id;

  var miInit = {
    method: "PUT",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data),
  };

  return fetch(URL_API + path, miInit).then((response) => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}
export function find_collection(form) {
  const path = "/api/collection/find";
  const headers = { Authorization: getToken() };

  return axios
    .post(URL_API + path, form, {
      headers: headers,
    })
    .then((response) => {      
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}
export function getKeywords() {
  var myHeaders = new Headers({
    "Content-Type": "application/json",
    Authorization: getToken(),
  });
  const path = "/api/collection/find/keywords";

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then((response) => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}


export function getlist(name) {

  var myHeaders = new Headers({
    "Content-Type": "application/json",
    Authorization: getToken(),
  });
  const path = "/api/list/byname/"+name;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then((response) => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });

}

export function getCollectionsByKeywords(propierty, keyword) {
  const path = "/api/collection/find/collectionsByKeywords";
  const headers = { Authorization: getToken() };   

  return axios
    .post(URL_API + path, {[propierty]: keyword}, {
      headers: headers,
    })
    .then((response) => {      
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}



export function getCollectionsByCategory(category) {
  const path = "/api/collection/find/collectionsByCategory";
  const headers = { Authorization: getToken() };

  return axios
    .post(URL_API + path, {category}, {
      headers: headers,
    })
    .then((response) => {      
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}
export function getAggregation(name) {
  const path = "/api/collection/aggregation";
  const headers = { Authorization: getToken() };

  return axios
    .post(URL_API + path, {name}, {
      headers: headers,
    })
    .then((response) => {      
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function collectionsByMetadata(key, values){
  const path = "/api/collection/find/collectionsByMetadata";
  const headers = { Authorization: getToken() };

  return axios
    .post(URL_API + path, {'key':key, 'values': values}, {
      headers: headers,
    })
    .then((response) => {      
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });

}
