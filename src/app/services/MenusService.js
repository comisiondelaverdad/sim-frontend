import axios from "axios";

import { URL_API, /*PAGE_SIZE,*/ TIMEOUT } from "../config/const";
import { getToken } from "../crud/auth.crud";

import { BehaviorSubject, Subject } from 'rxjs';


const menuEvent = new Subject();
export const menuEvent$ = menuEvent.asObservable();

export const updateCloseEvent = (event) => {
  menuEvent.next(event);
}

const multiSelectSubjecct = new BehaviorSubject({});
export const multiselect$ = multiSelectSubjecct.asObservable();


export function list(search, from) {
  let myHeaders = new Headers({ Authorization: getToken() });
  const page_size = 1000; //PAGE_SIZE

  search = search ? "q=" + search : "";
  const path =
    "/api/admin/menus?" +
    search +
    "&skip=" +
    (from - 1) * page_size +
    "&limit=" +
    page_size;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function get(id) {
  var myHeaders = new Headers({ Authorization: getToken() });

  const path = "/api/admin/menus/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}
export function getBySection(section) {
  var myHeaders = new Headers({ Authorization: getToken() });

  const path = "/api/admin/menus/section/" + section;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

// export function getCatalogadores() {
//   var myHeaders = new Headers({'Authorization': getToken()});

//   const path = "/api/users/catalogador";

//   var miInit = {
//     method: "GET",
//     headers: myHeaders,
//     mode: "cors",
//     cache: "default"
//   };

//   return fetch(URL_API + path, miInit).then(function(response) {
//     if(response.status !== 200){
//       return Promise.reject(response.status);
//     }
//     else{
//       return response.json();
//     }
//   });
// }

export function update(id, menus) {
  const path = "/api/admin/menus/" + id;
  const headers = { Authorization: getToken() };

  return axios
    .put(URL_API + path, menus, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function create(menus) {
  const path = "/api/admin/menus/";
  const headers = { Authorization: getToken() };

  return axios
    .post(URL_API + path, menus, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function eliminar(id) {
  const path = "/api/admin/menus/" + id;
  const headers = { Authorization: getToken() };

  return axios
    .delete(URL_API + path, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function send_element(id, form) {
  const path = "/api/admin/menus/element/" + id;
  const headers = { Authorization: getToken() };

  return axios
    .post(URL_API + path, form, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });

}
