import {URL_API} from "../config/const"
import { getToken } from "../crud/auth.crud";

// LOGS

export function serviceAdd(data) {
  var myHeaders = new Headers({"Content-Type": "application/json", 'Authorization': getToken()});

  const path = "/api/logs";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 201){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function serviceDownload(type){
  var myHeaders = new Headers({"Content-Type": "application/json", 'Authorization': getToken()});

  const path = "/api/logs/descarga";

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.blob();
    }
  });
}