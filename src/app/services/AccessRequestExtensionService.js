import {getToken} from "../crud/auth.crud";
import axios from "axios";
import {TIMEOUT, URL_API} from "../config/const";

export function createAccessRequestExtension(data) {
    const headers = {'Authorization': getToken()};

    return axios.post(`${URL_API}/api/access-request-extension/`, data, {
        headers: headers,
        timeout: TIMEOUT
    }).then((response) => {
        return response.data
    }).catch((error) => {
        return Promise.reject(error);
    });
}

export function updateAccessRequestExtension(data) {
    const headers = {'Authorization': getToken()};

    return axios.put(`${URL_API}/api/access-request-extension/${data.ident}`, data, {
        headers: headers,
        timeout: TIMEOUT
    }).then((response) => {
        return response.data;
    }).catch(function (error) {
        return Promise.reject(error);
    });
}
