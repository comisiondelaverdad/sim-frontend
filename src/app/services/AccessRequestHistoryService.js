import {getToken} from "../crud/auth.crud";
import axios from "axios";
import {TIMEOUT, URL_API} from "../config/const";

export function createAccessRequestHistory(data) {
    const headers = {'Authorization': getToken()};

    return axios.post(`${URL_API}/api/access-requests-history/`, data, {
        headers: headers,
        timeout: TIMEOUT
    }).then((response) => {
        return response.data
    }).catch((error) => {
        return Promise.reject(error);
    });
}
