import axios from "axios";

import { URL_API, /*PAGE_SIZE,*/ TIMEOUT } from "../config/const";
import { getToken } from "../crud/auth.crud";

export function list(search, from) {
  let myHeaders = new Headers({ Authorization: getToken() });
  const page_size = 1000; //PAGE_SIZE

  search = search ? "q=" + search : "";
  const path =
    "/api/admin/microsites?" +
    search +
    "&skip=" +
    (from - 1) * page_size +
    "&limit=" +
    page_size;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

export function get(id) {
  var myHeaders = new Headers({ Authorization: getToken() });

  const path = "/api/admin/microsites/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}
export function getBySection(section) {
  var myHeaders = new Headers({ Authorization: getToken() });

  const path = "/api/admin/microsites/section/" + section;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    } else {
      return response.json();
    }
  });
}

// export function getCatalogadores() {
//   var myHeaders = new Headers({'Authorization': getToken()});

//   const path = "/api/users/catalogador";

//   var miInit = {
//     method: "GET",
//     headers: myHeaders,
//     mode: "cors",
//     cache: "default"
//   };

//   return fetch(URL_API + path, miInit).then(function(response) {
//     if(response.status !== 200){
//       return Promise.reject(response.status);
//     }
//     else{
//       return response.json();
//     }
//   });
// }

export function update(id, microsite) {
  const path = "/api/admin/microsites/" + id;
  const headers = { Authorization: getToken() };

  return axios
    .put(URL_API + path, microsite, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function create(microsite) {
  const path = "/api/admin/microsites/";
  const headers = { Authorization: getToken() };

  return axios
    .post(URL_API + path, microsite, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function eliminar(id) {
  const path = "/api/admin/microsites/" + id;
  const headers = { Authorization: getToken() };

  return axios
    .delete(URL_API + path, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function send_element(id, form) {
  const path = "/api/admin/microsites/element/" + id;
  const headers = { Authorization: getToken() };

  return axios
    .post(URL_API + path, form, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function findMicrosite(form) {
  const path = "/api/admin/microsites/find";
  const headers = { Authorization: getToken() };

  return axios
    .post(URL_API + path, form, {
      headers: headers,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export async function findByName(name) {
  const path = "/api/admin/microsites/findByName";
  const headers = { Authorization: getToken() };

  try {
    const res = await axios.post(URL_API + path, { name }, { headers });
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function findBySection(section) {
  const path = "/api/admin/microsites/findBySection";
  const headers = { Authorization: getToken() };

  try {
    const res = await axios.post(URL_API + path, { section }, { headers });
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
}
