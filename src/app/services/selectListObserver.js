import { BehaviorSubject } from 'rxjs';


const listSelected = new BehaviorSubject({});
export const listSelected$ = listSelected.asObservable();

export const updateListSelected = (list) => {
    listSelected.next(list);
}

const multiSelectSubjecct = new BehaviorSubject({});
export const multiselect$ = multiSelectSubjecct.asObservable();

export const updateMultiselect = (event) => {
    console.log(event);
    multiSelectSubjecct.next(event);
}


const selectRecursiveSubject = new BehaviorSubject({});
export const selectRecursive$ = selectRecursiveSubject.asObservable();
let selectRecursiveObject = {};
export const updateSelectRecursive = (event) => {
    selectRecursiveObject = { ...selectRecursiveObject, ...event };
    selectRecursiveSubject.next(selectRecursiveObject);
}

export const removeSelectRecursive = () => {
    selectRecursiveObject = {};
    selectRecursiveSubject.next(null);
}
