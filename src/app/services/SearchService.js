import { URL_API, PAGE_SIZE } from "../config/const"
import { getToken, getUserId } from "../crud/auth.crud";

export function serviceKeyword(search, from, filters = '', showTags = 0) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });
  search = search ? "q=" + encodeURIComponent(search) : "";
  const path = "/api/search?" + search + "&from=" + ((from - 1) * PAGE_SIZE) + "&size=" + PAGE_SIZE + "&showTags=" + showTags;

  var data = {
    "filters": filters
  }

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function serviceKeywordMuseo(search, from, filters = {}, size = 10) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  var data = {
    user: getUserId(),
    source: "museo",
    q: search,
    from: from,
    size: size
  }

  if (filters.temporalCoverage)
    data['temporalCoverage'] = filters.temporalCoverage
  if (filters.dpto)
    data['dpto'] = filters.dpto
  if (filters.tipo)
    data['tipo'] = filters.tipo
  if (filters.fondo)
    data['fondo'] = filters.fondo
  if (filters.type)
    data['type'] = filters.type
  if (filters.resourceGroupId)
    data['resourceGroupId'] = filters.resourceGroupId
  if (filters.accessLevel)
    data['accessLevel'] = filters.accessLevel
  if (filters.order)
    data['order'] = filters.order
  if (filters.idents)
    data['idents'] = filters.idents
  if (filters.place)
    data['place'] = filters.place
  if (filters.keyword)
    data['keyword'] = filters.keyword

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  const path = "/api/search"


  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function serviceSingleMuseo(slug) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  const path = "/api/search/detail/" + slug

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function searchSuggestMuseo(str) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });
  var data = {
    q: str
  }

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  const path = "/api/search/suggestmuseo"

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function serviceAdd(data) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/search/search-history";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function serviceDelete(id) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/search/search-history/" + id;

  var miInit = {
    method: "DELETE",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function serviceLoadSaved(userid, filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  const path = "/api/search/search-history/" + userid;

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(filters)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if (response.status !== 201) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function list(id) {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  const path = "/api/search/get-history/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function getResourceHistory(id) {
  var myHeaders = new Headers({ 'Authorization': getToken() });

  const path = "/api/search/get-view/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}