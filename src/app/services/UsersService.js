import axios from 'axios';

import {URL_API, /*PAGE_SIZE,*/ TIMEOUT} from "../config/const"
import { getToken } from "../crud/auth.crud";

export function list(search,from) {
  let myHeaders = new Headers({'Authorization': getToken()});
  const page_size = 1000;//PAGE_SIZE

  search = search ? "q="+search : "";
  const path = "/api/admin/users?" + search + "&skip=" + ((from-1)*page_size) + "&limit=" + page_size;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function get(id) {
  var myHeaders = new Headers({'Authorization': getToken()});

  const path = "/api/admin/users/" + id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function getCatalogadores() {
  var myHeaders = new Headers({'Authorization': getToken()});

  const path = "/api/users/catalogador";

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(function(response) {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function update(id, users) {
  const path = "/api/admin/users/" + id;
  const headers = {'Authorization': getToken()};

  return axios.put(URL_API + path, users, {
      headers: headers,
      timeout: TIMEOUT
  }).then((response) => {
      return response.data
  }).catch((error) => {
      return Promise.reject(error);
  });
}

export function downloadQuota() {

  console.log("Consultando...")

  const path = "/api/users/quota";
  var myHeaders = new Headers({'Authorization': getToken()});


  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };



  return fetch(URL_API + path, miInit).then(function(response) {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}