import {URL_API} from "../config/const"
import { getToken } from "../crud/auth.crud";

// BOOKMARKS

export function serviceAdd(data) {
  var myHeaders = new Headers({"Content-Type": "application/json", 'Authorization': getToken()});

  const path = "/api/bookmarks";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 201){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function serviceList() {
  var myHeaders = new Headers({'Authorization': getToken()});

  const path = "/api/bookmarks";

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function serviceUpdate(id,data) {
  var myHeaders = new Headers({"Content-Type": "application/json", 'Authorization': getToken()});

  const path = "/api/bookmarks/"+id;

  var miInit = {
    method: "PUT",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function serviceDelete(id) {
  var myHeaders = new Headers({"Content-Type": "application/json", 'Authorization': getToken()});

  const path = "/api/bookmarks/"+id;

  var miInit = {
    method: "DELETE",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function serviceListByUser(id) {
  var myHeaders = new Headers({'Authorization': getToken()});

  const path = "/api/bookmarks/"+id;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

// LABELS

export function serviceLabelsByUser(userid) {
  var myHeaders = new Headers({'Authorization': getToken()});

  const path = "/api/bookmarks/labelsbyuser/" + userid ;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}

export function serviceUpdateLabels(userid,label) {
  var myHeaders = new Headers({"Content-Type": "application/json",'Authorization': getToken()});

  const path = "/api/bookmarks/labelsbyuser/" + userid ;

  var miInit = {
    method: "PUT",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(label)
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
  
}
