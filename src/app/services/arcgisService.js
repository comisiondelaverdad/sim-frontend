import { URL_API } from "../config/const"
import { getToken } from "../crud/auth.crud";

export function getSuggestions(text, maxSuggestions = 1) {
    const path = `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/suggest?f=json&text=${text}&maxSuggestions=${maxSuggestions}`;
    var miInit = {
        method: "GET",
        mode: "cors",
        cache: "default"
    };

    return fetch(path, miInit).then(response => {
        if (response.status !== 200) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}

export function getLocation(text, magicKey, maxLocation = 1, outFields = '*') {
    const path = `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=${text}&f=json&outSR=%7B%22wkid%22%3A102100%7D&outFields=${outFields}&magicKey=${magicKey}&maxLocations=${maxLocation}`
    var miInit = {
        method: "GET",
        mode: "cors",
        cache: "default"
    };

    return fetch(path, miInit).then(response => {
        if (response.status !== 200) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}

export async function getLocationByText(text, maxLocation = 1, outFields = '*') {
    const suggestions = await getSuggestions(text);
    if(suggestions.suggestions[0].magicKey){
        return getLocation(text, suggestions.suggestions[0].magicKey, maxLocation, outFields);
    }else {
        return [];
    }
}

export function worldAdministrativeDivision(iso) {
    const path = `https://services.arcgis.com/P3ePLMYs2RVChkJx/ArcGIS/rest/services/World_Administrative_Divisions/FeatureServer/0/query?where=ISO_CC+%3D+%27${iso}%27&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=4326&spatialRel=esriSpatialRelIntersects&resultType=none&distance=0.0&units=esriSRUnit_Meter&returnGeodetic=false&outFields=*&returnGeometry=false&returnCentroid=true&featureEncoding=esriDefault&multipatchOption=xyFootprint&maxAllowableOffset=&geometryPrecision=&outSR=4326&datumTransformation=&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnQueryGeometry=false&returnDistinctValues=false&cacheHint=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&having=&resultOffset=&resultRecordCount=&returnZ=false&returnM=false&returnExceededLimitFeatures=true&quantizationParameters=&sqlFormat=none&f=pjson&token=`
    var miInit = {
        method: "GET",
        mode: "cors",
        cache: "default"
    };

    return fetch(path, miInit).then(response => {
        if (response.status !== 200) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}

export function colAdministrativeDivision() {
    const path = `${URL_API}/api/resources/departaments`;
    var myHeaders = new Headers({"Content-Type": "application/json", 'Authorization': getToken()});

    var miInit = {
        method: "GET",
        mode: "cors",
        headers: myHeaders,
        cache: "default"
    };

    return fetch(path, miInit).then(response => {
        if (response.status !== 200) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}


export function worldCountries() {
    const path = `${URL_API}/api/resources/countries`;
    var myHeaders = new Headers({"Content-Type": "application/json", 'Authorization': getToken()});

    var miInit = {
        method: "GET",
        mode: "cors",
        headers: myHeaders,
        cache: "default"
    };

    return fetch(path, miInit).then(response => {
        if (response.status !== 200) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}

export function colmunicipality(id) {
    const path = `${URL_API}/api/resources/municipality/${id}`;
    var myHeaders = new Headers({"Content-Type": "application/json", 'Authorization': getToken()});

    var miInit = {
        method: "GET",
        mode: "cors",
        headers: myHeaders,
        cache: "default"
    };

    return fetch(path, miInit).then(response => {
        if (response.status !== 200) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}