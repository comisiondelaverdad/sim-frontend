import { URL_API } from "../config/const"
import { getToken } from "../crud/auth.crud";
import axios from "axios";

export function tagCloud(filters, type = true, selected = []) {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path

  path = "/api/viz/tagcloud";
  if (selected.length > 0) if (selected[0] === '') selected = []
  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify({ ...filters, "type": type, "gruposEntidades": selected })
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function getTimeline(from, to) {
  return axios
    .get(`${URL_API}/api/linea/${from}/from/${to}/to`)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export function getFondos() {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = "/api/viz/getFondos";

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function mapping() {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = "/api/viz/mapping";

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function grafoEntidades(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path

  path = "/api/viz/grafoentidades"

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(filters)
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    console.log(response.status)
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();

      return resp
    }
  });
}

export function totalSearchHits(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = '/api/viz/totalsearchhits'

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(filters)
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function getEtiquetas(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = '/api/viz/getetiquetas'

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(filters)
  };

  console.log(JSON.stringify(miInit))

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function getLineaTiempo(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = '/api/viz/getlineatiempo'

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(filters)
  };

  console.log(filters)

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function getMapaViz(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = '/api/viz/getmapaviz'

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(filters)
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function getMapaVizEnlaces(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = '/api/viz/getentidadesmapa'

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(filters)
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function getNgram(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = '/api/viz/getngram'

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(filters)
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function getHistogramDates(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = '/api/viz/getHistogramDates'

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(filters)
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

export function getListaEtiquetas() {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = '/api/viz/getEtiquetas'

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json()

      console.log(resp)
      return resp
    }
  });
}

export function getListaEntidades() {
  var myHeaders = new Headers({ "Content-Type": "application/json", "Authorization": getToken() });
  let path = '/api/viz/getEntidades'

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      let resp = response.json();
      return resp
    }
  });
}

/**
 * SERVICIOS de viz para el museo
 */

export function getMapaMuseoDpto(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  var data = {}

  if (filters.q !== undefined) data['q'] = filters.q
  if (filters.temporalCoverage !== undefined) data['temporalCoverage'] = filters.temporalCoverage
  if (filters.dpto !== undefined) data['dpto'] = filters.dpto

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  const path = "/api/viz/getMuseoDptoMapa"

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}

export function getMapaMuseoMpio(filters) {
  var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

  var data = {}

  if (filters.q !== undefined) data['q'] = filters.q
  if (filters.temporalCoverage !== undefined) data['temporalCoverage'] = filters.temporalCoverage
  if (filters.dpto !== undefined) data['dpto'] = filters.dpto

  var miInit = {
    method: "POST",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
    body: JSON.stringify(data)
  };

  const path = "/api/viz/getMuseoMpioMapa"

  return fetch(URL_API + path, miInit).then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(response.status);
    }
    else {
      return response.json();
    }
  });
}
