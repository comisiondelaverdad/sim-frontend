import { URL_API, RECAPTCHA_CLIENT } from "../config/const";
import Swal from "sweetalert2"

export default function login({ username, password }) {

    return fetch(URL_API + "/auth/loginAD", {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ username, password })
    });

}


export const tokenValid = (token) => {
    var myHeaders = new Headers({ "Content-Type": "application/json" });

    const path = "/auth/validCaptcha";

    var miInit = {
        method: "POST",
        headers: myHeaders,
        cache: "default",
        body: JSON.stringify({ token: token })
    };

    return fetch(URL_API + path, miInit).then(response => {
        if (![200, 201].includes(response.status)) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}

export const validRecaptchaFlow = (redirect, notCaptcha = false) => {
    if (notCaptcha) {
        window.location.replace(URL_API + redirect);
    } else {
        window.grecaptcha.ready(() => {
            window.grecaptcha.execute(RECAPTCHA_CLIENT, { action: 'submit' }).then((token) => {
                tokenValid(token).then((response) => {
                    if (response.success) {
                        window.location.replace(URL_API + redirect)
                    } else {
                        Swal.fire(
                            'Error de validación de captcha',
                            `ERROR: ${(response["error-codes"]).toString()}`,
                            'error'
                        )
                    }
                })
            });
        });
    }
}
