import {URL_API} from "../config/const"
import { getToken } from "../crud/auth.crud";

export function serviceList(userid) {
  var myHeaders = new Headers({'Authorization': getToken()});

  const path = "/api/notifications/"+userid;

  var miInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default"
  };

  return fetch(URL_API + path, miInit).then(response => {
    if(response.status !== 200){
      return Promise.reject(response.status);
    }
    else{
      return response.json();
    }
  });
}