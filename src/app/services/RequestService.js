import { getToken } from '../crud/auth.crud';
import { TIMEOUT, URL_API, PAGE_SIZE } from '../config/const';

export function getData(path) {
    var myHeaders = new Headers({ 'Authorization': getToken() });

    var miInit = {
        method: "GET",
        headers: myHeaders,
        mode: "cors",
        cache: "default"
    };

    return fetch(URL_API + path, miInit).then(async response => {
        if (response.status !== 200) {
            const err = await response.json()
            return Promise.reject(err);
        }
        else {
            console.log("Response.status: ", response.status);
            console.log("Response: ", response);
            return response.json();
        }
    });
}


export function postData(path, body) {
    var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

    var miInit = {
        method: "POST",
        headers: myHeaders,
        mode: "cors",
        cache: "default",
        body: JSON.stringify(body)
    };

    return fetch(URL_API + path, miInit).then(response => {
        if (response.status !== 200) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}

export function putData(path, body) {
    var myHeaders = new Headers({ "Content-Type": "application/json", 'Authorization': getToken() });

    var miInit = {
        method: "PUT",
        headers: myHeaders,
        mode: "cors",
        cache: "default",
        body: JSON.stringify(body)
    };

    return fetch(URL_API + path, miInit).then(response => {
        if (response.status !== 200) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}


export function deleteData(path) {
    var myHeaders = new Headers({ 'Authorization': getToken() });

    var miInit = {
        method: "DELETE",
        headers: myHeaders,
        mode: "cors",
        cache: "default"
    };

    return fetch(URL_API + path, miInit).then(response => {
        if (response.status !== 200) {
            return Promise.reject(response.status);
        }
        else {
            return response.json();
        }
    });
}
