import axios from 'axios';

import {TIMEOUT, URL_API} from '../config/const';
import {getToken} from '../crud/auth.crud';


export function getAccessRequests() {
    const headers = {'Authorization': getToken()};

    return axios.get(`${URL_API}/api/access-requests/all`, {
        headers: headers,
    }).then((response) => {
        return response.data;
    }).catch((error) => {
        return Promise.reject(error);
    });
}

export function getMyAccessRequests(ident) {
    const headers = {'Authorization': getToken()};

    return axios.get(`${URL_API}/api/access-requests/my?ident=${ident}`, {
        headers: headers,
        timeout: TIMEOUT
    }).then((response) => {
        return response.data;
    }).catch((error) => {
        return Promise.reject(error);
    });
}

export function getUserAccessRequests(userId) {
    const headers = {'Authorization': getToken()};

    return axios.get(`${URL_API}/api/users/${userId}/access-request`, {
        headers: headers,
        // timeout: TIMEOUT
    }).then((response) => {
        return response.data;
    }).catch((error) => {
        return Promise.reject(error);
    });
}

export function getAccessRequest(id) {
    const headers = {'Authorization': getToken()};

    return axios.get(`${URL_API}/api/access-requests/${id}`, {
        headers: headers,
        timeout: TIMEOUT
    }).then((response) => {
        return response.data;
    }).catch((error) => {
        return Promise.reject(error);
    });
}

export function createAccessRequest(data) {
    const headers = {'Authorization': getToken()};

    return axios.post(`${URL_API}/api/access-requests/`, data, {
        headers: headers,
        timeout: TIMEOUT
    }).then((response) => {
        return response.data
    }).catch((error) => {
        return Promise.reject(error);
    });
}

export function removeAccessRequest(id) {
    const headers = {'Authorization': getToken()};

    return axios.delete(`${URL_API}/api/access-requests/${id}`, {
        headers: headers,
        timeout: TIMEOUT
    }).then((response) => {
        return response.data;
    }).catch(function (error) {
        return Promise.reject(error);
    });
}

export function updateAccessRequest(data) {
    const headers = {'Authorization': getToken()};

    return axios.put(`${URL_API}/api/access-requests/${data.ident}`, data, {
        headers: headers,
        timeout: TIMEOUT
    }).then((response) => {
        return response.data;
    }).catch(function (error) {
        return Promise.reject(error);
    });
}
