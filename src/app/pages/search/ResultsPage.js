import React, { Component } from "react";
import { NavLink } from 'react-router-dom';
import { connect } from "react-redux";
import _ from "lodash";
import * as app from "../../store/ducks/app.duck";
import Result from "../../components/organisms/Result";
import { Container, Row, Alert, Button, Col } from "react-bootstrap";
import * as SearchService from "../../services/SearchService";
import * as BookmarksService from "../../services/BookmarksService";
import Paginator from "../../components/molecules/Paginator";
import { PAGE_SIZE, ELASTIC_LIMIT } from "../../config/const";
import * as AccessRequestService from "../../services/AccessRequestService";
import * as StatsService from "../../services/StatsService";
import moment from "moment";
import VizSelect from '../../components/organisms/VizSelect';
import { errorAlert } from "./../../services/utils"
import DownloadResult from "../../components/molecules/DownloadResult";
import Swal from "sweetalert2";
import { getResourceByIdent } from '../../services/ResourcesService';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

class ResultsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      results: [],
      views: [],
      bookmarks: [],
      activeAccessRequests: [],
      pendingAccessRequests: [],
      viz: '',
      loading: true,
      dataset: [],
      export: false,
      externas: false,
      associatedResources: []
    };
    this.pageChange = this.pageChange.bind(this);
  }

  componentDidUpdate(prevProps) {
    let oldQuery = new URLSearchParams(prevProps.location.search).toString();
    let query = new URLSearchParams(this.props.location.search);
    let numberFormatter = new Intl.NumberFormat("de-DE");
    let showTags = this.props.showTags ? 1 : 0;
    if (query.toString() === "") {
      this.props.history.push("/");
    }
    else {
      if (oldQuery !== query.toString() || prevProps.showTags !== this.props.showTags || prevProps.searchFilters.filters.resourceGroup.resourceGroupText !== this.props.searchFilters.filters.resourceGroup.resourceGroupText || prevProps.searchFilters.filters.mapPolygon !== this.props.searchFilters.filters.mapPolygon) {
        this.setState({
          loading: true,
          results: []
        })
        this.searchKeyword(query.get("q"), query.get("from"), this.props.searchFilters.filters, showTags);
        this.loadBookmarks();
        this.loadActiveAccessRequests();
        this.loadSavedSearch();
      }

      let from = query.get("from");
      this.props.pageTitle("Resultados");
      this.props.pageSubtitle(
        this.state.results &&
        this.state.results.total &&
        this.state.results.hits &&
        this.state.results.hits.length > 0 &&
        "Viendo " +
        (parseInt(from) === 1 ? parseInt(from) : parseInt(from) * PAGE_SIZE - PAGE_SIZE + 1) + " - " +
        (parseInt(from) * PAGE_SIZE < this.state.results.total.value ? numberFormatter.format(parseInt(from) * PAGE_SIZE) : numberFormatter.format(this.state.results.total.value)) +
        " de " + numberFormatter.format(this.state.results.total.value)
      );
      this.props.saveSearchButton(this.props.showSaveSearchButton);
      this.props.showVisualizationBar(this.props.showVisualizationsBar);
    }
  }

  componentDidMount() {

    if (this.props.searchFilters.filters && this.props.searchFilters.filters.resourceGroup &&
      this.props.searchFilters.filters.resourceGroup.resourceGroupIDS &&
      this.props.searchFilters.filters.resourceGroup.resourceGroupIDS == "105-OE") {
      this.setState({ externas: true })
    }

    let query = new URLSearchParams(this.props.location.search);

    if (query.toString() === "") {
      this.props.history.push("/");
    }
    else {
      this.getAssociatedResources(query.get("q") ? query.get("q") : "");
      this.props.keyword(query.get("q") ? query.get("q") : "");
      this.searchKeyword(query.get("q"), query.get("from"), this.props.searchFilters.filters);
      this.loadBookmarks();
      this.loadActiveAccessRequests();
      this.loadSavedSearch();
      this.props.showSearch(true);
      this.props.showVisualizationBar(true);
      this.props.showVisualization(true);
      this.props.saveSearchButton(true);
      this.props.showSearchTags(false);
      this.props.fromComponent("search");
    }
  }

  componentWillUnmount() {
    this.props.saveSearchButton(false);
    this.props.showVisualization(false);
    this.props.showSearch(false);
    this.props.newNotification(false);
  }

  loadBookmarks() {
    BookmarksService.serviceLabelsByUser(this.props.user._id)
      .then(
        (data) => {
          this.setState({
            bookmarks: data
          });
        },
        (error) => {
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar la información',
            confirmButtonText: 'Aceptar',
            action: () => { this.props.history.push('/dashboard') }
          })
        }
      )
  }

  loadViews(data) {
    let results = data;
    let ids = _.map(data.hits, (x) => {
      if (x._source.resource) {
        return x._source.resource.ident;
      }
    }).filter(x => x);

    StatsService.resourceViews(ids).then((data) => {
      let views = {};
      for (let i = 0; i < data.resource_views.buckets.length; i++) {
        views[data.resource_views.buckets[i].key] = data.resource_views.buckets[i].doc_count
      }
      this.setState({
        views: views,
        loading: false,
        results: results
      });
    }).catch((error) => {
      this.setState({
        views: [],
        loading: false,
        results: results
      });
      console.log(error);
      // if (error) {
      //   this.props.history.push("/logout");
      // }
    });
  }

  searchKeyword(search, from, filters, showTags = 0) {
    SearchService.serviceKeyword(search, from, filters, showTags)
      .then(
        (data) => {
          this.loadViews(data);
        },
        (error) => {
          console.log(error)
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar el recurso',
            confirmButtonText: 'Regresar',
            action: () => { }
          })
        }
      )
  }

  loadActiveAccessRequests() {
    AccessRequestService.getUserAccessRequests(this.props.user._id).then((data) => {
      this.setState({
        activeAccessRequests: _.map(_.filter(data, (r) => {
          return r.status === 'approved' && moment().local().isBetween(moment(r.grantedFrom).local(), moment(r.grantedTo).local());
        }), (r) => {
          return { accessRequestIdent: r.ident, resourceIdent: r.resource.ident };
        }),
        pendingAccessRequests: _.map(_.filter(data, (r) => {
          return r.status === 'pending' || (r.status === 'approved' && moment().local().isBefore(moment(r.grantedFrom).local()));
        }), (r) => {
          return { accessRequestIdent: r.ident, resourceIdent: r.resource.ident };
        })
      });
    }).catch((error) => {
      console.log('An unexpected error occurred while loading active access requests: %s', error);
    });
  }

  loadSavedSearch() {
    SearchService.serviceLoadSaved(this.props.user._id, this.props.searchFilters)
      .then(
        (data) => {
          this.props.saveSearchButton(data === 0 ? true : false);
        },
        (error) => {
          console.log("Error", error);
        }
      )
  }

  pageChange(page) {
    this.setState({
      loading: true,
      results: []
    })
    if (this.props.searchKeyword) {
      this.props.history.push("/search?q=" + this.props.searchKeyword + "&from=" + page);
    }
    else {
      this.props.history.push("/search?from=" + page);
    }
  }

  exportExcel() {

    // console.log(this.state.results)
    // return false

    let dataset = this.state.results.hits.filter(item => { return item._source.document }).map(item => (

      {
        ident: item?._source?.document?.resource?.ident,
        url: item?._source?.document?.resource?.metadata?.firstLevel?.url ?? '',
        documentalId: item?._source?.document?.resource?.metadata?.firstLevel?.documentalId ?? '',
        documentType: item?._source?.document?.resource?.metadata?.firstLevel?.documentType ?? '',
        descriptionLevel: item?._source?.document?.resource?.metadata?.firstLevel?.descriptionLevel ?? '',
        title: item?._source?.document?.resource?.metadata?.firstLevel?.title ?? '',
        otherTitles: item?._source?.document?.resource?.metadata?.firstLevel?.otherTitles ?? '',
        authors: item?._source?.document?.resource?.metadata?.firstLevel?.authors ?? '',
        collaborators: item?._source?.document?.resource?.metadata?.firstLevel?.collaborators ?? '',
        area: item?._source?.document?.resource?.metadata?.firstLevel?.area ?? '',
        custodian: item?._source?.document?.resource?.metadata?.firstLevel?.custodian ?? '',
        rights: item?._source?.document?.resource?.metadata?.firstLevel?.rights ?? '',
        accessLevel: item?._source?.document?.resource?.metadata?.firstLevel?.accessLevel ?? '',
        sourceLocation: item?._source?.document?.resource?.metadata?.firstLevel?.sourceLocation ?? '',
        creationDate: item?._source?.document?.resource?.metadata?.firstLevel?.creationDate ?? '',
        description: item?._source?.document?.resource?.metadata?.firstLevel?.description ?? '',
        topics: item?._source?.document?.resource?.metadata?.firstLevel?.topics ?? '',
        mandate: item?._source?.document?.resource?.metadata?.firstLevel?.mandate ?? '',
        objective: item?._source?.document?.resource?.metadata?.firstLevel?.objective ?? '',
        focus: item?._source?.document?.resource?.metadata?.firstLevel?.focus ?? '',
        milestone: item?._source?.document?.resource?.metadata?.firstLevel?.milestone ?? '',
        violenceType: item?._source?.document?.resource?.metadata?.missionLevel?.humanRights?.violenceType ?? '',
        temporalCoverage: item?._source?.document?.resource?.metadata?.firstLevel?.temporalCoverage ?? '',
        geographicCoverage: item?._source?.document?.resource?.metadata?.firstLevel?.geographicCoverage ?? '',
        conflictActors: item?._source?.document?.resource?.metadata?.missionLevel?.humanRights?.conflictActors ?? '',
        population: item?._source?.document?.resource?.metadata?.firstLevel?.population ?? '',
        occupation: item?._source?.document?.resource?.metadata?.firstLevel?.occupation ?? '',
        language: item?._source?.document?.resource?.metadata?.firstLevel?.language ?? '',
        associatedResources: item?._source?.document?.resource?.metadata?.firstLevel?.associatedResources ?? '',
        associatedResourceGroups: item?._source?.document?.resource?.metadata?.firstLevel?.associatedResourceGroups ?? ''
      })

    )

    if (dataset.length) {
      dataset = dataset.filter(function ({ ident }) {
        return !this[ident] && (this[ident] = ident)
      }, {})

      // dataset = dataset.map((item) => {
      //   const asArray = Object.entries(item);

      //   item = asArray.filter((element) => {
      //     console.log(element)
      //     if(!element[1])
      //       element[1] = '';
      //     console.log(element)
      //     return element
      //   })

      //   item = Object.fromEntries(item);

      //   return item

      // })
      // return false

      this.setState({ dataset, export: true })
      setTimeout(() => {
        this.setState({ export: false })
      }, 1000);
    }
    else {
      Swal.fire({
        title: 'No existen metadatos para los recursos de estos resultados, puedes probar con la siguiente página!',
        focusConfirm: false,
        confirmButtonText:
          'Aceptar'
      })
    }
  }

  getAssociatedResources(query) {
    let ident = query.trim().split(" ");
    if (ident.length === 1) {
      ident = ident.toString();
      if (ident !== "") {
        getResourceByIdent(ident)
          .then(res => {
            const listResources = res?.metadata?.firstLevel?.associatedResources;
            if (listResources) {
              listResources.forEach(item => {
                this.setState({ associatedResources: [...this.state.associatedResources, item] })
              })
            } else this.setState({ associatedResources: [] })
          })
          .catch(err => console.log("Ocurrió un error o no existen recursos asociados"))
      }
    }
  }

  render() {
    let queryPath = this.props.location.pathname;
    let query = new URLSearchParams(this.props.location.search);
    let highlight = this.props.showTags ? "2" : "1";

    return (
      <React.Fragment>{
        this.state.export &&
        <DownloadResult dataset={this.state.dataset} />
      }

        {this.state.results && this.state.results.total && this.state.results.total.value > ELASTIC_LIMIT &&
          <Container fluid={true} className="my-5">
            <div className="found-multiple-results">
              Su consulta ha generado más de 10.000 resultados, le sugerimos el uso de filtros para mejorar la búsqueda
            </div>
          </Container>
        }
        {this.props.showVisualizationsBar && this.state.results && this.state.results.hits && this.state.results.hits.length > 0 &&
          <VizSelect
            location={this.props.location}
            filter={this.props.searchFilters}
            total={this.state.results.total.value}
          />
        }
        {this.state.loading &&
          <Container fluid={true} className="my-5">
            <Alert variant="success">
              <div className="alert-icon">
                <i className="fas fa-sync fa-spin" />
              </div>
              <div className="alert-text">Cargando...</div>
            </Alert>
          </Container>
        }
        {!this.state.externas && this.state.results && this.state.results.hits && this.state.results.hits.length === 0 && !this.state.loading &&
          <div className="not-fount-results">
            <h3>No encontrado</h3>
            <h2>No hay resultados para los criterios de búsqueda</h2>
          </div>
        }
        {this.state.externas && this.state.results && this.state.results.hits && this.state.results.hits.length === 0 && !this.state.loading &&
          <div className="not-fount-results">
            <h3>No encontrado</h3>
            <h2>
              Si desea realizar búsquedas sobre Catálogo de fuentes de archivo externas, hágalo mediante el menú:
              <a href={window.location.origin + "/resourcegroup/"}> Explorar por Fondos</a>
            </h2>
          </div>
        }
        {this.props.user.roles.includes("catalogador_gestor") && this.state.results && this.state.results.hits && this.state.results.hits.length > 0 &&
          <Col className="float-right mb-2 " lg="12" >
            <Button onClick={() => this.exportExcel()} className="btn-small ml-2 btn btn-primary">  Exportar metadatos <i className="fa  fa-download"></i> </Button>
          </Col>
        }
        <div className="container-general">
          {
            this.state.results && this.state.results.hits && this.state.results.hits.map((result, idx) => (
              <React.Fragment key={`r-${idx}`}>
                {result._source.resource && result._source.extraResource && result._source.extraResource.origin && (highlight === "1" || (highlight === "2" && result._source.record.labelled)) &&
                  <Result
                    key={`rec-${idx}`}
                    record={result._source.resource}
                    extra={{
                      highlight: result.highlight,
                      bookmarks: this.state.bookmarks,
                      views: this.state.views,
                      activeAccessRequests: this.state.activeAccessRequests,
                      pendingAccessRequests: this.state.pendingAccessRequests,
                      extra: result._source.extra,
                      score: result._score,
                      highlightMode: highlight,
                      labelled: highlight === "2" ? result._source.record.labelled : '',
                      origin: result._source.extraResource.origin,
                      found: { type: result._source.record.type, ident: result._source.record.ident }
                    }}
                  />
                }
                {!result._source.resource &&
                  <div className="not-fount-results">
                    <h3 className="cev-portlet__head-title">No encontrado</h3>
                    <h2 className="badge-warning">{result._source._id}</h2>
                  </div>
                }
              </React.Fragment>
            ))}
        </div>
        {this.state.results &&
          this.state.results.total &&
          this.state.results.total.value &&
          this.state.results.buckets &&
          this.state.results.buckets.length > 0 &&
          <div className="container-general">

            <div className="container-result">
              <div className="header">
                <h3 className="label">Resumen búsqueda</h3>
              </div>
              <div className="meta-container">
                 {this.state.results.buckets.map((bucket, idx) => (
                  <div className="meta-list" key={idx}>
                    <span className="form-label">
                    {bucket.key}
                    </span>
                    <span className="meta-value">
                    {new Intl.NumberFormat("de-DE").format(bucket.doc_count)} -
                      {(bucket.doc_count / this.state.results.total.value * 100).toFixed(2)} %
                      
                    </span>
                </div>
                ))}
              </div>
            </div>
          </div>
        }
        {this.state.results &&
          this.state.results.total &&
          this.state.results.hits &&
          this.state.results.hits.length > 0 &&
          this.state.results.total.value &&
          this.state.results.total.value > 10 &&
          <Container fluid={true}>
            <Row className="justify-content-center">
              <Paginator total={this.state.results.total.value} query={query} pathname={queryPath} mode="callback" callback={this.pageChange} />
            </Row>
          </Container>
        }
      </React.Fragment>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user,
  searchKeyword: store.app.keyword,
  searchFilters: store.app.filters,
  newNotificationTrigger: store.app.newNotification,
  showVisualizationsBar: store.app.showVisualizationBar,
  showVisualizationButton: store.app.showVisualization,
  showSaveSearchButton: store.app.saveSearchButton,
  showTags: store.app.showSearchTags
});

export default connect(
  mapStateToProps,
  app.actions
)(ResultsPage);
