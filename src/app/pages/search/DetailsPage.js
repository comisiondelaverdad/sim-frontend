import React, { Component } from "react";
import { connect } from "react-redux";
import { toAbsoluteUrl } from "../../../theme/utils";
import DetailItem from "../../components/molecules/DetailItem";
import DetailContent from "../../components/molecules/DetailContent";
import Bookmarks from "../../components/organisms/BookmarksMenu";
import * as app from "../../store/ducks/app.duck";
import * as ResouceService from "../../services/ResourcesService";
import * as BookmarksService from "../../services/BookmarksService";
import { Redirect } from "react-router-dom";
import BreadCrumbs from "../../components/molecules/Breadcrumbs";
import _ from "lodash";
import * as AccessRequestService from "../../services/AccessRequestService";
import moment from "moment";
import { updateMetadata } from "./../../services/MetaService"
import { errorAlert } from "./../../services/utils"
import { Link } from 'react-router-dom';
import Swal from "sweetalert2";
import VerifiedIcon from '@mui/icons-material/Verified';


class DetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roles: props.user.roles,
      records: undefined,
      result: undefined,
      latlon: [],
      title: "",
      description: "",
      display: "",
      bookmarks: [],
      breadcumbs: [],
      activeAccessRequests: [],
      pendingAccessRequests: [],
      permissionGranted: null,
      showNavigation: true,
      resourceGroup: null,
      changed: false,
      toast: true,
    };
    this.loadTab = this.loadTab.bind(this);
  }

  componentDidMount() {
    this.setState({ toast: true });
    this.getDetail(this.props.match.params.id);
    this.loadBookmarks();
    this.loadBreadcrumbs(this.props.match.params.id);
    this.loadActiveAccessRequests();

    this.props.pageTitle("Detalle");
    this.props.pageSubtitle(this.props.match.params.id);
    this.props.showSearch(true);
    this.props.showSearchTags(false);
    this.props.fromComponent("details");
  }

  componentWillUnmount() {
    updateMetadata(null);
    this.props.showSearch(false);
    this.props.showSearchTags(false);
    this.props.fromComponent("");
    this.props.display({});
  }

  getDetail(id) {
    let records = []
    ResouceService.serviceDetail(id).then((data) => {
      if (data.error) {
        console.log("error", data.error)
        errorAlert({
          error: data ? data.status : '',
          text: data ? data.error : 'Se ha presentado un error al cargar la información',
          confirmButtonText: 'Aceptar',
          action: () => { this.props.history.push('/dashboard') }
        })
      } else {
        ResouceService.checkIn(id).then((r) => {
          if (data) {
            if (data.records && data.records.length) {
              records = data.records.filter((item) => item.extra.status != 'deleted')
            }
            this.setState({
              resourceGroup: data.identifier,
              result: data,
              title: data.metadata.firstLevel.title ? data.metadata.firstLevel.title : undefined,
              description: data.metadata.firstLevel.descriptionLevel ? data.metadata.firstLevel.descriptionLevel : undefined,
              records: records,
              display: data.origin === 'ModuloCaptura' ? this.props.app.display && this.props.app.display.display ? this.props.app.display.display : 'cev-transcription' : 'cev-metadata'
            });
          }
          if (data.metadata.firstLevel && data.metadata.firstLevel.geographicCoverage && Array.isArray(data.metadata.firstLevel.geographicCoverage)) {
            if (data.metadata.firstLevel.geographicCoverage[0].geoPoint) {
              this.setState({
                latlon: [
                  data.metadata.firstLevel.geographicCoverage[0].geoPoint.lat,
                  data.metadata.firstLevel.geographicCoverage[0].geoPoint.lon
                ],
              });
            }
          }
          updateMetadata(data);
        }).catch((error) => {
          console.log("error", error)
          errorAlert({
            error: error,
            text: error ? error.error : 'Se ha presentado un error al cargar la información',
            confirmButtonText: 'Aceptar',
            action: () => { this.props.history.push('/dashboard') }
          })
        })
      }
    }).catch((error) => {
      console.log("error", error)
      errorAlert({
        error: error,
        text: error ? error.error : 'Se ha presentado un error al cargar la información',
        confirmButtonText: 'Aceptar',
        action: () => { this.props.history.push('/dashboard') }
      })
    });
  }

  loadActiveAccessRequests() {
    const resourceIdent = this.props.match.params.id;
    const userId = this.props.user._id;

    const promise1 = ResouceService.serviceDetail(resourceIdent);
    const promise2 = AccessRequestService.getUserAccessRequests(userId);

    Promise.all([promise1, promise2]).then((data) => {
      updateMetadata(data[0]);
      const accessLevel = data[0].metadata.firstLevel.accessLevel;
      const activeAccessRequests = _.map(_.filter(data[1], (r) => {
        return r.resource != null && r.status === 'approved' && moment().local().isBetween(moment(r.grantedFrom).local(), moment(r.grantedTo).local());
      }), (r) => {
        return { accessRequestIdent: r.ident, resourceIdent: r.resource.ident };
      });

      const pendingAccessRequests = _.map(_.filter(data[1], (r) => {
        return r.resource != null && (r.status === 'pending' || (r.status === 'approved' && moment().local().isBefore(moment(r.grantedFrom).local())));
      }), (r) => {
        return { accessRequestIdent: r.ident, resourceIdent: r.resource.ident };
      });

      const permissionGranted = parseInt(accessLevel) >= this.props.user.accessLevel ? true : activeAccessRequests.map(r => r.resourceIdent).some(item => item === resourceIdent);

      this.setState({
        activeAccessRequests: activeAccessRequests,
        pendingAccessRequests: pendingAccessRequests,
        permissionGranted: permissionGranted
      });
    }).catch((error) => {
      console.log('An unexpected error occurred while loading active access requests: %s', error);
    });

  }

  loadBookmarks() {
    BookmarksService.serviceList().then((data) => {
      this.setState({
        bookmarks: data
      });
    }).catch((error) => {
      if (error) {
        // this.props.history.push("/logout");
        console.log(error);
      }
    });
  }

  loadBreadcrumbs(id) {
    ResouceService.serviceListParents(id)
      .then(
        (data) => {
          let breadcumb = [];
          const parents = (id, rawdata) => {
            rawdata.forEach(node => {
              if (node.ResourceGroupId === id) {
                breadcumb.push({ title: node.metadata.firstLevel.title, resourceGroup: node.ResourceGroupId });
                if (node.ResourceGroupParentId.split("-")[0] !== "0") {
                  return parents(node.ResourceGroupParentId, rawdata);
                }
                else {
                  return breadcumb;
                }
              }
            })
          }

          parents(data.ResourceGroupId, data.parents);
          breadcumb.reverse();
          breadcumb.push({ title: this.props.match.params.id, resourceGroup: '' });

          this.setState({
            breadcumbs: breadcumb
          });
        },
        (error) => {
          console.log(error)
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar el recurso',
            confirmButtonText: 'Regresar',
            action: () => { }
          })
        }
      )
  }

  loadTab(evt, item) {
    evt.preventDefault();
    this.setState({ display: item, changed: true });
  }

  recordIcon(record) {
    if (record.origin === "CatalogacionFuentesInternas" || record.origin === "CatalogacionFuentesExternas") {
      return "informe";
    }
    else if (record.origin === "Microdato") {
      return "microdato";
    }
    else if (record.origin === "ModuloCaptura" || record.origin === "ModuloCapturaCasosInformes") {
      let icon = "informe";
      switch (record.type) {
        case "Documento de etiquetado":
          icon = "etiquetado"
          break;

        case "Transcripción final":
        case "Transcripción preliminar":
          icon = "transcripcion"
          break;

        case "Ficha corta":
          icon = "fichacorta"
          break;

        case "Ficha larga":
          icon = "fichalarga"
          break;

        case "Audio de la entrevista":
          icon = "audio"
          break;

        default:
          icon = "informe";
          break;
      }
      return icon;
    }
    return "informe";
  }

  render() {
    const resourceIdent = this.props.match.params.id;
    const accessRequests = this.state.activeAccessRequests.concat(this.state.pendingAccessRequests);
    const accessRequestIdent = accessRequests.find(r => r.resourceIdent === resourceIdent)?.accessRequestIdent;

    // Filter records of certain type to give them a special display treatment
    const recordsFilter = r => !['Consentimiento informado', 'Documento de etiquetado'].includes(r.type) && !['attachedRightsAuthorization'].includes(r.type);
    const recordsFilterTags = r => r.type === 'Documento de etiquetado' && !['attachedRightsAuthorization'].includes(r.type);
    const recordsFilterAttachedRightsAuthorization = r => ['attachedRightsAuthorization'].includes(r.type);

    let {
      display,
    } = this.state;

    const {
      title,
      bookmarks,
      result,
      breadcumbs: breadcrumbs,
      latlon,
      permissionGranted,
      changed,
      roles,
      toast,
    } = this.state;

    let {
      records
    } = this.state;

    let transcription = records ? records.find(x => x.type === "Transcripción final" || x.type === "Transcripción parcial") : null;

    if (!this.props.user.roles.includes('escucha')) {
      records = transcription ? records.filter(x => x.type !== "Audio de la entrevista") : records;
    }

    if (!changed && ((this.props.app.display && this.props.app.display.type === "Audio de la entrevista" && transcription) || (this.props.app.display && this.props.app.display.type === "Transcripción final"))) {
      display = "cev-transcription";
    }
    else if (!changed && this.props.app.display && this.props.app.display.type === "Documento de etiquetado") {
      display = "cev-annotated-transcription";
    }

    let icon = 'documento';
    if (result) {
      switch (result.origin) {
        case 'ModuloCaptura':
          icon = 'entrevista'
          break;

        case 'VisualizacionesGeograficas':
          icon = 'vizgeo'
          break;

        case 'CatalogacionFuentesInternas':
          icon = 'omekai'
          break;

        case 'CatalogacionFuentesExternas':
          icon = 'omekae'
          break;

        case 'FileServer':
          icon = 'fileserver'
          break;

        case 'Microdato':
          icon = 'microdato'
          break;

        case 'ModuloCapturaCasosInformes':
          icon = 'casos_comision'
          break;

        case 'Visualizaciones':
          icon = 'viz'
          break;

        default:
          icon = 'documento'
          break;
      }
    }

    if (result && toast) {
      if (result.ident && ['CatalogacionFuentesExternas', 'FileServer', 'ModuloCapturaCasosInformes', 'CatalogacionFuentesInternas'].includes(result.origin)
        && toast) {
        Swal.fire({
          title: "Recuerde que puede unirse a la catalogación colaborativa - menú a la derecha",
          showCloseButton: true,
          showConfirmButton: false,
          customClass: {
            container: "swal-black",
          },
          toast: true,
          position: "center-end",
        });
      }
      this.setState({ toast: false });
    }

    return (
      <>
        {permissionGranted === false &&
          <>
            {accessRequestIdent &&
              <Redirect to={`/access-requests/${accessRequestIdent}`} />
            }
            {!accessRequestIdent &&
              <Redirect to={`/my-access-requests?resource=${resourceIdent}`} />
            }
          </>
        }
        {permissionGranted === true &&
          <div className="row content-view">
            {result && records &&
              <>
                {breadcrumbs &&
                  <div className="col-sm-12">
                    <BreadCrumbs items={breadcrumbs} />
                  </div>
                }
                <div className="col-sm-4">
                  <div className="view-tree-container height-70">
                    <div className="view-tree-title">
                      <span className="label">
                        {title && title.split(" ", 8).join(" ")}
                      </span>
                    </div>
                    <div className="view-tree-body">
                      <div className="title-content-detail">
                        <svg style={{ height: "7rem", width: "6rem" }}>
                          <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-" + icon)} className="flia-green"></use>
                        </svg>

                        <div className="item-content-detail">
                          <span className="label terciario-color">
                            {resourceIdent}
                          </span>
                          <span className="label terciario-color">
                            {title && title.split(" ", 8).join(" ")}
                            <span className="label success-color">
                              <VerifiedIcon></VerifiedIcon>
                            </span>
                          </span>
                          <span className="label terciario-color">
                            {result.type}
                          </span>
                          <Bookmarks extra={{ ident: resourceIdent, bookmarks }} />
                        </div>
                      </div>

                      <React.Fragment>
                        {result.origin === "ModuloCaptura" && transcription && transcription.content && !roles.includes('revision_bibliografica') &&
                          <DetailItem
                            loadTab={this.loadTab}
                            display={display}
                            item="transcription"
                            title="Transcripción de la entrevista"
                            icon2="transcripcion"
                            type="item"
                          />
                        }
                        {records && !roles.includes('revision_bibliografica') &&
                          <>
                            <DetailItem
                              loadTab={this.loadTab}
                              display={display}
                              title="Archivos"
                              type="title"
                            />
                            {records.filter(recordsFilter).map((record, idx) => (
                              <DetailItem
                                loadTab={this.loadTab}
                                display={display}
                                item={`files$${record.ident}`}
                                title={`${result.origin === "ModuloCaptura" ? record.type : record.metadata.firstLevel.title} `}
                                icon2={this.recordIcon(record)}
                                type="subitem"
                                key={idx}
                              />
                            )
                            )}
                          </>
                        }

                        {records && (roles.includes('catalogador') || roles.includes('catalogador_gestor')) && (
                          <>
                            {(records.filter(recordsFilterAttachedRightsAuthorization)).length > 0 &&
                              <DetailItem
                                loadTab={this.loadTab}
                                display={display}
                                title="Autorización de derechos de uso"
                                type="title"
                              />
                            }
                            {records.filter(recordsFilterAttachedRightsAuthorization).map((record, idx) => (
                              < DetailItem
                                loadTab={this.loadTab}
                                display={display}
                                item={`files$${record.ident}`}
                                title={`${result.origin === "ModuloCaptura" ? record.type : record.metadata.firstLevel.title} `}
                                icon2={this.recordIcon(record)}
                                type="subitem"
                                key={idx}
                              />
                            ))}
                          </>
                        )}

                        {records.filter(recordsFilterTags).map((record, idx) => (
                          <DetailItem
                            loadTab={this.loadTab}
                            display={display}
                            item="annotated-transcription"
                            title={result.origin === "ModuloCaptura" ? "Transcripción etiquetada" : "Documento de etiquetado"}
                            icon2="etiqueta"
                            type="subitem"
                            key={idx}
                          />
                        ))}
                        {latlon && latlon.length > 0 && !roles.includes('revision_bibliografica') &&
                          <DetailItem
                            loadTab={this.loadTab}
                            display={display}
                            item="maps"
                            title="Mapa"
                            icon2="localizacion"
                            type="item"
                          />
                        }
                        {result && result.origin === "VisualizacionesGeograficas" && !roles.includes('revision_bibliografica') &&
                          <DetailItem
                            loadTab={this.loadTab}
                            display={display}
                            item="visualizacion"
                            title="Visualización"
                            icon2="localizacion"
                            type="item"
                          />
                        }
                        {result && result.metadata &&
                          <DetailItem
                            loadTab={this.loadTab}
                            display={display}
                            item="metadata"
                            title="Metadata"
                            icon2="metadata"
                            type="item"
                          />
                        }
                      </React.Fragment>
                    </div>
                  </div>
                </div>
                <div className="col-sm-8">
                  <div className="view-tree-container height-70">
                    <div className="view-tree-title">
                      <span className="label">
                        {display}
                      </span>
                    </div>
                    <div className="view-tree-body">
                      <React.Fragment>
                        {result.origin === "ModuloCaptura" && transcription && transcription.content &&
                          <DetailContent showScrollBar={false} content="TestimonyTranscription" title="Transcripción de la entrevista" item="transcription" records={records} keyword={this.props.search} display={display} />
                        }
                        {records && display.split("$")[0] === 'cev-files' &&
                          <DetailContent showScrollBar={result.origin === "Microdato" ? true : false} content="RecordPreview" title={result.origin === "ModuloCaptura" ? records.find(x => x.ident === display.split("$")[1]).type : records.find(x => x.ident === display.split("$")[1]).metadata.firstLevel.title} item="files" records={records.find(x => x.ident === display.split("$")[1])} keyword={this.props.search} display={display.split("$")[0]} />
                        }
                        {display === 'cev-annotated-transcription' &&
                          <DetailContent
                            content="AnnotatedTranscription"
                            title={result.origin === "ModuloCaptura" ? "Transcripción etiquetada" : "Documento de etiquetado"}
                            item="annotated-transcription"
                            records={records}
                            display={display}
                            showScrollBar={false}
                            showTitle={true}
                          />
                        }
                        {latlon && display === 'cev-maps' &&
                          <DetailContent showScrollBar={true} content="SIMMap" title="Ubicación" item="maps" records={latlon} keyword={this.props.search} display={display} />
                        }
                        {result && result.origin === "VisualizacionesGeograficas" && display === 'cev-visualizacion' &&
                          <DetailContent showScrollBar={true} content="VisualizacionGeografica" title="MetaData" item="visualizacion" records={result} keyword={this.props.search} display={display} />
                        }
                        {result && result.metadata && display === 'cev-metadata' &&
                          <DetailContent showScrollBar={true} content="TestimonyTable" title="MetaData" item="metadata" records={result} keyword={this.props.search} display={display} />
                        }
                      </React.Fragment>
                    </div>
                  </div>
                </div>
              </>
            }
          </div>
        }
        {this.state.resourceGroup && this.state.result.ident && ['CatalogacionFuentesExternas', 'FileServer', 'ModuloCapturaCasosInformes', 'CatalogacionFuentesInternas'].includes(this.state.result.origin) && (
          <ul className="cev-sticky-toolbar">
            <li className="cev-sticky-toolbar__item" data-toggle="cev-tooltip" title="Catalogación colaborativa" data-placement="left" data-original-title="Layout Builder">
              <Link
                to={`/resources/resource/${this.state.resourceGroup ? this.state.resourceGroup : '0'}/${this.state.result.ident}`} >
                <span><i className="flaticon2-pen" /> <i className="flaticon2-file" /></span>
              </Link>
            </li>
          </ul>)}
      </>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user,
  app: store.app
});

export default connect(
  mapStateToProps,
  app.actions
)(DetailPage);
