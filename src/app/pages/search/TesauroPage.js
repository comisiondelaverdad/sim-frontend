import React, {Component} from "react";
import { Row, Col } from 'reactstrap';

import { Redirect } from "react-router-dom";

import TesauroSearch from '../../components/molecules/TesauroSearch'
import EntitySearch from '../../components/molecules/EntitySearch'

class Tesauro extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect:undefined
    };
  }

  search(q){
    this.setState({redirect:"/search?from=1&q="+q})
  }


  render() {


    return (
    <Row>
      <Col sm="6">
        <EntitySearch addQuery={(q)=>{this.search(q)}}/>
        {this.state.redirect &&
          <Redirect to={this.state.redirect} />
        }
      </Col>
      <Col sm="6">
        <TesauroSearch addQuery={(q)=>{this.search(q)}}/>
      </Col>
    </Row>
    );
  }

}


export default Tesauro;
