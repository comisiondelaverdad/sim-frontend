import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import * as ResourceGroupService from "../../services/ResourceGroupService";
import * as ResourcesService from "../../services/ResourcesService";
import TreeView from '../../components/molecules/TreeView';
import DetailContent from "../../components/molecules/DetailContent";
import Paginator from "../../components/molecules/Paginator";
import { Container, Row } from "react-bootstrap";
import * as AccessRequestService from "../../services/AccessRequestService";
import * as StatsService from "../../services/StatsService";
import * as BookmarksService from "../../services/BookmarksService";
import _ from "lodash";
import { PAGE_SIZE, TREE_DEPTH } from "../../config/const";
import BreadCrumbs from "../../components/molecules/Breadcrumbs";
import { Link } from 'react-router-dom';
import MetaData from "../../components/organisms/MetaData";
import { errorAlert } from "./../../services/utils"
import VizSelect from '../../components/organisms/VizSelect';
import CreateNewFolderIcon from '@mui/icons-material/CreateNewFolder';
import PostAddIcon from '@mui/icons-material/PostAdd';
import PreviewIcon from '@mui/icons-material/Preview';
import FolderIcon from '@mui/icons-material/Folder';
import EditIcon from '@mui/icons-material/Edit';
import UndoIcon from '@mui/icons-material/Undo';
import Swal from "sweetalert2";
class ResourceGroupPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      records: undefined,
      selected: undefined,
      resources: [],
      display: 'cev-resources',
      from: 1,
      views: [],
      bookmarks: [],
      activeRequests: [],
      resource: '',
      loading: false,
      collapse: false,
      rawData: [],
      breadcumbs: [],
      metadata: undefined,
      tree: undefined,
      actualPath: null,
      newSelected: { old: '', new: '' },
      showResults: false,
      back: false
    };
    this.loadResourceGroup = this.loadResourceGroup.bind(this);
    this.loadTab = this.loadTab.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.showMetada = this.showMetada.bind(this);
    this.showTree = this.showTree.bind(this);
    this.eventBreadCrumb = this.eventBreadCrumb.bind(this);
    this.redirect = this.redirect.bind(this);
    this.showMetadata = this.showMetadata.bind(this);

  }

  showMetadata(id) {
    ResourceGroupService.getMetadataRG(id)
      .then((data) => {
        Swal.fire({
          title: 'Metadata',
          width: 600,
          heightAuto: false,
          html: data.metadataHTML,
        });
      })
  }

  redirect(path) {
    this.props.history.push(`${path}`);
  }

  componentDidMount() {
    console.log(this.state)
    this.loadResourceGroup(this.props.match.params.id);
    this.loadBookmarks();
    this.loadActiveAccessRequests();
    this.props.pageTitle("Fondos");
    this.props.pageSubtitle("Explorar por fondos");
    this.props.showSearch(true);
    this.props.showSearchTags(false);
    this.props.fromComponent("resourceGroup");
  }

  componentWillUnmount() {
    this.props.showSearch(false);
    this.props.showSearchTags(false);
    this.props.fromComponent("");
  }

  eventBreadCrumb(data) {
    console.log(data);
    this.setState({ newSelected: { ...this.state.newSelected, ...{ new: data.item.resourceGroup }}, showResults: false  })
  }

  loadResourceGroup(initial = null) {
    ResourceGroupService.serviceListFirstLevel(initial)
      .then(
        (data) => {
          let tree = {
            "core": {
              "data": [
                {
                  "text": 'Fondos',
                  "state": { "opened": true },
                  "children": data,
                  "icon": "fa fa-folder"
                }
              ]
            }
          }
          let treeLazy = {
            "core": {
              "data": {
                "url": ResourceGroupService.url,
              },
            },
          }
          this.setState({ records: tree, tree: treeLazy, rawData: data });
        },
        (error) => {
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar la información',
            confirmButtonText: 'Aceptar',
            action: () => { }
          })
        }
      )
  }

  loadBreadcrumbs(data) {
    data.texts.shift();
    data.ids.shift();

    let texts = data.texts;
    let ids = data.ids;
    let breadcumbs = [];

    if (texts.length > 0 && ids.length > 0 && texts.length === ids.length) {
      breadcumbs = texts.map((item, i) => {
        return { title: item, resourceGroup: ids[i] };
      });
    }
    localStorage.setItem('breadcrumbs', JSON.stringify(breadcumbs));
    this.setState({ breadcumbs: breadcumbs})
  }


  handleChange(e, data) {
    e.preventDefault();
    console.log(this.state)

    if (data.node) {
      this.setState({ loading: true, breadcumbs: [] });
      // if(data.instance._model.data[data.selected[0]].children_d.length > 0){
      //   this.loadResources(data.instance._model.data[data.selected[0]].id);
      // }
      // else{
      //   this.loadResources(data.instance._model.data[data.selected[0]].id);
      // }
      this.loadResources(data.node.id);
      this.setState({
        selected: data,
        resource: data.node.original.text,
        metadata: data.node.original.metadata,
        resourceGroupId: data.selected[0],
        newSelected: { old: data.selected[0], new: '' },
        showResults: true
      });
      this.showMetada();
    }
    if (data.action === 'ready') {
      this.setState({ loading: true, breadcumbs: [] });
      // if(data.instance._model.data[data.selected[0]].children_d.length > 0){
      //   this.loadResources(data.instance._model.data[data.selected[0]].id);
      // }
      // else{
      //   this.loadResources(data.instance._model.data[data.selected[0]].id);
      // }
      this.loadResources(data.instance._model.data[data.selected[0]].id);
      this.setState({
        selected: data,
        resource: data.instance._model.data[data.selected[0]].original.text,
        metadata: data.instance._model.data[data.selected[0]].original.metadata,
        resourceGroupId: data.selected[0],
        newSelected: { old: data.selected[0], new: '' },
        showResults: true
      });
      this.showMetada();
    }
    if (data.path) {
      const path = data.path.ids.filter((a, i) => (i > 0)).reduce((a, b) => (a + '|' + b))
      console.log(path)

      this.props.history.push(`/resourcegroup/${path}`)
      this.loadBreadcrumbs(data.path);
    }

  }

  loadResources(parents, from = 1) {
    ResourcesService.serviceListByParents(parents, from)
      .then(
        (data) => {
          this.setState({ loading: false, resources: data, from: from }, () => { this.loadViews(data) });
        },
        (error) => {
          console.log(error)
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar el recurso',
            confirmButtonText: 'Regresar',
            action: () => { }
          })
        }
      )
  }

  loadTab(evt, item) {
    this.setState({ display: item });
  }

  pageChange(page) {
    let selected = this.state.selected;
    this.loadResources(selected.instance._model.data[selected.selected[0]].id, page);
    this.loadBookmarks();
    this.loadActiveAccessRequests();
  }

  showMetada() {
    ResourceGroupService.serviceDetail(this.state.selected.selected[0])
      .then(
        (data) => {
          this.setState({ metadata: data.metadata });
        },
        (error) => {

        }
      )
  }

  loadActiveAccessRequests() {
    AccessRequestService.getUserAccessRequests(this.props.user._id).then((data) => {
      this.setState({
        activeRequests: _.map(data, (r) => {
          return r.resource.ident;
        })
      });
    }).catch((error) => {
      console.log('An unexpected error occurred while loading active access requests: %s', error);
    });
  }

  loadViews(data) {
    let ids = _.map(data.docs, (x) => {
      return x.ident;
    });
    StatsService.resourceViews(ids).then((data) => {
      let views = {};
      for (let i = 0; i < data.resource_views.buckets.length; i++) {
        views[data.resource_views.buckets[i].key] = data.resource_views.buckets[i].doc_count
      }
      this.setState({
        views: views
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  loadBookmarks() {
    BookmarksService.serviceListByUser(this.props.user._id)
      .then(
        (data) => {
          this.setState({
            bookmarks: data
          });
        },
        (error) => {
          console.log(error)
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar el recurso',
            confirmButtonText: 'Regresar',
            action: () => { }
          })
        }
      )
  }

  showTree(path=`/resourcegroup/all`) {
    this.setState({ showResults: false, metadata: undefined, resources: [], selected: [] })
    this.props.history.push(path)
  }

  render() {
    let query = new URLSearchParams("?from=" + this.state.from);
    let pathname = "/resourcegroup";
    let numberFormatter = new Intl.NumberFormat("de-DE");
    let from = parseInt(this.state.from);
    let total = this.state.resources.total;

    let showing = !total ? "" : " - Viendo " +
      (from === 1 ?
        (total === 0 ? 0 : from)
        : from * PAGE_SIZE - PAGE_SIZE + 1) + " al " +
      (from * PAGE_SIZE < total ?
        numberFormatter.format(from * PAGE_SIZE)
        : numberFormatter.format(total)) +
      " de " + numberFormatter.format(this.state.resources.total) + " -";

    return (
      <>
        <VizSelect
          resource={this.state.selected}
        />
        <div id="container_fondos">
          {this.state.records &&
            <>
              {this.state.breadcumbs &&
                <BreadCrumbs items={this.state.breadcumbs} outputEvent={this.eventBreadCrumb} ></BreadCrumbs>
              }
              {!this.state.showResults &&
                <div className="cev-portlet cev-portlet--height-fluid">
                  <div className="cev-portlet__head">
                    <div className="cev-portlet__head-label">
                      <h3 className="cev-portlet__head-title">Lista de Fondos</h3>
                    </div>
                  </div>
                  <div className="cev-portlet__body cev-resource-group">
                    <div className="cev-widget cev-widget--user-profile-1">
                      <div className="cev-widget__body">
                        <div className="cev-widget__content">
                          <TreeView
                            treeData={this.state.tree}
                            newSelected={this.state.newSelected}
                            onChange={(e, data) => this.handleChange(e, data)}
                            depth={TREE_DEPTH}
                            resourcegroup={true}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              }
              <div>
                {this.state.loading &&
                  <div className="cev-loading">
                    Loading ...
                  </div>
                }

                {!this.state.loading && this.state.showResults &&
                  <>
                    <DetailContent
                      content="DetailResult"
                      title={this.state.resource ? this.state.resource + showing : 'Selecciona un Fondo'}
                      showTitle={false}
                      subtitle="Subttítulo"
                      item="resources"
                      records={this.state.resources.docs}
                      keyword=""
                      display={this.state.display}
                      actions={{
                        collapseAction: this.showMetada
                      }}
                      extra={{
                        bookmarks: this.state.bookmarks,
                        activeRequests: this.state.activeRequests,
                        views: this.state.views,
                        highlightMode: 1
                      }}
                      autoHeight={true}
                      showScrollBar={false}
                      resourceGroupId={this.state.resourceGroupId ? this.state.resourceGroupId : null}
                    />
                  </>
                }

                {this.state.resources && total &&
                  <Container fluid={true}>
                    <Row className="justify-content-center">
                      <Paginator
                        total={this.state.resources.total} query={query} pathname={pathname} mode="callback" callback={this.pageChange} />
                    </Row>
                  </Container>
                }
              </div>
            </>
          }
        </div>
        {!this.state.loading && this.state.showResults &&
        <div className="toolbar-cataloging">
          <button type="button" className="btn-action" title="Atraś" onClick={() => this.showTree()}>
            <UndoIcon></UndoIcon>
          </button>
          {(this.props.user.roles.includes('catalogador') || this.props.user.roles.includes('catalogador_gestor')) && this.state.resourceGroupId &&
            <>
              <button type="button" className="btn-action" title="Ver metadata" onClick={() => this.showMetadata(this.state.resourceGroupId)}>
                <PreviewIcon></PreviewIcon>
              </button>
              <button type="button" className="btn-action warning" title="Editar fondo"
                onClick={() => this.redirect(`/resourcegroupmanagment/edit/${this.state.resourceGroupId ? this.state.resourceGroupId : '0'}`)}>
                <EditIcon></EditIcon><FolderIcon></FolderIcon>
              </button>

              <button type="button" className="btn-action warning" title="Nuevo fondo"
                onClick={() => this.redirect(`/resourcegroupmanagment/new/${this.state.resourceGroupId ? this.state.resourceGroupId : '0'}`)}>
                <CreateNewFolderIcon></CreateNewFolderIcon>
              </button>
              <button type="button" className="btn-action" title="Nuevo recurso"
                onClick={() => this.redirect(`/resources/resource/${this.state.resourceGroupId ? this.state.resourceGroupId : '0'}/0`)}>
                <PostAddIcon></PostAddIcon>
              </button>
            </>
          }
        </div>
        }
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(ResourceGroupPage);
