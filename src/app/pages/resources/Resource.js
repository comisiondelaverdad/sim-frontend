import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { Form } from "react-bootstrap";
import FormFieldsBootstrap from '../../components/organisms/FormFieldsBootstrap';
import './../../styles/resource.scss';
import { serviceDetail } from '../../services/ResourceGroupService';
import { serviceResourceEditFiles, serviceResourceCreateFiles, getResource } from '../../services/ResourcesService';
import {
  findField,
  dateToAAAAMMDD,
  objectTransformation,
  submitAlert,
  objectTransformationInverse,
  resourcesValidation,
  getValueFromPath,
  is_string,
  is_array
} from '../../services/utils';
import { updateMetadata, clarMetadata } from '../../services/MetaService';
import { getData } from '../../services/RequestService';
import Swal from "sweetalert2";
import { toAbsoluteUrl } from "../../../theme/utils";

class Resource extends Component {
  formRef = React.createRef();
  constructor(props) {
    super(props);
    this.state = {
      firstLevel: {},
      formData: {},
      resourceData: {},
      resourceGroup: '',
      validated: false,
      edit: false,
      filesToDelete: [],
      roles: props.user.roles,
      origin: '',
      catalogacion_colaborativa: false,
      collaborativeForm: null,
      resourceForm: null,
      statment: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.outputFieldBootstrap = this.outputFieldBootstrap.bind(this);
    this.clearFile = this.clearFile.bind(this);
    this.goback = this.goback.bind(this);
  }
  goback() {
    this.props.history.goBack()
  }

  componentWillUnmount() {
    clarMetadata();
  }


  updateISBN_ISSN(formData) {
    let isbnField = findField(this.state.resourceForm, 'ISBN');
    let issnField = findField(this.state.resourceForm, 'ISSN');

    isbnField = { ...isbnField, ...{ required: !(formData.ISSN ? true : false || formData.ISBN ? true : false) } };
    issnField = { ...issnField, ...{ required: !(formData.ISSN ? true : false || formData.ISBN ? true : false) } };
    this.setState(
      {
        resourceForm: this.state.resourceForm.map((resource) => {
          if ('ISBN' === resource.id) {
            return isbnField
          } else if ('ISSN' === resource.id) {
            return issnField
          } else {
            return resource
          }
        })
      })
  }

  outputFieldBootstrap(event) {
    //console.log(event.formData);
    if (typeof event.attachedDocument !== 'undefined' ||
      typeof event.attachedRightsAuthorization !== 'undefined') {
      this.setState(event);
    }
    else {
      if (typeof event.formData.descriptionLevel !== 'undefined') {
        this.filterRuleFields(event.formData.descriptionLevel, this.state.resourceForm);
      }
      if (typeof event.formData.documentaryGroup !== 'undefined') {
        this.filterRuleFields(event.formData.documentaryGroup, this.state.resourceForm);
      } else if ((typeof event.formData.ISBN !== 'undefined') || (typeof event.formData.ISBN !== 'undefined')) {
        this.updateISBN_ISSN(event.formData);
      }
      const formData = { formData: { ...this.state.formData, ...event.formData } };
      this.setState(formData);
    }
  }

  filterRuleFields(field, form) {
    // console.log(form)
    const newField = (field ? field.indexOf('(') !== -1 : false) ?
      field.substring(
        field.indexOf("(") + 1,
        field.lastIndexOf(")")
      ) : field;
    const formBase = form
      .map((cf) => (cf.hasOwnProperty('ruleToShow') ?
        !cf.ruleToShow.includes(newField) ? { ...cf, ...{ hide: true } } : { ...cf, ...{ hide: false } } : cf));
    this.setState({ resourceForm: formBase });
  }

  mountForm(origin) {
    return new Promise((resolve, reject) => {
      Swal.fire({
        title: '¡Por favor espere!',
        html: 'Cargando formulario ...',
        allowOutsideClick: false,
        onBeforeOpen: () => {
          Swal.showLoading()
        }
      })
      getData(`/api/forms/byorigin/${origin}`)
        .then((form) => {
          Swal.close();
          resolve(form);
        })
        .catch((err) => {
          console.log(err);
          Swal.close();
          Swal.fire({
            title: `Error ${err.status}`,
            icon: 'error',
            text: err.error,
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonText: `Regresar`,
          }).then((result) => {
            this.goback();
          })
          reject(err);
        });

    })
  }

  componentDidMount() {
    getData('/api/forms/byname/COLABORATIVE_FORM')
      .then((collaborative) => {
        this.setState({ collaborativeForm: collaborative })
      });
    this.setState({ roles: this.props.user.roles });
    if (this.props.match.params.resourcegroup !== '0') {
      serviceDetail(this.props.match.params.resourcegroup)
        .then((data) => {
          if (data.origin) {
            this.setState({ origin: data.origin });
            console.log("imprimiendo el origin: " + data.origin);
            this.mountForm(data.origin)
              .then((formAny) => {
                const form = formAny.fields;
                if (this.props.match.params.id !== '0') {
                  this.setState({ edit: true })
                  this.props.pageTitle(`Editar Recurso`);
                  const creationDate = findField(form, 'creationDate');
                  creationDate.required = true;
                  getResource(this.props.match.params.id)
                    .then((resource) => {
                      updateMetadata(resource);
                      const user = this.props.user;
                      if ((user.accessLevel - 0) > (resource.metadata.firstLevel.accessLevel - 0)) {
                        Swal.fire({
                          title: `No tiene acceso `,
                          icon: 'warning',
                          text: `Usted no tiene nivel de acceso ${resource.metadata.firstLevel.accessLevel}, no puede editar este recurso`,
                          showDenyButton: true,
                          showCancelButton: false,
                          confirmButtonText: `Regresar`,
                        }).then((result) => {
                          this.goback();
                        })
                      } else {
                        this.filterRuleFields(resource.metadata.firstLevel.documentaryGroup, form);
                        form.forEach(field => {
                          if (field.type === 'select-multiple2-tags') {
                            console.log("field: ", field)
                            const valueTags = getValueFromPath(field.destiny, resource);
                            if (valueTags) {
                              const title = (is_array(valueTags)) ? valueTags : is_string(valueTags) ? valueTags.split(',') : []
                              field.options = title.map((t) => ({ name: t, value: t }))
                            } else {
                              field.options = []
                            }
                          }
                        });
                        const creationDate = findField(form, 'creationDate');
                        creationDate.required = true;
                        this.props.pageSubtitle(`${data.metadata.firstLevel.title ? data.metadata.firstLevel.title : ''}`);
                        const attachedDocumentField = findField(form, 'attachedDocument');
                        attachedDocumentField.required = false;
                        this.setState({ resourceData: resource, catalogacion_colaborativa: this.state.roles.includes('user') })
                        this.setState({
                          formData:
                          {
                            ...objectTransformationInverse(resource, this.buildStatment(form)),
                            ...{
                              creationDate: typeof resource.metadata.firstLevel.creationDate === 'undefined' || resource.metadata.firstLevel.creationDate === null ? null :
                                typeof resource.metadata.firstLevel.creationDate.start === 'undefined' ? dateToAAAAMMDD(resource.metadata.firstLevel.creationDate) :
                                  dateToAAAAMMDD(resource.metadata.firstLevel.creationDate.start)
                            },
                            ...{ records: resource.records },
                          }
                        })
                      }
                    })
                    .catch(err => {
                      Swal.close();
                      Swal.fire(
                        'Error al cargar la información.',
                        `ERROR: ${err}`,
                        'error'
                      )
                    })
                } else {
                  Swal.close();
                  this.filterRuleFields('BASE', form)
                  this.props.pageTitle(`Nuevo Recurso`);
                  const creationDate = findField(form, 'creationDate');
                  creationDate.required = true;
                  this.props.pageSubtitle(`${data.metadata.firstLevel.title ? data.metadata.firstLevel.title : ''}`);
                  const attachedDocumentField = findField(form, 'attachedDocument');
                  attachedDocumentField.required = true;
                  this.buildStatment(form)
                  this.setState({ edit: false, catalogacion_colaborativa: false })
                }
              })
          }
        })
    }
  }

  clearFile(fileToDelete) {
    Swal.fire({
      title: 'Se eliminará el archivo',
      text: fileToDelete.metadata.firstLevel.title,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar archivo'
    }).then((result) => {
      if (result.value) {
        let filesToDelete = [...this.state.filesToDelete, ...[fileToDelete._id]]
        this.setState({ filesToDelete: filesToDelete });
        if (filesToDelete.length === this.state.formData.records.length) {
          const attachedDocumentField = findField(this.state.resourceForm, 'attachedDocument');
          attachedDocumentField.required = true;
        }
      }
    })
  }

  async handleSubmit(event) {
    this.updateISBN_ISSN(this.state.formData);
    let dataSubmit = new FormData();
    let info = {}
    if (this.props.match.params.resourcegroup !== '0') {
      dataSubmit.append(`resourcegroup`, this.props.match.params.resourcegroup)
    }
    if (typeof this.state.attachedDocument !== 'undefined') {
      this.state.attachedDocument.map((file) => (dataSubmit.append(`${file.id}-${file.i}`, file.file)))
    }
    if (typeof this.state.attachedRightsAuthorization !== 'undefined') {
      this.state.attachedRightsAuthorization.map((file) => (dataSubmit.append(`${file.id}-${file.i}`, file.file)));
    }
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      this.setState({ validated: true });
    } else {
      info = objectTransformation(this.state.formData, this.state.statment);
      const validation = resourcesValidation(info.metadata);
      if (validation.error) {
        Swal.fire({
          icon: 'error',
          width: 600,
          heightAuto: false,
          html: validation.message,
        });
      } else {
        if (!this.state.edit) {
          dataSubmit.append('body', JSON.stringify(info));
          submitAlert({ option: 'create', type: 'recurso', fn: serviceResourceCreateFiles, data: dataSubmit, info: this.state.formData.title });
        } else {
          dataSubmit.append('filesToDelete', JSON.stringify(this.state.filesToDelete))
          dataSubmit.append('ident', JSON.stringify(this.state.resourceData.ident))
          dataSubmit.append('body', JSON.stringify(info))
          submitAlert({ option: 'update', type: 'recurso', fn: serviceResourceEditFiles, data: dataSubmit, info: this.state.formData.title });
        }
      }
    }
    event.preventDefault();
    event.stopPropagation();
  }

  buildStatment(form) {
    const statment = form
      .filter((field) => (!(['separator', 'file'].includes(field.type))))
      .map((field) => {
        return { defaultValue: field.defaultValue, destiny: field.destiny, origin: field.id, type: field.type }
      })
    this.setState({ statment: statment });
    return statment;
  }


  render() {
    const { validated, filesToDelete, formData, roles, origin, resourceForm, catalogacion_colaborativa, collaborativeForm } = this.state;
    return (
      <Form
        noValidate
        validated={validated}
        onSubmit={e => this.handleSubmit(e)}>
        <div className="view-tree-container height-80 p-20">
          <div className="view-tree-title">
            <div className="circle-icon">
              <svg className="cv-small">
                <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-documento")} className={"flia-azul"}></use>
              </svg>
            </div>
            <span className="label">Gestión de recursos </span>
            <span className="label">Origen: {origin ? origin : ''} </span>
          </div>
          <div className="view-tree-body">
            {(roles.includes('catalogador') || roles.includes('catalogador_gestor')) && (
              <div>
                <Form.Row>
                  <div className="files-container">
                    {formData.records && (
                      formData.records.map((fileDoc, index) => (
                        <div key={index} className={fileDoc.type === 'attachedRightsAuthorization' ? 'tag-files attachedRightsAuthorization' : filesToDelete.includes(fileDoc._id) ? 'tag-files tag-files-to-delete' : 'tag-files'}>
                          <span>{`${fileDoc.metadata.firstLevel.title} - (${Number.parseFloat(fileDoc.metadata.firstLevel.fileSize / 1024).toFixed(2)} KB)`}</span>
                          {!filesToDelete.includes(fileDoc._id) && (
                            <button type="button" className="clear-files-cev" onClick={() => (this.clearFile(fileDoc))}>
                              <i className="fas fa-times"></i>
                            </button>
                          )}
                        </div>
                      ))
                    )}
                  </div>
                </Form.Row>
                <Form.Row>
                  {!!resourceForm && !!formData && (
                    <FormFieldsBootstrap
                      formData={formData}
                      formConfig={resourceForm}
                      outputEvent={this.outputFieldBootstrap}
                    ></FormFieldsBootstrap>
                  )}
                </Form.Row>
              </div>
            )}
            {(roles.includes('user') && !roles.includes('catalogador') && !roles.includes('catalogador_gestor') && catalogacion_colaborativa &&
              <div>
                <Form.Row>
                  {!!resourceForm && !!collaborativeForm && (
                    <FormFieldsBootstrap
                      formData={formData}
                      formConfig={collaborativeForm}
                      outputEvent={this.outputFieldBootstrap}
                    ></FormFieldsBootstrap>
                  )}
                </Form.Row>
              </div>
            )}
          </div>

        </div>
        <div className="d-flex flex-row-reverse">
          {this.props.match.params.id === '0' && (
            <button className="btn btn-primary ml-2" type="submit" value="submit">Nuevo Recurso</button>
          )}
          {this.props.match.params.id !== '0' && (
            <button className="btn btn-primary ml-2" type="submit" value="submit">Actualizar Recurso</button>
          )}
          <button className="btn btn-secondary ml-2" id="return" onClick={() => this.goback()} type="reset" >Cancelar</button>
        </div>
      </Form>

    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(Resource);