import React, { Component } from "react";

import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { Form } from "react-bootstrap";
import FormFieldsBootstrap from '../../components/organisms/FormFieldsBootstrap';
import './../../styles/resource.scss';
import { serviceResourceMasive, serviceResourceEditFiles } from '../../services/ResourcesService';
import { updateResourceGroupMetadata } from '../../services/ResourceGroupService';
import { findField, sleep } from '../../services/utils';
import Swal from "sweetalert2";
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";

import { getData } from '../../services/RequestService';
import ReactExport from "react-export-excel";

import GetAppIcon from '@material-ui/icons/GetApp';
import UndoIcon from '@material-ui/icons/Undo';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

// compileData url error
// resourcesFound accessLevel, hashcode error
// mergeData accessLevel, hashcode error

const fieldsBase = [
    'ident',
    'title'
]

const collectionType = [
    { name: 'Recursos catalogación colaborativa (FAI)', value: 'RESOURCES_FAI' },
    { name: 'Recursos Fuentes de archivo externo FAE', value: 'RESOURCES_FAE' },
    { name: 'Fondos', value: 'RESOURCEGROUPS_FAE' },
    { name: 'Recursos Bases de Datos', value: 'RESOURCES_CKAN' },
    { name: 'Recursos de Objetos', value: 'RESOURCES_OBJ' }
]

const configFieldUploadExcel = [
    {
        id: 'attachedDocument',
        defaultValue: null,
        label: 'Documento Adjunto',
        type: 'file',
        placeholder: 'Adjunte el documento',
        col: 12,
        info: 'Cargue el documento o conjunto documental (por ejemplo, cuando se trate de una galería de imágenes). Verifique el nombre de los documentos esté de acuerdo con las indicaciones de la guía.',
        feedBack: 'Se ha cargado exitosamente!',
        required: true,
        multiple: false,
        extensionValidator: ['xlsx', 'xls']
    },
    {
        id: 'fieldBase',
        defaultValue: null,
        label: 'Campo base',
        type: 'select',
        col: 12,
        placeholder: 'Seleccione el campo base para realizar la busqueda',
        required: true,
        options: fieldsBase.map((data) => ({ name: data, value: data }))
    },
    {
        id: 'collectionType',
        defaultValue: null,
        label: 'Tipo de registro',
        type: 'select',
        col: 12,
        placeholder: 'Seleccione el tipo de registros',
        required: true,
        options: collectionType,
    },
];



class ResourceMasive extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: {},
            resourceData: {},
            statment: null,
            validated: false,
            roles: props.user.roles,
            compileData: [],
            resourcesFound: [],
            steep: 'Cargar  metadata',
            headerTable: [],
            statmentFAI: null,
            statmentFAE: null,
            statmentFondos: null,
            statmentOBJ: null,
            ds: [" "],
            headerTableErrors: [{ "title": "Identificador", "data": "ident", "type": "text" }, { "title": "Título", "data": "title", "type": "text" }, { "title": "Nombre de la lista", "data": "list", "type": "text" }, { "title": "Valores inválidos", "data": "value", "type": "text" }, { "title": "Observaciones", "data": "observacion", "type": "text" }],

        };
        this.props.pageTitle(`Actualización masiva de recursos`);
        this.props.pageSubtitle(``);
        this.outputFieldBootstrap = this.outputFieldBootstrap.bind(this);
        this.clearForm = this.clearForm.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    clearForm() {
        this.setState({ formData: {}, compileData: [], resourcesFound: [] })
    }

    buildStatment(form) {
        const statment = form
            .filter((field) => (!(['separator', 'file', 'simple-date'].includes(field.type))))
            .map((field) => {
                return { title: field.label, defaultValue: field.defaultValue, destiny: field.destiny, origin: field.id, type: field.type }
            })
        this.setState({ statment: statment });
        return statment;
    }

    componentDidMount() {
        getData('/api/forms/statment/RESOURCES_FAI')
            .then((statment) => {
                var ids = statment.map((field) => {
                    return field.origin;
                });
                this.setState({
                    statmentFAI: ids
                });
            })
        getData('/api/forms/statment/RESOURCES_FAE')
            .then((statment) => {
                var ids = statment.map((field) => {
                    return field.origin;
                });
                this.setState({
                    statmentFAE: ids
                });
            })
        getData('/api/forms/statment/RESOURCES_OBJ')
            .then((statment) => {
                var ids = statment.map((field) => {
                    return field.origin;
                });
                this.setState({
                    statmentOBJ: ids
                });
            })
        getData('/api/forms/statment/RESOURCEGROUPS_FAE')
            .then((statment) => {
                var ids = statment.map((field) => {
                    return field.origin;
                });
                this.setState({
                    statmentFondos: ids
                });
            })
    }

    genHeader(statmentName) {
        getData('/api/forms/statment/' + statmentName)
            .then((statment) => {
                this.setState({
                    headerTable: [
                        ...[
                            { title: 'Abrir', data: 'accessLevel', type: 'action' }],
                        ...statment.map((h) => {
                            let type = 'text';
                            if (['select-multiple2', 'select-multiple2-tags', 'author'].includes(h.type)) {
                                type = 'list';
                            } else if (['range-date-year', 'simple-date'].includes(h.type)) {
                                type = 'range-date';
                            }
                            return {
                                title: h.label,
                                data: h.origin,
                                type: type
                            }
                        })
                    ]
                })

            })

    }

    goback() {
        this.props.history.goBack()
    }

    outputLocation(event) {
        const formData = { formData: { ...this.state.formData, ...event } };
        this.setState(formData);
    }

    outputFieldBootstrap(event) {
        if (typeof event.attachedDocument !== 'undefined' || typeof event.attachedRightsAuthorization !== 'undefined') {
            this.setState(event);
        } else {
            const formData = { formData: { ...this.state.formData, ...event.formData } };
            this.setState(formData);
        }
    }

    clearFile(fileToDelete) {
        Swal.fire({
            title: 'Se eliminará el archivo',
            text: fileToDelete.metadata.firstLevel.title,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Eliminar archivo'
        }).then((result) => {
            if (result.value) {
                let filesToDelete = [...this.state.filesToDelete, ...[fileToDelete._id]]
                this.setState({ filesToDelete: filesToDelete });
                if (filesToDelete.length === this.state.formData.records.length) {
                    const attachedDocumentField = findField(configFieldUploadExcel, 'attachedDocument');
                    attachedDocumentField.required = true;
                }
            }
        })
    }

    async updateMetadata() {
        const data = this.state.updateData;
        Swal.fire({
            title: `Se actualizará la metadata de  ${data.length} recursos`,
            text: this.state.formData.title,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: `Actualizar ${data.length} recursos`,
        }).then(async (result) => {
            if (result.value) {
                Swal.fire({
                    title: 'Actualizando campos',
                    html: `<b></b> de ${data.length} registros actualizados`,
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    }
                });
                for (let i = 0; i < data.length; i++) {
                    let dataSubmit = new FormData();
                    dataSubmit.append('filesToDelete', JSON.stringify([]))
                    dataSubmit.append('ident', JSON.stringify(data[i].ident))
                    dataSubmit.append('body', JSON.stringify(data[i]))

                    const content = Swal.getContent();
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = (i + 1);
                        }
                    }

                    const result = (this.state.formData.collectionType === 'RESOURCEGROUPS_FAE') ?
                        await updateResourceGroupMetadata(data[i]) : await serviceResourceEditFiles(dataSubmit)

                    if ((i + 1) === this.state.updateData.length) {
                        Swal.fire({
                            title: 'Actualización correcta',
                            text: `Se editaron correctamente ${this.state.updateData.length} registros`,
                            icon: 'success'
                        }).then(() => {
                            this.clearForm();
                        })
                    }
                }
            }
        });


    }

    async handleSubmit(event) {
        //console.log(event);
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            this.setState({ validated: true });
        } else {
            Swal.fire({
                title: '¡Por favor espere!',
                html: 'Interpretando plantilla ...',
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
            });
            let dataSubmit = new FormData();
            if (typeof this.state.attachedDocument !== 'undefined') {
                this.genHeader(this.state.formData.collectionType ? this.state.formData.collectionType : 'RESOURCES_FAE')
                const bodyFields = JSON.stringify({ fieldBase: this.state.formData.fieldBase, collectionType: this.state.formData.collectionType })
                dataSubmit.append('body', bodyFields);
                this.state.attachedDocument.map((file) => {
                    return dataSubmit.append(`${file.id}-${file.i}`, file.file);
                })
                serviceResourceMasive(dataSubmit)
                    .then(async (data) => {
                        this.props.pageSubtitle(`Plantilla: ${data.compileData.length}, Encontrados: ${data.mergeData.length}`);
                        console.log(data);
                        this.setState(data);
                        await sleep(1500);
                        Swal.close();

                    })
                    .catch(err => {
                        Swal.close();
                        Swal.fire(
                            'Error al interpretar plantilla',
                            `ERROR: ${err}`,
                            'error'
                        )
                    })

            }

        }
        event.preventDefault();
        event.stopPropagation();
    }

    render() {
        const { validated, filesToDelete, formData, roles,
            compileData, resourcesFound, mergeData, headerTable, updateData, failedValueLists } = this.state;
        const { attachedDocument } = formData
        let dataForm1 = {
            attachedDocument
        };
        return (
            <>
                {roles.includes('catalogador_gestor') && compileData.length === 0 && this.state.statmentFAE &&
                    this.state.statmentFAI && this.state.statmentFondos && this.state.statmentOBJ &&
                    <div className="card-cev height-70">
                        <div className="card-cev-title"> <span className="label">Cargar metadata de recursos de forma masiva</span> </div>
                        <div className="card-cev-body">
                            <Form
                                noValidate
                                validated={validated}
                                onSubmit={e => this.handleSubmit(e)}>
                                <Form.Row>
                                    <span className="label">  En esta sección descargue la plantilla:</span>
                                    <div className="container-templates">
                                        <ExcelFile
                                            element={
                                                <button download className="btn-small ml-2 mt-2 btn btn-primary" >
                                                    <GetAppIcon /><span>Fuentes de archivo externas</span>
                                                </button>
                                            }
                                            filename="Plantilla FAE"
                                        >
                                            <ExcelSheet data={this.state.ds} name="Hoja 1">
                                                {this.state.statmentFAE.map(element => <ExcelColumn label={element} value="" />)}
                                            </ExcelSheet>
                                        </ExcelFile>

                                        <ExcelFile
                                            element={<button download className="btn-small ml-2 mt-2 btn btn-primary" >
                                                <GetAppIcon />
                                                <span>Catalogación Colaborativa</span>
                                            </button>}
                                            filename="Plantilla FAI"
                                        >
                                            <ExcelSheet data={this.state.ds} name="Hoja 1">
                                                {this.state.statmentFAI.map(element => <ExcelColumn label={element} value="" />)}
                                            </ExcelSheet>
                                        </ExcelFile>

                                        <ExcelFile
                                            element={<button download className="btn-small ml-2 mt-2 btn btn-primary" >
                                                <GetAppIcon />
                                                <span>Fondos</span>
                                            </button>}
                                            filename="Plantilla Fondos"
                                        >
                                            <ExcelSheet data={this.state.ds} name="Hoja 1">
                                                {this.state.statmentFondos.map(element => <ExcelColumn label={element} value="" />)}
                                            </ExcelSheet>
                                        </ExcelFile>

                                        <ExcelFile
                                            element={
                                                <button download className="btn-small ml-2 mt-2 btn btn-primary" >
                                                    <GetAppIcon />
                                                    <span>Objetos simbólicos</span>
                                                </button>}
                                            filename="Plantilla objetos simbólicos"
                                        >
                                            <ExcelSheet data={this.state.ds} name="Hoja 1">
                                                {this.state.statmentOBJ.map(element => <ExcelColumn label={element} value="" />)}
                                            </ExcelSheet>
                                        </ExcelFile>
                                    </div>

                                </Form.Row >

                                <Form.Row>
                                    <FormFieldsBootstrap
                                        formData={dataForm1}
                                        formConfig={configFieldUploadExcel}
                                        outputEvent={this.outputFieldBootstrap}
                                    ></FormFieldsBootstrap>
                                </Form.Row>
                                <button className="btn btn-primary ml-2" type="submit" value="submit">{this.state.steep}</button>
                                <button className="btn btn-secondary ml-2" id="return" onClick={() => this.goback()} type="reset" >Cancelar</button>
                            </Form >
                        </div >

                    </div >
                }
                {
                    roles.includes('catalogador_gestor') && compileData.length > 0 && resourcesFound.length > 0 &&
                    <>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="card-cev">
                                    <div className="card-cev-title">
                                        <button className="btn btn-outline-primary" id="return" type="reset"
                                            onClick={() => (this.clearForm())}>
                                            <UndoIcon />Volver a cargar registros
                                        </button>
                                        <span className="label">Datos cargados a partir de la plantilla</span> </div>
                                    <div className="card-cev-body">
                                        <SimpleDataTable
                                            id='compileData'
                                            columns={headerTable}
                                            data={compileData}
                                        ></SimpleDataTable>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="card-cev">
                                    <div className="card-cev-title">
                                        <button className="btn btn-outline-primary" id="return" type="reset"
                                            onClick={() => (this.clearForm())}>
                                            <UndoIcon />Volver a cargar registros
                                        </button>
                                        <span className="label">Datos existentes en el sistema</span> </div>
                                    <div className="card-cev-body">
                                        <SimpleDataTable
                                            id='resourcesFound'
                                            columns={headerTable}
                                            data={resourcesFound}
                                        ></SimpleDataTable>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br /><br />
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="card-cev">
                                    <div className="card-cev-title">
                                        <button className="btn btn-outline-primary" id="return" type="reset"
                                            onClick={() => (this.clearForm())}>
                                            <UndoIcon />Volver a cargar registros
                                        </button>
                                        <span className="label">Errores encontrados</span> </div>
                                    <div className="card-cev-body">
                                        <SimpleDataTable
                                            id='failedData'
                                            columns={this.state.headerTableErrors}
                                            data={failedValueLists}
                                        ></SimpleDataTable>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br /><br />
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="card-cev">
                                    <div className="card-cev-title">
                                        <button className="btn btn-outline-primary" id="return" type="reset"
                                            onClick={() => (this.clearForm())}>
                                            <UndoIcon />Volver a cargar registros
                                        </button>
                                        <span className="label">Datos unificados</span>
                                    </div>
                                    <div className="card-cev-body">
                                        <SimpleDataTable
                                            id='mergeData'
                                            columns={headerTable}
                                            data={mergeData}
                                        ></SimpleDataTable>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br /><br />
                        <div className="row" align="center">
                            <button className="btn btn-primary ml-2" id="Actualizar" onClick={() => this.updateMetadata()} >Actualizar recursos de forma masiva</button>
                            <button className="btn btn-secondary ml-2" onClick={() => this.clearForm()} >Cancelar</button>
                        </div>
                    </>
                }
            </>);

    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(ResourceMasive);

