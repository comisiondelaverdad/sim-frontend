import React, { Component } from "react";
import {connect} from "react-redux";
import DetailItem from "../../components/molecules/DetailItem";
import DetailContent from "../../components/molecules/DetailContent";
import * as app from "../../store/ducks/app.duck";
import * as BookmarksService from "../../services/BookmarksService";
import PerfectScrollbar from "react-perfect-scrollbar";
import { errorAlert } from "./../../services/utils"

class BookmarksPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bookmarks: [],
      bookmarksTags: [],
      labels: [],
      display: 'cev-todos'
    };
    this.loadTab = this.loadTab.bind(this);
    this.loadBookmarks = this.loadBookmarks.bind(this);
    this.deleteLabel = this.deleteLabel.bind(this);
    this.updateLabel = this.updateLabel.bind(this);
  }

  componentDidMount() {
    this.loadBookmarks();
    this.props.showSearch(true);
    this.props.showSearchTags(false);
    this.props.fromComponent("bookmarks");
  }

  componentWillUnmount(){
    this.props.showSearch(false);
    this.props.showSearchTags(false);
    this.props.fromComponent("");
  }

  loadBookmarks(display='cev-todos'){
    BookmarksService.serviceList()
      .then(
        (data) => {
          this.setState({
            bookmarks: data.filter(x => x.user === this.props.user),
            bookmarksTags: display === 'cev-todos' ? data.filter(x => x.user === this.props.user) : data.filter(x => x.user === this.props.user).filter(x => x.labels.map(v => v.toLowerCase()).indexOf(display.split("-").pop().toLowerCase()) !== -1 ),
            display: display
          });
          this.loadLabels();
        },
        (error) => {
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar la información',
            confirmButtonText: 'Aceptar',
            action: ()=> {this.props.history.push('/dashboard')}
          })
        }
      )
  }

  deleteBookmark(bookmark,label,total){
    if(bookmark.labels.length > 1){
      bookmark.labels = bookmark.labels.filter(x=> x.toLowerCase() !== label.toLowerCase());
      BookmarksService.serviceUpdate(bookmark._id,bookmark).then(
        (data) => {
          this.loadBookmarks(total > 1 ? "cev-"+label.toLowerCase() : "cev-todos");
        },
        (error) => {
          console.log("Error",error);
        }
      )
    }
    else{
      BookmarksService.serviceDelete(bookmark._id)
      .then(
        (data) => {
          this.loadBookmarks(total > 1 ? "cev-"+label.toLowerCase() : "cev-todos");
        },
        (error) => {
          console.log("Error",error);
        }
      )
    }
  }

  loadLabels(){
    BookmarksService.serviceLabelsByUser(this.props.user)
      .then(
        (data) => {
          this.setState({labels:data});
          this.props.pageTitle("Bookmarks");
          this.props.pageSubtitle(this.state.bookmarks.length+" en "+this.state.labels.length+" etiquetas");
        },
        (error) => {
          console.log(error)
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar el recurso',
            confirmButtonText: 'Regresar',
            action: ()=> {}
          })
        }
      )
  }

  updateLabel(userid,label){
    let labelUpdate = {
      "oldLabel" : label.oldLabel,
      "newLabel" : label.newLabel
    }
    
    BookmarksService.serviceUpdateLabels(userid,labelUpdate)
      .then(
        (data) => {
          this.loadBookmarks("cev-"+label.newLabel.toLowerCase());
        },
        (error) => {
          console.log("Error",error);
        }
      )
  }

  deleteLabel(label,bookmarks){
    bookmarks.map((bookmark, key) => {
      this.deleteBookmark(bookmark,label,bookmarks.length - key);
      return bookmark;
    })
  }

  loadTab(evt, item){
    this.setState({display: item});
    if(item === "cev-todos"){
      this.setState({bookmarksTags: this.state.bookmarks});
    }
    else{
      this.setState({bookmarksTags: this.state.bookmarks.filter(x => x.labels.map(v => v.toLowerCase()).indexOf(item.split("-").pop().toLowerCase()) !== -1 ) })
    }
  }

  render() {
    return (
      <>
        <div className="cev-container  cev-container--fluid  cev-grid__item cev-grid__item--fluid">
          {this.state.bookmarks && this.state.bookmarks.length > 0 &&
            <div className="cev-grid cev-grid--desktop cev-grid--ver cev-grid--ver-desktop cev-app">
              <div className="cev-grid__item cev-app__toggle cev-app__aside" id="cev_user_profile_aside">
                <div className="cev-portlet cev-portlet--height-fluid">
                  <div className="cev-portlet__head">
                    <div className="cev-portlet__head-label">
                      <h3 className="cev-portlet__head-title">ETIQUETAS</h3>
                    </div>
                    <div className="cev-portlet__head-toolbar">  
                      <span className="cev-badge cev-badge--primary cev-badge--md cev-badge--rounded">
                        {this.state.labels.length}
                      </span>
                    </div>
                  </div>
                  <div className="cev-portlet__body cev-content">
                    <PerfectScrollbar
                    className="cev-scroll"
                    options={{
                      wheelSpeed: 2,
                      wheelPropagation: true
                    }}
                    style={{ height: "calc(100vh - 230px)"}}
                    data-scroll="true"
                    >
                      <div className="cev-widget cev-widget--user-profile-1">
                        <div className="cev-widget__body">
                          <div className="cev-widget__items">
                              <React.Fragment>
                                <DetailItem type="item" loadTab={this.loadTab} display={this.state.display} item="todos" title="Todos" badge={this.state.bookmarks.length} icon="flaticon2-tag" showDelete={this.state.display !== "cev-todos"} />
                                {this.state.labels && this.state.labels.length > 0 && this.state.labels.map( (label,idx) => (
                                  <DetailItem 
                                    loadTab={this.loadTab} 
                                    display={this.state.display} 
                                    item={label} 
                                    title={label.toUpperCase()} 
                                    badge={this.state.bookmarks.filter(x => x.labels.map(v => v.toLowerCase()).indexOf(label.toLowerCase()) !== -1 ).length} 
                                    icon="flaticon2-tag" 
                                    showDelete={this.state.display !== "cev-todos"} 
                                    key={idx}
                                    type="item"
                                  />
                                ))}
                              </React.Fragment>
                          </div>
                        </div>
                      </div>
                    </PerfectScrollbar>
                  </div>
                </div>
              </div>
              <div className="cev-grid__item cev-grid__item--fluid cev-app__content cev-labels">
                <DetailContent 
                  content="Bookmarks"
                  editTitle="LabelsMenu"
                  title={this.state.display.split("-").pop().toUpperCase()} 
                  item={this.state.display.split("-").pop()} 
                  records={this.state.bookmarksTags} 
                  keyword={this.state.display.split("-").pop()}
                  display={this.state.display}
                  showEditTitle={this.state.display !== "cev-todos"}
                  showDeleteTitle={this.state.display !== "cev-todos"}
                  showDelete={this.state.display !== "cev-todos"} 
                  showTitle={true}
                  actions={{
                    deleteBookmark : this.deleteBookmark,
                    loadBookmarks : this.loadBookmarks,
                    deleteLabel : this.deleteLabel,
                    updateLabel : this.updateLabel
                  }}
                  showScrollBar={true}
                  />
              </div>
            </div>
          }
          {this.state.bookmarks && this.state.bookmarks.length ===0 &&
            <div className="cev-portlet">
              <div className="cev-portlet__head">
                <div className="cev-portlet__head-label">
                  <h3 className="cev-portlet__head-title">No encontrado</h3>
                </div>
              </div>
              <div className="cev-portlet__body">
                <div className="cev-section">
                  <div className="cev-section__content text-center">
                    <h2 className="badge-danger">No hay bookmarks guardados</h2>
                  </div>
                </div>
              </div>
            </div>
          }
        </div>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user._id
});

export default connect(
  mapStateToProps,
  app.actions
)(BookmarksPage);