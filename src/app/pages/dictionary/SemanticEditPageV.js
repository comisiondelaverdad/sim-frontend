import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
//import { toAbsoluteUrl } from "../../../theme/utils";
import AdminDictionaryMenu from "../../components/organisms/AdminDictionaryMenu";
import * as app from "../../store/ducks/app.duck";
import * as SemanticService from "../../services/SemanticService";
//import moment from "moment";
import Select from 'react-select'
import Form from "react-jsonschema-form-bs4";
import * as TermService from "../../services/TermService";
import { Button, Col, Row, Container } from "react-bootstrap";

const uiSchema = {
  definition: {
    "ui:widget": "textarea",
  },
  "ui:widget": "checkboxes",
  "ui:options": {
    inline: true
  }
};

const alinear_texto = {
  wordWrap:'break-word'  
};

class SemanticEditPageV extends Component {
  constructor(props) {
    super(props);
    this.state = {
      semanctic: [],
      schema: {},
      terms: [],
      valterms: [],
      formterms: [],
      formsubmit: true,
      placeholderterms: "",
    };
  }

  componentDidMount() {
    SemanticService.terms().then((data1) => {
      let schema = {
        type: "object",
        required: ["name", "definition"],
        properties: {
          name: { type: "string", title: "Nombre del Campo Semántico" },
          definition: { type: "string", title: "Definición" },
          active: { type: "boolean", title: "Activo?", default: true },
          img: {
            "type": "string",
            "format": "data-url",
            "title": "Imagen"
          },
        },
      };

      this.setState({ schema: schema });

      SemanticService.get(this.props.match.params.id).then((data) => {

        this.setState({ semanctic: data });
        this.setState({placeholderterms: data.terms.map((item,index) => <li><a href={"/dictionary/termedit/" + item._id}>{item.name}</a></li>)});          

      })
    });

  }

  onSubmit(formData, e) {
    if (this.state.formsubmit) {
      this.setState({ formsubmit: false });
      formData.status = formData.status === "Activo";
      formData.terms = this.state.formterms;
      SemanticService.update(this.props.match.params.id, formData).then((data) => {
        alert("Se editó con éxito el campo semántico");
        this.setState({ formsubmit: true });
        this.props.history.push("/dictionary/semantic");
      })
    }



  }

  onCancel() {
    this.props.history.push("/dictionary/semanticedit/" + this.props.match.params.id);
  }

  onPreliminar() {
    this.props.history.push("/dictionary/semanticeditv/" + this.props.match.params.id);
  }


  termChange(selectedOption) {
    this.state.formterms = [];
    if (selectedOption !== null) {
      selectedOption.forEach((t) => {
        this.state.formterms.push(t.value);
      });
      console.log(this.state.formterms);
    }

    //this.setState({ level: selectedOption, locationsValue: null, selected: undefined })
  }

  render() {

    /*
    const options = [
      { value: '60dced40ee377e2a60a12a8f', label: 'Termino 1' },
      { value: '60df95b856fff56bd0bee7fb', label: 'Termino 2' },
      { value: '60e251769ac10d21c07017df', label: 'Termino 3' },
    ];

    const defaultoptions = [
      { value: '60df95b856fff56bd0bee7fb', label: 'Termino 2' },
    ];

    console.log("CONSTANTES");
    console.log(options);
    console.log(defaultoptions);

    console.log("METODO");
    console.log(this.state.terms);
    console.log(this.state.valterms);
    */

    return (
      <>
        <UserAccessValidator rol="admin_diccionario" />
        <div className="cev-container  cev-container--fluid  cev-grid__item cev-grid__item--fluid">
          <>
            <div className="cev-grid cev-grid--desktop cev-grid--ver cev-grid--ver-desktop cev-app">
              <AdminDictionaryMenu />
              <div className="cev-grid__item cev-grid__item--fluid cev-app__content">
                <React.Fragment>
                  <div className="col-xl-12 cev-portlet cev-portlet--height-fluid">
                    <div className="cev-portlet__head">
                      <div className="cev-portlet__head-label">
                        Vista preliminar
                      </div>
                    </div>
                    <div className="cev-portlet__body table-responsive">
                      <Container>
                        <Row className='mb-4'>
                          <Col><b>Nombre del Campo Semántico:</b></Col>
                          <Col>{this.state.semanctic.name}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Definición:</b></Col>
                          <Col style={alinear_texto}><div dangerouslySetInnerHTML={{ __html: this.state.semanctic.definition }} /></Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Imagen:</b></Col>
                          <Col><img src={this.state.semanctic.img} alt="Imagen" width="200px"/>​</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Términos:</b></Col>
                          <Col> 
                          <ul>
                            {this.state.placeholderterms}
                          </ul>                           
                          </Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col>
                            <Button className='mr-4' variant="secondary" onClick={() => {
                              this.onCancel();
                            }}>Volver</Button>
                          </Col>
                        </Row>
                      </Container>
                    </div>
                  </div>
                </React.Fragment>
              </div>
            </div>
          </>
        </div>
      </>
    );
  }
}

const mapStateToProps = (store) => ({
  search: store.app.keyword,
  user: store.auth.user,
});

export default connect(mapStateToProps, app.actions)(SemanticEditPageV);
