import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
//import { toAbsoluteUrl } from "../../../theme/utils";
import AdminDictionaryMenu from "../../components/organisms/AdminDictionaryMenu";
import AlignDictionary from "../../components/organisms/AlignDictionary";
import TermsRecordsModal from "../../components/organisms/TermsRecordsModal";
import * as app from "../../store/ducks/app.duck";
import * as TermService from "../../services/TermService";
import * as SemanticService from "../../services/SemanticService";
import * as ArcgisService from "../../services/arcgisService";
//import moment from "moment";
import Select from "react-select";
import AsyncSelect from "react-select/async";
import Form2 from "react-jsonschema-form-bs4";
import { Button, Modal, Row, Container, Col, Form } from "react-bootstrap";
import ReactPlayer from "react-player";


const alinear_texto = {
  wordWrap: "break-word",
};

class TermEditPageV extends Component {
  constructor(props) {
    super(props);
    this.state = {
      semanctic: [],
      schema: {},
      terms: [],
      resources: [],
      semanctics: [],
      semanctics2: [],
      departamens: [],
      municipios: [],
      valterms: [],
      formterms: [],
      valresource: null,
      placeholderresource: "Seleccionar Entrevista",
      placeholdersemantic: "Seleccionar Campo Semántico",
      placeholderdepartamento: "Seleccionar Departamento",
      placeholdermunicipio: "Seleccionar Municipio",
      placeholderterms: "",
      placeholderPais: "",
      disabledbuttonaudios: true,
      formresource: null,
      formsemactic: null,
      formsubmit: true,
      show: false,
      showAlinear: false,
      audios: [],
      audioreproducir: "",
      record: null,
      segundoactual: 0,
      segundoinicio: null,
      segundofinal: null,
      listTerms: [],
      openModal: [],
      openModalAlinear: [],
      closeModal: [],
      closeModalAlinear: [],
      changeAudio: [],
      cargarAudio: [],
      createObjectURL: "",
      audio: null,
      name_dep: [],
    };
  }

  componentDidMount() {
    //Cargo los departamentos
    ArcgisService.colAdministrativeDivision().then((dep) => {
      dep.forEach((d) => {
        this.state.departamens.push(d);
      });
    });

    let schema = {
      type: "object",
      required: ["name", "definition"],
      properties: {
        name: { type: "string", title: "Nombre del término" },
        equivalent: { type: "string", title: "Término equivalente" },
        definition: { type: "string", title: "Definición" },
        source_definition: { type: "string", title: "Fuente de la definición" },
        context: { type: "string", title: "Contexto" },
        source_context: { type: "string", title: "Fuente del contexto" },
        interview: { type: "string", title: "Entrevista N°" },
        interview_date: {
          type: "string",
          title: "Fecha de la entrevista",
          format: "date",
        },
        //interview_dep: { type: "string", title: "Lugar de la entrevista - Departamento", enum: this.state.name_dep},
        //interview_mun: { type: "string", title: "Lugar de la entrevista - Municipio" },
        //year_facts: { type: "integer", title: "Año de ocurridos los hechos" },
        etnia: {
          type: "string",
          title: "Pertenencia étnica",
          enum: [
            "Mestizo/a",
            "Afrocolombiano/a",
            "Negro/a",
            "Raizal",
            "Palenquera/o",
            "Rrom",
            "Indígena",
          ],
        },
        sexo: {
          type: "string",
          title: " Sexo (asignado al nacer) de la persona entrevistada",
          enum: ["Femenino", "Masculino", "Intersexual"],
        },
        sexual_orientation: {
          type: "string",
          title:
            "Orientación sexual de la persona entrevistada (se siente atraído por):",
          enum: [
            "Asexual",
            "Bisexual",
            "Heterosexual",
            "Homosexual",
            "Pansexual",
          ],
        },
        others: { type: "string", title: "Otros" },
        age: { type: "integer", title: "Edad" },
        fragment: { type: "string", title: "Fragmento de la entrevista" },
        active: { type: "boolean", title: "Activo?", default: true },
        img: {
          type: "string",
          format: "data-url",
          title: "Imagen",
        },
      },
    };

    //Set del esquema general del formulario
    this.setState({ schema: schema });

    //Creo el select multiple de Términos
    TermService.terms().then((data1) => {
      data1.forEach((c) => {
        this.state.terms.push({ value: c._id, label: c.name });
      });
    });

    //Cargo el termino en el formulario
    TermService.get(this.props.match.params.id).then((data) => {
      if (data.interview_location) this.getLocation(data.interview_location);
      else {
        if (typeof data.interview_dep !== "undefined") {
          this.setState({ placeholderdepartamento: data.interview_dep });
        }

        if (typeof data.interview_mun !== "undefined") {
          this.setState({ placeholdermunicipio: data.interview_mun });
        }
      }

      if (Object.keys(data.terms).length > 0) {
        this.setState({ placeholderterms: "" });
      }

      //Cargo el select multiple de Términos, permite mostrar los Términos en el select
      data.terms.forEach((t) => {
        this.state.valterms.push({ value: t._id, label: t.name });
        this.state.formterms.push(t._id);
      });

      this.setState({
        placeholderterms: data.terms.map((item, index) => (
          <li>
            <a href={"/dictionary/termedit/" + item._id}>{item.name}</a>
          </li>
        )),
      });

      //Set del objeto termino y los tiempos
      this.setState({ semanctic: data });
      this.setState({ segundoinicio: data.segundoinicio });
      this.setState({ segundofinal: data.segundoinicio });

      //Guardo el id de la entrevista
      this.setState({ valresource: data.interview });

      //Consulto la entrevista
      /*
      if (typeof data.interview !== 'undefined')
      {
        SemanticService.findByResorce(data.interview).then((entrevista) => {
          this.setState({ placeholderresource: entrevista.ident });                  

          //Cargo los audios de la entrevista
          if (typeof entrevista.ident !== 'undefined')
          {
            SemanticService.records(entrevista.ident).then((aud) => {          
              aud.forEach((t) => {
                console.log("externo");
                console.log(t._id);
                this.state.audios.push({ value: t._id, filename: t.filename,name:t.extra.nombre_original});
              });
              
              this.setState({listTerms : this.state.audios.map((aud) => <Col>
                <label><Form.Check value={aud.value} id={aud.filename} onChange={(check) => { this.onAudioChanged(check) }} type="radio" name="audioreproducir" />{aud.name}</label>
              </Col>)});

              this.setState({ disabledbuttonaudios: false });                            
            });
          }
        });
      }  
      */

      //Consulto el campo semantico relacionado
      if (typeof data.thematic_field !== "undefined") {
        SemanticService.get(data.thematic_field).then((campo) => {
          this.setState({ placeholdersemantic: campo.name });
        });
      }

      if (typeof data.audiopath !== "undefined") {
        TermService.audio(data._id).then((aud) => {
          this.setState({ createObjectURL: URL.createObjectURL(aud) });
        });
      }
    });

    //Creo la funcion para que busque dentro del select de la entrevista
    this.setState({
      resources: (inputValue) => {
        if (inputValue.length > 3) {
          return SemanticService.resources(inputValue);
        } else {
          return {};
        }
      },
    });

    //Creo la funcion para abrir el modal
    this.setState({
      openModal: () => {
        this.setState({ show: true });

        if (typeof this.state.semanctic.audiopath !== "undefined") {
          TermService.audio(this.state.semanctic._id).then((aud) => {
            this.setState({ createObjectURL: URL.createObjectURL(aud) });
          });
        }
      },
    });

    //Creo la funcion para abrir el modal del alineador
    this.setState({
      openModalAlinear: () => {
        this.setState({ showAlinear: true });
      },
    });

    //Creo la funcion para cerrar el modal
    this.setState({
      closeModal: () => {
        this.setState({ show: false });
      },
    });

    //Creo la funcion para cerrar el modal alinear
    this.setState({
      closeModalAlinear: () => {
        this.setState({ showAlinear: false });
      },
    });

    //Creo cambia audio
    this.setState({
      changeAudio: (evt) => {
        this.setState({ audio: evt.target.files[0] });
        this.setState({
          createObjectURL: URL.createObjectURL(evt.target.files[0]),
        });
      },
    });

    //Creo cargar audio
    this.setState({
      cargarAudio: () => {
        if (this.state.audio != null) {
          var typemime = [
            "audio/x-wav",
            "audio/webm",
            "audio/aac",
            "audio/midi",
            "audio/ogg",
            "audio/3gpp",
            "audio/3gpp2",
            "audio/mpeg",
          ];

          if (typemime.includes(this.state.audio.type)) {
            const f = new FormData();
            f.append("audio", this.state.audio);
            f.append("_id", this.state.semanctic._id);
            TermService.cargaraudio(this.props.match.params.id, f).then(
              (data) => {
                alert("Se cargo el audio con éxito");
              }
            );
          } else {
            alert("No es un archivo de audio, por favor subir un audio");
          }
        } else {
          alert("No ha seleccionado un audio para cargar");
        }
      },
    });

    //Cargo los departamentos
    SemanticService.semantics("a").then((dep) => {
      this.state.semanctics2 = dep;
    });

    //Creo la funcion para que busque dentro del select los campos semanticos
    this.setState({
      semanctics: (inputValue) => {
        return SemanticService.semantics(inputValue);
      },
    });
  }

  getLocation(location) {
    console.log(location);
    const new_location = location.name.split(",");
    if (new_location.length) {
      if (new_location[0]) this.setState({ placeholderPais: new_location[0] });
      if (new_location[1])
        this.setState({ placeholderdepartamento: new_location[1] });
      if (new_location[2])
        this.setState({ placeholdermunicipio: new_location[2] });
    }
  }

  onSubmit(formData, e) {
    var today = new Date();
    var year = today.getFullYear();

    if (formData.year_facts > year) {
      alert(
        "El año de ocurridos los hechos, no puede ser mayor al año actual."
      );
    } else {
      if (new Date(formData.interview_date) > today) {
        alert(
          "La fecha de la entrevista, no puede ser mayor a la fecha actual."
        );
      } else {
        if (this.state.formsubmit) {
          this.setState({ formsubmit: false });
          formData.terms = this.state.formterms;
          if (
            this.state.formresource !== null &&
            this.state.formresource !== ""
          ) {
            formData.interview = this.state.formresource;
          }
          if (
            this.state.formsemactic !== null &&
            this.state.formsemactic !== ""
          ) {
            formData.thematic_field = this.state.formsemactic;
          }
          if (
            this.state.interview_dep !== null &&
            this.state.interview_dep !== ""
          ) {
            formData.interview_dep = this.state.interview_dep;
          }
          if (
            this.state.interview_mun !== null &&
            this.state.interview_mun !== ""
          ) {
            formData.interview_mun = this.state.interview_mun;
          }
          TermService.update(this.props.match.params.id, formData).then(
            (data) => {
              alert("Se editó con éxito el término");
              this.setState({ formsubmit: true });
              this.props.history.push("/dictionary/term");
            }
          );
        }
      }
    }
  }

  onGuardarTiempo(tipo) {
    if (tipo === "inicio") {
      this.state.semanctic.segundoinicio = this.state.segundoactual;
      this.setState({ segundoinicio: this.state.segundoactual });
    }

    if (tipo === "fin") {
      this.state.semanctic.segundofinal = this.state.segundoactual;
      this.setState({ segundofinal: this.state.segundoactual });
    }

    if (this.state.formresource !== null && this.state.formresource !== "") {
      this.state.semanctic.interview = this.state.formresource;
    }
    if (this.state.formsemactic !== null && this.state.formsemactic !== "") {
      this.state.semanctic.thematic_field = this.state.formsemactic;
    }

    TermService.update(this.props.match.params.id, this.state.semanctic).then(
      (data) => {
        alert("Se guardo con éxito el tiempo");
      }
    );
  }

  onCancel() {
    this.props.history.push(
      "/dictionary/termedit/" + this.props.match.params.id
    );
  }

  onPreliminar() {
    this.props.history.push(
      "/dictionary/termeditv/" + this.props.match.params.id
    );
  }

  termChange(selectedOption) {
    if (selectedOption !== null) {
      selectedOption.forEach((t) => {
        if (this.state.formterms.includes(t.value) == false) {
          this.state.formterms.push(t.value);
        }
      });
    } else {
      this.state.formterms = [];
    }
  }

  resourceChange(selectedOption) {
    if (selectedOption !== null) {
      this.setState({ formresource: selectedOption._id });

      this.setState({ disabledbuttonaudios: true });

      //Ajusto los parametros de los audios por que cambio la entrevista
      this.setState({ audios: [] });
      this.state.semanctic.segundoinicio = this.state.segundoactual;
      this.state.semanctic.segundofinal = this.state.segundoactual;
      this.state.segundoinicio = this.state.segundoactual;
      this.state.segundofinal = this.state.segundoactual;

      //Cargo los audios de la entrevista
      SemanticService.records(selectedOption.ident).then((aud) => {
        aud.forEach((t) => {
          this.state.audios.push({
            value: t._id,
            filename: t.filename,
            name: t.extra.nombre_original,
          });
        });

        this.setState({
          listTerms: this.state.audios.map((aud) => (
            <Col>
              <label>
                <Form.Check
                  value={aud.value}
                  id={aud.filename}
                  onChange={(check) => {
                    this.onAudioChanged(check);
                  }}
                  type="radio"
                  name="audioreproducir"
                />
                {aud.name}
              </label>
            </Col>
          )),
        });

        this.setState({ disabledbuttonaudios: false });
      });
    } else {
      this.setState({ formresource: null });
    }
  }

  semacticChange(selectedOption) {
    if (selectedOption !== null) {
      this.setState({ formsemactic: selectedOption._id });
    } else {
      this.setState({ formsemactic: null });
    }
  }

  departamentChange(selectedOption) {
    if (selectedOption !== null) {
      //Cargo los municipios
      this.setState({ municipios: [] });
      this.setState({ interview_dep: selectedOption.name });
      ArcgisService.colmunicipality(selectedOption.value).then((mun) => {
        mun.forEach((m) => {
          this.state.municipios.push(m);
        });
      });
    } else {
      this.setState({ interview_dep: null });
    }
  }

  municipioChange(selectedOption) {
    if (selectedOption !== null) {
      //Cargo los municipios
      this.setState({ interview_mun: selectedOption.name });
    } else {
      this.setState({ interview_mun: null });
    }
  }

  onOpenModal() {
    this.setState({ show: true });
  }

  onCloseModal() {
    this.setState({ show: false });
  }

  onAudioChanged(e) {
    this.setState({ record: e.currentTarget.value });
    this.setState({ audioreproducir: e.currentTarget.id });
  }

  handleProgress = (state) => {
    this.setState({ segundoactual: state.playedSeconds });
  };

  render() {
    return (
      <>
        <UserAccessValidator rol="admin_diccionario" />
        <div className="cev-container  cev-container--fluid  cev-grid__item cev-grid__item--fluid">
          <>
            <div className="cev-grid cev-grid--desktop cev-grid--ver cev-grid--ver-desktop cev-app">
              <AdminDictionaryMenu />
              <div className="cev-grid__item cev-grid__item--fluid cev-app__content">
                <React.Fragment>
                  <div className="col-xl-12">
                    <div className="cev-portlet cev-portlet--height-fluid">
                      <div className="cev-portlet__head">
                        <div className="cev-portlet__head-label">
                          Vista preliminar
                        </div>
                      </div>
                      <div className="cev-portlet__body table-responsive">
                      <Container>
                      <Row className='mb-4'>
                          <Col><b>Nombre del Campo Semántico:</b></Col>
                          <Col style={alinear_texto}>{this.state.semanctic.name}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Término equivalente:</b></Col>
                          <Col style={alinear_texto}>{this.state.semanctic.equivalent}</Col>
                        </Row>
                        
                        <Row className='mb-4'>
                          <Col><b>Definición:</b></Col>                          
                          <Col style={alinear_texto}><div dangerouslySetInnerHTML={{ __html: this.state.semanctic.definition }} /></Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Fuente de la definición:</b></Col>
                          <Col style={alinear_texto}><div dangerouslySetInnerHTML={{ __html: this.state.semanctic.source_definition }} /></Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Contexto:</b></Col>
                          <Col style={alinear_texto}><div dangerouslySetInnerHTML={{ __html: this.state.semanctic.context }} /></Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Fuente del contexto:</b></Col>
                          <Col style={alinear_texto}><div dangerouslySetInnerHTML={{ __html: this.state.semanctic.source_context }} /></Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Entrevista N°:</b></Col>
                          <Col style={alinear_texto}>{this.state.semanctic.interview}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Fecha de la entrevista:</b></Col>
                          <Col style={alinear_texto}>{this.state.semanctic.interview_date}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Pertenencia étnica:</b></Col>
                          <Col style={alinear_texto}>{this.state.semanctic.etnia}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Sexo (asignado al nacer) de la persona entrevistada:</b></Col>
                          <Col style={alinear_texto}>{this.state.semanctic.sexo}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Orientación sexual de la persona entrevistada (se siente atraído por):</b></Col>
                          <Col style={alinear_texto}>{this.state.semanctic.sexual_orientation}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Otros:</b></Col>
                          <Col style={alinear_texto}><div dangerouslySetInnerHTML={{ __html: this.state.semanctic.others }} /></Col>                          
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Edad:</b></Col>
                          <Col style={alinear_texto}>{this.state.semanctic.age}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Fragmento de la entrevista:</b></Col>
                          <Col style={alinear_texto}><div dangerouslySetInnerHTML={{ __html: this.state.semanctic.fragment }} /></Col>                                                    
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Imagen:</b></Col>
                          <Col><img src={this.state.semanctic.img} alt="Imagen" width="200px"/>​</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Lugar de la entrevista - Departamento:</b></Col>
                          <Col style={alinear_texto}>{this.state.placeholderdepartamento}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Lugar de la entrevista - Municipio:</b></Col>
                          <Col style={alinear_texto}>{this.state.placeholdermunicipio}</Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Campo semántico principal:</b></Col>
                          <Col style={alinear_texto}>{this.state.placeholdersemantic}</Col>
                        </Row>                        
                        <Row className='mb-4'>
                          <Col><b>Términos:</b></Col>
                          <Col> 
                          <ul>
                            {this.state.placeholderterms}
                          </ul>                           
                          </Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col><b>Audio Entrevista:</b></Col>
                          <Col> 
                          <ReactPlayer
                                    url={this.state.createObjectURL}
                                    width="400px"
                                    height="50px"
                                    playing={false}
                                    controls={true}                                    
                                  />
                          </Col>
                        </Row>
                        <Row className='mb-4'>
                          <Col>
                            <Button className='mr-4' variant="secondary" onClick={() => {
                              this.onCancel();
                            }}>Volver</Button>
                          </Col>
                        </Row>
                      </Container>
                                                
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              </div>
            </div>
          </>
        </div>
      </>
    );
  }
}

const mapStateToProps = (store) => ({
  search: store.app.keyword,
  user: store.auth.user,
});

function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) => <li>{number}</li>);
  return <ul>{listItems}</ul>;
}

export default connect(mapStateToProps, app.actions)(TermEditPageV);
