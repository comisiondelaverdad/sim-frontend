import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
//import { toAbsoluteUrl } from "../../../theme/utils";
import AdminDictionaryMenu from "../../components/organisms/AdminDictionaryMenu";
import * as app from "../../store/ducks/app.duck";
import * as SemanticService from "../../services/SemanticService";
//import moment from "moment";
import Select from 'react-select'
import Form from "react-jsonschema-form-bs4";
import * as TermService from "../../services/TermService";
import { Button } from "react-bootstrap";
import JoditEditor from "jodit-react";
import { isThisSecond } from "date-fns";

const uiSchema = {
  definition: {
    "ui:widget": "textarea",
  },
  "ui:widget": "checkboxes",
  "ui:options": {
    inline: true
  }
};

class SemanticEditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      semanctic: [],
      schema: {},
      terms: [],
      valterms: [],
      formterms: [],
      formsubmit: true,
      placeholderterms: "Seleccionar Términos",
      definition: "",
      config: { readonly: false },
      options: [],
      selected_options : []
    };
  }

  loadData = () => {
    this.setState({formterms: []})
    SemanticService.terms().then((data1) => {
      let schema = {
        type: "object",
        required: ["name"],
        properties: {
          name: { type: "string", title: "Nombre del Campo Semántico" },
          //definition: { type: "string", title: "Definición" },
          active: { type: "boolean", title: "Activo?", default: true },
          img: {
            type: "string",
            format: "data-url",
            title: "Imagen",
          },
        },
      };

      this.setState({ schema: schema });

      SemanticService.get(this.props.match.params.id).then((data) => {
        if (Object.keys(data.terms).length > 0) {
          this.setState({ placeholderterms: "" });
        }

        //Creo el select multiple de Términos
        let new_options = []
        TermService.terms().then((data1) => {
          data1.forEach((c) => {
            this.state.terms.push({ value: c._id, label: c.name });
            new_options.push({ value: c._id, label: c.name })
          });
          this.setState({options: new_options})
        });

        //Cargo el select multiple de Términos, permite mostrar los Términos en el select
        let loaded_options = []
        data.terms.forEach((t) => {
          this.state.valterms.push({ value: t._id, label: t.name });
          loaded_options.push({ value: t._id, label: t.name })
          // if (this.state.placeholderterms === "") {
          //   this.setState({ placeholderterms: t.name });
          // } else {
          //   this.setState({
          //     placeholderterms: this.state.placeholderterms + " , " + t.name,
          //   });
          // }
          this.state.formterms.push(t._id);
        });
        this.setState({selected_options: loaded_options})

        this.setState({ semanctic: data });

        this.setState({ definition: data.definition });
      });
    });
  };
  // upadte = () =>{
  //   alert('entra')

  // }
  // componentDidUpdate(){
  //   this.upadte()
  // }

  componentDidMount() {
    this.loadData()
  }

  onSubmit(formData, e) {
    if (this.state.formsubmit) {
      this.setState({ formsubmit: false });
      formData.status = formData.status === "Activo";
      formData.terms = this.state.formterms;
      formData.definition = this.state.definition;
      SemanticService.update(this.props.match.params.id, formData).then(
        (data) => {
          alert("Se editó con éxito el campo semántico");
          this.setState({ formsubmit: true });
          this.loadData()
        }
      );
    }
  }

  onCancel() {
    this.props.history.push("/dictionary/semantic");
  }

  onPreliminar() {
    this.props.history.push(
      "/dictionary/semanticeditv/" + this.props.match.params.id
    );
  }

  termChange(selectedOption) {
    this.setState({selected_options: selectedOption})
    this.state.formterms = [];
    if (selectedOption !== null) {
      selectedOption.forEach((t) => {
        this.state.formterms.push(t.value);
      });
      console.log(this.state.formterms);
    }

    //this.setState({ level: selectedOption, locationsValue: null, selected: undefined })
  }

  render() {
    /*
    const options = [
      { value: '60dced40ee377e2a60a12a8f', label: 'Termino 1' },
      { value: '60df95b856fff56bd0bee7fb', label: 'Termino 2' },
      { value: '60e251769ac10d21c07017df', label: 'Termino 3' },
    ];

    const defaultoptions = [
      { value: '60df95b856fff56bd0bee7fb', label: 'Termino 2' },
    ];

    console.log("CONSTANTES");
    console.log(options);
    console.log(defaultoptions);

    console.log("METODO");
    console.log(this.state.terms);
    console.log(this.state.valterms);
    */

    return (
      <>
        <UserAccessValidator rol="admin_diccionario" />
        <div className="cev-container  cev-container--fluid  cev-grid__item cev-grid__item--fluid">
          <>
            <div className="cev-grid cev-grid--desktop cev-grid--ver cev-grid--ver-desktop cev-app">
              <AdminDictionaryMenu />
              <div className="cev-grid__item cev-grid__item--fluid cev-app__content">
                <React.Fragment>
                  <div className="col-xl-12">
                    <div className="cev-portlet cev-portlet--height-fluid">
                      <div className="cev-portlet__head">
                        <div className="cev-portlet__head-label">
                          Editar campo semántico
                        </div>
                      </div>
                      <div className="cev-portlet__body table-responsive">
                        <Form
                          enctype="multipart/form-data"
                          schema={this.state.schema}
                          uiSchema={uiSchema}
                          formData={this.state.semanctic}
                          onSubmit={({ formData }, e) =>
                            this.onSubmit(formData, e)
                          }
                        >
                          <div>
                            <img
                              src={this.state.semanctic.img}
                              alt="Imagen"
                              width="200px"
                            />
                            ​
                          </div>
                          <br />
                          <div class="form-group field field-string">
                            <label for="terms">Definición</label>
                            <JoditEditor
                              value={this.state.definition}
                              config={this.state.config}
                              tabIndex={1} // tabIndex of textarea
                              onChange={(newContent) => {
                                this.state.definition = newContent;
                              }}
                            />
                          </div>
                          <br />
                          <div class="form-group field field-string">
                            <label for="terms">Términos</label>
                            <Select
                              value={this.state.selected_options}
                              isMulti
                              name="terms"
                              options={this.state.options}
                              className="basic-multi-select"
                              classNamePrefix="select"
                              onChange={(selected) => {
                                this.termChange(selected);
                              }}
                              placeholder="Seleccione términos..."
                            />
                          </div>
                          <br />
                          <div>
                            <button type="submit" class="btn btn-primary mr-4">
                              Guardar
                            </button>
                            <Button
                              className="mr-4"
                              variant="dark"
                              onClick={() => {
                                this.onCancel();
                              }}
                            >
                              Cancelar
                            </Button>
                            <Button
                              variant="secondary"
                              onClick={() => {
                                this.onPreliminar();
                              }}
                            >
                              Vista preliminar
                            </Button>
                          </div>
                        </Form>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              </div>
            </div>
          </>
        </div>
      </>
    );
  }
}

const mapStateToProps = (store) => ({
  search: store.app.keyword,
  user: store.auth.user,
});

export default connect(mapStateToProps, app.actions)(SemanticEditPage);
