import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
//import { toAbsoluteUrl } from "../../../theme/utils";
import AdminDictionaryMenu from "../../components/organisms/AdminDictionaryMenu";
import AlignDictionary from "../../components/organisms/AlignDictionary";
import TermsRecordsModal from "../../components/organisms/TermsRecordsModal";
import * as app from "../../store/ducks/app.duck";
import * as TermService from "../../services/TermService";
import * as SemanticService from "../../services/SemanticService";
import * as ArcgisService from "../../services/arcgisService"
//import moment from "moment";
import Select from 'react-select'
import AsyncSelect from 'react-select/async';
import Form2 from "react-jsonschema-form-bs4";
import { Button, Modal, Row, Container, Col, Form } from "react-bootstrap";
import ReactPlayer from 'react-player'

import JoditEditor from "jodit-react";
import FormLocationMultiple from "../../components/organisms/FormLocationMultiple";



const uiSchema = {
  definition: {
    "ui:widget": "textarea",
  },
  fragment: {
    "ui:widget": "textarea",
  },
  others: {
    "ui:widget": "textarea",
  },
  "ui:widget": "checkboxes",
  "ui:options": {
    inline: true
  }
};


class TermEditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      semanctic: [],
      schema: {},
      terms: [],
      resources: [],
      semanctics: [],
      semanctics2: [],
      departamens: [],
      municipios: [],
      valterms: [],
      formterms: [],
      valresource: null,
      placeholderresource: "Seleccionar Entrevista",
      placeholdersemantic: "Seleccionar Campo Semántico",
      placeholderdepartamento: "Seleccionar Departamento",
      placeholdermunicipio: "Seleccionar Municipio",
      placeholderterms: "Seleccionar Términos",
      disabledbuttonaudios:true,
      formresource: null,
      formsemactic: null,
      formsubmit: true,
      show: false,
      showAlinear: false,
      audios: [],
      audioreproducir: "",
      record: null,
      segundoactual:0,
      segundoinicio:null,
      segundofinal:null,
      listTerms:[],
      openModal:[],
      openModalAlinear:[],
      closeModal:[],
      closeModalAlinear:[],
      changeAudio:[],
      cargarAudio:[],
      createObjectURL:'',
      audio:null,
      name_dep:[],
      context:'',
      source_context:'',
      definition:'',  
      source_definition:'',         
      others:"",
      fragment: "",   
      interview_location: null,  
      interview_location_multiples: [],                                                                

      config: {readonly: false}
    };
  }

  componentDidMount() {

    //Cargo los departamentos      
    ArcgisService.colAdministrativeDivision().then((dep => {      
      dep.forEach((d) => {
        this.state.departamens.push(d);      
      });      
    }));  

  let schema = {
      type: "object",
      required: ["name"],
      properties: {
        name: { type: "string", title: "Nombre del término" },        
        equivalent: { type: "string", title: "Término equivalente" },
        //definition: { type: "string", title: "Definición" },
       // source_definition: { type: "string", title: "Fuente de la definición" },
        //context: { type: "string", title: "Contexto" },
       // source_context: { type: "string", title: "Fuente del contexto" },
        interview: { type: "string", title: "Entrevista N°"},
        interview_date: { type: "string", title: "Fecha de la entrevista" , "format": "date"},
        //interview_dep: { type: "string", title: "Lugar de la entrevista - Departamento", enum: this.state.name_dep},
        //interview_mun: { type: "string", title: "Lugar de la entrevista - Municipio" },                
        //year_facts: { type: "integer", title: "Año de ocurridos los hechos" },                
        etnia: { type: "string", title: "Pertenencia étnica", enum: ["Mestizo/a", "Afrocolombiano/a", "Negro/a", "Raizal", "Palenquera/o", "Rrom", "Indígena"] },
        sexo: { type: "string", title: " Sexo (asignado al nacer) de la persona entrevistada", enum: ["Femenino", "Masculino", "Intersexual"] },
        sexual_orientation: { type: "string", title: "Orientación sexual de la persona entrevistada (se siente atraído por):", enum: ["Asexual", "Bisexual", "Heterosexual", "Homosexual", "Pansexual"] },
        //others: { type: "string", title: "Otros" },                                 
        age: { type: "integer", title: "Edad" },
        //fragment: { type: "string", title: "Fragmento de la entrevista" },                                 
        active: { type: "boolean", title: "Activo?", default: true },
        img: {
          "type": "string",
          "format": "data-url",
          "title": "Imagen"
        }      },
    };

    //Set del esquema general del formulario
    this.setState({ schema: schema });

    //Creo el select multiple de Términos
    TermService.terms().then((data1) => {      
      data1.forEach((c) => {
        this.state.terms.push({ value: c._id, label: c.name });
      });      
    });

    //Cargo el termino en el formulario
    TermService.get(this.props.match.params.id).then((data) => {

      if(Object.keys(data.terms).length>0)
      {
        this.setState({ placeholderterms: ""});
      }
      
      //Cargo el select multiple de Términos, permite mostrar los Términos en el select
      data.terms.forEach((t) => {
        this.state.valterms.push({ value: t._id, label: t.name });
        if(this.state.placeholderterms==="")
        {
          this.setState({ placeholderterms: t.name });
        }
        else
        {
          this.setState({ placeholderterms: this.state.placeholderterms+" , "+t.name });
        }        
        this.state.formterms.push(t._id);
      });

      if (typeof data.interview_dep !== 'undefined')
      {
        this.setState({ placeholderdepartamento: data.interview_dep });
      }

      if (typeof data.interview_mun !== 'undefined')
      {
        this.setState({ placeholdermunicipio: data.interview_mun });
      }

      if(data.interview_location)
      {
        // this.setState({interview_location_multiples: data.interview_location})
        this.setState({ 
          interview_location_multiples: [...this.state.interview_location_multiples, data.interview_location] 
     })
      }

     

      //Para setear el editor html
      this.setState({ context: data.context });
      this.setState({ source_context: data.source_context });      
      this.setState({ definition: data.definition });
      this.setState({ source_definition: data.source_definition });      
      this.setState({ others: data.others });
      this.setState({ fragment: data.fragment });

      //Set del objeto termino y los tiempos
      this.setState({ semanctic: data });
      this.setState({ segundoinicio: data.segundoinicio });                      
      this.setState({ segundofinal: data.segundoinicio }); 

      //guardó el id de la entrevista
      this.setState({ valresource: data.interview });

      //Consulto la entrevista
      /*
      if (typeof data.interview !== 'undefined')
      {
        SemanticService.findByResorce(data.interview).then((entrevista) => {
          this.setState({ placeholderresource: entrevista.ident });                  

          //Cargo los audios de la entrevista
          if (typeof entrevista.ident !== 'undefined')
          {
            SemanticService.records(entrevista.ident).then((aud) => {          
              aud.forEach((t) => {
                console.log("externo");
                console.log(t._id);
                this.state.audios.push({ value: t._id, filename: t.filename,name:t.extra.nombre_original});
              });
              
              this.setState({listTerms : this.state.audios.map((aud) => <Col>
                <label><Form.Check value={aud.value} id={aud.filename} onChange={(check) => { this.onAudioChanged(check) }} type="radio" name="audioreproducir" />{aud.name}</label>
              </Col>)});

              this.setState({ disabledbuttonaudios: false });                            
            });
          }
        });
      }  
      */

      //Consulto el campo semantico relacionado      
      if (typeof data.thematic_field !== 'undefined')
      {
        SemanticService.get(data.thematic_field).then((campo) => {
          this.setState({ placeholdersemantic: campo.name });                  
        });
      }
      
    })

    //Creo la funcion para que busque dentro del select de la entrevista
    this.setState({
      resources: (inputValue) => {
        if(inputValue.length>3)
        {
          return SemanticService.resources(inputValue);
        }
        else
        {
          return {};
        }
      }
    });


    //Creo la funcion para abrir el modal
    this.setState({
      openModal: () => {
        this.setState({ show: true });

        if (typeof this.state.semanctic.audiopath !== 'undefined')
        {
          TermService.audio(this.state.semanctic._id).then((aud) => {
            this.setState({ createObjectURL: URL.createObjectURL(aud) });                   
          });          
        }

      }
    });

    //Creo la funcion para abrir el modal del alineador
    this.setState({
      openModalAlinear: () => {
        this.setState({ showAlinear: true });
      }
    });

    //Creo la funcion para cerrar el modal
    this.setState({
      closeModal: () => {
        this.setState({ show: false });
      }
    });

    //Creo la funcion para cerrar el modal alinear
    this.setState({
      closeModalAlinear: () => {
        this.setState({ showAlinear: false });
      }
    });

    //Creo cambia audio
    this.setState({
      changeAudio: evt => {
        this.setState({ audio: evt.target.files[0] });  
        this.setState({ createObjectURL: URL.createObjectURL(evt.target.files[0]) });          
      }
    });

    
    //Creo cargar audio
    this.setState({
      cargarAudio: () => {
        if(this.state.audio!=null)
        {
          var typemime=['audio/x-wav','audio/webm','audio/aac','audio/midi','audio/ogg','audio/3gpp','audio/3gpp2','audio/mpeg'];

          if( typemime.includes( this.state.audio.type ) )
          {
            const f = new FormData();
            f.append("audio",this.state.audio);
            f.append("_id",this.state.semanctic._id);
            TermService.cargaraudio(this.props.match.params.id, f).then((data) => {
              alert("Se cargo el audio con éxito");              
              this.state.semanctic.record=data.record;                          
            })        
          }
          else
          {
            alert("No es un archivo de audio, por favor subir un audio");
          }          
        }
        else
        {
          alert("No ha seleccionado un audio para cargar")
        }        
      }
    });


    //Cargo todos los campos
    SemanticService.selectsemantic().then((dep => {      
      this.state.semanctics2=dep;
    }));     

    //Creo la funcion para que busque dentro del select los campos semanticos
    /*
    this.setState({
      semanctics: (inputValue) => {        
        return SemanticService.semantics(inputValue);
      }
    });
    */

  }

  outputLocation = (event) => {    
    
    if (event.locationInterview && event.locationInterview.length) {
      this.state.interview_location = event.locationInterview[0]
    }
  };

  onSubmit(formData, e) {

    var today = new Date();
    var year = today.getFullYear();

    if(formData.year_facts>year)
    {
      alert("El año de ocurridos los hechos, no puede ser mayor al año actual.");
    }
    else
    {
      if(new Date(formData.interview_date)>today)
      {
        alert("La fecha de la entrevista, no puede ser mayor a la fecha actual.");
      }
      else
      {

        if (this.state.formsubmit) {
          this.setState({ formsubmit: false });      
          formData.terms = this.state.formterms;
          if (this.state.formresource !== null && this.state.formresource !== '') {
            formData.interview = this.state.formresource;
          }
          if (this.state.formsemactic !== null && this.state.formsemactic !== '') {
            formData.thematic_field = this.state.formsemactic;
          }
          if (this.state.interview_dep !== null && this.state.interview_dep !== '') {
            formData.interview_dep = this.state.interview_dep;
          }
          if (this.state.interview_mun !== null && this.state.interview_mun !== '') {
            formData.interview_mun = this.state.interview_mun;
          }
          //guardó el html
          formData.context = this.state.context;
          formData.source_context = this.state.source_context;
          formData.definition = this.state.definition;
          formData.source_definition= this.state.source_definition;
          formData.others = this.state.others;
          formData.fragment = this.state.fragment;
          formData.interview_location = this.state.interview_location
          
          TermService.update(this.props.match.params.id, formData).then((data) => {
            alert("Se editó con éxito el término");
            this.setState({ formsubmit: true });            
          })
        }
      }
    }

  }

  onGuardarTiempo(tipo) {
    
      if(tipo==='inicio')
      {        
        this.state.semanctic.segundoinicio=this.state.segundoactual;      
        this.setState({ segundoinicio: this.state.segundoactual });          
      }

      if(tipo==='fin')
      {
        this.state.semanctic.segundofinal=this.state.segundoactual;        
        this.setState({ segundofinal: this.state.segundoactual });                
      }

      if (this.state.formresource !== null && this.state.formresource !== '') {
        this.state.semanctic.interview = this.state.formresource;
      }
      if (this.state.formsemactic !== null && this.state.formsemactic !== '') {
        this.state.semanctic.thematic_field = this.state.formsemactic;
      }
      
      TermService.update(this.props.match.params.id, this.state.semanctic).then((data) => {
        alert("Se guardó con éxito el tiempo");
      })      
      
  }


  onCancel() {
    this.props.history.push("/dictionary/term");
  }

  onPreliminar() {
    this.props.history.push("/dictionary/termeditv/"+this.props.match.params.id);
  }

  termChange(selectedOption) {
    if (selectedOption !== null) {
      selectedOption.forEach((t) => {
        if (this.state.formterms.includes(t.value) == false) {
          this.state.formterms.push(t.value);
        }
      });
    }
    else {
      this.state.formterms = [];
    }
  }

  resourceChange(selectedOption) {
    if (selectedOption !== null) {
      this.setState({ formresource: selectedOption._id });
      
      this.setState({ disabledbuttonaudios: true });
      
      //Ajusto los parametros de los audios por que cambio la entrevista
      this.setState({ audios: [] });
      this.state.semanctic.segundoinicio=this.state.segundoactual;            
      this.state.semanctic.segundofinal=this.state.segundoactual;  
      this.state.segundoinicio=this.state.segundoactual;  
      this.state.segundofinal=this.state.segundoactual;  
      

      //Cargo los audios de la entrevista
      SemanticService.records(selectedOption.ident).then((aud) => {          
      
        aud.forEach((t) => {
          this.state.audios.push({ value: t._id, filename: t.filename,name:t.extra.nombre_original});
        }); 
        
        this.setState({listTerms : this.state.audios.map((aud) => <Col>
          <label><Form.Check value={aud.value} id={aud.filename} onChange={(check) => { this.onAudioChanged(check) }} type="radio" name="audioreproducir" />{aud.name}</label>
        </Col>)});               

        this.setState({ disabledbuttonaudios: false });                                    
      });
      
    }
    else {
      this.setState({ formresource: null });
    }
  }

  semacticChange(selectedOption) {
    if (selectedOption !== null) {
      this.setState({ formsemactic: selectedOption._id });
      
    }
    else {
      this.setState({ formsemactic: null });
    }
  }

  departamentChange(selectedOption) {
    if (selectedOption !== null) {
      //Cargo los municipios
      this.setState({ municipios: [] });  
      this.setState({ interview_dep: selectedOption.name });    
      ArcgisService.colmunicipality(selectedOption.value).then((mun => {      
        mun.forEach((m) => {
          this.state.municipios.push(m);
        });      
      }));  
    }
    else {
      this.setState({ interview_dep: null });          
    }
  }

  municipioChange(selectedOption) {
    if (selectedOption !== null) {
      //Cargo los municipios
      this.setState({ interview_mun: selectedOption.name });          
    }
    else {
      this.setState({ interview_mun: null });          
    }
  }

  onOpenModal() {
    this.setState({ show: true });
  }

  onCloseModal() {
    this.setState({ show: false });
  }

  onAudioChanged(e) {
    this.setState({ record: e.currentTarget.value });
    this.setState({ audioreproducir: e.currentTarget.id });
  }

  handleProgress = state => {
    this.setState({ segundoactual: state.playedSeconds });
  }

  render() {
    const termPlace = {
      paddingLeft: "0px",
      paddingRight: "0px",
      marginLeft: "-20px",
      marginRight: "0px",
      width: "104%",
      maxWidth: "104%",
      marginBottom: "-10px",
    };
    return (
      <>
        <UserAccessValidator rol="admin_diccionario" />
        <div className="cev-container  cev-container--fluid  cev-grid__item cev-grid__item--fluid">
          <>
            <div className="cev-grid cev-grid--desktop cev-grid--ver cev-grid--ver-desktop cev-app">
              <AdminDictionaryMenu />
              <div className="cev-grid__item cev-grid__item--fluid cev-app__content">
                <React.Fragment>
                  <div className="col-xl-12">
                    <div className="cev-portlet cev-portlet--height-fluid">
                      <div className="cev-portlet__head">
                        <div className="cev-portlet__head-label">
                          Editar término 
                        </div>
                      </div>
                      <div className="cev-portlet__body table-responsive">
                        <Form2 enctype="multipart/form-data"
                          schema={this.state.schema}
                          uiSchema={uiSchema}
                          formData={this.state.semanctic}
                          onSubmit={({ formData }, e) =>
                            this.onSubmit(formData, e)
                          }

                        >
                          <div><img src={this.state.semanctic.img} alt="Imagen" width="200px" />​</div>
                          <br /><br /> 
                          <div class="form-group field field-string">
                            <label for="terms">Definición</label>
                            <JoditEditor                                  
                                  value={this.state.definition}
                                  config={this.state.config}
                                  tabIndex={1} // tabIndex of textarea                                  
                                  onChange={newContent => {this.state.definition=newContent}}
                            />                            
                          </div>
                          <br />
                          <div class="form-group field field-string">
                            <label for="terms">Fuente de la definición</label>
                            <JoditEditor                                  
                                  value={this.state.source_definition}
                                  config={this.state.config}
                                  tabIndex={1} // tabIndex of textarea                                  
                                  onChange={newContent => {this.state.source_definition=newContent}}
                            />                            
                          </div>
                          <br />
                          <div class="form-group field field-string">
                            <label for="terms">Contexto</label>
                            <JoditEditor                                  
                                  value={this.state.context}
                                  config={this.state.config}
                                  tabIndex={1} // tabIndex of textarea                                  
                                  onChange={newContent => {this.state.context=newContent}}
                            />                            
                          </div>
                          <br /> 
                          <div class="form-group field field-string">
                            <label for="terms">Fuente del contexto</label>
                            <JoditEditor                                  
                                  value={this.state.source_context}
                                  config={this.state.config}
                                  tabIndex={1} // tabIndex of textarea                                  
                                  onChange={newContent => {this.state.source_context=newContent}}
                            />                            
                          </div>
                          <br /> 
                          <div class="form-group field field-string">
                            <label for="terms">Fragmento de la entrevista</label>
                            <JoditEditor                                  
                                  value={this.state.fragment}
                                  config={this.state.config}
                                  tabIndex={1} // tabIndex of textarea                                  
                                  onChange={newContent => {this.state.fragment=newContent}}
                            />                            
                          </div>
                          <br />
                          <div class="form-group field field-string">
                            <label for="terms">Otros</label>
                            <JoditEditor                                  
                                  value={this.state.others}
                                  config={this.state.config}
                                  tabIndex={1} // tabIndex of textarea                                  
                                  onChange={newContent => {this.state.others=newContent}}
                            />                            
                          </div>
                          <br />   
                          <div class="form-group field field-string">
                            <label for="terms">Lugar de la entrevista</label>
                            <div style={termPlace}>
                              <FormLocationMultiple
                                outputEvent={(e) => {this.outputLocation(e)}}
                                multiple={false}
                                name="locationInterview"
                                formData={(this.state.interview_location_multiples) ? this.state.interview_location_multiples : ''}
                              ></FormLocationMultiple>
                            </div>
                          </div>                      
                          {/* <div class="form-group field field-string">
                            <label for="terms">Lugar de la entrevista - Departamento</label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              placeholder={this.state.placeholderdepartamento}                                                           
                              getOptionLabel={e => e.name}
                              getOptionValue={e => e.value}  
                              options={this.state.departamens}
                              onChange={(value) => this.departamentChange(value)}                                
                            />                            
                          </div>
                          <br />
                          <div class="form-group field field-string">
                            <label for="terms">Lugar de la entrevista - Municipio</label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              placeholder={this.state.placeholdermunicipio}                                                           
                              getOptionLabel={e => e.name}
                              getOptionValue={e => e.value}  
                              options={this.state.municipios}      
                              onChange={(value) => this.municipioChange(value)}                                                        
                            />                            
                          </div> */}
                          <br />
                          <div class="form-group field field-string">
                            <label for="terms">Campo semántico principal</label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              placeholder={this.state.placeholdersemantic}                                                           
                              getOptionLabel={e => e.name}
                              getOptionValue={e => e._id}  
                              options={this.state.semanctics2}
                              onChange={(value) => this.semacticChange(value)}                                
                            />
                          </div>
                          <br />
                          <div class="form-group field field-string">
                            <label for="terms">Términos</label>
                            <Select
                              defaultValue={this.state.valterms}
                              isMulti
                              name="terms"
                              options={this.state.terms}
                              className="basic-multi-select"
                              classNamePrefix="select"
                              onChange={(selected) => { this.termChange(selected) }}
                              placeholder={this.state.placeholderterms}
                            />
                          </div>
                          <br />
                          <div>
                            <Button variant="success" onClick={this.state.openModal}>
                              Seleccionar audio de la entrevista
                            </Button>                            
                          </div>
                          <br />
                          <div>
                            <Button variant="warning" onClick={this.state.openModalAlinear}>
                              Alinear audio con el fragmento de la entrevista
                            </Button>                            
                          </div>
                          <br/>
                          <div>
                            <button type="submit" class="btn btn-primary mr-4">
                              Guardar
                            </button>
                            <Button className='mr-4' variant="dark" onClick={() => {
                                this.onCancel();
                              }}>Cancelar</Button>
                            <Button variant="secondary" onClick={() => {
                                this.onPreliminar();
                              }}>Vista preliminar</Button>
                          </div>
                        </Form2>
                        <Modal show={this.state.show} onHide={this.state.closeModal}>
                          <Modal.Header closeButton>
                            <Modal.Title>Cargar audio</Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                                  <input type="file" name="audio" multiple={false} onChange={this.state.changeAudio}  />

                                  <ReactPlayer
                                    url={this.state.createObjectURL}
                                    width="400px"
                                    height="50px"
                                    playing={false}
                                    controls={true}                                    
                                  />
                          </Modal.Body>
                          <Modal.Footer>
                            <Button variant="secondary" onClick={this.state.closeModal}>
                              Cerrar
                            </Button>
                            <Button variant="primary" onClick={this.state.cargarAudio}>
                              Guardar Audio
                            </Button>
                          </Modal.Footer>
                        </Modal>

                        <Modal 
                          show={this.state.showAlinear} 
                          onHide={this.state.closeModalAlinear} 
                          size="lg"
                          aria-labelledby="contained-modal-title-vcenter"
                          centered>
                          <Modal.Header closeButton>
                            <Modal.Title>Alinear audio con el fragmento de la entrevista</Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                          {this.state.semanctic.record &&
                            <AlignDictionary recordId={this.state.semanctic.record} history={this.props.history} pagehistory='/dictionary/term/'></AlignDictionary>
                          }
                          </Modal.Body>
                          <Modal.Footer>
                            
                          </Modal.Footer>
                        </Modal>                        
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              </div>
            </div>
          </>
        </div>
      </>
    );
  }
}

const mapStateToProps = (store) => ({
  search: store.app.keyword,
  user: store.auth.user,
});

function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) => <li>{number}</li>); return (
    <ul>{listItems}</ul>);
}

export default connect(mapStateToProps, app.actions)(TermEditPage);


/*
//Componente que pagina en la busqueda
                            <AsyncSelect                              
                              loadOptions={this.state.semanctics}
                              getOptionLabel={e => e.name}
                              getOptionValue={e => e._id}  
                              onChange={(value) => this.semacticChange(value)}  
                              placeholder={this.state.placeholdersemantic}                                                           
                            />




FUNCIONALIDAD PARA SELECCIONAR UN AUDIO
<br />
                          <div class="form-group field field-string">
                            <label for="terms">Entrevista N.º</label>
                            <AsyncSelect                              
                              loadOptions={this.state.resources}
                              getOptionLabel={e => e.ident}
                              getOptionValue={e => e._id}                                 
                              onChange={(value) => this.resourceChange(value)}
                              placeholder={this.state.placeholderresource}
                            />
                          </div>                          
                          <div>
                            <Button variant="success" disabled={this.state.disabledbuttonaudios} onClick={() => {
                              this.onOpenModal();
                            }}>
                              Seleccionar audio de la entrevista
                            </Button>
                            <Modal show={this.state.show} >
                              <Modal.Header>
                                <Modal.Title>Audios</Modal.Title>
                              </Modal.Header>
                              <Modal.Body>
                                <Container>
                                  <Row md={4}>
                                    {this.state.listTerms}
                                  </Row>
                                </Container>
                                <ReactPlayer
                                    url="https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3"
                                    width="400px"
                                    height="50px"
                                    playing={false}
                                    controls={true}
                                    onProgress={this.handleProgress}                                    
                                  />                                  
                                  <br/><br/>
                                  <Container>
                                  <Row>
                                    <Col sm><h4>Tiempo de reproducción actual: {this.state.segundoactual}</h4></Col>                                    
                                  </Row>
                                  <Row>
                                    <Col sm>Inicio: {this.state.segundoinicio}</Col>
                                    <Col sm>Fin: {this.state.segundofinal}</Col>                                    
                                  </Row>
                                  <Row>
                                    <Col sm>
                                      <button
                                        type="button"
                                        class="btn btn-success"
                                        onClick={() => {
                                          this.onGuardarTiempo('inicio');
                                        }}
                                      >
                                        Guardar tiempo inicio
                                      </button>                                      
                                    </Col>
                                    <Col sm>
                                    <button
                                        type="button"
                                        class="btn btn-warning"
                                        onClick={() => {
                                          this.onGuardarTiempo('fin');
                                        }}
                                      >
                                        Guardar tiempo fin
                                      </button>
                                    </Col>                                    
                                  </Row>
                                  
                                </Container>
                              </Modal.Body>
                              <Modal.Footer>
                                <Button variant="secondary" onClick={() => {
                                  this.onCloseModal();
                                }}>
                                  Cerrar
                                </Button>
                              </Modal.Footer>
                            </Modal>
                          </div>


*/

