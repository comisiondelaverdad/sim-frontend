import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
//import { toAbsoluteUrl } from "../../../theme/utils";
import AdminDictionaryMenu from "../../components/organisms/AdminDictionaryMenu";
import * as app from "../../store/ducks/app.duck";
import * as SemanticService from "../../services/SemanticService";
import * as TermService from "../../services/TermService";
//import moment from "moment";

import Form from "react-jsonschema-form-bs4";
import Select from 'react-select'
import JoditEditor from "jodit-react";

const uiSchema = {
  definition: {
    "ui:widget": "textarea",
  },
  "ui:widget": "checkboxes",
  "ui:options": {
    inline: true
  }
};

class SemanticAddPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: [],
      schema: {},
      formsubmit: true,
      placeholderterms: "Seleccionar Términos",
      terms: [],
      formterms: [],
      definition:'',    
      config: {readonly: false}  
    };
  }

  componentDidMount() {
      let schema = {
        type: "object",
        required: ["name"],
        properties: {
          name: { type: "string", title: "Nombre del Campo Semántico" },          
          //definition: { type: "string", title: "Definición" },
          img: {
            "type": "string",
            "format": "data-url",
            "title": "Imagen"
          },    
          active: { type: "boolean", title: "Activo?", default: true },
        },
      };
      this.setState({schema: schema})      

      //Creo el select multiple de Términos
      TermService.terms().then((data1) => {      
        data1.forEach((c) => {
          this.state.terms.push({ value: c._id, label: c.name });
        });      
      });
    
  }
  
  onSubmit(formData, e) {
    if(this.state.formsubmit)
    {
      this.setState({formsubmit: false});
      formData.terms = this.state.formterms;
      formData.definition = this.state.definition;
      SemanticService.create(formData).then((data) => {
        alert("Se creo con éxito el campo semántico");
        this.setState({formsubmit: true});
        this.props.history.push("/dictionary/semanticedit/"+data._id);
      });
    }
  }

  onCancel() {
    this.props.history.push("/dictionary/semantic");
  }

  termChange(selectedOption) {
    if (selectedOption !== null) {
      selectedOption.forEach((t) => {
        if (this.state.formterms.includes(t.value) == false) {
          this.state.formterms.push(t.value);        
        }
      });
    }
    else {
      this.state.formterms = [];
    }
  }


  render() {
    return (
      <>        
        <UserAccessValidator rol="admin_diccionario" />
        <div className="cev-container  cev-container--fluid  cev-grid__item cev-grid__item--fluid">
          <>
            <div className="cev-grid cev-grid--desktop cev-grid--ver cev-grid--ver-desktop cev-app">
              <AdminDictionaryMenu />
              <div className="cev-grid__item cev-grid__item--fluid cev-app__content">
                <React.Fragment>
                  <div className="col-xl-12">
                    <div className="cev-portlet cev-portlet--height-fluid">
                      <div className="cev-portlet__head">
                        <div className="cev-portlet__head-label">
                          Crear campo semántico
                        </div>
                      </div>
                      <div className="cev-portlet__body table-responsive">
                        <Form enctype="multipart/form-data"
                          schema={this.state.schema}
                          uiSchema={uiSchema}                                                    
                          onSubmit={({ formData }, e) =>
                            this.onSubmit(formData, e)
                          }
                          
                        >
                          <div class="form-group field field-string">
                            <label for="terms">Definición</label>
                            <JoditEditor                                  
                                  value={this.state.definition}
                                  config={this.state.config}
                                  tabIndex={1} // tabIndex of textarea                                  
                                  onChange={newContent => {this.state.definition=newContent}}
                            />                            
                          </div>
                          <br />
                           <br />
                          <div class="form-group field field-string">
                            <label for="terms">Términos</label>
                            <Select
                              isMulti
                              name="terms"
                              options={this.state.terms}
                              className="basic-multi-select"
                              classNamePrefix="select"
                              onChange={(selected) => { this.termChange(selected) }}
                              placeholder={this.state.placeholderterms}
                            />
                          </div>
                          <br />
                          <div>
                            <button type="submit" class="btn btn-primary mr-4">
                              Guardar
                            </button>
                            <button
                              type="button"
                              class="btn btn-secondary"
                              onClick={() => {
                                this.onCancel();
                              }}
                            >
                              Cancelar
                            </button>
                          </div>
                        </Form>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              </div>
            </div>
          </>
        </div>
      </>
    );
  }
}

const mapStateToProps = (store) => ({
  search: store.app.keyword,
  user: store.auth.user,
});

export default connect(mapStateToProps, app.actions)(SemanticAddPage);
