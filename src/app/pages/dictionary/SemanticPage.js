import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
import AdminDictionaryMenu from "../../components/organisms/AdminDictionaryMenu";
import * as app from "../../store/ducks/app.duck";
import * as SemanticService from "../../services/SemanticService";
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";
import { Button, Modal} from "react-bootstrap";
import * as TermService from "../../services/TermService";
//import moment from "moment";

const resourceHeader = [
  {
    title: "Campo semántico",
    data: "name",
  },
  {
    title: "Activo",
    data: "active",
  },
  {
    title: "Acciones",
    data: "_id",
    type: "action",
    width: "8%",
  },
];
class SemanticPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      semantics: [],
      show: false,
      closeModal:[],
      term: [],
      termshtml:'',
    };
    this.actionEvent = this.actionEvent.bind(this);
  }

  componentDidMount() {
    SemanticService.list("", this.state.page).then((data) => {
      const dataUser = data.map((s) => {
        return {
          ...{
            name: s.name,
            terms: s.terms,
            active: s.active ? "Activo" : "Inactivo",
            _id: s._id,
            accessLevel: s.accessLevel,
          },
        };
      });
      this.setState({ semantics: dataUser });
    });

    //Creo la funcion para cerrar el modal
    this.setState({
      closeModal: () => {
        this.setState({ show: false });
      }
    });

  }

  actionEvent({ action, row }) {
    switch (action.action) {
      case "edit":
        this.props.history.push("/dictionary/semanticedit/" + row._id);
        break;
      case "termino":
          this.setState({ show: true });          
          this.setState({termshtml: ''});
          this.setState({termshtml: row.terms.map((item,index) => <li><a href={"/dictionary/termedit/" + item._id}>{item.name}</a></li>)});        
          break;  
      default:
        break;
    }
  }

  onCrear() {
    this.props.history.push("/dictionary/semanticadd");
  }

  render() {
    let { semantics } = this.state;

    return (
      <>
        <UserAccessValidator rol="admin_diccionario" />
        <div className="cev-container  cev-container--fluid  cev-grid__item cev-grid__item--fluid">
          <>
            <div className="cev-grid cev-grid--desktop cev-grid--ver cev-grid--ver-desktop cev-app">
              <AdminDictionaryMenu />
              <div className="cev-grid__item cev-grid__item--fluid cev-app__content">
                <React.Fragment>
                  <div className="col-xl-12">
                    <div className="cev-portlet cev-portlet--height-fluid">
                      <div className="cev-portlet__head">
                        <div className="cev-portlet__head-label">
                          Gestión de campos semánticos
                        </div>
                        <div className="cev-portlet__head-label">
                          <button
                            onClick={() => {
                              this.onCrear();
                            }}
                            type="button"
                            className="btn btn-outline-primary"
                          >
                            Crear campo semántico
                          </button>{" "}
                        </div>
                      </div>                      
                      <div className="cev-portlet__body table-responsive">
                        <SimpleDataTable
                          id="simple-table"
                          columns={resourceHeader}                          
                          data={semantics}
                          ordering={false}
                          actions={[
                            {
                              action: "edit",
                              title: "Editar",
                              classLaIcon: "la la-edit",
                            },
                            {
                              action: "termino",
                              title: "Ver terminos",
                              classLaIcon: "la la-eye",
                            },
                          ]}
                          outputEvent={this.actionEvent}
                        ></SimpleDataTable>
                        <Modal show={this.state.show} onHide={this.state.closeModal}>
                          <Modal.Header closeButton>
                            <Modal.Title>Ver términos</Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                            <ul>
                              {this.state.termshtml}
                            </ul>                                  
                          </Modal.Body>
                          <Modal.Footer>
                            <Button variant="secondary" onClick={this.state.closeModal}>
                              Cerrar
                            </Button>
                          </Modal.Footer>
                        </Modal>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              </div>
            </div>
          </>
        </div>
      </>
    );
  }
}

const mapStateToProps = (store) => ({
  search: store.app.keyword,
  user: store.auth.user,
});

export default connect(mapStateToProps, app.actions)(SemanticPage);
