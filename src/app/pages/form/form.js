import React, { Component, useRef } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { getData, deleteData, postData } from "../../services/RequestService";
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";
import Swal from "sweetalert2";
import Editor from "@monaco-editor/react";
import FormFieldsBootstrap from '../../components/organisms/FormFieldsBootstrap';
import { Form } from "react-bootstrap";
import { submitAlert } from './../../services/utils'

import VisibilityIcon from '@material-ui/icons/Visibility';
import SaveIcon from '@material-ui/icons/Save';
import UndoIcon from '@material-ui/icons/Undo';

const listsHeader = [
    {
        title: 'Acciones',
        data: '_id',
        type: 'action',
        width: "12%"
    },
    {
        title: 'Nombre',
        data: 'name'
    },

    {
        title: 'label',
        data: 'label'
    },
    {
        title: 'Descipción',
        data: 'description'
    },

];

const formBase = [
    {
        id: 'name',
        defaultValue: null,
        label: 'Nombre',
        type: 'text',
        col: 12,
        placeholder: 'Ingrese el nombre del formulario',
        info: '',
        required: true
    },
    {
        id: 'label',
        defaultValue: null,
        label: 'Título',
        type: 'text',
        col: 12,
        placeholder: 'Ingrese un título para el formulario',
        info: '',
        required: true
    },
    {
        id: 'description',
        defaultValue: null,
        label: 'Descripción',
        type: 'text',
        col: 12,
        placeholder: 'Ingrese una descripción del formulario',
        info: '',
        required: true
    },
    {
        id: 'origin',
        defaultValue: null,
        label: 'Origen',
        type: 'select-multiple2-tags',
        col: 12,
        placeholder: 'Ingrese los origenes en los que se presetará el formulario',
        info: '',
        options: [],
        required: false
    },
]

const rowsPage = 25;

class FormEditor extends Component {

    constructor(props) {
        super(props);
        this.state = {
            optionSelection: false,
            lists: [],
            view: 'table',
            editorRef: null,
            formEditable: null,
            formUnsave: null,
            clone: null,
            originalClone: null,
            formConfig: null,
            listSelected: null,
            rowSelected: null
        };
        this.actionEvent = this.actionEvent.bind(this);
        this.handleEditorDidMount = this.handleEditorDidMount.bind(this);
        this.handleEditorWillMount = this.handleEditorWillMount.bind(this);
        this.handleEditorValidation = this.handleEditorValidation.bind(this);
        this.outputFieldBootstrap = this.outputFieldBootstrap.bind(this);
        this.atrasTable = this.atrasTable.bind(this);
        this.preview = this.preview.bind(this);
        this.save = this.save.bind(this);

    }
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async actionEvent({ action, row }) {
        console.log(action, row);
        if (action.action === 'code') {
            this.setState({ view: 'code', formEditable: null })
            this.setState({ formEditable: JSON.stringify(row, null, "\t") });
            this.setState({ formUnsave: JSON.stringify(row, null, "\t") });
            this.setState({ formConfig: null });
        }
        if (action.action === 'fork') {
            delete row._id
            delete row.origin
            this.setState({ view: 'fork', clone: row, originalClone: row })
        }
    }

    handleEditorDidMount(editor, monaco) {
        this.setState({ editorRef: editor });
        console.log("onMount: the editor instance:", editor);
        console.log("onMount: the monaco instance:", monaco)
    }

    handleEditorWillMount(monaco) {
        console.log("beforeMount: the monaco instance:", monaco);
    }

    handleEditorValidation(markers) {
        this.setState({ errorMarkers: markers.length > 0 ? markers : null });
    }

    redirect(path) {
        this.props.history.push(`${path}`);
    }

    preview() {

        if (!this.state.errorMarkers) {
            if (this.state.editorRef) {
                Swal.fire({
                    title: '¡Por favor espere!',
                    html: 'Cargando vista previa',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },
                });
                const newValue = this.state.editorRef.getValue();
                this.setState({ view: 'preview', formEditable: newValue })
                postData('/api/forms/getPreviewByForm', JSON.parse(newValue))
                    .then(formConfig => {
                        this.sleep(500)
                        Swal.close();
                        this.setState({ formConfig: formConfig });
                    })
            }
        } else {
            console.log(this.state.errorMarkers);
            Swal.fire({
                title: 'Error en el formato json',
                width: 600,
                heightAuto: false,
                html: `<ul>${this.state.errorMarkers.map((err) => (`<li>${err.message} in line ${err.startLineNumber}, column ${err.startColumn}</li>`)).reduce((a, b) => a + b, '')}</ul>`,
            });
        }
    }

    distintChanges() {
        return this.state.formUnsave !== this.state.formEditable;
    }

    atrasTable() {
        if (this.distintChanges()) {
            Swal.fire({
                title: '¿Está seguro que desea salir?',
                html: `No se guardarán los cambios efectuados`,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Salir'
            }).then((result) => {
                this.cargarListas();
                if (result) {
                    this.setState({ view: 'table' });
                }
            })
        } else {
            this.setState({ view: 'table' });
            this.cargarListas();
        }
    }

    save() {
        if (this.distintChanges()) {
            Swal.fire({
                title: 'Guardar cambios en el formulario',
                html: `Se guardarán los cambios realizados en el formulario`,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Guardar formulario'
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: '¿Esta seguro que desea guardar el formulario?',
                        html: `Se guardará el formulario, es posible que esto afecte los procesos de carga y de edición`,
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Guardar formulario'
                    }).then((result) => {
                        if (result) {
                            postData('/api/forms/updateForm', JSON.parse(this.state.formEditable))
                                .then((formConfig) => {
                                    Swal.fire(
                                        'Guardado',
                                        `Se guardó exitosamente el formulario ${formConfig.label}`,
                                        'success'
                                    )
                                    this.cargarListas()
                                    this.setState({ view: 'table' });

                                })
                        }
                    })
                }
            })
        } else {
            Swal.fire({
                title: 'No se puede guardar',
                html: `No se encontraron cambios para guardar`,
                icon: 'warning',
                confirmButtonText: 'Aceptar'
            })
        }

    }

    async cargarListas() {
        this.setState({ lists: [], optionSelection: false, listSelected: null });
        const lists = await getData('/api/forms');
        this.setState({ lists: lists, optionSelection: false, listSelected: null });
    }

    componentDidMount() {
        this.props.pageTitle(`Gestor de formularios`);
        this.cargarListas();
    }

    create(body) {
        return postData('/api/forms', body)
    }

    handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            this.setState({ validated: true });
        } else {
            submitAlert({
                option: 'create',
                type: 'clon de formulario',
                fn: this.create,
                data: this.state.clone,
                info: this.state.clone.name,
                gen: 'el',
                fnFinally: this.atrasTable
            })
        }
        event.preventDefault();
        event.stopPropagation();
    }

    outputFieldBootstrap(outputEvent) {
        const newForm = { ...this.state.clone, ...outputEvent.formData };
        this.setState({ clone: newForm });
    }

    render() {
        const { lists, formConfig, formEditable, view, validated, originalClone } = this.state;
        this.props.pageSubtitle(
            (view === 'table') ? 'Listado de formularios' : (view === 'preview') ? 'Vista previa formulario' : (view === 'fork') ? 'Clonar formulario desde: ' + originalClone.name : 'Edición de formulario');
        return (
            <>
                <div className="row content-view" style={{ "margin-left": (view === 'table') ? "0rem" : "6rem" }}>
                    <div className="view-tree-container height-80" >

                        <div className="view-tree-body" style={{ display: (view === 'table') ? "flex" : "none" }}>
                            {lists && (
                                <SimpleDataTable
                                    id='selection'
                                    columns={listsHeader}
                                    data={lists}
                                    actions={[
                                        { action: 'code', title: 'Editar', classLaIcon: 'fas fa-code' },
                                        { action: 'fork', title: 'Clonar', classLaIcon: 'fas fa-code-branch' },
                                    ]}
                                    outputEvent={this.actionEvent}
                                ></SimpleDataTable>
                            )}
                        </div>
                        <div className="view-tree-body  p-4" style={{ display: (view === 'fork') ? "flex" : "none" }}>
                            {formBase && (view === 'fork') && (
                                <Form
                                    noValidate
                                    validated={validated}
                                    onSubmit={e => this.handleSubmit(e)}>
                                    <Form.Row>
                                        <FormFieldsBootstrap
                                            formData={{}}
                                            preview={true}
                                            formConfig={formBase}
                                            outputEvent={this.outputFieldBootstrap}
                                        ></FormFieldsBootstrap>
                                    </Form.Row>
                                    <div className="d-flex flex-row-reverse">
                                        <button className="btn btn-primary ml-2" type="submit" value="submit">Clonar formulario</button>
                                    </div>
                                </Form>
                            )}
                        </div>
                        <div className="view-tree-body  p-0 m-0" style={{ display: (view === 'code') ? "flex" : "none" }}>
                            {formEditable && (
                                <Editor
                                    height="90vh"
                                    theme="vs-dark"
                                    defaultLanguage="json"
                                    defaultValue={formEditable}
                                    onMount={this.handleEditorDidMount}
                                    beforeMount={this.handleEditorWillMount}
                                    onValidate={this.handleEditorValidation}
                                ></Editor>
                            )}
                        </div>
                        <div className="view-tree-body p-4" style={{ display: (view === 'preview') ? "flex" : "none" }}>
                            {formConfig && (
                                <FormFieldsBootstrap
                                    formData={{}}
                                    preview={true}
                                    formConfig={formConfig}
                                    outputEvent={this.outputFieldBootstrap}
                                ></FormFieldsBootstrap>
                            )}
                        </div>
                    </div>
                </div >
                {
                    ['code'].includes(view) && (
                        <div className="toolbar-cataloging size-15">
                            <button type="button" className="btn-action warning" title="Atrás"
                                onClick={() => this.atrasTable()}>
                                <UndoIcon></UndoIcon>
                            </button>
                            <button type="button" className="btn-action warning" title="Vista Previa"
                                onClick={() => this.preview()}>
                                <VisibilityIcon></VisibilityIcon>
                            </button>
                        </div>
                    )
                }
                {
                    ['fork'].includes(view) && (
                        <div className="toolbar-cataloging size-15">
                            <button type="button" className="btn-action warning" title="Atrás"
                                onClick={() => this.atrasTable()}>
                                <UndoIcon></UndoIcon>
                            </button>
                        </div>
                    )
                }

                {
                    ['preview'].includes(view) && (
                        <div className="toolbar-cataloging size-15">
                            <button type="button" className="btn-action warning" title="Ver código"
                                onClick={() => this.setState({ view: 'code' })}>
                                <UndoIcon></UndoIcon>
                            </button>

                            <button type="button" className="btn-action warning" title="Guardar cambios"
                                onClick={() => this.save()}>
                                <SaveIcon></SaveIcon>
                            </button>
                        </div>
                    )
                }
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(FormEditor);
