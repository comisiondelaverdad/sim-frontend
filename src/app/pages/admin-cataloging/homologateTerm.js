import React, { Component } from "react";
import { connect } from "react-redux";
//import { toAbsoluteUrl } from "../../../theme/utils";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
import * as app from "../../store/ducks/app.duck";
import * as HomologateService from "../../services/HomologateService";
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";

//import moment from "moment";

const resourceHeader = [
  {
    title: 'Acciones',
    data: '_id',
    type: 'action',
    width: "8%"
  },
  {
    title: 'Campo origen',
    data: 'sourceField'
  },
  {
    title: 'Campo destino',
    data: 'destinationField',
  },
  {
    title: 'Valor',
    data: 'sourceValue'
  },
  {
    title: 'Valor destino',
    data: 'destinationValue',
  }
];
class HomologateTerm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      users: [],
    };
    this.actionEvent = this.actionEvent.bind(this);
  }

  componentDidMount() {
    HomologateService.list("", this.state.page).then((data) => {
      const dataTerm = data.map((u) => {
        return {
          ...{
            _id: u._id,
            sourceField: u.sourceField ? u.sourceField : 'Sin dato',
            destinationField: u.destinationField ? u.destinationField : 'Sin dato',
            sourceValue: u.sourceValue ? u.sourceValue : 'Sin dato',
            destinationValue: u.destinationValue ? u.destinationValue : 'Sin dato'
          },
        }
      });
      this.setState({ users: dataTerm })
    })
  }

  actionEvent({ action, row }) {
    switch (action.action) {
      case 'edit':
        this.props.history.push('/admin-cataloging/homologateTermEditPage/' + row._id)
        break;

      default:
        break;
    }
  }

  render() {
    let { users } = this.state;

    return (
      <>
        <UserAccessValidator rol="tesauro" />
        <div className="card-cev height-80">
          <div className="card-cev-title"> <span className="label">Tabla de homologación de campos</span> </div>
          <div className="card-cev-body">
            <SimpleDataTable
              id='simple-table'
              columns={resourceHeader}
              data={users}
              actions={[
                { action: 'edit', title: 'Editar', classLaIcon: 'la la-edit' },
              ]}
              outputEvent={this.actionEvent}
            ></SimpleDataTable>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(HomologateTerm);
