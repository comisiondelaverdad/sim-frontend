export const adminFilter = [
    {
        id: 'ident',
        defaultValue: null,
        label: 'Identificador',
        type: 'text',
        col: 3,
        placeholder: 'Identificador',
        info: '',
        required: false,
        hide: false,
        destiny: '"ident"'
    },
    {
        id: 'title',
        defaultValue: null,
        label: 'Título',
        type: 'text',
        col: 3,
        placeholder: 'Ingrese el título tal como aparece en el recurso',
        info: '',
        required: false,
        hide: false,
        destiny: '"title"'
    },
    {
        id: 'user',
        defaultValue: null,
        label: 'Usuario',
        type: 'select',
        col: 3,
        placeholder: 'Seleccione el usuario',
        required: false,
        options: [],
        destiny: '"user"'
    },{
        id: 'filename',
        defaultValue: null,
        label: 'Ruta',
        type: 'text',
        col: 3,
        placeholder: 'Ingrese la ruta original',
        required: false,
        destinity: 'filename'
    }
]