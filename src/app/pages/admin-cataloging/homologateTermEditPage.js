import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
//import { toAbsoluteUrl } from "../../../theme/utils";
import * as app from "../../store/ducks/app.duck";
import * as HomologateService from "../../services/HomologateService";

//import moment from "moment";

import Form from "react-jsonschema-form-bs4";



const schema = {

  "type": "object",
  "title": "Homologar Término",
  "properties": {
    "destinationField": {
      "title": "Campo destino",
      "type": "string",
      "enum": ["metadata.firstLevel.authors",
        "metadata.firstLevel.collaborators",
        "metadata.firstLevel.language",
        "metadata.firstLevel.rights",
        "metadata.firstLevel.population",
        "metadata.missionLevel.humanRights.conflictActors",
        "metadata.missionLevel.humanRights.incidentDescription",
        "metadata.missionLevel.humanRights.violenceType",
        "metadata.missionLevel.source.approach",
        "metadata.missionLevel.source.direction",
        "metadata.missionLevel.source.goal",
        "metadata.missionLevel.source.strategy",
        "metadata.missionLevel.target.approaches",
        "metadata.missionLevel.target.directions",
        "metadata.missionLevel.target.goals",
        "metadata.missionLevel.target.strategies",
        "metadata.missionLevel.target.territories"]
    },
    "destinationValue": {
      "title": "Editar término",
      "type": "string",
    },
  }
}

const uiSchema = {
  "status": {
    "ui:widget": "radio",
    "ui:options": {
      "inline": true
    }
  },
}


class AdminUsersEditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      termino: [],
    };
  }

  componentDidMount() {
    HomologateService.get(this.props.match.params.id).then((data) => {
      if (data.status)
        data.status = "Activo"
      else
        data.status = "Inactivo"
      this.setState({ termino: data })
    })
  }

  onSubmit(formData, e) {
    if (formData.destinationValue !== "" && formData.destinationValue !== undefined) {
      formData.status = formData.status === "Activo"
      HomologateService.update(this.props.match.params.id, formData).then((data) => {
        //console.log("El usuario es se guardó: ",data)
        alert("Término actualizado correctamente")
        this.props.history.push('/admin-cataloging/homologateTerm');
      })
    }
  }

  onCancel() {
    this.props.history.push('/admin-cataloging/homologateTerm');
  }

  render() {

    return (
      <>
        <UserAccessValidator rol="tesauro" />
        <div className="card-cev height-80">
          <div className="card-cev-title"> <span className="label">Homologar término</span> </div>
          <div className="card-cev-body">
            <table className="table table-striped table-hover table-bordered">
              <tbody>
                <tr>
                  <th scope="row">Campo</th>
                  <td>{this.state.termino.sourceField}</td>
                </tr>
                <tr>
                  <th scope="row">Valor original</th>
                  <td>{this.state.termino.sourceValue}</td>
                </tr>
              </tbody>
            </table>

            <Form
              schema={schema}
              uiSchema={uiSchema}
              formData={this.state.termino}
              onSubmit={({ formData }, e) => this.onSubmit(formData, e)}
            >
              <div>
                <button type="submit" class="btn btn-primary mr-4">Guardar</button>
                <button type="button" class="btn btn-secondary" onClick={() => { this.onCancel() }}>Cancelar</button>
              </div>
            </Form>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(AdminUsersEditPage);
