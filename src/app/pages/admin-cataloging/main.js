import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { serviceListFirstLevel, url, deleteResourceGroup, getMetadataRG, getRecursiveMetadataRG } from '../../services/ResourceGroupService';
import { serviceGetResourcesByFilter, deleteResource, toMoveResourceToResourcegroup } from '../../services/ResourcesService';
import { getCatalogadores } from '../../services/UsersService';
import moment from "moment";
import { is_array, findField } from '../../services/utils';
import { TREE_DEPTH } from "../../config/const";
import TreeView from '../../components/molecules/TreeView';
import Swal from "sweetalert2";
import { BehaviorSubject } from 'rxjs';
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";
import Paginacion from "./../../components/ui/Paginacion"
import { adminFilter } from "./formFilter";
import FormFieldsBootstrap from "./../../components/organisms/FormFieldsBootstrap";
import { toAbsoluteUrl } from "../../../theme/utils";
import DownloadResult from "../../components/molecules/DownloadResult";
import { from } from 'rxjs';
import { delay } from 'rxjs/operators';
import { getData } from "../../services/RequestService";
import CreateNewFolderIcon from '@mui/icons-material/CreateNewFolder';
import PostAddIcon from '@mui/icons-material/PostAdd';
import FolderDeleteIcon from '@mui/icons-material/FolderDelete';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import PreviewIcon from '@mui/icons-material/Preview';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import FolderIcon from '@mui/icons-material/Folder';
import EditIcon from '@mui/icons-material/Edit';

const filterSubject = new BehaviorSubject({});
const filter$ = filterSubject.asObservable();

const resourceHeader = [
    {
        title: 'Acciones',
        data: 'ident',
        type: 'action',
        width: "12%"
    },
    {
        title: 'Identificador',
        data: 'ident',
    },
    {
        title: 'Título',
        data: 'title'
    },
    {
        title: 'Fondo principal',
        data: 'resourcegroup'
    },
    {
        title: 'Última edición',
        data: 'history',
        type: 'list'
    },
];

const rowsPage = 25;

class MainAdminResources extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tree: undefined,
            optionsSelection: false,
            loading: false,
            users: [],
            selected: null,
            resourcesSelection: [],
            resources: null,
            resourcesGroups: null,
            father: null,
            from: 1,
            showTree: false,
            rowSelected: null,
            totalPages: null,
            currentPage: null,
            resourcegroupDestiny: null,
            adminFilterForm: null,
            showFilter: '',
            filter: {
                from: 1,
                size: rowsPage
            },
            formData: null,
            metadataExcel: [],
            export: "none",
            dataset: [],
            data: {},
            total: 0,
            statment: null,
            frmType: "FAE",
            datasetFondos: [],
            exportFondos: "none",
        };
        this.handleChangeSelected = this.handleChangeSelected.bind(this);
        this.boostrapEvents = this.boostrapEvents.bind(this);
        this.handlePagination = this.handlePagination.bind(this);
        this.actionEvent = this.actionEvent.bind(this);
        this.updateMyFilter = this.updateMyFilter.bind(this);
        this.deleteRG = this.deleteRG.bind(this);
        this.redirect = this.redirect.bind(this);
        this.showMetadata = this.showMetadata.bind(this);
        this.clearData = this.clearData.bind(this);
        this.deleteResource = this.deleteResource.bind(this);
        this.pageSelection = this.pageSelection.bind(this);
        this.deleteMultipleResource = this.deleteMultipleResource.bind(this);
        this.handleChangeResourceGroupDestiny = this.handleChangeResourceGroupDestiny.bind(this);
    }

    pageSelection(rows) {
        this.setState({ resourcesSelection: rows })

    }

    downloadMetadataFondos() {
        if (this.state.selected && this.state.selected.ResourceGroupId) {
            Swal.fire({
                title: "Esta descarga puede tomar tiempo, ¿desea continuar?",
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: 'Sí',
                cancelButtonText: 'No'
            }).then(async (result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "¡Por favor espere!",
                        html: "Exportando metadatos ...",
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            Swal.showLoading();
                        },
                    });
                    getRecursiveMetadataRG(this.state.selected.ResourceGroupId)
                        .then(async (data) => {
                            this.setState({ datasetFondos: data.fondos });
                            var frmStatement = await getData('/api/forms/statment/RESOURCEGROUPS_FAE');
                            this.setState({ frmType: "FONDOS" });
                            this.setState({ statment: frmStatement });
                            this.setState({ exportFondos: "actual" });
                            setTimeout(
                                function () {
                                    this.setState({ exportFondos: "none" });
                                }.bind(this),
                                1000
                            );

                            Swal.close();
                        })
                }
            });
        } else {
            Swal.fire({
                title: "Primero debe seleccionar un fondo.",
                confirmButtonText: 'Aceptar',
                showCancelButton: false
            });
        }
    }

    asking() {
        Swal.fire({
            title: "Exportar página actual o toda la búsqueda",
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Página actual`,
            cancelButtonText: "Todo",
            denyButtonText: `Todo`,
        }).then(async (result) => {
            let array_meta = [];
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                if (this.state.resources) {
                    this.state.resources.map((resource) => {
                        if (resource.metadataExcel) array_meta.push(resource.metadataExcel);
                    });
                    this.setState({ dataset: array_meta });
                }
            } else if (result.dismiss == "cancel") {

                let data = this.state.data;
                data.from = 1;
                data.size = this.state.total;
                Swal.fire({
                    title: "¡Por favor espere!",
                    html: "Exportando metadatos ...",
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                    },
                });
                const res = await serviceGetResourcesByFilter(data);
                Swal.close();
                const resources = res.resources.map((r) => {
                    return {
                        _id: r._id,
                        metadataExcel: r.metadataExcel,
                        metadata: r.metadataHTML,
                        ident: r.ident ? (typeof r.ident === "string" ? r.ident : "") : "",
                        rg: `${r.extra.resourcegroup.ident ? r.extra.resourcegroup.ident : ''}`,
                        title: r.metadata.firstLevel.title
                            ? r.metadata.firstLevel.title
                            : "",
                        description: r.metadata.firstLevel.description
                            ? r.metadata.firstLevel.description.length > 100
                                ? r.metadata.firstLevel.description.substring(0, 100)
                                : r.metadata.firstLevel.description
                            : "",
                        resourcegroup: `${r.extra ? r.extra.resourcegroup ? r.extra.resourcegroup.ident ?
                            r.extra.resourcegroup.ident : r.identifier ? r.identifier : '' : '' : ''} ${r.extra.resourcegroup.metadata.firstLevel.title ?
                                r.extra.resourcegroup.metadata.firstLevel.title : ''}`,
                        history: is_array(r.extra.history)
                            ? [
                                r.extra.history.map(
                                    (h) =>
                                        `${h.status === "created" ? "Creado" : "Actualizado"
                                        } el ${moment(h.date)
                                            .local()
                                            .format("LL")}`
                                )[r.extra.history.length - 1],
                            ]
                            : ["NO REGISTRA"],
                    };
                });

                resources.map((resource) => {
                    if (resource.metadataExcel) array_meta.push(resource.metadataExcel);

                });
                this.setState({ dataset: array_meta });
            }

            if (array_meta.length > 0) {
                var origin = array_meta[0].origin;
                var nameForm = '/api/forms/statment/RESOURCES_FAE';
                if (origin == 'CatalogacionFuentesInternas' || origin == 'CargasFuentesInternas'
                    || origin == 'VisualizacionesGeograficas' || origin == 'Visualizaciones') {
                    nameForm = '/api/forms/statment/RESOURCES_FAI';
                    this.setState({ frmType: "FAI" });
                } else if (origin == 'Objetos') {
                    nameForm = '/api/forms/statment/RESOURCES_OBJ';
                    this.setState({ frmType: "OBJ" });
                }
                else {
                    this.setState({ frmType: "FAE" });
                }
                var frmStatement = await getData(nameForm);
                this.setState({ statment: frmStatement });
                this.setState({ export: "actual" });
                setTimeout(
                    function () {
                        this.setState({ export: "none" });
                    }.bind(this),
                    1000
                );
            } else {
                Swal.fire({
                    title: 'No hay datos para exportar',
                    html: `<b>${this.state.selected.ResourceGroupId} - ${this.state.selected.text}</b>`,
                    icon: 'info',
                })
            }
        });

    }

    deleteResource() {
        Swal.fire({
            title: 'Se eliminará el recurso',
            text: this.state.rowSelected.title,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Eliminar recurso'
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    title: 'Esta operación no es reversible',
                    html: `Se eliminará el recurso<br><b>${this.state.rowSelected.ident} - ${this.state.rowSelected.title}</b>`,
                    icon: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Eliminar recurso'
                }).then((result) => {
                    if (result.value) {
                        Swal.fire({
                            title: '¡Por favor espere!',
                            html: 'Eliminando recurso ...',
                            allowOutsideClick: false,
                            onBeforeOpen: () => {
                                Swal.showLoading()
                            },
                        });
                        deleteResource(this.state.rowSelected.ident)
                            .then((data) => {
                                this.updateFilter();
                                Swal.close();
                                Swal.fire(
                                    'Eliminado',
                                    `Se eliminado exitosamente el recurso ${data.ident}`,
                                    'success'
                                )
                            })
                            .catch(err => {
                                Swal.close();
                                Swal.fire(
                                    'No se ha podido eliminar el recurso',
                                    `ERROR: ${err}`,
                                    'error'
                                )
                            })
                    }
                });
            }
        });
    }

    deleteMultipleResource() {
        Swal.fire({
            title: 'Se hará una eliminación masiva de recursos',
            html: `Se eliminarán<br>${this.state.resourcesSelection.length} recursos`,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Eliminar recursos'
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    title: 'Esta operación no es reversible',
                    html: `Se eliminarán<br>${this.state.resourcesSelection.length}recursos`,
                    icon: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Eliminar recursos'
                }).then(async (result) => {
                    if (result.value) {
                        Swal.fire({
                            title: 'Eliminando recursos',
                            html: `<b></b> de ${this.state.resourcesSelection.length} recursos eliminados`,
                            timerProgressBar: true,
                            onBeforeOpen: () => {
                                Swal.showLoading()
                            }
                        });
                        for (let i = 0; i < this.state.resourcesSelection.length; i++) {
                            const content = Swal.getContent();
                            if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                    b.textContent = i + 1;
                                }
                            }
                            const result = await deleteResource(this.state.resourcesSelection[i].ident);
                            if ((i + 1) === this.state.resourcesSelection.length) {
                                Swal.close();
                                Swal.fire({
                                    title: `Eliminación correcta`,
                                    text: `Se eliminaron correctamente ${this.state.resourcesSelection.length} recursos`,
                                    icon: 'success'
                                })
                                    .then(() => {
                                        this.setState({ optionsSelection: false, resourcesSelection: [] })
                                        filterSubject.next(this.state.filter)
                                    })
                            }
                        }
                    }
                });
            }
        });
    }

    deleteRG() {
        if (this.state.selected.children.length === 0) {
            Swal.fire({
                title: 'Eliminar fondo',
                html: `Se eliminará el fondo<br><b>${this.state.selected.ResourceGroupId} - ${this.state.selected.text}</b>`,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Eliminar fondo'
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: 'Esta operación no es reversible',
                        html: `Se eliminará el fondo<br><b>${this.state.selected.ResourceGroupId} - ${this.state.selected.text}</b>`,
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Eliminar fondo'
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire({
                                title: '¡Por favor espere!',
                                html: 'Eliminando fondo ...',
                                allowOutsideClick: false,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                },
                            });
                            deleteResourceGroup(this.state.selected.ResourceGroupId)
                                .then((data) => {
                                    if (data.error) {
                                        Swal.close();
                                        Swal.fire(
                                            'Error',
                                            `${data.error}`,
                                            'error'
                                        )
                                    } else {
                                        const ids = this.state.selected.path.ids.filter((a, i) => ((i > 0) && (i < this.state.selected.path.ids.length - 1)))
                                        const path = ids ? ids.length > 0 ? ids.reduce((a, b) => (a + '|' + b)) : '' : '';
                                        this.props.history.push(`/admin-cataloging/main/all`)

                                        this.loadResourceGroup();
                                        Swal.close();
                                        Swal.fire(
                                            'Eliminado',
                                            `Se eliminado exitosamente el fondo ${data.ident} - ${data.metadata.firstLevel.title}`,
                                            'success'
                                        )
                                    }
                                })
                                .catch(err => {
                                    Swal.close();
                                    Swal.fire(
                                        'No se ha podido eliminar el fondo',
                                        `ERROR: ${err}`,
                                        'error'
                                    )
                                })
                        }
                    });
                }
            });
        } else {
            Swal.fire({
                title: 'No se puede eliminar el fondo',
                html: `<b>${this.state.selected.ResourceGroupId} - ${this.state.selected.text}</b>`,
                icon: 'error',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Eliminar fondo'
            })
        }
    }

    actionEvent({ action, row }) {
        this.setState({ rowSelected: row });
        switch (action.action) {
            case 'edit':
                this.redirect(`/resources/resource/${row ? row.rg ? row.rg : '0' : '0'}/${row ? row.ident ? row.ident : '0' : '0'}`);
                break;
            case 'delete':
                this.deleteResource();
                break;
            case 'open':
                this.redirect(`/detail/${row ? row.ident ? row.ident : '0' : '0'}`);
                break;
            case 'metadata':
                Swal.fire({
                    title: 'Metadata',
                    width: 600,
                    heightAuto: false,
                    html: row.metadata,
                });
                break;
            default:
                break;
        }
    }

    redirect(path) {
        this.props.history.push(`${path}`);
    }

    componentDidMount() {
        //Cargando usuarios con rol catalogador
        const userField = findField(adminFilter, 'user');
        const { user } = this.props;
        if (user.roles.includes('catalogador') &&
            !user.roles.includes('catalogador_gestor')) {
            userField.options = [{ name: user.name, value: user._id }]
            this.setState({ adminFilterForm: adminFilter });
        } else {
            getCatalogadores()
                .then((users) => {
                    if (users.length > 0) {
                        userField.options = users.map((user) => {
                            return {
                                name: user.name,
                                value: user._id
                            }
                        })
                    }
                    this.setState({ adminFilterForm: adminFilter })
                })
        }


        this.props.pageTitle("Módulo de catalogación");
        this.loadResourceGroup(this.props.match.params.id);
        filter$.subscribe((data) => {
            if (JSON.stringify(data) !== '{}') {
                const { ident, title, user, ResourceGroupParentId, filename } = data;
                const users = (findField(adminFilter, 'user')).options;
                let showFilter = (ident ? ident + ' - ' : '') +
                    (title ? title + ' - ' : '') +
                    (user ? users.filter((u) => (u.value === user))[0].name + ' - ' : '') +
                    (filename ? filename + ' - ' : '') +
                    (ResourceGroupParentId ? ResourceGroupParentId.text + ' - ' : '');
                showFilter = showFilter.length > 70 ? showFilter.slice(0, 70) + ' ...' : showFilter;
                this.setState({ showFilter: showFilter })
                Swal.fire({
                    title: '¡Por favor espere!',
                    html: 'Cargando recursos ...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },
                });
                this.setState({ data });
                serviceGetResourcesByFilter(data)
                    .then((response) => {
                        const { total, totalPages, currentPage, resources } = response;
                        const resourcesData = resources.map((r) => {
                            return {
                                _id: r._id,
                                metadataExcel: r.metadataExcel,
                                metadata: r.metadataHTML,
                                ident: r.ident ? typeof r.ident === 'string' ? r.ident : '' : '',
                                rg: `${r.extra.resourcegroup.ident ? r.extra.resourcegroup.ident : ''}`, title: r.metadata.firstLevel.title ? r.metadata.firstLevel.title : '',
                                description: r.metadata.firstLevel.description ? r.metadata.firstLevel.description.length > 100 ?
                                    r.metadata.firstLevel.description.substring(0, 100) : r.metadata.firstLevel.description : '',
                                resourcegroup: `${r.extra.resourcegroup.ident ? r.extra.resourcegroup.ident : ''} ${r.extra.resourcegroup.metadata.firstLevel.title ? r.extra.resourcegroup.metadata.firstLevel.title : ''}`,
                                history: is_array(r.extra.history) ? [(r.extra.history.map((h) => (`${h.status === 'created' ? 'Creado' : 'Actualizado'} el ${moment(h.date).local().format('LL')}`)))[r.extra.history.length - 1]] : ["NO REGISTRA"]
                            }
                        })
                        this.props.pageSubtitle(`${resourcesData.length} recursos activos`);
                        this.setState({ resources: resourcesData, total: total, totalPages: totalPages, currentPage: currentPage });
                        Swal.close()
                    })
                    .catch((e) => {
                        console.log(e)
                        Swal.close()

                    })
            } else {
                this.setState({ showFilter: '' })

            }
        })
    }

    handlePagination(page) {
        const newFilter = {
            ...this.state.filter, ...{
                from: page
            }
        };
        this.setState({ filter: newFilter });
        filterSubject.next(newFilter)
    }

    updateFilter() {
        filterSubject.next(this.state.filter)
    }

    clearData() {
        filterSubject.next({});
        this.setState({ filter: {}, resources: [], total: null, formData: { ident: '', title: '', user: '' } })
    }

    handleChangeSelected(e, data) {
        if (data.node && data.path) {
            if (data.node.original.id) {
                this.setState({ selected: { ...data.node.original, ...{ path: data.path }, ...{ children: data.node.children } } });
                this.updateMyFilter({ ResourceGroupParentId: data.node.original });
            } else {
                this.setState({ selected: null });
            }
            if (data.node.id === 'j1_1') {
                this.setState({ selected: null });
                this.clearData();
            } else if (data.path.texts.length > 0) {
                const ids = data.path.ids.filter((a, i) => (i > 0))
                const path = ids ? ids.length > 0 ? ids.reduce((a, b) => (a + '|' + b)) : '' : '';
                this.props.history.push(`/admin-cataloging/main/${path}`)
            }
        }
    }

    handleChangeResourceGroupDestiny(e, data) {
        e.preventDefault();
        if (data.node) {
            if (data.node.original.id) {
                console.log({ ...data.node.original, ...{ path: data.path } })
                this.setState({ resourcegroupDestiny: { ...data.node.original, ...{ path: data.path } } });
            } else {
                this.setState({ resourcegroupDestiny: null });
            }
        }
    }

    toMoveResources() {
        const { selected, resourcegroupDestiny, resourcesSelection } = this.state;
        console.log({ selected, resourcegroupDestiny, resourcesSelection });

        Swal.fire({
            title: `Mover recursos`,
            html: `Se reubicarán ${resourcesSelection.length} recursos  al fondo ${resourcegroupDestiny.text}`,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: `Mover`
        })
            .then(async (result) => {
                if (result.value) {
                    Swal.fire({
                        title: 'Reubicando recursos...',
                        html: `<b></b> de ${resourcesSelection.length} recursos reubicados`,
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        }
                    });
                    for (let i = 0; i < resourcesSelection.length; i++) {
                        const body = {
                            resource: resourcesSelection[i].ident,
                            resourceGroupDestiny: resourcegroupDestiny.id,
                            resourceGroupOrigin: resourcesSelection[i].rg,
                        }
                        const content = Swal.getContent();
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = i + 1;
                            }
                        }
                        const result = await toMoveResourceToResourcegroup(body);
                        if ((i + 1) === resourcesSelection.length) {
                            Swal.close();
                            Swal.fire({
                                title: `Reubicación correcta`,
                                text: `Se reubicaron correctamente ${resourcesSelection.length} recursos`,
                                icon: 'success'
                            })
                                .then(() => {
                                    this.setState({ optionsSelection: false, resourcesSelection: [] })
                                    filterSubject.next(this.state.filter)
                                })
                        }
                    }

                } else {
                    Swal.close();
                }
            })
    }

    showMetadata() {
        getMetadataRG(this.state.selected.ResourceGroupId)
            .then((data) => {
                Swal.fire({
                    title: 'Metadata',
                    width: 600,
                    heightAuto: false,
                    html: data.metadataHTML,
                });
            })
    }


    updateMyFilter(filter) {
        if (filter.ResourceGroupParentId) {
            const newFilter = {
                ...{
                    from: 1,
                    size: rowsPage
                }, ...filter
            };
            this.setState({ filter: newFilter });
            filterSubject.next(newFilter)
        } else {
            const newFilter = {
                ...{
                    from: 1,
                    size: rowsPage
                }, ...filter
            };
            this.setState({ filter: newFilter });
            filterSubject.next(newFilter)
        }
    }

    boostrapEvents() {
        if (this.state.formData) {
            this.updateMyFilter(this.state.formData)
        }
    }


    loadResourceGroup(initial = null) {
        serviceListFirstLevel(initial)
            .then(
                (data) => {
                    let tree = {
                        "core": {
                            "check_callback": true,
                            "data": [
                                {
                                    "text": 'Fondos',
                                    "state": { "opened": true },
                                    "children": data,
                                }
                            ],
                        }
                    };
                    let treeLazy = {
                        "core": {
                            "data": {
                                "url": url,
                            },
                        },
                    }
                    this.setState({ records: tree, tree: treeLazy, rawData: data, showTree: true });
                },
                (error) => {
                    if (error === 403 || error === 401)
                        this.props.history.push("/logout");
                }
            )
    }




    render() {
        const {
            tree, showTree, selected, total, resources, currentPage, formData, showFilter, resourcesSelection, optionsSelection,
            adminFilterForm, resourcegroupDestiny, user } = this.state;
        return (
            <>
                <div className="row content-view" style={{ display: !optionsSelection ? "flex" : "none" }}>
                    {this.state.export == "actual" && (
                        <DownloadResult dataset={this.state.dataset} statment={this.state.statment} frmType={this.state.frmType} />
                    )}
                    {this.state.exportFondos == "actual" && (
                        <DownloadResult dataset={this.state.datasetFondos} statment={this.state.statment} frmType={this.state.frmType} />
                    )}
                    {adminFilterForm &&
                        <form>
                            <div className="filter-container">
                                <div className="col-sm-9">
                                    <FormFieldsBootstrap
                                        formData={formData}
                                        formConfig={adminFilterForm}
                                        outputEvent={(formData) => this.setState({ ...this.state.formData, ...formData })}
                                    ></FormFieldsBootstrap>
                                </div>
                                <div className="col-sm-1">
                                    <button type="button" className="btn-small ml-2 btn btn-primary" onClick={(data) => this.boostrapEvents()}> Buscar</button>
                                </div>
                            </div>
                        </form>
                    }
                    <div className="col-sm-4">
                        <div className="view-tree-container height-70">
                            <div className="view-tree-title">
                                <div className="circle-icon">
                                    <svg className="cv-small">
                                        <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-fondos")} className={"flia-azul"}></use>
                                    </svg>
                                </div>
                                <span className="label"> {selected ? (selected.text.length) > 30 ? selected.text.slice(0, 30) + ' ...' : selected.text : ''} </span>
                            </div>
                            <div className="view-tree-body">
                                {
                                    showTree &&
                                    <TreeView
                                        treeData={tree}
                                        newSelected={this.state.newSelected}
                                        onChange={(e, data) => this.handleChangeSelected(e, data)}
                                        depth={TREE_DEPTH}
                                        lazy={true}
                                    />
                                }
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <div className="view-tree-container height-70">
                            <div className="view-tree-title">
                                <div className="circle-icon">
                                    <svg className="cv-small">
                                        <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-documento")} className={"flia-azul"}></use>
                                    </svg>
                                </div>
                                <span className="label">{showFilter ? showFilter : 'No se ha definido criterio de busqueda'}</span>

                                <span className="child"> {total ? `Total: ${total}` : ''}</span>
                            </div>
                            <div className="view-tree-body">
                                <SimpleDataTable
                                    id='queries'
                                    outputFilterAndPagination={(e, data) => this.handlePagination(e, data)}
                                    columns={resourceHeader}
                                    data={resources}
                                    paging={false}
                                    itemsSelected={resourcesSelection}
                                    ordering={false}
                                    filter={false}
                                    info={false}
                                    isSelected={this.props.user.roles.includes('catalogador_gestor')}
                                    outputSelected={(rows) => this.pageSelection(rows)}
                                    eventSelect={() => this.setState({ optionsSelection: !optionsSelection })}
                                    actions={[...[
                                        { action: 'edit', title: 'Editar', classLaIcon: 'la la-edit' },
                                        { action: 'open', title: 'Ver detalle', classLaIcon: 'la la-link' },
                                        { action: 'metadata', title: 'Ver metadata', classLaIcon: 'la la-list' },
                                    ], ...showFilter ? (showFilter.includes(this.props.user.name) || this.props.user.roles.includes('catalogador_gestor')) ?
                                        [{ action: 'delete', title: 'Eliminar', classLaIcon: 'la la-trash' }]
                                        : [] : []]
                                    }
                                    outputEvent={this.actionEvent}
                                ></SimpleDataTable>
                            </div>
                        </div>
                        {
                            total > 0 && (
                                <Paginacion total={total} page={currentPage} step={rowsPage} callback={(data) => this.handlePagination(data)} />
                            )
                        }
                    </div>
                    {(selected || this.props.user.roles.includes('catalogador_gestor')) &&
                        <div className="toolbar-cataloging">
                            {this.props.user.roles.includes('catalogador_gestor') &&
                                <button type="button" className="btn-action" title="Reordenar fondos" onClick={() => this.redirect("/admin-cataloging/resourcegroups/all")}>
                                    <AccountTreeIcon></AccountTreeIcon>
                                </button>
                            }
                            {(this.props.user.roles.includes('catalogador') || this.props.user.roles.includes('catalogador_gestor')) && selected && (
                                <button type="button" className="btn-action" title="Ver metadata" onClick={() => this.showMetadata()}>
                                    <PreviewIcon></PreviewIcon>
                                </button>
                            )}
                            {(this.props.user.roles.includes('catalogador') || this.props.user.roles.includes('catalogador_gestor')) && selected && (
                                <button type="button" className="btn-action warning" title="Editar fondo" onClick={() => this.redirect(`/resourcegroupmanagment/edit/${selected.id ? selected.id : '0'}`)}>
                                    <EditIcon></EditIcon><FolderIcon></FolderIcon>

                                </button>
                            )}
                            {(this.props.user.roles.includes('catalogador') || this.props.user.roles.includes('catalogador_gestor')) && selected && (
                                <button type="button" className="btn-action warning" title="Nuevo fondo" onClick={() => this.redirect(`/resourcegroupmanagment/new/${selected.id ? selected.id : '0'}`)}>
                                    <CreateNewFolderIcon></CreateNewFolderIcon>
                                </button>
                            )}
                            {this.props.user.roles.includes('catalogador_gestor') && selected && (
                                <button type="button" className="btn-action danger" title="Eliminar fondo" onClick={() => this.deleteRG()}>
                                    <FolderDeleteIcon></FolderDeleteIcon>
                                </button>
                            )}
                            {(this.props.user.roles.includes('catalogador') || this.props.user.roles.includes('catalogador_gestor')) && selected && (
                                <button type="button" className="btn-action" title="Nuevo recurso" onClick={() => this.redirect(`/resources/resource/${selected.id ? selected.id : '0'}/0`)}>
                                    <PostAddIcon></PostAddIcon>
                                </button>
                            )}
                            {this.props.user.roles.includes("catalogador_gestor") && this.state.resources && (
                                <button
                                    onClick={() => this.asking()}
                                    type="button"
                                    className="btn-action"
                                    title="Exportar metadatos de recursos"
                                >
                                    <CloudDownloadIcon></CloudDownloadIcon>
                                    <InsertDriveFileIcon></InsertDriveFileIcon>

                                </button>
                            )}
                            {this.props.user.roles.includes("catalogador_gestor") && (
                                <button
                                    onClick={() => this.downloadMetadataFondos()}
                                    type="button"
                                    className="btn-action warning"
                                    title="Exportar metadatos de fondos"
                                >
                                    <CloudDownloadIcon></CloudDownloadIcon>
                                    <FolderIcon></FolderIcon>
                                </button>
                            )}
                        </div>
                    }
                </div>

                <div className="row content-view" style={{ display: optionsSelection ? "flex" : "none" }}>

                    <div className="filter-container d-flex justify-content-between">
                        <div className="d-flex">
                            {resourcesSelection.length > 0 &&
                                <button type="button" className="btn-small ml-2 mt-2 btn btn-secondary" onClick={() => this.setState({ optionsSelection: !optionsSelection })}> Atrás</button>
                            }
                        </div>
                        <div className="d-flex">
                            <button className="btn-small ml-2 mt-2 btn btn-danger" onClick={() => { this.deleteMultipleResource() }}>
                                <i className="flaticon-delete-1"></i> Eliminar masivamente {resourcesSelection.length} recursos </button>
                            <button className="btn-small ml-2 mt-2 btn btn-primary" onClick={() => { this.toMoveResources() }}>Enviar Recursos a -
                                {resourcegroupDestiny ? (resourcegroupDestiny.text.length) > 30 ? resourcegroupDestiny.text.slice(0, 30) + ' ...' : resourcegroupDestiny.text : ''}
                            </button>
                        </div>

                    </div>
                    <div className="col-sm-8">
                        <div className="view-tree-container height-70">
                            <div className="view-tree-title">
                                <div className="circle-icon">
                                    <svg className="cv-small">
                                        <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-documento")} className={"flia-azul"}></use>
                                    </svg>
                                </div>
                                <span className="label"> Recursos seleccionados</span>

                                <span className="child"> {resourcesSelection.length ? `Total: ${resourcesSelection.length}` : ''}</span>
                            </div>
                            <div className="view-tree-body">
                                <SimpleDataTable
                                    id='selection'
                                    columns={resourceHeader.slice(1)}
                                    data={resourcesSelection}
                                    outputEvent={this.actionEvent}
                                ></SimpleDataTable>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="view-tree-container height-70">
                            <div className="view-tree-title">
                                <div className="circle-icon">
                                    <svg className="cv-small">
                                        <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-fondos")} className={"flia-azul"}></use>
                                    </svg>
                                </div>

                                <span className="label">
                                    {resourcegroupDestiny ? (resourcegroupDestiny.text.length) > 30 ? resourcegroupDestiny.text.slice(0, 30) + ' ...' : resourcegroupDestiny.text : ''}
                                </span>
                            </div>
                            <div className="view-tree-body">
                                {
                                    showTree &&
                                    <TreeView
                                        treeData={tree}
                                        newSelected={this.state.newSelected}
                                        onChange={(e, data) => this.handleChangeResourceGroupDestiny(e, data)}
                                        depth={TREE_DEPTH}
                                        lazy={true}
                                    />
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}



const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(MainAdminResources);
