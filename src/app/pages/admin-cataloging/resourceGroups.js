import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { serviceListFirstLevel, url, toMoveResourceGroup } from '../../services/ResourceGroupService';
import {  TREE_DEPTH } from "../../config/const";
import TreeView from '../../components/molecules/TreeView';
import './../../styles/resource.scss';
import Swal from "sweetalert2";
import { errorAlert } from "./../../services/utils"
import { toAbsoluteUrl } from "../../../theme/utils";

class AdminResourceGroups extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tree: undefined,
            loading: false,
            resources: null,
            selected: null,
            father: null,
            from: 1,
            showTree: false
        };
        this.handleChangeSelected = this.handleChangeSelected.bind(this);
        this.handleChangeFather = this.handleChangeFather.bind(this);
        this.toMove = this.toMove.bind(this);
        this.goBack = this.goBack.bind(this);

    }

    componentDidMount() {
        this.props.pageTitle("Módulo de catalogación");
        this.loadResourceGroup(this.props.match.params.id);
    }

    toMove() {
        const { selected, father } = this.state;
        if (selected.ResourceGroupParentId === father.id) {
            Swal.fire({
                title: 'Error',
                html: `El fondo seleccionado ya es hijo de <b>${father.text}</b>`,
                icon: 'error',
            })
        } else if (selected.path.ids[1] !== father.path.ids[1]) {
            Swal.fire({
                title: 'Error',
                html: `No se puede mover fondos de <b>${selected.path.texts[1]}</b> a <b>${father.path.texts[1]}</b>`,
                icon: 'error',
            })
        } else {
            Swal.fire({
                title: `Mover fondo`,
                html: `Se moverá el fondo <b>${selected.text}</b> dentro del fondo <b>${father.text}</b>`,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonText: `Mover`
            })
                .then((result) => {
                    if (result.value) {
                        Swal.fire({
                            title: '¡Por favor espere!',
                            html: `Moviendo <b>${selected.text}</b> a <b>${father.text}</b>`,
                            allowOutsideClick: false,
                            onBeforeOpen: () => {
                                Swal.showLoading()
                            },
                        });
                        toMoveResourceGroup({ selected, father })
                            .then((response) => {
                                Swal.close();
                                Swal.fire({
                                    title: `Se ha movido exitosamente`,
                                    html: `El fondo ahora es hijo de <b>${response.metadata.firstLevel.title} - ${response.ResourceGroupParentId}</b>`,
                                    icon: 'success'
                                }).then((data2) => {
                                    this.loadResourceGroup();
                                    //   const buttonReturn = document.getElementById('return');
                                    //   buttonReturn.click();
                                })
                            })
                            .catch(err => {
                                Swal.close();
                                Swal.fire(
                                    `No se ha podido mover el fondo ${selected.text}`,
                                    `error: ${err}`,
                                    'error'
                                )
                            })
                    }
                })
        }
    }

    handleChangeSelected(e, data) {
        e.preventDefault();
        if (data.node && data.path) {
            if (data.node.original.id) {
                this.setState({ selected: { ...data.node.original, ...{ path: data.path } } });
            } else {
                this.setState({ selected: null });

            }
        }
    }

    handleChangeFather(e, data) {
        e.preventDefault();
        if (data.node) {
            if (data.node.original.id) {
                this.setState({ father: { ...data.node.original, ...{ path: data.path } } });
            } else {
                this.setState({ father: null });
            }
        }
    }

    loadResourceGroup(initial = null) {
        serviceListFirstLevel(initial)
            .then(
                (data) => {
                    let tree = {
                        "core": {
                            "check_callback": true,
                            "data": [
                                {
                                    "text": 'Fondos',
                                    "state": { "opened": true },
                                    "children": data,
                                }
                            ],
                        }
                    };
                    let treeLazy = {
                        "core": {
                            "data": {
                                "url": url,
                            },
                        },
                    }
                    this.setState({ records: tree, tree: treeLazy, rawData: data, showTree: true });
                },
                (error) => {
                    errorAlert({
                        error: error,
                        text: 'Se ha presentado un error al cargar la información',
                        confirmButtonText: 'Aceptar',
                        action: () => { }
                    })
                }
            )
    }

    goBack() {
        this.props.history.goBack()
    }


    render() {
        const { tree, showTree, selected, father } = this.state;
        return (
            <div className="row content-view" >
                <div className="col-sm-6">
                    <div className="view-tree-container height-60">
                        <div className="view-tree-title">
                            <div className="circle-icon">
                                <svg className="cv-small">
                                    <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-fondos")} className={"flia-azul"}></use>
                                </svg>
                            </div>
                            <span className="form-label">Seleccione fondo a mover</span>
                            <span className="child">{selected ? selected.id : ''}</span>
                        </div>
                        <div className="view-tree-body">
                            {
                                showTree &&
                                <TreeView
                                    treeData={tree}
                                    newSelected={this.state.newSelected}
                                    onChange={(e, data) => this.handleChangeSelected(e, data)}
                                    depth={TREE_DEPTH}
                                    lazy={true}
                                />
                            }
                        </div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="view-tree-container height-60">
                        <div className="view-tree-title">
                            <div className="circle-icon">
                                <svg className="cv-small">
                                    <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-fondos")} className={"flia-azul"}></use>
                                </svg>
                            </div>
                            <span className="form-label">Seleccione fondo padre</span>
                            <span className="child">{father ? father.id : ''}</span>
                        </div>
                        <div className="view-tree-body">
                            {showTree &&
                                <TreeView
                                    treeData={tree}
                                    newSelected={this.state.newSelected}
                                    onChange={(e, data) => this.handleChangeFather(e, data)}
                                    depth={TREE_DEPTH}
                                    lazy={true}
                                />
                            }
                        </div>
                    </div>
                </div>
                { 
                    <div className="col-sm-12">
                        <div className="content-destiny">
                            <div className="content-destiny-element">
                                <button className="btn btn-secondary ml-2" id="return" onClick={() => this.goBack()} >  Regresar </button>
                            </div>
                             { selected && father && (
                                <>
                                    {selected && (
                                        <div className="content-destiny-element">
                                            <span className="form-label">Fondo seleccionado</span>
                                            <span className="child">{selected.text} - {selected.id}</span>
                                        </div>
                                    )}
                                    {father && (
                                        <div className="content-destiny-element">
                                            <span className="form-label">Nuevo Fondo Padre</span>
                                            <span className="child">{father.text} - {father.id}</span>
                                        </div>
                                    )}
                                <div className="content-destiny-element">
                                    <button className="btn btn-primary ml-2" id="return" onClick={() => this.toMove()} > Mover </button>
                                </div>
                                </>
                             )}
                        </div>
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(AdminResourceGroups);

