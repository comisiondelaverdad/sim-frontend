import React, { Component } from 'react';
import { connect } from "react-redux";
import * as auth from "../../store/ducks/auth.duck";

class Google extends Component {

  componentDidMount() {
    const params = new URLSearchParams(window.location.search); 
    const token = params.get('token');
    //console.log(token);
    this.props.login(token);
  }

  render() {
    return (
      <div>
      </div>
    )
  }
}

export default connect(
  null,
  auth.actions
)(Google)