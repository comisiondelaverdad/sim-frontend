import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Login from "./Login";
import Google from "./Google";

export default function AuthPage() {
  return (
    <Switch>
      <Route path="/auth/login" component={Login} />
      <Route from="/auth" component={Google} />
      <Redirect to="/auth/login" />
    </Switch>
  );
}