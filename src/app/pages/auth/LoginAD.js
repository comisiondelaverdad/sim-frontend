import React, { useState } from 'react'
import { validRecaptchaFlow } from "../../services/Login"

export default function LoginAD() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    function encryt(data) {
        if (data)
            return Buffer.from(data).toString('base64')
        else
            return "00";
    }

    function onFormSubmit(e, status = false) {
        e.preventDefault();
        validRecaptchaFlow("/auth/loginAD?token=" + encryt(username) + "&prt=" + encryt(password), status);
    }

    return (
        <form >
            <div className="from-group">
                <div className="kt-login__options" style={{ marginBottom: "10px", padding: "5px" }}>
                    <input type="text" id="username" tabIndex={1} value={username} onChange={e => setUsername(e.target.value)} placeholder="Nombre de usuario" className="form-control" style={{ marginTop: "10px", padding: "5px", fontSize: "13px" }} />
                </div>
                <div className="kt-login__options" style={{ marginBottom: "10px", padding: "5px" }}>
                    <input type="password" tabIndex={2} value={password} onChange={e => setPassword(e.target.value)} placeholder="Contraseña" className="form-control" style={{ marginTop: "10px", padding: "5px", fontSize: "13px" }} />
                </div>
                <div className="kt-login__options" style={{ marginBottom: "20px", padding: "5px" }}>
                    <a data-action="submit" tabIndex={3} className="btn btn-primary kt-btn" onClick={e => onFormSubmit(e)} >Inicia sesión con directorio Activo</a>
                </div>
                <div className="login-button-hide" style={{ marginBottom: "20px", padding: "5px" }}>
                    <a data-action="submit" className="" tabIndex={4} onClick={e => onFormSubmit(e, true)} >.</a>
                </div>
            </div>
        </form>
    );
}