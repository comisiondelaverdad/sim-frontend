import React, { useEffect, useRef, useState } from 'react';
import { Link } from "react-router-dom"
import { useTranslation } from "react-i18next";
import { connect } from "react-redux";
import * as museo from "../../store/ducks/museo.duck"
import CEVLogoWhite from "../../sim-ui/assets/imgs/logo_comision.svg";
import CEVLogoBlue from "../../sim-ui/assets/imgs/logo_comision_3.svg";
import {
    makeStyles,
    FormControl,
    Box,
    Input,
    FilledInput,
    OutlinedInput,
    InputLabel,
    InputAdornment,
    FormHelperText,
    TextField,
    Typography,
    IconButton,
    Button,
    withWidth,
    isWidthDown
} from "@material-ui/core"
import { Alert } from '@material-ui/lab';


import { gsap } from 'gsap'
import { CSSPlugin } from 'gsap/CSSPlugin'
import { CenterFocusStrong, Visibility, VisibilityOff } from '@material-ui/icons';
import { validRecaptchaFlow } from "../../services/Login"



const encryt = (data) => {
    if (data)
        return Buffer.from(data).toString('base64')
    else
        return "00";
}

const decodeBase64 = (data) => {
    return Buffer.from(data, 'base64').toString('ascii');
}

gsap.registerPlugin(CSSPlugin)

export const col_explora = {
    main: '#f45353',
    dark: '#e02020'
}

export const col_conoce = {
    main: '#13c0c8',
    dark: '#019592'
}

export const col_crea = {
    main: '#ffc258',
    dark: '#e07714'
}


const useStyles = makeStyles((theme) => ({
    root: {
        position: 'fixed',
        display: 'flex',
        flexDirection: 'row',
        width: '100vw',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2a5080'
    },
    loginContainer: {
        width: '70vw',
        padding: '2rem',
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    loginContainerForm: {
        width: '500px',
        display: 'flex',
        flexDirection: 'column',
        height: '700px',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    loginContainerMobile: {
        width: '50vw',
        padding: '2rem',
        display: 'flex',
        flexDirection: 'column',
        height: '70vh',
        justifyContent: 'space-around',
        backgroundColor: '#fff'
    },
    input: {
        margin: '1rem',
        justifyContent: 'space-beetwen'
    },
    infoWhite: {
        height: '100vh',
        display: 'flex',
        textAlign: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        color: '#fff',
        margin: '2rem'
    },
    info: {
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    form: {
        display: 'flex',
        flexDirection: 'column'

    },
    buttons: {
        margin: '1rem',
        display: 'flex',
        justifyContent: 'space-around'
    }
}))

const alert = (error) => {
    return <Alert variant="outlined" severity="warning">{error}</Alert>
}

const Login = props => {
    const params = new URLSearchParams(window.location.search);
    const error = params.get('error') ? decodeBase64(params.get('error')) : '';

    const classes = useStyles()
    const [values, setValues] = useState({
        username: '',
        password: '',
        showPassword: false,
    });

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({
            ...values,
            showPassword: !values.showPassword,
        });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    const activeDirectory = () => () => {
        validRecaptchaFlow(`/auth/loginAD?token=${encryt(values.username)}&prt=${encryt(values.password)}`);

    };
    const corporative = () => () => {
        validRecaptchaFlow(`/auth/google`);
    };


    return (
        <div className={classes.root}>
            {isWidthDown('md', props.width) ? (
                <>
                    <Box className={classes.loginContainerMobile}>
                        <div className={classes.info}>
                            <img
                                src={CEVLogoBlue}
                                alt="CEV Logo"
                            />
                            <Typography
                                variant="h5"
                                className={classes.titleSection}
                            >¡Estás en el buscador de la verdad!</Typography>
                            <Typography
                                variant="h6"
                                className={classes.titleSection}
                            >Podrás buscar sobre las entrevistas, información de archivo e información bibliográfica.</Typography>
                        </div>
                        <div className={classes.form}>
                            <Typography
                                variant="h6"
                                className={classes.input}
                            >Iniciar sesión</Typography>
                            <FormControl variant="outlined" className={classes.input}>
                                <InputLabel htmlFor="outlined-adornment-username">Username</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-username"
                                    value={values.username}
                                    onChange={handleChange('username')}
                                    label="Username"
                                />
                            </FormControl>
                            <FormControl variant="outlined" className={classes.input}>
                                <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-password"
                                    type={values.showPassword ? 'text' : 'password'}
                                    value={values.password}
                                    onChange={handleChange('password')}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}
                                                edge="end"
                                            >
                                                {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                    label="Password"
                                />
                                {error && (<Alert variant="outlined" severity="warning">{error}</Alert>)}
                            </FormControl>


                            <Button variant="contained" color="primary" onClick={activeDirectory()} disabled={!(values.showPassword + values.username !== '')}  >
                                Directorio activo
                            </Button>
                        </div>
                        <Button variant="contained" color="secondary" onClick={corporative()} >
                            Correo corporativo
                        </Button>
                        <Typography
                            variant="h6"
                            className={classes.info}
                        >© 2020 Comisión de la Verdad</Typography>
                    </Box>
                </>
            ) : (
                <>
                    <div className={classes.infoWhite}>
                        <img
                            src={CEVLogoWhite}
                            alt="CEV Logo"
                        />
                        <div>
                            <Typography
                                variant="h5"
                                className={classes.titleSection}
                            >¡Estás en el buscador de la verdad!</Typography>
                            <Typography
                                variant="h6"
                                className={classes.titleSection}
                            >Podrás buscar sobre las entrevistas, información de archivo e información bibliográfica.</Typography>
                        </div>
                        <Typography
                            className={classes.titleSection}
                        >© 2020 Comisión de la Verdad</Typography>
                    </div>
                    < Box className={classes.loginContainer}>
                        <Box className={classes.loginContainerForm}>
                            <div className={classes.form}>
                                <Typography
                                    variant="h6"
                                    className={classes.input}
                                >Iniciar sesión</Typography>

                                <FormControl variant="outlined" className={classes.input}>
                                    <InputLabel htmlFor="outlined-adornment-username">Username</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-username"
                                        value={values.username}
                                        onChange={handleChange('username')}
                                        label="Username"
                                    />
                                </FormControl>

                                <FormControl variant="outlined" className={classes.input}>
                                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-password"
                                        type={values.showPassword ? 'text' : 'password'}
                                        value={values.password}
                                        onChange={handleChange('password')}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                >
                                                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                    />
                                </FormControl>

                                <div className={classes.buttons}>
                                    {error && (<Alert variant="outlined" severity="warning">{error}</Alert>)}

                                </div>
                            </div>
                            <div className={classes.buttons}>
                                <Button variant="contained" color="primary" onClick={activeDirectory()}  >
                                    Directorio activo
                                </Button>

                                <Button variant="contained" color="secondary" onClick={corporative()} >
                                    Correo corporativo
                                </Button>
                            </div>
                        </Box>
                    </Box>
                </>
            )}
        </div>
    )
}

const mapStateToProps = store => ({
    openLienzoCrea: store.museo.openLienzoCrea
});

export default connect(mapStateToProps, museo.actions)(withWidth()(Login))