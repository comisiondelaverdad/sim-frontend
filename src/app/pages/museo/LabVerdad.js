import React from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import MainLayout from "../../sim-ui/layout/MainLayout";
import Container from "@material-ui/core/Container";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import LabVerdadHeader from "../../components/organisms/LabVerdadHeader";
import LabVerdadMethodologicalTools from "../../components/organisms/LabVerdadMethodologicalTools";
import LabVerdadExperiences from "../../components/organisms/LabVerdadExperiences";
import LabVerdadCrea from "../../components/organisms/LabVerdadCrea";

const LabVerdad = (props) => {
  const [t, i18n] = useTranslation("common");

  return (
    <MainLayout>
      <Container>
        <LabVerdadHeader />
        <LabVerdadMethodologicalTools />
        <LabVerdadExperiences />
        <LabVerdadCrea />
      </Container>
    </MainLayout>
  );
};
const mapStateToProps = (store) => ({});

export default connect(mapStateToProps, app.actions)(LabVerdad);
