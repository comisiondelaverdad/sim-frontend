import React, { useEffect } from "react"
import { connect } from "react-redux"
import * as museo from "../../store/ducks/museo.duck"
import * as ColletionService from "../../services/BookmarkCollectionService";

const AutoSectionSelect = (props) => {
    const stringSplited = props.location.split('/')
    const section = stringSplited[2]

    props.setSection(section)

    useEffect(() => {
        ColletionService.getIdentCollection().then(
            (data) => {
                props.setUserBookmarks(data)
            },
            (error) => {
                console.log(error)
            }
        )
    })

    return (
        <>

        </>
    );
}


const mapStateToProps = store => ({
    currentSection: store.museo.currentSection
});

export default connect(mapStateToProps, museo.actions)(AutoSectionSelect);