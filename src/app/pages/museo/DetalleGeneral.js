import React, { useEffect } from "react";
import * as SearchService from "../../services/SearchService";
import MainLayout from "../../sim-ui/layout/MainLayout";

import { connect } from "react-redux";
import * as museo from "../../store/ducks/museo.duck";
import DetailRecord from "../../sim-ui/organisms/DetailResource";

const DetalleGeneral = (props) => {
  const [data, setData] = React.useState(null);
  const [idmongo, setIdmongo] = React.useState(null);

  useEffect(() => {
    // SearchService.serviceSingleMuseo(props.match.params.id).then(
    //   (data) => {
    //     setData(data.hits[0]._source.document);
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );

    if (props.match.params.id) setIdmongo(props.match.params.id);
  }, [props.match.params.id]);

  return (
    <MainLayout>
      <h1>Breadcrumb</h1>

      {idmongo ? <DetailRecord idmongo={idmongo} /> : ""}
    </MainLayout>
  );
};

const mapStateToProps = (store) => ({
  place: store.museo.currentSection,
});

export default connect(mapStateToProps, museo.actions)(DetalleGeneral);
