import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import * as SearchService from "../../services/SearchService";

import DetalleRecurso from "../../sim-ui/organisms/Detalle";

import MainLayout from "../../sim-ui/layout/MainLayout";
import Container from "@material-ui/core/Container";
import { connect } from "react-redux";
import * as museo from "../../store/ducks/museo.duck";
import TopFilters from "../../sim-ui/organisms/bloqueBusqueda/TopFilters";
import { useHistory } from "react-router-dom";
import DetailResource from "../../sim-ui/organisms/DetailResource";
import BreadCrumb from "../../sim-ui/organisms/BreadCrumb";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    height: "100%",
    maxHeight: "100%",
    width: "100%",
    overflow: "hidden",
  },
}))


const Detalle = (props) => {
  const [data, setData] = React.useState(null);
  const history = useHistory();
  const [previusUrl, setPreviusUrl] = React.useState(null)
  const [previusUrlLabel, setPreviusUrlLabel] = React.useState(null)
  const classes = useStyles();
  useEffect(() => {
    SearchService.serviceSingleMuseo(props.match.params.id).then(
      (data) => {
        setData(data.hits[0]._source.document);
      },
      (error) => {
        console.log(error);
      }
    );
    
  }, [props.match.params.id]);

  const goToSearch = (keyword) => {
    props.SetSearchToBack(keyword);
    history.push("/museo/explora/buscador");
  };
 

  return (
    <MainLayout newLayout = {true}>
      <div className={classes.wrapper}>
      {/* <TopFilters
        place={props.place}
        keyword={props.searchToBack}
        filtros={false}
        chips="hide"
        setKeyword={(keyword) => goToSearch(keyword)}
      /> */}
      {/* <Container>{data && <DetalleRecurso resource={data} />}</Container> */}
      {
        data && data.metadata && data.metadata.firstLevel && data.metadata.firstLevel.title && 
        <BreadCrumb previusUrl={previusUrl} previusUrlLabel={previusUrlLabel} title = { data.metadata.firstLevel.title} />

      }
      {data ? <DetailResource  setPreviusUrl={setPreviusUrl} setPreviusUrlLabel={setPreviusUrlLabel} resource={data} /> : ""}
      </div>
    </MainLayout>
  );
};

const mapStateToProps = (store) => ({
  place: store.museo.currentSection,
  searchToBack: store.museo.searchToBack,
});

export default connect(mapStateToProps, museo.actions)(Detalle);
