import React from 'react'
import HomeLayout from '../../sim-ui/organisms/home/HomeLayout'

const Home = props => {
    return (
        <HomeLayout />
    )
}

export default Home