import React, { useEffect } from 'react'
import { connect } from "react-redux"
import * as museo from "../../store/ducks/museo.duck"
import { Link } from "react-router-dom"

import ButtonMenuBase from "../../sim-ui/organisms/ButtonMenuBase"
import Container from '@material-ui/core/Container'
import MainLayout from "../../sim-ui/layout/MainLayout"
import explora from '../../sim-ui/assets/explora.jpg'
import crea from '../../sim-ui/assets/crea.jpg'
import conoce from '../../sim-ui/assets/conoce.jpg'

import HomeDesktop from '../../sim-ui/organisms/HomeDesktop'
import withWidth, { isWidthDown } from '@material-ui/core/withWidth'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    container: {
        height: '100%',
        padding: '40px 16px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    link: {
        width: '100%'
    }
}));


const Intro = (props) => {     
    const { setOpenLienzoCrea} = props
    setOpenLienzoCrea(false);
    useEffect(() => {
    setOpenLienzoCrea(false);

    
     }, []);
    const classes = useStyles();

    return (
        <>
        {isWidthDown('md', props.width) ?
            <MainLayout hideFooter={true}>
                <Container className={classes.container} > 
                    <Link to="/museo/explora/buscador" className={classes.link}>
                        <ButtonMenuBase
                        title=""
                        width='100%'
                        url={explora}
                        backgroundSize="contain"
                        height='25vh'
                        />
                    </Link>
                    <Link to="/museo/conoce" className={classes.link}>
                        <ButtonMenuBase
                        title=""
                        width='100%'
                        url={conoce}
                        backgroundSize="contain"
                        height='25vh'
                        
                        />
                    </Link>
                    {/* <Link to="/museo/crea" className={classes.link}>
                        <ButtonMenuBase
                        title=""
                        width='100%'
                        url={crea}
                        backgroundSize="contain"
                        height='25vh'
                        
                        />
                    </Link>   */}
                </Container>
            </MainLayout>
        : <HomeDesktop /> }
        </>
    );
  }

  const mapStateToProps = store => ({
    openLienzoCrea: store.museo.openLienzoCrea
  });
  
  export default connect(mapStateToProps,museo.actions)(withWidth()(Intro));
  
