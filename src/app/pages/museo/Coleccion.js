import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import * as museo from "../../store/ducks/museo.duck";
import VistaColeccion from "../../sim-ui/organisms/VistaColeccion";
import TopFilters from "../../sim-ui/organisms/bloqueBusqueda/TopFilters";
import Container from "@material-ui/core/Container";
import * as CollectionService from "../../services/CollectionService";
import MainLayout from "../../sim-ui/layout/MainLayout";

const Coleccion = (props) => {
  const [coleccion, setColeccion] = React.useState(null);
  const [carga, setCarga] = React.useState(false);
  const slug = props.match.params.id;

  useEffect(() => {
    if (!carga) {
      //   CollectionService.getCollectionById(slug).then(
      CollectionService.getCollectionBySlug(slug).then(
        (data) => {
          setColeccion(data);
          setCarga(true);
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }, []);

  return (
    <MainLayout>
      <TopFilters
        place={"Conoce"}
        keyword={null}
        temporalRange=""
        setTemporalRange=""
        dpto=""
        setDpto=""
        setKeyword=""
        total=""
        filtros={false}
      />
      {coleccion !== null && (
        <Container>
          <VistaColeccion coleccion={coleccion} />
        </Container>
      )}
    </MainLayout>
  );
};

const mapStateToProps = (store) => ({});

export default connect(mapStateToProps, museo.actions)(Coleccion);
