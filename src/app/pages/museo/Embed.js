import React, { useEffect, useState } from "react"
import VideoReproductorColeccion from '../../sim-ui/organisms/media/VideoReproductorColeccion';
import * as RecordsService from "../../services/RecordsService"


const getVideoType = (type) => {
    let formats = {
        ogg: 'video/ogg',
        mp4: 'video/mp4',
        webm: 'video/webm'
    }

    return formats[type]
}

const supportsVideoType = (type) => {
    let video;

    let formats = {
        ogg: 'video/ogg; codecs="theora"',
        mp4: 'video/mp4; codecs="avc1.42E01E"',
        webm: 'video/webm; codecs="vp8, vorbis"'
    };

    if (!video) {
        video = document.createElement('video')
    }

    return video.canPlayType(formats[type] || type);
}

const Embed = props => {
    const [tipo, setTipo] = useState(false);
    const [id, setId] = useState(false);
    const [archivo, setArchivo] = useState(false);

    useEffect(() => {
        setId(props.match.params.id)

        if (supportsVideoType('mp4')) setTipo('mp4')
        else if (supportsVideoType('ogg')) setTipo('ogv')
        else if (supportsVideoType('webm')) setTipo('webm')
    }, [props.match.params.id]);

    useEffect(() => {
        async function loadVideo() {
            const archivo = await RecordsService.serviceStream(id, tipo, 'Video')
            setArchivo(archivo)
        }
        if (tipo) {
            loadVideo()
        }
    }, [tipo])

    return (
        <>
            {archivo &&
                <VideoReproductorColeccion
                    color="white"
                    src={archivo}
                    embed={true}
                />
            }
        </>
    )
}

export default Embed