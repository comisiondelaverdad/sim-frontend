import React, { Component } from "react";
import { connect } from "react-redux";
import BubbleChart from "../../components/molecules/BubbleChart";
import FondoDropdown from "../../components/molecules/FondoDropdown";
import PeriodosDropdown from "../../components/molecules/PeriodosDropdown";
import OptionsDropdown from "../../components/molecules/OptionsDropdown";
import * as VizService from "../../services/VizService";
import { ORIGIN_MAP } from "../../config/const";
import { METADA_MAP } from "../../config/const";
import { PERIODOS_MAP } from "../../config/const";
import * as app from "../../store/ducks/app.duck";

class VisualMetadataPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      buckets: [],
      fondos: [],
      campos: [],
      periodos: [
        "1944-1958",
        "1958-1977",
        "1977-1990",
        "1991-2002",
        "2002-2016",
        "2016-2020"
      ],
      fondoSel: "Todos",
      campoSel: "Todos",
      periodoSel: "Todos"
    };

    this.getBuckets = this.getBuckets.bind(this);
    this.getFondos = this.getFondos.bind(this);
    this.getFields = this.getFields.bind(this);

    this.getFondos();
    this.getFields();
    this.getBuckets();
  }

  getFondos() {
    VizService.getFondos()
      .then(
        (data) => {
          let f = data.aggregations.fondos.buckets;
          this.setState({
            fondos: f
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }

  getFields() {
    VizService.mapping()
      .then(
        (data) => {
          let c = data;
          let indexName = Object.keys(c)[0];
          let properties = c[indexName].mappings.properties.record.properties

          let propertiesArray = [];
          for (var key in properties) {
            if (properties[key]["fields"]) {
              propertiesArray.push(key);
            }
          }

          this.setState({
            campos: propertiesArray
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }

  campoSelected = (campo) => {
    this.setState({ campoSel: campo }, () => { this.getBuckets() });
  };

  periodoSelected = (periodo) => {
    this.setState({ periodoSel: periodo }, () => { this.getBuckets() });
  };

  fondoSelected = (fondo) => {
    this.setState({ fondoSel: fondo }, () => { this.getBuckets() });

  };

  getBuckets() {
    let campoSel = this.state.campoSel === "Todos" ? "origin" : this.state.campoSel;

    let filters = {
      "origin": "",
      "type": campoSel,
      "min": "",
      "max": ""
    }

    if (this.state.fondoSel !== "Todos") {
      filters.origin = this.state.fondoSel;
    }

    if (this.state.periodoSel !== "Todos") {
      filters.min = this.state.periodoSel.split("-")[0] + "-01-01";
      filters.max = this.state.periodoSel.split("-")[1] + "-01-01";
    }

    VizService.tagCloud(filters, this.state.campoSel)
      .then(
        (data) => {
          console.log(data)
          let b = data.aggregations.tipo.buckets;

          this.setState({
            buckets: b
          })
        },
        (error) => {
          console.log(error)
        });
  }

  shouldComponentUpdate(newProps, newState) {
    if (newState !== this.state && newState.campos.length > 0 && newState.fondos.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  componentDidMount() {
    this.props.showSearch(true);
    this.props.showSearchTags(false);
    this.props.fromComponent("metadata-burble");
  }

  componentWillUnmount() {
    this.props.showSearch(false);
    this.props.showSearchTags(false);
    this.props.fromComponent("");
  }

  render() {

    return (
      <div className="cev-container cev-portlet  cev-margin-t-20">
        <div className="row cev-margin-t-20">
          <div className="col-auto">
            <FondoDropdown
              selected={this.state.fondoSel}
              fondos={this.state.fondos}
              fondoSelected={this.fondoSelected}
            />
          </div>
          <div className="col-auto">
            <OptionsDropdown
              selected={this.state.campoSel}
              campos={this.state.campos}
              campoSelected={this.campoSelected} />
          </div>
          <div className="col-auto">
            <PeriodosDropdown
              selected={this.state.periodoSel}
              periodos={this.state.periodos}
              periodoSelected={this.periodoSelected} />
          </div>
        </div>
        <div className="row cev-margin-t-10">
          <div className="col-auto">
            <p>
              Estás visualizando el campo <strong className="lowercase">
                {METADA_MAP[this.state.campoSel] ? METADA_MAP[this.state.campoSel] : this.state.campoSel}
              </strong>
              , en el fondo <strong className="lowercase">
                {ORIGIN_MAP[this.state.fondoSel] ? ORIGIN_MAP[this.state.fondoSel] : this.state.fondoSel}
              </strong>

              , durante el periodo <strong className="lowercase">
                {PERIODOS_MAP[this.state.periodoSel] ? PERIODOS_MAP[this.state.periodoSel] : this.state.periodoSel}
              </strong>
            </p>
          </div>
        </div>

        <div className="row cev-margin-t-20 cev-margin-b-20" >
          <div className="col-sm-12 bubble-container">
            <BubbleChart
              buckets={this.state.buckets}
              campoSel={this.state.campoSel}
              fondoSel={this.state.fondoSel}
              periodoSel={this.state.periodoSel}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(VisualMetadataPage);