import React, { Component } from "react"
import { connect } from "react-redux"
import * as app from "../../store/ducks/app.duck"
import * as VizService from "../../services/VizService";

import DualNGram from '../../components/organisms/VizOrganisms/DualNGram'
import EtiquetasBar from '../../components/organisms/EtiquetasBar'
import MapaBar from '../../components/organisms/MapaBar'
import TimeLineBar from '../../components/organisms/TimeLineBar'
import GrafoBarV2 from '../../components/organisms/GrafoBarV2'
import WordCloudBar from '../../components/organisms/WordCloudBar'
import Dona from '../../components/molecules/Dona'

import SideMenu from '../../components/ui/SideMenu'
import MainContent from '../../components/ui/MainContent'
import Card from '../../components/ui/Card'
import TabedCard from '../../components/ui/TabedCard'
import Paginacion from '../../components/ui/Paginacion'

import IconoGrafo from '../../components/ui/Iconos/Grafo'
import IconoCasos from '../../components/ui/Iconos/Casos'

import Result from "../../components/organisms/Result"

const NGramTabed = props => {
    return (
        <>
            <TabedCard
                active="arbol"
                intro=""
                list={[
                    {
                        slug: "arbol",
                        name: "Árbol",
                        component: <DualNGram filters={props.filters} changeFilter={props.changeFilter} location={props.location} />,
                        icono: <IconoGrafo />
                    },
                    {
                        slug: "barras",
                        name: "Barras",
                        component: <></>,
                        icono: <IconoGrafo />
                    }
                ]}
            />
        </>
    )

}

const GramaFilter = props => {
    return (
        <>
            <div className="gramaFilter">
                {
                    props.filters.map(m => {
                        return (
                            <>
                                <div className="filter">
                                    <span className="name">{m.nodo}</span>
                                    <span className="type">{m.gram}</span>
                                    <div className="close" onClick={(e) => props.removeFilter(m.nodo, 'grama')}></div>
                                </div>
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}

const EtiquetasFilter = props => {
    return (
        <>
            <div className="gramaFilter">
                {
                    props.filters.map(m => {
                        return (
                            <>
                                <div className="filter">
                                    <span className="name">{m.name}</span>
                                    <div className="close" onClick={(e) => props.removeFilter(m.name, 'etiquetas')}></div>
                                </div>
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}

const EntidadesFilter = props => {
    return (
        <>
            <div className="gramaFilter">
                {
                    props.filters.map(m => {
                        return (
                            <>
                                <div className="filter">
                                    <span className="name">{m}</span>
                                    <div className="close" onClick={(e) => props.removeFilter(m, 'entidades')}></div>
                                </div>
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}


class VisualExplorerPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            view: 'Patrones de texto',
            filterVisible: 'grama',
            totalResults: 0,
            page: 1,
            results: null,
            aggregations: null,
            filters: [
                {
                    tipo: 'grama',
                    filtros: []
                },
                {
                    tipo: 'etiquetas',
                    filtros: []
                },
                {
                    tipo: 'entidades',
                    filtros: []
                },
            ]
        }

        this.changeFilter = this.changeFilter.bind(this)
        this.removeFilter = this.removeFilter.bind(this)
        this.changeView = this.changeView.bind(this)
        this.changePage = this.changePage.bind(this)
        this.updateTotalResults = this.updateTotalResults.bind(this)
    }

    componentDidMount() {
        this.updateTotalResults()
    }

    updateTotalResults() {
        let params = {
            origin: "ModuloCaptura",
            page: this.state.page
        }

        if (this.state.filters !== undefined) {
            if (this.state.filters[0].filtros.length > 0) params.ngramFilter = this.state.filters[0].filtros
            if (this.state.filters[1].filtros.length > 0) params.etiquetasFilter = this.state.filters[1].filtros
            if (this.state.filters[2].filtros.length > 0) params.entidadesFilter = this.state.filters[2].filtros
        }

        let parent = this
        VizService.totalSearchHits(params)
            .then(data => {
                console.log('data', data)
                parent.setState({
                    totalResults: data.hits.total.value,
                    results: data.hits,
                    aggregations: data.aggregations
                })
            })
    }

    changeFilter(e, tipo) {
        if (tipo === 'grama') {
            let new_array = this.state.filters

            let item = new_array[0].filtros.find(d => d.nodo === e.nodo)
            if (item === undefined) new_array[0].filtros.push(e)

            this.setState({
                filters: new_array,
                filterVisible: tipo
            })
        } else if (tipo === 'etiquetas') {
            let new_array = this.state.filters
            let item = new_array[1].filtros.find(d => d.name === e.name)
            if (item === undefined) new_array[1].filtros.push(e)
            this.setState({
                filters: new_array,
                filterVisible: tipo
            })
        } else if (tipo === 'entidad') {
            let new_array = this.state.filters
            let item = new_array[2].filtros.find(d => d === e)
            if (item === undefined) new_array[2].filtros.push(e)
            this.setState({
                filters: new_array,
                filterVisible: tipo
            })
        }

        this.updateTotalResults()
    }

    removeFilter(e, tipo) {
        if (tipo === 'grama') {
            let new_array = this.state.filters

            let item = new_array[0].filtros.findIndex(d => d.nodo === e)
            if (item > -1) new_array[0].filtros.splice(item, 1)

            this.setState({
                filters: new_array
            })
        } else if (tipo === 'etiquetas') {
            let new_array = this.state.filters

            let item = new_array[1].filtros.findIndex(d => d.name === e)
            if (item > -1) new_array[1].filtros.splice(item, 1)

            this.setState({
                filters: new_array
            })
        } else if (tipo === 'entidades') {
            let new_array = this.state.filters

            let item = new_array[2].filtros.findIndex(d => d === e)
            if (item > -1) new_array[2].filtros.splice(item, 1)

            this.setState({
                filters: new_array
            })
        }

        this.updateTotalResults()
    }

    changeView(e) {
        this.setState({
            view: e,
            page: 1
        })
    }

    changePage(p) {
        this.setState({
            page: p,
            results: null
        }, this.updateTotalResults())
    }

    render() {
        return (
            <>
                <MainContent>

                    <Card>
                        <><b>Total resultados: </b>{this.state.totalResults}</>
                    </Card>

                    <TabedCard
                        active={this.state.filterVisible}
                        class="contraste"
                        intro="Filtros aplicados: "
                        list={[
                            {
                                slug: "grama",
                                name: "Nodos gramas",
                                component: <GramaFilter removeFilter={this.removeFilter} filters={this.state.filters[0].filtros} />,
                                icono: <IconoGrafo />
                            },
                            {
                                slug: "etiquetas",
                                name: "Etiquetas",
                                component: <EtiquetasFilter removeFilter={this.removeFilter} filters={this.state.filters[1].filtros} />,
                                icono: <IconoGrafo />
                            },
                            {
                                slug: "entidades",
                                name: "Entidades",
                                component: <EntidadesFilter removeFilter={this.removeFilter} filters={this.state.filters[2].filtros} />,
                                icono: <IconoGrafo />
                            }
                        ]}
                    />

                    {this.state.aggregations && this.state.view === 'Información de fichas' &&
                        <TabedCard
                            active="actores"
                            class="contraste"
                            intro="Información de los resultados"
                            list={[
                                {
                                    slug: "actores",
                                    name: "Actores del conflicto",
                                    component: <Card key={1}>
                                        <div className="donaContainer">
                                            <div className="dona">
                                                <Dona data={this.state.aggregations.conflictActors} />
                                            </div>
                                        </div>

                                    </Card>,
                                    icono: <IconoGrafo />
                                },
                                {
                                    slug: "macroTerritorio",
                                    name: "Macroterritorio",
                                    component: <Card key={2}>
                                        <div className="donaContainer">
                                            <div className="dona">
                                                <Dona data={this.state.aggregations.macroterritorio} />
                                            </div>
                                        </div>

                                    </Card>,
                                    icono: <IconoGrafo />
                                },
                                {
                                    slug: "tipoViolencia",
                                    name: "Tipo de violencia",
                                    component: <Card key={3}>
                                        <div className="donaContainer">
                                            <div className="dona">
                                                <Dona data={this.state.aggregations.violencia} />
                                            </div>
                                        </div>

                                    </Card>,
                                    icono: <IconoGrafo />
                                }
                            ]}
                        />
                    }

                    {this.state.results !== null &&
                        <>
                            {this.state.view === 'Resultados' && this.state.results.hits.map((result, idx) => (
                                <Result
                                    key={idx}
                                    record={result._source.resource}
                                    extra={{
                                        bookmarks: this.state.bookmarks,
                                        views: this.state.views,
                                        activeAccessRequests: this.state.activeAccessRequests,
                                        pendingAccessRequests: this.state.pendingAccessRequests,
                                        extra: result._source.extra,
                                        score: result._score,
                                        origin: result._source.extraResource.origin,
                                        found: { type: result._source.record.type, ident: result._source.record.ident }
                                    }}
                                />
                            ))}
                        </>
                    }


                    {this.state.view === 'Resultados' &&
                        <Paginacion total={this.state.totalResults} page={this.state.page} step={10} callback={this.changePage} />
                    }

                    {this.state.view === 'Patrones de texto' &&
                        <TabedCard
                            active="ngram"
                            class="contraste"
                            intro="Explorar patrones de texto: "
                            list={[
                                {
                                    slug: "ngram",
                                    name: "Ngram",
                                    component: <><Card>
                                        Un n-grama es una subsecuencia de n elementos de una secuencia dada.
                                    </Card><NGramTabed filters={this.state.filters} changeFilter={this.changeFilter} location={this.props.location} /></>,
                                    icono: <IconoGrafo />
                                },
                                {
                                    slug: "etiquetas",
                                    name: "Etiquetas analíticas",
                                    component: <><Card>

                                    </Card><EtiquetasBar key={`etiquetas_view_${JSON.stringify(this.state.filters)}`} filtros={this.state.filters} changeFilter={this.changeFilter} location={null} /></>,
                                    icono: <IconoCasos />
                                },
                                {
                                    slug: "grafo_entidades",
                                    name: "Grafo de entidades",
                                    component: <><Card>
                                        Esta visualización muestra las conexiones entre entidades a nivel de párrafo (inicio de la pregunta del entrevistador y final de la respuesta del entrevistado)
                                    </Card><GrafoBarV2 key={`grafo_view_${JSON.stringify(this.state.filters)}`} filtros={this.state.filters} changeFilter={this.changeFilter} location={this.props.location} /></>,
                                    icono: <IconoGrafo />
                                },
                                {
                                    slug: "mapa_entidades",
                                    name: "Mapa de entidades",
                                    component: <MapaBar filtros={this.state.filters} />,
                                    icono: <IconoGrafo />
                                },
                                {
                                    slug: "linea_tiempo",
                                    name: "Linea de tiempo",
                                    component: <TimeLineBar filtros={this.state.filters} />,
                                    icono: <IconoGrafo />
                                },
                                {
                                    slug: "nube",
                                    name: "Nube de entidades",
                                    component: <WordCloudBar key={`cloud_view_${JSON.stringify(this.state.filters)}`} filtros={this.state.filters} changeFilter={this.changeFilter} />,
                                    icono: <IconoGrafo />
                                }
                            ]}
                        />
                    }


                </MainContent>
                <SideMenu
                    callback={this.changeView}
                />
            </>
        )
    }
}

const mapStateToProps = store => ({
    search: store.app.filters.filters.keyword,
    user: store.auth.user,
    filtros: store.app.filters.filters
});

export default connect(
    mapStateToProps,
    app.actions
)(VisualExplorerPage);
