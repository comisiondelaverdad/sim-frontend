
import React, { Component } from "react";
import {connect} from "react-redux";
import BiGram from "../../components/molecules/BiGram";
import ThreeGram from "../../components/molecules/ThreeGram";
import FourGram from "../../components/molecules/FourGram";
import FondoDropdown from "../../components/molecules/FondoDropdown";
import GramsDropdown from "../../components/molecules/GramsDropdown";
import * as VizService from "../../services/VizService";
import { ORIGIN_MAP } from "../../config/const";
import Lottie from 'react-lottie';
import animationData from '../../../assets/viz-loading.json';
import * as app from "../../store/ducks/app.duck";

class VisualMetadataPage extends Component {
  constructor(props){
    super(props)
    this.state = {
      buckets: null,
      fondos: [],
      campos: [],
      fondoSel: "Todos",
      gramSel: "bigram",
      campoSel: "origin",
      periodoSel: "Todos",
      loading: true
    };

    this.getFondos = this.getFondos.bind(this);
    this.getFields = this.getFields.bind(this);
    this.gramSelected = this.gramSelected.bind(this);
    this.fondoSelected = this.fondoSelected.bind(this);
    this.getBuckets = this.getBuckets.bind(this);

    this.getFondos();
    this.getFields();
    this.getBuckets();
  }


  getBuckets(){
      let parent = this
      let params = {
          ngram: parent.state.gramSel
      }
      if(parent.state.fondoSel !== 'Todos'){
        params.origin = parent.state.fondoSel
      }

      console.log(params)

      this.setState({
        loading:true
    })

    VizService.getNgram(params)
    .then(
      (data) => {
        this.setState({
            buckets: data.aggregations.tipo.ngram.total.buckets,
            loading:false
        })

      },
      (error) => {
        console.log(error)
      }
    )
  }

  getFondos(){
    VizService.getFondos()
      .then(
        (data) => {
          let f = data.aggregations.fondos.buckets;
          this.setState({
            fondos: f
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }

  getFields(){
    VizService.mapping()
      .then(
        (data) => {
          let c = data;
          console.log(c)
          let indexName =Object.keys(c)[0];
          let properties =  c[indexName].mappings.properties.record.properties

          let propertiesArray = [];
          for (var key in properties) {
            if (properties[key]["fields"]) {
              propertiesArray.push(key);
            }
          }

          this.setState({
            campos: propertiesArray
          })
        },
        (error) => {
          console.log(error)
        }
      )
  }

  campoSelected = (campo) => {
    this.setState ({campoSel: campo}, () => {this.getBuckets()});
  };

  periodoSelected = (periodo) => {
    this.setState ({periodoSel: periodo}, () => {this.getBuckets()});
  };
  
  fondoSelected = (fondo) => {
    this.setState ({fondoSel: fondo, buckets: null}, () => {this.getBuckets()});

  };

  gramSelected = (fondo) => {
    this.setState ({gramSel: fondo, buckets: null}, () => {this.getBuckets()});
  };

  componentDidMount() {
    this.props.showSearch(true);
    this.props.showSearchTags(false);
    this.props.fromComponent("metadata-ngram");
  }

  componentWillUnmount() {
    this.props.showSearch(false);
    this.props.showSearchTags(false);
    this.props.fromComponent("");
  }
  
  render() {
    
    return (
      <div className="cev-container cev-portlet  cev-margin-t-20">
          <div className="row cev-margin-t-20">
              <div className="col-auto">
                <FondoDropdown 
                  fondos={this.state.fondos}
                  fondoSelected={this.fondoSelected}
                  />
              </div>
              <div className="col-auto">
                <GramsDropdown 
                  fondos={[
                      {
                      key:'bigram'
                    },
                    {
                        key:'3gram'
                      },
                      {
                        key:'4gram'
                      }
                    ]}
                  fondoSelected={this.gramSelected}
                  />
              </div>
          </div>

          <div className="row cev-margin-t-10">
            <div className="col-auto">
              <p>
                Estás el fondo <strong className="lowercase">
                  {ORIGIN_MAP[this.state.fondoSel] ? ORIGIN_MAP[this.state.fondoSel] : this.state.fondoSel}
                </strong>

                , con la vista <strong className="lowercase">
                  {this.state.gramSel ? this.state.gramSel : this.state.gramSel} 
                </strong>
              </p>
            </div>
          </div>

          <div class="wordcloudBar"><div class="cloudContainer">
              {this.state.loading && 
                <div className="loading_viz">
                  <Lottie height={150} width={150} options={
                    {loop: true,
                    autoplay: true,
                    animationData: animationData,
                    rendererSettings: {
                      preserveAspectRatio: 'xMidYMid slice'
                    }}
                  }/>
                </div>
              }
              {this.state.buckets && this.state.gramSel === 'bigram' &&
                <BiGram buckets={this.state.buckets}/>
              }
              {this.state.buckets && this.state.gramSel === '3gram' &&
                <ThreeGram buckets={this.state.buckets}/>
              }
              {this.state.buckets && this.state.gramSel === '4gram' &&
                <FourGram buckets={this.state.buckets}/>
              }
              </div>
          </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(VisualMetadataPage);
