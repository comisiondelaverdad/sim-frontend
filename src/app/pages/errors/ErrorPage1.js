import React from "react";
import { toAbsoluteUrl } from "../../../theme/utils";

export default function ErrorPage1() {
  return (
    <>
      <div className="cev-grid cev-grid--ver cev-grid--root">
        <div
          className="cev-grid__item cev-grid__item--fluid cev-grid cev-error-v1"
          style={{
            backgroundImage: `url(${toAbsoluteUrl(
              "/media/error/bg1.jpg"
            )})`
          }}
        >
          <div className="cev-error-v1__container">
            <h1 className="cev-error-v1__number">404</h1>
            <p className="cev-error-v1__desc">OOPS! Something went wrong here</p>
          </div>
        </div>
      </div>
    </>
  );
}
