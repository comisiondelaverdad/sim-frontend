import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import * as FormService from "../../services/FormService";
import AdvancedSearch from "../../components/organisms/AdvancedSearch";
import ResourceGroupSelect from "../../components/organisms/ResourceGroupSelect";
import * as app from "../../store/ducks/app.duck";
import { errorAlert } from "../../services/utils";

class SearchBar extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      keyword : this.props.from === "search" ? props.searchFilters.filters ? props.searchFilters.filters.keyword : "" : "",
      advancedSearchConfig : undefined,
      mapQuery: undefined,
      map: "",
      resourcesGroup: {},
      currentPosition : this.props.from === "search" ? props.searchFilters.filters ? props.searchFilters.filters.keyword.length : 0 : 0,
      highlight: 1
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleBur = this.handleBur.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOnKeyDown = this.handleOnKeyDown.bind(this);
  }

  componentDidMount(){
    let highlight = this.props.viewTags ? 2 : 1;
    FormService.getAdvancedSearchConfig()
      .then(
        (data) => {
          this.setState({advancedSearchConfig: data, highlight: highlight})
        },
        (error) => {
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar la información',
            confirmButtonText: 'Aceptar',
            action: ()=> {}
          })
        }          
      )
  }

  componentDidUpdate(prevProps) {
    let highlight = this.props.viewTags ? 2 : 1;
    if((prevProps.from !== this.props.from) || (prevProps.searchFilters.filters && this.props.searchFilters.filters && prevProps.searchFilters.filters.keyword !== this.props.searchFilters.filters.keyword)){
      if(this.props.from === "search"){
        this.setState({keyword: this.props.searchFilters.filters.keyword, highlight: highlight});
      }
      else{
        this.setState({keyword: "", highlight: 1, currentPosition: 0});
      }
    }
  }

  handleChange(evt){
    if(evt.target.name === "highlight"){
      this.props.showSearchTags(evt.target.checked);
    }
    else{
      this.setState({keyword: evt.target.value});
    }
  }

  handleBur(evt){
    let currentPosition = evt.target.selectionStart;
    this.setState({currentPosition: currentPosition});
  }

  handleOnKeyDown(evt){
    if(evt.which === 8){
      let query = evt.target.value;
      let position = evt.target.selectionStart;
      let nextChar = query.substring(position,position+1);
      let currentChar = query.substring(position-1,position);
      if(nextChar === " " || position === query.length || currentChar === " "){
        evt.preventDefault();
        if(currentChar === "\""){
          let queryFirstPart = query.substring(0,position-1).trim();
          let queryLastPart = query.substring(position,query.length).trim();
          let lastComma = queryFirstPart.lastIndexOf("\"");
          let lastSpace = queryFirstPart.substring(0,lastComma).lastIndexOf(" ");
          let newQuery = queryLastPart;
          if(lastSpace){
            newQuery = (queryFirstPart.substring(0,lastSpace).trim() + " " + queryLastPart).trim();
          }
          evt.target.value = newQuery.trim();
          this.setState({keyword: newQuery});
        }
        else{
          let queryFirstPart = query.substring(0,position).trim();
          let queryLastPart = query.substring(position,query.length).trim();
          let lastSpace = queryFirstPart.lastIndexOf(" ");
          let newQuery = queryLastPart;
          if(lastSpace){
            newQuery = (queryFirstPart.substring(0,lastSpace).trim() + " " + queryLastPart).trim();
          }
          evt.target.value = newQuery.trim();
          this.setState({keyword: newQuery});
        }
      }
    }
  }

  handleSubmit(evt){
    evt.preventDefault();
    let resourceGroup = this.state.resourcesGroup.children && this.state.resourcesGroup.children.length > 0 ? this.state.resourcesGroup.children.join(",") : "";
    let resourceGroupText = this.state.resourcesGroup.text && this.state.resourcesGroup.text.length > 0 ? this.state.resourcesGroup.text.join(",") : "";
    let keyword = "";
    let from = "from=" + encodeURIComponent(1);
    if(this.state.keyword){
      keyword = this.state.keyword.split(" ");
      if(keyword[keyword.length-1] === "OR" || keyword[keyword.length-1] === "AND"){
        keyword.pop();
      }
      keyword = keyword.join(" ");
    }

    this.props.fromComponent("search");
    this.props.keyword(keyword);
    this.props.filters({
      "filters":{
        "keyword": keyword,
        "resourceGroup": {
          "resourceGroupText": resourceGroupText,
          "resourceGroupIDS": resourceGroup
        },
        "mapPolygon": this.state.map
      }
    });

    if(keyword){
      this.props.history.push("/search?q="+encodeURIComponent(keyword) + "&" + from);
    }
    else{
      this.props.history.push("/search?" + from);
    }

  }

  addQuery(q){
    let currentPosition = this.state.currentPosition;
    let keyword = this.state.keyword;
    let before = keyword.substring(currentPosition - 1, currentPosition);
    let after = keyword.substring(currentPosition, currentPosition + 1);

    if(q === "AND" || q === "OR"){
      if(currentPosition === 0){
        q = "";
      }
      if(before === " " || after === " " || currentPosition === keyword.length){
        let valueANDLeft = keyword.substring(currentPosition - 4, currentPosition).trim();
        let valueORLeft = keyword.substring(currentPosition - 3, currentPosition).trim();
        let valueANDRight = keyword.substring(currentPosition, currentPosition + 4).trim();
        let valueORRight = keyword.substring(currentPosition, currentPosition + 3).trim();
        if(valueANDLeft === "AND" || valueORLeft === "OR" || valueANDRight === "AND" || valueORRight === "OR"){
          q = "";
        }
      }
      if(after !== " " && before !== " " && currentPosition<keyword.length){
        q = "";
      }
    }

    if(q){
      if(keyword === ""){
        keyword = q;
      }
      else{
        if(currentPosition === 0){
          keyword = q.trim() + " " + keyword.trim();
        }
        if(currentPosition === keyword.length){
          keyword = keyword.trim() + " " + q.trim()
        }
        if(currentPosition < keyword.length && (after === " " || before === " ")){
          let firstPart = keyword.substring(0,currentPosition).trim();
          let secondPart = keyword.substring(currentPosition).trim();
          keyword = firstPart + " " + q.trim() + " " + secondPart;
        }
      }
      this.setState({keyword:keyword});
    }
  }
  
  addMap(geojson, q){
    this.setState({"mapQuery": geojson, "map": q})
  }

  addResourceGroup(q){
    this.setState({"resourcesGroup": q})
  }

  render(){
    const { 
      showSearchBar
    } = this.props;

    const {
      advancedSearchConfig,
      keyword
    } = this.state;

    let highlight = this.props.showTags ? 2 : 1;

    return (
        <>
        {showSearchBar &&
          <div className="search-container" id="cev_subheader_search">
            {advancedSearchConfig && <AdvancedSearch dashboard={true} className="advanced-search-zindex" config={advancedSearchConfig} addQuery={(q)=>this.addQuery(q)} addMap={(geojson, q)=>this.addMap(geojson, q)} />}
            <ResourceGroupSelect addResourceGroup={(q)=>this.addResourceGroup(q)} />
            <form className="form-inline" onSubmit={this.handleSubmit}>
              <div className="input-group input-group-sm input-group-solid">
                <input onKeyDown={this.handleOnKeyDown} onBlur={this.handleBur} type="text" className="form-control cev-sub-search" id="cev_subheader_search_form" placeholder="Buscar" value={keyword} onChange={this.handleChange} />
                <button id="cev_login_signin_submit" type="submit" className="btn btn-primary btn-elevate kt-login__btn-primary">Buscar</button>              
              </div>
              {this.props.from === "search" &&
                <div role="group">
                  <div className="cev-checkbox-bar">
                    <label>
                      <input type="checkbox" name="highlight" value="2" checked={highlight === 2} onChange={this.handleChange} />Ver etiquetado
                      <span className="checkmark"></span>
                    </label>
                  </div>
                </div>
              }
            </form>
          </div>
        }
        </>
    );
  }
}

const mapStateToProps = store => ({
  showSearchBar: store.app.showSearch,
  searchFilters: store.app.filters,
  showTags: store.app.showSearchTags,
  from: store.app.fromComponent
});

export default withRouter(connect(mapStateToProps,app.actions)(SearchBar));