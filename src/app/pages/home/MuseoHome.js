import React, { Suspense } from "react";
import { useLocation, Switch } from "react-router-dom";
import { LayoutSplashScreen } from "../../../theme/LayoutContext";
import AutoLogout from "./../../components/atoms/AutoLogout";
import  ProtectedRoute from "./protectedRoute";
import AutoSectionSelect from '../museo/AutoSectionSelect'
// import Explora from "../museo/Explora"
import Explora from '../../sim-ui/organisms/bloqueBusqueda/Explora'
import GestorNarrativasIntro from "../museo/GestorNarrativasIntro"
import GestorNarrativas from "../museo/GestorNarrativas"
import Intro from "../museo/Intro"
import Home from "../museo/Home"
import Embed from "../museo/Embed"
import Crea from "../museo/Crea"
import Diccionario from "../dictionary/HomeDiccionario"
import DiccionarioTerminos from "../dictionary/TermsDiccionario"
import DiccionarioBusqueda from "../dictionary/BusquedaDiccionario"
import DiccionarioCampos from "../dictionary/FieldsDiccionario"
import TermDiccionario from "../dictionary/TermDiccionario"
import CardDiccionario from "../dictionary/CardDiccionario"
import FieldDiccionario from "../dictionary/FieldDiccionario"
import Conoce from "../museo/Conoce"
import Coleccion from "../museo/Coleccion"
import LabVerdad from "../museo/LabVerdad"
import MiBiblioteca from "../museo/MiBiblioteca"
import Detalle from "../museo/Detalle"
import { makeStyles, ThemeProvider, createMuiTheme } from "@material-ui/core"
import ViewMicrosite from "../../sim-ui/organisms/ViewMicrosite";
import DetalleGeneral from "../museo/DetalleGeneral";




function getSection(location) {
    const stringSplited = location.split('/')
    const section = stringSplited[2]
    return section
}

const MuseoHome = props => {
    const location = useLocation()
    const section = getSection(location.pathname)
    const col_explora = {
        main: '#f45353',
        dark: '#e02020'
    }

    const col_conoce = {
        main: '#13c0c8',
        dark: '#019592'
    }

    const col_crea = {
        main: '#ffc258',
        dark: '#e07714'
    }

    let section_col = {
        main: '#2a5080',
        dark: '#19447c'
    }

    switch (section) {
        case 'crea':
            section_col = col_crea
            break

        case 'conoce':
            section_col = col_conoce
            break

        case 'explora':
            section_col = col_explora
            break
    }

    const theme = createMuiTheme({
        palette: {
            primary: {
                main: '#2a5080',
                dark: '#19447c',
                gray: '#fafafa',
                light: '#ffffff'
            },
            secondary: section_col,
            all: {
                main: '#2a5080',
                conoce: '#13c0c8',
                explora: '#f45353',
                crea: '#ffc258'
            }
        },
        typography: {
            fontFamily: "'obliqua', sans-serif",
            fontSize: 16
        }
    })

    return (
        <Suspense fallback={<LayoutSplashScreen />}>
            <AutoLogout />
            <AutoSectionSelect location={location.pathname} />
            <ThemeProvider theme={theme}>
                <Switch>
                    <ProtectedRoute exact path="/museo/explora/buscador" rol={["invitado"]} component={Explora} />
                    <ProtectedRoute exact path="/museo/crea" rol={["invitado"]} component={Crea} />
                    <ProtectedRoute exact path="/museo/diccionario" rol={["invitado"]} component={Diccionario} />
                    <ProtectedRoute exact path="/museo/diccionario/terminos/:id" rol={["invitado"]} component={DiccionarioTerminos}/>
                    <ProtectedRoute exact path="/museo/diccionario/campos/:id" rol={["invitado"]} component={DiccionarioTerminos}/>
                    <ProtectedRoute exact path="/museo/diccionario/termino/:like/:id" rol={["invitado"]} component={TermDiccionario} />
                    <ProtectedRoute exact path="/museo/diccionario/campo/:like/:id" rol={["invitado"]} component={FieldDiccionario} />
                    <ProtectedRoute exact path="/museo/diccionario/card/:like/:id" rol={["invitado"]} component={CardDiccionario} />
                    <ProtectedRoute exact path="/museo/diccionario/busqueda/:id" rol={["invitado"]} component={DiccionarioBusqueda} />
                    <ProtectedRoute exact path="/museo" rol={["invitado"]} component={Home} />
                    <ProtectedRoute exact path="/museo/embed/:id" rol={["invitado"]} component={Embed} />
                    <ProtectedRoute exact path="/museo/crea/narrativas/lienzo" rol={["invitado"]} component={GestorNarrativas} />
                    <ProtectedRoute exact path="/museo/crea/narrativas" rol={["invitado"]} component={GestorNarrativasIntro} />
                    <ProtectedRoute exact path="/museo/crea/mi-biblioteca" rol={["invitado"]} component={MiBiblioteca} />
                    <ProtectedRoute exact path="/museo/crea/lab-verdad" rol={["invitado"]} component={LabVerdad} />
                    <ProtectedRoute exact path="/museo/conoce" rol={["invitado"]} component={Conoce} />
                    <ProtectedRoute exact path="/museo/conoce/:id" rol={["invitado"]} component={Coleccion} />
                    <ProtectedRoute exact path="/museo/microsite/:id" rol={["invitado"]} component={ViewMicrosite} />
                    <ProtectedRoute exact path="/museo/explora/detalle/:id" rol={["invitado"]} component={Detalle} />
                    <ProtectedRoute exact path="/museo/explora/detalle/registro/:id" rol={["invitado"]} component={DetalleGeneral} />

                </Switch>
            </ThemeProvider>
        </Suspense>
    );
}

export default MuseoHome