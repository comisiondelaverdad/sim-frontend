import React, { Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom"
import { makeStyles, ThemeProvider, createTheme } from "@material-ui/core"
import AutoLogout from "./../../components/atoms/AutoLogout";
import MenuSuperior from "../../components/organisms/MenuSuperior";
import Box from "@material-ui/core/Box";
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import IconButton from "@material-ui/core/IconButton";

import * as Scroll from 'react-scroll';
import { updateCloseEvent } from "./../../services/MenusService";
import SearchBar from "./searchBar";

const ApprovePage = React.lazy(() => import("../access-requests/ApprovePage"));
const MyPage = React.lazy(() => import("../access-requests/MyPage"));
const ReviewPage = React.lazy(() => import("../access-requests/ReviewPage"));
const AdminUsersPage = React.lazy(() => import("../admin/AdminUsersPage"));
const AdminHistoryPage = React.lazy(() => import("../admin/AdminHistoryPage"));
const AdminHistoryShowPage = React.lazy(() => import("../admin/AdminHistoryShowPage"));
const AdminViewShowPage = React.lazy(() => import("../admin/AdminViewShowPage"));
const AdminLocationsPage = React.lazy(() => import("../admin/AdminLocationsPage"));
const AdminUsersEditPage = React.lazy(() => import("../admin/AdminUsersEditPage"));
const SemanticPage = React.lazy(() => import("../dictionary/SemanticPage"));
const TermPage = React.lazy(() => import("../dictionary/TermPage"));
const SemanticAddPage = React.lazy(() => import("../dictionary/SemanticAddPage"));
const TermAddPage = React.lazy(() => import("../dictionary/TermAddPage"));
const SemanticEditPage = React.lazy(() => import("../dictionary/SemanticEditPage"));
const SemanticEditPageV = React.lazy(() => import("../dictionary/SemanticEditPageV"));
const TermEditPage = React.lazy(() => import("../dictionary/TermEditPage"));
const TermEditPageV = React.lazy(() => import("../dictionary/TermEditPageV"));
const AdminMenusPage = React.lazy(() => import("../admin/menus/AdminMenusPage"));
const MenuModify = React.lazy(() => import("../admin/menus/MenuModify"));
const HomologateTerm = React.lazy(() => import("../admin-cataloging/homologateTerm"));
const HomologateEditTerm = React.lazy(() => import("../admin-cataloging/homologateTermEditPage"));
const AdminMicrositesPage = React.lazy(() => import("../admin/microsites/AdminMicrositesPage"));
const MicrositeModify = React.lazy(() => import("../admin/microsites/MicrositeModify"));



const ResultsPage = React.lazy(() => import("../search/ResultsPage"));
const DetailPage = React.lazy(() => import("../search/DetailsPage"));
const ResourceGroupPage = React.lazy(() => import("../search/ResourceGroupPage"));
const VisualMetadataPage = React.lazy(() => import("../metadata/VisualMetadataPage"));
const VisualContentPage = React.lazy(() => import("../metadata/VisualContentPage"));
const VisualExplorerPage = React.lazy(() => import("../metadata/VisualExplorerPage"));
const TesauroPage = React.lazy(() => import("../search/TesauroPage"));
const DetailsPage = React.lazy(() => import("../access-requests/DetailsPage"));
const BookmarksPage = React.lazy(() => import("../bookmarks/BookmarksPage"));

const List = React.lazy(() => import("./../list/list"));
const OptionForm = React.lazy(() => import("../list/optionForm"));
const ListForm = React.lazy(() => import("../list/listForm"));
const ListAlert = React.lazy(() => import("../list/listAlert"));
const FormEditor = React.lazy(() => import("../form/form"));


const Dashboard = React.lazy(() => import("./Dashboard"));

const Resource = React.lazy(() => import("../resources/Resource"));
const ResourceMasive = React.lazy(() => import("../resources/resource-masive"));
const ProtectedRoute = React.lazy(() => import("./protectedRoute"));
const ResourceGroup = React.lazy(() => import("./../resource-group/resource-group"));
const AdminResourceGroups = React.lazy(() => import("./../admin-cataloging/resourceGroups"));
const MainAdminResources = React.lazy(() => import("../admin-cataloging/main"));

const scroll = Scroll.animateScroll;

function scrollToTop() {
  scroll.scrollToTop();
};

export default function HomePage() {

  const useStyles = makeStyles((theme) => ({
    bg: {
      minHeight: "100%",
      overflow: 'auto',
      padding: "95px 60px"
    }
  }))

  const classes = useStyles();

  return (
    <>
      <Box>
        <MenuSuperior />
        <SearchBar />
        <Box component="section" className={classes.bg} onClick={()=>(updateCloseEvent(false))}>
          <Suspense fallback={"Loading"} >
            <AutoLogout></AutoLogout>
            <Switch onChange={updateCloseEvent(false)}>
              {
                /* Redirect from root URL to /dashboard. */
                <Redirect exact from="/" to="/dashboard" />
              }
              <Route path="/dashboard" component={Dashboard} />
              <Route path="/search" component={ResultsPage} />
              <Route path="/detail/:id" component={DetailPage} />
              <Redirect exact from="/resourcegroup" to="resourcegroup/all" />
              <Route path="/resourcegroup/:id" component={ResourceGroupPage} />
              <Route
                path="/resourcegroupmanagment/:action/:id"
                component={ResourceGroup}
              />
              <Route path="/visualmetadata" component={VisualMetadataPage} />
              <Route path="/visualcontent" component={VisualContentPage} />
              <Route path="/visualexplorer" component={VisualExplorerPage} />

              <Route path="/tesauro" component={TesauroPage} />
              <Route path="/access-requests/:id" component={DetailsPage} />
              <Route path="/bookmarks" component={BookmarksPage} />


              <Route path="/my-access-requests" component={MyPage} />
              <Route path="/review-access-requests" component={ReviewPage} />
              <Route path="/approve-access-requests" component={ApprovePage} />

              <Route path="/admin/users/:id" component={AdminUsersEditPage} />
              <Route path="/admin/history/:id" component={AdminHistoryShowPage} />
              <Route path="/admin/view/:id" component={AdminViewShowPage} />
              <Route path="/resources/resource/:resourcegroup/:id" component={Resource} />
              <ProtectedRoute exact path="/list" rol={["catalogador_gestor", "admin", "tesauro"]} component={List} />
              <ProtectedRoute exact path="/forms" rol={["admin", "catalogador_gestor"]} component={FormEditor} />
              <ProtectedRoute exact path="/list/optionForm/:listid/:id" rol={["catalogador_gestor", "admin", "tesauro"]} component={OptionForm} />
              <ProtectedRoute exact path="/list/listForm/:optionid/:id" rol={["catalogador_gestor", "admin", "tesauro"]} component={ListForm} />
              <ProtectedRoute exact path="/list/listAlert/:optionid/:id" rol={["catalogador_gestor", "admin", "tesauro"]} component={ListAlert} />
              {/* <ProtectedRoute exact path="/resources/listResource" rol={["catalogador", "catalogador_gestor"]} component={ListResource} />
              <ProtectedRoute exact path="/admin-cataloging/listAllResources" rol={["catalogador", "catalogador_gestor"]} component={ListResource} /> */}
              <ProtectedRoute exact path="/admin-cataloging/resourceGroups/:id" rol={["catalogador_gestor", "catalogador"]} component={AdminResourceGroups} />
              <ProtectedRoute exact path="/admin-cataloging/main/:id" rol={["catalogador_gestor", "catalogador"]} component={MainAdminResources} />
              <ProtectedRoute exact path="/resources/masive" rol={["catalogador", "catalogador_gestor"]} component={ResourceMasive} />

              <Route path="/admin/users" component={AdminUsersPage} />
              <Route path="/admin/locations" component={AdminLocationsPage} />
              <Route path="/admin/history" component={AdminHistoryPage} />

              <Route exact path="/admin/menus" component={AdminMenusPage} />
              <Route exact path="/admin/menus/modify/" component={MenuModify} />
              <Route exact path="/admin/menus/modify/:id" component={MenuModify} />

              <Route exact path="/admin/microsites" component={AdminMicrositesPage} />
              <Route exact path="/admin/microsites/modify/" component={MicrositeModify} />
              <Route exact path="/admin/microsites/modify/:id" component={MicrositeModify} />

              <ProtectedRoute exact path="/admin-cataloging/homologateTerm" rol={["tesauro"]} component={HomologateTerm} />
              <ProtectedRoute exact path="/admin-cataloging/homologateTermEditPage/:id" rol={["tesauro"]} component={HomologateEditTerm} />


              <Route
                path="/dictionary/semantic"
                rol={["admin_diccionario"]}
                component={SemanticPage}
              />
              <Route
                path="/dictionary/term"
                rol={["admin_diccionario"]}
                component={TermPage}
              />
              <Route
                path="/dictionary/semanticadd"
                rol={["admin_diccionario"]}
                component={SemanticAddPage}
              />
              <Route
                path="/dictionary/termadd"
                rol={["admin_diccionario"]}
                component={TermAddPage}
              />
              <Route
                path="/dictionary/semanticedit/:id"
                rol={["admin_diccionario"]}
                component={SemanticEditPage}
              />
              <Route
                path="/dictionary/semanticeditv/:id"
                rol={["admin_diccionario"]}
                component={SemanticEditPageV}
              />
              <Route
                path="/dictionary/termedit/:id"
                rol={["admin_diccionario"]}
                component={TermEditPage}
              />
              <Route
                path="/dictionary/termeditv/:id"
                rol={["admin_diccionario"]}
                component={TermEditPageV}
              />

              <Redirect to="/error/error-v1" />
            </Switch>
          </Suspense >
          <IconButton color="primary" variant="outlined" class="toTop" onClick={scrollToTop} aria-label="up">
            <ArrowUpwardIcon />
          </IconButton>
        </Box>

      </Box>
    </>
  );
}
