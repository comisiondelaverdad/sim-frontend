import React, { useState, useEffect } from "react";
import FormSearch from "../../components/organisms/FormSearch";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import * as ResourceService from "../../services/ResourcesService";
import MuseoInvitation from "../../components/molecules/MuseoInvitation";

function Dashboard(props) {
  const [totalResources, setTotalResources] = useState("Aquí Gran Total");

  // De forma similar a componentDidMount y componentDidUpdate
  useEffect(() => {
    // Actualiza el título del documento usando la API del navegador
    ResourceService.serviceCount().then(
      (data) => {
        setTotalResources(new Intl.NumberFormat("de-DE").format(data.count));
        props.pageSubtitle(totalResources);
        props.pageTitle("Total de Recursos");
        props.saveSearchButton(false);
        props.showVisualization(false);
        props.showSearch(false);
      },
      (error) => {
        if (error) {
        }
        //props.history.push("/logout");
      }
    );
  });

  return (
    <div className="cev-dashboard">
      <h3>
        Busca la verdad entre {totalResources} recursos.
      </h3>
      <FormSearch />
      {/* <MuseoInvitation /> */}
    </div>
  );
}

export default connect(null, app.actions)(Dashboard);
