import React, { Component } from "react";
import FormFieldsBootstrap from './../../components/organisms/FormFieldsBootstrap';
import { serviceDetail, newResourceGroup, updateResourceGroupMetadata } from '../../services/ResourceGroupService';
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { Form } from "react-bootstrap";
import BreadCrumbs from "../../components/molecules/Breadcrumbs";
import Swal from "sweetalert2";
import { objectTransformation, findField, dateToAAAAMMDD, submitAlert, objectTransformationInverse, resourceGroupsValidation } from '../../services/utils';
import { getData } from '../../services/RequestService';
import { toAbsoluteUrl } from "../../../theme/utils";

class ResourceGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: {},
            edit: true,
            breadcrumbs: [],
            actualPath: '',
            statment: null,
            father: '',
            form: null,
            roles: null,
        };

        this.outputFieldBootstrap = this.outputFieldBootstrap.bind(this);
        this.goback = this.goback.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    goback() {
        this.props.history.goBack()
    }

    filterRuleFields(field, form) {
        // console.log(form)
        const formBase = form
            .map((cf) => (cf.hasOwnProperty('ruleToShow') ?
                !cf.ruleToShow.includes(field) ? { ...cf, ...{ hide: true } } : { ...cf, ...{ hide: false } } : cf));
        this.setState({ form: formBase });
    }

    componentDidMount() {
        //LOAD DATA BREADCRUMB
        this.setState({ roles: this.props.user.roles });
        const breadcrumbs = localStorage.getItem('breadcrumbs');
        getData('/api/forms/byname/RESOURCEGROUPS_FAE')
            .then((resourceGroupFields) => {
                if (breadcrumbs !== null) {
                    const breadcrumbsArr = JSON.parse(breadcrumbs);
                    this.setState({ breadcrumbs: breadcrumbsArr, actualPath: (breadcrumbsArr.map((b) => (b.resourceGroup)).join("|")) });
                }
                if (this.props.match.params.action === 'edit') {
                    this.setState({ edit: true });
                } else {
                    this.setState({ edit: false });
                }
                const creationDateStart = findField(resourceGroupFields, 'creationDateStart');
                creationDateStart.required = true;
                if (this.props.match.params.id !== '0' && this.props.match.params.action === 'edit') {
                    this.props.pageTitle(`Edición de metadata`);
                    serviceDetail(this.props.match.params.id)
                        .then((resourcegroup) => {
                            const associatedResources = findField(resourceGroupFields, 'associatedResources');
                            associatedResources.options = resourcegroup.metadata.firstLevel.associatedResources ? (resourcegroup.metadata.firstLevel.associatedResources.map((title) => {
                                title = title.split(',')
                                return title.map((t) => ({ name: t, value: t }))
                            })).flat([1]) : [];

                            const associatedResourceGroups = findField(resourceGroupFields, 'associatedResourceGroups');
                            associatedResourceGroups.options = resourcegroup.metadata.firstLevel.associatedResourceGroups ? (resourcegroup.metadata.firstLevel.associatedResourceGroups.map((title) => {
                                title = title.split(',')
                                return title.map((t) => ({ name: t, value: t }))
                            })).flat([1]) : [];
                            Swal.close();

                            this.setState({ form: resourceGroupFields });
                            this.props.pageSubtitle(`${resourcegroup.metadata.firstLevel.title ? resourcegroup.metadata.firstLevel.title : ''}`);
                            const { firstLevel } = resourcegroup.metadata;
                            const formData = {
                                ...{ origin: resourcegroup.origin },
                                ...objectTransformationInverse(resourcegroup, this.buildStatment(resourceGroupFields)),
                                ...{ creationDateStart: firstLevel.creationDate ? firstLevel.creationDate.start ? dateToAAAAMMDD(firstLevel.creationDate.start) : null : null },
                                ...{ creationDateEnd: firstLevel.creationDate ? firstLevel.creationDate.end ? dateToAAAAMMDD(firstLevel.creationDate.end) : null : null },
                            }
                            this.setState({ formData: formData });
                        })
                        .catch(err => {
                            console.log(err)
                        })
                } else if (this.props.match.params.id !== '0' && this.props.match.params.action === 'new') {
                    const identField = findField(resourceGroupFields, 'ident');
                    identField.label = 'Identificador del fondo padre'
                    serviceDetail(this.props.match.params.id)
                        .then((resourcegroup) => {
                            this.setState({ form: resourceGroupFields });
                            this.props.pageSubtitle(`${resourcegroup.metadata.firstLevel.title ? resourcegroup.metadata.firstLevel.title : ''}`);
                            this.setState({ father: resourcegroup.metadata.firstLevel.title });
                            const formData = {
                                ...{ ident: resourcegroup.ident },
                                ...{ origin: resourcegroup.origin },
                            }
                            this.buildStatment(resourceGroupFields)
                            this.setState({ formData: formData });
                        });
                    this.props.pageTitle(`Nuevo fondo `);

                }
            });

    }


    async handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            this.setState({ validated: true });
        } else {
            let metadata = this.state.formData;
            const info = { ...objectTransformation(metadata, this.state.statment), ...{ origin: metadata.origin } };
            //console.log(info);

            const validation = resourceGroupsValidation(info.metadata);
            if (validation.error) {
                Swal.fire({
                    icon: 'error',
                    width: 600,
                    heightAuto: false,
                    html: validation.message,
                });
            } else {

                if (this.state.edit) {
                    submitAlert({ option: 'update', type: 'fondo', fn: updateResourceGroupMetadata, data: info, info: metadata.title })
                } else {
                    submitAlert({ option: 'create', type: 'fondo', fn: newResourceGroup, data: info, info: metadata.title })
                }
            }
        }
        event.preventDefault();
        event.stopPropagation();
    }

    outputFieldBootstrap(event) {
        if (event.formData) {
            const formData = { formData: { ...this.state.formData, ...event.formData } };
            console.log(event);
            if (typeof event.formData.descriptionLevel !== 'undefined') {
                console.log(event);
                this.filterRuleFields(event.formData.descriptionLevel, this.state.form);
            }
            this.setState(formData);
        }
    }

    buildStatment(form) {
        const statment = form
            .filter((field) => (!(['separator', 'file'].includes(field.type))))
            .map((field) => {
                return { defaultValue: field.defaultValue, destiny: field.destiny, origin: field.id, type: field.type }
            })
        this.setState({ statment: statment });
        return statment;
    }

    render() {
        const { validated, formData, edit, breadcrumbs, form, roles } = this.state;
        return (
            <>
                {(roles && ((roles.includes('catalogador') && edit) || (roles.includes('catalogador_gestor')))) &&
                    <div className="row content-view">
                        <div className="col-sm-12">
                            {breadcrumbs.length > 0 &&
                                <BreadCrumbs items={breadcrumbs}></BreadCrumbs>
                            }
                            <div className="view-tree-container height-80 p-20">
                                <div className="view-tree-title">
                                    <div className="circle-icon">
                                        <svg className="cv-small">
                                            <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-fondos")} className={"flia-azul"}></use>
                                        </svg>
                                    </div>
                                    <span className="label">Gestión de fondos </span>
                                </div>
                                <div className="view-tree-body">
                                    <Form
                                        noValidate
                                        validated={validated}
                                        onSubmit={e => this.handleSubmit(e)}>
                                        <div className="cev-portlet__body table-responsive">
                                            {!!form && !!formData && (
                                                <FormFieldsBootstrap
                                                    formData={formData}
                                                    formConfig={form}
                                                    outputEvent={this.outputFieldBootstrap}
                                                ></FormFieldsBootstrap>
                                            )}
                                        </div>
                                        <div className="d-flex flex-row-reverse">
                                            {!edit && (
                                                <button className="btn btn-primary ml-2" type="submit" value="submit">Nuevo fondo</button>
                                            )}
                                            {!!edit && (
                                                <button className="btn btn-primary ml-2" type="submit" value="submit">Actualizar fondo</button>
                                            )}
                                            <button className="btn btn-secondary ml-2" id="return" onClick={() => this.goback()} type="reset" >Cancelar</button>
                                        </div>
                                    </Form>
                                </div>
                            </div>
                        </div>
                    </div>

                }
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(ResourceGroup);
