import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { getData, deleteData } from "../../services/RequestService";
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";
import { ListItem } from "./listItem";
import { listSelected$, updateListSelected } from "./../../services/selectListObserver"
import Swal from "sweetalert2";
import DownloadResult from "../../components/molecules/DownloadResult";
import { sleep } from '../../services/utils';
import * as copy from "clipboard-copy";

import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import DeleteIcon from '@material-ui/icons/Delete';

import ListIcon from '@material-ui/icons/List';
import EditIcon from '@material-ui/icons/Edit';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import FileCopyIcon from '@material-ui/icons/FileCopy';

const listsHeader = [
    {
        title: 'Acciones',
        data: '_id',
        type: 'action',
        width: "12%"
    },
    {
        title: 'Texto',
        data: 'term'
    },
    {
        title: 'Estado',
        data: 'status'
    },
];

const rowsPage = 25;

class List extends Component {

    constructor(props) {
        super(props);
        this.state = {
            optionSelection: false,
            lists: [],
            listSelected: null,
            rowSelected: null,
            export: "none",
            dataset: [],
        };
        this.actionEvent = this.actionEvent.bind(this);
        this.deleteList = this.deleteList.bind(this);
        this.separateResourcesByStatment = this.separateResourcesByStatment.bind(this);
    }

    separateResourcesByStatment(resources) {
        let resourcesFAE = [];
        let resourcesFAI = [];

        resources.forEach(resource => {
            if (resource.origin == 'CatalogacionFuentesInternas' || resource.origin == 'CargasFuentesInternas'
                || resource.origin == 'VisualizacionesGeograficas' || resource.origin == 'Visualizaciones') {
                resourcesFAI.push(resource);
            } else {
                resourcesFAE.push(resource);
            }
        });
        return {
            resourcesFAE: resourcesFAE,
            resourcesFAI: resourcesFAI
        }
    }

    actionEvent({ action, row }) {
        this.setState({ rowSelected: row });
        if (action.action === 'edit') {
            console.log(this.state.listSelected);
            console.log(row);
            this.redirect(`/list/optionForm/${this.state.listSelected ? this.state.listSelected._id ? this.state.listSelected._id : 0 : 0}/${row._id}`)
        } else if (action.action === 'delete') {
            this.deleteOption(row);
        } else if (action.action === 'new_list') {
            this.redirect('/list/listForm/' + row._id + '/0')
        }
    }

    copy_clipboard() {
        const options = this.state.listSelected.options;
        const terms = options.map((o) => (o.term))
        const textOptions = terms.reduce((a, b) => (a + '\n' + b));
        const textOptionsHTML = terms.reduce((a, b) => (a + '<br>' + b));
        copy(textOptions).then((data)=> {
            Swal.fire({
                title: 'Se copiaron al portapapeles ' + options.length + '<br>opciones de la lista <br>' + this.state.listSelected.name,
                icon: 'alert',
                html: `<div class='meta-container' align="left">${textOptionsHTML}</div>`
            })
        })
    }

    deleteOption(row) {
        if (typeof row.lists === 'undefined') {
            Swal.fire({
                title: 'Eliminar opción',
                html: `Se eliminará la opción<br><b>${row.term}</b>`,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Eliminar opción'
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: 'Esta operación no es reversible',
                        html: `Está seguro que desea eliminar la opción<br><b>${row.term}</b>`,
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Eliminar opción'
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire({
                                title: '¡Por favor espere!',
                                html: 'Eliminando opción ...',
                                allowOutsideClick: false,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                },
                            });
                            deleteData(`/api/option/${row._id}/${this.state.listSelected ? this.state.listSelected._id ? this.state.listSelected._id : 0 : 0}`)
                                .then((data) => {
                                    if (typeof data.message !== 'undefined') {
                                        Swal.close();
                                        Swal.fire({
                                            title: 'No se ha podido eliminar la opción',
                                            icon: 'error',
                                            showCancelButton: true,
                                            cancelButtonText: 'Descargar registros que contienen la opción',
                                            html: data.message
                                        }).then(async (result) => {
                                            Swal.close();
                                            if (result.dismiss == "cancel") {

                                                Swal.fire({
                                                    title: '¡Por favor espere!',
                                                    html: 'Descargando registros...',
                                                    allowOutsideClick: false,
                                                    onBeforeOpen: () => {
                                                        Swal.showLoading()
                                                    },
                                                });

                                                await sleep(20);

                                                let resourcesByStatment = this.separateResourcesByStatment(data.resourcesFlat);
                                                let dataset = {
                                                    statmentFAE: data.statmentFAE,
                                                    statmentFAI: data.statmentFAI,
                                                    statmentFondos: data.statmentFondos,
                                                    resourcegroupsFlat: data.resourcegroupsFlat,
                                                    resourcesFAI: resourcesByStatment.resourcesFAI,
                                                    resourcesFAE: resourcesByStatment.resourcesFAE
                                                }
                                                this.setState({ dataset: dataset, export: "actual" });
                                            }
                                        });
                                    } else {
                                        Swal.close();
                                        Swal.fire(
                                            'Eliminado',
                                            `Se eliminado exitosamente la opción ${row.term}`,
                                            'success'
                                        )
                                        this.cargarListas();
                                    }
                                })
                                .catch(err => {
                                    Swal.close();
                                    Swal.fire(
                                        'No se ha podido eliminar la opción',
                                        `ERROR: ${err}`,
                                        'error'
                                    )
                                })
                        }
                    });
                }
            })
        } else {
            Swal.fire(
                'No se ha podido eliminar la opción',
                `ERROR: Esta opción tiene la siguiente lista: <b>${row.lists.name}</b>`,
                'error'
            )
        }
    }

    deleteList() {
        const list = this.state.listSelected;
        if (list.options.length === 0) {
            Swal.fire({
                title: 'Eliminar lista',
                html: `Se eliminará la lista<br><b>${list.name}</b>`,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Eliminar lista'
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        title: 'Esta operación no es reversible',
                        html: `Está seguro que desea eliminar la lista<br><b>${list.name}</b>`,
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Eliminar lista'
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire({
                                title: '¡Por favor espere!',
                                html: 'Eliminando lista ...',
                                allowOutsideClick: false,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                },
                            });
                            deleteData(`/api/list/${this.state.listSelected ? this.state.listSelected._id ? this.state.listSelected._id : 0 : 0}`)
                                .then((data) => {
                                    if (typeof data.message !== 'undefined') {
                                        Swal.close();
                                        Swal.fire({
                                            title: 'No se ha podido eliminar la opción',
                                            icon: 'error',
                                            html: data.message
                                        })
                                    } else {
                                        Swal.close();
                                        Swal.fire(
                                            'Eliminado',
                                            `Se eliminado exitosamente la lista ${list.name}`,
                                            'success'
                                        )
                                        this.cargarListas();
                                    }
                                })
                                .catch(err => {
                                    Swal.close();
                                    Swal.fire(
                                        'No se ha podido eliminar la opción',
                                        `ERROR: ${err}`,
                                        'error'
                                    )
                                })
                        }
                    });
                }
            })
        } else {
            Swal.fire(
                'No se ha podido eliminar la opción',
                `ERROR: Esta lista tiene <b>${list.options.length}</b> opciones asociadas, debe eliminalas primero`,
                'error'
            )
        }
    }

    redirect(path) {
        this.props.history.push(`${path}`);
    }

    async cargarListas() {
        this.setState({ lists: [], optionSelection: false, listSelected: {options: []} });
        const lists = await getData('/api/list');
        this.setState({ lists: lists, optionSelection: false, listSelected: {options: []} });

    }

    componentDidMount() {
        this.cargarListas();
        listSelected$.subscribe((list) => {
            this.setState({ listSelected: list });
        })
    }

    render() {
        const { optionSelection, lists, listSelected } = this.state;
        return (
            <>
                <div className="row content-view">
                    {this.state.export == "actual" && (
                        <>
                            <DownloadResult dataset={this.state.dataset.resourcegroupsFlat} statment={this.state.dataset.statmentFondos} frmType={"Fondos"} />
                            <DownloadResult dataset={this.state.dataset.resourcesFAI} statment={this.state.dataset.statmentFAI} frmType={"FAI"} />
                            <DownloadResult dataset={this.state.dataset.resourcesFAE} statment={this.state.dataset.statmentFAE} frmType={"FAE"} />
                            {this.setState({ export: "none" })}
                        </>

                    )}
                    <div className="col-sm-5 offset-sm-1">
                        <div className="view-tree-container height-90">
                            <div className="view-tree-title">
                                <span className="label">
                                    Lista
                                </span>
                            </div>
                            <div className="view-tree-body">
                                {lists.map((list, key) => (<ListItem selected={listSelected} key={key} item={list} level={1}></ListItem>))}
                                <br />
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="view-tree-container height-90">
                            <div className="view-tree-title">
                                <span className="label">
                                    {listSelected ? listSelected.description ? listSelected.description : '' : ''}
                                    {listSelected ? listSelected.name ? `(${listSelected.name})` : '' : ''}
                                </span>
                                <span>Opciones</span>
                            </div>
                            <div className="view-tree-body">
                                {listSelected && listSelected.options && (
                                    <SimpleDataTable
                                        id='selection'
                                        columns={listsHeader}
                                        data={listSelected.options}
                                        actions={[
                                            { action: 'edit', title: 'Editar', classLaIcon: 'la la-edit' },
                                            { action: 'delete', title: 'Eliminar', classLaIcon: 'la la-trash' },
                                            { action: 'new_list', title: 'Nueva sublista', classLaIcon: 'la la-list' },
                                        ]}
                                        outputEvent={this.actionEvent}
                                    ></SimpleDataTable>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="toolbar-cataloging">
                    <button type="button" className="btn-action" title="Nueva Lista"
                        onClick={() => this.redirect(`/list/listForm/0/0`)}>
                        <PlaylistAddIcon></PlaylistAddIcon>
                    </button>

                    {(this.props.user.roles.includes('catalogador_gestor') ||
                        this.props.user.roles.includes("tesauro")) && listSelected && (
                            <button type="button" className="btn-action warning" title="Editar Lista"
                                onClick={() => this.redirect(`/list/listForm/${listSelected ? listSelected._id ? listSelected._id : '0' : '0'}/${listSelected._id}`)}
                            ><EditIcon></EditIcon><ListIcon></ListIcon>
                            </button>
                        )}

                    {(this.props.user.roles.includes('catalogador_gestor') ||
                        this.props.user.roles.includes("tesauro")) && listSelected && (
                            <button type="button" className="btn-action danger" title="Eliminar Lista"
                                onClick={() => this.deleteList()}
                            ><DeleteIcon></DeleteIcon><ListIcon></ListIcon>
                            </button>
                        )}

                    {(this.props.user.roles.includes('catalogador_gestor') ||
                        this.props.user.roles.includes("tesauro")) && listSelected && (
                            <button type="button" className="btn-action warning" title="Alertas sobre lista"
                                onClick={() => this.redirect(`/list/listAlert/${listSelected ? listSelected._id ? listSelected._id : '0' : '0'}/${listSelected._id}`)}
                            ><NotificationsActiveIcon></NotificationsActiveIcon><ListIcon></ListIcon>
                            </button>
                        )}

                    {(this.props.user.roles.includes('catalogador_gestor') ||
                        this.props.user.roles.includes("tesauro")) && listSelected && (
                            <button type="button" className="btn-action" title="Copiar lista"
                                onClick={() => this.copy_clipboard()}
                            ><FileCopyIcon></FileCopyIcon>
                            </button>
                        )}

                </div>
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(List);
