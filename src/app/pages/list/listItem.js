import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { updateListSelected } from "./../../services/selectListObserver"
export class ListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            item: props.item,
            level: props.level,
            listSelected: props.listSelected,
        };
        this.selectList = this.selectList.bind(this);
    }

    selectList(e, list) {
        e.stopPropagation();
        updateListSelected(list);
    }

    render() {
        const { item, level, listSelected } = this.state;
        const subList = item.options.filter((o) => (typeof o.lists !== 'undefined'))
        return (
            <>
                <div className={`list-item ${listSelected ? listSelected._id === item._id ? 'selected' : '' : ''}`} onClick={(e) => this.selectList(e, item)}>
                    {item.name ? `${item.name}` : ''} {item.description ? `(${item.description})` : ''}
                    {subList.map((list, key_sub) => {
                        return (
                            <div key={key_sub} className={`list-subitem level-${level + 1} ${listSelected ? listSelected._id === list._id ? 'selected' : '' : ''} `}>
                                <ListItem
                                    listSelected={listSelected}
                                    item={list.lists}
                                    level={level + 1}>
                                </ListItem>
                            </div>
                        )
                    })}
                </div>
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(ListItem);
