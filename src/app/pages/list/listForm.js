import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { getData, postData, putData } from "../../services/RequestService";
import { Form } from "react-bootstrap";
import { submitAlert } from './../../services/utils'
import FormFieldsBootstrap from '../../components/organisms/FormFieldsBootstrap';
import Swal from "sweetalert2";

const formBase = [
    {
        id: 'name',
        defaultValue: null,
        label: 'Nombre',
        type: 'text',
        col: 12,
        placeholder: 'Ingrese el nombre de la lista',
        info: '',
        required: true
    },
    {
        id: 'description',
        defaultValue: null,
        label: 'Description',
        type: 'text',
        col: 12,
        placeholder: 'Ingrese una breve descripción',
        info: '',
        required: false
    },
    {
        id: 'path',
        defaultValue: null,
        label: 'Nombre para formulario',
        type: 'text',
        col: 12,
        placeholder: 'Ingrese el nombre para asociar al formulario',
        info: '',
        hide: false, //change to false
        required: false
    },
    {
        id: 'deleteOptions',
        defaultValue: null,
        label: 'Opciones para eliminar',
        type: 'select-multiple2',
        col: 12,
        placeholder: 'Eliminar Opciones',
        info: '',
        hide: true,
        options: [],
        required: false
    },
    {
        id: 'newOptions',
        defaultValue: null,
        label: 'Nuevas opciones de la lista',
        type: 'select-multiple2-tags',
        col: 12,
        placeholder: 'Ingrese las opciones de la lista',
        info: '',
        hide: false,
        options: [],
        required: false
    },
]
class ListForm extends Component {


    constructor(props) {
        super(props);
        this.state = {
            optionSelection: false,
            option: null,
            formList: formBase,
            formData: {},
            edit: false,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.goback = this.goback.bind(this);
        this.outputFieldBootstrap = this.outputFieldBootstrap.bind(this);
    }

    redirect(path) {
        this.props.history.push(`${path}`);
    }

    goback() {
        this.props.history.goBack()
    }

    initList() {
        let edit = false;
        if (this.props.match.params.optionid !== '0' && this.props.match.params.id !== '0') {
            edit = true;
            this.props.pageTitle(`Editar lista secundaria`);
            formBase[3].hide = true; //please change false
            formBase[4].required = false;
            getData('/api/list/byId/' + this.props.match.params.id)
                .then((list) => {
                    formBase[3].options = list.options
                        .filter((o) => (typeof o.lists === 'undefined'))
                        .map((data) => ({ name: data.term, value: data._id }));
                    this.props.pageSubtitle(`${list.term ? list.term : ''} - ${list.description ? list.description : ''}`);
                    this.setState({ formData: list });
                })
                .catch(err => {
                    Swal.close();
                    Swal.fire(
                        'Error al cargar la información.',
                        `ERROR: ${err}`,
                        'error'
                    )
                })
            this.setState({ formList: formBase, edit: edit });
        } else if (this.props.match.params.optionid === '0' && this.props.match.params.id === '0') {
            this.props.pageTitle(`Crear lista primaria`);
            this.props.pageSubtitle('');
            console.log('crear nuevo desde una raiz')
            formBase[3].hide = true;
            formBase[4].required = true;
            this.setState({ formList: formBase, edit: edit });
        } else if (this.props.match.params.optionid === '0' && this.props.match.params.id !== '0') {
            this.props.pageTitle(`Editar lista primaria`);
            console.log('crear nuevo desde opción' + this.props.match.params.optionid);
            formBase[3].hide = true;
            this.setState({ formList: formBase, edit: edit });
        } else if (this.props.match.params.optionid !== '0' && this.props.match.params.id === '0') {
            formBase[0].hide = true;
            formBase[3].hide = true;
            formBase[4].required = true;
            this.props.pageTitle(`Crear lista secundaria`);
            getData('/api/option/byid/' + this.props.match.params.optionid)
                .then((option) => {
                    this.props.pageSubtitle(`${option.term ? option.term : ''} - ${option.description ? option.description : ''}`);
                    formBase[0].defaultValue = option.term ? option.term : '';
                    this.setState({ formList: formBase, edit: edit, option: option, formData: {name:option.term ? option.term : ''} });
                })
                .catch(err => {
                    Swal.close();
                    Swal.fire(
                        'Error al cargar la información.',
                        `ERROR: ${err}`,
                        'error'
                    )
                })
        }
    }

    outputFieldBootstrap(event) {
        console.log(event);
        this.setState(event);
    }

    componentDidMount() {
        this.initList()
    }

    createList(body) {
        return postData('/api/list', body)
    }

    updateList(body) {
        return putData('/api/list', body)
    }

    handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            this.setState({ validated: true });
        } else {
            if (this.state.edit) {
                const body = {
                    id: this.props.match.params.id,
                    body: {
                        options: this.state.formData.options,
                        newOptions: this.state.formData.newOptions?this.state.formData.newOptions.map((o) => ({ term: o })):[],
                        deleteOptions: this.state.formData.deleteOptions?this.state.formData.deleteOptions:[],
                        name: this.state.formData.name,
                        description: this.state.formData.description,
                        path: this.state.formData.path,
                    }
                }
                submitAlert({ option: 'update', type: 'lista', fn: this.updateList, data: body, info: this.state.formData.name, gen: 'la' });
            } else {
                const options = this.state.formData.newOptions;
                const body = {
                    ...this.state.formData,
                    ...{
                        newOptions: options.map((o) => ({ term: o })),
                        optionid: this.props.match.params.optionid,
                    }
                }
                submitAlert({ option: 'create', type: 'lista', fn: this.createList, data: body, info: this.state.formData.name, gen: 'la' });
            }
        }
        event.preventDefault();
        event.stopPropagation();
    }

    render() {
        const { validated, formList, formData } = this.state;
        return (
            <div className="row content-view">
                <div className="col-sm-12">
                    <div className="view-tree-container height-70">
                        <div className="view-tree-title">
                            <span className="label"> Lista </span>
                        </div>
                        <div className="view-tree-body">
                            <br />
                            <Form
                                noValidate
                                validated={validated}
                                onSubmit={e => this.handleSubmit(e)}>
                                <Form.Row>
                                    {!!formList && !!formData && (
                                        <FormFieldsBootstrap
                                            formData={formData}
                                            formConfig={formList}
                                            outputEvent={this.outputFieldBootstrap}
                                        ></FormFieldsBootstrap>
                                    )}
                                </Form.Row>
                                <div className="d-flex flex-row-reverse">
                                    {this.props.match.params.id === '0' && (
                                        <button className="btn btn-primary ml-2" type="submit" value="submit">Nueva lista</button>
                                    )}
                                    {this.props.match.params.id !== '0' && (
                                        <button className="btn btn-primary ml-2" type="submit" value="submit">Actualizar lista</button>
                                    )}
                                    <button className="btn btn-secondary ml-2" id="return" onClick={() => this.goback()} type="reset" >Cancelar</button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>

            </div>

        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(ListForm);
