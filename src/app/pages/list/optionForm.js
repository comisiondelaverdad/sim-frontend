import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { getData, postData, putData } from "../../services/RequestService";
import { Form } from "react-bootstrap";
import { submitAlert } from './../../services/utils'
import FormFieldsBootstrap from '../../components/organisms/FormFieldsBootstrap';
import Swal from "sweetalert2";

const formBase = [
    {
        id: 'term',
        defaultValue: null,
        label: 'Término',
        type: 'text',
        col: 12,
        placeholder: 'Ingrese el termino de la opción',
        info: '',
        required: true
    },
    {
        id: 'description',
        defaultValue: null,
        label: 'Descripción',
        type: 'text',
        col: 12,
        placeholder: 'Ingrese descripción (se sugiere el nombre del campo)',
        info: '',
        required: true
    },
]
class OptionForm extends Component {


    constructor(props) {
        super(props);
        this.state = {
            optionSelection: false,
            option: null,
            formData: {},
            formOption: formBase
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.goback = this.goback.bind(this);
        this.outputFieldBootstrap = this.outputFieldBootstrap.bind(this);
    }

    redirect(path) {
        this.props.history.push(`${path}`);
    }

    goback() {
        this.props.history.goBack()
    }

    initOptions() {
        let edit = false;
        if (this.props.match.params.listid !== '0' && this.props.match.params.id !== '0') {
            edit = true;
            this.props.pageTitle(`Editar opción`);
            getData('/api/option/byId/' + this.props.match.params.id)
                .then((option) => {
                    const { description, term, _id} = option
                    this.props.pageSubtitle(`${option.term ? option.term : ''} - ${option.description ? option.description : ''}`);
                    this.setState({ formData: { description, term, _id } });
                })
                .catch(err => {
                    Swal.close();
                    Swal.fire(
                        'Error al cargar la información.',
                        `ERROR: ${err}`,
                        'error'
                    )
                })
        } else if (this.props.match.params.listid !== '0' && this.props.match.params.id === '0') {
            this.props.pageTitle(`Crear opción`);
        }
        this.setState({ formOption: formBase, edit: edit })
    }

    outputFieldBootstrap(event) {
        console.log(event);
        this.setState(event);
    }

    componentDidMount() {
        this.initOptions()
    }

    create(body) {
        return postData('/api/option', body)
    }

    update(body) {
        return putData('/api/option', body)
    }

    handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            this.setState({ validated: true });
        } else {

            if (this.state.edit) {
                const body = {
                    _id: this.state.formData._id,
                    term: this.state.formData.term,
                    description: this.state.formData.description,
                }
                submitAlert({ option: 'update', type: 'opción', fn: this.update, data: body, info: this.state.formData.term, gen: 'la' });
            } else {
                const options = this.state.formData.options;
                const body = {
                    ...this.state.formData,
                    ...{
                        options: options.map((o) => ({ term: o })),
                        optionid: this.props.match.params.optionid,
                    }
                }
                submitAlert({ option: 'create', type: 'opción', fn: this.create, data: body, info: this.state.formData.name, gen: 'la' });
            }
        }
        event.preventDefault();
        event.stopPropagation();
    }

    render() {
        const { validated, formOption, formData } = this.state;
        return (
            <div className="row content-view">
                <div className="col-sm-12">
                    <div className="view-tree-container height-70">
                        <div className="view-tree-title">
                            <span className="label"> Editar opción </span>
                        </div>
                        <div className="view-tree-body">
                            <Form
                                noValidate
                                validated={validated}
                                onSubmit={e => this.handleSubmit(e)}>
                                <Form.Row>
                                    {!!formOption && !!formData && (
                                        <FormFieldsBootstrap
                                            formData={formData}
                                            formConfig={formOption}
                                            outputEvent={this.outputFieldBootstrap}
                                        ></FormFieldsBootstrap>
                                    )}
                                </Form.Row>
                                <div className="d-flex flex-row-reverse">
                                    {this.props.match.params.id === '0' && (
                                        <button className="btn btn-primary ml-2" type="submit" value="submit">Nuevo Recurso</button>
                                    )}
                                    {this.props.match.params.id !== '0' && (
                                        <button className="btn btn-primary ml-2" type="submit" value="submit">Actualizar Recurso</button>
                                    )}
                                    <button className="btn btn-secondary ml-2" id="return" onClick={() => this.goback()} type="reset" >Cancelar</button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>

            </div>

        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(OptionForm);
