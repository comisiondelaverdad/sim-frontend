import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { postData, putData, getData } from "../../services/RequestService";
import { Form } from "react-bootstrap";
import { submitAlertList } from './../../services/utils'
import FormFieldsBootstrap from '../../components/organisms/FormFieldsBootstrap';
import Swal from "sweetalert2";
import DownloadResult from "../../components/molecules/DownloadResult";
import { sleep } from '../../services/utils';

const formBase = [
    {
        id: 'origin',
        defaultValue: null,
        label: 'Origen',
        type: 'select',
        col: 9,
        placeholder: 'Seleccione el fondo origen',
        info: '',
        hide: false,
        required: true,
        options: [],
    }

]

var originOptions = [];
    
class ListAlert extends Component {
    constructor(props) {
        super(props);

        this.state = {
            optionSelection: false,
            option: null,
            formList: formBase,
            formData: {},
            edit: false,
            export: "none",
            dataset: [],
            list:null
        };
        this.goback = this.goback.bind(this);
        this.outputFieldBootstrap = this.outputFieldBootstrap.bind(this);
        this.countAlert = this.countAlert.bind(this);
        this.separateResourcesByStatment = this.separateResourcesByStatment.bind(this);
        this.initList = this.initList.bind(this);

    }

    redirect(path) {
        this.props.history.push(`${path}`);
    }

    goback() {
        this.props.history.goBack()
    }

    componentDidMount() {
        this.initList();
    }

    separateResourcesByStatment(resources) {
        let resourcesFAE = [];
        let resourcesFAI = [];

        resources.forEach(resource => {
            if (resource.origin == 'CatalogacionFuentesInternas' || resource.origin == 'CargasFuentesInternas'
                || resource.origin == 'VisualizacionesGeograficas' || resource.origin == 'Visualizaciones') {
                resourcesFAI.push(resource);
            } else {
                resourcesFAE.push(resource);
            }
        });
        return {
            resourcesFAE: resourcesFAE,
            resourcesFAI: resourcesFAI
        }
    }


    async countAlert() {

        var combo = document.getElementById("origin");
        var selected = combo.options[combo.selectedIndex].text;
        var selectedOrigin = originOptions.filter((item) => {
            return item.name === selected;
        });

        var origin = "ModuloCaptura";
        let listDescription = "conflictActors";

        if (this.state.list) {
            listDescription = this.state.list.description;
        }

        if (selectedOrigin) {
            origin = selectedOrigin[0].value;
        }

        Swal.fire({
            title: '¡Por favor espere!',
            html: 'Revisando recursos y fondos...',
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading()
            },
        });

        let count = await postData('/api/list/alert', { "origin": origin, "listDescription": listDescription });

        let resourcesToDownload = count.resourcesNotValid;
        let rGToDownload = count.resourceGroupsNotValid;
        let destiny = count.destiny;

        let countHtml = `                 
                                <div class="meta-list"> 
                                    <span class="form-label">Opciones totales no válidas encontradas: </span>
                                    <div class="meta-value">${count.numberOptions} </div>
                                </div>
                                <div class="meta-list"> 
                                    <span class="form-label">Opciones que corresponden a "Sin especificar": </span>
                                    <div class="meta-value">${count.sinEspecificar} </div>
                                </div>
                                <div class="meta-list"> 
                                    <span class="form-label">Recursos asociados: </span>
                                    <div class="meta-value">${count.numberResourcesNotValid} </div>
                                </div>
                                <div class="meta-list"> 
                                    <span class="form-label">Fondos asociados: </span>
                                    <div class="meta-value">${count.numberRGroupNotValid} </div>
                                </div>
                                `;

        Swal.fire({
            title: `Recursos con opciones no válidas ${this.state.list.path}`,
            html: countHtml,
            cancelButtonText: 'Aceptar',
            width: 400,
            heightAuto: false,
            showCancelButton: count.numberResourcesNotValid > 0 || count.numberRGroupNotValid > 0 ? true : false,
            cancelButtonText: 'Descargar registros',
        }).then(async (result) => {
            Swal.close();
            if (result.dismiss == "cancel") {
                Swal.fire({
                    title: '¡Por favor espere!',
                    html: 'Descargando registros...',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },
                });

                await sleep(20);

                postData(`/api/option/alert`, {listResources: resourcesToDownload, listRG: rGToDownload, destinyName:destiny, listName:listDescription}).then((data) => {
                    let resourcesByStatment = this.separateResourcesByStatment(data.resourcesFlat);
                    let dataset = {
                        statmentFAE: data.statmentFAE,
                        statmentFAI: data.statmentFAI,
                        statmentFondos: data.statmentFondos,
                        resourcegroupsFlat: data.resourcegroupsFlat.length > 0? data.resourcegroupsFlat : null,
                        resourcesFAI: resourcesByStatment.resourcesFAI.length > 0? resourcesByStatment.resourcesFAI : null,
                        resourcesFAE: resourcesByStatment.resourcesFAE.length > 0? resourcesByStatment.resourcesFAE : null
                    }
                    this.setState({ dataset: dataset, export: "actual" });

                });
            }
        });
    }

    async initList() {

        var origins = await getData('/api/resource-groups/origins/');
        originOptions.splice(0, originOptions.length);

        origins.forEach((origin) => {
            var name = origin.split(/(?=[A-Z])/).join(" ");
            originOptions.push({name: name, value:origin});
        }
        );

        let edit = false;
        if (this.props.match.params.optionid !== '0' && this.props.match.params.id !== '0') {
            this.setState({ formData: origin });
            formBase[0].options = originOptions;
        }
        this.setState({ formList: formBase, edit: edit })
        if (this.props.match.params.optionid !== '0' && this.props.match.params.id !== '0') {
            getData('/api/list/byId/' + this.props.match.params.id)
                .then((list) => {
                    this.setState({ list: list });
                })
        }
    }

    outputFieldBootstrap(event) {
        console.log(event);
        this.setState(event);
    }

    render() {
        const { validated, formList, formData } = this.state;
        return (
            <div className="container">
                {this.state.export == "actual" && this.state.dataset.resourcegroupsFlat != null && (
                    <>
                        <DownloadResult dataset={this.state.dataset.resourcegroupsFlat} statment={this.state.dataset.statmentFondos} frmType={"Fondos"} />
                        {this.setState({ export: "none" })}
                    </>

                )}
                {this.state.export == "actual" && this.state.dataset.resourcesFAI != null  &&(
                    <>
                        <DownloadResult dataset={this.state.dataset.resourcesFAI} statment={this.state.dataset.statmentFAI} frmType={"FAI"} />
                        {this.setState({ export: "none" })}
                    </>

                )}
                {this.state.export == "actual" && this.state.dataset.resourcesFAE != null  &&(
                    <>
                        <DownloadResult dataset={this.state.dataset.resourcesFAE} statment={this.state.dataset.statmentFAE} frmType={"FAE"} />
                        {this.setState({ export: "none" })}
                    </>

                )}
                <div className="row content-view justify-content-center align-items-center">
                    <div className="col-sm-12">
                        <div className="view-tree-container height-50">
                            <div className="view-tree-title">
                                <span className="label"> Lista </span>
                            </div>
                            <div className="view-tree-body">
                                <br />
                                <Form
                                    noValidate
                                    validated={validated}
                                    onSubmit={e => this.handleSubmit(e)}>
                                    <Form.Row>
                                        {!!formList && !!formData && (
                                            <FormFieldsBootstrap
                                                formData={formData}
                                                formConfig={formList}
                                                outputEvent={this.outputFieldBootstrap}
                                            ></FormFieldsBootstrap>
                                        )}
                                    </Form.Row>
                                    <Form.Row>
                                        <div className="d-flex flex-row-reverse col-sm-9">
                                            <button className="btn btn-primary ml-2" id="return" onClick={() => this.goback()} type="reset" >Cancelar</button>
                                            <a href="#" class="btn btn-primary ml-2" role="button" aria-pressed="true" onClick={() => this.countAlert()}> Ver alerta sobre esta lista</a>
                                        </div>
                                    </Form.Row>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(ListAlert);
