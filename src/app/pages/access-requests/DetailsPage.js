import React from "react"
import { connect } from "react-redux"
import * as app from "../../store/ducks/app.duck";
import * as AccessRequestService from "../../services/AccessRequestService";
import * as AccessRequestExtensionService from "../../services/AccessRequestExtensionService";
import { Alert, Button, Col, Form, Modal, Row } from "react-bootstrap";
import moment from "moment";
import "moment/locale/es";
import _ from "lodash";
import * as AccessRequestHistoryService from "../../services/AccessRequestHistoryService";
import * as ResourceGroupService from "../../services/ResourceGroupService";
import UserAccessShow from "../../components/atoms/UserAccessShow";
import Swal from "sweetalert2";

moment.locale('es');

class DetailsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            accessRequest: null,
            accessRequestIdent: this.props.match.params.id,
            showPreApproveModal: false,
            preApproveStatus: null,
            preApproveComment: "",
            formValidated: false,
            showApproveModal: false,
            showRejectModal: false,
            approveComment: "",
            rejectComment: "",
            accessRequests: [],
            resourceGroup: undefined,
        };
        this.loadData = this.loadData.bind(this);
        this.handleRenewModalClose = this.handleRenewModalClose.bind(this);
        this.handleRenewModalSubmit = this.handleRenewModalSubmit.bind(this);
        this.handlePreApproveModalClose = this.handlePreApproveModalClose.bind(this);
        this.handlePreApproveModalSubmit = this.handlePreApproveModalSubmit.bind(this);
        this.handleApproveModalClose = this.handleApproveModalClose.bind(this);
        this.handleApproveModalSubmit = this.handleApproveModalSubmit.bind(this);
        this.handleRejectModalClose = this.handleRejectModalClose.bind(this);
        this.handleRejectModalSubmit = this.handleRejectModalSubmit.bind(this);
        this.handleApproveRenewModalClose = this.handleApproveRenewModalClose.bind(this);
        this.handleApproveRenewModalSubmit = this.handleApproveRenewModalSubmit.bind(this);
    }

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        console.log("this.state.accessRequestIdent: ", this.state.accessRequestIdent)
        AccessRequestService.getAccessRequest(this.state.accessRequestIdent).then((data) => {
            this.setState({
                accessRequest: data
            }, this.loadDataAccessRequests());
        }).catch(() => {
            this.setState({
                accessRequest: null
            }, this.loadDataAccessRequests());
        });
    }

    loadDataAccessRequests() {
        AccessRequestService.getAccessRequests().then((data) => {
            const accessRequests = _.filter(data, (r) => {
                return r.resource != null && this.state.accessRequest.user === r.user ? r.user._id ? r.user._id : false : false;
            });
            this.setState({ accessRequests: accessRequests }, this.loadResourceGroup())
        })
    }

    loadResourceGroup() {
        console.log("Cargando resourseGroup ", this.state.accessRequest)
        if (this.state.accessRequest.resource && this.state.accessRequest.resource.ResourceGroupId !== undefined) {
            ResourceGroupService.serviceDetail(this.state.accessRequest.resource.ResourceGroupId).then((data) => {
                this.setState({ resourceGroup: data }, Swal.close())
            })
        }
    }

    ////////////////////////////////////////////////////////////////////////////

    handlePreApproveModalClose() {
        this.setState({ showPreApproveModal: false });
    }

    handlePreApproveModalSubmit(e) {
        e.preventDefault();
        this.setState({ formValidated: true });
        if (!e.target.checkValidity()) {
            return;
        }

        Swal.showLoading()
        const { accessRequest, preApproveStatus, preApproveComment } = this.state;
        let { grantedFrom, grantedTo } = this.state;

        const { user, grantedBy, resource, extensions, history } = accessRequest;

        let extra = ""

        if (grantedFrom === undefined) {
            grantedFrom = accessRequest.requestedFrom
        }
        if (grantedTo === undefined) {
            grantedTo = accessRequest.requestedTo
        }

        if (accessRequest.requestedFrom !== grantedFrom || accessRequest.requestedTo !== grantedTo) {
            extra = `. Las fechas de la solicitud fueron ajustadas por el pre-aprobador, eran: ${accessRequest.requestedFrom.substring(0, 10)
                } a ${accessRequest.requestedTo.substring(0, 10)
                } y cambiaron a: ${grantedFrom.substring(0, 10)
                } a ${grantedTo.substring(0, 10)
                }`
        }

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'validated',
            description: `${preApproveStatus ? 'Se' : 'No se'} pre-aprueba la solicitud de acceso. ${preApproveComment}${extra}`
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            resource: resource._id,
            grantedBy: grantedBy ? grantedBy._id : null,
            preApproveDate: moment().local().format(),
            preApproveStatus: preApproveStatus,
            preApproveComment: preApproveComment,
            requestedFrom: grantedFrom,
            requestedTo: grantedTo,
            status: 'validated',
            extensions: extensions.map(e => e._id)
        }, _.identity);

        AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                history: history.map(e => e._id).concat([data._id])
            });
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ preApproveStatus: null, preApproveComment: null, showPreApproveModal: false, formValidated: false });
    }


    handleApproveModalClose() {
        this.setState({ showApproveModal: false });
    }

    handleApproveModalSubmit(e) {
        e.preventDefault();
        this.setState({ formValidated: true });
        if (!e.target.checkValidity()) {
            return;
        }

        Swal.showLoading()
        const { accessRequest, approveComment } = this.state;
        let { grantedFrom, grantedTo } = this.state;
        const { user, resource, extensions, history } = accessRequest;

        if (grantedFrom === undefined) {
            grantedFrom = accessRequest.requestedFrom
        }
        if (grantedTo === undefined) {
            grantedTo = accessRequest.requestedTo
        }

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'approved',
            description: `Solicitud de acceso aprobada por ${moment(grantedFrom, 'YYYY/MM/DD').local().to(moment(grantedTo, 'YYYY/MM/DD').local(), true)
                } del ${moment(grantedFrom, 'YYYY/MM/DD').local().format('D')
                } de ${moment(grantedFrom, 'YYYY/MM/DD').local().format('MMMM')
                } de ${moment(grantedFrom, 'YYYY/MM/DD').local().format('YYYY')
                } al ${moment(grantedTo, 'YYYY/MM/DD').local().format('D')
                } de ${moment(grantedTo, 'YYYY/MM/DD').local().format('MMMM')
                } de ${moment(grantedTo, 'YYYY/MM/DD').local().format('YYYY')
                }.\n${approveComment
                }`
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            status: 'approved',
            resource: resource._id,
            grantDate: moment().local().format(),
            grantedBy: this.props.user._id,
            grantedFrom: moment(grantedFrom, 'YYYY/MM/DD').local().format(),
            grantedTo: moment(grantedTo, 'YYYY/MM/DD').local().format(),
            extensions: extensions.map(e => e._id),
            approveComment: approveComment,
        }, _.identity);

        AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                history: history.map(e => e._id).concat([data._id])
            });
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ showApproveModal: false, formValidated: false });
    }

    handleRejectModalClose() {
        this.setState({ showRejectModal: false });
    }

    handleRejectModalSubmit(e) {
        e.preventDefault();

        Swal.showLoading()
        const { accessRequest, rejectComment } = this.state;
        const { user, resource, extensions, history } = accessRequest;

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'rejected',
            description: `Se rechaza la solicitud de acceso.\n${rejectComment}`
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            status: 'rejected',
            resource: resource._id,
            rejectionDate: moment().local().format(),
            extensions: extensions.map(e => e._id),
            rejectComment: rejectComment,
        }, _.identity);

        AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                history: history.map(e => e._id).concat([data._id])
            });
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ showRejectModal: false, formValidated: false });
    }

    handleRenewModalClose() {
        this.setState({ showRenewModal: false });
    }

    handleRenewModalSubmit(e) {
        e.preventDefault();
        this.setState({ formValidated: true });
        if (!e.target.checkValidity()) {
            return;
        }

        Swal.showLoading()
        const { accessRequest, extendFrom, extendTo } = this.state;
        const { user, grantedBy, resource, extensions, history } = accessRequest;

        const extensionToCreate = {
            user: user._id,
            fate: moment().local().format(),
            from: extendFrom,
            to: extendTo,
            status: 'pending'
        };

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'renewalcreated',
            description: 'Se solicita renovación de acceso al recurso'
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            grantedBy: grantedBy ? grantedBy._id : null,
            resource: resource._id
        }, _.identity);

        const promise1 = AccessRequestExtensionService.createAccessRequestExtension(extensionToCreate);

        const promise2 = AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate);

        Promise.all([promise1, promise2]).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                extensions: extensions.map(e => e._id).concat([data[0]._id]),
                history: history.map(e => e._id).concat([data[1]._id])
            });
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ showRenewModal: false, formValidated: false });
    }


    handleApproveRenewModalClose() {
        this.setState({ showApproveRenewModal: false });
    }

    handleApproveRenewModalSubmit(e) {
        e.preventDefault();
        this.setState({ formValidated: true });
        if (!e.target.checkValidity()) {
            return;
        }

        Swal.showLoading()
        const { accessRequest, grantedFrom, grantedTo } = this.state;
        const { user, grantedBy, resource, extensions, history } = accessRequest;

        const extensionToUpdate = _.pickBy({
            ...extensions.find(e => e.status === 'pending') || {},
            status: 'processed'
        }, _.identity);

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'renewalapproved',
            description: `Se aprueba la solicitud de renovación de acceso al recurso por ${moment(grantedFrom, 'YYYY/MM/DD').local().to(moment(grantedTo, 'YYYY/MM/DD').local(), true)} del ${moment(grantedFrom, 'YYYY/MM/DD').local().format('D')} de ${moment(grantedFrom, 'YYYY/MM/DD').local().format('MMMM')} de ${moment(grantedFrom, 'YYYY/MM/DD').local().format('YYYY')} al ${moment(grantedTo, 'YYYY/MM/DD').local().format('D')} de ${moment(grantedTo, 'YYYY/MM/DD').local().format('MMMM')} de ${moment(grantedTo, 'YYYY/MM/DD').local().format('YYYY')}.`
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            grantedFrom: grantedFrom,
            grantedTo: grantedTo,
            resource: resource._id,
            extensions: extensions.map(e => e._id),
            grantedBy: grantedBy ? grantedBy._id : null,
            status: 'approved',
            rejectionDate: null
        }, _.identity);

        AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                history: history.map(e => e._id).concat([data._id])
            });
        }).then(() => {
            return AccessRequestExtensionService.updateAccessRequestExtension(extensionToUpdate);
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ showApproveRenewModal: false, formValidated: false });
    }

    render() {
        this.props.pageTitle('Detalle solicitud de acceso');
        this.props.pageSubtitle('');
        let status = {}
        let accessRequest = this.state.accessRequest
        if (accessRequest) {
            status = [accessRequest.status].concat(
                accessRequest.extensions.some(e => e.status === 'pending') &&
                    !this.state.accessRequests.some(r => r.resource.ident === accessRequest.resource.ident &&
                        moment(r.requestDate).local().isAfter(moment(accessRequest.requestDate).local())) ? ['renewal'] : []
            ).concat(
                accessRequest.status === 'approved' &&
                    moment(accessRequest.grantedTo).local().isBefore(moment().local()) ? ['expired'] : []
            ).concat(
                (accessRequest.status === 'approved' || accessRequest.status === 'rejected') &&
                    accessRequest.extensions.every(e => e.status === 'processed') &&
                    !this.state.accessRequests.some(
                        r => r.resource.ident === accessRequest.resource.ident &&
                            moment(r.requestDate).local().isAfter(moment(accessRequest.requestDate).local())
                    ) ? ['renewable'] : []
            )
        }

        return (
            <>
                {!this.state.accessRequest &&
                    <div className="cev-container cev-container--fluid cev-grid__item cev-grid__item--fluid">
                        <Alert variant="secondary">
                            <div className="alert-icon">
                                <i className="flaticon-warning" />
                            </div>
                            <div className="alert-text">Esta solicitud de acceso no se encuentra en el sistema</div>
                        </Alert>
                    </div>
                }
                {this.state.accessRequest && !this.state.accessRequest.resource &&
                    <div className="cev-container cev-container--fluid cev-grid__item cev-grid__item--fluid">
                        <Alert variant="secondary">
                            <div className="alert-icon">
                                <i className="flaticon-warning" />
                            </div>
                            <div className="alert-text">Este recurso no se encuentra disponible en el momento</div>
                        </Alert>
                    </div>
                }
                {
                    this.state.accessRequest && this.state.accessRequest.resource &&
                    <div className="row">
                        <Modal
                            show={this.state.showPreApproveModal}
                            onHide={this.handlePreApproveModalClose}
                            size="lg"
                        >
                            <Form noValidate validated={this.state.formValidated} onSubmit={this.handlePreApproveModalSubmit}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Pre-aprobar acceso a recurso</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Alert key={0} variant={'warning'}>
                                        <div className="alert-icon">
                                            <i className="flaticon-warning" />
                                        </div>
                                        <div className="alert-text">
                                            Pre-aprobará acceso temporal a un recurso clasificado, indique el motivo por el cual
                                            considera que se debe otorgar acceso a este recurso.
                                        </div>
                                    </Alert>
                                    <Form.Group as={Row}>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            <span>Fecha de inicio pre-aprobada</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={3}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                min={moment().local().format('YYYY-MM-DD')}
                                                defaultValue={moment(this.state.accessRequest.requestedFrom).local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({ grantedFrom: moment(e.target.value, 'YYYY-MM-DD').local().format() })
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                        </Col>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            Fecha solicitada
                                        </Form.Label>
                                        <Col sm={2}>
                                            {this.state.accessRequest &&
                                                <Form.Control
                                                    disabled
                                                    as="input"
                                                    type="text"
                                                    defaultValue={moment(this.state.accessRequest.requestedFrom).local().format('YYYY-MM-DD')}
                                                />
                                            }
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            <span>Fecha de fin pre-aprobada</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={3}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                min={moment(this.state.grantedFrom ? this.state.grantedFrom : undefined).local().format('YYYY-MM-DD')}
                                                defaultValue={moment(this.state.accessRequest.requestedTo).local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({ grantedTo: moment(e.target.value, 'YYYY-MM-DD').local().endOf('day').format() })
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                        </Col>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            Fecha solicitada
                                        </Form.Label>
                                        <Col sm={2}>
                                            {this.state.accessRequest &&
                                                <Form.Control
                                                    disabled
                                                    as="input"
                                                    type="text"
                                                    defaultValue={moment(this.state.accessRequest.requestedTo).local().format('YYYY-MM-DD')}
                                                />
                                            }
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} controlId="preApproveForm.Radio" style={{ marginBottom: '0px', marginLeft: '0px' }}>
                                        <Form.Label column sm={`12`}>
                                            <span>¿Pre-aprueba acceso?</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={`12`}>
                                            <>
                                                <Form.Check>
                                                    <Form.Check.Input
                                                        required
                                                        type={`radio`}
                                                        id={`inline-radio-1`}
                                                        name={`confirm-pre-approve`}
                                                        onChange={(e) => {
                                                            this.setState({ preApproveStatus: e.target.value === "on" })
                                                        }}
                                                    />
                                                    <Form.Check.Label>{`Si`}</Form.Check.Label>
                                                </Form.Check>
                                                <Form.Check>
                                                    <Form.Check.Input
                                                        required
                                                        type={`radio`}
                                                        id={`inline-radio-2`}
                                                        name={`confirm-pre-approve`}
                                                        onChange={(e) => {
                                                            this.setState({ preApproveStatus: e.target.value === "off" })
                                                        }}
                                                    />
                                                    <Form.Check.Label>{`No`}</Form.Check.Label>
                                                    <Form.Control.Feedback type="invalid" style={{ marginLeft: '-25px' }}>Este campo es requerido</Form.Control.Feedback>
                                                </Form.Check>
                                            </>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group controlId="preApproveForm.Textarea">
                                        <Form.Label column="false">
                                            <span>Observaciones de pre-aprobación / desaprobación</span>
                                            {/* <span className="text-danger">*</span> */}
                                        </Form.Label>
                                        <Form.Control

                                            as="textarea"
                                            rows="3"
                                            onChange={(e) => { this.setState({ preApproveComment: e.target.value }) }}
                                        />
                                        {/* <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback> */}
                                    </Form.Group>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button className="pull-left" variant="primary" type="submit">
                                        Pre-aprobar
                                    </Button>
                                    <Button className="pull-left" variant="secondary" onClick={this.handlePreApproveModalClose}>
                                        Cancelar
                                    </Button>
                                </Modal.Footer>
                            </Form>
                        </Modal>
                        <Modal
                            show={this.state.showApproveModal}
                            onHide={this.handleApproveModalClose}
                            size="lg"
                        >
                            <Form noValidate validated={this.state.formValidated} onSubmit={this.handleApproveModalSubmit}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Aprobar acceso a recurso</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Alert key={0} variant={'warning'}>
                                        <div className="alert-icon">
                                            <i className="flaticon-warning" />
                                        </div>
                                        <div className="alert-text">
                                            Aprobará acceso temporal a un recurso clasificado, indique el rango de fechas en las
                                            cuales el usuario tendrá acceso.
                                        </div>
                                    </Alert>
                                    <Form.Group as={Row}>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            <span>Fecha de inicio</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={3}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                min={moment().local().format('YYYY-MM-DD')}
                                                defaultValue={moment(this.state.accessRequest.requestedFrom).local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({ grantedFrom: moment(e.target.value, 'YYYY-MM-DD').local().format() })
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                        </Col>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            Fecha solicitada
                                        </Form.Label>
                                        <Col sm={2}>
                                            {this.state.accessRequest &&
                                                <Form.Control
                                                    disabled
                                                    as="input"
                                                    type="text"
                                                    defaultValue={moment(this.state.accessRequest.requestedFrom).local().format('YYYY-MM-DD')}
                                                />
                                            }
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            <span>Fecha de fin</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={3}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                defaultValue={moment(this.state.accessRequest.requestedTo).local().format('YYYY-MM-DD')}
                                                min={moment(this.state.grantedFrom ? this.state.grantedFrom : undefined).local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({ grantedTo: moment(e.target.value, 'YYYY-MM-DD').local().endOf('day').format() })
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                        </Col>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            Fecha solicitada
                                        </Form.Label>
                                        <Col sm={2}>
                                            {this.state.accessRequest &&
                                                <Form.Control
                                                    disabled
                                                    as="input"
                                                    type="text"
                                                    defaultValue={moment(this.state.accessRequest.requestedTo).local().format('YYYY-MM-DD')}
                                                />
                                            }
                                        </Col>
                                    </Form.Group>
                                    <Form.Group controlId="preApproveForm.Textarea">
                                        <Form.Label column="false">
                                            <span>Observaciones de aprobación</span>
                                        </Form.Label>
                                        <Form.Control
                                            required
                                            as="textarea"
                                            rows="3"
                                            onChange={(e) => { this.setState({ approveComment: e.target.value }) }}
                                        />
                                    </Form.Group>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button className="pull-left" variant="primary" type="submit">
                                        Aprobar
                                    </Button>
                                    <Button className="pull-left" variant="secondary" onClick={this.handleApproveModalClose}>
                                        Cancelar
                                    </Button>
                                </Modal.Footer>
                            </Form>
                        </Modal>
                        <Modal
                            show={this.state.showRejectModal}
                            onHide={this.handleRejectModalClose}
                            size="md"
                        >
                            <Form onSubmit={this.handleRejectModalSubmit}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Rechazar acceso a recurso</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Form.Group controlId="preApproveForm.Textarea">
                                        <Form.Label column="false">
                                            <span>Observaciones de desaprobación</span>
                                        </Form.Label>
                                        <Form.Control
                                            required
                                            as="textarea"
                                            rows="3"
                                            onChange={(e) => { this.setState({ rejectComment: e.target.value }) }}
                                        />
                                    </Form.Group>
                                    <Alert key={0} variant={'warning'}>
                                        <div className="alert-icon">
                                            <i className="flaticon-warning" />
                                        </div>
                                        <div className="alert-text">
                                            ¿Está seguro que desea rechazar el acceso a este recurso?
                                        </div>
                                    </Alert>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button className="pull-left" variant="primary" type="submit">
                                        Rechazar
                                    </Button>
                                    <Button className="pull-left" variant="secondary" onClick={this.handleRejectModalClose}>
                                        Cancelar
                                    </Button>
                                </Modal.Footer>
                            </Form>
                        </Modal>
                        <Modal
                            show={this.state.showRenewModal}
                            onHide={this.handleRenewModalClose}
                            size="lg"
                        >
                            <Form noValidate validated={this.state.formValidated} onSubmit={this.handleRenewModalSubmit}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Solicitud de renovación de acceso</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Alert key={0} variant={'warning'}>
                                        <div className="alert-icon">
                                            <i className="flaticon-warning" />
                                        </div>
                                        <div className="alert-text">
                                            Solicitará renovación de acceso a un recurso clasificado, indique el rango de fechas
                                            en las cuales desea extender su acceso
                                        </div>
                                    </Alert>
                                    <Form.Group as={Row}>
                                        <Form.Label column="false" sm={{ span: 3, offset: 3 }}>
                                            <span>Fecha de inicio</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={{ span: 3 }}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                min={moment().local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({ extendFrom: moment(e.target.value, 'YYYY-MM-DD').local().format() })
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es
                                                requerido</Form.Control.Feedback>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column="false" sm={{ span: 3, offset: 3 }}>
                                            <span>Fecha de fin</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={{ span: 3 }}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                min={moment(this.state.extendFrom ? this.state.extendFrom : undefined).local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({ extendTo: moment(e.target.value, 'YYYY-MM-DD').local().endOf('day').format() })
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es
                                                requerido</Form.Control.Feedback>
                                        </Col>
                                    </Form.Group>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button className="pull-left" variant="primary" type="submit">
                                        Enviar solicitud
                                    </Button>
                                    <Button className="pull-left" variant="secondary" onClick={this.handleRenewModalClose}>
                                        Cancelar
                                    </Button>
                                </Modal.Footer>
                            </Form>
                        </Modal>
                        <Modal
                            show={this.state.showApproveRenewModal}
                            onHide={this.handleApproveRenewModalClose}
                            size="lg"
                        >
                            <Form noValidate validated={this.state.formValidated} onSubmit={this.handleApproveRenewModalSubmit}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Renovar acceso a recurso</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Alert key={0} variant={'warning'}>
                                        <div className="alert-icon">
                                            <i className="flaticon-warning" />
                                        </div>
                                        <div className="alert-text">
                                            Aprobará renovación de acceso temporal a un recurso clasificado, indique el rango de
                                            fechas en las cuales el usuario tendrá acceso.
                                        </div>
                                    </Alert>
                                    <Form.Group as={Row}>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            <span>Fecha de inicio</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={3}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                min={moment().local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({ grantedFrom: moment(e.target.value, 'YYYY-MM-DD').local().format() })
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                        </Col>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            Fecha solicitada
                                        </Form.Label>
                                        <Col sm={2}>
                                            {this.state.accessRequest &&
                                                <Form.Control
                                                    disabled
                                                    as="input"
                                                    type="text"
                                                    defaultValue={moment((this.state.accessRequest.extensions.find(e => e.status === 'pending') || {}).from).local().format('DD/MM/YYYY')}
                                                />
                                            }
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row}>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            <span>Fecha de fin</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={3}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                min={moment(this.state.grantedFrom ? this.state.grantedFrom : undefined).local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({ grantedTo: moment(e.target.value, 'YYYY-MM-DD').local().endOf('day').format() })
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                        </Col>
                                        <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                            Fecha solicitada
                                        </Form.Label>
                                        <Col sm={2}>
                                            {this.state.accessRequest &&
                                                <Form.Control
                                                    disabled
                                                    as="input"
                                                    type="text"
                                                    defaultValue={moment((this.state.accessRequest.extensions.find(e => e.status === 'pending') || {}).to).local().format('DD/MM/YYYY')}
                                                />
                                            }
                                        </Col>
                                    </Form.Group>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button className="pull-left" variant="primary" type="submit">
                                        Aprobar
                                    </Button>
                                    <Button className="pull-left" variant="secondary" onClick={this.handleApproveRenewModalClose}>
                                        Cancelar
                                    </Button>
                                </Modal.Footer>
                            </Form>
                        </Modal>
                        <div className="col-md-6">
                            <div className="card-cev height-80">
                                <div className="card-cev-title"> <span className="label">Ficha de solicitud</span> </div>
                                <div className="card-cev-body">
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">ID Solicitud:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">{this.state.accessRequest.ident}</span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Fecha solicitud:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">
                                                <span style={{ textTransform: 'capitalize' }}>
                                                    {moment(this.state.accessRequest.requestDate).local().format('dddd')}
                                                </span>
                                                <span>
                                                    {moment(this.state.accessRequest.requestDate).local().format(', D [de] ')}
                                                </span>
                                                <span style={{ textTransform: 'capitalize' }}>
                                                    {moment(this.state.accessRequest.requestDate).local().format('MMMM')}
                                                </span>
                                                <span>
                                                    {moment(this.state.accessRequest.requestDate).local().format(' [de] YYYY, h:mm a')}
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Fecha solicitud inicio:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">{this.state.accessRequest.requestedFrom.substring(0, 10)}</span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Fecha solicitud fin:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">{this.state.accessRequest.requestedTo.substring(0, 10)}</span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Estado:</label>
                                        <div className="col-8">
                                            {
                                                this.state.accessRequest.status === 'pending' &&
                                                <span className="form-control-plaintext">Pendiente</span>
                                            }
                                            {
                                                this.state.accessRequest.status === 'validated' &&
                                                <span className="form-control-plaintext">Pre-aprobada</span>
                                            }
                                            {
                                                this.state.accessRequest.status === 'approved' &&
                                                <span className="form-control-plaintext">Aprobada</span>
                                            }
                                            {
                                                this.state.accessRequest.status === 'rejected' &&
                                                <span className="form-control-plaintext">Rechazada</span>
                                            }
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">ID Recurso:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">
                                                <a href={`/detail/${this.state.accessRequest.resource.ident}`}>
                                                    {this.state.accessRequest.resource.ident}
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Fondo:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">
                                                {this.state.accessRequest.resource.ResourceGroupId &&
                                                    <>{this.state.accessRequest.resource.ResourceGroupId}<br />
                                                        {this.state.resourceGroup && this.state.resourceGroup.metadata.firstLevel.title}</>
                                                }
                                                {!this.state.accessRequest.resource.ResourceGroupId &&
                                                    <>-</>
                                                }
                                            </span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Nivel:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">{this.state.accessRequest.resource.metadata.firstLevel.accessLevel}</span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Nombre del solicitante:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">{this.state.accessRequest.user.name}</span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Correo del solicitante:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">{this.state.accessRequest.user.username}</span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Titulo:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">{this.state.accessRequest.resource.metadata.firstLevel.title}</span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Creador:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">{this.state.accessRequest.resource.metadata.firstLevel.creator}</span>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-xs row">
                                        <label className="col-4 col-form-label">Tipo:</label>
                                        <div className="col-8">
                                            <span className="form-control-plaintext">{this.state.accessRequest.resource.type}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="card-cev height-80">
                                <div className="card-cev-title"> <span className="label">Historial</span> </div>
                                <div className="card-cev-body">
                                    <div className="cev-notes">
                                        <div className="cev-notes__items">
                                            {this.state.accessRequest.history && this.state.accessRequest.history.map((accessRequestHistory, idx) => (
                                                <>
                                                    <div className="cev-notes__item" style={{ paddingBottom: '20px' }}>
                                                        <div className="cev-notes__media">
                                                            <span className="cev-notes__icon">
                                                                {accessRequestHistory.status === 'created' &&
                                                                    <i className="flaticon2-rocket-1" />
                                                                }
                                                                {accessRequestHistory.status === 'validated' &&
                                                                    <i className="flaticon2-cup" />
                                                                }
                                                                {accessRequestHistory.status === 'approved' &&
                                                                    <i className="flaticon2-cup" />
                                                                }
                                                                {accessRequestHistory.status === 'rejected' &&
                                                                    <i className="flaticon-delete" />
                                                                }
                                                                {accessRequestHistory.status === 'renewalcreated' &&
                                                                    <i className="flaticon2-box-1" />
                                                                }
                                                                {accessRequestHistory.status === 'renewalapproved' &&
                                                                    <i className="flaticon2-cup" />
                                                                }
                                                            </span>
                                                        </div>
                                                        <div className="cev-notes__content">
                                                            <div className="cev-notes__section">
                                                                <div className="cev-notes__info">
                                                                    <a href={`#${accessRequestHistory.status}`} className="cev-notes__title" style={{ pointerEvents: 'none' }}>
                                                                        {accessRequestHistory.status === 'created' &&
                                                                            <>Solicitud creada</>
                                                                        }
                                                                        {accessRequestHistory.status === 'validated' &&
                                                                            <>Solicitud pre-aprobada</>
                                                                        }
                                                                        {accessRequestHistory.status === 'approved' &&
                                                                            <>Solicitud aprobada</>
                                                                        }
                                                                        {accessRequestHistory.status === 'rejected' &&
                                                                            <>Solicitud rechazada</>
                                                                        }
                                                                        {accessRequestHistory.status === 'renewalcreated' &&
                                                                            <>Solicitud de renovación creada</>
                                                                        }
                                                                        {accessRequestHistory.status === 'renewalapproved' &&
                                                                            <>Solicitud renovada</>
                                                                        }
                                                                    </a>
                                                                    <span className="cev-notes__desc">
                                                                        <span style={{ textTransform: 'capitalize' }}>{moment(accessRequestHistory.date).local().format('dddd')}</span>
                                                                        <span>{moment(accessRequestHistory.date).local().format(', D [de] ')}</span>
                                                                        <span style={{ textTransform: 'capitalize' }}>{moment(accessRequestHistory.date).local().format('MMMM')}</span>
                                                                        <span>{moment(accessRequestHistory.date).local().format(' [de] YYYY, h:mm a')}</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <span className="cev-notes__body">
                                                                <dl>
                                                                    <dt>
                                                                        {accessRequestHistory.status === 'created' &&
                                                                            <>Solicitante</>
                                                                        }
                                                                        {accessRequestHistory.status === 'validated' &&
                                                                            <>Revisor</>
                                                                        }
                                                                        {accessRequestHistory.status === 'approved' &&
                                                                            <>Aprobador</>
                                                                        }
                                                                        {accessRequestHistory.status === 'rejected' &&
                                                                            <>Aprobador</>
                                                                        }
                                                                        {accessRequestHistory.status === 'renewalcreated' &&
                                                                            <>Solicitante</>
                                                                        }
                                                                        {accessRequestHistory.status === 'renewalapproved' &&
                                                                            <>Aprobador</>
                                                                        }
                                                                    </dt>
                                                                    <dd>{accessRequestHistory.user.name}</dd>
                                                                    <dt>
                                                                        {accessRequestHistory.status === 'created' &&
                                                                            <>Justificación</>
                                                                        }
                                                                        {accessRequestHistory.status === 'validated' &&
                                                                            <>Detalle</>
                                                                        }
                                                                        {accessRequestHistory.status === 'approved' &&
                                                                            <>Detalle</>
                                                                        }
                                                                        {accessRequestHistory.status === 'rejected' &&
                                                                            <>Detalle</>
                                                                        }
                                                                        {accessRequestHistory.status === 'renewalcreated' &&
                                                                            <>Detalle</>
                                                                        }
                                                                        {accessRequestHistory.status === 'renewalapproved' &&
                                                                            <>Detalle</>
                                                                        }
                                                                    </dt>
                                                                    <dd>{accessRequestHistory.description}</dd>
                                                                </dl>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </>
                                            ))}
                                            <div className="cev-notes__item" style={{ paddingBottom: '20px' }}>
                                                <div className="cev-notes__media">
                                                    <span className="cev-notes__icon">
                                                        <i className="flaticon2-box-1" />
                                                    </span>
                                                </div>
                                                <div className="cev-notes__content">
                                                    <div className="cev-notes__section">
                                                        <div className="cev-notes__info">
                                                            <span className="cev-notes__title" style={{ pointerEvents: 'none' }}>Acciones</span>
                                                        </div>
                                                    </div>
                                                    <span className="cev-notes__body">

                                                        <UserAccessShow rol="user">
                                                            {status.includes('renewable') &&
                                                                <span>
                                                                    Solicitar renovación
                                                                    <button onClick={() => this.setState({ "showRenewModal": true })} className="btn btn-primary" id="cev_button_request_renewal" title="Solicitar renovación"><i className="la la-refresh"></i></button>
                                                                </span>
                                                            }
                                                        </UserAccessShow>

                                                        <UserAccessShow rol="reviewer">
                                                            {this.state.accessRequest.status === 'pending' &&
                                                                <span>

                                                                    <button onClick={() => this.setState({ "showPreApproveModal": true })} className="btn btn-primary" id="cev_button_pre_approve" title="Pre-aprobar solicitud">
                                                                        <i className="la la-edit"></i>
                                                                        Pre-aprobar solicitud
                                                                    </button>

                                                                </span>
                                                            }
                                                        </UserAccessShow>

                                                        <UserAccessShow rol="approver">
                                                            {this.state.accessRequest.status === 'validated' &&
                                                                <>
                                                                    <div>
                                                                        <button onClick={() => this.setState({ "showApproveModal": true })} className="btn btn-primary" id="cev_button_approve" title="Aprobar solicitud">
                                                                            <i className="la la-check-circle"></i>
                                                                            Aprobar solicitud
                                                                        </button>
                                                                    </div>
                                                                    <div>



                                                                        <button onClick={() => this.setState({ "showRejectModal": true })} className="btn btn-primary" id="cev_button_reject" title="Rechazar solicitud"><i className="la la-times-circle"></i>
                                                                            Rechazar solicitud
                                                                        </button>
                                                                    </div>
                                                                </>
                                                            }
                                                            {
                                                                this.state.accessRequest.extensions.some(e => e.status === 'pending') &&
                                                                !this.state.accessRequests.some(
                                                                    r => r.resource.ident === this.state.accessRequest.resource.ident &&
                                                                        moment(r.requestDate).isAfter(moment(this.state.accessRequest.requestDate))
                                                                ) &&
                                                                <>
                                                                    <div>
                                                                        Aprobar renovación
                                                                        <button onClick={() => this.setState({ "showApproveRenewModal": true })} className="btn btn-primary" id="cev_button_approve_renewal" title="Aprobar renovación"><i className="la la-check-square"></i></button>
                                                                    </div>
                                                                </>
                                                            }
                                                        </UserAccessShow>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(DetailsPage);
