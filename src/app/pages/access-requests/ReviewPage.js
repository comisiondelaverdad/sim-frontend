import React, { Component } from "react";
import * as AccessRequestService from "../../services/AccessRequestService";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { Alert, Button, Col, Form, Modal, Row } from "react-bootstrap";
import moment from "moment";
import _ from "lodash";
import * as AccessRequestHistoryService from "../../services/AccessRequestHistoryService";
import { Redirect } from "react-router-dom";
import DataTable from "../../components/molecules/DataTable";
import { ACCESS_REQUEST_COLUMNS } from "../../config/const";

class ReviewPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAdmin: this.props.match.path.endsWith('/approve-access-requests'),
            isReviewer: this.props.match.path.endsWith('/review-access-requests'),
            isUser: this.props.match.path.endsWith('/my-access-requests'),
            accessRequest: null,
            accessRequests: [],
            showPreApproveModal: false,
            preApproveStatus: null,
            preApproveComment: null,
            formValidated: false,
            loading: true,
        };
        this.props.pageTitle("Administrar solicitudes de acceso");
        this.handlePreApproveModalClose = this.handlePreApproveModalClose.bind(this);
        this.handlePreApproveModalSubmit = this.handlePreApproveModalSubmit.bind(this);
        this.loadData = this.loadData.bind(this);
        this.showDetails = this.showDetails.bind(this);
        this.showPreApproveModal = this.showPreApproveModal.bind(this);
    }

    componentDidMount() {
        this.loadData();
    }

    handlePreApproveModalClose() {
        this.setState({ accessRequest: null, showPreApproveModal: false });
    }

    handlePreApproveModalSubmit(e) {
        e.preventDefault();
        this.setState({ formValidated: true });
        if (!e.target.checkValidity()) {
            return;
        }

        const { accessRequest, preApproveStatus, preApproveComment } = this.state;
        const { user, grantedBy, resource, extensions, history } = accessRequest;

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'validated',
            description: `${preApproveStatus ? 'Se' : 'No se'} pre-aprueba la solicitud de acceso. ${preApproveComment}`
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            resource: resource._id,
            grantedBy: grantedBy ? grantedBy._id : null,
            preApproveDate: moment().local().format(),
            preApproveStatus: preApproveStatus,
            preApproveComment: preApproveComment,
            status: 'validated',
            extensions: extensions.map(e => e._id)
        }, _.identity);

        AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                history: history.map(e => e._id).concat([data._id])
            });
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ accessRequest: null, preApproveStatus: null, preApproveComment: null, showPreApproveModal: false, formValidated: false });
    }

    loadData() {
        AccessRequestService.getAccessRequests().then((data) => {
            const isAdmin = this.props.match.path.endsWith('/approve-access-requests');
            const isReviewer = this.props.match.path.endsWith('/review-access-requests');
            const isUser = this.props.match.path.endsWith('/my-access-requests');
            const accessRequests = _.filter(data, (r) => {
                return r.resource != null;
            });
            const isRequestForRenewal = r => r.extensions.some(e => e.status === 'pending');
            const isRequestNotRecent = r => accessRequests.some(ar => ar.resource.ident === r.resource.ident && moment(ar.requestDate).local().isBefore(moment(r.requestDate).local()));
            const isRequestValidated = r => r.status === 'validated';
            const adminFilter = r => isRequestValidated(r) || (isRequestForRenewal(r) && isRequestNotRecent(r));
            const isRequestPending = r => r.status === 'pending';
            const reviewerFilter = r => isRequestPending(r);
            const isRequestOwner = r => this.props.user._id === r.user._id;
            const userFilter = r => isRequestOwner(r);
            const accessRequestsCount = accessRequests.filter(r => (isAdmin ? adminFilter(r) : (isReviewer ? reviewerFilter(r) : (isUser ? userFilter(r) : true)))).length;
            this.setState({
                accessRequests: accessRequests,
                loading: false
            });
            if (isAdmin && accessRequestsCount === 1) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitud pendiente por aprobar o renovar`);
            } else if (isAdmin && (accessRequestsCount === 0 || accessRequestsCount > 1)) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitudes pendientes por aprobar o renovar`);
            } else if (isReviewer && accessRequestsCount === 1) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitud pendiente por pre-aprobar`);
            } else if (isReviewer && (accessRequestsCount === 0 || accessRequestsCount > 1)) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitudes pendientes por pre-aprobar`);
            } else {
                this.props.pageSubtitle(`${accessRequestsCount} solicitudes`);
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    showDetails(rowIndex) {
        const accessRequest = this.state.accessRequests[rowIndex];
        const accessRequestIdent = accessRequest.ident;
        this.props.history.push(`/access-requests/${accessRequestIdent}`);
    }

    showPreApproveModal(rowIndex) {
        const accessRequest = this.state.accessRequests[rowIndex];
        this.setState({ accessRequest: accessRequest, showPreApproveModal: true });
    }

    render() {
        const { accessRequests } = this.state;
        const { user } = this.props;
        return (
            <>
                <>
                    {user.roles && !user.roles.includes('reviewer') &&
                        <Redirect to={'/dashboard'} />
                    }
                </>
                <Modal
                    show={this.state.showPreApproveModal}
                    onHide={this.handlePreApproveModalClose}
                    size="lg"
                >
                    <Form noValidate validated={this.state.formValidated} onSubmit={this.handlePreApproveModalSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Pre-aprobar acceso a recurso</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Alert key={0} variant={'warning'}>
                                <div className="alert-icon">
                                    <i className="flaticon-warning" />
                                </div>
                                <div className="alert-text">
                                    Pre-aprobará acceso temporal a un recurso clasificado, indique el motivo por el cual
                                    considera que se debe otorgar acceso a este recurso.
                                </div>
                            </Alert>
                            <Form.Group as={Row} controlId="preApproveForm.Radio" style={{ marginBottom: '0px', marginLeft: '0px' }}>
                                <Form.Label column sm={`12`}>
                                    <span>¿Pre-aprueba acceso?</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Col sm={`12`}>
                                    <>
                                        <Form.Check>
                                            <Form.Check.Input
                                                required
                                                type={`radio`}
                                                id={`inline-radio-1`}
                                                name={`confirm-pre-approve`}
                                                onChange={(e) => {
                                                    this.setState({ preApproveStatus: e.target.value === "on" })
                                                }}
                                            />
                                            <Form.Check.Label>{'Sí'}</Form.Check.Label>
                                        </Form.Check>
                                        <Form.Check>
                                            <Form.Check.Input
                                                required
                                                type={`radio`}
                                                id={`inline-radio-2`}
                                                name={`confirm-pre-approve`}
                                                onChange={(e) => {
                                                    this.setState({ preApproveStatus: e.target.value === "off" })
                                                }}
                                            />
                                            <Form.Check.Label>{`No`}</Form.Check.Label>
                                            <Form.Control.Feedback type="invalid" style={{ marginLeft: '-25px' }}>Este campo es requerido</Form.Control.Feedback>
                                        </Form.Check>
                                    </>
                                </Col>
                            </Form.Group>
                            <Form.Group controlId="preApproveForm.Textarea">
                                <Form.Label column="false">
                                    <span>Observaciones de pre-aprobación / desaprobación</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Form.Control
                                    required
                                    as="textarea"
                                    rows="3"
                                    onChange={(e) => { this.setState({ preApproveComment: e.target.value }) }}
                                />
                                <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="pull-left" variant="primary" type="submit">
                                Pre-aprobar
                            </Button>
                            <Button className="pull-left" variant="secondary" onClick={this.handlePreApproveModalClose}>
                                Cancelar
                            </Button>
                        </Modal.Footer>
                    </Form>
                </Modal>
                <div className="card-cev height-80">
                    <div className="card-cev-title"> <span className="label">Mis solicitudes</span> </div>
                    <div className="card-cev-body">

                        {this.state.loading &&
                            <div className="container container--fluid">
                                <Alert key={0} variant={'warning'}>
                                    <div className="alert-icon">
                                        <i className="flaticon-warning" />
                                    </div>
                                    <div className="alert-text">
                                        <>Cargando información del servidor ...</>
                                    </div>
                                </Alert>
                            </div>
                        }
                        {!this.state.loading &&
                            <div className="container container--fluid">
                                {this.state && accessRequests && accessRequests.length === 0 &&
                                    <Alert key={0} variant={'warning'}>
                                        <div className="alert-icon">
                                            <i className="flaticon-warning" />
                                        </div>
                                        <div className="alert-text">
                                            {this.state.isAdmin &&
                                                <>Usted no tiene solicitudes de acceso pendientes por aprobar o renovar</>
                                            }
                                            {this.state.isReviewer &&
                                                <>Usted no tiene solicitudes de acceso pendientes por pre-aprobar</>
                                            }
                                            {this.state.isUser &&
                                                <>Usted no tiene solicitudes de acceso a recursos</>
                                            }
                                        </div>
                                    </Alert>
                                }
                                {this.state && accessRequests && accessRequests.length > 0 &&
                                    <DataTable
                                        data={accessRequests.map((accessRequest) => {
                                            return {
                                                ident: accessRequest.ident,
                                                name: accessRequest.user ? accessRequest.user.name : "--",
                                                username: accessRequest.user ? accessRequest.user.username : "--",
                                                resourceIdent: accessRequest.resource.ident,
                                                resourceGroupIdent: `${accessRequest.resource.identifier} ${accessRequest.resource.extra ? accessRequest.resource.extra.metadata.firstLevel.title : "--"}`,
                                                type: accessRequest.resource.type ? accessRequest.resource.type : null,
                                                title: accessRequest.resource.metadata.firstLevel.title ? accessRequest.resource.metadata.firstLevel.title.substring(0, 200) : "",
                                                creator: accessRequest.resource.metadata.firstLevel.creator ? accessRequest.resource.metadata.firstLevel.creator : "",
                                                requestDate: accessRequest.requestDate,
                                                status: [accessRequest.status].concat(accessRequest.extensions.some(e => e.status === 'pending') && !accessRequests.some(r => r.resource.ident === accessRequest.resource.ident && moment(r.requestDate).local().isAfter(moment(accessRequest.requestDate).local())) ? ['renewal'] : []).concat(accessRequest.status === 'approved' && moment(accessRequest.grantedTo).local().isBefore(moment().local()) ? ['expired'] : []),
                                                accessLevel: accessRequest.resource.metadata.firstLevel.accessLevel,
                                                actions: null
                                            };
                                        })}
                                        columns={ACCESS_REQUEST_COLUMNS}
                                        isReviewer={true}
                                        showDetails={this.showDetails}
                                        showPreApproveModal={this.showPreApproveModal}
                                    />
                                }
                            </div>
                        }
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(ReviewPage);
