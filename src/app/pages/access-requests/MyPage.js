import React, { Component } from "react";
import * as AccessRequestService from "../../services/AccessRequestService";
import * as AccessRequestExtensionService from "../../services/AccessRequestExtensionService";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { Alert, Button, Col, Form, Modal, Row } from "react-bootstrap";
import moment from "moment";
import _ from "lodash";
import * as AccessRequestHistoryService from "../../services/AccessRequestHistoryService";
import { Redirect } from "react-router-dom";
import * as queryString from "query-string";
import FormAccessRequest from "../../components/organisms/FormAccessRequest";
import DataTable from "../../components/molecules/DataTable";
import { ACCESS_REQUEST_COLUMNS } from "../../config/const";

class MyPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAdmin: this.props.match.path.endsWith('/approve-access-requests'),
            isReviewer: this.props.match.path.endsWith('/review-access-requests'),
            isUser: this.props.match.path.endsWith('/my-access-requests'),
            resourceIdent: (this.props.location.search ? queryString.parse(this.props.location.search) : {}).resource,
            accessRequest: null,
            accessRequests: [],
            showRenewModal: false,
            formValidated: false
        };
        this.props.pageTitle("Mis solicitudes de acceso");
        this.handleRenewModalClose = this.handleRenewModalClose.bind(this);
        this.handleRenewModalSubmit = this.handleRenewModalSubmit.bind(this);
        this.loadData = this.loadData.bind(this);
        this.showDetails = this.showDetails.bind(this);
        this.showRenewModal = this.showRenewModal.bind(this);
    }

    componentDidMount() {
        this.loadData();
    }

    handleRenewModalClose() {
        this.setState({ accessRequest: null, showRenewModal: false });
    }

    handleRenewModalSubmit(e) {
        e.preventDefault();
        this.setState({ formValidated: true });
        if (!e.target.checkValidity()) {
            return;
        }

        const { accessRequest, extendFrom, extendTo } = this.state;
        const { user, grantedBy, resource, extensions, history } = accessRequest;

        const extensionToCreate = {
            user: user._id,
            fate: moment().local().format(),
            from: extendFrom,
            to: extendTo,
            status: 'pending'
        };

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'renewalcreated',
            description: 'Se solicita renovación de acceso al recurso'
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            grantedBy: grantedBy ? grantedBy._id : null,
            resource: resource._id
        }, _.identity);

        const promise1 = AccessRequestExtensionService.createAccessRequestExtension(extensionToCreate);

        const promise2 = AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate);

        Promise.all([promise1, promise2]).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                extensions: extensions.map(e => e._id).concat([data[0]._id]),
                history: history.map(e => e._id).concat([data[1]._id])
            });
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ accessRequest: null, showRenewModal: false, formValidated: false });
    }

    loadData(state) {
        AccessRequestService.getMyAccessRequests(this.state.resourceIdent).then((data) => {
            const isAdmin = this.props.match.path.endsWith('/approve-access-requests');
            const isReviewer = this.props.match.path.endsWith('/review-access-requests');
            const isUser = this.props.match.path.endsWith('/my-access-requests');
            const accessRequests = _.filter(data.accessRequest, (r) => {
                return r.resource != null && (isUser ? (this.props.user._id === r.user._id) : true);
            });
            const isRequestForRenewal = r => r.extensions.some(e => e.status === 'pending');
            const isRequestNotRecent = r => accessRequests.some(ar => ar.resource.ident === r.resource.ident && moment(ar.requestDate).local().isBefore(moment(r.requestDate).local()));
            const isRequestValidated = r => r.status === 'validated';
            const adminFilter = r => isRequestValidated(r) || (isRequestForRenewal(r) && isRequestNotRecent(r));
            const isRequestPending = r => r.status === 'pending';
            const reviewerFilter = r => isRequestPending(r);
            const isRequestOwner = r => this.props.user._id === r.user._id;
            const userFilter = r => isRequestOwner(r);
            const accessRequestsCount = accessRequests.filter(r => (isAdmin ? adminFilter(r) : (isReviewer ? reviewerFilter(r) : userFilter(r)))).length;
            this.setState({
                ...(state || {}),
                accessRequests: accessRequests,
                actualResource: data.actual
            });
            if (isAdmin && accessRequestsCount === 1) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitud pendiente por aprobar o renovar`);
            } else if (isAdmin && (accessRequestsCount === 0 || accessRequestsCount > 1)) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitudes pendientes por aprobar o renovar`);
            } else if (isReviewer && accessRequestsCount === 1) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitud pendiente por pre-aprobar`);
            } else if (isReviewer && (accessRequestsCount === 0 || accessRequestsCount > 1)) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitudes pendientes por pre-aprobar`);
            } else {
                this.props.pageSubtitle(`${accessRequestsCount} solicitudes`);
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    showDetails(rowIndex) {
        const accessRequest = this.state.accessRequests[rowIndex];
        const accessRequestIdent = accessRequest.ident;
        this.props.history.push(`/access-requests/${accessRequestIdent}`);
    }

    showRenewModal(rowIndex) {
        const accessRequest = this.state.accessRequests[rowIndex];
        this.setState({ accessRequest: accessRequest, showRenewModal: true });
    }

    render() {
        const { accessRequests, resourceIdent } = this.state;
        const { user } = this.props;
        return (
            <>
                <>
                    {user.roles && !user.roles.includes('user') &&
                        <Redirect to={'/dashboard'} />
                    }
                </>
                {this.state.actualResource &&
                    <FormAccessRequest showModal={resourceIdent} toggleModal={() => this.loadData({ resourceIdent: null })} accessLevel={this.state.actualResource.metadata.firstLevel.accessLevel} />
                }
                {resourceIdent !== "" && this.state.actualResource === undefined &&
                    <>
                        <Alert key={0} variant={'warning'}>
                            <div className="alert-icon">
                                <i className="flaticon-warning" />
                            </div>
                            <div className="alert-text">
                                Consultando información del recurso {this.state.resourceIdent}
                            </div>
                        </Alert>
                    </>
                }
                <Modal
                    show={this.state.showRenewModal}
                    onHide={this.handleRenewModalClose}
                    size="lg"
                >
                    <Form noValidate validated={this.state.formValidated} onSubmit={this.handleRenewModalSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Solicitud de renovación de acceso</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Alert key={0} variant={'warning'}>
                                <div className="alert-icon">
                                    <i className="flaticon-warning" />
                                </div>
                                <div className="alert-text">
                                    Solicitará renovación de acceso a un recurso clasificado, indique el rango de fechas
                                    en las cuales desea extender su acceso
                                </div>
                            </Alert>
                            <Form.Group as={Row}>
                                <Form.Label column="false" sm={{ span: 3, offset: 3 }}>
                                    <span>Fecha de inicio</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Col sm={{ span: 3 }}>
                                    <Form.Control
                                        required
                                        as="input"
                                        type="date"
                                        min={moment().local().format('YYYY-MM-DD')}
                                        onChange={(e) => {
                                            this.setState({ extendFrom: moment(e.target.value, 'YYYY-MM-DD').local().format() })
                                        }}
                                    />
                                    <Form.Control.Feedback type="invalid">Este campo es
                                        requerido</Form.Control.Feedback>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                                <Form.Label column="false" sm={{ span: 3, offset: 3 }}>
                                    <span>Fecha de fin</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Col sm={{ span: 3 }}>
                                    <Form.Control
                                        required
                                        as="input"
                                        type="date"
                                        min={moment(this.state.extendFrom ? this.state.extendFrom : undefined).local().format('YYYY-MM-DD')}
                                        onChange={(e) => {
                                            this.setState({ extendTo: moment(e.target.value, 'YYYY-MM-DD').local().endOf('day').format() })
                                        }}
                                    />
                                    <Form.Control.Feedback type="invalid">Este campo es
                                        requerido</Form.Control.Feedback>
                                </Col>
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="pull-left" variant="primary" type="submit">
                                Enviar solicitud
                            </Button>
                            <Button className="pull-left" variant="secondary" onClick={this.handleRenewModalClose}>
                                Cancelar
                            </Button>
                        </Modal.Footer>
                    </Form>
                </Modal>
                <div className="card-cev height-80">
                    <div className="card-cev-title"> <span className="label">Mis solicitudes</span> </div>
                    <div className="card-cev-body">
                        {this.state && accessRequests && accessRequests.length === 0 &&
                            <Alert key={0} variant={'warning'}>
                                <div className="alert-icon">
                                    <i className="flaticon-warning" />
                                </div>
                                <div className="alert-text">
                                    {this.state.isAdmin &&
                                        <>Usted no tiene solicitudes de acceso pendientes por aprobar o renovar</>
                                    }
                                    {this.state.isReviewer &&
                                        <>Usted no tiene solicitudes de acceso pendientes por pre-aprobar</>
                                    }
                                    {this.state.isUser &&
                                        <>Usted no tiene solicitudes de acceso a recursos</>
                                    }
                                </div>
                            </Alert>
                        }
                        {this.state && accessRequests && accessRequests.length > 0 &&
                            <DataTable
                                data={accessRequests.map((accessRequest) => {
                                    return {
                                        ident: accessRequest.ident,
                                        name: accessRequest.user ? accessRequest.user.name : "--",
                                        username: accessRequest.user ? accessRequest.user.username : "--",
                                        resourceIdent: accessRequest.resource.ident,
                                        resourceGroupIdent: `${accessRequest.resource.identifier} ${accessRequest.resource.extra ? accessRequest.resource.extra.metadata.firstLevel.title : "--"}`,
                                        type: accessRequest.resource.type ? accessRequest.resource.type : null,
                                        title: accessRequest.resource.metadata.firstLevel.title ? accessRequest.resource.metadata.firstLevel.title.substring(0, 200) : "",
                                        creator: accessRequest.resource.metadata.firstLevel.creator ? accessRequest.resource.metadata.firstLevel.creator : "",
                                        requestDate: accessRequest.requestDate,
                                        status: [accessRequest.status].concat(accessRequest.extensions.some(e => e.status === 'pending') && !accessRequests.some(r => r.resource.ident === accessRequest.resource.ident && moment(r.requestDate).local().isAfter(moment(accessRequest.requestDate).local())) ? ['renewal'] : []).concat(accessRequest.status === 'approved' && moment(accessRequest.grantedTo).local().isBefore(moment().local()) ? ['expired'] : []).concat((accessRequest.status === 'approved' || accessRequest.status === 'rejected') && accessRequest.extensions.every(e => e.status === 'processed') && !accessRequests.some(r => r.resource.ident === accessRequest.resource.ident && moment(r.requestDate).local().isAfter(moment(accessRequest.requestDate).local())) ? ['renewable'] : []),
                                        accessLevel: accessRequest.resource.metadata.firstLevel.accessLevel,
                                        actions: null
                                    };
                                })}
                                columns={ACCESS_REQUEST_COLUMNS.map((column, idx) => {
                                    return idx === 1 || idx === 2 ? {
                                        ...column,
                                        visible: false
                                    } : column;
                                })}
                                isUser={true}
                                showDetails={this.showDetails}
                                showRenewModal={this.showRenewModal}
                            />
                        }
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(MyPage);
