import React, { Component } from "react";
import * as AccessRequestService from "../../services/AccessRequestService";
import * as AccessRequestExtensionService from "../../services/AccessRequestExtensionService";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { Alert, Button, Col, Form, Modal, Row } from "react-bootstrap";
import moment from "moment";
import _ from "lodash";
import * as AccessRequestHistoryService from "../../services/AccessRequestHistoryService";
import { Redirect } from "react-router-dom";
import DataTable from "../../components/molecules/DataTable";
import { ACCESS_REQUEST_COLUMNS } from "../../config/const";
import "./../../styles/resource.scss"

class ApprovePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAdmin: this.props.match.path.endsWith('/approve-access-requests'),
            isReviewer: this.props.match.path.endsWith('/review-access-requests'),
            isUser: this.props.match.path.endsWith('/my-access-requests'),
            accessRequest: null,
            accessRequests: [],
            showApproveModal: false,
            showRejectModal: false,
            showApproveRenewModal: false,
            grantedFrom: null,
            grantedTo: null,
            formValidated: false,
            approveComment: "",
            rejectComment: "",
        };
        this.props.pageTitle("Administrar solicitudes de acceso");
        this.handleApproveModalClose = this.handleApproveModalClose.bind(this);
        this.handleApproveModalSubmit = this.handleApproveModalSubmit.bind(this);
        this.handleApproveRenewModalClose = this.handleApproveRenewModalClose.bind(this);
        this.handleApproveRenewModalSubmit = this.handleApproveRenewModalSubmit.bind(this);
        this.handleRejectModalClose = this.handleRejectModalClose.bind(this);
        this.handleRejectModalSubmit = this.handleRejectModalSubmit.bind(this);
        this.loadData = this.loadData.bind(this);
        this.showApproveModal = this.showApproveModal.bind(this);
        this.showApproveRenewModal = this.showApproveRenewModal.bind(this);
        this.showDetails = this.showDetails.bind(this);
        this.showRejectModal = this.showRejectModal.bind(this);
    }

    componentDidMount() {
        this.loadData();
    }

    handleApproveModalClose() {
        this.setState({ showApproveModal: false });
    }

    handleApproveModalSubmit(e) {
        e.preventDefault();
        this.setState({ formValidated: true });
        if (!e.target.checkValidity()) {
            return;
        }

        const { accessRequest, grantedFrom, grantedTo, approveComment } = this.state;
        const { user, resource, extensions, history } = accessRequest;

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'approved',
            description: `Solicitud de acceso aprobada por ${moment(grantedFrom, 'YYYY/MM/DD').local().to(moment(grantedTo, 'YYYY/MM/DD').local(), true)} del ${moment(grantedFrom, 'YYYY/MM/DD').local().format('D')} de ${moment(grantedFrom, 'YYYY/MM/DD').local().format('MMMM')} de ${moment(grantedFrom, 'YYYY/MM/DD').local().format('YYYY')} al ${moment(grantedTo, 'YYYY/MM/DD').local().format('D')} de ${moment(grantedTo, 'YYYY/MM/DD').local().format('MMMM')} de ${moment(grantedTo, 'YYYY/MM/DD').local().format('YYYY')}.\n${approveComment}`
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            status: 'approved',
            resource: resource._id,
            grantDate: moment().local().format(),
            grantedBy: this.props.user._id,
            grantedFrom: moment(grantedFrom, 'YYYY/MM/DD').local().format(),
            grantedTo: moment(grantedTo, 'YYYY/MM/DD').local().format(),
            extensions: extensions.map(e => e._id),
            approveComment: approveComment,
        }, _.identity);

        AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                history: history.map(e => e._id).concat([data._id])
            });
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ accessRequest: null, showApproveModal: false, formValidated: false, approveComment: null });
    }

    handleApproveRenewModalClose() {
        this.setState({ accessRequest: null, showApproveRenewModal: false });
    }

    handleApproveRenewModalSubmit(e) {
        e.preventDefault();
        this.setState({ formValidated: true });
        if (!e.target.checkValidity()) {
            return;
        }

        const { accessRequest, grantedFrom, grantedTo } = this.state;
        const { user, grantedBy, resource, extensions, history } = accessRequest;

        const extensionToUpdate = _.pickBy({
            ...extensions.find(e => e.status === 'pending') || {},
            status: 'processed'
        }, _.identity);

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'renewalapproved',
            description: `Se aprueba la solicitud de renovación de acceso al recurso por ${moment(grantedFrom, 'YYYY/MM/DD').local().to(moment(grantedTo, 'YYYY/MM/DD').local(), true)} del ${moment(grantedFrom, 'YYYY/MM/DD').local().format('D')} de ${moment(grantedFrom, 'YYYY/MM/DD').local().format('MMMM')} de ${moment(grantedFrom, 'YYYY/MM/DD').local().format('YYYY')} al ${moment(grantedTo, 'YYYY/MM/DD').local().format('D')} de ${moment(grantedTo, 'YYYY/MM/DD').local().format('MMMM')} de ${moment(grantedTo, 'YYYY/MM/DD').local().format('YYYY')}.`
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            grantedFrom: grantedFrom,
            grantedTo: grantedTo,
            resource: resource._id,
            extensions: extensions.map(e => e._id),
            grantedBy: grantedBy ? grantedBy._id : null,
            status: 'approved',
            rejectionDate: null
        }, _.identity);

        AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                history: history.map(e => e._id).concat([data._id])
            });
        }).then(() => {
            return AccessRequestExtensionService.updateAccessRequestExtension(extensionToUpdate);
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ accessRequest: null, showApproveRenewModal: false, formValidated: false });
    }

    handleRejectModalClose() {
        this.setState({ accessRequest: null, showRejectModal: false });
    }

    handleRejectModalSubmit(e) {
        e.preventDefault();

        const { accessRequest, rejectComment } = this.state;
        const { user, resource, extensions, history } = accessRequest;

        console.log("Estoy rechazando esto con este mensaje: ", rejectComment)

        const accessRequestHistoryToCreate = {
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'rejected',
            description: `Se rechaza la solicitud de acceso.\n${rejectComment}`
        };

        const accessRequestToUpdate = _.pickBy({
            ...accessRequest,
            user: user._id,
            status: 'rejected',
            resource: resource._id,
            rejectionDate: moment().local().format(),
            extensions: extensions.map(e => e._id),
            rejectComment: rejectComment,
        }, _.identity);

        AccessRequestHistoryService.createAccessRequestHistory(accessRequestHistoryToCreate).then((data) => {
            return AccessRequestService.updateAccessRequest({
                ...accessRequestToUpdate,
                history: history.map(e => e._id).concat([data._id])
            });
        }).then(() => {
            this.loadData();
        }).catch((error) => {
            console.log(error);
        });

        this.setState({ accessRequest: null, showRejectModal: false, formValidated: false });
    }

    loadData() {

        AccessRequestService.getAccessRequests().then((data) => {
            const isAdmin = this.props.match.path.endsWith('/approve-access-requests');
            const isReviewer = this.props.match.path.endsWith('/review-access-requests');
            const isUser = this.props.match.path.endsWith('/my-access-requests');
            const accessRequests = _.filter(data, (r) => {
                return r.resource != null && (isUser ? (this.props.user._id === r.user._id) : true);
            });
            const isRequestForRenewal = r => r.extensions.some(e => e.status === 'pending');
            const isRequestNotRecent = r => accessRequests.some(ar => ar.resource.ident === r.resource.ident && moment(ar.requestDate).local().isBefore(moment(r.requestDate).local()));
            const isRequestValidated = r => r.status === 'validated';
            const adminFilter = r => isRequestValidated(r) || (isRequestForRenewal(r) && isRequestNotRecent(r));
            const isRequestPending = r => r.status === 'pending';
            const reviewerFilter = r => isRequestPending(r);
            const isRequestOwner = r => this.props.user._id === r.user._id;
            const userFilter = r => isRequestOwner(r);
            const accessRequestsCount = accessRequests.filter(r => (isAdmin ? adminFilter(r) : (isReviewer ? reviewerFilter(r) : userFilter(r)))).length;
            this.setState({
                accessRequests: accessRequests
            });
            if (isAdmin && accessRequestsCount === 1) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitud pendiente por aprobar o renovar`);
            } else if (isAdmin && (accessRequestsCount === 0 || accessRequestsCount > 1)) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitudes pendientes por aprobar o renovar`);
            } else if (isReviewer && accessRequestsCount === 1) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitud pendiente por pre-aprobar`);
            } else if (isReviewer && (accessRequestsCount === 0 || accessRequestsCount > 1)) {
                this.props.pageSubtitle(`${accessRequestsCount} solicitudes pendientes por pre-aprobar`);
            } else {
                this.props.pageSubtitle(`${accessRequestsCount} solicitudes`);
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    showApproveModal(rowIndex) {
        const accessRequest = this.state.accessRequests[rowIndex];
        this.setState({ accessRequest: accessRequest, showApproveModal: true });
    }

    showApproveRenewModal(rowIndex) {
        const accessRequest = this.state.accessRequests[rowIndex];
        this.setState({ accessRequest: accessRequest, showApproveRenewModal: true });
    }

    showDetails(rowIndex) {
        const accessRequest = this.state.accessRequests[rowIndex];
        const accessRequestIdent = accessRequest.ident;
        this.props.history.push(`/access-requests/${accessRequestIdent}`);
    }

    showRejectModal(rowIndex) {
        const accessRequest = this.state.accessRequests[rowIndex];
        this.setState({ accessRequest: accessRequest, showRejectModal: true });
    }

    render() {
        const { accessRequest, accessRequests } = this.state;
        const { user } = this.props;
        return (
            <>
                <>
                    {user.roles && !user.roles.includes('approver') &&
                        <Redirect to={'/dashboard'} />
                    }
                </>
                <Modal
                    show={this.state.showApproveModal}
                    onHide={this.handleApproveModalClose}
                    size="lg"
                >
                    <Form noValidate validated={this.state.formValidated} onSubmit={this.handleApproveModalSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Aprobar acceso a recurso</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Alert key={0} variant={'warning'}>
                                <div className="alert-icon">
                                    <i className="flaticon-warning cev-font-brand" />
                                </div>
                                <div className="alert-text">
                                    Aprobará acceso temporal a un recurso clasificado, indique el rango de fechas en las
                                    cuales el usuario tendrá acceso.
                                </div>
                            </Alert>
                            <Form.Group as={Row}>
                                <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                    <span>Fecha de inicio</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Col sm={3}>
                                    <Form.Control
                                        required
                                        as="input"
                                        type="date"
                                        min={moment().local().format('YYYY-MM-DD')}
                                        onChange={(e) => {
                                            this.setState({ grantedFrom: moment(e.target.value, 'YYYY-MM-DD').local().format() })
                                        }}
                                    />
                                    <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                </Col>
                                <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                    Fecha solicitada
                                </Form.Label>
                                <Col sm={2}>
                                    {accessRequest &&
                                        <Form.Control
                                            disabled
                                            as="input"
                                            type="text"
                                            defaultValue={moment(accessRequest.requestedFrom).local().format('YYYY-MM-DD')}
                                        />
                                    }
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                                <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                    <span>Fecha de fin</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Col sm={3}>
                                    <Form.Control
                                        required
                                        as="input"
                                        type="date"
                                        min={moment(this.state.grantedFrom ? this.state.grantedFrom : undefined).local().format('YYYY-MM-DD')}
                                        onChange={(e) => {
                                            this.setState({ grantedTo: moment(e.target.value, 'YYYY-MM-DD').local().endOf('day').format() })
                                        }}
                                    />
                                    <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                </Col>
                                <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                    Fecha solicitada
                                </Form.Label>
                                <Col sm={2}>
                                    {accessRequest &&
                                        <Form.Control
                                            disabled
                                            as="input"
                                            type="date"
                                            defaultValue={moment(accessRequest.requestedTo).local().format('YYYY-MM-DD')}
                                        />
                                    }
                                </Col>
                            </Form.Group>
                            <Form.Group controlId="preApproveForm.Textarea">
                                <Form.Label column="false">
                                    <span>Observaciones de aprobación</span>
                                </Form.Label>
                                <Form.Control
                                    required
                                    as="textarea"
                                    rows="3"
                                    onChange={(e) => { this.setState({ approveComment: e.target.value }) }}
                                />
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="pull-left" variant="primary" type="submit">
                                Aprobar
                            </Button>
                            <Button className="pull-left" variant="secondary" onClick={this.handleApproveModalClose}>
                                Cancelar
                            </Button>
                        </Modal.Footer>
                    </Form>
                </Modal>
                <Modal
                    show={this.state.showRejectModal}
                    onHide={this.handleRejectModalClose}
                    size="md"
                >
                    <Form onSubmit={this.handleRejectModalSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Rechazar acceso a recurso</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form.Group controlId="preApproveForm.Textarea">
                                <Form.Label column="false">
                                    <span>Observaciones de desaprobación</span>
                                </Form.Label>
                                <Form.Control
                                    required
                                    as="textarea"
                                    rows="3"
                                    onChange={(e) => { this.setState({ rejectComment: e.target.value }) }}
                                />
                            </Form.Group>
                            <Alert key={0} variant={'warning'}>
                                <div className="alert-icon">
                                    <i className="flaticon-warning cev-font-brand" />
                                </div>
                                <div className="alert-text">
                                    ¿Está seguro que desea rechazar el acceso a este recurso?
                                </div>
                            </Alert>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="pull-left" variant="primary" type="submit">
                                Rechazar
                            </Button>
                            <Button className="pull-left" variant="secondary" onClick={this.handleRejectModalClose}>
                                Cancelar
                            </Button>
                        </Modal.Footer>
                    </Form>
                </Modal>
                <Modal
                    show={this.state.showApproveRenewModal}
                    onHide={this.handleApproveRenewModalClose}
                    size="lg"
                >
                    <Form noValidate validated={this.state.formValidated} onSubmit={this.handleApproveRenewModalSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Renovar acceso a recurso</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Alert key={0} variant={'warning'}>
                                <div className="alert-icon">
                                    <i className="flaticon-warning cev-font-brand" />
                                </div>
                                <div className="alert-text">
                                    Aprobará renovación de acceso temporal a un recurso clasificado, indique el rango de
                                    fechas en las cuales el usuario tendrá acceso.
                                </div>
                            </Alert>
                            <Form.Group as={Row}>
                                <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                    <span>Fecha de inicio</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Col sm={3}>
                                    <Form.Control
                                        required
                                        as="input"
                                        type="date"
                                        min={moment().local().format('YYYY-MM-DD')}
                                        onChange={(e) => {
                                            this.setState({ grantedFrom: moment(e.target.value, 'YYYY-MM-DD').local().format() })
                                        }}
                                    />
                                    <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                </Col>
                                <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                    Fecha solicitada
                                </Form.Label>
                                <Col sm={2}>
                                    {accessRequest &&
                                        <Form.Control
                                            disabled
                                            as="input"
                                            type="text"
                                            defaultValue={moment((accessRequest.extensions.find(e => e.status === 'pending') || {}).from).local().format('DD/MM/YYYY')}
                                        />
                                    }
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                                <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                    <span>Fecha de fin</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Col sm={3}>
                                    <Form.Control
                                        required
                                        as="input"
                                        type="date"
                                        min={moment(this.state.grantedFrom ? this.state.grantedFrom : undefined).local().format('YYYY-MM-DD')}
                                        onChange={(e) => {
                                            this.setState({ grantedTo: moment(e.target.value, 'YYYY-MM-DD').local().endOf('day').format() })
                                        }}
                                    />
                                    <Form.Control.Feedback type="invalid">Este campo es requerido</Form.Control.Feedback>
                                </Col>
                                <Form.Label column="false" sm={{ span: 2, offset: 1 }}>
                                    Fecha solicitada
                                </Form.Label>
                                <Col sm={2}>
                                    {accessRequest &&
                                        <Form.Control
                                            disabled
                                            as="input"
                                            type="text"
                                            defaultValue={moment((accessRequest.extensions.find(e => e.status === 'pending') || {}).to).local().format('DD/MM/YYYY')}
                                        />
                                    }
                                </Col>
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="pull-left" variant="primary" type="submit">
                                Aprobar
                            </Button>
                            <Button className="pull-left" variant="secondary" onClick={this.handleApproveRenewModalClose}>
                                Cancelar
                            </Button>
                        </Modal.Footer>
                    </Form>
                </Modal>
                <div className="cev-container  cev-container--fluid  cev-grid__item cev-grid__item--fluid">
                    {this.state && accessRequests && accessRequests.length === 0 &&
                        <Alert key={0} variant={'warning'}>
                            <div className="alert-icon">
                                <i className="flaticon-warning cev-font-brand" />
                            </div>
                            <div className="alert-text">
                                {this.state.isAdmin &&
                                    <>Usted no tiene solicitudes de acceso pendientes por aprobar o renovar</>
                                }
                                {this.state.isReviewer &&
                                    <>Usted no tiene solicitudes de acceso pendientes por pre-aprobar</>
                                }
                                {this.state.isUser &&
                                    <>Usted no tiene solicitudes de acceso a recursos</>
                                }
                            </div>
                        </Alert>
                    }
                    {this.state && accessRequests && accessRequests.length > 0 &&
                        <div className="card-cev height-80">
                            <div className="card-cev-title"> <span className="label">Solicitudes de acceso</span> </div>
                            <div className="card-cev-body">
                                <DataTable
                                    data={accessRequests.map((accessRequest) => {
                                        return {
                                            ident: accessRequest.ident,
                                            name: accessRequest.user ? accessRequest.user.name : "--",
                                            username: accessRequest.user ? accessRequest.user.username : "--",
                                            resourceIdent: accessRequest.resource.ident,
                                            resourceGroupIdent: `${accessRequest.resource.identifier} ${accessRequest.resource.extra ? accessRequest.resource.extra.metadata.firstLevel.title : "--"}`,
                                            type: accessRequest.resource.type ? accessRequest.resource.type : null,
                                            title: accessRequest.resource.metadata.firstLevel.title ? accessRequest.resource.metadata.firstLevel.title.substring(0, 200) : "",
                                            creator: accessRequest.resource.metadata.firstLevel.creator ? accessRequest.resource.metadata.firstLevel.creator : "",
                                            requestDate: accessRequest.requestDate,
                                            hasExtensions: accessRequest.extensions.some(e => e.status === 'pending'),
                                            isMostRecent: !accessRequests.some(r => r.resource.ident === accessRequest.resource.ident && moment(r.requestDate).isAfter(moment(accessRequest.requestDate))),
                                            status: [accessRequest.status].concat(accessRequest.extensions.some(e => e.status === 'pending') && !accessRequests.some(r => r.resource.ident === accessRequest.resource.ident && moment(r.requestDate).local().isAfter(moment(accessRequest.requestDate).local())) ? ['renewal'] : []).concat(accessRequest.status === 'approved' && moment(accessRequest.grantedTo).local().isBefore(moment().local()) ? ['expired'] : []),
                                            accessLevel: accessRequest.resource.metadata.firstLevel.accessLevel,
                                            actions: null
                                        };
                                    })}
                                    columns={ACCESS_REQUEST_COLUMNS}
                                    isAdmin={true}
                                    showApproveModal={this.showApproveModal}
                                    showApproveRenewModal={this.showApproveRenewModal}
                                    showDetails={this.showDetails}
                                    showRejectModal={this.showRejectModal}
                                />
                            </div>
                        </div>
                    }
                </div>
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(ApprovePage);
