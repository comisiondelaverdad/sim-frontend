import React, { useState, useEffect } from "react";
import UserAccessValidator from "../../../components/atoms/UserAccessValidator";
import { Button, Form, Card, Container, Row, Col } from "react-bootstrap";
import * as MenusService from "../../../services/MenusService";
import { useParams } from "react-router-dom";
import MenuLinks from "../../../components/organisms/MenuLinks";
import MenuEstructure from "../../../components/organisms/MenuEstructure";
import AddElementsMenu from "./AddElementsMenu";
import Accordion from "react-bootstrap/Accordion";
import { LABELS_MENU } from "../../../../app/config/constLabels";
import { useTranslation } from "react-i18next";

function MenuModify(props) {
  const [t, i18n] = useTranslation("common");
  const styleCard = {
    header: { fontWeight: "900" },
    label: { fontWeight: "400 !important" },
  };
  const { id } = useParams();
  const [form, setForm] = useState({
    nombreMenu: "",
    estado: 1,
    elements: [],
    section: null,
  });
  const [id_menu, setId] = useState(id);
  const [loading, setLoading] = useState(false);
  const [show1, setShow1] = useState(true);

  useEffect(() => {
    async function getmenu() {
      if (id) {
        setLoading(true);
        const res = await MenusService.get(id);
        setForm({
          ...form,
          nombreMenu: res.nombreMenu,
          elements: res.elements,
          section: res.section,
        });
        setLoading(false);
      }
    }
    getmenu();
  }, []);
  function handleChange(event) {
    setForm({
      ...form,
      nombreMenu: event.target.value,
    });
  }
  function handleChangeSection(event) {
    setForm({
      ...form,
      section: event.target.value,
    });
  }
  async function sendform() {
    let res;
    setLoading(true);
    if (id) res = await MenusService.update(id, form);
    else res = await MenusService.create(form);
    props.history.push("/admin/menus/");
  }

  function addElement(element) {
    let orden = form.elements.length + 1;
    element.orden = orden;
    setForm((form) => ({
      ...form,
      elements: form.elements.concat(element),
    }));
  }

  async function add_collection_element(element) {
    const new_element = {
      clases: "",
      icon: null,
      id: element._id,
      img: "",
      material_icon: "",
      only: false,
      orden: 1,
      route: `museo/conoce/${element._id}`,
      tab: false,
      tag: element.title,
    };

    //verificar si no existe ya un elemento con ese id
    const res = await verifyDuplicateElements(element._id, form.elements);
    if (!res) addElement(new_element);
    else alert("Esta colección ya existe en la estructura del menú");
  }

  const changeName = (name) => {
    return name.replaceAll(" ", "_");
  };

  async function add_microsite_element(element) {
    const new_element = {
      clases: "",
      icon: null,
      id: element._id,
      img: "",
      material_icon: "",
      only: false,
      orden: 1,
      route: `museo/microsite/${changeName(element.name)}`,
      tab: false,
      tag: element.name,
    };
    //verificar si no existe ya un elemento con ese id
    const res = await verifyDuplicateElements(element._id, form.elements);
    if (!res) addElement(new_element);
    else alert("Este micrositio ya existe en la estructura del menú");
  }
  const verifyDuplicateElements = (new_id, elements) => {
    let exist = false;
    elements.forEach((item) => {
      if (item.id == new_id) exist = true;
      else if (item.children && item.children.length)
        exist = verifyDuplicateElements(new_id, item.children);
      // else exist = false;
    });

    return exist;
  };

  function updateElements(elements) {
    setForm((form) => ({
      ...form,
      elements: elements,
    }));
  }
  const deleteItem = (id) => {
    if (!window.confirm("Está seguro que desea eliminar este item?"))
      return false;
    let new_elements = form.elements.filter((item) => item.id != id);
    if (JSON.stringify(new_elements) == JSON.stringify(form.elements)) {
      new_elements = new_elements.map((item) => {
        if (item.children && item.children.length) {
          let child = removeOnChildren(item, item.children, id);
          return {
            ...item,
            children: child,
          };
        } else {
          return item;
        }
      });
    }
    updateElements(new_elements);
  };

  const removeOnChildren = (parent, children, id) => {
    children = children.filter((item) => item.id != id);
    children = children.map((item) => {
      if (item.children.length) {
        let child = removeOnChildren(item, item.children, id);
        return {
          ...item,
          children: child,
        };
      } else {
        return item;
      }
    });
    return children;
  };
  const changeElement = (element) => {
    let new_elements = changeItemchildren(form.elements, element);
    updateElements(new_elements);
  };
  const changeItemchildren = (elements, element) => {
    let new_elements = elements.map((item) => {
      if (item.id == element.id) {
        return element;
      } else if (item.children && item.children.length) {
        return {
          ...item,
          children: changeItemchildren(item.children, element),
        };
      } else {
        return item;
      }
    });

    return new_elements;
  };

  return (
    <>
      <UserAccessValidator rol="admin" />
      <div className="card-cev height-80">
        <div className="card-cev-title">
          <span className="label">{t("micrositios.menus.menuConfig")}</span>
          {loading ? (
            <div className="fa-3x">
              <i className="fas fa-spinner fa-spin"></i>
            </div>
          ) : form.nombreMenu &&
            form.elements.length &&
            !loading ? (
            <Button
              className="btn-outline-primary"
              onClick={sendform}
              variant="btn-outline-primary"
              type="button"
            >
              {LABELS_MENU.menu_save}
            </Button>
          ) : (
            ""
          )}
        </div>

        <div className="card-cev-body">
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              props.history.push("/admin/menus/");
            }}
          >
            <i className="fas fa-long-arrow-alt-left"></i>
          </a>

          <Container>
            <Row style={{ marginTop: "6px" }}>
              <Col xs={12} sm={4}>
                <Accordion
                  style={{ marginBottom: "6px", width: "100%" }}
                >
                  <Card>
                    <Card.Header style={styleCard.header}>
                      <div className="d-flex justify-content-between align-items-center">
                        <strong className="mx-auto">
                          {t("micrositios.menus.basicInfo")}
                        </strong>
                        <div>
                          <button
                            onClick={(e) => {
                              setShow1(!show1);
                            }}
                            type="button"
                            className="btn btn-"
                          >
                            <i className="fas fa-sort-down"></i>
                          </button>
                        </div>
                      </div>
                    </Card.Header>
                    <Accordion.Collapse
                      className={show1 ? "show" : ""}
                      eventKey="1"
                    >
                      <Card.Body>
                        <Form
                          onSubmit={(e) => {
                            e.preventDefault();
                          }}
                        >
                          <Form.Group controlId="">
                            <label>
                              {t("micrositios.menus.menuName")} *
                            </label>
                            <Form.Control
                              type="text"
                              onChange={handleChange}
                              value={form.nombreMenu}
                            />
                          </Form.Group>
                          <Form.Group controlId="exampleForm.ControlSelect1">
                            <label>{t("micrositios.menus.section")} </label>
                            <Form.Control
                              value={
                                form.section ? form.section : "-1"
                              }
                              onChange={handleChangeSection}
                              as="select"
                            >
                              <option value="-1">
                                Seleccione...
                              </option>
                              <option value="1">
                                Menú principal buscador
                              </option>
                              <option value="2">
                                Menú principal museo
                              </option>
                              <option value="3">
                                Menú inferior museo
                              </option>
                            </Form.Control>
                          </Form.Group>
                        </Form>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
                <hr />
                <MenuLinks
                  addElement={addElement}
                  id_menu={id_menu}
                  section={form.section}
                />
                <hr />
                <AddElementsMenu
                  add_collection_element={add_collection_element}
                  add_microsite_element={add_microsite_element}
                />
              </Col>
              <Col xs={12} sm={8}>
                <MenuEstructure
                  section={form.section}
                  deleteItem={deleteItem}
                  updateElements={updateElements}
                  elements={form.elements}
                  changeElement={changeElement}
                />
              </Col>
            </Row>
          </Container>


        </div>
      </div>
    </>
  );
}
export default MenuModify;
