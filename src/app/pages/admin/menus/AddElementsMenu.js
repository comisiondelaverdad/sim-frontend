import React, { useState, useEffect } from "react";
import { Card, Row, Col } from "react-bootstrap";
import Accordion from "react-bootstrap/Accordion";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import AddCollectionMenu from "../../../components/organisms/AddCollectionMenu";
import AddMicrositeMenu from "../../../components/organisms/AddMicrositeMenu";
import { useTranslation } from "react-i18next";


const AddElementsMenu = (props) => {
  const [t, i18n] = useTranslation("common");

  const [show, setShow] = useState(false);
  const [key, setKey] = useState("colections");

  return (
    <>
      <Accordion style={{ marginBottom: "6px", width: "100%" }}>
        <Card>
          <Card.Header>
            <div className="d-flex justify-content-between align-items-center">
              <strong className="mx-auto">{t("micrositios.menus.addElements")}</strong>
              <div>
                <button
                  onClick={(e) => {
                    setShow(!show);
                  }}
                  type="button"
                  className="btn btn-"
                >
                  <i className="fas fa-sort-down"></i>
                </button>
              </div>
            </div>
          </Card.Header>
          <Accordion.Collapse className={show ? "show" : ""} eventKey="1">
            <Card.Body id="1">
              <Row>
                <Col>
                  <Tabs
                    id="controlled-tab-example"
                    activeKey={key}
                    onSelect={(k) => setKey(k)}
                    // className="mb-3 elements_menu"
                    className="mb-3"
                  >
                    <Tab
                      className="element_menu"
                      eventKey="colections"
                      title={t("micrositios.menus.collections")}
                    >
                      <AddCollectionMenu
                        add_collection_element={props.add_collection_element}
                      />
                    </Tab>
                    <Tab
                      className="element_menu"
                      eventKey="metadata"
                      title={t("micrositios.menus.microsites")}
                    >
                      <AddMicrositeMenu
                        add_microsite_element={props.add_microsite_element}
                      />
                    </Tab>
                  </Tabs>
                </Col>
              </Row>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </>
  );
};

export default AddElementsMenu;
