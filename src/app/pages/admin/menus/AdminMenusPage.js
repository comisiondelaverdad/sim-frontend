import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../../components/atoms/UserAccessValidator";
import * as app from "../../../store/ducks/app.duck";
import * as MenusService from "../../../services/MenusService";
import { SimpleDataTable } from "../../../components/molecules/SimpleDataTable";
import Swal from "sweetalert2";
import { LABELS_MENU } from "../../../../app/config/constLabels"


const headerMenu = [
  {
    title: "No",
    data: "No",
    width: "3%"
  },
  {
    title: LABELS_MENU.menu_name,
    data: "nombreMenu",
  },
  {
    title: LABELS_MENU.actions,
    data: "_id",
    type: "action",
    width: "8%",
  },
];

class AdminMenusPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      menus: null,
      loading: false,
    };
    this.actionEvent = this.actionEvent.bind(this);
  }

  loadData() {
    this.setState({ loading: true });
    MenusService.list("", this.state.page).then((data) => {
      if (data.length) {
        let i = 0;
        const dataMenus = data
          .filter((item) => {
            return item.estado > 0;
          })
          .map((u) => {
            i++;
            return {
              ...{
                _id: u._id,
                nombreMenu: u.nombreMenu,
                No: i,
                estado: u.estado,
              },
            };
          });
        this.setState({ menus: dataMenus });

      }
      this.setState({ loading: false });
    });
  }

  componentDidMount() {
    this.loadData();
  }
  async delete(row) {
    let result = await Swal.fire({
      title: LABELS_MENU.delete_confirmation,
      showCancelButton: true,
      confirmButtonText: LABELS_MENU.delete,
      cancelButtonText: `Cancelar`,

    });


    if (result.isConfirmed) {
      const res = await MenusService.eliminar(row._id);
      this.loadData();
    }
  }

  actionEvent({ action, row }) {
    switch (action.action) {
      case "edit":
        this.props.history.push("/admin/menus/modify/" + row._id);
        break;
      case "delete":
        this.delete(row);

        break;

      default:
        break;
    }
  }

  render() {
    let { menus } = this.state;

    return (
      <>
        <UserAccessValidator rol="admin" />
        <div className="card-cev height-80">
          <div className="card-cev-title">
            <span className="label">{LABELS_MENU.microsites_subtitle}</span>
            <button
              onClick={() =>
                this.props.history.push("/admin/menus/modify")
              }
              type="button"
              className="btn btn-outline-primary"
            >
              {LABELS_MENU.menu_create}
            </button>{" "}
          </div>

          <div className="card-cev-body">

            {this.state.loading && (
              <div className="fa-3x">
                <i className="fas fa-spinner fa-spin"></i>
              </div>
            )}
            {!this.state.loading && this.state.menus && (
              <SimpleDataTable
                id="simple-table"
                columns={headerMenu}
                data={menus}
                actions={[
                  {
                    action: "edit",
                    title: LABELS_MENU.edit,
                    classLaIcon: "la la-edit",
                  },
                  {
                    action: "delete",
                    title: LABELS_MENU.delete,
                    classLaIcon: "la la-trash",
                  },
                ]}
                outputEvent={this.actionEvent}
              ></SimpleDataTable>
            )}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (store) => ({
  search: store.app.keyword,
  user: store.auth.user,
});

export default connect(mapStateToProps, app.actions)(AdminMenusPage);
