import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
//import { toAbsoluteUrl } from "../../../theme/utils";
import * as app from "../../store/ducks/app.duck";
import * as UsersService from "../../services/UsersService";
//import moment from "moment";

import Form from "react-jsonschema-form-bs4";



const schema = {

  "type": "object",
  "title": "Editar usuario",
  "properties": {
    "roles": {
      "title": "Roles",
      "type": "array",
      "uniqueItems": true,
      "items": {
        "type": "string",
        "anyOf": [
          {
            "type": "string",
            "title": "Usuario",
            "enum": ["user"]
          },
          {
            "type": "string",
            "title": "Administrador",
            "enum": ["admin"]
          },
          {
            "type": "string",
            "title": "Revisor solicitudes",
            "enum": ["reviewer"]
          },
          {
            "type": "string",
            "title": "Aprobador solicitudes",
            "enum": ["approver"]
          },
          {
            "type": "string",
            "title": "Catalogador",
            "enum": ["catalogador"]
          },
          {
            "type": "string",
            "title": "Catalogador Gestor",
            "enum": ["catalogador_gestor"]
          },
          {
            "type": "string",
            "title": "Escucha",
            "enum": ["escucha"]
          },
          {
            "type": "string",
            "title": "Descarga",
            "enum": ["descarga"]
          },
          {
            "type": "string",
            "title": "Administración del Diccionario Colaborativo",
            "enum": ["admin_diccionario"]
          },
          {
            "type": "string",
            "title": "Invitado",
            "enum": ["invitado"]
          },
         
          {
            "type": "string",
            "title": "Tesauro",
            "enum": ["tesauro"]
          },
          {
            "type": "string",
            "title": "Comisionado",
            "enum": ["comisionado"]
          },
          {
            "type" : "string",
            "title" : "Revisión Bibliográfica",
            "enum" : ["revision_bibliografica"]
          },
          {
            "type": "string",
            "title": "Editor de colecciones",
            "enum": ["editor_archivo"],
          }
        ]
      }
    },
    "accessLevel": {
      "title": "Nivel de acceso",
      "type": "number",
      "enum": [4, 3, 2, 1],
    },
    "status": {
      "title": "Estado",
      "type": "string",
      "enum": ["Inactivo", "Activo"],
    },
    "downloadQuota": {
      "title": "Cuota Descargas",
      "type": "number"
    },
  }
}

const uiSchema = {
  "status": {
    "ui:widget": "radio",
    "ui:options": {
      "inline": true
    }
  },
}


class AdminUsersEditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: [],
    };
  }

  componentDidMount() {
    UsersService.get(this.props.match.params.id).then((data) => {
      if (data.status)
        data.status = "Activo"
      else
        data.status = "Inactivo"
      this.setState({ user: data })
      console.log("El usuario es: ", data)
    })
    document.getElementById("root_roles").setAttribute("size", "13");
  }

  onSubmit(formData, e) {
    //console.log("Guardando ", formData)
    formData.status = formData.status === "Activo"
    UsersService.update(this.props.match.params.id, formData).then((data) => {
      //console.log("El usuario es se guardó: ",data)
      alert("Usuario actualizado")
      this.props.history.push('/admin/users');
    })
  }

  onCancel() {
    this.props.history.push('/admin/users');
  }

  render() {

    return (
      <>
        <UserAccessValidator rol="admin" />
        <UserAccessValidator rol="admin" />
        <div className="card-cev">
          <div className="card-cev-title">
            <span className="label">Editar usuario</span>
          </div>
          <div className="card-cev-body">
            <div className="row">
              <div className="col-6">
                <table className="table table-striped table-hover table-bordered">
                  <tbody>
                    <tr>
                      <th scope="row"> <span><img src={this.state.user.photo} alt="user" />​</span></th>
                      <td>{this.state.user.name}</td>
                    </tr>
                    <tr>
                      <th scope="row">Correo</th>
                      <td>{this.state.user.username}</td>
                    </tr>
                    <tr>
                      <th scope="row">Registro</th>
                      <td>{this.state.user.date}</td>
                    </tr>
                    <tr>
                      <th scope="row">​Último ingreso:</th>
                      <td>{this.state.user.lastLogin}</td>
                    </tr>
                    <tr>
                      <th scope="row">​Estado:</th>
                      <td>{this.state.user.status}</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div className="col-6">
                <Form
                  schema={schema}
                  uiSchema={uiSchema}
                  formData={this.state.user}
                  onSubmit={({ formData }, e) => this.onSubmit(formData, e)}
                >
                  <div>
                    <button type="submit" class="btn btn-primary mr-4">Guardar</button>
                    <button type="button" class="btn btn-secondary" onClick={() => { this.onCancel() }}>Cancelar</button>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(AdminUsersEditPage);
