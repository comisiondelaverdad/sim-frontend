import React, { useState, useEffect } from "react";
import { Card, Row, Col } from "react-bootstrap";
import Accordion from "react-bootstrap/Accordion";

import { LABELS_MENU } from "../../../config/constLabels";

const AddElementsMenu = (props) => {
  const [show, setShow] = useState(true);
  const [key, setKey] = useState("colections");

  return (
    <>
      <Accordion style={{ marginBottom: "6px", width: "100%" }}>
        <Card>
          <Card.Header>
            <div className="d-flex justify-content-between align-items-center">
              <strong className="mx-auto">{LABELS_MENU.add_elements}</strong>
              <div>
                <button
                  onClick={(e) => {
                    setShow(!show);
                  }}
                  type="button"
                  className="btn btn-"
                >
                  <i className="fas fa-sort-down"></i>
                </button>
              </div>
            </div>
          </Card.Header>
          <Accordion.Collapse className={show ? "show" : ""} eventKey="1">
            <Card.Body id="1">
              <div className="sectionMicrosite">
                <i  className=" fa-2x fas fa-object-group"></i>
                <h6>Agregar sección</h6>
                <a onClick={() => props.addSection()} href="#">
                  <i className="fas fa-2x fa-arrow-right"></i>
                </a>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </>
  );
};

export default AddElementsMenu;
