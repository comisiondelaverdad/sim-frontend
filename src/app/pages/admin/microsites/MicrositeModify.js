import React, { useState, useEffect } from "react";
import UserAccessValidator from "../../../components/atoms/UserAccessValidator";
import { Button, Form, Card, Container, Row, Col } from "react-bootstrap";
import * as MicrositesService from "../../../services/MicrositesService";
import { useParams } from "react-router-dom";
import AddElementsMicrosite from "./AddElementsMicrosite";
import MicrositeEstructure from "../../../components/organisms/MicrositeEstructure";

import Accordion from "react-bootstrap/Accordion";
import { v4 as uuidv4 } from "uuid";
import { useTranslation } from "react-i18next";

function MicrositeModify(props) {
  const [t, i18n] = useTranslation("common");

  const styleCard = {
    header: { fontWeight: "900" },
    label: { fontWeight: "400 !important" },
  };
  const { id } = useParams();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [loading, setLoading] = useState(false);
  const [show1, setShow1] = useState(true);
  const [elements, setElements] = useState([]);
  const [save, setSave] = useState(false);
  const [errors, setErrors] = useState([]);
  const [section, setSection] = useState("");

  useEffect(() => {
    getMicrosite();
  }, []);
  useEffect(() => {
    if (save) sendform("section");
  }, [save]);

  const renderErrors = () => {
    if (errors && errors.length) {
      return errors.map((text) => <li>{text}</li>);
    }
  };

  const deleteElement = (id) => {
    const newElements = elements.filter((element) => element.id != id);
    setElements(newElements);
  };

  const validateSections = () => {
    let validate = true;
    if (elements && elements.length) {
      elements.forEach((element, index) => {
        if (!element.title) {
          validate = false;
          setErrors((errors) => [
            ...errors,
            `La sección ${index +
            1} no tiene un título, por favor ingrese un título válido`,
          ]);
        }
        if (!element.metadato) {
          validate = false;
          setErrors((errors) => [
            ...errors,
            `La sección ${index +
            1} no tiene metadato seleccionado, por favor selecciona un metadato o elimina la sección`,
          ]);
        }
        if (!element.values && !element.values.length) {
          validate = false;
          setErrors((errors) => [
            ...errors,
            `La sección ${index +
            1} no tiene valores de metadato seleccionado, por favor selecciona un metadato y sus valores o elimina la sección`,
          ]);
        }
      });
    }
    return validate;
  };

  const addSection = () => {
    const element = {
      title: "Nueva sección",
      metadato: "",
      values: [],
      relevants: [],
      id: uuidv4(),
    };
    setElements((elements) => [...elements, element]);
  };
  async function getMicrosite() {
    if (id) {
      setLoading(true);
      const res = await MicrositesService.get(id);
      setName(res.name);
      setDescription(res.description);
      setElements(res.elements);
      setSection(res.section);

      setLoading(false);
    }
  }
  function handleChange(event) {
    setName(event.target.value);
  }
  function handleChangeDescription(event) {
    setDescription(event.target.value);
  }

  const validateName = async (name) => {
    let exist = false;
    try {
      const res = await MicrositesService.findByName(name);
      console.log(res);
      if (res && res.length) {
        setErrors((errors) => [
          ...errors,
          "Este Nombre de micrositio ya existe, prueba con uno diferente",
        ]);
        exist = true;
      }
    } catch (error) { }
    return exist;
  };

  async function sendform(type = null) {
    setSave(false);
    setErrors([]);

    if (!name) {
      setErrors((errors) => [
        ...errors,
        "No es posible guardar un micrositio sin nombre",
      ]);
      return false;
    }
    if (name.includes("_")) {
      setErrors((errors) => [
        ...errors,
        "El nombre del micrositio no debe tener el caracter especial '_'",
      ]);
      return false;
    }
    if (name.includes("/")) {
      setErrors((errors) => [
        ...errors,
        "El nombre del micrositio no debe tener el caracter especial '/'",
      ]);
      return false;
    }

    const newName = name.replace(/ +(?= )/g, "");

    if (!validateSections()) {
      return false;
    }

    const form = { name: newName, description, elements, section };
    let res;
    setLoading(true);
    if (id) res = await MicrositesService.update(id, form);
    else {
      //validate if name exist on collections
      const resValidate = await validateName(newName);
      if (resValidate) {
        setLoading(false);
        return false;
      }
      res = await MicrositesService.create(form);
      if (res._id) {
        props.history.push("/admin/microsites/modify/" + res._id);
      }
    }

    if (!type) props.history.push("/admin/microsites/");
    else {
      setSave(false);
      setLoading(false);
      alert("Se ha guardado la sección");
    }
    return true;
  }

  const saveElement = async (item) => {
    const exist = elements.findIndex((p) => p.id == item.id);
    if (exist != "-1") {
      let newArr = [...elements];
      newArr[exist] = item;

      setElements(newArr);
    } else {
      setElements((elements) => [...elements, item]);
    }
    setSave(true);
  };

  const reOrder = (elements) => {
    setElements(elements);
  };
  function handleChangeSection(event) {
    setSection(event.target.value);
  }

  return (
    <>
      <UserAccessValidator rol="admin" />
      <div className="card-cev height-80">
        <div className="card-cev-title">
          <span className="label">
            {t("micrositios.configMicrosite")}
          </span>
        </div>
        <div className="card-cev-body">
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              props.history.push("/admin/microsites/");
            }}
          >
            <i className="fas fa-long-arrow-alt-left"></i>
          </a>
        </div>
        {loading ? (
          <div className="fa-3x">
            <i className="fas fa-spinner fa-spin"></i>
          </div>
        ) : !loading ? (
          <Button
            className="btn-outline-primary"
            onClick={() => sendform()}
            variant="btn-outline-primary"
            type="button"
          >
            {t("micrositios.saveMicrosite")}
          </Button>
        ) : (
          ""
        )}
        <Container>
          <Row style={{ marginTop: "6px" }}>
            <Col xs={12} sm={4}>
              <Accordion
                style={{ marginBottom: "6px", width: "100%" }}
              >
                <Card>
                  <Card.Header style={styleCard.header}>
                    <div className="d-flex justify-content-between align-items-center">
                      <strong className="mx-auto">
                        {t("micrositios.basicInfo")}
                      </strong>
                      <div>
                        <button
                          onClick={(e) => {
                            setShow1(!show1);
                          }}
                          type="button"
                          className="btn btn-"
                        >
                          <i className="fas fa-sort-down"></i>
                        </button>
                      </div>
                    </div>
                  </Card.Header>
                  <Accordion.Collapse
                    className={show1 ? "show" : ""}
                    eventKey="1"
                  >
                    <Card.Body>
                      <Form
                        onSubmit={(e) => {
                          e.preventDefault();
                        }}
                      >
                        <Form.Group controlId="">
                          <label>
                            {t("micrositios.nameMicrosite")}*
                          </label>
                          <Form.Control
                            type="text"
                            onChange={(e) => handleChange(e)}
                            value={name}
                          />
                        </Form.Group>
                        <Form.Group controlId="">
                          <label>
                            {t("micrositios.descriptionMicrosite")}
                          </label>
                          <Form.Control
                            type="text"
                            onChange={(e) =>
                              handleChangeDescription(e)
                            }
                            value={description}
                          />
                        </Form.Group>
                        <Form.Group controlId="exampleForm.ControlSelect1">
                          <label>{t("micrositios.section")}</label>
                          <Form.Control
                            value={section ? section : ""}
                            onChange={handleChangeSection}
                            as="select"
                          >
                            <option value="">Ninguna</option>
                            <option value="Conoce">Conoce</option>
                            <option value="Labverdad">
                              LabVerdad
                            </option>
                          </Form.Control>
                        </Form.Group>
                      </Form>
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
              <hr />
              <AddElementsMicrosite addSection={addSection} />
            </Col>
            <Col xs={12} sm={8}>
              {errors && errors.length ? (
                <div
                  style={{ transition: "2s all ease-in-out" }}
                  class="alert alert-danger"
                  role="alert"
                >
                  <ul>{renderErrors()}</ul>
                </div>
              ) : (
                ""
              )}

              <MicrositeEstructure
                elements={elements}
                saveElement={saveElement}
                reOrder={reOrder}
                idMenu={id}
                deleteElement={deleteElement}
              />
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}
export default MicrositeModify;
