import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../../components/atoms/UserAccessValidator";
import * as app from "../../../store/ducks/app.duck";
import * as MenusService from "../../../services/MenusService";
import * as MicrositesService from "../../../services/MicrositesService";

import { SimpleDataTable } from "../../../components/molecules/SimpleDataTable";
import Swal from "sweetalert2";
import { LABELS_MENU } from "../../../config/constLabels";

const headerMicrosites = [
  {
    title: "No",
    data: "No",
    width: "3%",
  },
  {
    title: "Micrositio",
    data: "name",
  },
  {
    title: LABELS_MENU.actions,
    data: "_id",
    type: "action",
    width: "8%",
  },
];

class AdminMicrositesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      microsites: [],
      loading: false,
    };
    this.actionEvent = this.actionEvent.bind(this);
  }

  loadData() {
    this.setState({ loading: true });
    MicrositesService.list("", this.state.page).then((data) => {
      if (data.length) {
        let i = 0;
        const dataMicrosites = data.map((u) => {
          i++;
          return {
            ...{
              _id: u._id,
              name: u.name,
              No: i,
            },
          };
        });

        this.setState({ microsites: dataMicrosites });
      }
      this.setState({ loading: false });
    });
  }

  componentDidMount() {
    this.loadData();
  }
  async delete(row) {
    let result = await Swal.fire({
      title: LABELS_MENU.delete_confirmation,
      showCancelButton: true,
      confirmButtonText: LABELS_MENU.delete,
      cancelButtonText: `Cancelar`,
    });

    if (result.isConfirmed) {
      const res = await MicrositesService.eliminar(row._id);
      this.loadData();
    }
  }

  actionEvent({ action, row }) {
    switch (action.action) {
      case "edit":
        this.props.history.push("/admin/microsites/modify/" + row._id);
        break;
      case "delete":
        this.delete(row);

        break;

      default:
        break;
    }
  }

  render() {
    let { microsites } = this.state;


    return (
      <>
        <UserAccessValidator rol="admin" />

        <div className="card-cev height-80">
          <div className="card-cev-title">
            <span className="label">Gestión de micrositios</span>
          </div>
          <div className="card-cev-body">
            <button
              onClick={() =>
                this.props.history.push(
                  "/admin/microsites/modify"
                )
              }
              type="button"
              className="btn btn-outline-primary"
            >
              Crear micrositio
            </button>{" "}
            <div className="table-responsive">
              
              { this.state.microsites && (
                <SimpleDataTable
                  id="simple-table"
                  columns={headerMicrosites}
                  data={microsites}
                  actions={[
                    {
                      action: "edit",
                      title: LABELS_MENU.edit,
                      classLaIcon: "la la-edit",
                    },
                    {
                      action: "delete",
                      title: LABELS_MENU.delete,
                      classLaIcon: "la la-trash",
                    },
                  ]}
                  outputEvent={this.actionEvent}
                ></SimpleDataTable>
              )}
            </div>
          </div>
        </div>

      </>
    );
  }
}

const mapStateToProps = (store) => ({
  search: store.app.keyword,
  user: store.auth.user,
});

export default connect(mapStateToProps, app.actions)(AdminMicrositesPage);
