import React, { Component } from "react";
import { connect } from "react-redux";
//import { toAbsoluteUrl } from "../../../theme/utils";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
import * as app from "../../store/ducks/app.duck";
import * as LocationsService from "../../services/LocationsService";
//import _ from "lodash";
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";

//import moment from "moment";
const resourceHeader = [
  /*
    {
      title: 'Acciones',
      data: '_id',
      type: 'action',
      width: "8%"
    },
  */
  {
    title: 'Tipo',
    data: 'admin_level',
  },
  {
    title: 'Departamento',
    data: 'dpto'
  },
  {
    title: 'Nombre departamento',
    data: 'nombre_dpt',
  },
  {
    title: 'Municipio',
    data: 'mpio'
  },
  {
    title: 'Nombre municipio',
    data: 'nombre_mpi',
  },
  /*
    {
      title: 'Nombre cabecera',
      data: 'nombre_cab'
    },
    {
      title: 'Clase de municipio',
      data: 'clasemun',
    },
    {
      title: 'Municipios',
      data: 'mpios',
    },
  */
  {
    title: 'Zona',
    data: 'zona',
  },
  {
    title: 'Oficina regional',
    data: 'of_reg',
  },
  {
    title: 'Reg zonas',
    data: 'reg_zonas',
  },
  {
    title: 'Area',
    data: 'area',
  },
  {
    title: 'Perimetro',
    data: 'perimeter',
  },
  {
    title: 'Hectareas',
    data: 'hectares',
  },
  /*
    {
      title: 'Poligono',
      data: 'poligon',
      type: 'poligon',
    },
    {
      title: 'Centroide',
      data: 'centroid',
      type: 'point',
    },
  */
];

class AdminUsersPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      locations: [],
    };
    this.actionEvent = this.actionEvent.bind(this);
  }

  componentDidMount() {
    LocationsService.list("", this.state.page).then((data) => {
      const dataUser = data.map((l) => {
        return {
          ...{
            _id: l._id,
            admin_level: l.admin_level,
            dpto: l.properties.DPTO,
            nombre_dpt: l.properties.NOMBRE_DPT,
            mpio: l.properties.MPIO ? l.properties.MPIO : "",
            nombre_mpi: l.properties.NOMBRE_MPI ? l.properties.NOMBRE_MPI : "",
            nombre_cab: l.properties.NOMBRE_CAB ? l.properties.NOMBRE_CAB : "",
            clasemun: l.properties.CLASEMUN ? l.properties.CLASEMUN : "",
            mpios: l.properties.MPIOS ? l.properties.MPIOS : "",
            zona: l.properties.ZONA ? l.properties.ZONA : "",
            of_reg: l.properties.OF_REG ? l.properties.OF_REG : "",
            reg_zonas: l.properties.REG_ZONAS ? l.properties.REG_ZONAS : "",
            area: l.properties.AREA,
            perimeter: l.properties.PERIMETER,
            hectares: l.properties.HECTARES,
            //poligon: l.geometry,
            //centroid: l.centroid,
            pais: l.pais,
            localidad: l.localidad,
          },
        }
      });
      this.setState({ locations: dataUser })
    })
  }

  actionEvent({ action, row }) {
    switch (action.action) {
      case 'edit':
        this.props.history.push('/admin/location/' + row._id)
        break;

      default:
        break;
    }
  }

  render() {
    let { locations } = this.state;

    return (
      <>
        <UserAccessValidator rol="admin" />
        <div className="card-cev height-80">
          <div className="card-cev-title">
            <span className="label">Administración de ubicaciones</span>
          </div>
          <div className="card-cev-body">
            <SimpleDataTable
              id='simple-table'
              columns={resourceHeader}
              data={locations}
              actions={[
                { action: 'edit', title: 'Editar', classLaIcon: 'la la-edit' },
              ]}
              outputEvent={this.actionEvent}
            ></SimpleDataTable>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(AdminUsersPage);
