import React, { Component } from "react";
import {connect} from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
//import { toAbsoluteUrl } from "../../../theme/utils";
import * as app from "../../store/ducks/app.duck";
import * as UsersService from "../../services/UsersService";
import * as SearchService from "../../services/SearchService";
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";

//import moment from "moment";

const resourceHeader = [
  {
    title: 'Recurso',
    data: 'ident',
  },
  {
    title: 'Fecha',
    data: 'date',
  },
  {
    title: 'Tipo',
    data: 'type'
  },
  {
    title: 'Enlace',
    data:'ident',
    type: 'action'
  }
];

class AdminUsersEditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: [],
      identifier:undefined,
    };
    this.actionEvent = this.actionEvent.bind(this);
  }

  

  componentDidMount() {
    UsersService.get(this.props.match.params.id).then((data) => {
      if(data.status)
        data.status = "Activo"
      else
        data.status = "Inactivo"
      this.setState({user: data})
      SearchService.getResourceHistory(data.username , this.state.page).then((data)=>{
        const dataView = data.hits.hits.map((r) => {
          return {
           ident: r._source.result.ident ? r._source.result.ident : "No registra",
           date: r._source.date ? r._source.date : "No registra",
           type: r._source.result.type ? r._source.result.type : "No registra",
           url: r._source.result.ident ? r._source.result.ident : "No registra"
          };
        });
        this.setState({historyData:dataView})
      });
    });
    
  }
  
  
  onCancel(){
      this.props.history.push('/admin/history');
  }

  viewSorce(url){
    this.props.history.push(url);
  }

  actionEvent({ action, row }) {
    this.props.history.push('/detail/'+row.ident);
  }

  redirect(path) {
    this.props.history.push(`${path}`);
}

  render() {
    //const { rowSelected } = this.state;
       /* const selectRow = {
            mode: 'radio',
            clickToSelect: true,
            onSelect: this.handleOnSelect,
        };*/
    return (
      <>
      <UserAccessValidator rol="admin" />
        <div className="card-cev height-80">
          <div className="card-cev-title">
            <span className="label">Historial recursos visitados</span>
          </div>
          <div className="card-cev-body">
            <table className="table table-striped table-hover table-bordered">
              <tbody>
                <tr>
                  <th scope="row"><span><img src={this.state.user.photo} alt="user" />​</span></th>
                  <td>{this.state.user.name}</td>
                </tr>
                <tr>
                  <th scope="row">Correo</th>
                  <td>{this.state.user.username}</td>
                </tr>
              </tbody>
            </table>
            <div className="cev-portlet__body table-responsive">
              <SimpleDataTable
                id='simple-table'
                columns={resourceHeader}
                data={this.state.historyData}
              ></SimpleDataTable>
            </div>

            <div>
              <button type="button" className="btn btn-secondary" onClick={() => { this.onCancel() }}>Cancelar</button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});


export default connect(
  mapStateToProps,
  app.actions
)(AdminUsersEditPage);
