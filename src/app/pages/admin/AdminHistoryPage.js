import React, { Component } from "react";
import { connect } from "react-redux";
//import { toAbsoluteUrl } from "../../../theme/utils";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
import * as app from "../../store/ducks/app.duck";
import * as UsersService from "../../services/UsersService";
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";

//import moment from "moment";

const resourceHeader = [
  {
    title: 'Acciones',
    data: '_id',
    type: 'action',
    width: "8%"
  },
  {
    title: 'Foto',
    data: 'photo',
    type: 'miniature'
  },
  {
    title: 'Nombre',
    data: 'name',
  },
  {
    title: 'Correo',
    data: 'username'
  }
];
class AdminUsersPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      users: [],
    };
    this.actionEvent = this.actionEvent.bind(this);
  }

  componentDidMount() {
    UsersService.list("", this.state.page).then((data) => {
      const dataUser = data.map((u) => {
        return {
          ...{
            _id: u._id,
            photo: u.photo ? u.photo : 'no-photo',
            name: u.name,
            username: u.username,
            roles: u.roles,
            status: u.status === 1 ? "Activo" : "Inactivo",
            accessLevel: u.accessLevel,
          },
        }
      });
      this.setState({ users: dataUser })
    })
  }

  actionEvent({ action, row }) {
    switch (action.action) {
      case 'history':
        this.props.history.push('/admin/history/' + row._id)
        break;
      case 'view':
        this.props.history.push('/admin/view/' + row._id)
        break;
      default: break;
    }
  }

  render() {
    let { users } = this.state;

    return (
      <>
        <UserAccessValidator rol="admin" />
        <div className="card-cev height-80">
          <div className="card-cev-title">
            <span className="label">Historial de búsqueda</span>
          </div>
          <div className="card-cev-body">
            <SimpleDataTable
              id='simple-table'
              columns={resourceHeader}
              data={users}
              actions={[
                { action: 'history', title: 'Historial de búsqueda', classLaIcon: 'la la-history' },
                { action: 'view', title: 'Historial vistas', classLaIcon: 'la la-eye' },
              ]}
              outputEvent={this.actionEvent}
            ></SimpleDataTable>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(AdminUsersPage);
