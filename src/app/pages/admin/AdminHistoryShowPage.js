import React, { Component } from "react";
import { connect } from "react-redux";
import UserAccessValidator from "../../components/atoms/UserAccessValidator";
//import { toAbsoluteUrl } from "../../../theme/utils";
import * as app from "../../store/ducks/app.duck";
import * as UsersService from "../../services/UsersService";
import * as SearchService from "../../services/SearchService";
import { SimpleDataTable } from "../../components/molecules/SimpleDataTable";

//import moment from "moment";


const resourceHeader = [
  {
    title: 'Búsqueda',
    data: 'query',
  },
  {
    title: 'Fecha',
    data: 'date',
  }
];

class AdminUsersEditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: []
    };
  }

  componentDidMount() {
    UsersService.get(this.props.match.params.id).then((data) => {
      if (data.status)
        data.status = "Activo"
      else
        data.status = "Inactivo"
      this.setState({ user: data })
      SearchService.list(data.username, this.state.page).then((data) => {
        const dataHistory = data.hits.hits.map((r) => {
          return {
            query: r._source.params.query.q ? r._source.params.query.q : "No registra",
            date: r._source.date ? r._source.date : "No registra"
          };
        });

        this.setState({ historyData: dataHistory })
      });
      console.log("El usuario es: ", data)
    });
    console.log("aca los parametros" + JSON.stringify(this.props.match.params));


  }

  onCancel() {
    this.props.history.push('/admin/history');
  }

  render() {

    return (
      <>
        <UserAccessValidator rol="admin" />
        <div className="card-cev height-80">
          <div className="card-cev-title">
            <span className="label">Historial de búsqueda por usuario</span>
          </div>
          <div className="card-cev-body">
            <table className="table table-striped table-hover table-bordered">
              <tbody>
                <tr>
                  <th scope="row"><span><img src={this.state.user.photo} alt="user" />​</span></th>
                  <td>{this.state.user.name}</td>
                </tr>
                <tr>
                  <th scope="row">Correo</th>
                  <td>{this.state.user.username}</td>
                </tr>
              </tbody>
            </table>
            <div className="cev-portlet__body table-responsive">
              <SimpleDataTable
                id='simple-table'
                columns={resourceHeader}
                data={this.state.historyData}
              ></SimpleDataTable>
            </div>

            <div>
              <button type="button" className="btn btn-secondary" onClick={() => { this.onCancel() }}>Cancelar</button>
            </div>
          </div>
        </div>
      </>
    )
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(AdminUsersEditPage);
