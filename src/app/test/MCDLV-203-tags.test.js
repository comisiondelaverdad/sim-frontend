const { describe, it, after, before } = require('mocha');
const Page = require('../test/selenium/basePage');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => {});

(async function example() {
    try {
        describe ('Labelled transcription automated testing', async function () {
            this.timeout(50000);
            let driver, page;

             beforeEach (async () => {
                page = new Page();
                driver = page.driver;
               
            });

            it ('Get color tag', async () => {
                const result = await page.getLabellendTranscription();
                expect(result).to.include('Transcripción etiquetada');
            });

           
        });
    } catch (ex) {
        console.log (new Error(ex.message));
    } finally {

    }
})();