const {Builder, By, until, Key} = require('selenium-webdriver');

const firefox = require('selenium-webdriver/firefox');
let o = new firefox.Options();


var Page = function() {
     this.driver = new Builder()
                .forBrowser('firefox')
                .build();;

    //Get labelled transcriptions
    this.getLabellendTranscription = async function() {
        try {
          //** Auth
        await this.driver.get('https://pruebasbuscador.comisiondelaverdad.co/detail/001-VI-00001');
        await this.driver.executeScript("return document.getElementById('splash-screen').remove();")
        await this.driver.wait(until.elementLocated(By.className('btn-primary')), 100000).click();         
        await this.driver.wait(until.elementLocated(By.id('identifierId')), 100000).sendKeys('sim'); 
        await this.driver.wait(until.elementLocated(By.className('RveJvd snByac')), 100000).click();
        await this.driver.wait(until.elementLocated(By.id('password')), 100000);
        await this.driver.wait(until.elementLocated(By.xpath("//input[@name='password']")), 100000).sendKeys('c0m1s10n'); 
        await this.driver.findElement(By.id("passwordNext")).sendKeys('webdriver', Key.ENTER);

        //Auth **
        await this.driver.wait(until.elementLocated(By.linkText("Resumen")), 100000);
        var element = await this.driver.findElement(By.linkText("Transcripción etiquetada"));
        await this.driver.executeScript("arguments[0].scrollIntoView()", element);
        await element.click();

       // return await element.click();   RveJvd snByac
  
        
      }catch (ex){
           console.log (new Error(ex.message));
      } 
    };
   
};

module.exports = Page;



