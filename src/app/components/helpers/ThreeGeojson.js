import * as THREE from 'three';

export default class ThreeGeojson {
    constructor(w, h) {
        this.width = w
        this.height = h
        // this.getGeometry('https://gist.githubusercontent.com/john-guerra/43c7656821069d00dcbc/raw/be6a6e239cd5b5b803c6e7c2ec405b793a9064dd/Colombia.geo.json')
    }

    getGeometry(url, width) {
        var json_obj = this.getJSON(url)
        let mesh = this.generateGeo(json_obj['features'], width)
        return mesh
    }

    getLines(url) {
        var json_obj = this.getJSON(url)
        let mesh = this.generateLines(json_obj['features'])
        return mesh
    }

    generateGeo(features, w_) {
        let mesh = []

        features.forEach(i => {
            let newF = {
                name: i.properties["NOMBRE_DPT"],
                geometry: []
            }
            i.geometry.coordinates.forEach(g => {
                if (g.length > 1) {
                    var shape = new THREE.Shape()
                    g.forEach((s, p) => {
                        let coorXY = this.mercatorProjection(s[1], s[0])
                        if (p === 0) {
                            shape.moveTo(coorXY.x, coorXY.y)
                        } else {
                            shape.lineTo(coorXY.x, coorXY.y)
                        }
                    })
                    var extrudeSettings = {
                        steps: 2,
                        depth: w_,
                        bevelEnabled: false,
                    };
                    var geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
                    var material = new THREE.MeshBasicMaterial({
                        color: 0x333333,
                        // wireframe: true
                    });
                    var mesh = new THREE.Mesh(geometry, material);
                    newF.geometry.push(mesh)
                } else {
                    g.forEach(t => {
                        var shape = new THREE.Shape()
                        t.forEach((s, p) => {
                            let coorXY = this.mercatorProjection(s[1], s[0])
                            if (p === 0) {
                                shape.moveTo(coorXY.x, coorXY.y)
                            } else {
                                shape.lineTo(coorXY.x, coorXY.y)
                            }
                        })
                        var extrudeSettings = {
                            steps: 2,
                            depth: w_,
                            bevelEnabled: false,
                        };
                        var geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
                        var material = new THREE.MeshBasicMaterial({
                            color: 0x333333
                        });
                        var mesh = new THREE.Mesh(geometry, material);
                        newF.geometry.push(mesh)
                    })
                }
            })

            mesh.push(newF)
        })

        return mesh
    }

    generateLines(features) {
        let mesh = []

        features.forEach(i => {
            let newF = {
                name: i.properties["NOMBRE_DPT"],
                geometry: []
            }
            i.geometry.coordinates.forEach(g => {
                if (g.length > 1) {
                    var points = [];

                    g.forEach((s, p) => {
                        let coorXY = this.mercatorProjection(s[1], s[0])
                        if (p === 0) {
                            points.push(new THREE.Vector3(coorXY.x, coorXY.y, 0));
                        } else {
                            points.push(new THREE.Vector3(coorXY.x, coorXY.y, 0));
                        }
                    })
                    var geometry = new THREE.BufferGeometry().setFromPoints(points);
                    var material = new THREE.LineBasicMaterial({
                        color: 0x333333
                    });
                    var mesh = new THREE.Line(geometry, material);
                    newF.geometry.push(mesh)
                } else {
                    g.forEach(t => {
                        var points = [];

                        t.forEach((s, p) => {
                            let coorXY = this.mercatorProjection(s[1], s[0])
                            if (p === 0) {
                                points.push(new THREE.Vector3(coorXY.x, coorXY.y, 0));
                            } else {
                                points.push(new THREE.Vector3(coorXY.x, coorXY.y, 0));
                            }
                        })
                        var geometry = new THREE.BufferGeometry().setFromPoints(points);
                        var material = new THREE.LineBasicMaterial({
                            color: 0x333333
                        });
                        var mesh = new THREE.Line(geometry, material);
                        newF.geometry.push(mesh)
                    })
                }
            })

            mesh.push(newF)
        })

        return mesh
    }

    /**
     * https://stackoverflow.com/questions/14329691/convert-latitude-longitude-point-to-a-pixels-x-y-on-mercator-projection/14457180#14457180
     * 
     * @param {float} lat 
     * @param {float} lon 
     */
    mercatorProjection(lat, lon) {
        let x = (lon + 180) * (this.width / 360)
        let latRad = lat * Math.PI / 180
        let mercN = Math.log(Math.tan((Math.PI / 4) + (latRad / 2)))
        let y = (this.height / 2) - (this.width * mercN / (2 * Math.PI))
        return {
            x: x - x / 10,
            y: -y
        }
    }

    /**
     * https://stackoverflow.com/a/22790025
     * 
     * @param {string} url 
     */
    getJSON(url) {
        let resp
        let Httpreq = new XMLHttpRequest();
        Httpreq.open("GET", url, false);
        Httpreq.send(null);
        resp = JSON.parse(Httpreq.responseText)
        return resp
    }
}