import * as THREE from 'three';
import CameraControls from "camera-controls";
import ThreeGeojson from './ThreeGeojson';

CameraControls.install({
    THREE: THREE
});

export default class ThreeMapHelper {
    constructor(elem) {
        this.elem = elem

        this.geojson = new ThreeGeojson(200, 300)
        this.start()
    }

    start() {
        this.THREE = THREE
        var width = 1200;
        var height = 700;

        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xFFFFFF);
        const camera = new THREE.PerspectiveCamera(
            50,
            width / window.innerHeight,
            0.1,
            1000
        );
        // var camera = new THREE.OrthographicCamera(width / -2, width / 2, height / 2, height / -2, 1, 1000);
        const renderer = new THREE.WebGLRenderer({
            antialias: true,
        });
        const cameraControls = new CameraControls(camera, renderer.domElement);
        // cameraControls.maxPolarAngle = 3.14 / 2 - 0.2;

        renderer.setSize(width, height);

        this.elem.appendChild(renderer.domElement);

        this.mesh = this.geojson.getGeometry('https://gist.githubusercontent.com/john-guerra/43c7656821069d00dcbc/raw/be6a6e239cd5b5b803c6e7c2ec405b793a9064dd/Colombia.geo.json', 0.15)

        this.w_mesh = this.geojson.getLines('https://gist.githubusercontent.com/nestorandrespe/e3695c1d3961be676b06624e699d3998/raw/c3749e7df2a318d1d39e6ef2be0c4b064ff149d2/World%2520contours.json')

        this.colombia = new THREE.Group();
        this.mesh.forEach(i => {
            if (i.geometry.length > 0) {
                i.geometry.forEach(g => {
                    // console.log(g)
                    g.material.color = {
                        r: 1,
                        g: 1,
                        b: 1
                    }
                    this.colombia.add(g)
                })
            }
        })

        this.d_mesh = this.geojson.getLines('https://gist.githubusercontent.com/john-guerra/43c7656821069d00dcbc/raw/be6a6e239cd5b5b803c6e7c2ec405b793a9064dd/Colombia.geo.json')

        this.d_mesh.forEach(i => {
            if (i.geometry.length > 0) {
                i.geometry.forEach(g => {
                    // console.log(g)
                    g.position.z = 0.2
                    g.material.color = {
                        r: 0.3,
                        g: 0.3,
                        b: 0.3
                    }
                    this.colombia.add(g)
                })
            }
        })


        this.m_mesh = this.geojson.getLines('https://gist.githubusercontent.com/nestorandrespe/1b87fc08bc60431a1c3363e318cad02d/raw/41d0e4e9686e5953ddeb5b1c7a2e3562df199edd/mpios.json')

        this.m_mesh.forEach(i => {
            if (i.geometry.length > 0) {
                i.geometry.forEach(g => {
                    // console.log(g)
                    g.position.z = 0.18
                    g.material.color = {
                        r: 0.8,
                        g: 0.8,
                        b: 0.8
                    }
                    this.colombia.add(g)
                })
            }
        })


        this.world = new THREE.Group();
        this.w_mesh.forEach(i => {
            if (i.geometry.length > 0) {
                i.geometry.forEach(g => {
                    console.log(g)
                    g.material.color = {
                        r: 0.2,
                        g: 0.2,
                        b: 0.2
                    }
                    this.world.add(g)
                })
            }
        })

        this.coor = {
            coor1: { lat: 4.61495996452868, lon: -74.0694100689143 },
            coor2: { lat: 3.4340199641883373, lon: -76.52645006775856 }
        }


        var pos = this.geojson.mercatorProjection(this.coor.coor1.lat, this.coor.coor1.lon)
        var pos2 = this.geojson.mercatorProjection(this.coor.coor2.lat, this.coor.coor2.lon)
        var dist = Math.hypot((pos2.x - pos.x), (pos2.y - pos.y))
        console.log(pos, pos2, dist)

        var geometry = new THREE.RingGeometry(dist - 0.07, dist, 30, 1, 0, 3.14);
        var material = new THREE.MeshBasicMaterial({ color: 0xffff00, side: THREE.DoubleSide });

        // var points = [];
        // points.push( new THREE.Vector3( pos.x, pos.y, 100 ) );
        // points.push( new THREE.Vector3( pos2.x, pos2.y, 100 ) );

        // var geometry = new THREE.BufferGeometry().setFromPoints( points );

        var mesh = new THREE.Mesh(geometry, material);
        // mesh.geometry.rotateY(Math.PI/2)
        mesh.geometry.rotateX(Math.PI / 2)
        mesh.position.x = pos.x
        mesh.position.y = pos.y
        this.scene.add(mesh);

        // var geometry = new THREE.BoxGeometry(100, 100, 100);
        // var material = new THREE.MeshBasicMaterial({
        //     color: 0x00ff00
        // });
        // var cube = new THREE.Mesh(geometry, material);
        // this.scene.add(cube);

        // var axesHelper = new THREE.AxesHelper(5);
        // this.scene.add(axesHelper);

        let center = this.geojson.mercatorProjection(4.624335, -74.063644)

        // colombia.rotation._x = Math.PI
        // colombia.position.y = -center.y
        // colombia.position.x = -center.x

        this.scene.add(this.world)
        this.scene.add(this.colombia)

        const render = () => {
            requestAnimationFrame(render);

            renderer.render(this.scene, camera);

        }



        render()
        // cameraControls.rotateTo(20, 20, true);
        // cameraControls.dollyTo(150, true);
        cameraControls.setLookAt(center.x, center.y, 13, center.x, center.y, 0, true)
        // cameraControls.rotateTo(1, 0, true);
        // cameraControls.dollyTo(1, true);
    }

    unmount() {
        this.scene.remove(this.colombia)
        this.mesh.forEach(i => {
            if (i.geometry.length > 0) {
                i.geometry.forEach(g => {
                    g.geometry.dispose()
                    g.material.dispose()
                })
            }
        })
    }


}