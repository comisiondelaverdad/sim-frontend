import React, {Component} from "react";
import {connect} from "react-redux";
import PDFViewer from "./PDFViewer";
import ImgViewer from "./ImgViewer";
import HTMLViewer from "./HTMLViewer";
import MediaViewer from "./MediaViewer";
import ZIPViewer from "./ZIPViewer";
import * as RecordsService from "../../services/RecordsService";
import * as LogsService from "../../services/LogsService";
import {Alert, Container} from "react-bootstrap";
import MicroData from "./MicroData";
import VizViewer from "./../../components/molecules/VizViewer";
import {saveAs} from 'file-saver';
import {DOWNLOAD_ACCESS_LEVEL} from "../../config/const";
import * as UsersService from "../../services/UsersService";

class Records extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ver: false,
      loading: true,
      file: '',
      quota: undefined,
      loadFile: undefined
    };
    this.loadFiles = this.loadFiles.bind(this);
  }

  componentDidMount() {
    if(this.props.from !== "zip"){
      this.loadFiles();
    }
    else{
      this.setState({ver: true, file: this.props.record.file, loading: false});
    }
    UsersService.downloadQuota().then(data=>{
      this.setState({quota:data});
    })
  }

  loadFiles() {
    const types = ["rtf", "doc", "docx", "csv", "xls", "xlsx", "pdf", "wav", "mp3", "mp4", "m4a", "wma", "ppt", "pptx", "odp", "odt", "jpeg", "jpg", "png", "tiff", "tif", "html", "txt", "otr","bmp","gif","zip", "ico", "wmf", "hdr", "m4v", "webm", "mov", "js", "eml", "gdoc", "gsheet", "htm", "mht", "msg", "xml", "twbx"];
    const type = this.props.record.filename.split('.').pop().toLowerCase();
    if (types.includes(type)) {
      RecordsService.serviceFile(this.props.record._id).then((data) => {
        this.setState({ver: true, file: data, loading: false});
      }).catch((err) => {
        if(err == 500)
          this.setState({loadFile: false});
        this.setState({ver: false, file: '', loading: false});
      });
    } else {
      this.setState({ver: false, file: '', loading: false});
    }
  }

  getExtention() {
    let type = ""
    if(this.props.record.filename.includes('.'))    
      type = this.props.record.filename.split('.').pop().toLowerCase();
    return type;
  }

  getWarningMessage(type) {
    if(!this.state.loadFile){
      return "El archivo no se encuentra en la ruta especificada";
    }
    return "Archivo no soportado " + type;
  }

  download(pType='') {
    if(this.state.quota.download || pType === 'twbx') {
      let type = this.getExtention();
      if(this.props.record.origin === "Microdato"){
        let pdfTypes = ["rtf", "docx","doc","pdf","ppt","pptx","odt","odp","jpg", "jpeg", "png", "tiff","bmp","gif","tif", "ico", "wmf", "hdr"];
        if(pdfTypes.indexOf(type) !== -1){
          type = "pdf";
        }
      }
      else{
        let pdfTypes = ["rtf", "xls", "docx","doc","pdf","ppt","pptx","odt","odp","jpg", "jpeg", "png", "tiff","bmp","gif","tif", "ico", "wmf", "hdr"];
        if(pdfTypes.indexOf(type) !== -1){
          type = "pdf";
        }
      }
      let nameFile = this.props.record.metadata.firstLevel.title.replace("."+type,"");
      saveAs(this.state.file, (this.props.record.origin === "ModuloCaptura" ? this.props.record.type : nameFile) + "." + type);
      let logInfo = {
        user: "",
        from: "records", 
        action: "Download File",
        message: "",
        metadata: {
          ident: this.props.record.ident,
          file:{
            filename: this.props.record.filename,
            extension: type
          },
          origin: this.props.record.origin,
          ip: ""
        }
      }

      LogsService.serviceAdd(logInfo)
      .then(
        (data) => {
          UsersService.downloadQuota().then(data=>{
            this.setState({quota:data})
          });
        },
        (error) => {
          console.log("Error",error);
        }
      )
    }
  }

  render() {
    const type = this.getExtention();
    const message = this.getWarningMessage(type);
    let microdataTypes = [];
    let pdfTypes = [];
    if(this.props.record.origin === "Microdato"){
      microdataTypes = ["csv", "xls", "xlsx"];
      pdfTypes = ["rtf", "docx", "doc", "pdf", "ppt", "pptx", "odt", "odp", "jpg", "jpeg", "png", "tiff", "bmp", "gif", "tif", "ico", "wmf", "hdr"];  
    }
    else{
      microdataTypes = ["csv", "xlsx"];
      pdfTypes = ["xls", "rtf", "docx", "doc", "pdf", "ppt", "pptx", "odt", "odp", "jpg", "jpeg", "png", "tiff", "bmp", "gif", "tif", "ico", "wmf", "hdr"];
    }
    const mediaTypes = ["wav", "mp3", "mp4", "m4a", "wma", "wmv", "m4v", "webm", "mov"];
    const imgTypes = [];
    const htmlTypes = ["html","otr", "txt", "js", "eml", "gdoc", "gsheet", "htm", "mht", "msg", "xml"];
    const jsTypes = ["js"];
    const zipTypes = ["zip"];

    return (
      <>
        <Container fluid={true}>
          {this.props.record &&
          <>
            {this.state.loading &&
              <Alert variant="success">
                <div className="alert-icon">
                  <i className="fas fa-sync fa-spin"/>
                </div>
                <div className="alert-text">Cargando...</div>
              </Alert>    
            }
            {!this.state.ver && !this.state.loading &&
              <Alert variant="danger">
                <div className="alert-icon">
                  <i className="flaticon-warning"/>
                </div>
                <div className="alert-text">{message}</div>
              </Alert>    
            }
            {(type === "twbx" || (this.state.quota && this.state.quota.download && this.props.user.roles.includes('descarga') && parseInt(this.props.record.metadata.firstLevel.accessLevel,10) >= DOWNLOAD_ACCESS_LEVEL)) &&
              <div style={{display: 'flex', justifyContent: "end", marginBottom: "20px"}}>
                <button className="btn btn-icon btn-success" onClick={() => this.download(type)}>
                  <i className="flaticon2-download"/>
                </button>
                <>
                  {this.state.quota && this.props.user.roles.includes('descarga') &&
                    <button className="btn btn-icon btn-quota">
                      {`Quota ${this.state.quota.downloaded} / ${this.state.quota.quota}`} 
                    </button>
                  }
                </>
              </div>
            }
            {pdfTypes.indexOf(type) !== -1 && this.state.ver && this.state.file &&
              <PDFViewer file={this.state.file} id={this.props.record.ident} type={type}/>
            }
            {microdataTypes.indexOf(type) !== -1 && this.state.ver && this.state.file &&
              <MicroData file={this.state.file} record={this.props.record} id={this.props.record.ident} type={type} filename={this.props.record.filename.split('.').shift()} />
            }
            {mediaTypes.indexOf(type) !== -1 && this.state.ver && this.state.file &&
              <MediaViewer file={this.state.file} id={this.props.record.ident} type={type}/>
            }
            {imgTypes.indexOf(type) !== -1 && this.state.ver && this.state.file &&
              <ImgViewer file={this.state.file} id={this.props.record.ident} type={type}/>
            }
            {jsTypes.indexOf(type) !== -1 && this.state.ver && this.state.file && this.props.record.origin === "CatalogacionFuentesInternas" && this.props.record.type === "Código fuente" &&
              <VizViewer record={this.props.record} metadata={this.props.record.metadata.firstLevel} />
            }
            {htmlTypes.indexOf(type) !== -1 && this.state.ver && this.state.file && this.props.record.origin !== "CatalogacionFuentesInternas" && this.props.record.type !== "Código fuente" &&
              <HTMLViewer file={this.state.file} id={this.props.record.ident} type={type} />
            }
            {zipTypes.indexOf(type) !== -1 && this.state.ver && this.state.file &&
              <ZIPViewer file={this.state.file} id={this.props.record.ident} type={type} fileName={this.props.record.filename.split('/').pop().toUpperCase()} />
            }
          </>
          }
        </Container>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps
)(Records);