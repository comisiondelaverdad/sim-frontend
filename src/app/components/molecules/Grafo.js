import React , {Component} from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3'

/**
 * Bloque para generar un Grafo en D3
 * 
 * @version 0.1
 */

export default class Grafo extends Component {
    static propTypes = {
        /** Función D3 para la escala de color */
        color: PropTypes.func,
        /** Network: Objeto con los nodos y links del grafo */
        network: PropTypes.object,
        /** Entidades seleccionadas por click */
        selected: PropTypes.array
      }

      
    constructor(props){
        super(props)
        this.r = 7
    }

    componentDidMount(){
        console.log(this.props.network)
        this.svg = d3.select(this.refs.d3_canvas)
        this.updateViz()
    }

    componentDidUpdate() {
        // let r = this.r

        if(this.props.selected.length > 0){
            this.node
            .transition()
            .attr("fill", d => {
                if(this.props.selected.includes(d.name))
                    return this.props.color(d.group)
                else
                    return '#fff'
            })
            .attr("stroke", d => {
                if(this.props.selected.includes(d.name))
                    return '#fff'
                else
                    return '#ccc'
            })
        } else {
            this.node
            .transition()
            .attr("fill", d => {
                return this.props.color(d.group)
            })
            .attr("stroke", d => {
                return '#fff'
            })
        }
    }

    updateViz() {
        const intensity = d3.scaleSequential(d3.interpolateGreys).domain([-100,this.props.network.max])
        const distancia = d3.scaleLinear().domain([this.props.network['link-data'][0].value,0]).range([25,40])
        const grueso = d3.scaleLinear().domain([this.props.network['link-data'][0].value,0]).range([4,1])
        const radio = d3.scaleLinear().domain([0,this.props.network['node-data'][0].value]).range([5,20])
        const fuerza_atraccion = d3.scaleLinear().domain([this.props.network['link-data'][0].value,0]).range([1,0])
        // const r = this.r

        const fade = opacity => {
            return d => {
              leyenda.style('visibility', o => d.index === o.index ? "visible" : "hidden" );
              if(opacity === 1){
                this.node.style('opacity', 1)
                leyenda.style('visibility', 'hidden')
                link.style('stroke-opacity', 1)
              }
            };
          }

        const drag = simulation => {
  
            function dragstarted(d) {
              if (!d3.event.active) simulation.alphaTarget(0.3).restart();
              d.fx = d.x;
              d.fy = d.y;
            }
            
            function dragged(d) {
              d.fx = d3.event.x;
              d.fy = d3.event.y;
            }
            
            function dragended(d) {
              if (!d3.event.active) simulation.alphaTarget(0);
              d.fx = null;
              d.fy = null;
            }
            
            return d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended);
          }
        const simulation = d3.forceSimulation(this.props.network['node-data'])
            .force("link", d3.forceLink(this.props.network['link-data']).id((d,i) => i).distance(d => distancia(d.value)).strength(d=>fuerza_atraccion(d.value)))
            .force("collide", d3.forceCollide(d=>radio(d.value)).iterations(2))
            .force("charge", d3.forceManyBody().strength(-10))
            .force("center", d3.forceCenter((900 / 2), (900 / 2)))

        const link = this.svg.append('g')
            .selectAll('line')
            .data(this.props.network['link-data'])
            .join('line')
            .attr('stroke-width', d=>grueso(d.value))
            .attr('stroke', d => intensity(d.value))

        this.node = this.svg.append("g")
            .attr("stroke", "#fff")
            .attr("stroke-width", 1.5)
            .selectAll("circle")
            .data(this.props.network['node-data'])
            .join("circle")
            .style('cursor', 'pointer')
            .attr("fill", d => this.props.color(d.group))
            .attr("r", d=>radio(d.value))
            .call(drag(simulation))
            .on('mouseover.fade', fade(0.1))
            .on('mouseout.fade', fade(1))
              
        const leyenda = this.svg.append('g')
            .selectAll('text')
            .data(this.props.network['node-data'])
            .join('text')
            .text(d => d.name)
            .attr('font-size',10)
            .call(drag(simulation))
            .on('mouseover.fade', fade(0.1))
            .on('mouseout.fade', fade(1))

        simulation.on("tick", () => {
            link
                .attr("x1", d => d.source.x)
                .attr("y1", d => d.source.y)
                .attr("x2", d => d.target.x)
                .attr("y2", d => d.target.y)
        
            this.node
                .attr("cx", d => d.x = Math.max(radio(d.value), Math.min(900 - radio(d.value), d.x)))
                .attr("cy", d => d.y = Math.max(radio(d.value), Math.min(900 - radio(d.value), d.y)))

            leyenda
                .attr("x", d => d.x + 10)
                .attr("y", d => d.y)
                .attr("visibility", "hidden");
        })

        const linkedByIndex = {};

        this.props.network['link-data'].map(d => {
            linkedByIndex[`${d.source.index},${d.target.index}`] = 1;
            return ''
        });

        // function isConnected(a, b) {
        //     return linkedByIndex[`${a.index},${b.index}`] || linkedByIndex[`${b.index},${a.index}`] || a.index === b.index;
        // }
    }

    render(){
        return(
            <>
            <svg
              ref="d3_canvas"
              width="900"
              height="900"
              viewBox="0 0 900 900"
            />
            </>
        )
    }
}