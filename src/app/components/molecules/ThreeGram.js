import React, { Component } from 'react';
import * as d3 from 'd3'

/**
 * Bloque básico para generar una visualización en D3
 * 
 * @version 0.1
 */

export default class ThreeGram extends Component {
  componentDidMount() {
    this.step = 400
    let parent = this
    this.svg = d3.select(this.refs.d3_canvas_2).append('g')
    this.svg.attr('style', `transform: translate(0,-${this.props.margin}px)`)
    const yScale = d3.scaleBand()
      .rangeRound([40, 6000])
    const radius = d3.scaleLinear().range([1, 5])
    const font_size = d3.scaleLinear().range([20, 25])
    const strokew = d3.scaleLinear().range([1, 10])
    const color = d3.scaleSequential(d3.interpolateRdYlBu)


    let data = this.props.buckets
    let level_uno = get_level_gram(0)
    let level_dos = get_level_gram(1)
    let level_tres = get_level_gram(2)
    let cone_uno = get_cone_gram(0)
    let cone_dos = get_cone_gram(1)

    const n1 = draw_level(this.svg, level_uno, 'nivel_uno', 100)
    const n2 = draw_level(this.svg, level_dos, 'nivel_dos', 500)
    const n3 = draw_level(this.svg, level_tres, 'nivel_tres', 900)
    const c1 = draw_cone(this.svg, cone_uno, level_uno, level_dos, 100)
    const c2 = draw_cone(this.svg, cone_dos, level_dos, level_tres, 500)
    const b1 = draw_btn(this.svg, level_uno, 'nivel_uno_btn', 100)
    const b2 = draw_btn(this.svg, level_dos, 'nivel_dos_btn', 500)
    const b3 = draw_btn(this.svg, level_tres, 'nivel_tres_btn', 900)

    n1.on('click', (d, i) => {
      let index = level_uno.find(t => t.name === d.name)
      level_uno = level_uno.filter(t => t.name !== d.name)
      level_uno = [...level_uno]
      level_uno.unshift(index)

      let compa = cone_uno.filter(t => t.origin === d.name)
      compa.forEach(e => {
        let index = level_dos.find(t => t.name === e.destiny)
        level_dos = level_dos.filter(t => t.name !== e.destiny)
        level_dos = [...level_dos]
        level_dos.unshift(index)
      })

      updateLevel(c1, n1, level_uno, level_dos, 100, b1)
      updateLevel(c2, n2, level_dos, level_tres, 500, b2)
      updateLevel(false, n3, level_tres, false, 900, b3)
    })

    n3.on('click', (d, i) => {
      let index = level_tres.find(t => t.name === d.name)
      level_tres = level_tres.filter(t => t.name !== d.name)
      level_tres = [...level_tres]
      level_tres.unshift(index)

      let compa = cone_dos.filter(t => t.destiny === d.name)
      compa.forEach(e => {
        let index = level_dos.find(t => t.name === e.origin)
        level_dos = level_dos.filter(t => t.name !== e.origin)
        level_dos = [...level_dos]
        level_dos.unshift(index)
      })

      updateLevel(c1, n1, level_uno, level_dos, 100, b1)
      updateLevel(c2, n2, level_dos, level_tres, 500, b2)
      updateLevel(false, n3, level_tres, false, 900, b3)
    })

    n2.on('click', (d, i) => {
      let index = level_dos.find(t => t.name === d.name)
      level_dos = level_dos.filter(t => t.name !== d.name)
      level_dos = [...level_dos]
      level_dos.unshift(index)

      let compa = cone_uno.filter(t => t.destiny === d.name)
      compa.forEach(e => {
        let index = level_uno.find(t => t.name === e.origin)
        level_uno = level_uno.filter(t => t.name !== e.origin)
        level_uno = [...level_uno]
        level_uno.unshift(index)
      })

      compa = cone_dos.filter(t => t.origin === d.name)
      compa.forEach(e => {
        let index = level_tres.find(t => t.name === e.destiny)
        level_tres = level_tres.filter(t => t.name !== e.destiny)
        level_tres = [...level_tres]
        level_tres.unshift(index)
      })

      updateLevel(c1, n1, level_uno, level_dos, 100, b1)
      updateLevel(c2, n2, level_dos, level_tres, 500, b2)
      updateLevel(false, n3, level_tres, false, 900, b3)
    })

    b1.on('click', (d, i) => {
      this.props.setFilter({
        nodo: d.name,
        gram: '3gram'
      }, 'grama')
    })

    b2.on('click', (d, i) => {
      this.props.setFilter({
        nodo: d.name,
        gram: '3gram'
      }, 'grama')
    })

    b3.on('click', (d, i) => {
      this.props.setFilter({
        nodo: d.name,
        gram: '3gram'
      }, 'grama')
    })

    bindOver([n1, n2, n3], data)

    function bindOver(array) {
      array.forEach((a, i) => {
        a.on('mouseover', y => {
          let cone = data.filter(t => t.key.split('-').includes(y.name) === true)
          let resp = []
          cone.forEach(c => {
            let i_ = c.key.split('-').indexOf(y.name)
            if (i_ > -1 && i === i_) {
              resp.push(c)
            }
          })
          console.log(resp)

          array.forEach((a_, i_) => {
            if (i !== i_) {
              a_.attr('fill', d => {
                let r = '#999'
                resp.forEach(_ => {
                  if (_.key.split('-')[i_] === d.name) r = 'rgb(225, 87, 89)'
                })
                return r
              })
            } else {
              a_.attr('fill', d => {
                let r = '#999'
                resp.forEach(_ => {
                  if (_.key.split('-')[i_] === d.name) r = 'rgb(225, 87, 89)'
                })
                return r
              })
            }
          })
        })
          .on('mouseleave', () => {
            array.forEach((a_, i_) => {
              a_.attr('fill', '#000')
            })
          })

      })
    }

    this.svg.on('click', () => {
      // sorting()
      // updateLevel(c1,n1,level_uno,level_dos,100,b1)
      // updateLevel(false,n2,level_dos,false,900,b2)
      // updateLevel(c3,n3,level_tres,level_cuatro,4,b340)
      // updateLevel(false,n4,level_cuatro,false,640)
    })

    function draw_cone(svg, cone, level__uno, level__dos, xPos) {
      // console.log(d3.max(cone,d=>d.value))
      strokew.domain([1, d3.max(cone, d => d.value)])
      color.domain([1, d3.max(cone, d => d.value)])
      const con = svg.selectAll('.cone_' + xPos)
        .data(cone)
        .join('path')
        .attr('stroke', d => {
          return "#999"
        })
        .attr('opacity', 0.4)
        .attr('fill', '#fff')
        .attr('stroke-width', d => {
          return strokew(d.value)
        })
        .attr('fill-opacity', 0)
        .attr("d", d => {
          let _ = level__uno.find(i => i.name === d.origin)
          let __ = level__dos.find(i => i.name === d.destiny)

          yScale.domain(level__uno.map(i => i.name))

          let t = {
            coor: [xPos, yScale(_.name)]
          }
          yScale.domain(level__dos.map(d => d.name))

          let l = {
            coor: [xPos + parent.step, yScale(__.name)]
          }
          return `
              M${t.coor[0] + 10},${t.coor[1]}
              C${l.coor[0] - 50},${t.coor[1]}
              ${l.coor[0] - 150},${l.coor[1]}
              ${l.coor[0] - 10},${l.coor[1]}
              `});

      return con
    }

    function updateLevel(con, nivel, level, level2, xPos, btn) {
      yScale.domain(level.map(d => d.name))
      nivel.transition().duration(600).attr('transform', (d, i) => {
        return `translate(${xPos},${yScale(d.name)})`
      })

      btn.transition().duration(600).attr('transform', (d, i) => {
        return `translate(${xPos},${yScale(d.name)})`
      })


      if (con) {
        con.transition().duration(600).attr('d',)
          .attr("d", d => {
            let _ = level.find(i => i.name === d.origin)
            let __ = level2.find(i => i.name === d.destiny)

            yScale.domain(level.map(i => i.name))

            let t = {
              coor: [xPos, yScale(_.name)]
            }
            yScale.domain(level2.map(d => d.name))

            let l = {
              coor: [xPos + parent.step, yScale(__.name)]
            }
            return `
              M${t.coor[0] + 10},${t.coor[1]}
              C${l.coor[0] - 50},${t.coor[1]}
              ${l.coor[0] - 150},${l.coor[1]}
              ${l.coor[0] - 10},${l.coor[1]}
              `});
      }


    }

    function draw_level(svg, level, classString, xPos) {
      yScale.domain(level.map(d => d.name))

      const nivel = svg.selectAll('.' + classString)
        .data(level)
        .join('g')
        .attr('class', classString)
        .attr('transform', (d, i) => {
          return `translate(${xPos},${yScale(d.name)})`
        })


      font_size.domain([1, d3.max(level, d => d.value)])
      radius.domain([1, d3.max(level, d => d.value)])

      nivel.append('text')
        .text(d => d.name)
        .attr('y', -10)
        .attr('font-size', d => {
          return font_size(d.value)
        })
        .attr('text-anchor', 'middle')

      // nivel.append('circle')
      // .attr('cx', 0)
      // .attr('cy', 0)
      // .attr('fill', '#333')
      // .attr('stroke', '#fff')
      // .attr('r', d=>radius(d.value))

      return nivel

    }

    function draw_btn(svg, level, classString, xPos) {
      yScale.domain(level.map(d => d.name))

      const nivel = svg.selectAll('.' + classString)
        .data(level)
        .join('g')
        .attr('class', classString)
        .attr('transform', (d, i) => {
          return `translate(${xPos},${yScale(d.name)})`
        })


      font_size.domain([1, d3.max(level, d => d.value)])
      radius.domain([1, d3.max(level, d => d.value)])

      // nivel.append('text')
      // .text(d=>d.name)
      // .attr('y', -13)
      // .attr('font-size', d=>{
      //   return font_size(d.value)
      // })
      // .attr('text-anchor', 'middle')

      nivel.append('circle')
        .attr('cx', 0)
        .attr('cy', 0)
        .attr('fill', '#333')
        .attr('stroke', '#fff')
        .attr('r', 10)
      // .attr('r', d=>radius(d.value))

      return nivel

    }

    function get_cone_gram(level) {
      let resp = []
      data.forEach(item => {
        let splited = item['key'].split('-')
        let _item = resp.find(d => d.origin === splited[level] && d.destiny === splited[level + 1])
        if (_item === undefined) {
          let obj = {
            origin: splited[level],
            destiny: splited[level + 1],
            level: level,
            value: item.doc_count
          }
          resp.push(obj)
        } else {
          _item.value++
        }
      })
      return resp
    }

    function get_level_gram(level) {
      let resp = []
      data.forEach(item => {
        let splited = item['key'].split('-')
        let _item = resp.find(d => d.name === splited[level])
        if (_item === undefined) {
          let obj = {
            "name": splited[level],
            "value": 1
          }
          resp.push(obj)
        }
        else {
          _item.value++
        }
        // console.log(_item)
      })
      return resp
    }
  }

  componentDidUpdate() {
    this.svg.attr('style', `transform: translate(0,-${this.props.margin}px)`)
  }

  render() {
    return (
      <>
        <svg
          ref="d3_canvas_2"
          viewBox="0 0 1000 7000"
        />
      </>
    )
  }
}