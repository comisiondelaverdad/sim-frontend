import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { updateListSelected } from "../../services/selectListObserver";
import { Form } from "react-bootstrap";
import $ from 'jquery';
import { isArray } from "lodash";
import * as select2 from 'select2';
import { updateMultiselect, multiselect$ } from '../../services/selectListObserver';
export class RecursiveMultiSelect extends Component {

    constructor(props) {
        super(props);
        this.state = {
            item: props.item,
            level: props.level,
            field: props.field,
            fieldChild: null,
            data: props.data,
            formData: null,
            itemChild: props.itemChild,
        };
        this.selectList = this.selectList.bind(this);
    }

    selectList(e, list) {
        e.stopPropagation();
        updateListSelected(list);
    }

    componentDidUpdate(prevProps) {
        if (this.props.item !== prevProps.item) {
            this.setState({ item: this.props.item });
            this.mountField();
        }
        if (this.props.data !== prevProps.data) {
            console.log("Actualizacion " + this.state.level, this.props.item);
            this.updateField(this.state.field);
        }

    }
    updateField(field) {
        let form = {
            ...this.state.formData, ...{ [field.id]: ($('#' + field.id).select2('data')) }
        }
        form[field.id] = form[field.id].map((selected) => (selected.id))
        const options = this.state.item;
        let newSublist = isArray(options) ? options
            .filter((data) => (form[[field.id]].includes(data.value)))
            .map((data) => {
                return data.lists ? data.lists.options ? data.lists.options : [] : [];
            }) : [];
        console.log("Nueva lista", newSublist);
        newSublist = isArray(newSublist) && newSublist.length >= 2 && JSON.stringify(newSublist) !== '[]' ?
            newSublist.reduce((prev, current) => ([...prev, ...current])) : newSublist[0];
        this.setState({
            formData: form,
            itemChild: isArray(newSublist) ? newSublist : null,
            fieldChild: newSublist !== undefined ? {
                ...this.state.field,
                ...{ id: this.state.field.id + "_level_" + this.state.level }
            } : null,
        });
        console.log(this.state.level, {
            formData: form,
            itemChild: isArray(newSublist) ? newSublist : null,
            fieldChild: newSublist !== undefined ? {
                ...this.state.field,
                ...{ id: this.state.field.id + "_level_" + this.state.level }
            } : null,
        })
    }

    mountField() {
        let form = {};
        const field = this.state.field;
        $('#' + field.id).select2({
            placeholder: field.placeholder
        });
        $('#' + field.id).on('change', (e) => {
            e.stopPropagation();
            this.updateField(field);
            updateMultiselect({ [field.id]: ($('#' + field.id).select2('data')).map((selected) => (selected.id)) })
        });
    }

    componentDidMount() {
        this.mountField();
    }

    render() {
        const { item, itemChild, level, field, data, fieldChild } = this.state;
        return (
            <>
                {isArray(item) && JSON.stringify(item) !== '[]' && (
                    <div className={field.col ? `form-group col-md-${field.col}` : 'form-group col-md-12'}>
                        <>
                            <label className="form-label">{level == 1 ? field.label : ''}{level == 1 ? field.required ? ' *' : '' : ''}</label>
                            <select
                                multiple="multiple"
                                title={field.placeholder}
                                className="required form-control"
                                name={field.id}
                                id={field.id}
                                required={level == 1 ? field.required ? field.required : false : false}
                            >
                                {item.map((o, key2) => (<option key={key2} value={o.value}
                                    selected={isArray(data) && data.length >= 2 && JSON.stringify(data) !== '[]' ?
                                        data.reduce((a, b) => (a + b)).includes(o.value) ? true : false : false}>{o.name}</option>))}
                            </select>
                            <Form.Text className="text-muted">
                                {field.info}
                            </Form.Text>
                        </>
                        {itemChild !== null && itemChild !== undefined && (
                            <RecursiveMultiSelect
                                field={fieldChild}
                                item={itemChild}
                                data={data}
                                level={level + 1}>
                            </RecursiveMultiSelect>
                        )}
                    </div>
                )}
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(RecursiveMultiSelect);
