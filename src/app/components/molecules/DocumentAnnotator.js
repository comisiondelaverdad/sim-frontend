import React, {Component} from 'react';
import PropTypes from 'prop-types';
import striptags from "striptags";

class DocumentAnnotator extends Component {

  static propTypes = {
    annotations: PropTypes.array.isRequired,
    documentText: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    console.debug('DocumentAnnotator props', props);
    this.state = {
      annotations: props.annotations,
      documentText: this.props.documentText
    };
  }

  componentWillReceiveProps(nextProps) {
    console.debug('DocumentAnnotator componentWillReceiveProps', nextProps);
    if (nextProps.length !== this.state.annotations || nextProps.annotations.some(a => !this.state.annotations.includes(a))) {
      this.setState({
        annotations: nextProps.annotations
      });
    }
  }

  componentWillUnmount() {
    console.debug('DocumentAnnotator componentWillUnmount');
  }

  highlightAnnotationsDoc(doc, annotations, offset) {
    let highlighted = '';
    const tempAnnotations = [];
    const tempPosNAnnotationMap = {};
    annotations.sort(function (aa, bb) {
      if (aa.start === bb.start) {
        return bb.end - aa.end;
      }
      return bb.start - aa.start;
    });
    for (let jIndex = 0; jIndex < annotations.length; jIndex++) {
      if (annotations[jIndex].color.length > 0) {
        tempAnnotations.push(annotations[jIndex]);
        const start = annotations[jIndex].start;
        const end = annotations[jIndex].end;
        const id = annotations[jIndex].id;
        const color = annotations[jIndex].color;
        for (let kIndex = start; kIndex <= end; kIndex++) {
          if (kIndex in tempPosNAnnotationMap) {
            const currentValue = tempPosNAnnotationMap[kIndex];
            currentValue.push({id, color});
            tempPosNAnnotationMap[kIndex] = currentValue;
          } else {
            tempPosNAnnotationMap[kIndex] = [{id, color}];
          }
        }
      }
    }
    tempAnnotations.sort(function (aa, bb) {
      if (aa.start === bb.start) {
        return bb.end - aa.end;
      }
      return aa.start - bb.start;
    });
    let spanCount = 0;
    let lastDiv = '';
    for (let cursor = 0; cursor < doc.length; cursor++) {
      let endIsZero = 0;
      if (cursor > 0) {
        if (doc[cursor] === '\n' && spanCount > 0) {
          // console.debug('continuing last div');
          highlighted += '</span>' + doc[cursor] + lastDiv;
        } else {
          highlighted += doc[cursor];
        }
      } else {
        // console.debug('cursor mismatch', cursor);
      }
      const removeIds = [];
      for (let annotationIdx = 0; annotationIdx < tempAnnotations.length; annotationIdx++) {
        let annotation = tempAnnotations[annotationIdx];
        let start = annotation.start;
        const end = annotation.end;
        const posNAnnotation = tempPosNAnnotationMap[start];
        const nextAnnotation = tempPosNAnnotationMap[end + 1];
        start = (start !== 0) ? start - offset : start;
        if (cursor === start && posNAnnotation && posNAnnotation.length > 0 && posNAnnotation[0].color.length > 0) {
          lastDiv = `<span data-id="${annotation.id}" class="annotated-span" style="display:inline; background-color:${(annotation.color[0])}; color: ${annotation.color[0] === 'white' ? 'inherit' : 'white'};" title="${Array.isArray(annotation.category) ? annotation.category.join(' ') : annotation.category}" >`;
          highlighted += lastDiv;
          spanCount = spanCount + 1;
          if (start === end && end === 0) {
            endIsZero = endIsZero + 1;
          }
        }
        if (cursor === end && annotation.color.length > 0 && end !== 0) {
          // console.debug('going for annotation ', index, cursor, posNAnnotation, nextAnnotation);
          for (let jindex = 0; jindex < spanCount; jindex++) {
            highlighted += '</span>';
          }
          /* remove the annotation from the array after it's not needed.
          ** This help with performance for subsequent runs through the loop
          */
          removeIds.push(annotation.id);
          spanCount = spanCount - 1;
          for (let jIndex = 0; jIndex < spanCount; jIndex++) {
            if (nextAnnotation && nextAnnotation.length > 0 && nextAnnotation[0].color.length > 0) {
              lastDiv = `<span data-id="${nextAnnotation[0].id}" class="annotated-span" style="display:inline; background-color:${(nextAnnotation[0].color)[0]}; color: ${annotation.color[0] === 'white' ? 'inherit' : 'white'};" title="${Array.isArray(annotation.category) ? annotation.category.join(' ') : annotation.category}" >`;
              highlighted += lastDiv;
            }
          }
        }
      }

      for (let kIndex = 0; kIndex < removeIds.length; kIndex++) {
        for (let yIndex = 0; yIndex < tempAnnotations.length; yIndex++) {
          if (removeIds[kIndex] === tempAnnotations[yIndex].id) {
            tempAnnotations.splice(yIndex, 1);
          }
        }
      }
      if (cursor === 0) {
        highlighted += doc[cursor];
        for (let kIndex = 0; kIndex < endIsZero; kIndex++) {
          highlighted += '</span>';
          spanCount = spanCount - 1;
        }
        endIsZero = 0;
      }
    }

    return highlighted;
  }

  limpiar(annotated){
    let tx = striptags(annotated,['i','span']);
    tx = tx.replace(/\xA0/g,'');
    tx = tx.replace(/ENT+[\s0-9:]*/g,"<br /><br /><span class='badge badge-primary'>ENT: </span>");
    tx = tx.replace(/TEST+[\s0-9:]*/g,"<br /><br /><span class='badge badge-success'>TEST: </span>");
    return tx.trim();
  }

  render() {

    let annotated = this.highlightAnnotationsDoc(this.state.documentText, this.state.annotations, 1);

    return (
      <div style={{minHeight: '300px'}}>
        <div>
          <p
            id="annotationDoc"
            className="document text-justify"
            style={{lineHeight: '2.0rem'}}
            dangerouslySetInnerHTML={{__html: this.limpiar(annotated)}}
          >
          </p>
        </div>
      </div>
    );
  }
}

export default DocumentAnnotator;
