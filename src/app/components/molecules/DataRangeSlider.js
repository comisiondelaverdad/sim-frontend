import React, { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import TimeBar from "../molecules/TimeBar"
import _ from "lodash";
import * as VizService from "../../services/VizService";


const styles = theme => ({
  root: {
    width: 550,
    background: "#fffe",
  },
});

class DataRangeSlider extends Component {

  constructor(props) {
    super(props);

    this.state = {
      value: [0, 100],
      valuetext: "",
      min: 0,
      max: 100,
      marks: []
    }

    this.valueLabelFormat = this.valueLabelFormat.bind(this);
  }

  valueLabelFormat(value) {
    return this.state.marks[value].label;
  }

  handleChange(event, newValue) {
    this.setState({"value": newValue});
  };

  normalizeData(data){
    let n = data.length;
    let x = 100/n;
    data = data.sort((a,b) =>{
      return a.value - b.value;
    });

    let newData = data.map((d,k)=>{
      if(k === 0){
        return {"name": d.name, "value": 3*x, "data": d.value}
      }
      else{
        return {"name": d.name, "value": 3*k*x, "data": d.value}
      }
    });
    
    return newData.sort((a,b) =>{
      return a.name - b.name;
    });
  }

  componentDidMount(){
    VizService.getHistogramDates({}).then(
      (data) => {
        console.log("Datos visualización")
        let marks = [];
        let i = 0;
        let graph = _.map(data.aggregations.resource_by_month.buckets , (a, k)=>{
          if(a.doc_count !== 0){
            marks.push({value: i, label: (new Date(a.key)).getUTCFullYear()})
            i++;
          }
          return {"name":(new Date(a.key)).getUTCFullYear(),"value":a.doc_count}
        }).filter(a=> a.value !== 0);
        if (graph.length>0){
          this.setState({
            "histogramData": this.normalizeData(graph),
            //"min": graph[0].name,
            "max": graph.length - 1,
            "value": [0, graph.length - 1],
            "marks": marks
          })
        }
      },
      (error) => {
        console.log("Error en la consulta getHistogramDates")
        console.log(error)
      }
    )
  }
  
  addSearch(){
    let s = new Date(this.state.marks[this.state.value[0]].label+"-01-01")
    let e = new Date(this.state.marks[this.state.value[1]].label+"-12-31")
    this.props.addQuery("temporal.inicio: ["+s.toISOString().substring(0,10)+" TO "+e.toISOString().substring(0,10)+"]")
  }

  addSearchWithout(){
    this.props.addQuery("!_exists_:temporal.inicio")
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {this.state.histogramData &&
          <>
            <Typography id="range-slider" gutterBottom>
              Cobertura temporal
            </Typography>
            <TimeBar
              data={this.state.histogramData}
            />
            <Slider
              value={this.state.value}
              onChange={(e, value)=>{this.handleChange(e, value)}}
              aria-labelledby="range-slider"
              valueLabelDisplay="on"
              min={this.state.min}
              max={this.state.max}
              getAriaValueText={this.valueLabelFormat}
              valueLabelFormat={this.valueLabelFormat}
              marks={this.state.marks}
            />
            <button type="button" className="btn btn-success btn-sm" onClick={()=>{this.addSearch()}}>
              <i className="flaticon2-plus"> Agregar rango fechas</i>
            </button>
            <button type="button" className="btn btn-warning btn-sm" onClick={()=>{this.addSearchWithout()}}>
              <i className="flaticon2-plus">Sin Cobertura</i>
            </button>
          </>
        }
      </div>
    )
    //          step={2628000000}
  }
}


export default withStyles(styles)(DataRangeSlider);
