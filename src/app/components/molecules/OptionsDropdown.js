import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown'
import { METADA_MAP } from "../../config/const";

function OptionsDropdown ({campos, campoSelected, selected}) {  

    let dropdownItems = [];
    for (var i = 0; i < campos.length; i++) {
      let campo = campos[i];
      if(campo === "offFlag"){
        continue;
      }
      dropdownItems.push(
      <Dropdown.Item 
        key={i + campo} 
        onSelect={() => campoSelected(campo)
        }
        >
          {METADA_MAP[campo] ? METADA_MAP[campo] : campo}
      </Dropdown.Item>
      );
    }

    return (
      <Dropdown>
        <Dropdown.Toggle id="dropdown-basic">
              {selected === "Todos" ? "Selecciona un campo" : METADA_MAP[selected]}
        </Dropdown.Toggle>

        <Dropdown.Menu className="d-options">
          <Dropdown.Item key="Todos" onSelect={() => campoSelected("Todos")}> 
            Todos
          </Dropdown.Item>
          {dropdownItems}
        </Dropdown.Menu>
      </Dropdown>
    );
}

export default OptionsDropdown;

