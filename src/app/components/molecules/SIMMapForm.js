import React, { Component } from 'react';
import { Map, TileLayer, FeatureGroup } from 'react-leaflet';
import { EditControl } from 'react-leaflet-draw';
import L from 'leaflet';
import _ from "lodash";
import Select from 'react-select';
import * as LocationsService from "../../services/LocationsService";
import { Container, Alert } from 'react-bootstrap';
import { connect } from "react-redux";

class SIMMapForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      z: this.props.zoom ? this.props.zoom : 5,
      c: this.props.center ? this.props.center : [4.36, -74.04],
      anteriorH: -1,
      anteriorW: -1,
      minHeigth: this.props.minHeigth ? this.props.minHeigth : 350,
      levels: [
        { label: "Macroregión", value: 3 },
        { label: "Departamento", value: 1 },
        { label: "Municipio", value: 2 },
      ],
      level: undefined,
      selected: undefined,
      locationsValue: undefined
    }
  }

  update() {
    setTimeout(() => {
      let ref = this.map
      if (ref != null) {
        let h = ref.clientHeight
        if (h < this.state.minHeigth)
          h = this.state.minHeigth
        if (this.state.anteriorH === -1 || this.state.anteriorH === 0 || this.state.anteriorW === -1 || this.state.anteriorW === 0) {
          this.setState(
            { height: h, width: ref.clientWidth, anteriorH: ref.clientHeight, anteriorW: ref.clientWidth },
            () => { this.updateLocations() }
          )
        } else {
        }
      }
    }, 1)
  }

  componentDidUpdate(nextProps, nextState) {
    let ref = this.map
    if (
      ref.clientHeight > 0 &&
      ref.clientWidth > 0 &&
      (this.state.height === 0 || this.state.width === 0)
    ) {
      setTimeout(() => {
        this.setState(
          {
            height: ref.clientHeight,
            width: ref.clientWidth,
            anteriorH: ref.clientHeight,
            anteriorW: ref.clientWidth
          })
      }, 500)
    }
  }

  componentDidMount() {
    this.update()
  }

  updateLocations() {
    LocationsService.list("", 1).then((data) => {
      const dataLocations = data.map((l) => {
        if (l.admin_level === 1 || l.admin_level === 2) {
          return {
            ...{
              _id: l._id,
              admin_level: l.admin_level,
              dpto: l.properties.DPTO,
              nombre_dpt: l.properties.NOMBRE_DPT,
              mpio: l.properties.MPIO ? l.properties.MPIO : "",
              nombre_mpi: l.properties.NOMBRE_MPI ? l.properties.NOMBRE_MPI : "",
              nombre_cab: l.properties.NOMBRE_CAB ? l.properties.NOMBRE_CAB : "",
              clasemun: l.properties.CLASEMUN ? l.properties.CLASEMUN : "",
              mpios: l.properties.MPIOS ? l.properties.MPIOS : "",
              zona: l.properties.ZONA ? l.properties.ZONA : "",
              of_reg: l.properties.OF_REG ? l.properties.OF_REG : "",
              reg_zonas: l.properties.REG_ZONAS ? l.properties.REG_ZONAS : "",
              area: l.properties.AREA,
              perimeter: l.properties.PERIMETER,
              hectares: l.properties.HECTARES,
              poligon: l.geometry,
              pais: l.pais,
              localidad: l.localidad,
              value: l._id,
              label: l.properties.NOMBRE_DPT + (l.properties.NOMBRE_MPI ? " - " + l.properties.NOMBRE_MPI : "")
            },
          }
        }
        if (l.admin_level === 3) {
          return {
            ...{
              _id: l._id,
              admin_level: l.admin_level,
              nombre_dpt: l.properties.NOMBRE_MR,
              area: l.properties.AREA,
              poligon: l.geometry,
              value: l._id,
              label: l.properties.NOMBRE_MR
            },
          }
        }
        return l
      });
      this.setState({ locations: dataLocations })
    })
  }

  _onEdited = (e) => {
    this._onChange();
  }

  _onCreated = (e) => {
    this._onChange();
  }

  _onDeleted = (e) => {
    this._onChange();
  }

  _editableFG = null

  _onFeatureGroupReady = (reactFGref) => {

    if (reactFGref) {
      // populate the leaflet FeatureGroup with the geoJson layers
      let leafletGeoJSON = new L.GeoJSON(getGeoJson());
      let leafletFG = reactFGref.leafletElement;

      leafletGeoJSON.eachLayer((layer) => {
        leafletFG.addLayer(layer);
      });
      // store the ref for future access to content
      this._editableFG = reactFGref;
    }
  }

  addNew(name, poligon, mapId) {
    let reactFGref = this._editableFG
    if (reactFGref) {
      // populate the leaflet FeatureGroup with the geoJson layers
      let g = getGeoJson()
      g.features.push(poligon)
      let leafletGeoJSON = new L.GeoJSON(g);
      let leafletFG = reactFGref.leafletElement;

      leafletGeoJSON.eachLayer((layer) => {
        leafletFG.addLayer(layer);
      });
      const geojsonData = reactFGref.leafletElement.toGeoJSON();
      this._editableFG = reactFGref;
      this.props.onChange(name, geojsonData, mapId);
      // store the ref for future access to content
    }
  }

  _onChange = () => {
    // this._editableFG contains the edited geometry, which can be manipulated through the leaflet API
    const { onChange } = this.props;
    if (!this._editableFG || !onChange) {
      return;
    }
    const geojsonData = this._editableFG.leafletElement.toGeoJSON();
    this.setState({ selected: false }, () => {
      this.props.onChange("Polígono", geojsonData);

    })
  }

  clear() {
    const drawnItems = this._editableFG.leafletElement._layers;
    Object.keys(drawnItems).forEach((layerid, index) => {
      const layer = drawnItems[layerid];
      this._editableFG.leafletElement.removeLayer(layer);
    });
    this.props.clear();
  }


  unselect() {
    this.setState({ selected: undefined, levelsValue: -1, locationsValue: -1, level: undefined }, () => { this.clear() })
  }

  select() {
    this.props.onAdd(this.state)
    this.setState({ selected: true })
  }

  locationChange(selectedOption) {
    let id = selectedOption.value
    this.setState({ locationsValue: selectedOption, loading: true }, () => {
      this.clear()
      LocationsService.get(id).then((data) => {
        if (data.admin_level === 1) {
          this.addNew(data.properties.NOMBRE_DPT, data.geometry, data._id)
        }
        if (data.admin_level === 2) {
          this.addNew(data.properties.NOMBRE_DPT + " " + data.properties.NOMBRE_MPI, data.geometry, data._id)
        }
        if (data.admin_level === 3) {
          this.addNew(data.properties.NOMBRE_MR, data.geometry, data._id)
        }
        this.setState({ selected: false, loading: false })
      })
    })
  }

  levelChange(selectedOption) {
    this.setState({ level: selectedOption, locationsValue: null, selected: undefined })
  }

  render() {
    const {
      level,
      locationsValue
    } = this.state;

    const nLevel = this.state.level ? this.state.level.value : this.state.level
    const nLocations = _.orderBy(_.filter(this.state.locations, { "admin_level": nLevel }), ["nombre_dpt", "nombre_mpi"])

    return (
      <div className="map">
        <div className="space"></div>
        <div ref={el => this.map = el} className="mw-100">
          {
            this.state.height &&
            <Map style={{ width: this.state.width, height: this.state.height }} center={this.state.c} zoom={this.state.z} scrollWheelZoom={false}>
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
              />
              <FeatureGroup ref={(reactFGref) => { this._onFeatureGroupReady(reactFGref); }}>
                {(this.state.selected === undefined && !this.state.selected) &&
                  <EditControl
                    position='topright'
                    onEdited={this._onEdited}
                    onCreated={(e) => { this._onCreated(e) }}
                    onDeleted={(e) => { this._onDeleted(e) }}
                    draw={{
                      polyline: false,
                      polygon: true,
                      rectangle: false,
                      circle: false,
                      circlemarker: false,
                      marker: false,
                    }}
                  />
                }
              </FeatureGroup>
            </Map>
          }

          {!this.state.selected &&
            <>
              <Select
                value={this.state.level}
                onChange={(selected) => { this.levelChange(selected) }}
                options={this.state.levels}
                placeholder={'Seleccione nivel'}
              />
              {this.state.level &&
                <Select
                  value={this.state.locationsValue}
                  onChange={(selected) => this.locationChange(selected)}
                  options={nLocations}
                  placeholder={'Seleccione el polígono'}
                />
              }
            </>
          }
          {this.state.loading &&
            <>
              <Container fluid={true} className="my-5">
                <Alert variant="success">
                  <div className="alert-icon">
                    <i className="fas fa-sync fa-spin" />
                  </div>
                  <div className="alert-text">Cargando...</div>
                </Alert>
              </Container>
            </>
          }
          {this.state.selected !== undefined && !this.state.selected && this.state.locationsValue !== -1 && !this.state.loading &&
            <div>
              <button type="button" onClick={() => this.select()} className="btn btn-success">Seleccionar</button>
            </div>
          }
          {this.state.selected &&
            <>
              <div>
                <button type="button" onClick={() => this.unselect()} className="btn btn-danger">Borrar</button>
              </div>
              <div className="">
                <i className="flaticon2-map"></i>
                Mapa seleccionado: {level.label + " - " + locationsValue.label}
              </div>
            </>
          }
        </div>
      </div>
    )
  }
}


function getGeoJson() {
  return {
    "type": "FeatureCollection",
    "features": [
    ]
  }
}

const mapStateToProps = store => ({
  searchFilters: store.app.filters
});

export default connect(
  mapStateToProps
)(SIMMapForm);
