import React, { useState, useEffect } from "react";
import ReactExport from "react-export-excel";
import Swal from "sweetalert2";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function DownloadResult(props) {

  const deleteKey = (key, object) => {
    if (object.hasOwnProperty(key)) delete object[key];
    return object;
  };

  const idsFields = (fieldsForm) => {
    if (fieldsForm) {
      var ids = fieldsForm.map((field) => {
        return field.origin;
      });
      ids.push('history');
      ids.push('authorizationRights');
      ids.push('status');
      return ids;

    } else {
      return [];
    }

  };

  const convert = (data, name) => {   
    
    if (name == "temporalCoverage" && data) {
      data = deleteKey("description", data);
    }
    
    if ((name == "geographicCoverage" || name == "editionPlace") && data) {
      let result = data.map((place) => { return place ? place.name ? place.name : '' : '' });
      return result.join("|");
    }
    
    if (name == "history" && data) {      
      if(data[0].includes('')) 
        return "No registra";      
      data = data[0].join("|");
      return data;
    }
    if (Array.isArray(data)) {
      data = data.join("|");
      return data;
    }
    if (typeof data === "object" && data !== null) {
      data = Object.values(data);
      if (name == 'temporalCoverage' && data[0] !== null && data[1] !== null &&
        (data[0].includes('NaN') || data[1].includes('NaN'))) {
        data[0] = '';
        data[1] = '';
      }
      if (data[0] === null && data[1] === null) {
        data[0] = '';
        data[1] = '';
      }
      return data.join("") === "" ? "" : data.join("|");
    }

    return data;
  };

  const dataToDownload = (fields) => {
    let arrayResources = [];

    let dstemp = [];
    if (props.dataset.length > 15000) {
      dstemp = props.dataset.slice(0, 10000);
    } else {
      dstemp = props.dataset.slice();
    }
    
    dstemp.forEach(resource => {
      var resourceInSchema = {};
      fields.forEach(element => {
        if (resource[element]) {
          resourceInSchema[element] = convert(resource[element], element);
        } else {
          resourceInSchema[element] = "";
        }
      });
      arrayResources.push(resourceInSchema);
    });

    return arrayResources;
  };

  var campos = idsFields(props.statment);
  var dataset = dataToDownload(campos);
  var matrizExcel = [];
  campos.forEach((element, index) => { matrizExcel.push(<ExcelColumn key={index} label={element} value={element} />); });
  Swal.close();

  return (
    <div>
      {dataset && props.frmType && (
        <ExcelFile
          hideElement={true}
          filename={new Date().toLocaleString() + "_" + props.frmType + ""}
        >
          <ExcelSheet data={dataset} name="hoja">
            {matrizExcel}
          </ExcelSheet>
        </ExcelFile>
      )}
    </div>
  );
}