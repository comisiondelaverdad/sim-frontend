import React from "react";
import { connect } from "react-redux";
import { Dropdown } from "react-bootstrap";
import DropDownIcon from "../molecules/DropDownIcon";
import SIMMapForm from "../molecules/SIMMapForm";

import * as FormService from "../../services/FormService";

import _ from "lodash";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { toAbsoluteUrl } from "../../../theme/utils";
import PlaceIcon from '@mui/icons-material/Place';

class FormAdvancedSearchMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newLabel: '',
      show: false,
      alert: false,
      form: undefined,
      isLoading: false,
    };

    this.validate = this.validate.bind(this);
  }
  componentDidMount() {
    this.setState({ oldLabel: this.props.label, newLabel: this.props.label, show: false });
  }

  validate(evt) {
    evt.preventDefault()
  }

  selectForm(event) {
    let f = this.props.config.forms[event.target.value]
    if (f !== undefined) {
      if (f.type === 'form-select') {
        FormService.getOptions(f.field)
          .then(json => {
            if (json.aggregations.options.buckets.length > 0) {
              f.schema.properties[f.name].enum = _.map(json.aggregations.options.buckets, 'key')
            } else {
              f.schema.properties[f.name].enum = ["Sin Registros"]
            }
            f.schema.properties[f.name].enum = f.schema.properties[f.name].enum.sort()
            this.setState({ form: f })
          });
      } else {
        this.setState({ form: f })
      }
    } else {
      this.setState({ form: null })
    }

  }

  change(e) {
  }

  onSubmit(formData, e) {
    e.preventDefault()
    let query = this.state.form.queryAlias ? this.state.form.queryAlias : this.state.form.query
    if (typeof formData === "object") {
      for (const prop in formData) {
        console.log(prop, formData[prop])
        if (formData[prop] !== "Sin Registros") {
          query = query.split("[" + prop + "]").join(formData[prop]);
        }
      }
    } else {
      query = formData
    }

    if (query !== this.state.form.query) {
      this.setState({ "query": query, "formData": formData, show: false, form: undefined }, this.props.addQuery(query))
    } else {
      console.log("No se puede agregar registro")
    }

  }

  asyncSearch(query) {
    this.setState({ isLoading: true });
    FormService.getAsyncOptions(this.state.form, query)
      .then(json => {
        this.setState({
          isLoading: false,
          options: json.items,
        })
      });
  }

  mapSelect(name, geojsonData, mapId) {
    this.setState({ "mapData": geojsonData, "nameMap": name, "mapId": mapId })
  }

  mapAdd() {
    if (this.state.nameMap === "Polígono") {
      let q = ""
      let comma = ""
      _.map(this.state.mapData.features, (v, k) => {
        q += comma + v.geometry.type[0] + "_"
        let comma2 = ""
        _.map(v.geometry.coordinates, (w, i) => {
          q += comma2 + w
          comma2 = "-"
        })
        comma = "|"
      })
      this.setState({ show: false, form: undefined }, this.props.addMap(this.state.mapData, q, this.state.nameMap))
    } else {
      this.setState({ show: false, form: undefined }, this.props.addMap(this.state.mapData, "M_" + this.state.mapId, this.state.nameMap))
    }
  }


  render() {
    const {
      show,
    } = this.state;

    let classDropdown = this.props.showSearchBar ? "" : "cev-advancedform";

    return (
      <Dropdown
        drop="down"
        show={show}
        onToggle={() => { this.setState({ show: false }) }}
      >
        <Dropdown.Toggle
          as={DropDownIcon}
          onClick={() => { this.setState({ show: true }) }}
        >
          <button type="button" className="btn-small ml-2 btn btn-primary" title="Busqueda por cobertura geográfica">
            <PlaceIcon></PlaceIcon>
          </button>
        </Dropdown.Toggle>
        <Dropdown.Menu flip={"false"} className={`dropdown-menu-fit dropdown-menu-left dropdown-menu-anim dropdown-menu-xl ${classDropdown} advanceSearch`}>
          <div className="cev-portlet">
            <h3>Búsqueda por cobertura geográfica</h3>
            <div className="cev-portlet__body">
              <SIMMapForm
                addQuery={this.props.addQuery}
                onChange={(n, f, l) => { this.mapSelect(n, f, l) }}
                onAdd={(x) => { this.mapAdd(x) }}
                clear={() => { this.props.addMap(undefined, undefined, undefined) }}
              />
            </div>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    )
  }
}

const mapStateToProps = store => ({
  user: store.auth.user._id,
  showSearchBar: store.app.showSearch
});

export default connect(
  mapStateToProps
)(FormAdvancedSearchMap);
