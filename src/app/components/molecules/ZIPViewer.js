import React, { Component } from "react";
import {connect} from "react-redux";
import ReactWaterMark from 'react-watermark-component';
import * as RecordsService from "../../services/RecordsService";
import PerfectScrollbar from "react-perfect-scrollbar";
import TreeView from "./TreeView";
import Records from "../molecules/Records";

class ZIPViewer extends Component {
  constructor () {
    super()

    this.state = {
      tree: {},
      ver: false,
      file: '',
      loading: false,
      id: '',
      name: ''
    }
  }

  componentDidMount() {
    this.convertBlobToJSON();
  }

  handleChange(e, data) {
    e.preventDefault();
    if (data.node) {
      let original = data.node.original;
      if(!original.isDirectory){
        RecordsService.serviceZipFile({id:original._id, name:original.fileName}).then((data) => {
          this.setState({ver: true, file: data, loading: false, id: original._id, name: original.fileName});
        }).catch((err) => {
          console.log(err);
          this.setState({ver: false, file: '', loading: false});
        });
      }
    }
  }

  convertBlobToJSON() {
    let reader = new FileReader();
    reader.onload = (evt) => {
      let tree = {
        "core": {
          "data": [
            {
              "text": this.props.fileName,
              "state": { "opened": true },
              "children": JSON.parse(evt.target.result)
            }
          ]
        }
      }
      this.setState({ tree: tree });
    }
    reader.readAsText(this.props.file);
  }

  render() {
    const {
      name
    } = this.props.user;

    const options = {
      chunkWidth: 450,
      chunkHeight: 90,
      textAlign: 'left',
      textBaseline: 'bottom',
      globalAlpha: 0.2,
      font: '36px Microsoft Yahei',
      rotateAngle: -0.1,
      fillStyle: '#666'
    }
    
    return (
      <>
      {!this.state.ver &&  
          <ReactWaterMark
            waterMarkText={name}
            options={options}
          >
            <PerfectScrollbar
              className="cev-scroll"
              options={{
                wheelSpeed: 1,
                wheelPropagation: false
              }}
              style={{ height: "calc(100vh - 240px)"}}
              data-scroll="true"
            >
              <div className="cev-portlet__body cev-resource-group">
                <div className="cev-widget cev-widget--user-profile-1">
                  <div className="cev-widget__body">
                    <div className="cev-widget__content">
                      <TreeView
                        treeData={this.state.tree}
                        onChange={(e, data) => this.handleChange(e, data)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </PerfectScrollbar>
          </ReactWaterMark>
        }
        {this.state.ver && 
          <>
            <div className="float-left">  
              <button href="#" className="btn btn-brand btn-icon-md" onClick={(e)=>{e.preventDefault(); this.setState({ver: false, file: '', loading: false});}}>
                <i className="la la-caret-left"/>
                <span>Volver</span>
              </button>  
            </div>
            <Records from="zip" record={{file:this.state.file,filename:this.state.name,ident:this.state.id}}/>
          </>
        }
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps
)(ZIPViewer);