import React, { Component } from 'react';
import { toAbsoluteUrl } from "../../../theme/utils";
import * as d3 from 'd3';
import * as difflib from "difflib";

export default class Choropleth extends Component {

  componentDidMount() {
    this.svg = d3.select(this.refs.d3_canvas);
    this.updateViz()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data.length !== this.props.data.length) {
      this.updateViz();
    }
  }

  updateViz() {
    const data = this.props.data.filter(x => x.name !== null);
    const noData = this.props.data.filter(x => x.name === null);
    const level = this.props.level;
    //const total = this.props.total - (noData && noData.length > 0 ? noData[0].frequency : 0);
    const maxFrequency = data[0].frequency > (noData && noData.length > 0 ? noData[0].frequency : 0) ? data[0].frequency : noData && noData.length > 0 ? noData[0].frequency : 0;
    const localization = level === 1 ? "DEPARTAMENTOS" : "MUNICIPIOS";
    const sumary = this.props.sumary;
    const compute_string_diffence = this.compute_string_diffence;
    const g = this.svg.append("g");
    const width = this.svg.attr("width")
    const height = this.svg.attr("height")

    const colorScale = d3.scaleThreshold()
      .domain([0, maxFrequency / 5, maxFrequency / 4, maxFrequency / 3, maxFrequency / 2, maxFrequency])
      .range(d3.schemeOranges[7]);

    const projection = d3.geoMercator()
      .scale(1250)
      .center([-68, 4])
      .translate([width / 1.3, height / 2]);

    const toolTip = d3.select("body").append("div")
      .attr("class", "d3-tooltip")
      .style("opacity", 0);

    d3.json(toAbsoluteUrl("/geojson/COLOMBIA_" + localization + ".json")).then((mapa) => {
      // ESCALA 
      const x = d3.scaleLinear()
        .domain([-1, colorScale.range().length - 1])
        .rangeRound([0, 150]);

        g.selectAll("rect")
        .data(colorScale.range())
        .join("rect")
        .attr("x", (d, i) => x(i - 1))
        .attr("y", 0)
        .attr("width", (d, i) => x(i) - x(i - 1))
        .attr("height", 10)
        .attr("fill", d => d);

      g.call(d3.axisBottom(x)
        .tickPadding(-1)
        .tickFormat(function (x, i) { return (x === 7 ? 100 : x * 15) })
        .tickSize(6)
        .tickValues(d3.range(1, 8)))
        .select(".domain")
        .remove();

      g.selectAll("text")
        .attr("transform", "translate(" + (14) + ", 0)")
        .attr("x", -40)
        .attr("y", 12)
        .attr("fill", "currentColor")
        .attr("text-anchor", "start")
        .attr("font-weight", "bold");

      // DATOS SIN UBICACIÓN
      if (noData && noData.length > 0) {
          g.selectAll("circle")
          .data(noData)
          .join("circle")
          .attr("cx", 70)
          .attr("cy", 315)
          .attr("r", 25)
          .style("fill", d => {
            return colorScale(d.frequency);
          })
          .style("opacity", "0.7")
          .attr("stroke", "#000")
          .attr("stroke-width", "0.1px")
          .on("mouseover", (d) => {
            toolTip.transition()
              .duration(200)
              .style("opacity", .9);
            toolTip.html((d.name === null ? "NoData" : d.name) + ": " + d.frequency)
              .style("left", (d3.event.pageX) + "px")
              .style("top", (d3.event.pageY - 28) + "px");
          })
          .on("mouseleave", (d) => {
            toolTip.transition()
              .duration(500)
              .style("opacity", 0);
          });
      }

      // INICIA MAPA
      function mouseOver(d) {
        let sw = data.filter(x => compute_string_diffence(x.name, d.properties.NOMBRE_DPT) >= 0.9 || compute_string_diffence(x.name, d.properties.NOMBRE_MPI) >= 0.9);
        if (sw.length > 0) {
          sumary(sw[0].name);
          d3.selectAll("." + localization)
            .transition()
            .duration(200)
            .style("opacity", .2)
          d3.select(this)
            .transition()
            .duration(200)
            .style("opacity", 1)
            .style("stroke", "black")
            .style("stroke-width", "0.5px");
          toolTip.transition()
            .duration(200)
            .style("opacity", .9);
          toolTip.html(d.properties.NOMBRE_DPT + (d.properties.NOMBRE_MPI ? " - " + d.properties.NOMBRE_MPI : "") + ": " + sw[0].frequency)
            .style("left", (d3.event.pageX) + "px")
            .style("top", (d3.event.pageY - 28) + "px");
        }
      }

      function mouseLeave(d) {
        let sw = data.filter(x => compute_string_diffence(x.name, d.properties.NOMBRE_DPT) >= 0.9 || compute_string_diffence(x.name, d.properties.NOMBRE_MPI) >= 0.9);
        if (sw.length > 0) {
          sumary("todos");
          d3.selectAll("." + localization)
            .transition()
            .duration(200)
            .style("opacity", .8)
          d3.select(this)
            .transition()
            .duration(200)
            .style("stroke", "transparent")
          toolTip.transition()
            .duration(500)
            .style("opacity", 0);
        }
      }

      g.attr("stroke", "#000")
        .attr("stroke-width", "0.1px")
        .selectAll("path")
        .data(mapa.features)
        .enter()
        .append("path")
        .attr("d", d3.geoPath()
          .projection(projection)
        )
        .attr("fill", d => {
          for (let i = 0; i < data.length; i++) {
            if (compute_string_diffence(data[i].name, d.properties.NOMBRE_DPT) >= 0.9 || compute_string_diffence(data[i].name, d.properties.NOMBRE_MPI) >= 0.9) {
              return colorScale(data[i].frequency);
            }
          }
          return "#F5F5F5";
        })
        .attr("class", d => { return localization })
        .on("mouseover", mouseOver)
        .on("mouseleave", mouseLeave);

      this.props.loading(false);
    });

    let zoom = d3.zoom()
      .scaleExtent([1, 100])
      .translateExtent([[0, 0], [width, height + 30]])
      .extent([[0, 0], [width, height + 30]])
      .on("zoom", zoomed);

    function zoomed() {
      g.attr("transform", d3.event.transform);
    }

    this.svg.call(zoom);
  }

  compute_string_diffence(word1, word2) {
    return new difflib.SequenceMatcher(null, word1, word2).ratio();
  }

  render() {
    return (
      <>
        <svg
          ref="d3_canvas"
          width="350"
          height="380"
          viewBox="0 0 350 380"
        />
      </>
    )
  }
}