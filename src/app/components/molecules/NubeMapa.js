import React, { Component } from 'react';
import * as d3 from 'd3'

/**
 * Bloque básico para generar una visualización en D3
 * 
 * @version 0.1
 */

export default class NubeMapa extends Component {
    
    componentDidMount() {
        this.svg = d3.select(this.refs.d3_canvas)
        const circles_data = this.props.buckets

        const projection = d3.geoMercator()
            .translate([600 / 2, 600 / 2])
            .scale(2000)
            .center([-72.31054687500001, 3.9132075008970837])

        const div = d3.select("body").append("div")
            .attr("class", "tooltip mapa")
            .style("opacity", 0);

        const color = d3.scaleOrdinal(d3.schemeTableau10)



        const radialScale = d3.scaleLinear().range([2, 15]).domain([0, d3.max(this.props.buckets, d => d.count)])
        //const opacity = d3.scaleLinear().range([0, 1]).domain([0, d3.max(this.props.buckets, d => d.count)])

        fetch('https://gist.githubusercontent.com/john-guerra/43c7656821069d00dcbc/raw/3aadedf47badbdac823b00dbe259f6bc6d9e1899/colombia.geo.json')
            .then(response => response.json())
            .then(data => {

                fetch('https://gist.githubusercontent.com/nestorandrespe/a1a0c14e772b3f578e410b49835bf31c/raw/e436431233d197c23581b839f88367e5c94e17f7/colombia_cercanos.json')
                    .then(response => response.json())
                    .then(data_ => {
                        this.svg.append('g').selectAll('path')
                            .data(data_.features)
                            .join('path')
                            .attr('fill', d => {
                                return '#ececec'
                            })
                            .attr('stroke', '#fff')
                            .attr('d', d3.geoPath(projection))

                        this.svg.append('g').selectAll('path')
                            .data(data.features)
                            .join('path')
                            .attr('fill', d => {
                                return '#fff'
                            })
                            .attr('stroke', '#555')
                            .attr('d', d3.geoPath(projection))

                        const nodes = this.svg.append('g').selectAll('circles')
                            .data(circles_data)
                            .join('circle')
                            .attr('cx', d => {
                                return projection([d.centroid.lon, d.centroid.lat])[0]
                            })
                            .attr('cy', d => {
                                return projection([d.centroid.lon, d.centroid.lat])[1]
                            })
                            .attr('r', d => radialScale(d.count))
                            .attr('fill', d => color(d.key.split('||')[0]))
                            .attr('stroke', '#555')
                            .on("mouseover", function (d) {
                                div.style("opacity", 1);
                                div.html(d.key.split('||')[1])
                                    .style("left", (d3.event.pageX) + "px")
                                    .style("top", (d3.event.pageY - 30) + "px");
                            })
                            .on("mouseout", function (d) {
                                div.style("opacity", 0)
                                    .style("left", 0 + "px")
                                    .style("top", 0 + "px");
                            })

                        d3.forceSimulation(circles_data)
                            .force("collide", d3.forceCollide(d => radialScale(d.count)).iterations(2))
                            .force("x", d3.forceX(function (d) {
                                var str = projection([d.centroid.lon, d.centroid.lat])[0]
                                return str;
                            }))
                            .force("y", d3.forceY(function (d) {
                                var str = projection([d.centroid.lon, d.centroid.lat])[1]
                                return str;
                            }))
                            .on("tick", () => {
                                nodes.attr("cx", function (d) {
                                    return d.x;
                                })
                                    .attr("cy", function (d) {
                                        return d.y;
                                    })
                            })
                    })
            })
        // this.svg.
    }

    render() {
        return (
            <>
                <svg
                    ref="d3_canvas"
                    width="600"
                    height="600"
                    viewBox="0 0 600 600"
                />
            </>
        )
    }
}
