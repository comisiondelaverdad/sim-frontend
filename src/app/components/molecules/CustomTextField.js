import React from "react";

function CustomTextField(props) {
  return (
    <div className="cev-custom-textarea">
      {props.tags && props.tags.length > 0 && props.tags.map((tag, idx) => (
        <React.Fragment key={`r-${idx}`}>
          {props.currentPosition === 0 && idx === 0 &&
            <input autoFocus type="text" className="form-control" onKeyDown={(evt) => { props.events.onKeyDown(evt) }} 
            onChange={(evt) => { props.events.onChange(evt) }}/>
          }
          <button type="button" className="cev-close-button" key={idx} onClick={(evt) => { props.events.deleteTag(idx) }}>{tag}</button>
          {props.currentPosition > 0 && idx === props.currentPosition - 1 &&
            <input autoFocus type="text" className="form-control" onKeyDown={(evt) => { props.events.onKeyDown(evt) }} 
            onChange={(evt) => { props.events.onChange(evt) }} />
          }
        </React.Fragment>
      ))}
      {props.tags.length === 0 &&
        <input autoFocus type="text" className="form-control" onKeyDown={(evt) => { props.events.onKeyDown(evt) }} 
        onChange={(evt) => { props.events.onChange(evt) }} />
      }
    </div>
  );
}

export default CustomTextField;