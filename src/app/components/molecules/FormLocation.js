import React, { Component } from "react";
import { Form, Col } from "react-bootstrap";
import _ from "lodash";
import { worldCountries, worldAdministrativeDivision, colAdministrativeDivision, colmunicipality } from "../../services/arcgisService"
export default class FormLocation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            formData: {},
            countries: [],
            departaments: [],
            cities: [],
            formConfig: props.formConfig,
            place: [],
            code: [],
            geoPoint: { lat: 0, lon: 0 },
            showCity: true,
            loaded: false,
        }
        this.handleChangeSelectContry = this.handleChangeSelectContry.bind(this);
        this.handleChangeSelectDepartament = this.handleChangeSelectDepartament.bind(this);
        this.handleChangeSelectCity = this.handleChangeSelectCity.bind(this);
        this.loadContries();
        this.loadColDepartaments();
    }

    async componentDidUpdate(prevProps) {

        let updateValid = false;
        if (this.props.keyValue > 0 | (JSON.stringify(prevProps.formConfig) !== JSON.stringify(this.props.formConfig))) {
            updateValid = true;
        }
        if (updateValid && !this.state.loaded) {
            let formLocation = this.props.formData.code ? this.props.formData.code.split("-") : '';
            let formData = {
                local: formLocation[2] ? formLocation[2] : '',
                departament: formLocation[1] ? formLocation[1] : '',
                country: formLocation[0] ? formLocation[0] : ''
            }
            switch (formLocation.length) {
                case 1:
                    if (formLocation[0] !== 'CO') {
                        this.loadDepartaments(formLocation[0])
                        this.setState({ showCity: false })
                    }
                    break;
                case 2:
                    if (formLocation[0] === 'CO') {
                        this.loadColMunicipality(formLocation[1])
                    } else {
                        this.setState({ showCity: false })
                        this.loadDepartaments(formLocation[0])
                    }
                    break;
                case 3:
                    this.loadColMunicipality(formLocation[1])
                    break;

                default:
                    break;
            }
            this.setState({ loaded: true, formData: formData, place: this.props.formData.name ? this.props.formData.name.split(',') : [] });
        }
    }

    handleChangeSelectContry(event) {
        const { name, value } = event.target;
        if (value === '') {
            this.setState({ departaments: [], cities: [] })
        }
        let form = { [name]: value };
        //console.log({ name, value });
        if (value) {
            const countrySelected = this.state.countries.filter((country) => (country.value === value));
            this.setState({ place: [(countrySelected[0].name) ? (countrySelected[0].name).toUpperCase() : ''], geoPoint: countrySelected[0].geoPoint });
            this.setState({ formData: form });

            this.props.outputEvent({ formData: form, place: [(countrySelected[0].name).toUpperCase()], geoPoint: countrySelected[0].geoPoint });
            this.setState({ cities: [] });
            this.setState({ departaments: [] });
            if (value === 'CO') {
                this.loadColDepartaments();
                this.setState({ showCity: true });
            } else {
                this.loadDepartaments(value);
                this.setState({ showCity: false });
            }
        } else {
            console.log({ name, value });
            this.setState({ formData: form });
            this.props.outputEvent({ formData: form, place: null });
        }
    }

    handleChangeSelectDepartament(event) {
        const { name, value } = event.target;
        let form = { ...this.state.formData, ...{ [name]: value } };
        const departament = this.state.departaments.filter((dep) => (dep.value === value));
        let place = this.state.place;
        place[1] = (departament[0] ? departament[0].name ? departament[0].name : '' : '').toUpperCase();
        this.setState({ place: place });
        this.setState({ formData: form });
        this.props.outputEvent({ formData: form, place: place });
        if (this.state.formData.country === 'CO') {
            this.loadColMunicipality(value);
        }
    }

    handleChangeSelectCity(event) {
        const { name, value } = event.target;
        console.log({ name, value });
        const city = this.state.cities.filter((c) => (c.value === value));
        let place = this.state.place;
        place[2] = (city[0].name).toUpperCase();
        this.setState({ place: place });
        let form = { ...this.state.formData, ...{ [name]: value } };
        this.setState({ formData: form });
        this.props.outputEvent({ formData: form, place: place });
    }

    loadContries() {
        worldCountries()
            .then((countries) => {
                this.setState({
                    countries: countries
                })
                const defaultValue = {
                    formData: {
                        ...this.state.formData,
                        ...{ country: 'CO' }
                    }
                };
                if (!this.state.formData)
                    this.setState(defaultValue)
            });
    }


    loadDepartaments(iso) {
        worldAdministrativeDivision(iso)
            .then((departament) => {
                this.setState({
                    departaments: _.uniqBy(departament.features.map((dep) => {
                        return {
                            name: dep.attributes.NAME,
                            value: dep.attributes.ISO_SUB
                        }
                    }), 'value')
                })
            });
    }

    loadColDepartaments() {
        colAdministrativeDivision()
            .then((departament) => {
                this.setState({ departaments: departament })
            });
    }

    loadColMunicipality(departamentId) {
        colmunicipality(departamentId)
            .then((cities) => {
                this.setState({ cities: _.orderBy(cities, [(city) => ((city.name.toLowerCase())).trim()], ['asc']) })
            });
    }


    render() {
        return (
            <div className="col-md-12">
                <Form.Row>
                    <Form.Group as={Col} md={this.state.showCity === true ? "4" : "6"} controlId="country" >
                        <Form.Label>{"Pais"}{this.state.formConfig.country.required ? ' *' : ''}</Form.Label>
                        <Form.Control
                            as="select"
                            name="country"
                            value={this.state.formData.country ? this.state.formData.country : ''}
                            onChange={this.handleChangeSelectContry}
                            required={this.state.formConfig.country.required ? this.state.formConfig.country.required : false}>
                            <option key={-1} value="">{"Seleccione el país"}</option>
                            {this.state.countries.map((data, key) => (<option key={key} value={data.value}>{data.name}</option>))}
                        </Form.Control>
                    </Form.Group>

                    <Form.Group as={Col} md={this.state.showCity === true ? "4" : "6"} controlId="departament" >
                        <Form.Label>{"Departamento"}{this.state.formConfig.departament.required ? ' *' : ''}</Form.Label>
                        <Form.Control
                            as="select"
                            name="departament"
                            value={this.state.formData.departament ? this.state.formData.departament : ''}
                            onChange={this.handleChangeSelectDepartament}
                            required={this.state.formConfig.departament.required ? this.state.formConfig.departament.required : false}>
                            <option key={-1} value="">{"Seleccione el departamento"}</option>
                            {this.state.departaments.map((data, key) => (<option key={key} value={data.value}>{data.name}</option>))}
                        </Form.Control>
                    </Form.Group>
                    {(this.state.showCity &&
                        (<Form.Group as={Col} md={this.state.showCity === true ? "4" : "6"} controlId="local" >
                            <Form.Label>{"Ciudad / Municipio"}{this.state.formConfig.city.require ? ' *' : ''}</Form.Label>
                            <Form.Control
                                as="select"
                                name="local"
                                value={this.state.formData.local ? this.state.formData.local : ''}
                                onChange={this.handleChangeSelectCity}
                                required={this.state.formConfig.city.required ? this.state.formConfig.city.required : false}>
                                <option key={-1} value="">{"Seleccione Ciudad / Municipio"}</option>
                                {this.state.cities.map((data, key) => (<option key={key} value={data.value}>{data.name}</option>))}
                            </Form.Control>
                        </Form.Group>)
                    )}
                </Form.Row>
            </div>
        )
    }
}