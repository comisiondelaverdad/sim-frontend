import React, { Component } from "react";

class Resume extends Component {
  render() {
    return (
      <div>
        RESUMEN DE CADA UNA DE LAS FICHAS
        <h1>{this.props.result.title}</h1>
        <p>{this.props.result.description}</p>
        <ul>
          <li>{this.props.result.format}</li>
        </ul>
      </div>
    );
  }
}

export default Resume;
