
import React, {
    Component
} from 'react'
import ThreeHelper from '../helpers/ThreeMapHelper'

class Mapa3D extends Component {

    componentDidMount() {
        this.helper = new ThreeHelper(document.getElementById('mapa_view'))
    }

    componentWillUnmount() {
        this.helper.unmount()
    }

    render() {
        return (
            <>
                <div id="mapa_view"></div>
            </>
        )
    }
}

export default Mapa3D