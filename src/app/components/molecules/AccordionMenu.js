import React, { useState, useEffect, useRef } from "react";
import { Button, Form, Card, Row, Col } from "react-bootstrap";
import Accordion from "react-bootstrap/Accordion";
import Dropdown from "react-bootstrap/Dropdown";
import { toAbsoluteUrl } from "../../../theme/utils";
import { LABELS_MENU } from "../../../app/config/constLabels";

const style = {
  width: "100%",
  backgroundColor: "white",
  color: "black",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
};

const Icon = (props) => {
  return (
    <div>
      <svg className="cv-medium">
        <use
          xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#" + props.name)}
          className="flia-green"
        ></use>
      </svg>
      <span>{props.name}</span>
    </div>
  );
};

function AccordionMenu(props) {
  const [hover, setHover] = useState(false);
  const [element, setElement] = useState(props.item);
  const [prev, setPrev] = useState(props.item);
  const [toggleContents, setToggleContents] = useState(
    props.item.icon ? <Icon name={props.item.icon} /> : "Seleccione..."
  );
  const [hover2, setHover2] = useState(false);
  const [show, setShow] = useState(false);

  useEffect(() => {
    if (JSON.stringify(element) !== JSON.stringify(prev)) {
      if (element.tag) props.changeElement(element);
      else alert("El campo etiqueta de navegación no puede ser vacío");
    }
  }, [element]);

  const styleButtonDown = {
    backgroundColor: "#f7f8fa",
    color: "black",
  };
  const styleIcon = {
    verticalAlign: "unset",
  };
  function deleteItem(id) {
    props.deleteItem(id);
  }

  function handleChange(event) {
    let { name, value, checked } = event.target;
    setElement({
      ...element,
      [name]: value,
    });
  }
  function handleChangeCheck(event) {
    let { name, checked } = event.target;
    setElement({
      ...element,
      tab: checked,
    });
  }
  function handleChangeOnly(event) {
    let { name, checked } = event.target;
    setElement({
      ...element,
      only: checked,
    });
  }

  function handleChangeIcon(icon) {
    setElement({
      ...element,
      icon: icon,
    });
  }

  return (
    <>
      <Accordion style={{ marginBottom: "6px", width: "75%" }}>
        <Card>
          <Card.Header>
            <div className="d-flex justify-content-between align-items-center">
              <strong className="mx-auto">{props.tag}</strong>
              <div>
                <button
                  onClick={(e) => {
                    setShow(!show);
                  }}
                  type="button"
                  className="btn btn-"
                >
                  <i className="fas fa-sort-down"></i>
                </button>
              </div>
            </div>
          </Card.Header>
          <Accordion.Collapse
            className={show ? "show" : ""}
            eventKey={element.id}
          >
            <Card.Body id={element.id}>
              <Form.Group controlId="">
                <label>{LABELS_MENU.navigation_label} *</label>
                <Form.Control
                  name="tag"
                  type="text"
                  onChange={handleChange}
                  value={element.tag}
                />
              </Form.Group>
              <Form.Group controlId="">
                <label>{LABELS_MENU.route}</label>
                <Form.Control
                  type="text"
                  name="route"
                  placeholder=""
                  onChange={handleChange}
                  value={element.route}
                />
              </Form.Group>

              <Form.Group controlId="">
                <label>{LABELS_MENU.image}</label>
                <Form.Control
                  type="text"
                  placeholder=""
                  onChange={handleChange}
                  value={element.img}
                  name="img"
                />
              </Form.Group>
              {props.section && props.section == "1" ? (
                <Form.Group>
                  <label>{LABELS_MENU.icon}</label>
                  <Dropdown
                    size="lg"
                    onSelect={(eventKey, e) => {
                      handleChangeIcon(eventKey);
                      if (eventKey)
                        setToggleContents(
                          <>
                            <Icon name={eventKey} />
                          </>
                        );
                      else setToggleContents("Seleccione un ícono...");
                    }}
                  >
                    <Dropdown.Toggle
                      variant="primary"
                      id="dropdown-flags"
                      className="text-left border"
                      style={hover2 ? style : style}
                      onMouseLeave={() => setHover2(false)}
                      onMouseEnter={() => setHover2(true)}
                    >
                      {toggleContents}
                    </Dropdown.Toggle>
                    <Dropdown.Menu onClick={(e) => e.preventDefault()}>
                      {
                        <>
                          <Dropdown.Item
                            onClick={(e) => e.stopPropagation()}
                            eventKey={null}
                          >
                            Seleccione un ícono...
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-localizacion"}>
                            <Icon name="icono-localizacion" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-entrevista"}>
                            <Icon name="icono-entrevista" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-audio"}>
                            <Icon name="icono-audio" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-calendar"}>
                            <Icon name="icono-calendar" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-informe"}>
                            <Icon name="icono-informe" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-fichacorta"}>
                            <Icon name="icono-fichacorta" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-concepto"}>
                            <Icon name="icono-concepto" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-metadata"}>
                            <Icon name="icono-metadata" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-casos"}>
                            <Icon name="icono-casos" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-acceso"}>
                            <Icon name="icono-acceso" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-acta"}>
                            <Icon name="icono-acta" />
                          </Dropdown.Item>
                          <Dropdown.Item eventKey={"icono-fichalarga"}>
                            <Icon name="icono-fichalarga" />
                          </Dropdown.Item>
                        </>
                      }
                    </Dropdown.Menu>
                  </Dropdown>
                </Form.Group>
              ) : (
                <Form.Group controlId="">
                  <label>{LABELS_MENU.material_design_icon}</label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    onChange={handleChange}
                    value={element.material_icon}
                    name="material_icon"
                  />
                </Form.Group>
              )}
              {props.section && props.section == "3" ? (
                <Form.Group controlId="">
                  <label>{LABELS_MENU.css_class_icon}</label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    onChange={handleChange}
                    value={element.clases}
                    name="clases"
                  />
                </Form.Group>
              ) : (
                ""
              )}

              <Form.Group controlId="">
                <Form.Check
                  onChange={handleChangeOnly}
                  type="checkbox"
                  label={LABELS_MENU.only_icon}
                  checked={element.only}
                  name="tab"
                />
              </Form.Group>
              <Form.Group controlId="">
                <Form.Check
                  onChange={handleChangeCheck}
                  type="checkbox"
                  label={LABELS_MENU.new_tab}
                  checked={element.tab}
                  name="tab"
                />
              </Form.Group>
              <a
                onClick={() => {
                  deleteItem(element.id);
                }}
                href="#"
              >
                {" "}
                {LABELS_MENU.delete_element}
              </a>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
      <div className="child"></div>
    </>
  );
}
export default AccordionMenu;
