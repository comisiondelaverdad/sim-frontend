import React, { Component } from 'react';
import ChoroplethBar from '../organisms/ChoroplethBar';
import { Container } from 'react-bootstrap';
import { HotTable } from "@handsontable/react";
import PDFViewer from "./PDFViewer";
import "handsontable/dist/handsontable.full.css";
import 'handsontable/languages/es-MX';

export default class MicroData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      json: undefined,
      excel: false
    }

    this.download = this.download.bind(this);
    this.convertBlobToJSON = this.convertBlobToJSON.bind(this);

    this.tableData = React.createRef();
  }

  componentDidMount() {
    this.convertBlobToJSON();
  }

  convertBlobToJSON() {
    let type = this.props.file.type.split("/").pop();
    if(type === "json"){
      let reader = new FileReader();
      reader.onload = (evt) => {
        this.setState({ json: JSON.parse(evt.target.result) });
      }
      reader.readAsText(this.props.file);
    }
    else{
      this.setState({excel: true})
    }
  }


  download() {
    const exportCSV = this.tableData.current.hotInstance.getPlugin('exportFile');
    exportCSV.downloadFile('csv', { filename: this.props.filename });
  }

  render() {
    const settigns = {
      data: this.state.json ? this.state.json : [],
      rowHeaders: true,
      colHeaders: true,
      licenseKey: 'non-commercial-and-evaluation',
      filters: true,
      dropdownMenu: ['filter_by_value', 'filter_by_condition', 'filter_action_bar'],
      width: "100%",
      sortIndicator: true,
      manualColumnFreeze: true,
      autoColumnSize: { useHeaders: true },
      exportFile: true,
      language: 'es-MX',
      contextMenu: true,
      multiColumnSorting: true
    }

    return (
      <>
        {false && this.props.file &&
          <div style={{ display: 'flex', justifyContent: "end", marginBottom: "20px" }}>
            <button className="btn btn-icon btn-success" onClick={this.download}>
              <i className="flaticon2-download" />
            </button>
          </div>
        }
        <>
          <Container fluid={true} className="mb-10">
            <ChoroplethBar data={this.state.json} id={this.props.id} />
          </Container>
        </>
        <>
          <Container fluid={true} className="p-10 mt-20 mr-20 w-90">
            {this.state.json && this.state.json.length > 0 &&
              <Container fluid={true} className="w-100 h-auto">
                <HotTable ref={this.tableData} settings={settigns} />
              </Container>
            }
            {this.state.excel &&
              <PDFViewer file={this.props.file} type={this.props.type} />
            }
          </Container>
        </>
      </>
    )
  }
}