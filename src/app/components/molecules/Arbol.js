import React, {
  Component
} from 'react';
import * as d3 from 'd3'

/**
 * Bloque básico para generar una visualización en D3
 * 
 * @version 0.1
 */

export default class Arbol extends Component {

  componentDidMount() {
    this.svg = d3.select(this.refs.d3_canvas).append('g')
    this.svg.attr('style', `transform: translate(0,-${this.props.margin}px)`)

    this.updateViz()
  }

  componentDidUpdate() {
    this.svg.attr('style', `transform: translate(0,-${this.props.margin}px)`)
  }

  updateViz() {
    const radialScale = d3.scaleLinear().range([2, 20]).domain([0, 1])
    const step = 60
    const color_tipo = d3.scaleOrdinal(d3.schemeSet2)
    const scaleColor = d3.scaleLinear().range(['#FFF', '#F2B705']).domain([0, 1])
    const pie = d3.pie()
      .padAngle(0.005)
      .sort(null)
      .value(d => d.value)

    let resp = drawChild(this.svg, this.props.root, 0, "red")

    console.log(resp)

    resp.children.forEach(r => {

      if (r.children.children) {
        r.children.children.forEach(l => {
          if (l.children.children) {
            l.children.children.forEach(t => {
              // svg.append('line')
              // .attr('x1', l.coor[0])
              // .attr('y1', l.coor[1])
              // .attr('x2', t.coor[0])
              // .attr('y2', t.coor[1])
              // .attr('stroke', "#000")
              // .attr('opacity', 0.3)

              this.svg.append('path')
                .attr('stroke', "#000")
                .attr('opacity', 0.3)
                .attr('fill', '#fff')
                .attr('fill-opacity', 0)
                .attr("d", () => `
                      M${t.coor[0]},${t.coor[1]}
                      C${l.coor[0]},${t.coor[1]}
                       ${l.coor[0] + 200},${l.coor[1]}
                       ${l.coor[0]},${l.coor[1]}
                    `);

              drawCircles(this.svg, t)

            })
          }
          this.svg.append('path')
            .attr('stroke', "#000")
            .attr('opacity', 0.3)
            .attr('fill', '#fff')
            .attr('fill-opacity', 0)
            .attr("d", () => `
                      M${l.coor[0]},${l.coor[1]}
                      C${r.coor[0]},${l.coor[1]}
                       ${r.coor[0] + 200},${r.coor[1]}
                       ${r.coor[0]},${r.coor[1]}
                    `);

          drawCircles(this.svg, l)
        })
      }
      drawCircles(this.svg, r)

    })

    function drawCircles(svg, r) {
      const g = svg.append('g')
        .attr('transform', `translate(${r.coor[0]}, ${r.coor[1]})`)
      scaleColor.range(["#fff", r.color])

      let data = [{
        name: "si",
        value: r.value
      },
      {
        name: "no",
        value: 1 - r.value
      }
      ]

      const arcs = pie(data);

      g.append('circle')
        .attr('r', () => {
          return radialScale(1)
        })
        .attr('stroke', '#ccc')
        .attr('fill', '#fff')
        .attr('fill-opacity', 0.7)

      g.selectAll("path")
        .data(arcs)
        .join("path")
        .attr("fill", d => d.data.name === 'si' ? r.color : '#fcfcfc')
        .attr("d", d3.arc().innerRadius(radialScale(0.9)).outerRadius(radialScale(1)))

      g.append('circle')
        .attr('r', () => {
          return radialScale(r.value)
        })
        .attr('fill', d => scaleColor(r.value))

      g.append('text')
        .text(r.name)
        .attr('text-anchor', 'middle')
        .attr('fill', '#333')
        .attr('font-size', 14)
        .attr('y', -23)
      // .attr('font-weight', 'bold')

      if (r.value) {
        g.append('circle')
          .attr('r', () => {
            return 15
          })
          .attr('stroke', '#ccc')
          .attr('fill', '#fff')
          .attr('fill-opacity', 0.9)
          .attr('cx', 40)

        g.append('text')
          .text(r.value.toFixed(2))
          .attr('text-anchor', 'middle')
          .attr('font-size', 11)
          .attr('alignment-baseline', 'middle')
          .attr('fill', '#333')
          .attr('y', 2)
          .attr('x', 40)
      }

    }


    function getChildHeight(child) {
      let resp = 0
      if (child.children.length > 0) {
        child.children.forEach(c => {
          if (c.children.length > 0) resp += c.children.length * step
          else resp += step
        })
      } else {
        resp = step
      }

      return resp
    }

    function getHeight(r) {
      let m = 0
      if (r.data.children.length > 0) {
        m += getChildHeight(r.data)
        // drawChild(svg, r.children, r.depth, 0)
      } else m += step

      return m
    }

    function drawChild(svg, data, level, color) {
      let m = 0

      let co = color

      let resp = {
        children: []
      }

      if (data.parent != null) {
        for (let i = 0; i < data.index; i++) {
          m += getHeight(data.parent.children[i])
        }
        if (data.depth === 2) {
          for (let i = 0; i < data.parent.index; i++) {
            m += getHeight(data.parent.parent.children[i])
          }
        }
      }

      data.children.forEach((r, i) => {
        if (data.depth === 0) co = color_tipo(r.data.name)

        let obj = {
          coor: [],
          children: [],
          value: 0,
          color: co,
          name: r.data.name,
        }

        const m_ = m
        m += getHeight(r)


        if (r.data.children.length > 0) {
          obj.children = drawChild(svg, r, r.depth, co)
        }

        const x_step = 350 * level + 40
        const y_step = (m - m_) / 2



        obj.coor = [x_step, m_ + y_step]
        if (data.data.children[i].value) obj.value = r.data.value
        // if (data.data.children[i].value) obj.value = data.data.children[i].value
        else obj.value = 0

        resp.children.push(obj)
      })

      return resp
    }

  }



  render() {
    return (<>
      <svg ref="d3_canvas"
        width="1000"
        height="40000"
        viewBox="0 0 1000 40000" />
    </>
    )
  }
}