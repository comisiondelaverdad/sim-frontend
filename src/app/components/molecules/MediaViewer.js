import React, { Component } from "react";
import {connect} from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import ReactWaterMark from 'react-watermark-component';
import ReactPlayer from 'react-player'

class MediaViewer extends Component {
  state = {};

  render() {
    const {
      name
    } = this.props.user;

    const options = {
      chunkWidth: 450,
      chunkHeight: 90,
      textAlign: 'left',
      textBaseline: 'bottom',
      globalAlpha: 0.2,
      font: '36px Microsoft Yahei',
      rotateAngle: -0.1,
      fillStyle: '#666'
    }

    return (
      <>
        <Container className="bg-light">
          <Row>
            <Col xs={12}>
              <ReactWaterMark
                waterMarkText={name}
                options={options}
              >
                <Container style={{width:"660px",height:"360px"}}>
                  <Container style={{width:"660px",height:"100%", backgroundColor:"rgba(0,0,0,.1)"}}>                  
                    <ReactPlayer
                      onContextMenu={(evt)=>{evt.preventDefault();}}
                      key={this.props.id}
                      url={URL.createObjectURL(this.props.file)}
                      controls={true}
                      config={{ file: { attributes: { controlsList: 'nodownload' }}}}
                    />
                  </Container>
                </Container>
              </ReactWaterMark>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps
)(MediaViewer);