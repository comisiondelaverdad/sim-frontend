import React, { Component } from "react";
import {connect} from "react-redux";
import { Container, Row, Col, Image } from "react-bootstrap";
import ReactWaterMark from 'react-watermark-component';
import Viewer from 'react-viewer';

class ImgViewer extends Component {
  constructor () {
    super()

    this.state = {
      visible: false
    }

  }

  render() {
    const {
      name
    } = this.props.user;

    const options = {
      chunkWidth: 450,
      chunkHeight: 90,
      textAlign: 'left',
      textBaseline: 'bottom',
      globalAlpha: 0.2,
      font: '36px Microsoft Yahei',
      rotateAngle: -0.1,
      fillStyle: '#666'
    }

    const img = [
      {"src": URL.createObjectURL(this.props.file), "title": "Img Name", "content": "content"}
    ]

    return (
      <>
        <Container className="bg-light">
          <Row>
            <Col xs={12}>
              <ReactWaterMark
                waterMarkText={name}
                options={options}
              >
                <Container style={{width:"660px",height:"450px"}}>
                  <Container style={{width:"660px",height:"100%", backgroundColor:"rgba(0,0,0,.1)"}}>
                    <Image 
                      className="mx-auto d-block" 
                      src={URL.createObjectURL(this.props.file)} 
                      fluid 
                      style={{maxWidth:"100%", maxHeight:"100%"}}
                      onClick={() => { this.setState({visible: true}) } }
                    />
                    <Viewer
                      visible={this.state.visible}
                      onClose={() => { this.setState({visible: false}) } }
                      images={img}
                      showTotal={false}
                      noImgDetails={true}
                    />
                  </Container>
                </Container>
              </ReactWaterMark>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps
)(ImgViewer);