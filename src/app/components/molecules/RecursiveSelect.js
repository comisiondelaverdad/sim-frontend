import React, { Component } from "react";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { updateListSelected } from "../../services/selectListObserver";
import $ from 'jquery';
import { isArray } from "lodash";
import { updateSelectRecursive } from '../../services/selectListObserver';
import { Form, Col } from "react-bootstrap";

export class RecursiveSelect extends Component {

    constructor(props) {
        super(props);
        this.state = {
            item: props.item,
            level: props.level,
            field: props.field,
            fieldChild: null,
            data: props.data,
            datachild: props.datachild,
            formData: null,
            itemChild: props.itemChild,
            baseData: null,
            changed: props.changed,
            changedChild: props.changedChild,
        };
        this.handleChangeSelect = this.handleChangeSelect.bind(this);
    }

    handleChangeSelect(event) {
        this.setState({ itemChild: null })
        event.stopPropagation();
        const { name, value, selectedOptions, multiple } = event.target;
        let form = {
            ...this.state.formData, ...{
                [name]: multiple ? [...selectedOptions]
                    .map((data) => (data.value))
                    .filter((data) => (data !== '')) : value
            }
        };
        const idBase = name.split("-").join('');
        // console.log('changeSelected', value);
        const fieldBase = value.split("-").map((o) => (o.trim())).filter((o, i) => (i < this.state.level - 1));
        updateSelectRecursive({ [idBase]: form[name] });
        this.updateField(this.state.field, form, true, fieldBase.join(" - "));
    }

    componentDidUpdate(prevProps) {
        if (this.props.item !== prevProps.item) {
            this.setState({ item: this.props.item });
        }
        if (JSON.stringify(this.props.data) !== JSON.stringify(prevProps.data)) {
            const { field, data, level } = this.state;
            const fieldBase = this.props.data.split("-").map((o) => (o.trim())).filter((o, i) => (i < this.state.level - 1));
            const elementHTML = document.getElementById(this.state.field.id);
            const newData = this.props.data.split("-").map((o) => (o.trim())).filter((o, i) => (i < this.state.level));
            elementHTML.value = newData.join(" - ") === '' ? fieldBase.join(" - ") : newData.join(" - ");
            this.setState({ data: this.props.data, fieldBase: fieldBase.join(" - ") });
            this.mountField({ field: field, data: this.props.data, level: level });
        }
    }

    updateField(field, itemSelected, changeSelected, baseData) {
        if (itemSelected && itemSelected[field.id]) {
            const elementHTML = document.getElementById(field.id);
            const newData = itemSelected[field.id].split("-").map((o) => (o.trim())).filter((o, i) => (i < this.state.level));
            elementHTML.value = newData.join(" - ") === '' ? baseData : newData.join(" - ");
            const newList = this.state.item.filter((data) => (itemSelected[field.id].includes(data.value)));
            const options = JSON.stringify(newList) !== '[]' ? newList[0].lists ? newList[0].lists.options : false : false;
            if (options) {
                this.setState({
                    datachild: itemSelected ? itemSelected[field.id] ? itemSelected[field.id] : baseData : baseData,
                    data: itemSelected ? itemSelected[field.id] ? itemSelected[field.id] : baseData : baseData,
                    baseData: baseData,
                    itemChild: isArray(options) ? options : null,
                    fieldChild: options !== undefined ? {
                        ...this.state.field,
                        ...{ id: this.state.field.id + "-" }
                    } : null,
                });
            } else {
                this.setState({
                    datachild: itemSelected ? itemSelected[field.id] ? itemSelected[field.id] : baseData : baseData,
                    data: itemSelected ? itemSelected[field.id] ? itemSelected[field.id] : baseData : baseData,
                    baseData: baseData,
                    itemChild: null,
                    fieldChild: null,

                });
            }
        } else {
            this.setState({
                datachild: itemSelected ? itemSelected[field.id] ? itemSelected[field.id] : baseData : baseData,
                data: itemSelected ? itemSelected[field.id] ? itemSelected[field.id] : baseData : baseData,
                baseData: baseData,
                itemChild: null,
                fieldChild: null,
            });
        }
    }

    mountField({ field, data, level }) {
        if (data) {
            const fieldBase = data.split("-").map((o) => (o.trim())).filter((o, i) => (i < level - 1));
            this.updateField(field, { [field.id]: data }, false, fieldBase.join(" - "))
        }
    }

    componentDidMount() {
        const { field, data, level } = this.state;
        this.mountField({ field, data, level });
    }

    render() {
        let { item, itemChild, level, field, baseData, data, fieldChild, datachild } = this.state;
        baseData = (data.split("-").map((o) => (o.trim())).filter((o, i) => (i < level - 1))).join(" - ");
        return (
            <>
                {isArray(item) && JSON.stringify(item) !== '[]' && (
                    <>
                        <Form.Group as={Col} md={field.col ? field.col : "6"} controlId={field.id}>
                            <Form.Label className="form-label">{level == 1 ? field.label : ''}{level == 1 ? field.required ? ' *' : '' : ''}</Form.Label>
                            <Form.Control
                                as="select"
                                name={field.id}
                                required={level == 1 ? field.required ? field.required : false : false}
                                onChange={this.handleChangeSelect}
                            >
                                <option key={0} value={baseData}>{field.placeholder}</option>
                                {item.map((data, key2) =>
                                    (<option key={key2} value={data.value}>{data.name}</option>))
                                }
                            </Form.Control>
                            <Form.Text className="text-muted">
                                {field.info}
                            </Form.Text>
                            {field.infoHTML && <div dangerouslySetInnerHTML={{ __html: field.infoHTML }} />}
                        </Form.Group>
                        {itemChild !== null && itemChild !== undefined && (
                            <RecursiveSelect
                                field={fieldChild}
                                item={itemChild}

                                data={datachild}
                                level={level + 1}
                            ></RecursiveSelect>
                        )}
                    </>
                )}
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(RecursiveSelect);


