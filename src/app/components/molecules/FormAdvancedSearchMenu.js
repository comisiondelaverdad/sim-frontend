import React from "react";
import { connect } from "react-redux";
import { Dropdown, Alert, Form as FormB, InputGroup, Button } from "react-bootstrap";
import DropDownIcon from "../molecules/DropDownIcon";
import SIMMapForm from "../molecules/SIMMapForm";
import Form from "react-jsonschema-form-bs4";
import { Typeahead } from 'react-bootstrap-typeahead';
import * as FormService from "../../services/FormService";
import _ from "lodash";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
class FormAdvancedSearchMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newLabel: '',
      show: false,
      alert: false,
      form: undefined,
      isLoading: false,
    };
  }

  componentDidMount() {
    this.setState({ oldLabel: this.props.label, newLabel: this.props.label, show: false });
  }

  selectForm(event) {
    let f = this.props.config.forms[event.target.value]
    if (f !== undefined) {
      if (f.type === 'form-select') {
        FormService.getOptions(f.field)
          .then(json => {
            if (json.aggregations.options.buckets.length > 0) {
              let data = _.map(json.aggregations.options.buckets, 'key')
              if (data.length > 20) {
                f.type = "autocomplete"
                f.data = data
              }
              else {
                f.schema.properties[f.name].enum = data
              }
            } else {
              f.schema.properties[f.name].enum = ["Sin Registros"]
            }
            f.schema.properties[f.name].enum = f.schema.properties[f.name].enum.sort()
            this.setState({ form: f })
          });
      } else {
        this.setState({ form: f })
      }
    } else {
      this.setState({ form: null })
    }
  }

  onChange(value) {
    if (value.selected.length > 0) {
      let search = value.selected[0];
      let query = this.state.form.queryAlias ? this.state.form.queryAlias : this.state.form.query
      query = query.split("[" + this.state.form.name + "]").join(search);
      this.setState({ "query": query, "formData": search })
    }
  }

  onClick() {
    let query = this.state.query
    this.setState({ show: false, form: undefined }, this.props.addQuery(query))
  }

  onSubmit(formData, e) {
    e.preventDefault()
    let query = this.state.form.queryAlias ? this.state.form.queryAlias : this.state.form.query
    if (typeof formData === "object") {
      for (const prop in formData) {
        if (formData[prop] !== "Sin Registros") {
          query = query.split("[" + prop + "]").join(formData[prop]);
        }
      }
    } else {
      query = formData
    }
    if (query !== this.state.form.query) {
      this.setState({ "query": query, "formData": formData, show: false, form: undefined }, this.props.addQuery(query))
    } else {
      console.log("No se puede agregar registro")
    }
  }

  asyncSearch(query) {
    this.setState({ isLoading: true });
    FormService.getAsyncOptions(this.state.form, query)
      .then(json => {
        this.setState({
          isLoading: false,
          options: json.items,
        })
      });
  }

  mapSelect(geojsonData) {
    this.setState({ "mapData": geojsonData })
  }

  mapAdd() {
    let q = ""
    let comma = ""
    _.map(this.state.mapData.features, (v, k) => {
      q += comma + v.geometry.type[0] + "_"
      let comma2 = ""
      _.map(v.geometry.coordinates, (w, i) => {
        q += comma2 + w
        comma2 = "-"
      })
      comma = "|"
    })
    this.setState({ show: false, form: undefined }, this.props.addMap(this.state.mapData, q))
  }

  render() {
    const {
      show,
      alert
    } = this.state;

    let classDropdown = this.props.showSearchBar ? "" : "cev-advancedform";

    return (
      <Dropdown
        drop="down"
        show={show}
        onToggle={() => { this.setState({ show: false }) }}
      >
        <Dropdown.Toggle
          as={DropDownIcon}
          onClick={() => { this.setState({ show: true }) }}
        >
          <button title="Búsqueda avanzada" type="button" className="btn-small ml-2 btn btn-primary">
            <AddIcon></AddIcon>
          </button>
        </Dropdown.Toggle>
        <Dropdown.Menu flip={"false"} className={`dropdown-menu-fit dropdown-menu-left dropdown-menu-anim dropdown-menu-xl ${classDropdown} advanceSearch`}>
          <div className="cev-portlet">
            <div className="cev-portlet__head-title">
              <h3>Agregar búsqueda</h3>
            <button type="button" className="btn-small ml-2 btn btn-danger" onClick={() => { this.setState({ show: false }) }}>
              <CloseIcon></CloseIcon>
            </button>
            </div>
            <div className="cev-portlet__head-toolbar">
              {(!this.props.config || !this.props.config.forms) &&
                <span>
                  Cargando configuración de formularios
                </span>
              }
              {this.props.config && this.props.config.forms &&
                <select onChange={(e) => this.selectForm(e)} >
                  <option value="-1" >Seleccione una opción</option>
                  {this.props.config.forms.map((form, idx) => (
                    <option key={idx} value={idx} >{form.title}</option>
                  ))}
                </select>
              }
            </div>
            <div className="cev-portlet__body">
              <div className="input-group">
                {this.state.form && (this.state.form.type === "form" || this.state.form.type === "form-select") &&
                  <Form
                    schema={this.state.form.schema}
                    uiSchema={this.state.form.uiSchema}
                    widgets={this.state.form.widgets}
                    fields={this.state.form.fields}
                    onSubmit={({ formData }, e) => { e.preventDefault(); this.onSubmit(formData, e) }}
                  >
                    <div>
                      <button type="submit" className="btn btn-success">Agregar</button>
                    </div>
                  </Form>
                }
                {this.state.form && this.state.form.type === "autocomplete" &&
                  <FormB.Group className="w-100 mb-10">
                    <InputGroup>
                      <Typeahead
                        id="autocompletado"
                        paginate={true}
                        defaultOpen={true}
                        options={this.state.form.data}
                        onChange={(s) => this.onChange({ selected: s })}
                      />
                      <InputGroup.Append>
                        <Button
                          onClick={() => this.onClick()}
                          variant="outline-secondary">
                          Agregar
                        </Button>
                      </InputGroup.Append>
                    </InputGroup>
                  </FormB.Group>
                }
                {this.state.form && this.state.form.type === "map" &&
                  <SIMMapForm
                    addQuery={this.props.addQuery}
                    onChange={(f) => { this.mapSelect(f) }}
                    onAdd={() => { this.mapAdd() }}
                  />
                }
              </div>
              <Alert variant="danger" show={alert}>Introduzca datos</Alert>
            </div>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    )
  }
}

const mapStateToProps = store => ({
  user: store.auth.user._id,
  showSearchBar: store.app.showSearch
});

export default connect(
  mapStateToProps
)(FormAdvancedSearchMenu);
