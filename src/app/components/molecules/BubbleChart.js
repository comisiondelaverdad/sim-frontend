import React, { Component } from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import * as app from "../../store/ducks/app.duck";
import * as d3 from 'd3'

const width = 800;
const height = 700;
const colors = ["#5867DD", "#58DDCE", "#4AB9FF",];

class BubbleChart extends Component {
    componentDidMount() {
        const vis = this;

        vis.svg = d3.select(vis.refs.d3_canvas)
            .attr("class", "bubble");

        vis.color = d3.scaleOrdinal()
            .range(colors);

        //vis.color = d3.scaleOrdinal(d3.schemeCategory10);
        vis.diameter = height;
        vis.data = {};

        this.props.pageTitle("Metadatos");
        this.props.pageSubtitle("Explora los metadatos");
    }

    componentDidUpdate() {
        const vis = this;


        if (this.props.buckets.length > 0 && this.props.buckets) {
            vis.svg.selectAll(".empty").remove();

            vis.data.children = this.props.buckets;

            //circle packing
            let bubble = d3.pack(vis.data)
                .size([vis.diameter, vis.diameter])
                .padding(10);

            //sumar todos los nodos
            let nodes = d3.hierarchy(vis.data)
                .sum(function (d) { return d.doc_count; });

            //data join
            let circles = vis.svg.selectAll("circle")
                .data(bubble(nodes).children)

            //transition
            circles.transition().duration(500)
                .attr("transform", (d) => `translate( ${d.x}, ${d.y})`)
                .style("fill", (d, i) => vis.color(i))
                .attr("r", (d) => 3 + d.r)

            //exit
            circles.exit()
                .transition().duration(200)
                .attr("r", 0)
                .remove()

            //enter
            circles.enter()
                .append("circle")
                .attr("transform", (d) => `translate( ${d.x}, ${d.y})`)
                .attr("r", 0)
                .style("fill", (d, i) => vis.color(i))
                .on('click', (d) => {

                    let fondoQuery = this.props.fondoSel === "Todos" ? "" : "%20AND%20record.origin.keyword%3A\"" + this.props.fondoSel + "\"";

                    let periodoQuery = this.props.periodoSel === "Todos" ? "" : "(record.temporalCoverage.start%3A%20%5B" + this.props.periodoSel.split("-")[0] + "-01-01%20TO%20" + this.props.periodoSel.split("-")[1] + "-01-01%5D)%20"

                    let url =
                        "/search?q=" +
                        periodoQuery +
                        "record." +
                        this.props.campoSel +
                        ".keyword%3A\"" +
                        d.data.key +
                        "\"" +
                        fondoQuery +
                        "&from=1";

                    this.props.filters({
                        "filters":{
                            "keyword": periodoQuery + "record." + this.props.campoSel + ".keyword%3A\"" + d.data.key + "\"" + fondoQuery,
                            "resourceGroup": {
                            "resourceGroupText": "",
                            "resourceGroupIDS": ""
                            },
                            "mapPolygon": ""
                        }
                        })

                    this.props.history.push(url);

                })
                .on("mouseover", (d) => {

                    var xPosition = parseFloat(d.x + d.r);
                    var yPosition = parseFloat(d.y);

                    //Update the tooltip position and value
                    let tooltip = d3.select("#tooltip")
                        .style("left", xPosition + "px")
                        .style("top", yPosition + "px")

                    tooltip.select("#value")
                        .text(() => {
                            if (d.data.doc_count > 1) {
                                return d.data.doc_count + " resultados"
                            } else {
                                return d.data.doc_count + " resultado"
                            }
                        });


                    tooltip.select("#name")
                        .text(d.data.key);

                    //Show the tooltip
                    d3.select("#tooltip").classed("hidden", false);
                })
                .on("mouseout", () => {
                    d3.select("#tooltip").classed("hidden", true);
                })
                .transition().duration(500)
                .attr("r", (d) => 3 + d.r)


            //count lables
            let countLabels = vis.svg.selectAll(".count")
                .data(bubble(nodes).children)

            countLabels.exit()
                .remove()

            countLabels
                .attr("transform", (d) => `translate( ${d.x}, ${d.y})`)
                .text((d) => d.data.doc_count)
                .attr("font-size", (d) => d.r / 2)

            countLabels.enter()
                .append("text")
                .classed("count", true)
                .attr("transform", (d) => `translate( ${d.x}, ${d.y})`)
                .style("text-anchor", "middle")
                .text((d) => d.data.doc_count)
                .attr("font-size", (d) => d.r / 2)
                .attr("fill", "white")

            //labels
            let labels = vis.svg.selectAll(".text")
                .data(bubble(nodes).children)

            labels.exit()
                .remove()

            labels
                .attr("transform", (d) => `translate( ${d.x}, ${d.y + d.r / 4})`)
                .text((d) => d.data.key.substring(0, 20))
                .attr("font-size", (d) => d.r / 6)

            labels.enter()
                .append("text")
                .classed("text", true)
                .attr("transform", (d) => `translate( ${d.x}, ${d.y + d.r / 4})`)
                .style("text-anchor", "middle")
                .text((d) => d.data.key.substring(0, 20))
                .attr("font-size", (d) => d.r / 6)
                .attr("fill", "white");

        } else {
            vis.svg.selectAll("*").remove();
            vis.svg.append("text")
                .classed("empty", true)
                .attr("transform", `translate( ${width / 2}, ${height / 2})`)
                .attr("font-size", width / 8)
                .attr("fill", "#5867DD")
                .attr("opacity", 0.7)
                .style("text-anchor", "middle")
                .text("No hay datos");
        }
    }

    shouldComponentUpdate(newProps) {

        if (newProps.buckets !== this.props.buckets) {
            return true;

        } else {
            return false;
        }
    }

    render() {

        return (
            <>
                <div id="tooltip" className="hidden">
                    <p><strong id="name" ></strong></p>
                    <p><span id="value"></span></p>
                </div>

                <svg ref="d3_canvas"
                    width={width}
                    height={height}
                    viewBox={`0 0 ${width} ${height}`}
                />
            </>
        )
    }
}

export default withRouter(connect(
  null,
  app.actions
)(BubbleChart));