import React, { useState, useEffect } from "react";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function DownloadResult(props) {

  const deleteKey = (key, object) => {
    if (object.hasOwnProperty(key)) delete object[key];
    return object;
  };

  const convert = (data, name = null) => {
    if (Array.isArray(data)) {
      data = data.join("|");
      // if (data.indexOf("-")>-1) data = data.replaceAll("-", " ");
      return data;
    }
    if (typeof data === "object" && data !== null) {
      data = Object.values(data);
      return data.join("|");
    }
    return data;
  };

  return (
    <div>
      {props.dataset && (
        <ExcelFile
          hideElement={true}
          filename={new Date().toLocaleString() + ""}
        >
          <ExcelSheet data={props.dataset} name="hoja">
            <ExcelColumn label="ident" value={(col) => convert(col.ident)} />
            <ExcelColumn label="url" value={(col) => convert(col.url)} />
            <ExcelColumn
              label="documentalId"
              value={(col) => convert(col.documentalId)}
            />
            <ExcelColumn
              label="documentType"
              value={(col) => convert(col.documentType)}
            />
            <ExcelColumn
              label="descriptionLevel"
              value={(col) => convert(col.descriptionLevel)}
            />
            <ExcelColumn label="title" value={(col) => convert(col.title)} />
            <ExcelColumn
              label="otherTitles"
              value={(col) => convert(col.otherTitles)}
            />
            <ExcelColumn
              label="authors"
              value={(col) => convert(col.authors)}
            />
            <ExcelColumn
              label="collaborators"
              value={(col) => convert(col.collaborators)}
            />
            <ExcelColumn label="area" value={(col) => convert(col.area)} />
            <ExcelColumn
              label="custodian"
              value={(col) => convert(col.custodian)}
            />
            <ExcelColumn label="rights" value={(col) => convert(col.rights)} />
            <ExcelColumn
              label="accessLevel"
              value={(col) => convert(col.accessLevel)}
            />
            <ExcelColumn
              label="sourceLocation"
              value={(col) => convert(col.sourceLocation)}
            />
            <ExcelColumn
              label="creationDate"
              value={(col) => convert(col.creationDate)}
            />
            <ExcelColumn
              label="description"
              value={(col) => convert(col.description)}
            />
            <ExcelColumn label="topics" value={(col) => convert(col.topics)} />
            <ExcelColumn
              label="mandate"
              value={(col) => convert(col.mandate)}
            />
            <ExcelColumn
              label="objective"
              value={(col) => convert(col.objetive)}
            />
            <ExcelColumn label="focus" value={(col) => convert(col.focus)} />
            <ExcelColumn
              label="milestone"
              value={(col) => convert(col.milestone)}
            />
            <ExcelColumn
              label="violenceType"
              value={(col) => convert(col.violenceType)}
            />
            <ExcelColumn
              label="temporalCoverage"
              value={(col) =>
                convert(deleteKey("description", col.temporalCoverage))
              }
            />
            <ExcelColumn label="geographicCoverage" value="" />
            <ExcelColumn
              label="conflictActors"
              value={(col) => convert(col.conflictActors)}
            />
            <ExcelColumn
              label="population"
              value={(col) => convert(col.population)}
            />
            <ExcelColumn
              label="occupation"
              value={(col) => convert(col.occupation)}
            />
            <ExcelColumn
              label="language"
              value={(col) => convert(col.language)}
            />
            <ExcelColumn
              label="associatedResources"
              value={(col) => convert(col.associatedResources)}
            />
            <ExcelColumn
              label="associatedResourceGroups"
              value={(col) => convert(col.associatedResourceGroups)}
            />
            <ExcelColumn label="notes" value={(col) => convert(col.notes)} />
          </ExcelSheet>
        </ExcelFile>
      )}
    </div>
  );
}
