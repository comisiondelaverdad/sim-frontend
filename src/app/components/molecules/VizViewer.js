import React, { Component } from "react";
import { connect } from "react-redux";
import Lottie from 'react-lottie';
import animationData from '../../../assets/viz-loading.json';
import * as RecordsService from "../../services/RecordsService";

class VizViewer extends Component {
  constructor () {
    super()

    this.state = {
      text: '',
      file: '',
      app: '',
      loading: true,
      filename: ''
    }
  }

  componentDidMount() {
    this.loadFile();
    this.setState({filename: this.props.record.extra.originalName.split('.').slice(0, -1).join('.').replace('_', '-')});
  }

  loadFile() {
    const id = this.props.record._id || this.props.record.idmongo;
    RecordsService.serviceFile(id).then((data) => {
      this.setState({file: data, loading: false});
      this.convertBlobToJSON();
    }).catch((err) => {
      if(err === 500)
        this.setState({file: '', loading: false});
    });
  }
  
  loadScript() {
    const isAlredyRegistered = customElements.get(this.state.filename);
    if (!isAlredyRegistered) {
      const script = document.createElement('script');
      script.setAttribute('id', 'viz-web-component-embedded');
      script.type = 'text/javascript';
      script.innerHTML = this.state.text;
      script.onload = function() {
        console.log("Script loaded and ready");
      };
      document.getElementsByTagName('head')[0].appendChild(script);
    }
  }

  convertBlobToJSON() {
    let reader = new FileReader();
    reader.onload = (evt) => {
      this.setState({ text: evt.target.result });
      const alreadyCreated = document.getElementById('viz-web-component-embedded');
      if (alreadyCreated === null) {
        this.loadScript();
        this.setState({app: React.createElement(this.state.filename, {}, null)});
      } else {
        alreadyCreated.remove();
        const viz = document.getElementsByTagName(this.state.filename)
        viz.length > 0 && viz[0].remove();
        this.setState({app: null});
        this.convertBlobToJSON();
      }
    }
    reader.readAsText(this.state.file);
  }

  render() {

    return (
      <>
        <div className="viz-viewer">
          {!this.state.loading &&
            (
              <div>
                <div className="description">
                  <h3>{this.props.metadata.title}</h3>
                  <p>{this.props.metadata.description}</p>
                </div>
                {this.state.app}
              </div>
            )
          }
          {this.state.loading &&
            (
              <div className="loading_viz">
                <Lottie height={150} width={150} options={
                  {
                    loop: true,
                    autoplay: true,
                    animationData: animationData,
                    rendererSettings: {
                      preserveAspectRatio: 'xMidYMid slice'
                    }
                  }
                } />
                <p>Cargando...</p>
            </div>
            )
          }
        </div>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps
)(VizViewer);