import React, {Component} from "react";
import { Card } from "reactstrap";
import { Button, Badge } from 'react-bootstrap';

class EntityItem extends Component {

  search(q){
    this.props.addQuery(`etiquetado:"${q}"`);
    //record.labelled_for_viz.entidades.text.keyword
  }

  render() {
    return (
      <>
        {this.props.data &&
          <Card className="mt-10">
            <div>
              <span className="cev-menu__link-badge">
                <span className={`mr-1 badge badge-primary`}>
                  {this.props.data.key}
                  <span className="cev-menu__link-badge">
                    <span className={`mr-1 badge`}>
                      {this.props.data.doc_count}
                    </span>
                  </span>
                </span>
              </span>

            </div>
            <div className="">
            {this.props.data.valores && this.props.data.valores.buckets &&
              this.props.data.valores.buckets.map((value, k)=>(
                <Button key={k} variant="light" onClick={()=>{this.search(value.key)}}>
                  {value.key.split("|")[1]} <Badge variant="secondary">{value.doc_count}</Badge>
                </Button>
              ))
            }
            </div>
          </Card>
        }
      </>
    );
  }

}

export default EntityItem
