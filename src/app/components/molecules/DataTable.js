import React, { Component } from "react";
import PropTypes from "prop-types";
import $ from "jquery";
import _ from "lodash";
import moment from "moment";

import "datatables.net";
import "datatables.net-bs4";
import "datatables.net-bs4/css/dataTables.bootstrap4.css";
import "datatables.net-responsive-bs4";
import "datatables.net-responsive-bs4/css/responsive.bootstrap4.css";
const INDEX_COLUMN_DATE = 8;
class DataTable extends Component {

  static propTypes = {
    data: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    isAdmin: PropTypes.bool.isRequired,
    isReviewer: PropTypes.bool.isRequired,
    isUser: PropTypes.bool.isRequired,
    showApproveModal: PropTypes.func,
    showApproveRenewModal: PropTypes.func,
    showDetails: PropTypes.func.isRequired,
    showPreApproveModal: PropTypes.func,
    showRejectModal: PropTypes.func,
    showRenewModal: PropTypes.func
  };

  static defaultProps = {
    isAdmin: false,
    isReviewer: false,
    isUser: false,
    showApproveModal: () => false,
    showApproveRenewModal: () => false,
    showDetails: () => false,
    showPreApproveModal: () => false,
    showRejectModal: () => false,
    showRenewModal: () => false
  };

  constructor(props) {
    super(props);
    $.fn.dataTable.Api.register('column().title()', function () {
      return $(this.header()).text().trim();
    });
    $.fn.dataTable.ext.search.push(function (settings, searchData, index, rowData, counter) {
      const minDate = $('#cev_datepicker_1').val();
      const maxDate = $('#cev_datepicker_2').val();
      const requestDate = new Date(searchData[INDEX_COLUMN_DATE]);
      if (minDate) {
        if (maxDate) {
          // filter by both
          return moment(requestDate).isBetween(minDate, maxDate);
        } else {
          // filter only by minDate
          return moment(requestDate).isAfter(minDate);
        }
      } else {
        if (maxDate) {
          // filter only by maxDate
          return moment(requestDate).isBefore(maxDate);
        } else {
          // no need to filter because both are undefined
          return true;
        }
      }
    });
  }

  componentDidMount() {
    const oThis = this;
    $(this.refs.main).DataTable({
      responsive: true,
      dom: `<'row'<'col-sm-12'<'table'tr>>>
      <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      ordering: false,
      searchDelay: 500,
      data: this.props.data,
      columns: this.props.columns,
      lengthMenu: [5, 10, 20],
      pageLength: 10,
      language: {
        emptyTable: "Ningún dato disponible en esta tabla",
        info: "Registros _START_ - _END_ de _TOTAL_",
        infoEmpty: "0 registros",
        infoFiltered: "(filtrado de _MAX_ registros)",
        lengthMenu: "Mostrar _MENU_ registros",
        zeroRecords: "No se encontraron resultados",
        paginate: {
          first: '<i class="la la-angle-double-left"></i>',
          last: '<i class="la la-angle-double-right"></i>',
          next: '<i class="la la-angle-right"></i>',
          previous: '<i class="la la-angle-left"></i>'
        }
      },
      initComplete: function () {
        const thisTable = this;
        const rowFilter = $('<tr class="filter"/>').appendTo($(thisTable.api().table().header()));

        this.api().columns().every(function () {
          let column = this;
          let input;

          if (!column.visible()) {
            return false;
          }

          switch (column.title()) {
            case 'ID Solicitud':
            case 'Nombre Usuario':
            case 'Correo Usuario':
            case 'Título':
              $(`<input type="text" class="form-control form-control-sm form-filter cev-input" data-col-index="${column.index()}"/>`).appendTo($('<th>').appendTo(rowFilter));
              break;
            case 'ID Recurso':
            case 'Fondo':
              input = $(`<select class="form-control form-control-sm form-filter cev-input" title="Select" data-col-index="${column.index()}"><option value="">Select</option></select>`);
              column.data().unique().sort().each(function (d) {
                $(input).append(`<option value="${d}">${d}</option>`);
              });
              input.appendTo($('<th>').appendTo(rowFilter));
              break;
            case 'Tipo':
              $(`<input type="text" class="form-control form-control-sm form-filter cev-input" data-col-index="${column.index()}"/>`).appendTo($('<th>').appendTo(rowFilter));
              break; 
            case 'Creador':
              $(`<input type="text" class="form-control form-control-sm form-filter cev-input" data-col-index="${column.index()}"/>`).appendTo($('<th>').appendTo(rowFilter));
              break;
            case 'Estado':
              const status = {
                pending: { 'title': 'Pendiente', 'class': 'cev-badge--primary' },
                validated: { 'title': 'Validada', 'class': 'cev-badge--primary' },
                approved: { 'title': 'Aprobada', 'class': 'cev-badge--success' },
                rejected: { 'title': 'Rechazada', 'class': 'cev-badge--danger' },
                renewal: { 'title': 'En renovación', 'class': 'cev-badge--primary' },
                expired: { 'title': 'Expirada', 'class': 'cev-badge--warning' },
                renewable: { 'title': 'Renovable', 'class': 'cev-badge--primary' }
              };
              input = $(`<select class="form-control form-control-sm form-filter cev-input" title="Select" data-col-index="${column.index()}"><option value="">Select</option></select>`);
              column.data().flatten().unique().sort().each(function (d) {
                $(input).append(`<option value="${d}">${status[d].title}</option>`);
              });
              input.appendTo($('<th>').appendTo(rowFilter));
              break;
            case 'Fecha de solicitud':
              $(`
                <div class="input-group date">
                    <input type="date" class="form-control form-control-sm cev-input" id="cev_datepicker_1" data-col-index="${column.index()}">
                </div>
                <div class="input-group date">
                    <input type="date" class="form-control form-control-sm cev-input" id="cev_datepicker_2" data-col-index="${column.index()}">
                </div>`).appendTo($('<th>').appendTo(rowFilter));
              break;
            case 'Acciones':
              let search = $(`<button class="btn btn-brand cev-btn btn-sm cev-btn--icon"><span><i class="la la-search"></i></span></button>`);
              let reset = $(`<button class="btn btn-secondary cev-btn btn-sm cev-btn--icon"><span><i class="la la-close"></i></span></button>`);

              $('<th>').append(search).append(reset).appendTo(rowFilter);

              $(search).on('click', function (e) {
                e.preventDefault();
                let params = {};
                $(rowFilter).find('.cev-input').each(function () {
                  let i = $(this).data('col-index');
                  params[i] = i === INDEX_COLUMN_DATE ? "" : $(this).val(); // important  
                });
                $.each(params, function (i, val) {
                  // apply search params to datatable
                  thisTable.api().column(i).search(val ? val : '', false, false);
                });
                thisTable.api().table().draw();
              });

              $(reset).on('click', function (e) {
                e.preventDefault();
                $(rowFilter).find('.cev-input').each(function (i) {
                  $(this).val('');
                  thisTable.api().column($(this).data('col-index')).search('', false, false);
                });
                thisTable.api().table().draw();
              });
              break;
            default:
              $('<th>').appendTo(rowFilter);
          }

          return true;
        });

        // hide search column for responsive table
        const hideSearchColumnResponsive = function () {
          thisTable.api().columns().every(function () {
            const column = this;
            if (column.responsiveHidden()) {
              $(rowFilter).find('th').eq(column.index()).show();
            } else {
              $(rowFilter).find('th').eq(column.index()).hide();
            }
            return true;
          })
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
      },
      columnDefs: [
        {
          targets: -1,
          title: 'Actions',
          orderable: false,
          render: function (data, type, full, meta) {
            if (oThis.props.isAdmin) {
              return `
              <span>
                <button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="cev_button_view_detail" title="Ver detalle de solicitud"><i class="la la-search"></i></button>
              </span>`
/*
              +
              (full.status.includes('validated') ?
              `<span>
                <button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="cev_button_approve" title="Aprobar solicitud"><i class="la la-check-circle"></i></button>
              </span>
              <span>
                <button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="cev_button_reject" title="Rechazar solicitud"><i class="la la-times-circle"></i></button>
              </span>`: ``) +
              (full.hasExtensions && full.isMostRecent ?
              `<span>
                <button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="cev_button_approve_renewal" title="Aprobar renovación"><i class="la la-check-square"></i></button>
              </span>` : ``);
*/            } else if (oThis.props.isReviewer) {
              return `
              <span>
                <button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="cev_button_view_detail" title="Ver detalle de solicitud"><i class="la la-search"></i></button>
              </span>`
/*
              +
              (full.status.includes('pending') ?
              `<span>
                <button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="cev_button_pre_approve" title="Pre-aprobar solicitud"><i class="la la-edit"></i></button>
              </span>` : ``);
*/            } else if (oThis.props.isUser) {
              return `
              <span>
                <button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="cev_button_view_detail" title="Ver detalle de solicitud"><i class="la la-search"></i></button>
              </span>`

                +
                (full.status.includes('renewable') ?
                  `<span>
                <button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="cev_button_request_renewal" title="Solicitar renovación"><i class="la la-refresh"></i></button>
              </span>` : ``);
              /**/
} else {
              return `<span></span>`;
            }
          },
          createdCell: function (cell, cellData, rowData, rowIndex) {
            $(cell).find('#cev_button_view_detail').on('click', function (e) {
              e.preventDefault();
              oThis.props.showDetails(rowIndex);
            });
            $(cell).find('#cev_button_pre_approve').on('click', function (e) {
              e.preventDefault();
              oThis.props.showPreApproveModal(rowIndex);
            });
            $(cell).find('#cev_button_approve').on('click', function (e) {
              e.preventDefault();
              oThis.props.showApproveModal(rowIndex);
            });
            $(cell).find('#cev_button_reject').on('click', function (e) {
              e.preventDefault();
              oThis.props.showRejectModal(rowIndex);
            });
            $(cell).find('#cev_button_approve_renewal').on('click', function (e) {
              e.preventDefault();
              oThis.props.showApproveRenewModal(rowIndex);
            });
            $(cell).find('#cev_button_request_renewal').on('click', function (e) {
              e.preventDefault();
              oThis.props.showRenewModal(rowIndex);
            });
          }
        },
        {
          targets: 2,
          width: '150px',
        },
        {
          targets: 3,
          width: '150px',
          render: function (data, type) {
            if (type === 'display') {
              return `<a href="/detail/${data}">${data}</a>`;
            }
            return data;
          }
        },
        {
          targets: 4,
          width: '150px',
          render: function (data) {
            return data ? data : '-';
          }
        },
        {
          targets: 6,
          width: '150px',
          render: function (data) {
            return data ? data : '-';
          }
        },
        {
          targets: 7,
          width: '150px',
          render: function (data) {
            return data ? data : '-';
          }
        },
        {
          targets: INDEX_COLUMN_DATE,
          render: function (data, type) {
            if (type === 'display') {
              return moment(data).local().format('LL');
            }
            return data;
          }
        },
        {
          targets: 9,
          render: function (data, type) {
            const status = {
              pending: { 'title': 'Pendiente', 'class': 'cev-badge--primary' },
              validated: { 'title': 'Validada', 'class': 'cev-badge--primary' },
              approved: { 'title': 'Aprobada', 'class': 'cev-badge--success' },
              rejected: { 'title': 'Rechazada', 'class': 'cev-badge--danger' },
              renewal: { 'title': 'En renovación', 'class': 'cev-badge--primary' },
              expired: { 'title': 'Expirada', 'class': 'cev-badge--warning' },
              renewable: { 'title': 'Renovable', 'class': 'cev-badge--primary' }
            };
            if (type === 'display') {
              return data.map(d => {
                if (status[d]) {
                  return `<span class="cev-badge ${status[d].class} cev-badge--inline cev-badge--pill">${status[d].title}</span>`;
                } else {
                  return d;
                }
              }).join('');
            }
            return data;
          }
        },
        {
          targets: 10,
          render: function (data) {
            const accessLevel = {
              1: { 'badgeClass': 'cev-badge--danger', 'fontClass': 'cev-font-danger' },
              2: { 'badgeClass': 'cev-badge--primary', 'fontClass': 'cev-font-primary' },
              3: { 'badgeClass': 'cev-badge--success', 'fontClass': 'cev-font-success' }
            };
            if (accessLevel[data]) {
              return `
              <span>
                <span class="cev-badge ${accessLevel[data].badgeClass} cev-badge--dot"></span>&nbsp;
                <span class="cev-font-bold ${accessLevel[data].fontClass}">${data}</span>
              </span>`;
            } else {
              return data;
            }
          }
        }
      ]
    });
  }

  componentDidUpdate() {
    $(this.refs.main).DataTable().clear();
    $(this.refs.main).DataTable().rows.add(this.props.data);
    $(this.refs.main).DataTable().draw();
  }

  componentWillUnmount() {
    $(this.refs.main).DataTable().destroy(true);
  }

  shouldComponentUpdate(nextProps) {
    return !_.isEqual(nextProps.data, this.props.data);
  }

  render() {
    return (
      <div className="cev-datatable cev-datatable--default cev-datatable--brand cev-datatable--loaded">
        <table className="table table-striped table-bordered table-hover dt-responsive table-checkable" id="cev_table_1" ref="main" style={{ width: '100%' }}></table>
      </div>);
  }

}

export default DataTable;
