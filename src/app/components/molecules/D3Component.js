import React , {Component} from 'react';
import * as d3 from 'd3'

/**
 * Bloque básico para generar una visualización en D3
 * 
 * @version 0.1
 */

export default class D3Component extends Component {
    constructor(props){
        super(props)
    }

    componentDidMount(){
        this.svg = d3.select(this.refs.d3_canvas)
    }

    render(){
        return(
            <>
            <svg
              ref="d3_canvas"
              width="600"
              height="650"
              viewBox="0 0 600 650"
            />
            </>
        )
    }
}