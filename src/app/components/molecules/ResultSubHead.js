import React, { useEffect, useState } from "react"

export default function ResultSubHead(props){
    const [validate, setValidate] = useState(false);
    const [value, setValue] = useState(0);

    useEffect(() => {
        if(props.type && (props.type === "Date" || props.type === "DateRange")){
            try{
                setValidate(true);
                setValue( props.type === "DateRange" ? new Date(props.description[0]).toLocaleDateString("es-CO") + " - " + new Date(props.description[1]).toLocaleDateString("es-CO") : new Date(props.description).toLocaleDateString("es-CO"));
            }
            catch(e){
                setValidate(false);
                setValue("");
            }
        }else{
            setValue(props.description);
            setValidate(true);
        }
        
    }, [props.description, props.type]);

    return(
        <>
            {validate &&
                <span><i className={props.icon}></i>{value}</span>
            }
        </>
    );

}