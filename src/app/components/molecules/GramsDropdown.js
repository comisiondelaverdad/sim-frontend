import React from 'react';
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
import { ORIGIN_MAP } from "../../config/const";

function GramsDropdown ({fondos, fondoSelected}) {
    
    let dropdownItems = [];
    for (var i = 0; i < fondos.length; i++) {
        let fondo = fondos[i];

        dropdownItems.push(
        <Dropdown.Item 
          key={i + fondo.key}
          onSelect={() => fondoSelected(fondo.key)}
        > 
          {ORIGIN_MAP[fondo.key] ? ORIGIN_MAP[fondo.key] : fondo.key} 
        </Dropdown.Item>
        );
      }

    return (
        
    <DropdownButton id="dropdown-fondo-button" title="Selecciona un grado">
       {dropdownItems}
    </DropdownButton>
    );
}

export default GramsDropdown;
