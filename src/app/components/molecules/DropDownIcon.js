import React from "react"

const DropDownIcon = React.forwardRef((props, ref) => (
  <div
    ref={ref}
    onClick={e => {
      e.preventDefault();
      props.onClick(e);
    }}
    onMouseOver={e => {
      e.preventDefault();
      if(props.onMouseOver){
        props.onMouseOver(e);
      }
    }}
  >
    {props.children}
  </div>
));

export default DropDownIcon;