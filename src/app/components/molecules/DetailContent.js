import React, { Component, Suspense } from "react";
import ConfirmMenu from "../organisms/ConfirmMenu";
import VizSelect from '../organisms/VizSelect';

const debounce = (fn, ms = 0) => {
  let timeoutId;
  return function wrapper(...args) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => fn.apply(this, args), ms);
  };
};

class DetailContent extends Component {
  
  handleSync = debounce((ps) => {
    ps.update();
  }, 1000)

  render() {
    const DetailContent = React.lazy(() => import("../organisms/" + this.props.content));
    const EditTitle = React.lazy(() => import("../organisms/" + this.props.editTitle));
    const CollapseContent = React.lazy(() => import("../organisms/" + this.props.collapseContent));

    return (
      <>
        <div className={`row ${(this.props.display === `cev-${this.props.item}` ? '' : 'hidden')}`}>
          <div className="col-xl-12" >
              {this.props.showTitle && 
                <div className="cev-portlet__head">
                  <div className="cev-portlet__head-label">
                    <h3 className="cev-portlet__head-title">{this.props.title}</h3>
                  </div>
                  <div className="cev-portlet__head-toolbar">
                    {this.props.showCollapse && this.props.collapseContent && this.props.actions.collapseAction &&
                      <>
                        {!this.props.collapse &&
                          <button href="#" className="btn btn-brand btn-icon-md" onClick={this.props.actions.collapseAction}>
                            <i className="la la-angle-down"/>
                            <span>Ver {this.props.collapseText}</span>
                          </button>
                        }
                        {this.props.collapse &&
                          <button className="btn btn-brand btn-icon-md" onClick={this.props.actions.collapseAction}>
                            <i className="la la-angle-down"/>
                            <span>Ocultar {this.props.collapseText}</span>
                          </button>
                        }
                      </>
                    }
                    {this.props.editTitle && this.props.showEditTitle && this.props.actions.updateLabel &&
                        <EditTitle label={this.props.title} updateLabel={this.props.actions.updateLabel}/>
                    }
                    {this.props.showDeleteTitle && this.props.actions.deleteLabel &&
                      <ConfirmMenu
                        title="Eliminar la etiqueta"
                        message={"¿Está seguro desea la etiqueta: "+this.props.title + "?"}
                        confirmAction={()=>{this.props.actions.deleteLabel(this.props.keyword,this.props.records)}}
                      />
                    }
                  </div>
                </div>
              }
              <div className="cev-portlet__body cev-content">

                {this.props.content === 'Bookmarks' &&
                  <VizSelect
                    bookmarks={this.props.records}
                    content={this.props.content}
                  />
                }

                {this.props.showScrollBar ? (
                  <div className="cev-section cev-section--first">
                      <div className="cev-section__body">
                        {this.props.collapse &&
                          <div className="bg-white cev-p-20 cev-margin-b-20">
                            <Suspense fallback={"Loading ..."}>
                              <CollapseContent records={this.props.collapseItem}></CollapseContent>
                            </Suspense>
                          </div>

                        }
                        <Suspense fallback={"Loading ..."}>
                          <DetailContent
                            records={this.props.records}
                            keyword={this.props.keyword}
                            showDelete={this.props.showDelete}
                            actions={this.props.actions}
                            extra={this.props.extra}
                          />
                        </Suspense>
                      </div>
                    </div>
                ) :(
                    <div className="cev-section cev-section--first">  
                      <div className="cev-section__body">
                        {this.props.collapse &&
                          <div className="bg-white cev-p-20 cev-margin-b-20">
                            <Suspense fallback={"Loading ..."}>
                              <CollapseContent records={this.props.collapseItem}></CollapseContent>
                            </Suspense>
                          </div>
                        }
                        <Suspense fallback={"Loading ..."}>
                          <DetailContent
                            records={this.props.records}
                            keyword={this.props.keyword}
                            showDelete={this.props.showDelete}
                            actions={this.props.actions}
                            extra={this.props.extra}
                          />
                        </Suspense>
                      </div>
                    </div>
                  )}
              </div>
            </div>
          </div>
      </>
    );
  }
}

export default DetailContent;