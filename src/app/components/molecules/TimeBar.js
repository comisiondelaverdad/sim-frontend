import React, { Component } from 'react';
import * as d3 from 'd3'

/**
 * Bloque que dibuja una lina de tiempo interactiva
 * 
 * @version 0.1
 */

export default class TimeBar extends Component {

  componentDidMount() {
    this.svg = d3.select(this.refs.d3_canvas)
    this.updateViz()
  }

  updateViz() {

    let width = 550
    let height = 200
    let color = "steelblue"
    let margin = { bottom: 10, top: 10, left: 10, right: 10 }

    let x = d3.scaleBand()
      .domain(d3.range(this.props.data.length))
      .range([margin.left, width - margin.right])
      .padding(0.1)

    let y = d3.scaleLinear()
      .domain([0, d3.max(this.props.data, d => d.value)]).nice()
      .range([height - margin.bottom, margin.top])

    let xAxis = g => g
      .attr("transform", `translate(0,${height - margin.bottom - 10})`)
      .style("font-size", `8px`)
      .call(d3.axisBottom(x).tickFormat(i => this.props.data[i].name).tickSizeOuter(0))

    const toolTip = d3.select("body").append("div")
      .attr("class", "d3-tooltip")
      .style("opacity", 0);

    this.svg
      .attr("viewBox", [0, 0, width - 10, height]);

    this.svg.append("g")
      .attr("fill", color)
      .attr("transform", `translate(0,-10)`)
      .selectAll("rect")
      .data(this.props.data)
      .join("rect")
      .attr("x", (d, i) => x(i))
      .attr("y", d => y(d.value))
      .attr("height", d => y(0) - y(d.value))
      .attr("width", x.bandwidth())
      .on("mouseover", (d) => {
        toolTip.transition()
          .duration(200)
          .style("opacity", .9);
        toolTip.html("Año: " + d.name + "<br/>Registros: " + d.data)
          .style("left", (d3.event.pageX) + "px")
          .style("top", (d3.event.pageY - 28) + "px");
      })
      .on("mouseleave", (d) => {
        toolTip.transition()
          .duration(500)
          .style("opacity", 0);
      });

    this.svg.append("g")
      .call(xAxis);

    /*this.svg.append("g")
        .call(yAxis);*/

    return this.svg.node();


  }

  render() {
    return (
      <>
        <svg
          ref="d3_canvas"
          width="550"
          height="200"
          viewBox="0 0 630 200"
        />
      </>
    )
  }
}
