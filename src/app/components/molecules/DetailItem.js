import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { toAbsoluteUrl } from "../../../theme/utils";
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';

class DetailItem extends Component {
  render() {
    return (
        <ListItem alignItems="flex-start">
          {(this.props.type === "item" || this.props.type === "subitem") &&

            <ListItemButton
              className={"widget__item " + (this.props.display === "cev-" + this.props.item ? "widget__item--active" : "")}
              role={"button"}
              onClick={(evt) => this.props.loadTab(evt, "cev-" + this.props.item)}
              name={"cev-" + this.props.item}
              href={`#${this.props.item}`}
            >
              <span className={"cev-widget__section cev-text-justify " + (this.props.type === "subitem" ? "padding-l-30" : "")}>
                {this.props.icon2 &&
                  <svg className="cv-small">
                    <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-" + this.props.icon2)} className="flia-green"></use>
                  </svg>
                }
                {this.props.icon &&
                  <span className="cev-widget__icon cev-font-success">
                    <i className={this.props.icon} />
                  </span>
                }
                <span className="cev-widget__desc">
                  {this.props.title}
                </span>
              </span>
              {this.props.badge &&
                <span className="cev-badge cev-badge--success cev-badge--md cev-badge--rounded">
                  {this.props.badge}
                </span>
              }
            </ListItemButton>
          }
          {this.props.type === "title" &&
            <span className="cev-nav__link-title">{this.props.title}</span>
          }
          {this.props.type === "link" &&
            <Link to={`/resources/resource/${this.props.item}`}>
              <span
                className={"cev-widget__item " + (this.props.display === "cev-" + this.props.item ? "cev-widget__item--active" : "")}
                role={"button"}
                name={"cev-" + this.props.item}
              >
                <span className={"cev-widget__section cev-text-justify " + (this.props.type === "subitem" ? "cev-padding-l-30" : "")}>
                  {this.props.icon2 &&
                    <svg className="cv-small">
                      <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-" + this.props.icon2)} className="flia-green"></use>
                    </svg>
                  }
                  {this.props.icon &&
                    <span>
                      <i className={this.props.icon} />
                    </span>
                  }
                  <span className="cev-widget__desc">
                    {this.props.title}
                  </span>
                </span>
              </span>
            </Link>
          }
        </ListItem>

    );
  }
}

export default DetailItem;
