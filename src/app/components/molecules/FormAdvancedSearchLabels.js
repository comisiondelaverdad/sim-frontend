import React from "react";
import { connect } from "react-redux";
import { Dropdown } from "react-bootstrap";
import DropDownIcon from "../molecules/DropDownIcon";
import * as FormService from "../../services/FormService";
import TesauroSearch from './TesauroSearch';
import _ from "lodash";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { toAbsoluteUrl } from "../../../theme/utils";
import PerfectScrollbar from "react-perfect-scrollbar";
import LoyaltyIcon from '@material-ui/icons/Loyalty';
import CloseIcon from '@material-ui/icons/Close';

const debounce = (fn, ms = 0) => {
  let timeoutId;
  return function wrapper(...args) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => fn.apply(this, args), ms);
  };
};

class FormAdvancedSearchLabels extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newLabel: '',
      show: false,
      alert: false,
      form: undefined,
      isLoading: false,
    };
  }

  componentDidMount() {
    this.setState({ oldLabel: this.props.label, newLabel: this.props.label, show: false });
  }

  handleSync = debounce((ps) => {
    ps.update();
  }, 1000)

  selectForm(event) {
    let f = this.props.config.forms[event.target.value]
    if (f !== undefined) {
      if (f.type === 'form-select') {
        FormService.getOptions(f.field)
          .then(json => {
            if (json.aggregations.options.buckets.length > 0) {
              f.schema.properties[f.name].enum = _.map(json.aggregations.options.buckets, 'key')
            } else {
              f.schema.properties[f.name].enum = ["Sin Registros"]
            }
            f.schema.properties[f.name].enum = f.schema.properties[f.name].enum.sort()
            this.setState({ form: f })
          });
      } else {
        this.setState({ form: f })
      }
    } else {
      this.setState({ form: null })
    }
  }

  onSubmit(formData, e) {
    e.preventDefault()
    let query = this.state.form.queryAlias ? this.state.form.queryAlias : this.state.form.query
    if (typeof formData === "object") {
      for (const prop in formData) {
        if (formData[prop] !== "Sin Registros") {
          query = query.split("[" + prop + "]").join(formData[prop]);
        }
      }
    } else {
      query = formData
    }
    if (query !== this.state.form.query) {
      this.setState({ "query": query, "formData": formData, show: false, form: undefined }, this.props.addQuery(query))
    }
  }

  asyncSearch(query) {
    this.setState({ isLoading: true });
    FormService.getAsyncOptions(this.state.form, query)
      .then(json => {
        this.setState({
          isLoading: false,
          options: json.items,
        })
      });
  }

  mapSelect(geojsonData) {
    this.setState({ "mapData": geojsonData })
  }

  mapAdd() {
    let q = ""
    let comma = ""
    _.map(this.state.mapData.features, (v, k) => {
      q += comma + v.geometry.type[0] + "_"
      let comma2 = ""
      _.map(v.geometry.coordinates, (w, i) => {
        q += comma2 + w
        comma2 = "-"
      })
      comma = "|"
    })
    this.setState({ show: false, form: undefined }, this.props.addMap(this.state.mapData, q))
  }


  render() {
    const {
      show,
    } = this.state;

    let classDropdown = this.props.showSearchBar ? "" : "cev-advancedform";

    return (
      <Dropdown
        drop="down"
        show={show}
        onToggle={() => { this.setState({ show: false }) }}
      >
        <Dropdown.Toggle
          as={DropDownIcon}
          onClick={() => { this.setState({ show: true }) }}
        >
          <button type="button" className="btn-small ml-2 btn btn-primary" title="Búsqueda por etiquetas">
            <LoyaltyIcon></LoyaltyIcon>
          </button>
        </Dropdown.Toggle>
        <Dropdown.Menu flip={"false"} className={`dropdown-menu-fit dropdown-menu-left dropdown-menu-anim dropdown-menu-xl ${classDropdown} advanceSearch`}>
          <div className="cev-portlet">
            <div className="cev-portlet__head-title">
              <h3>Búsqueda por etiquetas</h3>
              <button type="button" className="btn-small ml-2 btn btn-danger" onClick={() => { this.setState({ show: false }) }}>
                <CloseIcon></CloseIcon>
              </button>
            </div>
            <div className="cev-portlet__body">
              <PerfectScrollbar
                className="cev-scroll"
                options={{
                  wheelSpeed: 1,
                  wheelPropagation: false
                }}
                style={{ height: "300px" }}
                data-scroll="true"
                onSync={this.handleSync}
              >
                <div className="cev-portlet">
                  <TesauroSearch addQuery={this.props.addQuery} />
                </div>
              </PerfectScrollbar>
            </div>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    )
  }
}

const mapStateToProps = store => ({
  user: store.auth.user._id,
  showSearchBar: store.app.showSearch
});

export default connect(
  mapStateToProps
)(FormAdvancedSearchLabels);
