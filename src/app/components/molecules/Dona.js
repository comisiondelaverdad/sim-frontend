import React, {
    Component
} from 'react';
import * as d3 from 'd3'

/**
 * Bloque básico para generar una dona en d3
 * 
 * @version 0.1
 */

export default class Arbol extends Component {
    
    componentDidMount() {
        console.log(this.props.data)
        this.svg = d3.select(this.refs.d3_canvas).append('g')

        this.updateViz()
    }

    componentDidUpdate() {

    }

    updateViz() {
        const margin = {
            top: 10,
            right: 10,
            bottom: 10,
            left: 10,
        },
            width = 1000 - margin.left - margin.right,
            height = width - margin.top - margin.bottom,
            radius = Math.min(width, height) / 3 - margin.left,
            color = d3.scaleOrdinal(d3.schemePaired)


        let data = []

        data = this.props.data.buckets.map(d => {
            const obj = {
                name: d.key,
                value: d.doc_count
            }

            return obj
        })

        const labels = data.map(d => d.name)

        const pie = d3.pie()
            .padAngle(0.005)
            .sort(null)
            .value(d => d.value)

        const arc = d3.arc()
            .innerRadius(radius * 0.6)
            .outerRadius(radius - 1)

        const size = 12;
        const arcs = pie(data);

        var arcGroups = this.svg.selectAll(".arc")
            .data(arcs)
            .enter()
            .append("g")
            .attr('transform', `translate(${1000 / 2}, ${1000 / 2})`)
            .attr("class", "arc")

        arcGroups.selectAll(".donut-arc")
            .data(arcs)
            .enter()
            .append("path")
            .attr('class', 'donut-arc')
            .attr("fill", d => color(d.data.name))
            .attr("d", arc)
        // .on("mouseenter", this.circleMouseover)
        // .on("mouseout", this.circleMouseout);

        this.svg.selectAll(".donut-label")
            .data(arcs)
            .enter()
            .append("text")
            .attr("transform", (d) => `translate(${arc.centroid(d)}) translate(${1000 / 2}, ${1000 / 2})`)
            .attr('class', 'donut-label')
            .attr("text-anchor", "middle")
            .text((d) => d.value)


        this.svg.selectAll('.legend-rect')
            .data(labels)
            .enter()
            .append('rect')
            .attr('class', 'legend-rect')
            .attr('transform', `translate(${1000 / 2}, ${1000 / 2})`)
            .attr('x', width / 3)
            .attr('y', (d, i) => -50 + i * (size + 5))
            .attr('width', size)
            .attr('height', size)
            .style('fill', (d, i) => color(d));

        this.svg.selectAll('.label-pie')
            .data(labels)
            .enter()
            .append('text')
            .attr('class', 'label-pie')
            .attr('transform', `translate(${1000 / 2}, ${1000 / 2})`)
            .attr('x', width / 3 + size * 1.2)
            .attr('y', (d, i) => -50 + i * (size + 5) + size / 2)
            .text(d => d)
            .style('alignment-baseline', 'middle');
    }

    circleMouseover(event, d) {
        var xPosition = parseFloat(event.clientX) + 5;
        var yPosition = parseFloat(event.clientY) + 2;

        let tooltip = d3.select(".tooltip-dona")
            .style("left", xPosition + "px")
            .style("top", yPosition + "px")

        tooltip.select(".key")
            .text(d.data.x)

        tooltip.select(".value")
            .text(d.data.value)

        tooltip.classed("hidden", false);
    }

    circleMouseout() {
        d3.select(".tooltip-dona").classed("hidden", true);
    }


    render() {
        return (<>
            <svg ref="d3_canvas"
                viewBox="0 0 1000 1000" />
        </>
        )
    }
}