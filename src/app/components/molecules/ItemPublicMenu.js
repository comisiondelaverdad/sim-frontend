import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function ItemPublicMenu(props) {
  const [item, setItem] = useState();

  useEffect(() => {
    if (props.item && !props.item.img) {
      const new_item = () => {
        
        return (
          <li
            className={`cev-menu__item  ${props.active} cev-menu__item--rel`}
            aria-haspopup="true"
            data-ktmenu-submenu-toggle="hover"
          >
            <Link
              onClick={() => {
                props.set_active(props.item.id);
              }}
              to={`/${props.item.route || ""}`}
              className={` cev-menu__link `}
              target = {props.item.tab ? '_blank' : ''}
              
            >
              <>
                <span className="cev-menu__item-here" />
                <span className="cev-menu__link-text">{props.item.tag}</span>
              </>
            </Link>
          </li>
        );
      };

      setItem(new_item);
    }
  }, [props.active]);

  return <>{item && item}</>;
}

export default ItemPublicMenu;
