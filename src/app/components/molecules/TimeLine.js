import React, { Component } from 'react';
import * as d3 from 'd3'

/**
 * Bloque que dibuja un mapa de calor
 * 
 * @version 0.1
 */

export default class TimeLine extends Component {
    componentDidMount() {
        this.svg = d3.select(this.refs.d3_canvas)
        this.updateViz()
    }

    updateViz() {
        const domain = []
        this.props.timeline.forEach(d => {
            domain.push(d.entidad)
        })

        let valueMax = d3.max(this.props.timeline, d => d3.max(d.years, e => e.value))
        let rectW = (950 - 200) / (this.props.years.length)

        const categoricalScale = d3.scaleBand().range([50, 750]).domain(domain)
        const color = d3.scaleOrdinal(d3.schemePaired)
        const opacity = d3.scaleLinear().range([0, 1]).domain([0, valueMax])
        // const time = d3.scaleLinear().range([200,950]).domain([new Date(min+"-01-01T00:00"), new Date(max+"-01-01T00:00")])
        const time = d3.scaleBand().range([200, 950]).domain(this.props.years)

        for (var i = 0; i < domain.length + 1; i++) {
            this.svg.append('line')
                .attr('x1', 0)
                .attr('x2', 950)
                .attr('y1', i * categoricalScale.step() + 50)
                .attr('y2', i * categoricalScale.step() + 50)
                .attr('stroke', '#ececec')
        }

        for (var j = 0; j < this.props.years.length; j++) {
            const line = this.svg.append('g')
                .attr('transform', `translate(${j * rectW + 200} 0)`)

            line.append('line')
                .attr('x1', 0)
                .attr('x2', 0)
                .attr('y1', 50)
                .attr('y2', 750)
                .attr('stroke', '#ececec')

            line.append('text')
                .attr('transform', `translate(${rectW / 2} 45) rotate(-45)`)
                .attr('fill', '#555')
                .attr('dominant-baseline', 'central')
                .text(parseInt(this.props.years[j]))
        }

        const names = this.svg.selectAll('.block')
            .data(this.props.timeline)
            .join('g')
            .attr('class', 'block')
            .attr('transform', d => `translate(0 ${categoricalScale(d.entidad)})`)

        names.append('text')
            .text(d => d.entidad)
            .attr('fill', '#000')
            .attr('y', 15)

        const years = names.selectAll('g').data(d => d.years)
            .join('g')
            .attr('transform', d => `translate(${time(d.year)} 0)`)

        years.append('rect')
            .attr('width', () => rectW)
            .attr('height', () => categoricalScale.step())
            .attr('style', 'transition: all 1s ease')
            .attr('fill', d => {
                return color(d.name)
            })
            .attr('opacity', d => opacity(d.value))
    }

    render() {
        return (
            <>
                <svg
                    ref="d3_canvas"
                    width="1000"
                    height="800"
                    viewBox="0 0 1000 800"
                />
            </>
        )
    }
}