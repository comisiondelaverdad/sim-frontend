import React, { Component, useState } from "react";
import PropTypes from "prop-types";
import $ from "jquery";
import "jstree/dist/jstree.min";
import "jstree/dist/themes/default/style.css";
import Swal from "sweetalert2";

let original_data = null;

class TreeView extends Component {
  constructor(props) {
    super(props);
    this.state = { toast: true };
  }

  static propTypes = {
    treeData: PropTypes.object.isRequired,
    onChange: PropTypes.func,
    onNodeDeselected: PropTypes.func,
    onNodeOpened: PropTypes.func,
    onNodeSelected: PropTypes.func,
    newSelected: PropTypes.object,
    depth: PropTypes.number,
    lazy: PropTypes.bool,
  };

  static defaultProps = {
    onChange: () => false,
    onNodeDeselected: () => false,
    onNodeOpened: () => false,
    onNodeSelected: () => false,
    newSelected: { old: "", new: "" },
    depth: 0,
  };

  shouldComponentUpdate(nextProps) {
    if (nextProps.newSelected.new !== "") {
      $(this.treeContainer)
        .jstree(true)
        .deselect_node(nextProps.newSelected.old);
      $(this.treeContainer)
        .jstree(true)
        .select_node(nextProps.newSelected.new);
    }
    return nextProps.treeData !== this.props.treeData;
  }

  componentDidMount() {
    this.setState({ toast: true });
    const { treeData, lazy } = this.props;
    console.log(treeData);
    if (treeData) {
      $(this.treeContainer).jstree(treeData);
      if (this.props.resourcegroup) {
        $(this.treeContainer).on("dblclick.jstree", (e) => {
          let data = original_data;
          this.props.onNodeSelected(e, data);
          if (data.selected.length > 0 && $(this.treeContainer).jstree(true)) {
            data.path = {
              texts: $(this.treeContainer)
                .jstree(true)
                .get_path(data.selected[0]),
              ids: $(this.treeContainer)
                .jstree(true)
                .get_path(data.selected[0], false, true),
            };
          }
          this.props.onChange(e, data);
        });
        $(this.treeContainer).on("select_node.jstree", (e, data) => {
          original_data = data;
          if ($(data.event.target.parentElement).hasClass("jstree-leaf")) {
            this.props.onNodeSelected(e, data);
            if (
              data.selected.length > 0 &&
              $(this.treeContainer).jstree(true)
            ) {
              data.path = {
                texts: $(this.treeContainer)
                  .jstree(true)
                  .get_path(data.selected[0]),
                ids: $(this.treeContainer)
                  .jstree(true)
                  .get_path(data.selected[0], false, true),
              };
            }
            this.props.onChange(e, data);
          } else {
            $(this.treeContainer)
              .jstree(true)
              .open_node(data.node.id);
            this.props.onNodeSelected(e, data);

            if (this.state.toast) {
              Swal.fire({
                title: "Recuerda dar doble click para ir al enlace del fondo",
                target: "#container_fondos",
                showCloseButton: true,
                showConfirmButton: false,
                customClass: {
                  container: "swal-black",
                },
                toast: true,
                position: "center-end",
              });
            }
            this.setState({toast: false})
          }
        });
      } else {
        $(this.treeContainer).on("changed.jstree", (e, data) => {
          if (data.selected.length > 0 && $(this.treeContainer).jstree(true)) {
            data.path = {
              texts: $(this.treeContainer)
                .jstree(true)
                .get_path(data.selected[0]),
              ids: $(this.treeContainer)
                .jstree(true)
                .get_path(data.selected[0], false, true),
            };
          }
          this.props.onChange(e, data);
        });
        $(this.treeContainer).on("select_node.jstree", (e, data) => {
          $(this.treeContainer)
            .jstree(true)
            .open_node(data.node.id);
          this.props.onNodeSelected(e, data);
        });
      }

      $(this.treeContainer).on("deselect_node.jstree", (e, data) => {
        this.props.onNodeDeselected(e, data);
      });
      $(this.treeContainer).on("after_open.jstree", (e, data) => {
        this.props.onNodeOpened(e, data);
      });
      $(this.treeContainer).on("ready.jstree", (e, data) => {
        if (this.props.depth > 0 && $(this.treeContainer).jstree(true)) {
          if (!lazy) {
            this.expandNode(
              $(this.treeContainer)
                .jstree(true)
                .get_node("#"),
              this.props.depth
            );
          }
        }
      });
      $(this.treeContainer).on("dnd_move.vakata.jstree", (e, data) => {
        var nodeType = $(data.rslt.o).attr("rel");
        var parentType = $(data.rslt.np).attr("rel");

        if (nodeType && parentType) {
          console.log(nodeType);
          console.log(nodeType);
        }
      });
    }
  }

  expandNode(node, depth) {
    var treeContainer = $(this.treeContainer);
    var DOM = this;
    treeContainer.jstree(true).open_node(node, function() {
      let newDepth = depth - 1;
      if (depth >= 0) {
        let newNode = treeContainer.jstree(true).get_node(node);
        for (var i = 0; i < newNode.children.length; i++) {
          DOM.expandNode(
            treeContainer.jstree(true).get_node(newNode.children[i]),
            newDepth
          );
        }
      }
    });
    depth--;
    if (depth >= 0) {
      for (var i = 0; i < node.children.length; i++) {
        this.expandNode(
          treeContainer.jstree(true).get_node(node.children[i]),
          depth
        );
      }
    }
  }

  componentDidUpdate() {
    const { treeData } = this.props;
    if (treeData) {
      $(this.treeContainer).jstree({
        core: { check_callback: true },
        plugins: ["dnd"],
      });
      $(this.treeContainer).jstree(true).settings = treeData;
      $(this.treeContainer)
        .jstree(true)
        .refresh();
    }
  }

  render() {
    return (
      <div>
        <div ref={(div) => (this.treeContainer = div)} />
      </div>
    );
  }
}

export default TreeView;
