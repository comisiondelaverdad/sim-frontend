import React, { Component } from "react";
import $ from "jquery";

import "datatables.net";
import "datatables.net-bs4";
import "datatables.net-bs4/css/dataTables.bootstrap4.css";
import "datatables.net-responsive-bs4";
import "datatables.net-responsive-bs4/css/responsive.bootstrap4.css";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import _ from "lodash";
export class SimpleDataTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            table: null,
            itemsSelected: props.itemsSelected !== undefined ? props.itemsSelected : [],
            id: props.id,
            columns: props.columns,
            data: props.data ? props.data : [],
            isMounted: false,
            paging: props.paging !== undefined ? props.paging : true,
            ordering: props.ordering !== undefined ? props.ordering : true,
            isSelected: props.isSelected !== undefined ? props.isSelected : false,
            filter: props.filter !== undefined ? props.filter : true,
            info: props.info !== undefined ? props.info : true,
            actions: props.actions ? props.actions : []
        };
        this.clickActions = this.clickActions.bind(this);
        this.outputFilterAndPagination = this.outputFilterAndPagination.bind(this);
    }

    clickActions(action) {
        this.props.outputEvent(action);
    }

    outputFilterAndPagination(paginationEvent) {
        this.props.outputFilterAndPagination(paginationEvent)
    }

    outputSelectedData(newItemsSelected) {
        this.props.outputSelected(newItemsSelected);
    }

    componentDidMount() {
        const componentTable = this;
        console.log('create datatable', `#${componentTable.state.id}`);
        $(`#${componentTable.state.id}`).DataTable({
            data: componentTable.state.data,
            columns: componentTable.state.columns,
            responsive: true,
            ordering: componentTable.state.ordering,
            select: componentTable.state.isSelected,
            paging: componentTable.state.paging,
            filter: componentTable.state.filter,
            info: componentTable.state.info,
            language: {
                totalPages: componentTable.state.data,
                emptyTable: "Ningún dato disponible en esta tabla",
                info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                infoFiltered: "(filtrado de un total de _MAX_ registros)",
                lengthMenu: "Mostrar _MENU_ registros",
                search: '<i class="la la-search"></i>Buscar',
                zeroRecords: "No se encontraron resultados",
                paginate: {
                    first: '<i class="la la-angle-double-left"></i>',
                    last: '<i class="la la-angle-double-right"></i>',
                    next: '<i class="la la-angle-right"></i>',
                    previous: '<i class="la la-angle-left"></i>'
                }
            },

            columnDefs: componentTable.state.columns.map((colSetting, key) => {
                let settingsObject = {};
                switch (colSetting.type) {
                    case 'action':
                        settingsObject = {
                            targets: key,
                            orderable: false,
                            width: colSetting.width ? colSetting.width : null,
                            render: (data, type, rowData, meta) => {
                                const actions = this.state.actions.map((action) => {
                                    return `<button class="btn btn-sm btn-clean btn-icon btn-icon-md" id="cev_button_${action.action}" title="${action.title}">
                                                    <i class="${action.classLaIcon}"></i>
                                                </button>`
                                });
                                return actions.length > 0 ? actions.reduce((a, b) => (a + b)) : ''
                            },

                            createdCell: (cell, cellData, rowData, rowIndex) => {
                                this.state.actions.map((action) => {
                                    return $(cell).find(`#cev_button_${action.action}`).on('click', function (e) {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        componentTable.clickActions({ action: action, row: rowData });
                                    });
                                })
                            },
                        }
                        break;
                    case 'list':
                        settingsObject = {
                            targets: key,
                            orderable: true,
                            width: colSetting.width ? colSetting.width : null,
                            render: (data, type, rowData, meta) => {
                                const dataList = Array.isArray(data) ? data : []
                                return dataList.map((data) => {
                                    return `<li>
                                                    ${data}
                                                </li>`
                                }).reduce((a, b) => (a + b), "")
                            },
                        }
                        break;
                    case 'range-date':
                        settingsObject = {
                            targets: key,
                            orderable: true,
                            width: colSetting.width ? colSetting.width : null,
                            render: (data, type, rowData, meta) => {
                                return JSON.stringify(data)
                            },
                        }
                        break;
                    case 'miniature':
                        settingsObject = {
                            targets: key,
                            orderable: true,
                            width: colSetting.width ? colSetting.width : null,
                            render: ((data, type, rowData, meta) => {
                                return data === 'no-photo' ? data : `<img style="max-width: 50px; border-radius: 25px" src=${rowData.photo} alt="user" />`;
                            })
                        }
                        break;

                    default:
                        settingsObject = {
                            targets: key,
                            orderable: true,
                            width: colSetting.width ? colSetting.width : null,
                            render: (data, type, rowData, meta) => {
                                if (Object.prototype.toString.call(data) === "[object String]") {
                                    return data ? data : '';
                                } else if (Object.prototype.toString.call(data) === "[object Object]") {
                                    return data ? JSON.stringify(data) : '';
                                } else if (Object.prototype.toString.call(data) === "[object Number]") {
                                    return data ? `${data}` : '';
                                } else {
                                    return data ? JSON.stringify(data) : '';
                                }
                            },
                        }
                        break;
                }
                return settingsObject;
            })
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.data !== this.props.data) {
            this.loadSelectedData();
        }
        if (prevProps.actions !== this.props.actions) {
            this.setState({ actions: this.props.actions })
        }
    }

    loadSelectedData() {
        console.log('loading data ... ')
        let table = $(`#${this.state.id}`).DataTable();
        table.clear();
        table.rows.add(this.props.data);
        table.draw();
        if (this.state.isSelected) {
            this.checkPageSelection(this.props.data);
            const rows = document.getElementsByTagName(`tr`);
            for (let i = 1; i < rows.length; i++) {
                rows[i].addEventListener('click', () => {
                    rows[i].classList.toggle("selected");
                    if ((rows[i].classList.value).includes('selected')) {
                        const selected = (table.rows('.selected'))[0];
                        const returnSelected = selected.map((i) => {
                            return this.props.data[i];
                        })
                        this.updateMySelected(returnSelected);
                    } else {
                        const item = this.props.data[i - 1];
                        this.removeSelected(item);
                    }
                });
            }
        }
    }

    removeSelected(item) {
        const removeElement = this.state.itemsSelected.filter((element) => (item.ident !== element.ident))
        this.outputSelectedData(removeElement);
        this.setState({ itemsSelected: removeElement });
    }

    updateMySelected(pageSelected) {
        const newSelection = _.unionBy(this.state.itemsSelected, pageSelected, 'ident');
        this.outputSelectedData(newSelection);
        this.setState({ itemsSelected: newSelection });
    }

    checkPageSelection(data) {
        const intersection = _.intersectionWith(this.state.itemsSelected, data, _.isEqual);
        const rows = document.getElementsByTagName(`tr`);
        for (let i = 0; i < intersection.length; i++) {
            const returnIndex = (_.findIndex(data, intersection[i]));
            if (returnIndex !== -1) {
                rows[returnIndex + 1].classList.add("selected");
            }
        }
    }

    componentWillUnmount() {
        $(`#${this.state.id}`).DataTable().destroy(true);
    }

    selectPage() {
        let table = $(`#${this.state.id}`).DataTable();
        const rows = document.getElementsByTagName(`tr`);
        for (let i = 1; i < rows.length; i++) {
            rows[i].classList.add("selected");
        }
        const selected = (table.rows('.selected'))[0];
        const returnSelected = selected.map((i) => {
            return this.props.data[i];
        })
        this.outputSelectedData(returnSelected);
        this.updateMySelected(returnSelected);
    }

    clearSelect() {
        let table = $(`#${this.state.id}`).DataTable();
        const rows = document.getElementsByTagName(`tr`);
        for (let i = 1; i < rows.length; i++) {
            rows[i].classList.remove("selected");
        }
        const selected = (table.rows('.selected'))[0];
        const returnSelected = selected.map((i) => {
            return this.props.data[i];
        })
        this.outputSelectedData([]);
        this.setState({ itemsSelected: [] });
    }

    render() {
        const { id, isSelected, itemsSelected, data } = this.state;
        return (
            <>
                {isSelected && (
                    <div className="d-flex justify-content-between">
                        <div className="d-flex justify-content-between">
                            <span className="m-2 ">{itemsSelected.length > 0 ? itemsSelected.length + ' recursos eleccionados' : ''}</span>
                        </div>
                        <div className="d-flex justify-content-between">
                            <button className="btn-small ml-2 mt-2 btn btn-primary" onClick={() => { this.selectPage() }}>Seleccionar página</button>
                            <button className="btn-small ml-2 mt-2 btn btn-primary" onClick={() => { this.clearSelect() }}>limpiar selección</button>
                            {itemsSelected.length > 0 && (
                                <button className="btn-small ml-2 mt-2 btn btn-primary" onClick={() => { this.props.eventSelect() }}>
                                    Gestionar Selección<i className="flaticon2-right-arrow" />
                                </button>
                            )}
                        </div>
                    </div>
                )}
                <table className="table table-striped- table-bordered table-hover dt-responsive table-checkable" id={id}></table>
            </>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(SimpleDataTable);