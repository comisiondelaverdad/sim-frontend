import React from 'react';
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
import { ORIGIN_MAP } from "../../config/const";

function FondoDropdown ({fondos, fondoSelected, selected}) {
    
    let dropdownItems = [];
    for (var i = 0; i < fondos.length; i++) {
        let fondo = fondos[i];
        dropdownItems.push(
        <Dropdown.Item 
          key={i + fondo.key}
          onSelect={() => fondoSelected(fondo.key)}
        > 
          {ORIGIN_MAP[fondo.key] ? ORIGIN_MAP[fondo.key] : fondo.key} 
        </Dropdown.Item>
        );
      }

    return (
        
    <DropdownButton id="dropdown-fondo-button" title={selected === "Todos" ? "Selecciona un fondo" : ORIGIN_MAP[selected]}>
      <Dropdown.Item key="Todos" onSelect={() => fondoSelected("Todos")}> 
          Todos
        </Dropdown.Item>
       {dropdownItems}
    </DropdownButton>
    );
}

export default FondoDropdown;
