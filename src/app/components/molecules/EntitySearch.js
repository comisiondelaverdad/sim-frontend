import React, {Component} from "react";

import _ from 'lodash'

import EntityItem from './EntityItem';

import * as VizService from "../../services/VizService";
import { Alert, Container, Card } from "react-bootstrap";

class EntitySearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      entitiesList: undefined,
    };
  }

  componentDidMount() {
    VizService.getListaEntidades()
      .then(json => {
        let list = _.sortBy(json.aggregations.tipo.entidades.buckets, "doc_count").reverse()
        this.setState({
          entitiesList: list
        });
      })
  }

  render() {

    return (
      <>
        {this.state.entitiesList &&
          <Card>
            <Card.Header className="text-center">Entidades</Card.Header>
            <Card.Body>
              {this.state.entitiesList &&
                this.state.entitiesList.map((data, k)=>(
                  <EntityItem key={k} data={data} addQuery={this.props.addQuery} />
                ))
              }
            </Card.Body>
          </Card>
        }
        {!this.state.entitiesList &&
          <>
            <Container fluid={true} className="my-5">
              <Alert variant="success">
                <div className="alert-icon">
                  <i className="fas fa-sync fa-spin"/>
                </div>
                <div className="alert-text">Cargando entidades ...</div>
              </Alert>
            </Container>
          </>
        }
      </>
    );
  }

}

export default EntitySearch
