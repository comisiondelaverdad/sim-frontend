import React from 'react';
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
import { PERIODOS_MAP } from "../../config/const";

function PeriodosDropdown ({periodos, periodoSelected, selected}) {
    
    let dropdownItems = [];
    for (var i = 0; i < periodos.length; i++) {
        let periodo = periodos[i];

        dropdownItems.push(
        <Dropdown.Item 
          key={i + periodo}
          onSelect={() => periodoSelected(periodo)}
        > 
          {PERIODOS_MAP[periodo] ? PERIODOS_MAP[periodo] : periodo} 
        </Dropdown.Item>
        );
      }

    return (
        
    <DropdownButton id="dropdown-periodo-button" title={selected === "Todos" ? "Selecciona un periodo de esclarecimiento" : PERIODOS_MAP[selected]}>
      <Dropdown.Item key="Todos" onSelect={() => periodoSelected("Todos")}> 
          Todos
        </Dropdown.Item>
       {dropdownItems}
    </DropdownButton>
    );
}

export default PeriodosDropdown;
