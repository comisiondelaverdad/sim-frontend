import React from "react";
import { connect } from "react-redux";
import { Dropdown } from "react-bootstrap";
import DropDownIcon from "../molecules/DropDownIcon";

import DataRangeSlider from "../molecules/DataRangeSlider"

import * as FormService from "../../services/FormService";

import _ from "lodash";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import DateRangeIcon from '@material-ui/icons/DateRange';

class FormAdvancedSearchTime extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
      newLabel : '',
      show : false,
      alert : false,
      form: undefined,
      isLoading: false,
    };

    this.validate = this.validate.bind(this);
  }
 
  componentDidMount(){
    this.setState({oldLabel:this.props.label, newLabel:this.props.label, show:false});
  }

  validate(evt){
    evt.preventDefault()
  }

  selectForm(event){
    let f = this.props.config.forms[event.target.value]
    if (f!==undefined){
      if (f.type==='form-select'){
        FormService.getOptions(f.field)
        .then(json => {
          if(json.aggregations.options.buckets.length>0){
            f.schema.properties[f.name].enum = _.map(json.aggregations.options.buckets, 'key')
          }else{
            f.schema.properties[f.name].enum = ["Sin Registros"]
          }
          f.schema.properties[f.name].enum = f.schema.properties[f.name].enum.sort()
          this.setState({form:f})
        });
      }else{
        this.setState({form:f})
      }
    }else{
      this.setState({form:null})
    }    
  }
  
  onSubmit(formData, e) {
    e.preventDefault()
    let query = this.state.form.queryAlias ? this.state.form.queryAlias : this.state.form.query
    if (typeof formData === "object"){
      for (const prop in formData) {
        console.log(prop, formData[prop])
        if(formData[prop]!=="Sin Registros"){
          query = query.split("["+prop+"]").join(formData[prop]);
        }
      }
    }else{
      query = formData
    }
    if(query !== this.state.form.query){
      this.setState({"query": query, "formData": formData, show:false, form: undefined}, this.props.addQuery(query))
    }else{
      console.log("No se puede agregar registro")
    }
    
  }
  
  asyncSearch(query) {
    this.setState({isLoading: true});
    FormService.getAsyncOptions(this.state.form, query)
      .then(json => {
        this.setState({
          isLoading: false,
          options: json.items,
        }, ()=>{console.log("El estado ahora es ", this.state)})
      });
  }
  
  mapSelect(geojsonData){
    this.setState({"mapData": geojsonData})
  }
  
  mapAdd(){
    let q = ""
    let comma = ""
    _.map(this.state.mapData.features, (v, k)=>{
      q += comma + v.geometry.type[0] + "_"
      let comma2 = "" 
      _.map(v.geometry.coordinates, (w, i)=>{
        q += comma2 + w
        comma2 = "-"
      })
      comma = "|"
    })
    this.setState({show:false, form: undefined}, this.props.addMap(this.state.mapData, q))
  }

  
  render(){
    const { 
      show,
    } = this.state;

    let classDropdown = this.props.showSearchBar ? "" : "cev-advancedform";

    return(
      <Dropdown
        drop="down"
        show={show}
        onToggle={()=>{this.setState({show:false})}}
        flip={"false"}
      >
        <Dropdown.Toggle 
          as={DropDownIcon}
          onClick={()=>{this.setState({show:true})}}
        >
            <button type="button" className="btn-small ml-2 btn btn-primary" title="Búsqueda por fecha">
              <DateRangeIcon></DateRangeIcon>
            </button>
        </Dropdown.Toggle>
        <Dropdown.Menu flip={"false"} className={`dropdown-menu-fit dropdown-menu-left dropdown-menu-anim dropdown-menu-xl ${classDropdown} advanceSearch`}>
          <div className="cev-portlet">
            <div className="cev-portlet__head">
              <div className="cev-portlet__head-label">
                <h3 className="cev-portlet__head-title cev-font-primary">
                  Búsqueda por tiempo 
                </h3>
              </div>
            </div>
            <div className="cev-portlet__body">
              <DataRangeSlider
                addQuery={(q)=>this.props.addQuery(q)}
              />
            </div>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    )
  }
}

const mapStateToProps = store => ({
  user: store.auth.user._id,
  showSearchBar: store.app.showSearch
});

export default connect(
  mapStateToProps
)(FormAdvancedSearchTime);
