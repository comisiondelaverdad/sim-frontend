import React, { Component } from "react";
import { Form, Container } from "react-bootstrap";
import Fullscreenable from 'react-fullscreenable';
import PropTypes from 'prop-types';
import ReactWaterMark from 'react-watermark-component';

const fontSizes = [12,14,16,18,20,22,24,26,28,30,34,38,42,46,50,56,62,68,74,80];

const fontFamilies = [
  {name: "Arial", value: "Arial"},
  {name: "Courier New", value: "Courier New"},
  {name: "Georgia", value: "Georgia"},
  {name: "Lucida Console", value: "Lucida Console"},
  {name: "Poppins", value: "Poppins, Helvetica, sans-serif"},
  {name: "Times New Roman", value: "Times New Roman"},
  {name: "Verdana", value: "Verdana"}
]

class ControlBar extends Component {
  constructor(props){
    super(props);
    this.state = {
      fontSize: "14px",
      fontFamily: "Poppins, Helvetica, sans-serif",
      backgroundColor: "#fff",
      color: "#000"
    };
    this.increaseFontSize = this.increaseFontSize.bind(this);
    this.decreaseFontSize = this.decreaseFontSize.bind(this);
    this.changeFontFamily = this.changeFontFamily.bind(this);
    this.changeFontSize = this.changeFontSize.bind(this);
    this.changeTheme = this.changeTheme.bind(this);
  }

  changeTheme(e){
    let theme = e.target.value;
    if(theme==="Dark"){
      this.setState({
        backgroundColor:"#000",
        color:"#fff",
      });
    }
    else{
      this.setState({
        backgroundColor:"#fff",
        color:"#000",
      });
    }
  }

  increaseFontSize(e){
    e.preventDefault();
    let fontSize = parseInt(this.state.fontSize,10);
    let fontPosition = fontSizes.indexOf(fontSize);
    let totalFontSize = fontSizes.length;
    if(fontPosition < (totalFontSize - 1)){
      this.setState({fontSize:fontSizes[fontPosition+1]+"px"});
    }
  }

  decreaseFontSize(e){
    e.preventDefault();
    let fontSize = parseInt(this.state.fontSize,10);
    let fontPosition = fontSizes.indexOf(fontSize);
    if(fontPosition > 0){
      this.setState({fontSize:fontSizes[fontPosition-1]+"px"});
    }
  }

  changeFontFamily(e){
    this.setState({fontFamily:e.target.value});
  }

  changeFontSize(e){
    this.setState({fontSize:e.target.value+"px"});
  }

  increaseZoom(e){
    e.preventDefault();
  }

  decreaseZoom(e){
    e.preventDefault();
  }

  render() {

    let style = this.state;
    delete style.fullScreen;

    const options = {
      chunkWidth: 450,
      chunkHeight: 90,
      textAlign: 'left',
      textBaseline: 'bottom',
      globalAlpha: 0.2,
      font: '36px Microsoft Yahei',
      rotateAngle: -0.1,
      fillStyle: '#666'
    }

    return (
        <>
          <div className="cev-control-bar"
            style={{
              alignItems: 'center',
              display: 'flex',
              width: '100%',
            }}
          >
            <div
              style={{
                alignItems: 'center',
                display: 'flex',
              }}
            >
              {this.props.config.search &&
                <div style={{ padding: '0 2px' }}>
                  Search
                </div>
              }
              {this.props.config.themes &&
                <div style={{ padding: '0 2px' }}>
                  <Form.Control
                    as="select"
                    onChange={this.changeTheme}
                    defaultValue="Light"
                  >
                    <option value="Light">Light</option>
                    <option value="Dark">Dark</option>
                  </Form.Control>
                </div>
              }
              {this.props.config.fontFamily &&
                <div style={{ padding: '0 2px' }}>
                  <Form.Control
                    as="select"
                    onChange={this.changeFontFamily}
                    value={this.state.fontFamily}
                  >
                    {fontFamilies.map((data, key) => (<option key={key} value={data.value}>{data.name}</option>))}
                  </Form.Control>
                </div>
              }
              {this.props.config.fontSize &&
                <div style={{ padding: '0 2px' }}>
                  <Form.Control
                    as="select"
                    onChange={this.changeFontSize}
                    value={parseInt(this.state.fontSize,10)}
                  >
                    {fontSizes.map((data, key) => (<option key={key} value={data}>{data}</option>))}
                  </Form.Control>
                </div>
              }
              {this.props.config.textUP &&
                <div style={{ padding: '0 3px' }}>
                  <span className="cev-widget__icon" onClick={this.increaseFontSize}>
                    <i className="fas fa-font cev-font-dark" style={{fontSize:"24px"}}></i>
                    <i className="fas fa-sort-up cev-font-dark" style={{fontSize:"16px",float:"right",margin:"-1px -5px"}}></i>
                  </span>
                </div>
              }
              {this.props.config.textDOWN &&
                <div style={{ padding: '0 5px',marginTop: "4.7px" }}>
                  <span className="cev-widget__icon" onClick={this.decreaseFontSize}>
                    <i className="fas fa-font cev-font-dark" style={{fontSize:"20px"}}></i>
                    <i className="fas fa-sort-down cev-font-dark" style={{fontSize:"16px",float:"right",margin:"-8px -5px"}}></i>
                  </span>
                </div>
              }
            </div>
            <div
              style={{
                alignItems: 'center',
                display: 'flex',
                flexGrow: 1,
                flexShrink: 1,
                justifyContent: 'center',
              }}
            >
              {this.props.config.zoom &&
                <div style={{ padding: '0 5px' }}>
                  <span className="cev-widget__icon cev-font-dark" onClick={(evt)=>{this.increaseZoom()}}>
                    <i className="fas fa-search-plus" style={{fontSize:"24px"}}></i>
                  </span>
                </div>
              }
              {this.props.config.zoom &&
                <div style={{ padding: '0 5px' }}>
                  <Form.Control
                    type="text"
                    readOnly
                    value={100+"%"}
                    style={{width:"60px"}}
                  />
              </div>
              }
              {this.props.config.zoom &&
                <div style={{ padding: '0 5px' }}>
                  <span className="cev-widget__icon cev-font-dark" onClick={(evt)=>{this.decreaseZoom()}}>
                    <i className="fas fa-search-minus" style={{fontSize:"24px"}}></i>
                  </span>
                </div>
              }
              
            </div>
            <div
              style={{
                alignItems: 'center',
                display: 'flex',
                marginLeft: 'auto',
              }}
            >
              {this.props.config.fullScreen &&
                <div style={{ padding: '0 5px' }}>
                  <span className="cev-widget__icon cev-font-dark" onClick={this.props.toggleFullscreen}>
                    <i className={`fas ${this.props.isFullscreen ? "fa-compress-alt" : "fa-expand-alt"}`} style={{fontSize:"24px"}}></i>
                  </span>
                </div>
              }
            </div>
          </div>
          {this.props.isFullscreen &&
            <ReactWaterMark
              waterMarkText={this.props.user}
              options={options}
            >
              <Container fluid={true} style={this.state}>
                {this.props.children}
              </Container>
            </ReactWaterMark>
          }
          {!this.props.isFullscreen &&
            <Container fluid={true} style={this.state}>
              {this.props.children}
            </Container>
          }
        </>
    );
  }
}

ControlBar.propTypes = {
  isFullscreen: PropTypes.bool,
  toggleFullscreen: PropTypes.func,
  viewportDimensions: PropTypes.object,
};

export default Fullscreenable()(ControlBar);