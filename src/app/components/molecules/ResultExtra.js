import React, { useEffect, useState } from "react"
import { toAbsoluteUrl } from "../../../theme/utils";
import { dateToAAAAMMDD } from "./../../services/utils"
export default function ResultExtra(props) {
    const [validate, setValidate] = useState(false);
    const [value, setValue] = useState(0);

    useEffect(() => {
        if (props.type && (props.type === "Date" || props.type === "DateRange")) {
            try {
                setValidate(true);

                setValue(props.type === "DateRange" ? dateToAAAAMMDD(props.description[0]) + " - " + dateToAAAAMMDD(props.description[1]) : dateToAAAAMMDD(props.description));
            }
            catch (e) {
                setValidate(false);
                setValue("");
            }
        } else {
            setValue(props.description);
            setValidate(true);
        }

    }, [props.description, props.type]);

    return (
        <>
            {validate &&
                <div className="item">
                    <span className="label">{props.title}</span>
                    {props.icon && (
                    <svg className="cv-medium">
                        <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-" + props.icon)} className="flia-green"></use>
                    </svg>
                    )}
                    <span>{value}</span>
                </div>
            }
        </>
    );

}