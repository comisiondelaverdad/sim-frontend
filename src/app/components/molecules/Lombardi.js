import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3'

/**
 * Bloque para generar un Grafo Lombardi en D3
 * 
 * @version 0.1
 */

export default class Lombardi extends Component {
    static propTypes = {
        /** Función D3 para la escala de color */
        color: PropTypes.func,
        /** Network: Objeto con los nodos y links del grafo */
        network: PropTypes.object,
        /** Entidades seleccionadas por click */
        selected: PropTypes.array
    }


    constructor(props) {
        super(props)
        this.r = 7
    }

    componentDidMount() {
        let parent = this
        this.svg = d3.select(this.refs.d3_canvas).append('g').attr('class', 'container')
        var zoom = d3.zoom()
            .scaleExtent([0.2, 8])
            .on('zoom', function () {
                parent.svg.selectAll('path,circle,text')
                    .attr('transform', d3.event.transform);
            });

        this.svg.call(zoom);
        this.updateViz()
    }

    componentDidUpdate() {
        // let r = this.r

        // if(this.props.selected.length > 0){
        //     this.node
        //     .transition()
        //     .attr("fill", d => {
        //         if(this.props.selected.includes(d.name))
        //             return this.props.color(d.group)
        //         else
        //             return '#fff'
        //     })
        //     .attr("stroke", d => {
        //         if(this.props.selected.includes(d.name))
        //             return '#fff'
        //         else
        //             return '#ccc'
        //     })
        // } else {
        //     this.node
        //     .transition()
        //     .attr("fill", d => {
        //         return this.props.color(d.group)
        //     })
        //     .attr("stroke", d => {
        //         return '#fff'
        //     })
        // }
    }

    updateViz() {
        const fuerza_atraccion = d3.scaleLinear().domain([this.props.network['link-data'][0].value, 0]).range([1, 0])
        const size = d3.scaleLinear().domain([0, this.props.network.max]).range([5, 15])
        const stw = d3.scaleLinear().range([0, 10]).domain([0, this.props.network['node-data'][0].value])
        const radio = d3.scaleLinear().domain([0, this.props.network['node-data'][0].value]).range([10, 150])

        // const fade = opacity => {
        //     return d => {
        //       leyenda.style('visibility', o => d.index === o.index ? "visible" : "hidden" );
        //       if(opacity === 1){
        //         this.node.style('opacity', 1)
        //         leyenda.style('visibility', 'hidden')
        //         link.style('stroke-opacity', 1)
        //       }
        //     };
        //   }

        // const drag = simulation => {

        //     function dragstarted(d) {
        //       if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        //       d.fx = d.x;
        //       d.fy = d.y;
        //     }

        //     function dragged(d) {
        //       d.fx = d3.event.x;
        //       d.fy = d3.event.y;
        //     }

        //     function dragended(d) {
        //       if (!d3.event.active) simulation.alphaTarget(0);
        //       d.fx = null;
        //       d.fy = null;
        //     }

        //     return d3.drag()
        //         .on("start", dragstarted)
        //         .on("drag", dragged)
        //         .on("end", dragended);
        //   }
        const simulation = d3.forceSimulation(this.props.network['node-data'])
            .force("link", d3.forceLink(this.props.network['link-data']).id((d, i) => i).strength(d => fuerza_atraccion(d.value)))
            .force("collide", d3.forceCollide(d => radio(d.value)).iterations(2))
            .force("charge", d3.forceManyBody().strength(100))
            .force("center", d3.forceCenter((900 / 2), (900 / 2)))



        const link = this.svg.append('g')
            .selectAll('line')
            .data(this.props.network['link-data'])
            .join('path')
            .attr('stroke', '#999')
            .attr('stroke-width', d => stw(d.value))
            .attr('fill', 'none')

        const node = this.svg.append("g")
            .attr("stroke", "#fff")
            .attr("stroke-width", 1.5)
            .selectAll("circle")
            .data(this.props.network['node-data'])
            .join("circle")
            .style('cursor', 'pointer')
            .attr("fill", d => this.props.color(d.group))
            .attr('stroke-width', 1)
            .attr("r", 4)

        const leyenda = this.svg.append('g')
            .selectAll('text')
            .data(this.props.network['node-data'])
            .join('text')
            .text(d => d.name)
            .attr('fill', 'rgba(255,255,255,0.25)')
            .attr('text-anchor', 'middle')
            .attr('transform', 'translate(0 -100)')
            .attr('font-size', d => {
                return size(d.value)
            })

        simulation.on("tick", () => {
            link.attr("d", d => {
                let r = Math.hypot(d.target.x - d.source.x, d.target.y - d.source.y)

                if (d.target.value > d.source.value) return `
                  M${d.source.x},${d.source.y}
                  A${r},${r} 1 0,1 ${d.target.x},${d.target.y}
                `
                else return `
                  M${d.target.x},${d.target.y}
                  A${r},${r} 1 0,1 ${d.source.x},${d.source.y}
                `
            })

            node
                .attr("cx", d => d.x)
                .attr("cy", d => d.y)

            leyenda
                .attr("x", d => d.x + 10)
                .attr("y", d => d.y)
        })

        const linkedByIndex = {};

        this.props.network['link-data'].map(d => {
            linkedByIndex[`${d.source.index},${d.target.index}`] = 1;
            return ''
        });

        // function isConnected(a, b) {
        //     return linkedByIndex[`${a.index},${b.index}`] || linkedByIndex[`${b.index},${a.index}`] || a.index === b.index;
        // }
    }

    render() {
        return (
            <>
                <div className="left">
                    <svg
                        ref="d3_canvas"
                        width="900"
                        height="900"
                        viewBox="0 0 900 900"
                    />
                </div>

            </>
        )
    }
}