import React, {Component} from "react";
import {connect} from "react-redux";
import TreeView from "../../components/molecules/TreeView";
import * as app from "../../store/ducks/app.duck";
import _ from 'lodash'
import * as VizService from "../../services/VizService";
import { DOC_ENTITY_COLORS } from "../../config/const"
import { Alert, Container, Card } from "react-bootstrap";

class TesauroSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      annotations: null,
      entitiesTree: null,
      documentText: null,
      entitiesColorMap: null,
      showEntities: false
    };
    this.toggleEntities = this.toggleEntities.bind(this);
  }

  componentDidMount() {
    VizService.getListaEtiquetas()
      .then(json => {
        console.log(json)
        let list = _.map(_.sortBy(json.aggregations.tipo.etiquetas.buckets, "key"), "key")
        const entitiesColorMap = this.createEntitiesColorMap(list.map(a => JSON.parse(`"${a}"`)), DOC_ENTITY_COLORS);
        const entitiesTree = this.createEntitiesTree(list.map(a => JSON.parse(`"${a}"`)), entitiesColorMap);

        this.setState({
          entitiesColorMap: entitiesColorMap,
          entitiesTree: entitiesTree,
          entitiesList: list
        });

      })
      
      this.props.showSearch(true);
      this.props.showSearchTags(false);
      this.props.fromComponent("metadata-tesauro");
    }
  
    componentWillUnmount() {
      this.props.showSearch(false);
      this.props.showSearchTags(false);
      this.props.fromComponent("");
    }
  

  createEntitiesTree(entities, entitiesColorMap) {
    let entitiesTree = [];

    entities.forEach((entity, index) => {
      // Reference tree root array
      let result = entitiesTree;
      // split annotation
      let parts = entity.split(" - ")
      if (parts[0]==="Entidades") {
        return;
      }
      parts.forEach((mapKey, id) => {
        // Validate mapKey is not empty
        if (!mapKey) {
          return;
        }
        if (result!==undefined){
          // Find node by mapKey
          let node = result.find(e => e.text === mapKey);
          // Create key if not present
          if (!node) {
            node = result.push({
              id: index,
              text: mapKey,
              state: {opened: false, selected: false},
              icon: "fas fa-square",
              children: [],
              data: 'etiquetado:"'+entity+'"',
              a_attr: {href: `/search?from=1&q=etiquetado:"${entity}"`, style: `color: ${entitiesColorMap[entity]}`}
            });
          }
          // Reference node children array
          result = node.children;
        }
      });
    });

    return {
      core: {
        data: entitiesTree,
        expand_selected_onload: false,
        themes: {dots: false}
      },
      plugins: [],
      checkbox: {
        three_state: true
      }
    };
  }

  createEntitiesColorMap(entities, docEntityColors) {
    return entities.reduce((colorMap, entity, index) => {
      return {...colorMap, [entity]: docEntityColors[index % docEntityColors.length]};
    }, {});
  }

  handleNodeSelection(e, data) {
    //console.log("handleNodeSelection", data)
    const target = data.event.target
    const href = data.node.a_attr.href
    const keyword = data.node.data
    if (target.tagName.toLowerCase() === 'a' && href !== '#') {
      this.props.filters({
        "filters":{
          "keyword": keyword,
          "resourceGroup": {
            "resourceGroupText": "",
            "resourceGroupIDS": ""
          },
          "mapPolygon": ""
        }
      })
      this.props.addQuery(keyword)
    }
  }

  toggleEntities() {
    this.setState({showEntities: !this.state.showEntities});
  }

  render() {

    const {entitiesTree} = this.state;

    return (
      <>
        {entitiesTree &&
          <Card>
            <Card.Header className="text-center">Etiquetas</Card.Header>
            <Card.Body>
              <TreeView
                  treeData={entitiesTree}
                  onNodeSelected={(e, data) => this.handleNodeSelection(e, data)}
              />
            </Card.Body>
          </Card>
        }
        {!entitiesTree &&
          <>
          <Container fluid={true} className="my-5">
            <Alert variant="success">
              <div className="alert-icon">
                <i className="fas fa-sync fa-spin"/>
              </div>
              <div className="alert-text">Cargando etiquetas ...</div>
            </Alert>
          </Container>
        </>
        }
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(TesauroSearch);