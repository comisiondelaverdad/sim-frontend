import React, { Component } from 'react';
import { Map, TileLayer, /*Circle,*/ FeatureGroup } from 'react-leaflet';
import { EditControl } from 'react-leaflet-draw';
import L from 'leaflet';
import _ from "lodash";

class SIMMapView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      z: this.props.zoom ? this.props.zoom : 5,
      c: this.props.center ? this.props.center : [4.36, -74.04],
      anteriorH: -1,
      anteriorW: -1,
      minHeigth: this.props.minHeigth ? this.props.minHeigth : 350
    }
  }

  update() {
    setTimeout(() => {
      let ref = this.map
      if (ref != null) {
        let h = ref.clientHeight
        if (h < this.state.minHeigth)
          h = this.state.minHeigth
        if (this.state.anteriorH === -1 || this.state.anteriorH === 0 || this.state.anteriorW === -1 || this.state.anteriorW === 0) {
          this.setState({ height: h, width: ref.clientWidth, anteriorH: ref.clientHeight, anteriorW: ref.clientWidth })
        } else {
        }
      }
    }, 1)
  }

  componentDidUpdate(nextProps, nextState) {
    let ref = this.map
    if (
      ref.clientHeight > 0 &&
      ref.clientWidth > 0 &&
      (this.state.height === 0 || this.state.width === 0)
    ) {
      setTimeout(() => {
        this.setState({ height: ref.clientHeight, width: ref.clientWidth, anteriorH: ref.clientHeight, anteriorW: ref.clientWidth })
      }, 500)
    }
  }

  componentDidMount() {
    this.update()
  }


  _onEdited = (e) => {

    let numEdited = 0;
    e.layers.eachLayer((layer) => {
      numEdited += 1;
    });
    console.log(`_onEdited: edited ${numEdited} layers`, e);

    this._onChange();
  }

  _onCreated = (e) => {
    let type = e.layerType;
    //let layer = e.layer;
    if (type === 'marker') {
      // Do marker specific actions
      console.log("_onCreated: marker created", e);
    }
    else {
      console.log("_onCreated: something else created:", type, e);
      console.log("Poligono: ", e.layer.toGeoJSON())
    }
    // Do whatever else you need to. (save to db; etc)

    this._onChange();
  }

  _onDeleted = (e) => {

    let numDeleted = 0;
    e.layers.eachLayer((layer) => {
      numDeleted += 1;
    });
    console.log(`onDeleted: removed ${numDeleted} layers`, e);

    this._onChange();
  }

  _onMounted = (drawControl) => {
    console.log('_onMounted', drawControl);
  }

  _onEditStart = (e) => {
    console.log('_onEditStart', e);
  }

  _onEditStop = (e) => {
    console.log('_onEditStop', e);
  }

  _onDeleteStart = (e) => {
    console.log('_onDeleteStart', e);
  }

  _onDeleteStop = (e) => {
    console.log('_onDeleteStop', e);
  }


  _editableFG = null

  _onFeatureGroupReady = (reactFGref) => {

    if (reactFGref) {
      // populate the leaflet FeatureGroup with the geoJson layers
      console.log("Se terminó la selección")

      let leafletGeoJSON = new L.GeoJSON(this.props.data);
      let leafletFG = reactFGref.leafletElement;

      leafletGeoJSON.eachLayer((layer) => {
        leafletFG.addLayer(layer);
      });

      // store the ref for future access to content

      this._editableFG = reactFGref;
    }
  }

  _onChange = () => {

    // this._editableFG contains the edited geometry, which can be manipulated through the leaflet API

    const { onChange } = this.props;

    if (!this._editableFG || !onChange) {
      return;
    }

    const geojsonData = this._editableFG.leafletElement.toGeoJSON();
    this.props.onChange(geojsonData);
  }

  select() {
    console.log("Seleccionado ", this.state)
    this.props.onAdd(this.state)
  }

  render() {
    console.log("El mapa es: ", this.props.data)
    return (
      <div className="map">
        <div className="space"></div>
        <div ref={el => this.map = el} className="mw-100">
          {
            this.state.height &&
            <Map style={{ width: this.state.width, height: this.state.height }} center={this.state.c} zoom={this.state.z} scrollWheelZoom={false}>
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
              />
              <FeatureGroup ref={(reactFGref) => { this._onFeatureGroupReady(reactFGref); }}>
                <EditControl
                  position='topright'
                  onEdited={this._onEdited}
                  onCreated={(e) => { this._onCreated(e) }}
                  onDeleted={(e) => { this._onDeleted(e) }}
                  onMounted={(e) => { this._onMounted(e) }}
                  onEditStart={(e) => { this._onEditStart(e) }}
                  onEditStop={(e) => { this._onEditStop(e) }}
                  onDeleteStart={(e) => { this._onDeleteStart(e) }}
                  onDeleteStop={(e) => { this._onDeleteStop(e) }}
                  draw={{
                    polyline: false,
                    polygon: false,
                    rectangle: false,
                    circle: false,
                    circlemarker: false,
                    marker: false,
                  }}
                  edit={{
                    edit: false,
                    remove: false,
                    poly: false,
                    allowIntersection: false,
                  }}
                />
              </FeatureGroup>
            </Map>
          }
          {/*<div>
            <button type="button" onClick={()=>this.select()} className="btn btn-success">Agregar</button>
          </div>
         */}
        </div>
      </div>
    )
  }
}


function getGeoJson() {
  return {
    "type": "FeatureCollection",
    "features": [
    ]
  }
}

export default SIMMapView;
