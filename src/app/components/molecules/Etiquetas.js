import React , {Component} from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3'

/**
 * Bloque para generar un Grafo en D3
 * 
 * @version 0.1
 */

export default class Grafo extends Component {
    static propTypes = {
        /** Network: Objeto con los nodos y links del grafo */
        root: PropTypes.object,
      }

      
    constructor(props){
        super(props)
        
        this.state = {
            width: 1400,
            height: 650
        }

        this.updateViz = this.updateViz.bind(this)
    }

    componentDidMount(){
        this.svg = d3.select(this.refs.d3_canvas)
        this.updateViz()

        console.log(this.props.root)
    }

    updateViz(){
        let width = this.state.width
        let margin = ({top: 30, right: 80, bottom: 0, left: 300})
        let barStep = 50
        let barPadding = 0.5
        let height = () => {
            let max = 1;
            this.props.root.each(d => d.children && (max = Math.max(max, d.children.length)));
            return max * barStep + margin.top + margin.bottom;
          }

        this.setState({
            height: height()
        })
        let x = d3.scaleLinear().range([margin.left, width - margin.right])
        let delay = 25
        let duration = 500
        let color = d3.scaleOrdinal(d3.schemeTableau10)
        let yAxis = g => g
        .attr("class", "y-axis")
        .attr("transform", `translate(${margin.left + 0.5},0)`)
        .call(g => g.append("line")
            .attr("stroke", "currentColor")
            .attr("y1", margin.top)
            .attr("y2", this.state.height - margin.bottom))
        
        let xAxis = g => g
        .attr("class", "x-axis")
        .attr("transform", `translate(0,${margin.top})`)
        .call(d3.axisTop(x).ticks(width / 80).tickSize(-this.state.height))

        x.domain([0, this.props.root.value]).nice();

        this.svg.append("rect")
        .attr("class", "background")
        .attr("fill", "none")
        .attr("pointer-events", "all")
        .attr("width", width)
        .attr("height", this.state.height)
        .attr("cursor", "pointer")
        .on("click", d => {
          up(this.svg, d)
        });

        this.svg.append("g")
            .call(xAxis);

        this.svg.append("g")
            .call(yAxis);

        down(this.svg, this.props.root)

        function down(svg, d) {
            if (!d.children || !svg.selectAll(".exit").empty()) return;
            
            const end = duration + d.children.length * delay;
            
            const exit = svg.selectAll(".enter")
                .attr("class", "exit");
            
            exit.selectAll("rect").filter(p => p === d)
                .attr("fill-opacity", 0);
            
            const enter = bar(svg, down, d)
                .attr("transform", stack(d.index))
                .attr("fill", d => {
                    if(d.children) return color(d.data.name)
                    else return "#FFF"
                })
                .attr("fill-opacity", d=>{
                    if(d.children) return 1
                    else return 1
                })
                .attr("stroke", d => {
                    return color(d.data.name)
                })
                .attr("opacity", 1);
            
            enter.selectAll("text").attr("fill-opacity", 0);
            enter.select("rect")
                .attr("stroke", d => {
                    return color(d.data.name)
                })
                .attr("fill", d => {
                    if(d.children) return color(d.data.name)
                    else return "#FFF"
                })
                .attr("fill-opacity", d=>{
                    if(d.children) return 1
                    else return 1
                });
            
            const exitTransition = exit.transition()
                .duration(duration)
                .attr("opacity", 0)
                .remove();
            
            exitTransition.selectAll("rect")
                .attr("width", d => x(d.value) - x(0));
                
            const doEnterTransition = () => {
                x.domain([0, d3.max(d.children, d => d.value)]).nice();
            
                svg.selectAll(".x-axis").transition()
                    .duration(duration)
                    .call(xAxis);
                
                const enterTransition = enter.transition()
                    .duration(duration)
                    .delay((d, i) => i * delay)
                    .attr("transform", (d, i) => `translate(0,${barStep * i})`);
            
                enterTransition.selectAll("text")
                    .attr("fill-opacity", 1);
                
                enterTransition.selectAll(".value_num")
                .attr("x", d => {
                    return (x(d.value) + 5)
                })
            
                enterTransition.select("rect")
                    .attr("width", d => x(d.value) - x(0))
                    .attr("stroke", d => {
                    return color(d.data.name)
                    })
                    .attr("fill", d => {
                    if(d.children) return color(d.data.name)
                    else return "#FFF"
                    })
                    .attr("fill-opacity", d=>{
                    if(d.children) return 1
                    else return 1
                    });    
            }
            
            exitTransition.on("end", doEnterTransition);
            
            if (!exit.nodes().length) doEnterTransition();
            
            svg.select(".background")
                .datum(d)
                .transition()
                .duration(end);
            }

        function up(svg, d) {
            if (!d.parent || !svg.selectAll(".exit").empty()) return;
            const end = duration + d.children.length * delay;
            
            const exit = svg.selectAll(".enter")
                .attr("class", "exit");
            
            const enter = bar(svg, down, d.parent)
                .attr("transform", (d, i) => `translate(0,${barStep * i})`)
                .attr("opacity", 0);
            
            x.domain([0, d3.max(d.parent.children, d => d.value)]).nice();
            
            enter.select("rect")
                .attr("fill", d => {
                    if(d.children) return color(d.data.name)
                    else return "#FFF"
                })
                    .attr("fill-opacity", d=>{
                    if(d.children) return 1
                    else return 1
                })
                .attr("stroke", d => {
                    return color(d.data.name)
                })
                .filter(p => p === d)
                .attr("fill-opacity", 0);
                                    
            const doEnterTransition = () => {
                enter
                .filter(p => p === d)      
                .attr("opacity", 1)
                
                enter
                .select("rect")
                    .attr("width", d => x(d.value) - x(0))
                    .attr("fill", d => {
                    if(d.children) return color(d.data.name)
                    else return "#FFF"
                    })
                    .attr("fill-opacity", d=>{
                    if(d.children) return 1
                    else return 1
                    })
                    .attr("stroke", d => {
                    return color(d.data.name)
                    });      
                    
                const enterTransition = enter.transition()
                    .duration(end)
                    .attr("opacity", 1);
                
                enter.selectAll(".value_num")
                .attr("x", d => {
                    return (x(d.value) + 5)
                })
            
                enterTransition.select("rect")
                    .attr("width", d => x(d.value) - x(0));    
            }  
            
            svg.selectAll(".x-axis").transition()
                .duration(duration)
                .call(xAxis);  
            
            const exitTransition = exit.selectAll("g").transition()
                .duration(duration)
                .delay((d, i) => i * delay)
                .attr("transform", stack(d.index));
            
            exitTransition.selectAll("text")
                .attr("fill-opacity", 0);
            
            exitTransition.select("rect")
                .attr("width", d => x(d.value) - x(0))
                .attr("fill", d => {
                    if(d.children) return color(d.data.name)
                    else return "#FFF"
                })
                    .attr("fill-opacity", d=>{
                    if(d.children) return 1
                    else return 1
                })
                .attr("stroke", d => {
                    return color(d.data.name)
                });
            
            exit.transition()
                .duration(end)
                .remove()
                .on("end", doEnterTransition) ;   
            
            svg.select(".background")
                .datum(d.parent)
                .transition()
                .duration(end);
            }

        function bar(svg, down, d) {
            const bar = svg.insert("g", ".y-axis")
                .attr("class", "enter")
                .attr("opacity", 1)
                .attr("transform", `translate(0,${margin.top + barStep * barPadding})`)
                .attr("text-anchor", "end")
                .style("font", "10px sans-serif")
                .selectAll("g")
                .data(d.children)
                .join("g")
                .attr("cursor", d => !d.children ? null : "pointer")
                .on("click", d => down(svg, d));
            
            bar.append("text")
                .attr("x", margin.left + 5)
                .attr("y", -10)
                .attr("dy", ".35em")
                .attr("fill-opacity", 1)
                .text(d => {
                    if(d.children) return d.children.length + " sub."
                })
                .attr("text-anchor", "start")
                .attr("stroke","none")
                .attr("fill",d=>{
                    return "#ccc"
                });
            
            bar.append("text")
                .attr("class", "value_num")
                .attr("x", d => {
                    return (x(d.value) + 5)
                })
                .attr("y", barStep * (1 - barPadding) / 2)
                .attr("dy", ".35em")
                .attr("fill-opacity", 1)
                .attr("font-size", 10)
                .text(d => {
                    return d.value.toFixed(2)
                })
                .attr("text-anchor", "start")
                .attr("stroke","none")
                .attr("fill",d=>{
                    return "#666"
                });
            
            bar.append("text")
                .attr("x", margin.left - 6)
                .attr("y", barStep * (1 - barPadding) / 2)
                .attr("dy", ".35em")
                .attr("fill-opacity", 1)
                .text(d => d.data.name)
                .attr("stroke","none")
                .attr("font-size", 12)
                .attr("font-weight", "bold")
                .attr("fill",d=>{
                    return color(d.data.name)
                });
            
            bar.append("rect")
                .attr("x", x(0))
                .attr("width", d => x(d.value) - x(0))
                .attr("height", barStep * (1 - barPadding));
            
            return bar;
            }

        function stack(i) {
            let value = 0;
            return d => {
              const t = `translate(${x(value) - x(0)},${barStep * i})`;
              value += d.value;
              return t;
            };
          }
    }

    render(){
        return(
            <>
            <svg
              ref="d3_canvas"
              width={this.state.width}
              height={this.state.height}
              viewBox={`0 0 ${this.state.width} ${this.state.height}`}
            />
            </>
        )
    }
}