import React from "react";
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import { Link } from 'react-router-dom';

function BreadCrumbs(props) {

  const {
    items,
  } = props;

  if (!items || !items.length) {
    return "";
  }

  const returnPath = (index) => {
    return items.filter((a, i) => (i <= index)).map(a => a.resourceGroup).reduce((a, b) => (a + '|' + b))
  }

  const eventReturn = (item, index) => {
    if (props.outputEvent) {
      props.outputEvent({ item: item, index: index });

    }
  }

  const length = items.length;

  return (
    <Breadcrumb style={{ marginTop: "1rem" }}>
      {items.map((item, index) => (
        <Breadcrumb.Item key={index} active={(index + 1) === length}>
          {(index + 1) < length && (
            <Link to={`/resourcegroup/${returnPath(index)}`} onClick={() => (eventReturn(item, index))}>
              {item.title}
            </Link>
          )
          }
          {(index + 1) === length && (
            <>
              {item.title}
            </>
          )
          }

        </Breadcrumb.Item >
      ))}
    </Breadcrumb>
  );
}

export default BreadCrumbs;