import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

const container = {
  display: 'flex',
  justifyContent: 'end',
};
const subContainer = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center'
};
const img = {
  width: '100%'

};
const oneTitle = {
  color: 'white'
}
const twoTitle = {
  color: 'white'
}
const hr = {
     
    width: '100%', 
    borderTop: '1px solid white'
}

function MuseoInvitation(props) {
  const [show, setShow] = useState(true);
  const [user, setUser] = React.useState(props.user);
  
  const [image, setImage] = React.useState('/media/cev/pic/logos/logoMuseo.svg');

  useEffect(() => { }, []);

  const getName = () => {
    if (user.name) {
      let name = user.name.split(" ");
      if (name.length == 2) return name[0] + " " + name[1];
      else if (name.length >= 3) return name[0] + " " + name[2];
      else return name[0];
    }
    return "Username";
  };

  return (
    <>
      {show && user.roles && user.roles.includes("invitado") ? (
        <div style={container}>
          <div style={subContainer}>
            <h3 style={oneTitle} >Te invitamos a visitar el <strong><a href="/museo">Archivo</a></strong> del </h3>
            <h2 style={twoTitle}>Esclarecimiento de la Verdad </h2>
            <hr style={hr} />
            <img style={img} src={image} alt="" />
          </div>

        </div>
      ) : (
        ""
      )}
    </>
  );
}
const mapStateToProps = ({ auth: { user } }) => ({
  user,
});

export default withRouter(connect(mapStateToProps)(MuseoInvitation));
