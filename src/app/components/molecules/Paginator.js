import React from "react";
import { Pagination, PageItem } from "react-bootstrap";
import { PAGE_SIZE, PAGES, ELASTIC_LIMIT } from "../../config/const";

class Paginator extends React.Component{

  nextPage(total,active){
    active = parseInt(active);
    let totalPages = Math.ceil(total/PAGE_SIZE) - 2;
    let page = active === 1 ? active : ( active === totalPages + 2 ? Math.ceil((active-2)/(PAGES -2))  : Math.ceil((active-1)/(PAGES -2)) );
    page++;
    let start = (PAGES - 2)*(page-1) + 2;
    return this.searchQuery(start);
  }

  previousPage(total,active){
    active = parseInt(active);
    let totalPages = Math.ceil(total/PAGE_SIZE) - 2;
    let page = active === 1 ? active : ( active === totalPages + 2 ? Math.ceil((active-2)/(PAGES -2))  : Math.ceil((active-1)/(PAGES -2)) );
    page--;
    let start = (PAGES - 2)*(page-1) + 2;
    return this.searchQuery(start);
  }
  
  calculateItems(total,active){
    active = parseInt(active);
    let items = [];
    let totalPages = Math.ceil(total/PAGE_SIZE) - 2;
    let page = active === 1 ? active : ( active === totalPages + 2 ? Math.ceil((active-2)/(PAGES -2))  : Math.ceil((active-1)/(PAGES -2)) );
    let start = (PAGES - 2)*(page-1) + 2;
    let fin = page*(PAGES - 2) <= totalPages ? start + (PAGES - 2) - 1 : totalPages + 1;

    for(let i=start;i<=fin;i++){
      let mode = this.props.mode;
      let role = mode === "callback" ? {role:"button"} : '';
      items.push(
        <PageItem
          onClick={(evt)=>{
            if(this.props.callback){
              this.props.callback(i)
            }
          }}
          {...role}
          key={i} 
          active={i === active}
          href={mode === "callback" ? "#" : this.searchQuery(i)}
        >
          {i}
        </PageItem>
      );
    }
    return items;
  }

  searchQuery(from){
    if(this.props.mode === "callback"){
      return from;
    }
    let query = this.props.query;
    let pathName = this.props.pathname;
    query.set("from",from);
    return pathName+"?"+query.toString();
  }

  render(){
    const from = parseInt(this.props.query.get("from")),
          total = this.props.total <= ELASTIC_LIMIT ? this.props.total : ELASTIC_LIMIT,
          mode = this.props.mode,
          role = mode === "callback" ? {role:"button"} : '';

    const end = Math.ceil(total/PAGE_SIZE);
    const start = 1;
    const page = from === 1 ? from : ( from === end + 2 ? Math.ceil((from-2)/(PAGES -2))  : Math.ceil((from-1)/(PAGES -2)) );

    return(
      <Pagination size={"lg"}>
        {end > start &&
          <Pagination.First
            onClick={(evt)=>{
              if(this.props.callback){
                this.props.callback(start)
              }
            }}
            {...role}
            href={mode === "callback" ? "#" : this.searchQuery(start)}
            disabled={from===start} 
          />
        }

        {end > start &&
          <Pagination.Prev
            onClick={(evt)=>{
              if(this.props.callback){
                this.props.callback(from-1)
              }
            }}
            {...role}
            href={mode === "callback" ? "#" : this.searchQuery(from-1)}
            disabled={from===start}  
          />
        }
        
        <Pagination.Item 
          onClick={(evt)=>{
            if(this.props.callback){
              this.props.callback(start)
            }
          }}
          {...role}
          href={mode === "callback" ? "#" : this.searchQuery(start)}
          active={start === from} 
        >
            {start}
        </Pagination.Item>
        
        {end > PAGES && page > 1 &&
          <Pagination.Ellipsis
            onClick={(evt)=>{
              if(this.props.callback){
                this.props.callback(this.previousPage(total,from))
              }
            }}
            {...role}
            href={mode === "callback" ? "#" : this.previousPage(total,from)}
            disabled={from<=(PAGES-1)}
          />
        }

        { /* PAGINAS*/ }
        {total > PAGE_SIZE*2 && 
          this.calculateItems(total,from)
        }
        
        {end > PAGES && (page*PAGE_SIZE) < end  &&
          <Pagination.Ellipsis
            onClick={(evt)=>{
              if(this.props.callback){
                this.props.callback(this.nextPage(total,from))
              }
            }}
            {...role}
            href={mode === "callback" ? "#" : this.nextPage(total,from)}
            disabled={from>=end-(PAGES-1)}
          />
        }
        
        {end > start &&
          <Pagination.Item
            onClick={(evt)=>{
              if(this.props.callback){
                this.props.callback(end)
              }
            }}
            {...role}
            href={mode === "callback" ? "#" : this.searchQuery(end)}
            active={end === from}
          >
            {end}
          </Pagination.Item>
         }

         {end > start &&  
          <Pagination.Next
            onClick={(evt)=>{
              if(this.props.callback){
                this.props.callback(from+1)
              }
            }}
            {...role}
            href={mode === "callback" ? "#" : this.searchQuery(from+1)}
            disabled={from===end} 
          />
        }

        {end > start &&
          <Pagination.Last
            onClick={(evt)=>{
              if(this.props.callback){
                this.props.callback(end)
              }
            }}
            {...role}
            href={mode === "callback" ? "#" : this.searchQuery(end)}
            disabled={from===end} 
          />
        }
      </Pagination>
    )
  }
}

export default Paginator;