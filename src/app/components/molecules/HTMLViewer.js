import React, { Component } from "react";
import {connect} from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import ReactWaterMark from 'react-watermark-component';
import ControlBar from "../molecules/ControlBar";
import PerfectScrollbar from "react-perfect-scrollbar";

class HTMlViewer extends Component {
  constructor () {
    super()

    this.state = {
      text: ''
    }
  }

  componentDidMount() {
    this.convertBlobToJSON();
  }

  convertBlobToJSON() {
    let reader = new FileReader();
    reader.onload = (evt) => {
      this.setState({ text: evt.target.result });
    }
    reader.readAsText(this.props.file);
  }

  render() {
    const {
      name
    } = this.props.user;

    const options = {
      chunkWidth: 450,
      chunkHeight: 90,
      textAlign: 'left',
      textBaseline: 'bottom',
      globalAlpha: 0.2,
      font: '36px Microsoft Yahei',
      rotateAngle: -0.1,
      fillStyle: '#666'
    }

    return (
      <>
        <Container className="bg-light">
          <Row>
            <Col xs={12}>
              <ReactWaterMark
                waterMarkText={name}
                options={options}
              >
                <ControlBar user={this.props.user.name} config={{themes:true,textUP:true,textDOWN:true,fontFamily:true,fontSize:true,fullScreen:true}} container={"cev-testimony"}> 
                  <PerfectScrollbar
                    className="cev-scroll"
                    options={{
                      wheelSpeed: 1,
                      wheelPropagation: false
                    }}
                    style={{ height: "calc(100vh - 240px)"}}
                    data-scroll="true"
                  >
                    <Container
                      id="cev-testimony" 
                      className="text-justify"
                      dangerouslySetInnerHTML={{
                        __html: this.state.text
                      }}
                      fluid={true}
                    />
                  </PerfectScrollbar>
                </ControlBar>
              </ReactWaterMark>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps
)(HTMlViewer);