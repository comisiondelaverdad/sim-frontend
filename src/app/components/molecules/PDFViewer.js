import React, { Component } from "react";
import {connect} from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import ReactWaterMark from 'react-watermark-component';
import ControlBar from "../molecules/ControlBar";
import PDF, { Worker, defaultLayout } from '@phuocng/react-pdf-viewer';
import '@phuocng/react-pdf-viewer/cjs/react-pdf-viewer.css';
import { PDF_WORKER } from "../../config/const"; 

class PDFViewer extends Component {
  constructor(props){
    super(props);
    this.renderToolbar = this.renderToolbar.bind(this);
  }

  renderToolbar = (toolbarSlot) => {
    return (
      <div
        style={{
          alignItems: 'center',
          display: 'flex',
          width: '100%',
        }}
      >
        <div
          style={{
            alignItems: 'center',
            display: 'flex',
          }}
        >
          <div style={{ padding: '0 2px' }}>
            {toolbarSlot.searchPopover}
          </div>
          <div style={{ padding: '0 2px' }}>
            {toolbarSlot.previousPageButton}
          </div>
          <div style={{ padding: '0 2px' }}>
            {toolbarSlot.currentPageInput} / {toolbarSlot.numPages}
          </div>
          <div style={{ padding: '0 2px' }}>
            {toolbarSlot.nextPageButton}
          </div>
        </div>
        <div
          style={{
            alignItems: 'center',
            display: 'flex',
            flexGrow: 1,
            flexShrink: 1,
            justifyContent: 'center',
          }}
        >
          <div style={{ padding: '0 2px' }}>
            {toolbarSlot.zoomOutButton}
          </div>
          <div style={{ padding: '0 2px' }}>
            {toolbarSlot.zoomPopover}
          </div>
          <div style={{ padding: '0 2px' }}>
            {toolbarSlot.zoomInButton}
          </div>
        </div>
        <div
          style={{
            alignItems: 'center',
            display: 'flex',
            marginLeft: 'auto',
          }}
        >
          <div style={{ padding: '0 2px' }}>
            {toolbarSlot.moreActionsPopover}
          </div>
        </div>
      </div>
    );
  };
  
  render() {
    const {
      name
    } = this.props.user;

    const options = {
      chunkWidth: 450,
      chunkHeight: 90,
      textAlign: 'left',
      textBaseline: 'bottom',
      globalAlpha: 0.2,
      font: '36px Microsoft Yahei',
      rotateAngle: -0.1,
      fillStyle: '#666'
    }

    const layout = (
      isSidebarOpened,
      container,
      main,
      toolbar,
      sidebar,
    ) => {
        return defaultLayout(
            isSidebarOpened,
            container,
            main,
            toolbar(this.renderToolbar),
            sidebar,
        );
    };

    return (
      <>
        <Container fluid={true} className="bg-light">
          <Row>
            <Col xs={12}>
              <ReactWaterMark
                waterMarkText={name}
                options={options}
                onContextMenu={(evt)=>{evt.preventDefault();}}
              >
                <ControlBar user={this.props.user.name} config={{themes:false,textUP:false,textDOWN:false,fontFamily:false,fontSize:false,fullScreen:true}} container={"cev-testimony"}>
                  <Container className="cev-pdf" fluid={true} key={this.props.id}>
                    <Worker workerUrl={"https://unpkg.com/pdfjs-dist@"+PDF_WORKER+"/build/pdf.worker.min.js"}>
                      <PDF
                        fileUrl={URL.createObjectURL(this.props.file)}
                        layout={layout}
                      />
                    </Worker>
                  </Container>
                </ControlBar>
              </ReactWaterMark>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps
)(PDFViewer);