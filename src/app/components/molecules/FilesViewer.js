import React, { Component } from "react";
import {connect} from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import ReactWaterMark from 'react-watermark-component';
import FileViewer from 'react-file-viewer';

class FilesViewer extends Component {
  state = {};

  render() {
    const {
      name
    } = this.props.user;

    const options = {
      chunkWidth: 450,
      chunkHeight: 90,
      textAlign: 'left',
      textBaseline: 'bottom',
      globalAlpha: 0.2,
      font: '36px Microsoft Yahei',
      rotateAngle: -0.1,
      fillStyle: '#666'
    }
    
    return (
      <>
        <Container className="bg-light">
          <Row>
            <Col xs={12}>
                <ReactWaterMark
                  waterMarkText={name}
                  options={options}
                  onContextMenu={(evt)=>{evt.preventDefault();}}
                >
                  <Container key={this.props.id}>
                    <FileViewer
                      key={this.props.id}
                      filePath={URL.createObjectURL(this.props.file)}
                      fileType={this.props.type}
                    />
                  </Container>
                </ReactWaterMark>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps
)(FilesViewer);