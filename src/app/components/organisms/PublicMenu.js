import React, { useState, useEffect } from "react";
import * as MenusService from "../../services/MenusService";
import ItemPublicMenu from "../molecules/ItemPublicMenu";
import { Link } from "react-router-dom";

function PublicMenu(props) {
  const [elements, setElements] = useState([]);
  const [items, setItems] = useState([]);

  const [logo, setLogo] = useState([]);
  const [active, setActive] = useState();

  useEffect(() => {
    loadItems(props.section);
  }, []);
  useEffect(() => {
    renderItems();
  }, [elements, active]);

  const set_active = (id) => {
    setActive(id);
  };

  const renderItems = () => {
    if (elements) {
      getLogo(elements);
      const items = elements.map((item) => {
        return (
          <ItemPublicMenu
            active={(active == item.id)              
                ? "cev-menu__item--active cev-menu__item--here"
                : ""
            }
            set_active={set_active}
            key={item.id}
            item={item}
          />
        );
      });

      setItems(items);
    } else {
      console.log("no existen elementos");
    }
  };

  const loadItems = async (section) => {
    let res = await MenusService.getBySection(section);
    if (res.elements) {
      setElements(res.elements);
    }
  };

  const getLogo = (elements) => {
    let logo = elements.filter((item) => item.img);
    if (logo.length) {
      if (!logo[0].route) {
        logo[0].route = "/";
      }
      setLogo(logo[0]);
    }
  };

  return (
    <>
      <div className="cev-header-menu-wrapper">
        {logo && (
          <div className="cev-header-logo">
            <Link to={logo.route ? logo.route : "/"}>
              <img alt={logo.tag} src={logo.img} />
            </Link>
          </div>
        )}
        <div
          id="cev_header_menu"
          className={`cev-header-menu cev-header-menu-mobile cev-header-menu cev-header-menu-mobile cev-header-menu--layout-tab ${props.classMenu}`}
          // ref={this.ktMenuCommonRef}
        >
          <ul className={`cev-menu__nav ${props.classMenu}`}>
            {items && items}
          </ul>
        </div>
      </div>
    </>
  );
}
export default PublicMenu;
