import React, {useState} from "react";
import { Dropdown, Alert } from "react-bootstrap";
import DropDownIcon from "../molecules/DropDownIcon";

function ConfirmMenu(props){
  const [show, setShow] = useState(false);
  let title = props.title ? props.title : "MetaBuscador CEV dice:";
  let message = props.message ? props.message : "¿Está seguro desea continuar?";

  return(
    <Dropdown
      alignRight
      drop="down"
      show={show}
      onToggle={()=>{setShow(false)}}
    >
      <Dropdown.Toggle 
        as={DropDownIcon}
        onClick={()=>{setShow(true)}}
      >
        <button type="button" className="btn btn-icon btn-outline-danger">
          <i className="flaticon2-trash"></i>
        </button>
      </Dropdown.Toggle>
      <Dropdown.Menu className="dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl cev-bookmarks">
        <div className="cev-portlet">
          <div className="cev-portlet__head">
            <div className="cev-portlet__head-label">
              <h3 className="cev-portlet__head-title cev-font-primary">
                {title}
              </h3>
            </div>
            <div className="cev-portlet__head-toolbar">
              <div className="cev-portlet__head-actions">
                <button type="button" className="btn btn-outline-secondary btn-icon btn-sm" onClick={()=>{setShow(false)}}>
                  <i className="flaticon2-cross"></i>
                </button>
              </div>
            </div>
          </div>
          <div className="cev-portlet__body">
            <Alert variant="danger">{message}</Alert>
          </div>
          <div className="cev-portlet__foot">
            <div className="row">
              <div className="col-12 cev-align-right">
                <button type="button" className="btn btn-outline-danger" onClick={()=>{setShow(false)}}>
                  <i className="flaticon2-cross"></i>
                  Cancelar
                </button>
                <button type="button" className="btn btn-outline-success" onClick={()=>{props.confirmAction()}}>
                  <i className="flaticon2-check-mark"></i>
                  Si
                </button>
              </div>
            </div>
          </div>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  )
}

export default ConfirmMenu;