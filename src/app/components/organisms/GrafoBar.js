import React, { Component } from 'react'
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import * as VizService from "../../services/VizService";
import PropTypes from 'prop-types';
import Grafo from '../molecules/Grafo';
import Lombardi from '../molecules/Lombardi';
import * as d3 from 'd3'
import Lottie from 'react-lottie';
import animationData from '../../../assets/viz-loading.json'

/**
 * Bloque que muestra un grafo
 * @version 0.1
 */
class GrafoBar extends Component {
  static propTypes = {
    /** URL string location */
    location: PropTypes.object
  }

  constructor(props) {
    super(props)
    this.state = {
      network: null,
      total: 0,
      moreInfo: false,
      loading: true,
      data: null,
      selected: [],
      linkedByIndex: {},
      selectedGroup: [''],
      view: 'lombardi'
    };

    this.moreInfo = this.moreInfo.bind(this)
    this.changeSelected = this.changeSelected.bind(this)
    this.organizeData = this.organizeData.bind(this)
    this.changeView = this.changeView.bind(this)
  }

  componentDidMount() {
    this.color = d3.scaleOrdinal(d3.schemeTableau10)
    if (this.props.location) {
      let query = new URLSearchParams(this.props.location.search);

      if (query.toString() !== '') {
        this.updateGrafo(this.props.searchFilters);
      }
    } else if (this.props.bookmarks) {
      let idArray = []
      this.props.bookmarks.map(p => {
        idArray.push(p.path)
        return ''
      })

      let params = {
        idArray: idArray
      }

      this.updateGrafo(params)
    } else if (this.props.resource) {

      let params = {
        rg: this.props.resource[0]
      }

      this.updateGrafo(params)
    } else {
      let params = {
        origin: "ModuloCaptura"
      }

      if (this.props.filtros !== undefined) {
        if (this.props.filtros[0].filtros.length > 0) params.ngramFilter = this.props.filtros[0].filtros
        if (this.props.filtros[1].filtros.length > 0) params.etiquetasFilter = this.props.filtros[1].filtros
      }

      this.updateGrafo(params)
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location) {
      let oldQuery = new URLSearchParams(prevProps.location.search).toString();
      let query = new URLSearchParams(this.props.location.search);

      if (query.toString() !== '') {
        if (query.toString() !== oldQuery.toString()) {
          this.updateGrafo(this.props.searchFilters);
        }
      }
    }
  }

  /**
   * Actualiza los datos que reciben del back
   * 
   * @param {string} search 
   * @param {string} resourceGroup 
   */
  updateGrafo(filters) {
    if (this.props.total === 10000) {
      VizService.totalSearchHits(filters, true)
        .then(
          (data) => {
            var resp = data.hits.total.value

            this.setState({
              total: resp
            })
          },
          (error) => {
            console.log(error)
            //this.props.history.push("/logout");
          }
        )
    } else {
      this.setState({
        total: this.props.total
      })
    }

    VizService.grafoEntidades(filters)
      .then(
        (data) => {
          this.setState({
            loading: false
          })

          let buckets = data.aggregations.tipo.connections.total.buckets
          let arrayTemp = []
          let arrayGroups = []
          let nodos = []

          if (data.hits.hits.length > 0) {
            let max = buckets[0].doc_count
            // let min = buckets[buckets.length - 1].doc_count
            let links = []

            buckets.map(b => {

              let e = b.key.split('---')

              let link = {
                "source": null,
                "target": null,
                "value": b.doc_count
              }

              e.map((n, i) => {
                let _nodos = n.replace(/\s+/g, ' ').trim().split('||')
                let index = null

                if (!arrayTemp.includes(_nodos[1].replace(/\s+/g, ' ').trim())) {
                  var resp = {
                    "name": _nodos[1].replace(/\s+/g, ' ').trim(),
                    "group": _nodos[0].replace(/\s+/g, ' ').trim(),
                    "index": nodos.length,
                    "value": b.doc_count
                  }

                  index = nodos.length

                  nodos.push(resp)
                  arrayTemp.push(_nodos[1].replace(/\s+/g, ' ').trim())
                } else {
                  index = arrayTemp.indexOf(_nodos[1].replace(/\s+/g, ' ').trim())
                  nodos[index]['value'] += b.doc_count
                }

                if (!arrayGroups.includes(_nodos[0])) arrayGroups.push(_nodos[0])

                if (i === 0) {
                  link.source = index
                } else {
                  link.target = index
                }

                return ''
              })

              links.push(link)
              return ''
            })

            let por = (data.hits.total.value / this.state.total) * 100
            por = por.toFixed(2)

            let network = {
              "node-data": nodos,
              "link-data": links,
              "max": max,
              "groups": arrayGroups.sort((a, b) => a.localeCompare(b, 'en', { 'sensitivity': 'base' }))
            }

            this.setState({
              network: network,
              data: network,
              percent: por
            })
          }

        },
        (error) => {
          console.log(error)
          //.props.history.push("/logout");
        }
      )
  }

  organizeData() {
    let network = { ...this.state.data }
    let arrayNodos_ = []
    // let arrayLinks_ = []

    if (this.state.selected.length === 0) {
      this.setState({
        network: network
      })
    } else {
      this.state.selected.map(d => {
        let resp = this.state.data['node-data'].find(i => {
          return i.name === d
        })
        arrayNodos_.push(resp)

        network['node-data'] = arrayNodos_

        this.setState({
          network: network
        })

        return ''
      })

    }
  }

  /**
   * Calcula el mapeo de un número dándole un rango de entrada y de salida.
   *
   * @param {int} i input.
   * @param {int} r1 rango mínimo del input.
   * @param {int} r2 rango máximo del input.
   * @param {int} m1 rango mínimo del output.
   * @param {int} m2 rango máximo del output.
   * @returns {float} el mapeo del input entre los rangos especificados del output.
   */
  scaleLinear(i, r1, r2, m1, m2) {
    var resp = (i - r1) * (m2 - m1) / (r2 - r1) + m1
    return resp
  }

  moreInfo() {
    if (this.state.moreInfo) {
      this.setState({
        moreInfo: false
      })
    } else {
      this.setState({
        moreInfo: true
      })
    }
  }

  /**
   * Actualiza las entidades por click
   * 
   * @param {*} e 
   */
  changeSelected(e) {
    let val = e.target.getAttribute('value')
    if (this.state.selected.includes(val)) {
      let array = this.state.selected
      let index = array.indexOf(val)
      array.splice(index, 1)
      this.setState({
        selected: array
      })
    } else {
      let array = this.state.selected
      array.push(val)
      this.setState({
        selected: array
      })
    }
  }

  changeView(event) {
    if (this.state.view !== event.target.getAttribute('value')) {
      this.setState({
        view: event.target.getAttribute('value')
      })
    }
  }

  render() {
    let nodos_sorted = null
    if (this.state.data)
      nodos_sorted = this.state.data['node-data']

    return (
      <>
        {this.state.loading &&
          <div className="loading_viz">
            <Lottie height={150} width={150} options={
              {
                loop: true,
                autoplay: true,
                animationData: animationData,
                rendererSettings: {
                  preserveAspectRatio: 'xMidYMid slice'
                }
              }
            } />
          </div>
        }
        {this.state.network === null && !this.state.loading &&
          <>
            <span class="badge badge-danger">Los resultados no tienen los datos suficientes para la visualización</span>
          </>
        }
        {this.state.network &&
          <>
            <div className="wordcloudBar grafo">
              <div className="cloudContainer">
                {/* <div className="info">
                  <b>{this.state.percent}%</b> {this.props.info}:
                      </div> */}
                <div className="viewSelect">
                  <div value="grado" className={this.state.view === 'grado' ? 'item active' : 'item'} onClick={this.changeView}>Centralidad de grado</div>
                  <div value="lombardi" className={this.state.view === 'lombardi' ? 'item active' : 'item'} onClick={this.changeView}>Lombardi</div>
                </div>
                <div className="inside">
                  {this.state.view === 'grado' &&
                    <Grafo color={this.color} network={this.state.network} selected={this.state.selected} />
                  }
                  {this.state.view === 'lombardi' &&
                    <Lombardi color={this.color} network={this.state.network} selected={this.state.selected} />
                  }
                  <div className="listado">
                    {nodos_sorted.map(n => {
                      let style = {
                        backgroundColor: this.color(n.group)
                      }
                      return (
                        <div
                          className={this.state.selected.length === 0 || this.state.selected.includes(n.name) ? "item active" : "item notactive"}
                          onClick={this.changeSelected}
                          value={n.name}
                        >
                          <div className="icon" style={style}></div>
                          {n.name}
                        </div>
                      )
                    })}
                  </div>
                </div>

                <div className="groups">
                  {this.state.network['groups'].map(n => {
                    let style = {
                      backgroundColor: this.color(n)
                    }
                    return (
                      <div className="item">
                        <div className="icon" style={style}></div>
                        {n}
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </>
        }
      </>
    )
  }
}

const mapStateToProps = store => ({
  searchFilters: store.app.filters
});

export default connect(
  mapStateToProps,
  app.actions
)(GrafoBar);