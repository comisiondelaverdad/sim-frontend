import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import ExpandMore from "@material-ui/icons/ExpandMore";
import ExpandLess from "@material-ui/icons/ExpandLess";
import IconButton from "@material-ui/core/IconButton";
import { toAbsoluteUrl } from "../../../theme/utils";
import * as Icons from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  subItemsDesktop: {
    display: "flex",
    flexDirection: "column",
    // alignItems: "center",
    marginLeft: "50px",
  },
  subItemsDesktopNoIcon: {
    display: "flex",
    flexDirection: "column",
    // alignItems: "center",
    marginLeft: "75px",
  },
  itemMenu: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  },
  subItem: {
    color: "black",
    marginBottom: "5px",
  },
  subItemWhite: {
    color: "#ffffffbf",
    marginBottom: "5px",
  },
  itemPrincipal: {
    fontWeight: "600",
  },
}));

const Icon = (props) => {
  return (
    <div>
      <svg className="cv-small">
        <use
          xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#" + props.name)}
          className={"flia_2"}
        ></use>
      </svg>
    </div>
  );
};

const MakeIcon = (item, section) => {
  item.tag = item.tag.trim();
  if (item.icon) return <Icon name={item.icon} />;
  else if (item.material_icon) {
    let color = "var(--terciario)";
    let my_style = {
      fontSize: "20",
      color: color,
    };

    if (
      Icons[item.material_icon.trim()] &&
      React.createElement(Icons[item.material_icon.trim()])
    ) {
      if (section == "footer")
        return React.createElement(
          Icons[item.material_icon.replace("TwoTone", "Outlined").trim()],
          {
            style: my_style,
            color: "primary",
          }
        );

      return React.createElement(Icons[item.material_icon.trim()], {
        style: my_style,
        color: "primary",
      });
    }
    return (
      <FiberManualRecordIcon
        fontSize="small"
        style={{ color: "rgb(218, 218, 218)" }}
      />
    );
  }
};

const ItemMenu = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [open, setOpen] = React.useState(false);
  const [padding, setPadding] = React.useState(
    props.padding ? props.padding : 16
  );

  useEffect(() => {
    if (props.padding) setPadding(props.padding + 5);
  }, []);

  const handleClick = () => {
    console.log("menu", open);
    setOpen(!open);
  };

  const arrow = () => {
    if (props.item.children && props.item.children.length) {
      if (open)
        return (
          <IconButton onClick={handleClick}>
            <ExpandLess />
          </IconButton>
        );
      else
        return (
          <IconButton onClick={handleClick}>
            <ExpandMore />
          </IconButton>
        );
    }
  };
  const route = (route) => {
    if (route === "/") return "/";
    else if (route === "") return "#";
    else return "/" + route;
  };

  const collapsingDesktop = () => {
    return props.item.children.map((item) => (
      <Link
        className={
          props.section == "footer" ? classes.subItemWhite : classes.subItem
        }
        to={`/${item.route}`}
      >
        {item.tag}
      </Link>
    ));
  };

  const collapsing = () => {
    return (
      <Collapse
        in={open}
        timeout="auto"
        unmountOnExit
      >
        <List component="div">
          {props.item.children &&
            props.item.children.length &&
            props.item.children.map((item) => {
              return (
                <ItemMenu
                  key={item.id}
                  child={true}
                  padding={padding}
                  item={item}
                  activate={props.activate}
                  active={props.active}
                />
              );
            })}
        </List>
      </Collapse>
    );
  };

  return (
    <>
      <div>
        <ListItem
          style={{
            paddingLeft: padding + "px",
            paddingBottom: "3px",
            paddingTop: "3px",
            // backgroundColor: (props.active == props.item.id)? '':''
          }}
        >
          <ListItemIcon>
            {props.item.icon || props.item.material_icon &&  (
              MakeIcon(props.item, props.section)
            )}
          </ListItemIcon>
          <ListItem
            onClick={() => {
              props.activate(props.item.id);
              history.push(route(props.item.route));
              handleClick();
            }}
            button
            exact
            to={route(props.item.route)}
            target={props.item.tab ? "_blank" : ""}
            style={{
              color: "black",
              paddingLeft: "0px",
              paddingBottom: "3px",
              paddingTop: "3px",
            }}
          >
            <ListItemText primary={props.item.tag} />
          </ListItem>
          {arrow()}
        </ListItem>
        {props.item.children && props.item.children.length
          ? collapsing()
          : ""}
      </div>
    </>
  );
};
export default ItemMenu;
