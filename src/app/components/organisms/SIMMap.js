import React, { Component } from 'react';
import { Map, TileLayer, Marker } from 'react-leaflet';

class SIMMap extends Component {

  constructor(props) {
    super(props);

    this.state = {
      z: this.props.zoom ? this.props.zoom : 5,
      c: this.props.center ? this.props.center : [4.36, -74.04],
      anteriorH: -1,
      anteriorW: -1,
      minHeigth: this.props.minHeigth ? this.props.minHeigth : 450
    }
  }

  update() {
    setTimeout(() => {
      let ref = this.map
      if (ref != null) {
        let h = ref.clientHeight
        if (h < this.state.minHeigth)
          h = this.state.minHeigth
        if (this.state.anteriorH === -1 || this.state.anteriorH === 0 || this.state.anteriorW === -1 || this.state.anteriorW === 0) {
          this.setState({ height: h, width: ref.clientWidth, anteriorH: ref.clientHeight, anteriorW: ref.clientWidth })
        } else {
        }
      }
    }, 1)
  }

  componentDidUpdate(nextProps, nextState) {
    let ref = this.map
    if (
      ref.clientHeight > 0 &&
      ref.clientWidth > 0 &&
      (this.state.height === 0 || this.state.width === 0)
    ) {
      setTimeout(() => {
        this.setState({ height: ref.clientHeight, width: ref.clientWidth, anteriorH: ref.clientHeight, anteriorW: ref.clientWidth })
      }, 500)
    }
  }

  componentDidMount() {
    this.update()
  }

  render() {
    return (
      <>
        <div ref={el => this.map = el} className="mw-100">
          {
            this.state.height &&
            <Map style={{ width: this.state.width, height: this.state.height }} center={this.state.c} zoom={this.state.z} scrollWheelZoom={false}>
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
              />
              <Marker position={this.props.records}></Marker>
            </Map>
          }
        </div>
      </>
    )
  }
}

export default SIMMap;