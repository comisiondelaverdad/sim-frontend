import React from "react";
import { connect } from "react-redux";
import { Dropdown, Alert } from "react-bootstrap";
import DropDownIcon from "../molecules/DropDownIcon";
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import * as BookmarksService from "../../services/BookmarksService"
import _ from "lodash"
import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarIcon from '@mui/icons-material/Star';
import CloseIcon from '@mui/icons-material/Close';
class BookmarksMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: '',
      data: [],
      selected: [],
      bookmarked: false,
      isLoading: false,
      show: false,
      alert: false
    };

    this.saveBookmark = this.saveBookmark.bind(this);
    this.deleteBookmark = this.deleteBookmark.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.loadLabels = this.loadLabels.bind(this);
  }

  handleChange(newTag) {
    let selected = [];
    for (let i = 0; i < newTag.length; i++) {
      selected[i] = newTag[i].label ? newTag[i].label : newTag[i];
    }

    this.setState({
      selected: selected,
      alert: false
    });
  }

  saveBookmark() {
    this.setState({ alert: false });
    if (this.state.selected.length !== 0) {

      this.props.extra.highlight = _.mapKeys(this.props.extra.highlight, function (value, key) {
        return key.replace(/[.]/g, '');
      });

      let bookmark = {
        "user": this.props.user,
        "path": this.props.extra.ident,
        "title": this.props.extra.title ? this.props.extra.title : '',
        "labels": this.state.selected,
        "description": this.props.extra.highlight,
        "extra": {
          "temporalCoverage": {
            "start": this.props.extra.temporalCoverage ? this.props.extra.temporalCoverage.start : '',
            "end": this.props.extra.temporalCoverage ? this.props.extra.temporalCoverage.end : '',
          },
          "geographicCoverage": {
            "originalLocation": this.props.extra.geographicCoverage ? this.props.extra.geographicCoverage.originalLocation : ''
          }
        }
      }

      if (!this.state.bookmarked) {
        BookmarksService.serviceAdd(bookmark)
          .then(
            (data) => {
              this.setState({ bookmarked: true, id: data._id, show: false });
            },
            (error) => {
              console.log("Error", error);
            }
          )
      }
      else {
        BookmarksService.serviceUpdate(this.state.id, bookmark).then(
          (data) => {
            this.setState({ show: false });
          },
          (error) => {
            console.log("Error", error);
          }
        )
      }
    }
    else {
      this.setState({ alert: true });
    }
  }

  deleteBookmark() {
    if (this.state.id) {
      BookmarksService.serviceDelete(this.state.id)
        .then(
          (data) => {
            this.setState({ id: '', selected: [], bookmarked: false, show: false });
            //this.typeahead.getInstance().clear();
          },
          (error) => {
            console.log("Error", error);
          }
        )
    } else {
      this.setState({ show: false })
    }
  }

  loadLabels() {
    this.setState({ alert: false });
    BookmarksService.serviceLabelsByUser(this.props.user)
      .then(
        (data) => {
          this.setState({ data: data });
          this.setState({ show: true });
        },
        (error) => {
          console.log("Error", error);
        }
      )

  }

  componentDidMount() {
    let bookmark = this.props.extra.bookmarks.filter(x => x.user === this.props.user).find(x => x.path === this.props.extra.ident);
    if (bookmark) {
      this.setState({ bookmarked: true, selected: bookmark.labels, id: bookmark._id });
    }
  }

  render() {
    const {
      data,
      selected,
      bookmarked,
      isLoading,
      show,
      alert
    } = this.state;

    return (
      <Dropdown
        alignRight
        drop="down"
        show={show}
        onToggle={() => { this.setState({ show: false }) }}
      >
        <Dropdown.Toggle
          as={DropDownIcon}
          onClick={this.loadLabels}>
          <div className="bookmarksIcon">
            {!bookmarked && <StarBorderIcon fontSize="large"></StarBorderIcon>}
            {bookmarked && <StarIcon fontSize="large"></StarIcon>}
          </div>
        </Dropdown.Toggle>
        <Dropdown.Menu className="dropdown-menu-fit dropdown-menu dropdown-menu-anim dropdown-menu-xl cev-bookmarks">

          <div className="cev-bookmarks-content">
            <div className="label title">
              Bookmarks
              <button type="button" className="btn btn-outline-secondary btn-icon btn-sm" onClick={() => { this.setState({ show: false }) }}>
                <CloseIcon></CloseIcon>
              </button>
            </div>
            <div className="content">
              <Typeahead
                className="cev-typehead"
                id="bookmarks"
                allowNew
                clearButton
                multiple
                options={data}
                placeholder="Selecciona / Crea Etiquetas"
                newSelectionPrefix="Nueva etiqueta:&nbsp;"
                selectHintOnEnter
                onChange={this.handleChange}
                isLoading={isLoading}
                searchText="Cargando etiquetas"
                promptText="Buscar etiquetas"
                emptyLabel="No se encontraron etiquetas"
                defaultSelected={selected}
                ref={(typeahead) => this.typeahead = typeahead}
              />
              <Alert variant="danger" show={alert}>Debe crear/seleccionar una etiqueta</Alert>
            </div>
            <div className="portlet__foot">
              <div className="row">
                <div className="col-12">
                  <button type="button" className="btn btn-outline-danger" onClick={this.deleteBookmark}>
                    Eliminar
                  </button>
                  <button type="button" className="btn btn-outline-success" onClick={this.saveBookmark}>
                    Guardar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    )
  }

}

const mapStateToProps = store => ({
  user: store.auth.user._id
});

export default connect(
  mapStateToProps
)(BookmarksMenu);