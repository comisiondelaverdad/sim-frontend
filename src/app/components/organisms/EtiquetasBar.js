import React, { Component } from 'react'
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import * as VizService from "../../services/VizService";
import PropTypes from 'prop-types';
import Etiquetas from '../molecules/Etiquetas';
import ArbolVista from './VizOrganisms/ArbolVista';
import * as d3 from 'd3';
import Lottie from 'react-lottie';
import animationData from '../../../assets/viz-loading.json'
import TabedCard from '../ui/TabedCard'


const EtiquetasTabed = props => {
    return (
        <>
            <TabedCard
                active={props.active}
                intro=""
                list={[
                    {
                        slug: "arbol",
                        name: "Árbol",
                        component: <ArbolVista changeFilter={props.changeFilter} active={props.arbolActive} root={props.roots} />,
                        icono: <></>
                    },
                    {
                        slug: "barras",
                        name: "Barras",
                        component: <Etiquetas root={props.roots} />,
                        icono: <></>
                    }
                ]}
            />
        </>
    )

}

/**
 * Bloque que muestra barras de etiquetas
 * @version 0.1
 */
class EtiquetasBar extends Component {
    static propTypes = {
        /** URL string location */
        location: PropTypes.object.isRequired,
        /** Total de hits para la búsqueda realizada */
        total: PropTypes.number.isRequired
    }

    constructor(props) {
        super(props)

        this.state = {
            loading: true,
            root: null,
            view: 'tree'
        }

        this.changeView = this.changeView.bind(this)
    }

    componentDidMount() {
        if (this.props.location) {
            let query = new URLSearchParams(this.props.location.search);

            if (query.toString() !== '') {
                this.updateGrafo(this.props.location);
            }
            console.log(this.props.searchFilters)

        } else if (this.props.bookmarks) {
            let idArray = []
            this.props.bookmarks.map(p => {
                idArray.push(p.path)
                return ''
            })

            let params = {
                idArray: idArray
            }

            this.updateGrafo(params)
        } else if (this.props.resource) {
            let params = {
                rg: this.props.resource[0]
              }

            this.updateGrafo(params)
        } else {
            let params = {
                origin: "ModuloCaptura"
            }



            if (this.props.filtros !== undefined) {
                if (this.props.filtros[0].filtros.length > 0) params.ngramFilter = this.props.filtros[0].filtros
                if (this.props.filtros[1].filtros.length > 0) params.etiquetasFilter = this.props.filtros[1].filtros
                if (this.props.filtros[2].filtros.length > 0) params.entidadesFilter = this.props.filtros[2].filtros
            }

            this.updateGrafo(params)
        }
    }

    /**
 * Actualiza los datos que reciben del back
 * 
 * @param {string} search 
 * @param {string} resourceGroup 
 */
    updateGrafo(filters) {
        if (this.props.total === 10000) {
            VizService.totalSearchHits(filters, true)
                .then(
                    (data) => {
                        var resp = data.hits.total.value

                        this.setState({
                            total: resp
                        })
                    },
                    (error) => {
                        console.log(error)
                        //this.props.history.push("/logout");
                    }
                )
        } else {
            this.setState({
                total: this.props.total
            })
        }

        console.log(filters)

        VizService.getEtiquetas(filters)
            .then(
                (data) => {
                    this.setState({
                        loading: false
                    })
                    console.log(data)

                    // var resp = false
                    let buckets = data.aggregations.tipo.etiquetas.buckets

                    if (data.hits.hits.length > 0) {
                        //   let max = buckets[0].doc_count
                        //   let min = buckets[buckets.length - 1].doc_count
                        //     let total = data.aggregations.tipo.etiquetas.sum_other_doc_count
                        let arreglo = []

                        buckets.map(b => {
                            let splited = b.key.split(' - ')

                            let obj = {}
                            let objTemp = {}
                            splited.map((s, i) => {
                                let obj_ = {
                                    "name": s
                                }
                                if (i === 0) {
                                    obj = obj_
                                    obj["children"] = []
                                    objTemp = obj
                                }
                                else if (i < splited.length) {
                                    obj_["children"] = []
                                    objTemp["children"].push(obj_)
                                    objTemp = obj_
                                }

                                if (i === splited.length - 1) {
                                    obj_["value"] = b.value.value
                                    // let normalizado = b.doc_count/max
                                    // obj_["value"] = normalizado
                                    // obj_["value"] = Math.log(max / b.doc_count) * normalizado
                                    // obj_["value"] = Math.log(total / b.doc_count)
                                }
                                return ''
                            })

                            arreglo.push(obj)
                            return ''
                        })

                        let final = []

                        arreglo.map(a => {
                            pushToChildren(final, a)
                            return ''
                        })

                        function pushToChildren(parent, child) {
                            let t = parent.find(x => x.name === child.name)
                            if (t === undefined) parent.push(child)
                            else {
                                if (child.children.length > 0) pushToChildren(t.children, child.children[0])
                                else if (t.value) t.value += child.value
                                else if (t.value === undefined) t.value = child.value
                            }
                        }

                        //   console.log(JSON.stringify(final))

                        let root = d3.hierarchy({ name: "root", children: final })
                            .sum(d => d.value)
                            .sort((a, b) => b.value - a.value)
                            .eachAfter(d => d.index = d.parent ? d.parent.index = d.parent.index + 1 || 0 : 0)

                        this.setState({
                            root: root
                        })

                        //   console.log(this.state.root)

                        //   let por = (data.hits.total.value / this.state.total) * 100
                        //   por = por.toFixed(2)
                    }

                },
                (error) => {
                    console.log(error)
                    //.props.history.push("/logout");
                }
            )

    }

    changeView(event) {
        if (this.state.view !== event.target.getAttribute('value')) {
            this.setState({
                view: event.target.getAttribute('value')
            })
        }
    }

    render() {
        return (
            <>
                {this.state.loading &&
                    <div className="loading_viz">
                        <Lottie height={150} width={150} options={
                            {
                                loop: true,
                                autoplay: true,
                                animationData: animationData,
                                rendererSettings: {
                                    preserveAspectRatio: 'xMidYMid slice'
                                }
                            }
                        } />
                    </div>
                }
                {this.state.root === null && !this.state.loading &&
                    <>
                        <span class="badge badge-danger">Los resultados no tienen los datos suficientes para la visualización</span>
                    </>
                }
                {this.state.root &&
                    <>
                        {
                            <>
                                <div className="dual">
                                    <EtiquetasTabed changeFilter={this.props.changeFilter} arbolActive="vista arbol" active="arbol" roots={this.state.root} location={this.props.location} />
                                    <EtiquetasTabed changeFilter={this.props.changeFilter} arbolActive="vista lista" active="arbol" roots={this.state.root} location={this.props.location} />
                                </div>

                            </>
                        }
                    </>
                }
            </>
        )
    }
}

const mapStateToProps = store => ({
    searchFilters: store.app.filters
});

export default connect(
    mapStateToProps,
    app.actions
)(EtiquetasBar);