import React, { Component } from 'react';
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import BiGram from "../../components/molecules/BiGram";
import ThreeGram from "../../components/molecules/ThreeGram";
import FourGram from "../../components/molecules/FourGram";
import Lottie from 'react-lottie';
import animationData from '../../../assets/viz-loading.json'
import * as VizService from "../../services/VizService";

class NGram extends Component {
    constructor(props) {
        super(props)
        this.state = {
            gramSel: "bigram",
            loading: true,
            buckets: null
        };

        this.changeView = this.changeView.bind(this)
    }

    componentDidMount() {
        if (this.props.location) {
            let query = new URLSearchParams(this.props.location.search);
            if (query.toString() !== '') {
                this.updateViz(this.props.searchFilters)
            }
        } else if (this.props.bookmarks) {
            let idArray = []
            this.props.bookmarks.map(p => {
                idArray.push(p.path)
                return ''
            })

            let params = {
                idArray: idArray
            }

            this.updateViz(params)
        } else if (this.props.resource) {
            let params = {
                rg: this.props.resource[0]
              }

            this.updateViz(params)
        }
    }

    changeView(event) {
        if (this.state.gramSel !== event.target.getAttribute('value')) {
            this.setState({
                gramSel: event.target.getAttribute('value')
            })

            this.updateViz(this.props.searchFilters)
        }
    }

    updateViz(filters) {
        filters.ngram = this.state.gramSel

        VizService.getNgram(filters)
            .then(
                (data) => {
                    this.setState({
                        buckets: data.aggregations.tipo.ngram.total.buckets,
                        loading: false
                    })
                },
                (error) => {
                }
            )
    }

    render() {
        return (
            <>
                {this.state.loading &&
                    <div className="loading_viz">
                        <Lottie height={150} width={150} options={
                            {
                                loop: true,
                                autoplay: true,
                                animationData: animationData,
                                rendererSettings: {
                                    preserveAspectRatio: 'xMidYMid slice'
                                }
                            }
                        } />
                    </div>
                }
                {this.state.buckets &&
                    <>
                        <div className="wordcloudBar grafo">
                            <div className="cloudContainer">
                                <div className="viewSelect">
                                    <div value="bigram" className={this.state.view === 'bigram' ? 'item active' : 'item'} onClick={this.changeView}>Bigram</div>
                                    <div value="3gram" className={this.state.view === '3gram' ? 'item active' : 'item'} onClick={this.changeView}>3gram</div>
                                    <div value="4gram" className={this.state.view === '4gram' ? 'item active' : 'item'} onClick={this.changeView}>4gram</div>
                                </div>

                                {this.state.gramSel === 'bigram' &&
                                    <BiGram buckets={this.state.buckets} />
                                }
                                {this.state.gramSel === '3gram' &&
                                    <ThreeGram buckets={this.state.buckets} />
                                }
                                {this.state.gramSel === '4gram' &&
                                    <FourGram buckets={this.state.buckets} />
                                }
                            </div>
                        </div>

                    </>
                }

            </>
        )
    }
}


const mapStateToProps = store => ({
    searchFilters: store.app.filters
});

export default connect(
    mapStateToProps,
    app.actions
)(NGram);