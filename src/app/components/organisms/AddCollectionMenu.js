import React, { useState, useEffect } from "react";
import * as CollectionService from "../../services/CollectionService";
import { Card } from "react-bootstrap";
import { LABELS_MENU } from "../../../app/config/constLabels"
import { useTranslation } from "react-i18next";



const AddCollectionMenu = (props) => {
  const [t, i18n] = useTranslation("common");

  const [loading, setLoading] = useState(false);
  const [elements, setElements] = useState([]);

  const search_colection = async (e) => {
    let keyWord = e.target.value;
    let form = {
      keyWord,
    };
    setLoading(true);
    setElements([]);
    if (keyWord) {
      let res = await CollectionService.find_collection(form);
      if (res.length) setElements(res);
    }

    setLoading(false);
  };

  const render_elements = () => {
    if (elements.length) {
      return elements.map((elemento, index) => {
        return (
          <div key={elemento._id} className="card  mb-2">
            {/* <div className="card-header">#{index + 1}</div> */}
            <div className="card-body">
              <div className="d-flex justify-content-between">
              <h5 className="card-title">{elemento.title.toUpperCase()}</h5>
              <a target="_blank" href={`/museo/conoce/${elemento._id}`}>
                <i className="fas fa-eye"></i>
              </a>
              </div>           
              <Card.Link
                onClick={() => {
                  props.add_collection_element(elemento);
                }}
                href="#"
              >
               {LABELS_MENU.add_to_menu}
              </Card.Link>
            
            </div>
          </div>
        );
      });
    }
  };
  return (
    <>
      <div className="input-group rounded">
        <input
          type="search"
          className="form-control rounded"
          placeholder={t("micrositios.menus.searchCollection")}
          aria-label="Search"
          aria-describedby="search-addon"
          onChange={(e) => {
            search_colection(e);
          }}
        />
        <span className="input-group-text border-0" id="search-addon">
          <i className="fas fa-search"></i>
        </span>
      </div>

      <hr />
      {loading && (
        <div className="fa-3x">
          <i className="fas fa-spinner fa-spin"></i>
        </div>
      )}

      {!loading && elements.length ? render_elements() : ""}
      {/* {!loading && !elements.length  && 'No '} */}
    </>
  );
};

export default AddCollectionMenu;
