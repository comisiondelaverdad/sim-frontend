import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {connect} from "react-redux";
import _ from "lodash";
import BootstrapTable from "react-bootstrap-table-next";
import { METADA_MAP } from "../../config/const";
import * as app from "../../store/ducks/app.duck";

const flattenKeys = (obj, path = []) =>
    !_.isObject(obj)
        ? { [path.join('.')]: obj }
        : _.reduce(obj, (cum, next, key) => _.merge(cum, flattenKeys(next, [...path, key])), {});

class TestimonyTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //result: this.props.records,
      products: [
       
      ],
      columns: [
        {
          dataField: "name",
          text: "Metadato",
          formatter: this.metaDataSearch
        },
        {
          dataField: "description",
          text: "Valor"
        }
      ]
    };

    this.search = this.search.bind(this);
  }

  metaDataSearch(cell, row){
    if(row.name){
      return <div className="cev-link">{row.name}</div>
    }
  }

  search(evt,row, rowIndex) {
    let search = `resource.${row.search}.keyword:"${row.description}"`;
    if(row.search === "title"){
      search = `resource.${row.search}:"${row.description}"`;
    }
    this.props.filters({
      "filters":{
        "keyword": search,
        "resourceGroup": {
          "resourceGroupText": "",
          "resourceGroupIDS": ""
        },
        "mapPolygon": ""
      }
    })
    this.props.history.push("/search?q=" + search + "&from=1");
    
  }
  
  componentDidMount(){
    if(this.props.records.records){
      delete this.props.records.records;
    }
    if(this.props.records._id){
      delete this.props.records._id;
    }
    if(this.props.records.identifier){
      delete this.props.records.identifier;
    }
    if(this.props.records.ResourceGroupId){
      delete this.props.records.ResourceGroupId;
    }
    if(this.props.records.metadata._id){
      delete this.props.records.metadata._id;
    }
    let i = 0;

    let list = _.map(flattenKeys(this.props.records), (v, k)=>{
      let name = k.split(".").filter(item => isNaN(item)).join(".");
      return {
          id: ++i,
          name: METADA_MAP[name] ? METADA_MAP[name] : null,
          description: v !== null && isNaN(v) ? v.replace(/[[]{}]/g,'') : v,
          search: name.split(".").pop()
        }
    })

    list = Object.values(list.reduce((item, {id,name,description,search}) => {
      item[name] = item[name] || {id,name,description:'',search:''};
      item[name].description = item[name].description ? item[name].description + "\r\n" + description : description; 
      item[name].search = search; 
      return item;
    }, {})).filter(item => item.name !== null );
    
    this.setState({products: list})
  }

  render() {
    return (
      <>
        {this.state.products &&
          <div className="card">
            <BootstrapTable
              striped
              keyField="id"
              data={this.state.products}
              columns={this.state.columns}
              rowEvents={{
                onClick: this.search
              }}
            />
          </div>
        }
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default withRouter(connect(
  mapStateToProps,
  app.actions
)(TestimonyTable));