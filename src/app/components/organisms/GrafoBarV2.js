import React, { Component } from 'react'
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import * as VizService from "../../services/VizService";
import PropTypes from 'prop-types';
import Lombardi from '../molecules/LombardiV2';
import * as d3 from 'd3'
import Lottie from 'react-lottie';
import animationData from '../../../assets/viz-loading.json'
import Slider from '../ui/Slider'

/**
 * Bloque que muestra un grafo
 * @version 0.1
 */
class GrafoBarV2 extends Component {
    static propTypes = {
        /** URL string location */
        location: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props)
        this.state = {
            network: null,
            total: 0,
            moreInfo: false,
            loading: true,
            data: null,
            allResults: false,
            selected: null,
            linkedByIndex: {},
            selectedGroup: null,
            view: 'lombardi'
        };

        this.moreInfo = this.moreInfo.bind(this)
        this.changeSelected = this.changeSelected.bind(this)
        this.changeView = this.changeView.bind(this)
        this.closeOverlay = this.closeOverlay.bind(this)
        this.filterSelected = this.filterSelected.bind(this)
        this.changeSlider = this.changeSlider.bind(this)
        this.resetFilter = this.resetFilter.bind(this)
        this.updateDataFiltered = this.updateDataFiltered.bind(this)
    }

    componentDidMount() {
        let query = new URLSearchParams(this.props.location.search);
        this.color = d3.scaleOrdinal(d3.schemeTableau10)

        if (query.toString() !== '') {
            this.updateGrafo(this.props.searchFilters);
        } else if (this.props.bookmarks) {
            this.updateDataFiltered()
        } else {
            this.updateDataFiltered()
        }
    }

    updateDataFiltered() {
        let params = {
            origin: "ModuloCaptura"
        }

        if (this.props.filtros !== undefined) {
            if (this.props.filtros[0].filtros.length > 0) params.ngramFilter = this.props.filtros[0].filtros
            if (this.props.filtros[1].filtros.length > 0) params.etiquetasFilter = this.props.filtros[1].filtros
        }

        if (this.state.selected !== null) {
            params.entidadFilter = this.state.selected
        }

        this.updateGrafo(params)
    }

    componentDidUpdate(prevProps) {
        let oldQuery = new URLSearchParams(prevProps.location.search).toString();
        let query = new URLSearchParams(this.props.location.search);

        if (query.toString() !== '') {
            if (query.toString() !== oldQuery.toString()) {
                this.updateGrafo(this.props.searchFilters);
            }
        }
    }

    /**
     * Actualiza los datos que reciben del back
     * 
     * @param {string} search 
     * @param {string} resourceGroup 
     */
    updateGrafo(filters) {
        VizService.grafoEntidades(filters)
            .then(
                (data) => {
                    this.setState({
                        loading: false
                    })

                    let buckets = data.aggregations.tipo.connections.total.buckets
                    let arrayTemp = []
                    let arrayGroups = []
                    let nodos = []

                    if (data.hits.hits.length > 0) {
                        let max = buckets[0].doc_count
                        // let min = buckets[buckets.length - 1].doc_count
                        let links = []

                        buckets.forEach(b => {

                            let e = b.key.split('---')

                            let link = {
                                "source": null,
                                "target": null,
                                "value": b.doc_count
                            }

                            e.forEach((n, i) => {
                                let _nodos = n.replace(/\s+/g, ' ').trim().split('||')
                                let index = null

                                if (!arrayTemp.includes(_nodos[1].replace(/\s+/g, ' ').trim())) {
                                    var resp = {
                                        "name": _nodos[1].replace(/\s+/g, ' ').trim(),
                                        "group": _nodos[0].replace(/\s+/g, ' ').trim(),
                                        "index": nodos.length,
                                        "value": b.doc_count
                                    }

                                    index = nodos.length

                                    nodos.push(resp)
                                    arrayTemp.push(_nodos[1].replace(/\s+/g, ' ').trim())
                                } else {
                                    index = arrayTemp.indexOf(_nodos[1].replace(/\s+/g, ' ').trim())
                                    nodos[index]['value'] += b.doc_count
                                }

                                if (!arrayGroups.includes(_nodos[0])) arrayGroups.push(_nodos[0])

                                if (i === 0) {
                                    link.source = index
                                } else {
                                    link.target = index
                                }

                                return ''
                            })

                            links.push(link)
                            return ''
                        })

                        let por = (data.hits.total.value / this.state.total) * 100
                        por = por.toFixed(2)

                        let network = {
                            "node-data": nodos,
                            "link-data": links,
                            "max": max,
                            "groups": arrayGroups.sort((a, b) => a.localeCompare(b, 'en', { 'sensitivity': 'base' }))
                        }

                        console.log(network)

                        this.setState({
                            network: JSON.parse(JSON.stringify(network)),
                            data: JSON.parse(JSON.stringify(network)),
                            percent: por
                        })
                    }

                },
                (error) => {
                    console.log(error)
                    //.props.history.push("/logout");
                }
            )
    }

    /**
     * Calcula el mapeo de un número dándole un rango de entrada y de salida.
     *
     * @param {int} i input.
     * @param {int} r1 rango mínimo del input.
     * @param {int} r2 rango máximo del input.
     * @param {int} m1 rango mínimo del output.
     * @param {int} m2 rango máximo del output.
     * @returns {float} el mapeo del input entre los rangos especificados del output.
     */
    scaleLinear(i, r1, r2, m1, m2) {
        var resp = (i - r1) * (m2 - m1) / (r2 - r1) + m1
        return resp
    }

    moreInfo() {
        if (this.state.moreInfo) {
            this.setState({
                moreInfo: false
            })
        } else {
            this.setState({
                moreInfo: true
            })
        }
    }

    /**
     * Actualiza las entidades por click
     * 
     * @param {*} e 
     */
    changeSelected(e) {
        let new_data = this.state.data['node-data']
        new_data = new_data.filter(d => d.group === e)

        this.setState({
            selectedGroup: {
                array: new_data,
                group: e
            }
        })
    }

    changeView(event) {
        if (this.state.view !== event.target.getAttribute('value')) {
            this.setState({
                view: event.target.getAttribute('value')
            })
        }
    }

    closeOverlay() {
        this.setState({
            selectedGroup: null
        })
    }

    filterSelected(i) {

        if (!this.state.allResults) {
            let node_index = [i.index]
            const links = this.state.data['link-data'].filter(d => d.source === i.index || d.target === i.index)
            links.forEach(l => {
                if (!node_index.includes(l.source)) {
                    node_index.push(l.source)
                }
                if (!node_index.includes(l.target)) {
                    node_index.push(l.target)
                }
            })

            const nodes = this.state.data['node-data'].filter(d => node_index.includes(d.index))

            links.forEach(l => {
                const index_source = nodes.findIndex(d => d.index === l.source)
                const index_target = nodes.findIndex(d => d.index === l.target)

                l.source = index_source
                l.target = index_target
            })

            let network = {
                "node-data": nodes,
                "link-data": links,
                "max": this.state.data.max,
                "groups": this.state.data['groups']
            }

            this.setState({
                network: JSON.parse(JSON.stringify(network)),
                selectedGroup: null,
                selected: i
            })
        } else {
            this.setState({
                selectedGroup: null,
                selected: i
            }, () => {
                let params = {
                    origin: "ModuloCaptura"
                }

                if (this.props.filtros !== undefined) {
                    if (this.props.filtros[0].filtros.length > 0) params.ngramFilter = this.props.filtros[0].filtros
                    if (this.props.filtros[1].filtros.length > 0) params.etiquetasFilter = this.props.filtros[1].filtros
                }

                if (this.state.selected !== null) {
                    params.entidadFilter = this.state.selected
                }

                this.updateGrafo(params)
            })


        }

    }

    changeSlider() {
        this.state.allResults ? this.setState({ allResults: false }, this.updateDataFiltered()) : this.setState({ allResults: true }, this.updateDataFiltered())
    }

    resetFilter() {
        this.setState({
            selected: null
        }, () => {
            this.updateDataFiltered()
        })
    }

    render() {
        return (
            <>
                {this.state.loading &&
                    <div className="loading_viz">
                        <Lottie height={150} width={150} options={
                            {
                                loop: true,
                                autoplay: true,
                                animationData: animationData,
                                rendererSettings: {
                                    preserveAspectRatio: 'xMidYMid slice'
                                }
                            }
                        } />
                    </div>
                }
                {this.state.network === null && !this.state.loading &&
                    <>
                        <span class="badge badge-danger">Los resultados no tienen los datos suficientes para la visualización</span>
                    </>
                }
                {this.state.network &&
                    <>
                        <div className="wordcloudBar grafo">
                            <div className="cloudContainer">
                                {/* <div className="info">
                  <b>{this.state.percent}%</b> {this.props.info}:
                      </div> */}
                                <Slider
                                    label="ver todos los resultados"
                                    toggle={this.changeSlider}
                                    active={this.state.selected}
                                    removeFilter={this.resetFilter}
                                />
                                <div className="inside">
                                    {this.state.view === 'lombardi' &&
                                        <Lombardi color={this.color} network={this.state.network} />
                                        // <></>
                                    }

                                    <div className="groups side">
                                        {this.state.network['groups'].forEach(n => {
                                            let style = {
                                                backgroundColor: this.color(n)
                                            }
                                            return (
                                                <div className="item" onClick={e => this.changeSelected(n)}>
                                                    <div className="icon" style={style}></div>
                                                    {n}
                                                </div>
                                            )
                                        })}
                                    </div>

                                    {this.state.selectedGroup &&
                                        <div className="overlay">
                                            <div className="top">
                                                <h2>{this.state.selectedGroup.group}</h2>
                                                <div className="close" onClick={this.closeOverlay}></div>
                                            </div>
                                            <div className="listado_grupo">
                                                {
                                                    this.state.selectedGroup.array.forEach(i => {
                                                        return (
                                                            <>
                                                                <div className="item" onClick={e => this.filterSelected(i)}>{i.name}</div>
                                                            </>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    }

                                </div>


                            </div>
                        </div>
                    </>
                }
            </>
        )
    }
}

const mapStateToProps = store => ({
    searchFilters: store.app.filters
});

export default connect(
    mapStateToProps,
    app.actions
)(GrafoBarV2);