import React, { Component } from 'react'
import * as VizService from "../../services/VizService";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import Lottie from 'react-lottie';
import animationData from '../../../assets/viz-loading.json'
import TimeLine from '../molecules/TimeLine'

/**
 * Componente que se encarga de un mapa de calor cronológico de entidades
 */
class TimeLineBar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: true,
      timeline: null
    }
  }

  componentDidMount() {
    if (this.props.location) {
      let query = new URLSearchParams(this.props.location.search);

      if (query.toString() !== '') {
        this.updateTimeLine(this.props.searchFilters);
      }
    } else if (this.props.bookmarks) {
      let idArray = []
      this.props.bookmarks.map(p => {
        idArray.push(p.path)
        return ''
      })

      let params = {
        idArray: idArray
      }

      this.updateTimeLine(params)
    } else if (this.props.resource) {
      let params = {
        rg: this.props.resource[0]
      }
      this.updateTimeLine(params)
    } else {
      this.updateDataFiltered()
    }
  }

  updateDataFiltered() {
    let params = {
      origin: "ModuloCaptura"
    }

    if (this.props.filtros !== undefined) {
      if (this.props.filtros[0].filtros.length > 0) params.ngramFilter = this.props.filtros[0].filtros
      if (this.props.filtros[1].filtros.length > 0) params.etiquetasFilter = this.props.filtros[1].filtros
      if (this.props.filtros[2].filtros.length > 0) params.entidadesFilter = this.props.filtros[2].filtros
    }

    this.updateTimeLine(params)
  }

  /**
   * Servicio para actualizar el estado cuando es una búsqueda
   * 
   * @param {string} search 
   * @param {string} resourceGroup 
   */
  updateTimeLine(filters) {
    VizService.getLineaTiempo(filters)
      .then(
        (data) => {
          let temp = [], arrayTemp = [], years_domain = []
          if (data.aggregations) {
            temp = data.aggregations.tipo.connections.total.buckets
            temp.forEach(t => {
              const s = t.key.split('---')
              if (s[0].split('||')[0] !== 'Fecha' || s[1].split('||')[0] !== 'Fecha') {
                let entidad, fecha, group
                if (s[0].split('||')[0] !== 'Fecha') { entidad = s[0].split('||')[1]; group = s[0].split('||')[0]; fecha = s[1].split('||')[1] }
                else { entidad = s[1].split('||')[1]; group = s[1].split('||')[0]; fecha = s[0].split('||')[1] }

                const item = arrayTemp.find(d => d.entidad === entidad)
                let fecha_ = Date.parse(fecha + "T00:00")


                if (!isNaN(fecha_)) {
                  const ano = fecha.split('-')[0]
                  const year = years_domain.find(d => d === ano)

                  if (!year) years_domain.push(ano)

                  if (item) {
                    const date = item.years.find(d => d.year === ano)
                    if (date) {
                      date.value++
                    } else {
                      item.years.push({
                        year: ano,
                        group: group,
                        value: 1
                      })
                    }
                  }
                  else {
                    arrayTemp.push({
                      entidad: entidad,
                      group: group,
                      years: [
                        {
                          year: ano,
                          group: group,
                          value: 1
                        }
                      ]
                    })
                  }
                }

              }
            })

          }

          years_domain = years_domain.sort((a, b) => a - b)

          this.setState({
            loading: false,
            timeline: arrayTemp,
            yeardomain: years_domain
          })

          console.log(this.state)
        },
        (error) => {
          console.log(error)
          //this.props.history.push("/logout");
        }
      )



  }

  render() {
    return (
      <>
        {this.state.loading &&
          <div className="loading_viz">
            <Lottie height={150} width={150} options={
              {
                loop: true,
                autoplay: true,
                animationData: animationData,
                rendererSettings: {
                  preserveAspectRatio: 'xMidYMid slice'
                }
              }
            } />
          </div>
        }

        {this.state.timeline && this.state.timeline.length > 0 &&
          <>
            <div className="wordcloudBar">
              <TimeLine years={this.state.yeardomain} timeline={this.state.timeline} />
            </div>
          </>
        }
      </>
    )
  }
}

const mapStateToProps = store => ({
  searchFilters: store.app.filters
});

export default connect(
  mapStateToProps,
  app.actions
)(TimeLineBar);