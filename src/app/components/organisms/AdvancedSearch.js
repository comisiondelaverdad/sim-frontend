import React, { Component } from "react";
import FormAdvancedSearchMenu from "../molecules/FormAdvancedSearchMenu";
import FormAdvancedSearchMap from "../molecules/FormAdvancedSearchMap";
import FormAdvancedSearchTime from "../molecules/FormAdvancedSearchTime";
import FormAdvancedSearchLabels from "../molecules/FormAdvancedSearchLabels";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import "./advancedSearchForm.scss";

class AdvancedSearch extends Component {

  componentDidMount() {
  }
  
  render() {
    return (
      <div className="AdvancedSearchForm">
        <span className="buttons">
            <button  title="agregar condicional OR" type="button" className="btn-small ml-2 btn btn-primary" onClick={x=>this.props.addQuery("OR")} >
              OR
            </button>
            <button title="agregar condicional AND" type="button" className="btn-small ml-2 btn btn-primary" onClick={x=>this.props.addQuery("AND")} >
              AND
            </button>
        </span>

        <FormAdvancedSearchMenu config={this.props.config} addQuery={this.props.addQuery} />
        
        {this.props.dashboard &&
          <FormAdvancedSearchMap  config={this.props.config} addQuery={this.props.addQuery} addMap={this.props.addMap}/>
        }
        
            <FormAdvancedSearchLabels config={this.props.config} addQuery={this.props.addQuery} />
        
        {true &&
          <>
            <FormAdvancedSearchTime config={this.props.config} addQuery={this.props.addQuery} />

          </>
        }

      </div>
    );
  }
}

export default AdvancedSearch;
