import React, {Component} from "react";
import {connect} from "react-redux";
import {DOC_ANNOTATION, DOC_ENTITY_COLORS, DOC_ENTITIES} from "../../config/const";
import DocumentAnnotator from "../molecules/DocumentAnnotator";
import TreeView from "../molecules/TreeView";
import PropTypes from "prop-types";
import {Button, Card, CardBody, Collapse} from "reactstrap";
import {Alert} from "react-bootstrap";
import ReactWaterMark from 'react-watermark-component';
import PerfectScrollbar from "react-perfect-scrollbar";
import ControlBar from "../molecules/ControlBar"

class AnnotatedTranscription extends Component {

  constructor(props) {
    super(props);
    this.state = {
      annotations: null,
      annotationsMap: null,
      entitiesTree: null,
      documentText: null,
      entitiesColorMap: null,
      showEntities: false
    };
    this.toggleEntities = this.toggleEntities.bind(this);
  }

  handleSync = ps => {
    if(this.props.scrollTo && this.props.scrollTo.label){
      let offsetTop = document.querySelector("[data-id='"+this.props.scrollTo.label.start+"-"+this.props.scrollTo.label.end+"']").offsetTop;
    }
  }

  componentDidMount() {
    const record = this.props.records.find(x => x.type === DOC_ANNOTATION);

    const documentText = record ? record.hasOwnProperty("labelled") ? record.labelled.content : null : null;
    const annotations = record ? record.hasOwnProperty("labelled") ? record.labelled.annotation : [] : [];

    const entitiesColorMap = this.createEntitiesColorMap(DOC_ENTITIES.map(a => JSON.parse(`"${a}"`)), DOC_ENTITY_COLORS);
    const annotationsMap = this.createAnnotationsMap(annotations, entitiesColorMap);
    const entitiesTree = this.createEntitiesTree(DOC_ENTITIES.map(a => JSON.parse(`"${a}"`)), entitiesColorMap);

    this.setState({
      annotations: annotations,
      annotationsMap: annotationsMap,
      documentText: documentText,
      entitiesColorMap: entitiesColorMap,
      entitiesTree: entitiesTree
    });
  }

  createAnnotationsMap(annotations, entitiesColorMap) {
    console.log(annotations)
    if(annotations && annotations.length){
      return annotations.map(annotation => {
        return {
          category: annotation.label,
          start: annotation.points[0].start,
          end: annotation.points[0].end,
          text: annotation.points[0].text,
          id: `${annotation.points[0].start}-${annotation.points[0].end}`,
          color: !Array.isArray(annotation.label) ? "white" : annotation.label.map(label => {
            if (!(label in entitiesColorMap)) {
              console.debug(label, label in entitiesColorMap);
            }
            return label in entitiesColorMap ? entitiesColorMap[label] : "white";
          })
        };
      });
    }
    
  }

  createEntitiesTree(entities, entitiesColorMap) {
    let entitiesTree = [{
      text: 'Seleccionar todo',
      state: {opened: false, selected: true},
      icon: "fas fa-square",
      children: [],
      a_attr: {href: '#', style: `color: black`}
    }];

    entities.forEach((entity, index) => {
      // Reference tree root array
      let result = entitiesTree[0].children;
      // split annotation
      entity.split(" - ").forEach((mapKey) => {
        // Validate mapKey is not empty
        if (!mapKey) {
          return;
        }
        // Find node by mapKey
        let node = result.find(e => e.text === mapKey);
        // Create key if not present
        if (!node) {
          node = result.push({
            id: index,
            text: mapKey,
            state: {opened: false, selected: true},
            icon: "fas fa-square",
            children: [],
            a_attr: {href: `/search?q=record.labelled.annotation.label:"${entity}"&from=1`, style: `color: ${entitiesColorMap[entity]}`}
          });
        }
        // Reference node children array
        result = node.children;
      });
    });

    return {
      core: {
        data: entitiesTree,
        expand_selected_onload: false,
        themes: {dots: false}
      },
      plugins: ["checkbox", "wholerow"],
      checkbox: {
        three_state: true
      }
    };
  }

  createEntitiesColorMap(entities, docEntityColors) {
    return entities.reduce((colorMap, entity, index) => {
      return {...colorMap, [entity]: docEntityColors[index % docEntityColors.length]};
    }, {});
  }

  handleChange(e, data) {
    const {annotations, entitiesColorMap} = this.state;
    const selectedEntities = data.selected.map(n => DOC_ENTITIES.map(a => JSON.parse(`"${a}"`))[parseInt(n)]);

    const selectedAnnotations = annotations.filter(a => !Array.isArray(a.label) ? a.label : a.label.some(l => selectedEntities.includes(l))).map(function (a) {return {...a, label: !Array.isArray(a.label) ? a.label : a.label.filter(l => selectedEntities.includes(l))};});
    const annotationsMap = this.createAnnotationsMap(selectedAnnotations, entitiesColorMap);

    this.setState({annotationsMap: annotationsMap});
  }

  handleNodeSelection(e, data) {
    const target = data.event.target;
    const href = data.node.a_attr.href;
    if (target.tagName.toLowerCase() === 'a' && href !== '#') {
      window.open(href, "_blank", "noopener");
    }
  }

  toggleEntities() {
    this.setState({showEntities: !this.state.showEntities});
  }

  render() {

    const {annotationsMap, documentText, entitiesTree} = this.state;

    const options = {
      chunkWidth: 450,
      chunkHeight: 90,
      textAlign: 'left',
      textBaseline: 'bottom',
      globalAlpha: 0.2,
      font: '36px Microsoft Yahei',
      rotateAngle: -0.1,
      fillStyle: '#666'
    }

    return (
      <>
        {documentText && annotationsMap &&
          <>
            <ReactWaterMark
              waterMarkText={this.props.user.name}
              options={options}
            >
              <ControlBar user={this.props.user.name} config={{themes:true,textUP:true,textDOWN:true,fontFamily:true,fontSize:true,fullScreen:true}}>
                <PerfectScrollbar
                  id="scrollTranscription"
                  className="cev-scroll"
                  options={{
                    wheelSpeed: 1,
                    wheelPropagation: false
                  }}
                  style={{ height: "calc(100vh - 240px)"}}
                  data-scroll="true"
                  onSync={this.handleSync}
                >
                  {entitiesTree &&
                    <div className="jstree-cev-annotations">
                      <Button color="primary" type="button"
                              onClick={this.toggleEntities}>{this.state.showEntities ? 'Ocultar etiquetado' : 'Mostrar etiquetado'}</Button>
                      <Collapse isOpen={this.state.showEntities}>
                        <Card>
                          <CardBody>
                            <TreeView
                                treeData={entitiesTree}
                                onChange={(e, data) => this.handleChange(e, data)}
                                onNodeDeselected={(e, data) => this.handleNodeSelection(e, data)}
                                onNodeSelected={(e, data) => this.handleNodeSelection(e, data)}
                            />
                          </CardBody>
                        </Card>
                      </Collapse>
                    </div>
                  }
                  <div className="text-justify">
                    <DocumentAnnotator
                      annotations={annotationsMap}
                      documentText={documentText}
                    />
                  </div>
                </PerfectScrollbar>
              </ControlBar>
            </ReactWaterMark>
          </>
        }
        {!documentText &&
        <Alert variant="secondary">
          <div className="alert-icon">
            <i className="flaticon-warning cev-font-brand"/>
          </div>
          <div className="alert-text">Texto de la transcripción no encontrado</div>
        </Alert>
        }
      </>
    );
  }

}

AnnotatedTranscription.propTypes = {
  records: PropTypes.array.isRequired
};

const mapStateToProps = store => ({
  user: store.auth.user,
  scrollTo: store.app.display
});

export default connect(
  mapStateToProps
)(AnnotatedTranscription);
