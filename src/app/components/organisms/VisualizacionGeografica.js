import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {connect} from "react-redux";
import _ from "lodash";
import BootstrapTable from "react-bootstrap-table-next";
import { METADA_MAP } from "../../config/const";
import * as app from "../../store/ducks/app.duck";

class VisualizacionGeografica extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    console.log(this.props)
    return (
      <>
        <iframe style={{width:"100%", height:"425px"}} src={this.props.records.metadata.firstLevel.url}></iframe>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default withRouter(connect(
  mapStateToProps,
  app.actions
)(VisualizacionGeografica));