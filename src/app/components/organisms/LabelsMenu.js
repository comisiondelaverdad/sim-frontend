import React from "react";
import { connect } from "react-redux";
import { Dropdown, Alert } from "react-bootstrap";
import DropDownIcon from "../molecules/DropDownIcon";

class LabelsMenu extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
      newLabel : '',
      show : false,
      alert : false
    };

    this.handleChange = this.handleChange.bind(this);
    this.validate = this.validate.bind(this);
  }

  handleChange(e){
    const {name,value} = e.target;
    this.setState({[name]:value, alert : value === ''});
  }

  componentDidMount(){
    this.setState({oldLabel:this.props.label, newLabel:this.props.label, show:false});
  }

  validate(evt){
    evt.preventDefault();
    if(this.state.newLabel === ''){
      this.setState({alert:true});
    }
    else{
      this.props.updateLabel(this.props.user,{oldLabel:this.state.oldLabel,newLabel:this.state.newLabel})
    }
    
  }

  render(){
    const { 
      newLabel,
      show,
      alert
    } = this.state;

    return(
      <Dropdown
        alignRight
        drop="down"
        show={show}
        onToggle={()=>{this.setState({show:false})}}
      >
        <Dropdown.Toggle 
          as={DropDownIcon}
          onClick={()=>{this.setState({show:true})}}
        >
          <button type="button" className="btn btn-icon btn-outline-success">
            <i className="flaticon2-edit"></i>
          </button>
        </Dropdown.Toggle>
        <Dropdown.Menu className="dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl cev-bookmarks">
          <div className="cev-portlet">
            <div className="cev-portlet__head">
              <div className="cev-portlet__head-label">
                <h3 className="cev-portlet__head-title cev-font-primary">
                  Editar Etiqueta
                </h3>
              </div>
              <div className="cev-portlet__head-toolbar">
                <div className="cev-portlet__head-actions">
                  <button type="button" className="btn btn-outline-secondary btn-icon btn-sm" onClick={()=>{this.setState({show:false})}}>
                    <i className="flaticon2-cross"></i>
                  </button>
                </div>
              </div>
            </div>
            <div className="cev-portlet__body">
              <form className="cev-sc__form" onSubmit={evt=>evt.preventDefault()}>
                <div className="input-group">
                  <input 
                    type="text"
                    name="newLabel"
                    className="form-control" 
                    placeholder="Etiqueta"
                    value={newLabel}
                    onChange={this.handleChange}
                  />
                </div>
                <Alert variant="danger" show={alert}>Introduzca el nombre de la etiqueta</Alert>
              </form>
            </div>
            <div className="cev-portlet__foot">
              <div className="row">
                <div className="col-12 cev-align-right">
                  <button type="button" className="btn btn-outline-success" onClick={this.validate}>
                    <i className="flaticon2-check-mark"></i>
                    Guardar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    )
  }
}

const mapStateToProps = store => ({
  user: store.auth.user._id
});

export default connect(
  mapStateToProps
)(LabelsMenu);