import React, { Component,useEffect, useState } from "react";
import {Alert, Button, Col, Collapse, Form, Modal, Row, SafeAnchor} from "react-bootstrap";

class TermsRecordsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {        
      show: false,
      idr: this.props.idr
    };                 
}

componentDidMount() {
  console.log(this.state);
}

onOpenModal() {
  this.setState({show: true});  
}

onCloseModal() {
  this.setState({show: false});  
}

  render() {
    
    return (
      <div>
      <Button variant="success" onClick={() => {
                                this.onOpenModal();
                              }}>
        Seleccionar audio de la entrevista
      </Button>
      <ul>
        
      </ul>

      <Modal show={this.state.show} >
        <Modal.Header>
          <Modal.Title>Audios</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => {
                                this.onCloseModal();
                              }}>
            Cerrar
          </Button>          
        </Modal.Footer>
      </Modal>        
      </div>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user,
  scrollTo: store.app.display
});

export default TermsRecordsModal;
