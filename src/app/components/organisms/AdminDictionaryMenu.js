import React, { Component } from "react";

class AdminDictionaryMenu extends Component {
  render() {
    return (
      <div
        className="cev-grid__item cev-app__toggle cev-app__aside"
        id="cev_user_profile_aside"
      >
        <div className="cev-portlet cev-portlet--height-fluid">
          <div className="cev-portlet__head">
            <div className="cev-portlet__head-label">
              <h3 className="cev-portlet__head-title">Menu Administración del Diccionario Colaborativo</h3>
            </div>
          </div>
          <div className="cev-portlet__body cev-content">
            <div className="cev-widget cev-widget--user-profile-1">
              <div className="cev-widget__items">
                <React.Fragment>
                  <a
                    className={"cev-widget__item "}
                    role={"button"}
                    href={`/dictionary/semantic`}
                  >
                    <span className={"cev-widget__section cev-text-justify "}>
                      <span className="cev-widget__icon cev-font-success">
                        <i className="flaticon-settings" />
                      </span>
                      <span className="cev-widget__desc">Campos Semánticos</span>
                    </span>
                  </a>
                </React.Fragment>
              </div>
            </div>
            <div className="cev-widget cev-widget--user-profile-1">
              <div className="cev-widget__items">
                <React.Fragment>
                  <a
                    className={"cev-widget__item "}
                    role={"button"}
                    href={`/dictionary/term`}
                  >
                    <span className={"cev-widget__section cev-text-justify "}>
                      <span className="cev-widget__icon cev-font-success">
                        <i className="flaticon-settings" />
                      </span>
                      <span className="cev-widget__desc">Términos</span>
                    </span>
                  </a>
                </React.Fragment>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AdminDictionaryMenu;
