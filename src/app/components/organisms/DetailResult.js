import React, { Component } from "react";
import Result from "../../components/organisms/Result";

class DetailResult extends Component {
  render() {
    return (
      <div className="container-general">
        {this.props.records && this.props.records.map((record,idx)=>(
          <Result 
            key={idx}
            record={record} 
            extra={this.props.extra}
          />
        ))}
      </div>
    );
  }
}

export default DetailResult;
