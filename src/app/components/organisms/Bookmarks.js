import React, { Component } from "react";
import { toAbsoluteUrl } from "../../../theme/utils";
import striptags from "striptags";
import { TITLE_SIZE } from "../../config/const";
import _ from "lodash";
import ResultSubHead from "../molecules/ResultSubHead";
import ConfirmMenu from "./ConfirmMenu";
import Swal from 'sweetalert2/dist/sweetalert2.all.min.js'

class Bookmarks extends Component {

  confirmModal(bookmark){
    Swal.fire({
      title: "¿Está seguro desea eliminar el Bookmark: "+bookmark.path+" - "+(bookmark.title && bookmark.title.split(" ", TITLE_SIZE).join(" ")) + "?",
      showDenyButton: true,
      showCancelButton: true,
      cancelButtonText: 'No',
      confirmButtonText: `Sí`,
      denyButtonText: `No`,
      customClass : "swal2-delete-modal"
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteAction(bookmark,this.props.keyword,this.props.records.length)                            
      } 
    })
  }

  limpiar(description){
    description = striptags(description);
    description = description.replace(/\xA0/g,'');
    description = description.replace(/\\n/g,'<br/>');
    return description.trim();
  }

  deleteAction(bookmark,keyword,length){
    this.props.actions.deleteBookmark(bookmark,keyword,length);
  }

  render() {
    console.log(this.props.records)
    return (
      <>
        {this.props.records && this.props.records.length > 0 && this.props.records.map((bookmark,idx) => (
          <div className="cev-portlet" style={{width:"100%",background:"#ffffff"}} key={idx}>
            <div className="cev-portlet__body">
              <div className="cev-widget cev-widget--user-profile-3">
                <div className="cev-widget__top">
                  <div className="cev-widget__media hidden-">
                    <img src={toAbsoluteUrl("/media/users/default.jpg")} alt="1" />
                  </div>
                  <div className="cev-widget__content">
                    <div className="cev-widget__head">
                      <a href={"/detail/"+bookmark.path} className="cev-widget__username">
                        {bookmark.path} - {bookmark.title && bookmark.title.split(" ", TITLE_SIZE).join(" ")}
                        <i className="flaticon2-correct cev-font-success"></i>
                      </a>
                      <div className="cev-widget__action">
                        {this.props.showDelete &&

                        <button
                        onClick={()=>this.confirmModal(bookmark)}
                        type="button" className="btn btn-icon btn-outline-danger">
                          <i className="flaticon2-trash"></i>
                        </button>

                          // <ConfirmMenu
                          //   title="Eliminar Bookmark"
                          //   message={"¿Está seguro desea eliminar el Bookmark: "+bookmark.path+" - "+(bookmark.title && bookmark.title.split(" ", TITLE_SIZE).join(" ")) + "?"}
                          //   confirmAction={()=>{this.deleteAction(bookmark,this.props.keyword,this.props.records.length)}}
                          // />
                        }
                      </div>
                    </div>
                    <a href={"/detail/"+bookmark.path}>
                      <div className="cev-widget__subhead">
                        {bookmark.extra && bookmark.extra.temporalCoverage &&
                          <ResultSubHead description={bookmark.extra.temporalCoverage.start+" - "+bookmark.extra.temporalCoverage.end} icon="flaticon-event-calendar-symbol"/>
                        }
                        {bookmark.extra && bookmark.extra.geographicCoverage &&
                          <ResultSubHead description={bookmark.extra.geographicCoverage.originalLocation} icon="flaticon2-placeholder"/>
                        }
                    </div>
                      <div className="cev-widget__info">
                        <div className="cev-widget__desc">
                          {bookmark.description && _.map(bookmark.description, (v, k)=>(
                              <React.Fragment key={k}>
                                {_.map(v, (v2, k2)=>(
                                  <div key={k+k2}
                                    dangerouslySetInnerHTML={{
                                      __html: this.limpiar(v2)
                                    }}
                                  />
                                ))}
                              </React.Fragment>
                            ))}
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </>
    );
  }
}

export default Bookmarks;