import React, { Component } from "react";
import _ from "lodash";
import striptags from "striptags";
import { METADA_MAP } from "../../config/const";

const flattenKeys = (obj, path = []) =>
    !_.isObject(obj)
        ? { [path.join('.')]: obj }
        : _.reduce(obj, (cum, next, key) => _.merge(cum, flattenKeys(next, [...path, key])), {});

class MetaData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      result: this.props.records,
      products: [
       
      ],
      columns: [
        {
          dataField: "name",
          text: "Metadato"
        },
        {
          dataField: "description",
          text: "Valor"
        }
      ]
    };
  }
  
  componentDidMount(){
    if(this.props.records.records){
      delete this.props.records.records;
    }
    if(this.props.records._id){
      delete this.props.records._id;
    }
    if(this.props.records.identifier){
      delete this.props.records.identifier;
    }
    if(this.props.records.ResourceGroupId){
      delete this.props.records.ResourceGroupId;
    }
    if(this.props.records.metadata._id){
      delete this.props.records.metadata._id;
    }
    let i = 0;

    let list = _.map(flattenKeys(this.props.records), (v, k)=>{
      let name = k.split(".").filter(item => isNaN(item)).join(".");
      return {
          id: ++i,
          name: METADA_MAP[name] ? METADA_MAP[name] : null,
          description: v !== null && isNaN(v) ? v.replace(/[[]{}]/g,'') : v,
        }
    })

    list = Object.values(list.reduce((item, {id,name,description}) => {
      item[name] = item[name] || {id,name,description: ''};
      item[name].description = item[name].description ? item[name].description + "\r\n" + description : description; 
      return item;
    }, {})).filter(item => item.name !== null && item.description !== null );

    this.setState({products: list})
  }

  limpiar(metadata){
    let tx = metadata.toString();
    tx = striptags(tx,['i']);
    tx = tx.replace(/\xA0/g,'');
    tx = tx.replace(/" , "/g,'<br />');
    tx = tx.replace(/\"/g,'');
    tx = tx.replace(/\[/g,'');
    tx = tx.replace(/\]/g,'');
    tx = tx.replace(null,'No registra');
    return tx.trim();
  }

  render() {
    return (
      <>
        {this.state.products &&
          <div className="card card-custom card-stretch gutter-b">
            <div className="card-header border-0">
              <h4 className="card-title font-weight-bolder text-dark my-2 float-left">Metadata</h4>
              {this.props.actions.showTree &&
                <button className="btn btn-brand btn-icon-md float-right" onClick={this.props.actions.showTree}>
                  <i className="la la-angle-down"/>
                  <span>Ver Fondos</span>
                </button>
              }
            </div>
            <div className="card-body">
              <div className="cev-widget2">
                {this.state.products.map((metadata, idx) => (
                  <>
                    {metadata.name !== null && metadata.description !== null &&
                      <div key={idx} className="cev-widget2__item cev-widget2__item--warning">  
                        <div className="cev-widget2__info ml-4">
                          <span className="cev-widget2__title">{metadata.name}</span>
                          <span className="cev-widget2__username">
                            <div dangerouslySetInnerHTML={{
                                __html: this.limpiar(metadata.description)
                              }}
                            />
                          </span>
                        </div>
                      </div>
                    }
                  </>
                ))}
              </div>
            </div>
          </div>
        }
      </>
    );
  }
}

export default MetaData;
