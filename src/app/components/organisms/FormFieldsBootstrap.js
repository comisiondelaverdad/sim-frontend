import React, { Component } from "react";
import { Form, Col } from "react-bootstrap";
import { saveAs } from 'file-saver';
import $ from 'jquery';
import * as select2 from 'select2';
import FormAuthor from './FormAuthor';
import FormLocationMultiple from './FormLocationMultiple';
import RecursiveSelect from '../molecules/RecursiveSelect';
import { selectRecursive$, removeSelectRecursive } from '../../services/selectListObserver';
import { is_array, is_string } from './../../services/utils';
export default class FormFieldsBootstrap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      formData: {},
      formConfig: props.formConfig,
      sections: props.sections,
      loaded: false,
    }
    this.select2 = select2;
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeFile = this.handleChangeFile.bind(this);
    this.handleChangeDate = this.handleChangeDate.bind(this);
    this.handleChangeSelect = this.handleChangeSelect.bind(this);
    this.handleChangeSelectMultiple = this.handleChangeSelectMultiple.bind(this);
    this.handleMagic = this.handleMagic.bind(this);
    this.clearFile = this.clearFile.bind(this);
    this.downloadBase64Zip = this.downloadBase64Zip.bind(this);
    this.downloadBase64 = this.downloadBase64.bind(this);
    this.outputAuthor = this.outputAuthor.bind(this);
    this.outputLocation = this.outputLocation.bind(this);
    this.handleChangeFileBase64Compressed = this.handleChangeFileBase64Compressed.bind(this);
    this.handleChangeFileBase64 = this.handleChangeFileBase64.bind(this);

    this.subscription = selectRecursive$.subscribe((data) => {
      if (JSON.stringify(data) !== '{}') {
        let form = {
          ...this.state.formData, ...data
        };
        this.setState({ formData: form });
        this.props.outputEvent({ formData: form });
      }
    })

  }

  outputAuthor(event) {
    const formData = { formData: { ...this.state.formData, ...event } };
    this.setState(formData);
    this.props.outputEvent(formData);
  }

  outputLocation(event) {
    const formData = { formData: { ...this.state.formData, ...event } };
    this.setState(formData);
    this.props.outputEvent(formData);
  }

  handleChangeSelect(event) {
    const { name, value, selectedOptions, multiple } = event.target;
    let form = {
      ...this.state.formData, ...{
        [name]: multiple ? [...selectedOptions]
          .map((data) => (data.value))
          .filter((data) => (data !== '')) : value
      }
    };
    this.setState({ formData: form });
    this.props.outputEvent({ formData: form });
  }

  componentDidMount() {
    this.mountForm();
  }

  componentWillUnmount() {
    removeSelectRecursive();
    this.subscription.unsubscribe();
  }

  mountForm() {
    this.props.formConfig.map((configElement) => {
      switch (configElement.type) {
        case 'select-multiple2':
          $(document).ready(() => {
            $('#' + configElement.id).select2({
              placeholder: configElement.placeholder
            });
            $('#' + configElement.id).on('change', (e) => {
              let form = {
                ...this.state.formData, ...{ [configElement.id]: ($('#' + configElement.id).select2('data')) }
              }
              form[configElement.id] = form[configElement.id].map((selected) => (selected.id))
              this.setState({ formData: form });
              this.props.outputEvent({ formData: form });
            });
          });
          break;
        case 'select-multiple2-tags':
          $(document).ready(() => {
            $('#' + configElement.id).select2({
              placeholder: configElement.placeholder,
              tags: true,
              tokenSeparators: ['|'],
              language: "es"
            });
            $('#' + configElement.id).on('change', (e) => {
              let form = {
                ...this.state.formData, ...{ [configElement.id]: ($('#' + configElement.id).select2('data')) }
              }
              form[configElement.id] = form[configElement.id].map((selected) => (selected.id))
              this.setState({ formData: form });
              this.props.outputEvent({ formData: form });
            });
          });
          break;
        case 'file':
          $(document).ready(() => {
            $('.clear-files-cev').on('change', (e) => {
            });

          });
          break;
        case 'base64zip':
          $(document).ready(() => {
            let base64value = this.state.formData[configElement.id];
            if (base64value && base64value != null) {
              var JSZip = require("jszip");
              var zip = new JSZip();
              var arrayBase64 = base64value.split(",");
              const filename = arrayBase64[0];
              const contentType = arrayBase64[1].split(";")[0];
              const b64Data = arrayBase64[2];

              const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
                const byteCharacters = atob(b64Data);
                const byteArrays = [];

                for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                  const slice = byteCharacters.slice(offset, offset + sliceSize);

                  const byteNumbers = new Array(slice.length);
                  for (let i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                  }

                  const byteArray = new Uint8Array(byteNumbers);
                  byteArrays.push(byteArray);
                }

                const blob = new Blob(byteArrays, { type: contentType });
                return blob;
              }
              const file = b64toBlob(b64Data, contentType);
              file.name = filename;
              var valorKb = 0;
              if (file) {
                valorKb = parseInt(file.size / 1024, 10);
              }
              file.valorKb = valorKb
              file.isValid = true;
              file.url = URL.createObjectURL(file);
              let filesDoc = [];
              filesDoc.push(
                {
                  file: file,
                  i: 0,
                  id: configElement.id,
                }
              )

              this.setState({ [configElement.id]: filesDoc });
              this.props.outputEvent({ [configElement.id]: filesDoc });

            }
          });
          break;
        case 'base64':
          $(document).ready(() => {
            let base64value = this.state.formData[configElement.id];
            if (base64value && base64value != null) {

              var arrayBase64 = base64value.split(",");
              const filename = arrayBase64[0];
              const contentType = arrayBase64[1].split(";")[0];
              const b64Data = arrayBase64[2];

              const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
                const byteCharacters = atob(b64Data);
                const byteArrays = [];

                for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                  const slice = byteCharacters.slice(offset, offset + sliceSize);

                  const byteNumbers = new Array(slice.length);
                  for (let i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                  }

                  const byteArray = new Uint8Array(byteNumbers);
                  byteArrays.push(byteArray);
                }

                const blob = new Blob(byteArrays, { type: contentType });
                return blob;
              }

              const file = b64toBlob(b64Data, contentType);

              file.name = filename;
              var valorKb = 0;
              if (file) {
                valorKb = parseInt(file.size / 1024, 10);
              }
              file.valorKb = valorKb
              file.isValid = true;
              file.url = URL.createObjectURL(file);
              let filesDoc = [];
              filesDoc.push(
                {
                  file: file,
                  i: 0,
                  id: configElement.id,
                }
              )

              this.setState({ [configElement.id]: filesDoc });
              this.props.outputEvent({ [configElement.id]: filesDoc });

            }
          });
          break;

        default:
          break;
      }
      return configElement
    })
  }

  componentDidUpdate(prevProps) {
    let elementHTML = null;
    if (JSON.stringify(prevProps.formConfig) !== JSON.stringify(this.props.formConfig)) {
      this.setState({ formConfig: this.props.formConfig });
      this.mountForm();
    }
    if (prevProps.formData !== this.props.formData && !this.state.loaded) {
      if (JSON.stringify(this.props.formData) !== "{}") {
        this.setState({ loaded: true });
      }
      this.setState({ formData: this.props.formData });
      for (let fieldID in this.props.formData) {
        let configElement = this.props.formConfig.filter((field) => (field.id === fieldID))[0];
        if (configElement && (this.props.formData[fieldID] !== null)) {
          switch (configElement.type) {
            case 'text':
              elementHTML = document.getElementById(fieldID);
              if (elementHTML) {
                elementHTML.value = this.props.formData[fieldID] ? this.props.formData[fieldID] : '';
              }
              break;
            case 'pattern':
              elementHTML = document.getElementById(fieldID);
              if (elementHTML) {
                elementHTML.value = this.props.formData[fieldID] ? this.props.formData[fieldID] : '';
              }
              break;
            case 'textarea':
              elementHTML = document.getElementById(fieldID);
              if (elementHTML) {
                elementHTML.value = this.props.formData[fieldID] ? this.props.formData[fieldID] : '';
              }
              break;
            case 'select':
              elementHTML = document.getElementById(fieldID);
              if (elementHTML) {
                elementHTML.value = this.props.formData[fieldID] ? this.props.formData[fieldID] : '';
              } break;
            case 'select-multiple':
              //(elementHTML.options).filter((option) => (this.props.formData[fieldID].includes(option.value))).map((op)=>(op.select = true));
              break;
            case 'select-multiple2':
              $(document).ready(() => {
                $('#' + configElement.id).on('change', (e) => {
                  let form = {
                    ...this.state.formData, ...{ [configElement.id]: ($('#' + configElement.id).select2('data')) }
                  }
                  form[configElement.id] = form[configElement.id].map((selected) => (selected.id))
                  this.setState({ formData: form });
                  this.props.outputEvent({ formData: form });
                });
              });
              break;
            case 'select-multiple2-tags':
              $(document).ready(() => {
                $('#' + configElement.id).on('change', (e) => {
                  let form = {
                    ...this.state.formData, ...{ [configElement.id]: ($('#' + configElement.id).select2('data')) }
                  }
                  form[configElement.id] = form[configElement.id].map((selected) => (selected.id))
                  this.setState({ formData: form });
                  this.props.outputEvent({ formData: form });
                });
              });
              break;
            case 'simple-date':
              if ((typeof this.props.formData[fieldID]) !== 'undefined') {
                //debugger;
                let elementdd = document.getElementsByName(fieldID + '-dd')[0];
                let elementmm = document.getElementsByName(fieldID + '-mm')[0];
                let elementaaaa = document.getElementsByName(fieldID + '-aaaa')[0];
                if (typeof (this.props.formData[fieldID]) === 'string') {
                  if (((this.props.formData[fieldID]).split('-')).length > 0) {
                    let date = (this.props.formData[fieldID]).split('-')
                    elementaaaa.value = date[0];
                    elementmm.value = date[1];
                    elementdd.value = date[2];
                  }
                }
              }
              break;
            case 'range-date-year':
              if ((typeof this.props.formData[fieldID]) !== 'undefined') {
                let start = document.getElementsByName(fieldID + '-start')[0];
                let end = document.getElementsByName(fieldID + '-end')[0];
                if (typeof (this.props.formData[fieldID]) === 'string') {
                  let date = (this.props.formData[fieldID]).split('-')
                  start.value = date[0];
                } else {
                  if ((this.props.formData[fieldID]).start) {
                    if (((this.props.formData[fieldID]).start.split('-')).length > 0) {
                      let date = (this.props.formData[fieldID]).start.split('-')
                      start.value = date[0];
                    }
                  }
                  if ((this.props.formData[fieldID]).end) {
                    if (((this.props.formData[fieldID]).end.split('-')).length > 0) {
                      let date = (this.props.formData[fieldID]).end.split('-')
                      end.value = date[0];
                    }
                  }
                }
              }
              break;
            case 'base64zip':
              $(document).ready(() => {
                let base64value = this.state.formData[configElement.id];
                if (base64value && base64value != null) {
                  var JSZip = require("jszip");
                  var zip = new JSZip();
                  var arrayBase64 = base64value.split(",");
                  const filename = arrayBase64[0];
                  const contentType = arrayBase64[1].split(";")[0];
                  const b64Data = arrayBase64[2];

                  const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
                    const byteCharacters = atob(b64Data);
                    const byteArrays = [];

                    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                      const slice = byteCharacters.slice(offset, offset + sliceSize);

                      const byteNumbers = new Array(slice.length);
                      for (let i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                      }

                      const byteArray = new Uint8Array(byteNumbers);
                      byteArrays.push(byteArray);
                    }

                    const blob = new Blob(byteArrays, { type: contentType });
                    return blob;
                  }
                  const file = b64toBlob(b64Data, contentType);
                  file.name = filename;
                  var valorKb = 0;
                  if (file) {
                    valorKb = parseInt(file.size / 1024, 10);
                  }
                  file.valorKb = valorKb
                  file.isValid = true;
                  file.url = URL.createObjectURL(file);
                  let filesDoc = [];
                  filesDoc.push(
                    {
                      file: file,
                      i: 0,
                      id: configElement.id,
                    }
                  )

                  this.setState({ [configElement.id]: filesDoc });
                  this.props.outputEvent({ [configElement.id]: filesDoc });

                }
              });
              break;
            case 'base64':
              $(document).ready(() => {
                let base64value = this.state.formData[configElement.id];
                if (base64value && base64value != null) {

                  var arrayBase64 = base64value.split(",");
                  const filename = arrayBase64[0];
                  const contentType = arrayBase64[1].split(";")[0];
                  const b64Data = arrayBase64[2];

                  const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
                    const byteCharacters = atob(b64Data);
                    const byteArrays = [];

                    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                      const slice = byteCharacters.slice(offset, offset + sliceSize);

                      const byteNumbers = new Array(slice.length);
                      for (let i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                      }

                      const byteArray = new Uint8Array(byteNumbers);
                      byteArrays.push(byteArray);
                    }

                    const blob = new Blob(byteArrays, { type: contentType });
                    return blob;
                  }

                  const file = b64toBlob(b64Data, contentType);

                  file.name = filename;
                  var valorKb = 0;
                  if (file) {
                    valorKb = parseInt(file.size / 1024, 10);
                  }
                  file.valorKb = valorKb
                  file.isValid = true;
                  file.url = URL.createObjectURL(file);
                  let filesDoc = [];
                  filesDoc.push(
                    {
                      file: file,
                      i: 0,
                      id: configElement.id,
                    }
                  )

                  this.setState({ [configElement.id]: filesDoc });
                  this.props.outputEvent({ [configElement.id]: filesDoc });

                }
              });
              break;
            default:
              new Event('change');
              break;

          }
        }
      }
    }


  }

  clearFile(index, id, label) {

    let files = this.state[id];
    files.splice(index, 1);
    this.setState({ [id]: files })
    this.props.outputEvent({ [id]: files });

    let form = {
      ...this.state.formData
    };
    form[id] = null;
    this.setState({ formData: form });
    this.props.outputEvent({ formData: form });

    if (files.length === 0) {
      let fieldFile = document.getElementById(id)
      fieldFile.value = "";
      $('#' + id)
        .removeAttr("hidden")
        .removeAttr("disabled");
      $('#' + id)
        .siblings(".custom-file-label")
        .removeClass("hide")
        .html('<i className="fas fa-paperclip"></i>' + label);
    }

  }

  downloadBase64Zip(index, id, label) {
    let files = this.state[id];
    var JSZip = require("jszip");
    JSZip.loadAsync(files[0].file)
      .then((zip) => {
        zip.forEach((relativePath, zipEntry) => {
          zipEntry.async("blob").then((content) => {
            let reader = new FileReader();
            saveAs(content, files[0].file.name);
          });
        });
      }, function (e) {
        alert("Error reading: " + e.message);
      });
  }

  downloadBase64(index, id, label) {
    let files = this.state[id];
    saveAs(files[0].file, files[0].file.name);
  }

  handleChangeDate(event) {
    const { name, value } = event.target;
    const DDMMAAAA = name.split("-");
    if (DDMMAAAA.includes('start') || DDMMAAAA.includes('end')) {
      let actualDate = { start: '', end: '' };
      if (typeof this.state.formData[DDMMAAAA[0]] !== 'undefined') {
        actualDate = (this.state.formData[DDMMAAAA[0]]);
      }
      if (DDMMAAAA.includes('start')) {
        actualDate = { ...actualDate, ...{ start: value ? `${value}-01-01` : null } };
      } else if (DDMMAAAA.includes('end')) {
        actualDate = { ...actualDate, ...{ end: value ? `${value}-12-30` : null } };
      }
      let form = {
        ...this.state.formData, ...{ [DDMMAAAA[0]]: actualDate }
      };
      this.setState({ formData: form });
      this.props.outputEvent({ formData: form });
    } else {
      let actualDate = ['', '', ''];
      if (typeof this.state.formData[DDMMAAAA[0]] !== 'undefined') {
        actualDate = (this.state.formData[DDMMAAAA[0]]) ? (this.state.formData[DDMMAAAA[0]]).split("-") : ['', '', ''];
      }

      switch (DDMMAAAA[1]) {
        case "dd":
          actualDate[2] = parseInt(value, 10) < 10 ? `0${parseInt(value, 10)}` : `${parseInt(value, 10)}`
          break;
        case "mm":
          actualDate[1] = parseInt(value, 10) < 10 ? `0${parseInt(value, 10)}` : `${parseInt(value, 10)}`
          break;
        case "aaaa":
          actualDate[0] = `${parseInt(value, 10)}`;
          break;
        default:
          break;
      }
      const dateString = `${actualDate[0]}-${actualDate[1]}-${actualDate[2]}` === '--' ? null : `${actualDate[0]}-${actualDate[1]}-${actualDate[2]}`;
      const path_regex4 = /\d{4}-\d{2}-\d{2}/;
      let dateFormat = (path_regex4.exec(dateString)) ? dateString : null;
      let form = {
        ...this.state.formData, ...{ [DDMMAAAA[0]]: dateString }
      };

      this.setState({ formData: form });
      if (dateFormat) {
        this.props.outputEvent({ formData: form });
      }
    }
  }

  handleMagic(event, key = 0) {
    let form = { ...this.state.formData, ...{ [event.id]: event.action.defaultMagic } };
    if (this.state.formConfig[key].type === 'simple-date') {
      const newFormComfig = this.state.formConfig;
      newFormComfig[key].required = false;

      this.setState({ formData: form });
      this.props.outputEvent({ formData: form });
    }
    switch (event.type) {
      case 'simple-date':
        this.setState({ formData: form });
        this.props.outputEvent({ formData: form });
        let elementdd = document.getElementsByName(event.id + '-dd')[0];
        let elementmm = document.getElementsByName(event.id + '-mm')[0];
        let elementaaaa = document.getElementsByName(event.id + '-aaaa')[0];
        if (elementdd && elementmm && elementaaaa) {
          elementaaaa.value = '';
          elementaaaa.required = false;
          elementmm.value = '';
          elementmm.required = false;
          elementdd.value = '';
          elementdd.required = false;
        }

        break;
      default:
        break;
    }
  }

  handleChangeFile(event) {
    let { name, files } = event.target;
    var len = files.length
    let filesDoc = [];
    for (var i = 0; i < len; i++) {
      var file = files[i];
      var valorKb = 0;
      if (file) {
        valorKb = parseInt(file.size / 1024, 10);
      }
      file.valorKb = valorKb
      file.isValid = true;
      file.url = URL.createObjectURL(file);

      filesDoc.push(
        {
          file: file,
          i: i,
          id: name,
        }
      )
    }
    this.setState({ [name]: filesDoc });
    this.props.outputEvent({ [name]: filesDoc });
  }

  handleChangeFileBase64(event) {
    let { name, files } = event.target;
    var len = files.length
    let filesDoc = [];
    for (var i = 0; i < len; i++) {
      var file = files[i];
      var valorKb = 0;
      if (file) {
        valorKb = parseInt(file.size / 1024, 10);
      }
      file.valorKb = valorKb
      file.isValid = true;
      file.url = URL.createObjectURL(file);

      filesDoc.push(
        {
          file: file,
          i: i,
          id: name,
        }
      )
    }

    let reader = new FileReader();
    reader.readAsDataURL(filesDoc[0].file);
    reader.onloadend = (() => {
      var base64data = filesDoc[0].file.name + ',' + reader.result;
      let form = {
        ...this.state.formData, ...{ [name]: base64data }
      };
      this.setState({ formData: form });
      this.props.outputEvent({ formData: form });
      this.setState({ [name]: filesDoc });
      this.props.outputEvent({ [name]: filesDoc });
    });

    reader.onerror = function () {
      console.log("On error");
      console.log(reader.error);
    };
  }


  handleChangeFileBase64Compressed(event) {
    let { name, files } = event.target;
    var len = files.length
    let filesDoc = [];
    for (var i = 0; i < len; i++) {
      var file = files[i];
      var valorKb = 0;
      if (file) {
        valorKb = parseInt(file.size / 1024, 10);
      }
      file.valorKb = valorKb
      file.isValid = true;
      file.url = URL.createObjectURL(file);

      filesDoc.push(
        {
          file: file,
          i: i,
          id: name,
        }
      )
    }

    let reader = new FileReader();
    reader.readAsText(filesDoc[0].file);
    reader.onload = (() => {
      let texto_tozip = reader.result;
      var JSZip = require("jszip");
      var zip = new JSZip();
      zip.file(filesDoc[0].name, texto_tozip);
      zip.generateAsync({
        type: "blob",
        compression: "DEFLATE",
        compressionOptions: {
          level: 9
        }
      }).then((blobs) => {
        let value = blobs;
        var reader2 = new FileReader();
        reader2.readAsDataURL(blobs);
        reader2.onloadend = (() => {
          var base64data = filesDoc[0].file.name + ',' + reader2.result;
          let form = {
            ...this.state.formData, ...{ [name]: base64data }
          };
          this.setState({ formData: form });
          this.props.outputEvent({ formData: form });
          this.setState({ [name]: filesDoc });
          this.props.outputEvent({ [name]: filesDoc });
        })
      });
    });
    reader.onerror = function () {
      console.log("On error");
      console.log(reader.error);
    };
  }


  handleChange(event) {
    const { name, value } = event.target;
    let form = {
      ...this.state.formData, ...{ [name]: value }
    };
    this.setState({ formData: form });
    this.props.outputEvent({ formData: form });
  }

  handleChangeSelectMultiple(event) {
    console.log(event);
  }

  render() {
    const { formConfig, formData } = this.state;
    return (
      <div className="col-md-12">
        <Form.Row>
          {formConfig.map((field, key) => {
            if (field.type === 'file' && !field.hide) return (
              <div key={key} className={field.col ? `form-group col-md-${field.col}` : 'form-group col-md-12'}>
                <label className="form-label">{field.label}{field.required ? ' *' : ''}</label>
                <div className="custom-file">
                  <input
                    type={field.type}
                    className="custom-file-input"
                    name={field.id}
                    id={field.id}
                    required={field.required ? field.required : false}
                    onChange={this.handleChangeFile}
                    multiple={field.multiple ? field.multiple : false}
                  />
                  <label className="custom-file-label" htmlFor={field.id}>
                    {field.placeholder}
                  </label>
                  <div className="files-container">
                    {this.state[field.id] && (
                      this.state[field.id].map((fileDoc, index) => (
                        <div key={index} className={`tag-govco tag-negative tag-files fileItem-${index}`}>
                          <span>{`${fileDoc.file.name} - (${fileDoc.file.valorKb} KB)`}</span>
                          <button type="button" className="clear-files-cev" onClick={() => (this.clearFile(index, fileDoc.id, field.placeholder))}>
                            <i className="fas fa-times"></i>
                          </button>
                        </div>
                      ))
                    )}
                  </div>
                </div>
              </div>
            )
            if (field.type === 'base64zip' && !field.hide) return (
              <div key={key} className={field.col ? `form-group col-md-${field.col}` : 'form-group col-md-12'}>
                <label className="form-label">{field.label}{field.required ? ' *' : ''}</label>
                <div className="custom-file">
                  <input
                    type={'file'}
                    accept={".mhx"}
                    className="custom-file-input"
                    name={field.id}
                    id={field.id}
                    required={field.required ? field.required : false}
                    onChange={this.handleChangeFileBase64Compressed}
                    multiple={false}
                  />
                  <label className="custom-file-label" htmlFor={field.id}>
                    {field.placeholder}
                  </label>
                  <div className="files-container">
                    {this.state[field.id] && (
                      this.state[field.id].map((fileDoc, index) => (
                        <div key={index} className={`tag-govco tag-negative tag-files fileItem-${index}`}>
                          <span>{`${fileDoc.file.name} - (${fileDoc.file.valorKb} KB)`}</span>
                          <button type="button" className="clear-files-cev" onClick={() => (this.clearFile(index, fileDoc.id, field.placeholder))}>
                            <i className="fas fa-times"></i>
                          </button>
                          <button type="button" className="clear-files-cev" onClick={() => (this.downloadBase64Zip(index, fileDoc.id, field.placeholder))}>
                            <i className="fas fa-download"></i>
                          </button>
                        </div>
                      ))
                    )}
                  </div>
                </div>
              </div>
            )
            if (field.type === 'base64' && !field.hide) return (
              <div key={key} className={field.col ? `form-group col-md-${field.col}` : 'form-group col-md-12'}>
                <label className="form-label">{field.label}{field.required ? ' *' : ''}</label>
                <div className="custom-file">
                  <input
                    type={'file'}
                    className="custom-file-input"
                    name={field.id}
                    id={field.id}
                    required={field.required ? field.required : false}
                    onChange={this.handleChangeFileBase64}
                    multiple={false}
                  />
                  <label className="custom-file-label" htmlFor={field.id}>
                    {field.placeholder}
                  </label>
                  <div className="files-container">
                    {this.state[field.id] && (
                      this.state[field.id].map((fileDoc, index) => (
                        <div key={index} className={`tag-govco tag-negative tag-files fileItem-${index}`}>
                          <span>{`${fileDoc.file.name} - (${fileDoc.file.valorKb} KB)`}</span>
                          <button type="button" className="clear-files-cev" onClick={() => (this.clearFile(index, fileDoc.id, field.placeholder))}>
                            <i className="fas fa-times"></i>
                          </button>
                          <button type="button" className="clear-files-cev" onClick={() => (this.downloadBase64(index, fileDoc.id, field.placeholder))}>
                            <i className="fas fa-download"></i>
                          </button>
                        </div>
                      ))
                    )}
                  </div>
                </div>
              </div>
            )
            if (field.type === 'text' && !field.hide) return (
              <Form.Group key={key} as={Col} md={field.col ? field.col : "6"} controlId={field.id} >
                <Form.Label className="form-label">{field.label}{field.required ? ' *' : ''}</Form.Label>
                <Form.Control
                  type={field.type}
                  name={field.id}
                  disabled={field.disabled ? field.disabled : false}
                  placeholder={field.placeholder}
                  required={field.required ? field.required : false}
                  onChange={this.handleChange}
                >
                </Form.Control>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
              </Form.Group>
            )
            if (field.type === 'pattern' && !field.hide) return (
              <Form.Group key={key} as={Col} md={field.col ? field.col : "6"} controlId={field.id} >
                <Form.Label className="form-label">{field.label}{field.required ? ' *' : ''}</Form.Label>
                <Form.Control
                  type={field.type}
                  name={field.id}
                  disabled={field.disabled ? field.disabled : false}
                  patern={field.pattern ? field.pattern : false}
                  placeholder={field.placeholder}
                  required={field.required ? field.required : false}
                  onChange={this.handleChange}
                >
                </Form.Control>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
              </Form.Group>
            )
            if (field.type === 'separator') return (
              <div key={key} className="col-12">
                <div className="separator"></div>
              </div>
            )
            if (field.type === 'select' && !field.hide) return (
              <Form.Group key={key} as={Col} md={field.col ? field.col : "6"} controlId={field.id}>
                <Form.Label className="form-label">{field.label}{field.required ? ' *' : ''}</Form.Label>
                <Form.Control
                  as="select"
                  name={field.id}
                  required={field.required ? field.required : false}
                  onChange={this.handleChangeSelect}
                >
                  <option key={0} value="">{field.placeholder}</option>
                  {field.options.map((data, key2) => (<option key={key2} value={data.value}>{data.name}</option>))}
                </Form.Control>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
                {field.infoHTML && <div dangerouslySetInnerHTML={{ __html: field.infoHTML }} />}
              </Form.Group>
            )
            if (field.type === 'select-multiple' && !field.hide) return (
              <Form.Group key={key} as={Col} md={field.col ? field.col : "6"} controlId={field.id} >
                <Form.Label className="form-label">{field.label}{field.required ? ' *' : ''}</Form.Label>
                <Form.Control
                  as="select"
                  name={field.id}
                  value={this.state.formData[field.id]}
                  required={field.required ? field.required : false}
                  onChange={this.handleChangeSelect}
                  multiple>
                  <option key={0} value="">{field.placeholder}</option>
                  {field.options.map((data, key2) => (<option key={key2} value={data.value} selected={this.state.formData.hasOwnProperty(field.id) ? this.state.formData[field.id].includes(data.value) ? true : false : false}>{data.name}</option>))}                </Form.Control>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
              </Form.Group>
            )
            if (field.type === 'select-multiple2' && !field.hide) return (
              <div key={key} className={field.col ? `form-group col-md-${field.col}` : 'form-group col-md-12'}>
                <label className="form-label">{field.label}{field.required ? ' *' : ''}</label>
                <select
                  multiple="multiple"
                  title={field.placeholder}
                  className="required form-control"
                  name={field.id}
                  id={field.id}
                  required={field.required ? field.required : false}
                  onChange={this.handleChangeSelect2}
                >
                  {field.options.map((data, key2) => (<option key={key2} value={data.value}
                    selected={this.state.formData[field.id] ? this.state.formData[field.id].includes(data.value) ? true : false : false}>{data.name}</option>))}
                </select>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
              </div>
            )
            if (field.type === 'select-multiple2-recursive' && !field.hide) return (
              <RecursiveSelect key={key}
                field={field}
                item={field.options}
                data={["Hechos victimizantes - Amenaza al derecho a la vida - Amenaza por seguimiento"]}
                level={1}>
              </RecursiveSelect>
            )
            if (field.type === 'select-recursive' && !field.hide) return (
              <RecursiveSelect key={key}
                field={field}
                item={field.options}
                //data={"Hechos victimizantes - Amenaza al derecho a la vida - Amenaza por carta"}
                data={formData[field.id] ? formData[field.id] : ''}
                level={1}
              >
              </RecursiveSelect>
            )
            if (field.type === 'select-multiple2-tags' && !field.hide) return (
              <div key={key} className={field.col ? `form-group col-md-${field.col}` : 'form-group col-md-12'}>
                <label className="form-label">{field.label}{field.required ? ' *' : ''}</label>
                <select
                  multiple="multiple"
                  title={field.placeholder}
                  className="required form-control"
                  name={field.id}
                  id={field.id}
                  required={field.required ? field.required : false}
                  onChange={this.handleChangeSelect2}
                >
                  {field.options.map((data, key2) => (<option key={key2} value={data.value}
                    selected={this.state.formData[field.id] ? this.state.formData[field.id].includes(data.value) ? true : false : false}>{data.name}</option>))}
                </select>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
              </div>
            )

            if (field.type === 'date' && !field.hide) return (
              <Form.Group key={key} as={Col} md={field.col ? field.col : "6"} controlId={field.id} >
                <Form.Label className="form-label">{field.label}{field.required ? ' *' : ''}</Form.Label>
                <Form.Control
                  type={field.type ? field.type : 'text'}
                  name={field.id}
                  placeholder={field.placeholder}
                  required={field.required ? field.required : false}
                  value={this.state.formData.hasOwnProperty(field.id) ? this.state.formData[field.id] : ''}
                  onChange={this.handleChange}
                >
                </Form.Control>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
              </Form.Group>
            )
            if (field.type === 'textarea' && !field.hide) return (
              <Form.Group key={key} as={Col} md={field.col ? field.col : "6"} controlId={field.id} >
                <Form.Label className="form-label">{field.label}{field.required ? ' *' : ''}</Form.Label>
                <Form.Control
                  as='textarea'
                  name={field.id}
                  placeholder={field.placeholder}
                  rows={field.rows}
                  required={field.required ? field.required : false}
                  value={this.state.formData.hasOwnProperty(field.id) ? this.state.formData[field.id] ? this.state.formData[field.id] : '' : ''}
                  onChange={this.handleChange}
                >
                </Form.Control>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
              </Form.Group>
            )
            if (field.type === 'author' && !field.hide) return (
              <div key={key} className="col-12">
                <Form.Label className="form-label">{field.label}{field.required ? ' *' : ''}</Form.Label>
                <Form.Text>{field.info}</Form.Text>
                <FormAuthor
                  authorConfig={field.authorConfig}
                  formData={this.state.formData.hasOwnProperty(field.id) ? this.state.formData[field.id] : ''}
                  outputEvent={this.outputAuthor}
                  name={field.id}
                  required={field.required ? field.required : false}></FormAuthor>
              </div>
            )
            if (field.type === 'location' && !field.hide) return (
              <div key={key} className="col-12">
                <Form.Label className="form-label">{field.label}{field.required ? ' *' : ''}</Form.Label>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
                <FormLocationMultiple
                  outputEvent={this.outputLocation}
                  multiple={field.multiple}
                  name={field.id}
                  formData={this.state.formData.hasOwnProperty(field.id) ? this.state.formData[field.id] : ''}
                ></FormLocationMultiple>
              </div>
            )
            if (field.type === 'simple-date' && !field.hide) return (
              <div key={key} className={`col-md-${field.col}`}>
                <Form.Label className="form-label">{field.label}{field.required ? ' *' : ''}</Form.Label>
                <Form.Text className="text-muted">
                  {field.info}
                </Form.Text>
                <div className='row'>
                  <Form.Group as={Col} xs={`${(field.action ? field.action.state : false && field.required) ? '3' : '4'}`}>
                    <Form.Control
                      name={`${field.id}-aaaa`}
                      id={`${field.id}-aaaa`}
                      type="number"
                      placeholder="aaaa"
                      min="1800"
                      max="2070"
                      required={field.required}
                      onChange={this.handleChangeDate}
                    >
                    </Form.Control>
                  </Form.Group>

                  <Form.Group as={Col} xs={`${(field.action ? field.action.state : false && field.required) ? '3' : '4'}`}>
                    <Form.Control
                      name={`${field.id}-mm`}
                      id={`${field.id}-mm`}
                      type="number"
                      placeholder="mm"
                      min="1"
                      max="12"
                      required={field.required}
                      onChange={this.handleChangeDate}
                    >
                    </Form.Control>
                  </Form.Group>

                  <Form.Group as={Col} xs={`${(field.action ? field.action.state : false && field.required) ? '3' : '4'}`}>
                    <Form.Control
                      name={`${field.id}-dd`}
                      id={`${field.id}-dd`}
                      type="number"
                      placeholder="dd"
                      min="1"
                      max="31"
                      required={field.required}
                      onChange={this.handleChangeDate}
                    >
                    </Form.Control>
                  </Form.Group>
                  {(field.action ? field.action.state : false) && field.required && (
                    <div className="col-3">
                      <button type="button" className="btn btn-clean btn-bold btn-upper"
                        onClick={() => { this.handleMagic(field, key) }}>
                        {(field.action.type === 'icon') && (
                          <i className={field.action.icon}></i>
                        )}
                        {(field.action.type === 'text') && (
                          <>{field.action.text}</>
                        )}
                      </button>
                    </div>
                  )}
                </div>
              </div>
            )
            if (field.type === 'range-date-year' && !field.hide) return (
              <div key={key} className={`col-md-${field.col}`}>
                <Form.Label className="form-label">{field.label}{field.required.start && field.required.end ? ' *' : ''}</Form.Label>
                <Form.Text className="text-muted">
                  {field.infoField}
                </Form.Text>
                <div className='row'>
                  <Form.Group as={Col} xs={`6`}>
                    <Form.Control
                      name={`${field.id}-start`}
                      id={`${field.id}-start`}
                      type={field.typeField.start}
                      placeholder={field.placeholder.start}
                      min={field.min.start}
                      max={field.max.end}
                      required={field.required.start}
                      onChange={this.handleChangeDate}
                    >
                    </Form.Control>
                  </Form.Group>
                  <Form.Group as={Col} xs={`6`}>
                    <Form.Control
                      name={`${field.id}-end`}
                      id={`${field.id}-end`}
                      type={field.typeField.end}
                      placeholder={field.placeholder.end}
                      min={field.min.start}
                      max={field.max.end}
                      required={field.required.end}
                      onChange={this.handleChangeDate}
                    >
                    </Form.Control>
                  </Form.Group>
                </div>
              </div>
            )
          })}
        </Form.Row>
      </div>
    )
  }
}