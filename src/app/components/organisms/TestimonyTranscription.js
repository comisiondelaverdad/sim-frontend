import React, { Component } from "react";
import {connect} from "react-redux";
import striptags from "striptags";
import ReactWaterMark from 'react-watermark-component';
import ControlBar from "../molecules/ControlBar";
import { Container } from "react-bootstrap";
import PerfectScrollbar from "react-perfect-scrollbar";

class TestimonyTranscription extends Component {
  constructor(props){
    super(props);
    this.testimonyRef = new React.createRef();
  }

  limpiar(records,keyword){
    let transcription = records.find(x=> x.type === "Transcripción final" || x.type === "Transcripción parcial");
    let tx = transcription && transcription.content ? transcription.content : 'Transcripción en proceso';
    tx = striptags(tx,['i']);
    tx = tx.replace(/\xA0/g,'');
    tx = tx.replace(/ENT+[\s0-9:]*/g,"<br /><br /><span class='badge badge-primary'>ENT: </span>");
    tx = tx.replace(/TEST+[\s0-9:]*/g,"<br /><br /><span class='badge badge-success'>TEST: </span>");
    if(keyword){
      keyword = keyword.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
      let searchExp = new RegExp(keyword,"gi");
      tx = tx.replace(searchExp,"<span class='badge badge-warning'><i>"+keyword+"</i></span>");
    }
    return tx.trim();
  }

  render() {
    const options = {
      chunkWidth: 450,
      chunkHeight: 90,
      textAlign: 'left',
      textBaseline: 'bottom',
      globalAlpha: 0.2,
      font: '36px Microsoft Yahei',
      rotateAngle: -0.1,
      fillStyle: '#666'
    }

    return (
      <>
      <ReactWaterMark
        waterMarkText={this.props.user.name}
        options={options}
      >
        <ControlBar user={this.props.user.name} config={{themes:true,textUP:true,textDOWN:true,fontFamily:true,fontSize:true,fullScreen:true}} container={"cev-testimony"}> 

            <Container
              id="cev-testimony" 
              className="text-justify"
              dangerouslySetInnerHTML={{
                __html: this.limpiar(this.props.records,this.props.keyword)
              }}
              fluid={true}
            />
        </ControlBar>
      </ReactWaterMark>
      </>
    );
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
});

export default connect(
  mapStateToProps
)(TestimonyTranscription);