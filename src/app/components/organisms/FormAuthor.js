import React, { Component } from "react";
import { Button, Form, Col } from "react-bootstrap";

export default class FormAuthor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name,
      authorConfig: props.authorConfig,
      formData: {},
      required: props.required,
      author: [{ nombre: '', apellido: '', organizacion: '', sigla: '' }],
      authorOutput: [''],
      loaded: false,
    }
    this.handleSwitch = this.handleSwitch.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.removeField = this.removeField.bind(this);
    this.removeField = this.removeField.bind(this);


  }

  componentDidUpdate(prevProps) {
    if (prevProps.formData !== this.props.formData && !this.state.loaded && this.props.formData) {
      this.setState({ loaded: true });
      const author = this.props.formData.map(a => {
        const splitAuthor = a.split(',');
        const splitOrg = a.split('-');
        if (splitAuthor.length === 2 && splitOrg.length < 2) {
          return { nombre: splitAuthor[1], apellido: splitAuthor[0], organizacion: '', sigla: '' }
        } else {
          return { nombre: '', apellido: '', organizacion: splitOrg[0], sigla: splitOrg[1] }
        }
      })
      const authorConfig = author.map((a, i) => {
        if (a.organizacion === '' && a.nombre === '') {
          return this.props.authorConfig[0];
        } else {
          return { key: i, statusOrg: a.organizacion !== '' };
        }
      })
      this.setState({ authorConfig: authorConfig });
      this.setState({ author: author });
      this.setState({ authorOutput: this.props.formData });
      this.props.outputEvent({ [this.props.name]: this.props.formData });
    }
  }

  addField(key) {
    const authorConfig = [...this.state.authorConfig];
    const author = [...this.state.author];
    const authorOutput = [...this.state.authorOutput];

    authorConfig.push({ key: key + 1, statusOrg: this.state.authorConfig[key].statusOrg });
    author.push({ nombre: '', apellido: '', organizacion: '', sigla: '' });
    authorOutput.push('');

    this.setState({ authorConfig: authorConfig });
    this.setState({ author: author });
    this.setState({ authorOutput: authorOutput });
  }
  removeField(key) {
    const authorConfig = this.state.authorConfig;
    const author = this.state.author;
    const authorOutput = this.state.authorOutput;

    authorConfig.splice(key, 1);
    author.splice(key, 1);
    authorOutput.splice(key, 1);

    this.setState({ authorConfig: authorConfig });
    this.setState({ author: author });
    this.setState({ authorOutput: authorOutput });
  }

  handleSwitch(event) {
    const { value, checked } = event.target;
    let authorConfig = this.state['authorConfig'];
    let author = this.state['author'];

    authorConfig[value] = { key: value, statusOrg: checked };
    author[value] = { ...author[value], ...{ nombre: '', apellido: '', organizacion: '', sigla: '' } };
    this.setState({ authorConfig: authorConfig });
  }

  handleChange(event) {
    const { name, value } = event.target;
    const field = (name.split("-"));
    const index = parseInt(field[1], 10)
    let author = this.state['author'];
    author[index] = { ...author[index], ...{ [field[0]]: value } };
    this.setState({ author: author });
    let authorOutput = this.state.authorOutput;
    if (this.state.authorConfig[index].statusOrg) {
      authorOutput[index] = `${this.state.author[index].organizacion}-${this.state.author[index].sigla ? this.state.author[index].sigla : ''}`;
    } else {
      authorOutput[index] = `${this.state.author[index].apellido},${this.state.author[index].nombre ? this.state.author[index].nombre : ''}`;
    }
    this.setState({ authorOutput: authorOutput });
    this.props.outputEvent({
      [this.props.name]: authorOutput
    });

  }

  render() {
    return (
      this.state.authorConfig.map((data, key) => {
        if (!data.statusOrg) {
          return (
            <Form.Row key={key}>
              <div className="col-md-12">
                <div className="row">
                  <div className="col-md-1 switch-element">
                    <label class="switch">
                      <input className="switch"
                        checked={data.statusOrg}
                        type="checkbox"
                        value={key}
                        name={"check-" + key}
                        onChange={this.handleSwitch}
                      />
                      <span className="slider round"></span>
                    </label>
                  </div>

                  <Form.Group as={Col} md="5" controlId={"nombre-" + key} >
                    <Form.Label></Form.Label>
                    <Form.Control
                      type="text"
                      value={this.state.author[key].nombre}
                      name={"nombre-" + key}
                      placeholder="Nombre(s)"
                      required={this.state.required}
                      onChange={this.handleChange}
                    >
                    </Form.Control>
                  </Form.Group>

                  <Form.Group as={Col} md="5" controlId={"apellido-" + key} >
                    <Form.Label></Form.Label>
                    <Form.Control
                      type="text"
                      value={this.state.author[key].apellido}
                      name={"apellido-" + key}
                      placeholder="Apellido(s)"
                      required={this.state.required}
                      onChange={this.handleChange}
                    >
                    </Form.Control>
                  </Form.Group>

                  <div className="col-md-1 container-button">
                    {(this.state.authorConfig.length > 1) && (this.state.authorConfig.length - 1 === (key)) && (
                      <Button variant="btn btn-secondary btn-sm btn-icon btn-circle ml-2"
                        onClick={() => { this.removeField(key) }}>
                        <i className="la la-trash"></i>
                      </Button>
                    )}
                    {(this.state.authorConfig.length - 1 === (key)) && (
                      <Button variant="btn btn-primary btn-sm btn-icon btn-circle ml-2"
                        onClick={() => { this.addField(key) }}>
                        <i className="la la-plus"></i>
                      </Button>
                    )}
                  </div>

                </div>
              </div>

            </Form.Row >
          )
        } else {
          return (
            <Form.Row key={key}>
              <div className="col-md-12">
                <div className="row">
                  <div className="col-md-1 switch-element">
                    <label class="switch">
                      <input className="switch"
                        checked={data.statusOrg}
                        type="checkbox"
                        value={key}
                        name={"check-" + key}
                        onChange={this.handleSwitch}
                      />
                      <span className="slider round"></span>
                    </label>
                  </div>

                  <Form.Group as={Col} md="8" controlId={"organizacion-" + key}>
                    <Form.Label></Form.Label>
                    <Form.Control
                      type="text"
                      value={this.state.author[key].organizacion}
                      name={"organizacion-" + key}
                      placeholder="Nombre de la institución"
                      required={this.state.required}
                      onChange={this.handleChange}
                    >
                    </Form.Control>
                  </Form.Group>

                  <Form.Group as={Col} md="2" controlId={"sigla-" + key}>
                    <Form.Label></Form.Label>
                    <Form.Control
                      type="text"
                      value={this.state.author[key].sigla}
                      name={"sigla-" + key}
                      placeholder="Sigla"
                      onChange={this.handleChange}
                    >
                    </Form.Control>
                  </Form.Group>
                  <div className="col-md-1 container-button">
                    {(this.state.authorConfig.length > 1) && (
                      <Button variant="btn btn-secondary btn-sm btn-icon btn-circle ml-2"
                        onClick={() => { this.removeField(key) }}>
                        <i className="la la-trash"></i>
                      </Button>
                    )}
                    {(this.state.authorConfig.length - 1 === (key)) && (
                      <Button variant="btn btn-primary btn-sm btn-icon btn-circle ml-2"
                        onClick={() => { this.addField(key) }}>
                        <i className="la la-plus"></i>
                      </Button>
                    )}
                  </div>

                </div>
              </div>
            </Form.Row >
          )
        }
      })
    )
  }
}