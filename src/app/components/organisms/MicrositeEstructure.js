import React, { useState, useEffect } from "react";
import * as CollectionService from "../../services/CollectionService";
import { Card } from "react-bootstrap";
import { LABELS_MENU } from "../../config/constLabels";
import SectionMicrosite from "./SectionMicrosite";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

const MicrositeEstructure = (props) => {
  const [loading, setLoading] = useState(false);
  const [option, setOption] = useState();
  const [elements, setElements] = useState(props.elements);

  useEffect(() => {
    setElements(props.elements);
  }, [props.elements]);

  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  };

  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    const items = reorder(
      elements,
      result.source.index,
      result.destination.index
    );
    setElements(items);
    props.reOrder(items)
    
  };

  const renderElements = () => {
    if (elements && elements.length) {
      return elements.map((element, index) => (
        <Draggable key={element.id} draggableId={element.id} index={index}>
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
          
            >
              <SectionMicrosite
                key={index}
                id={element.id}
                title={element.title}
                subtitle={element.subtitle}
                color={element.color}
                metadato={element.metadato}
                values={element.values}
                relevants={element.relevants}
                saveElement={props.saveElement}
                idMenu={props.idMenu}
                deleteElement={props.deleteElement}
                index = {index}
               
              />
            </div>
          )}
        </Draggable>
      ));
    }
  };

  return (
    <>
      {loading && (
        <div className="fa-3x">
          <i className="fas fa-spinner fa-spin"></i>
        </div>
      )}
      {!loading ? (
        elements && elements.length ? (
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided, snapshot) => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  // style={getListStyle(snapshot.isDraggingOver)}
                >
                  {renderElements()}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        ) : (
          ""
        )
      ) : (
        ""
      )}
    </>
  );
};

export default MicrositeEstructure;
