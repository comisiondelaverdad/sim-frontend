import React, { Component } from 'react'
import { connect } from "react-redux"
import BiGram from "../../molecules/BiGram"
import ThreeGram from "../../molecules/ThreeGram"
import FourGram from "../../molecules/FourGram"
import MenuDesplegable from "../../ui/Utils/MenuDesplegable"
import * as VizService from "../../../services/VizService"
import * as app from "../../../store/ducks/app.duck"
import * as PIXI from 'pixi.js'

class NGram extends Component {
    constructor(props) {
        super(props)
        this.state = {
            vista: 'bigram',
            loading: true,
            buckets: [],
            margin: 0
        }
        this._zoom = React.createRef();


        this.getBuckets = this.getBuckets.bind(this)
        this.cambiarFiltroNGram = this.cambiarFiltroNGram.bind(this)
        this.cambiarVista = this.cambiarVista.bind(this)
        this.updateZoom = this.updateZoom.bind(this)
        this.setWebGL = this.setWebGL.bind(this)
    }

    componentDidMount() {
        let query = new URLSearchParams(this.props.location.search);
        if (query.toString() !== '') {
            this.getBuckets(this.props.searchFilters)
        } else {
            this.getBuckets(null)
        }

        this.setWebGL()
    }

    cambiarVista(e) {
        let name = e.target.getAttribute('value')
        this.setState({
            vista: name
        })
    }

    cambiarFiltroNGram(e, tipo) {
        this.props.changeFilter(e, tipo)
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.vista !== this.state.vista) {
            this.getBuckets(null)
        }
    }

    getBuckets(params_) {
        let self = this
        let params = {
            ngram: self.state.vista,
            origin: "ModuloCaptura"
        }

        if (params_ !== null) {
            params = params_
        }

        if (this.props.filtros !== undefined) {
            if (this.props.filtros[0].filtros.length > 0) params.ngramFilter = this.props.filtros[0].filtros
            if (this.props.filtros[1].filtros.length > 0) params.etiquetasFilter = this.props.filtros[1].filtros
            if (this.props.filtros[2].filtros.length > 0) params.entidadesFilter = this.props.filtros[2].filtros
        }

        this.setState({
            loading: true
        })

        // console.log(params)

        VizService.getNgram(params)
            .then(
                (data) => {
                    console.log(data)
                    this.setState({
                        buckets: data.aggregations.tipo.ngram.total.buckets,
                        loading: false
                    })
                    console.log(this.state)
                },
                (error) => {
                    console.log(error)
                }
            )
    }

    updateZoom(e) {
    }

    setWebGL() {
        let width = 100
        let height = (document.body.clientHeight / 100) * 80
        let parent = this

        this.renderer = new PIXI.Application({
            width: width,
            height: height,
            antialias: true,
            transparent: true,
            resolution: (window.devicePixelRatio > 1) ? 2 : 1,
            autoDensity: true,
            powerPreference: "high-performance",
            // backgroundColor: 0x999999
        })

        this._zoom.current.appendChild(this.renderer.view)

        this.stage = new PIXI.Container()

        const context = new PIXI.Graphics();

        context.lineStyle(1, '0x999999')
        context.beginFill('0x999999')
        context.drawRect(width - 20, 0, 20, 50)
        context.endFill()

        context.interactive = true
        context.buttonMode = true
        context.on('pointerdown', onDragStart)
            .on('pointerup', onDragEnd)
            .on('pointerupoutside', onDragEnd)
            .on('pointermove', onDragMove)

        this.stage.addChild(context)

        function onDragStart(event) {
            this.inicio = event.data.global.y - this.y
            this.data = event.data;
            this.dragging = true;
        }

        function onDragEnd() {
            this.alpha = 1;
            this.dragging = false;
            // this.data = null;
        }

        function onDragMove() {
            if (this.dragging) {
                this.alpha = .5
                const newPosition = this.data.getLocalPosition(this.parent);
                let posY = newPosition.y - this.inicio
                // posY >= 0 && posY <=20 ? posY = 0 : null
                if (posY < 0) posY = 0
                if (posY > height - 50) posY = height - 50
                if (posY >= 0 && posY <= height - 50) this.y = posY

                parent.setState({
                    margin: (posY / height) * 7000
                })
            }
        }

        this.renderer.stage.addChild(this.stage)
    }

    render() {
        return (
            <>
                <div>
                    <MenuDesplegable
                        item={this.state.vista}
                        list={[
                            "bigram",
                            "3gram",
                            "4gram"
                        ]}
                        onClick={this.cambiarVista}
                    />
                    <div className="containerVizScroll">
                        <div className="inside">
                            {!this.state.loading && this.state.vista === 'bigram' &&
                                <BiGram setFilter={this.cambiarFiltroNGram} margin={this.state.margin} buckets={this.state.buckets} />
                            }
                            {!this.state.loading && this.state.vista === '3gram' &&
                                <ThreeGram setFilter={this.cambiarFiltroNGram} margin={this.state.margin} buckets={this.state.buckets} />
                            }
                            {!this.state.loading && this.state.vista === '4gram' &&
                                <FourGram setFilter={this.cambiarFiltroNGram} margin={this.state.margin} buckets={this.state.buckets} />
                            }
                        </div>

                        <nav className="scroll" ref={this._zoom}>
                        </nav>
                    </div>
                </div>
            </>
        )
    }
}

const mapStateToProps = store => ({
    search: store.app.keyword,
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(NGram);