import React from 'react'
import NGram from './NGram'

const DualNGram = props => {

    return (
        <>
            <div className="dual">
                <NGram key={`gramview1_${JSON.stringify(props.filters)}`} filtros={props.filters} changeFilter={props.changeFilter} location={props.location} />
                <NGram key={`gramview2_${JSON.stringify(props.filters)}`} filtros={props.filters} changeFilter={props.changeFilter} location={props.location} />
            </div>
        </>
    )
}

export default DualNGram