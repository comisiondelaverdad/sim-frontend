import React, { useRef, useState, useEffect } from 'react'
import Arbol from "../../molecules/Arbol"
import ListadoEtiquetas from "../../molecules/ListadoEtiquetas"
import MenuDesplegable from "../../ui/Utils/MenuDesplegable"
import * as PIXI from 'pixi.js'

const ArbolVista = props => {
    const _zoom = useRef(null)

    const [vista, cambiarVista] = useState(props.active)
    const [marginTop, cambiarMargen] = useState(0)

    console.log(props)

    useEffect(() => {
        setWebGL()
    }, [_zoom])

    function setWebGL() {
        let width = 100
        let height = (document.body.clientHeight / 100) * 80

        const renderer = new PIXI.Application({
            width: width,
            height: height,
            antialias: true,
            transparent: true,
            resolution: (window.devicePixelRatio > 1) ? 2 : 1,
            autoDensity: true,
            powerPreference: "high-performance",
            // backgroundColor: 0x999999
        })

        _zoom.current.appendChild(renderer.view)

        const stage = new PIXI.Container()

        const context = new PIXI.Graphics();

        context.lineStyle(1, '0x999999')
        context.beginFill('0x999999')
        context.drawRect(width - 20, 0, 20, 50)
        context.endFill()

        context.interactive = true
        context.buttonMode = true
        context.on('pointerdown', onDragStart)
            .on('pointerup', onDragEnd)
            .on('pointerupoutside', onDragEnd)
            .on('pointermove', onDragMove)

        stage.addChild(context)

        function onDragStart(event) {
            this.inicio = event.data.global.y - this.y
            this.data = event.data;
            this.dragging = true;
        }

        function onDragEnd() {
            this.alpha = 1;
            this.dragging = false;
            // this.data = null;
        }

        function onDragMove() {
            if (this.dragging) {
                this.alpha = .5
                const newPosition = this.data.getLocalPosition(this.parent);
                let posY = newPosition.y - this.inicio
                // posY >= 0 && posY <=20 ? posY = 0 : null
                if (posY < 0) posY = 0
                if (posY > height - 50) posY = height - 50
                if (posY >= 0 && posY <= height - 50) this.y = posY

                cambiarMargen((posY / height) * 40000)
            }
        }

        renderer.stage.addChild(stage)
    }

    return (
        <>
            <MenuDesplegable
                item={vista}
                list={[
                    "vista arbol",
                    "vista lista"
                ]}
                onClick={e => { cambiarVista(e.target.getAttribute('value')) }}
            />
            <div className="containerVizScroll">
                <div className="inside">
                    {vista === 'vista arbol' &&
                        <Arbol changeFilter={props.changeFilter} margin={marginTop} root={props.root} />
                    }
                    {vista === 'vista lista' &&
                        <ListadoEtiquetas changeFilter={props.changeFilter} margin={marginTop} root={props.root} />
                    }
                </div>

                <nav className="scroll" ref={_zoom}>
                </nav>
            </div>
        </>
    )
}

export default ArbolVista