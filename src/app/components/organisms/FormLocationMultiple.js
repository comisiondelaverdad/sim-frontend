import React, { Component } from "react";
import FormLocation from "./../molecules/FormLocation"
import { Button, Form } from "react-bootstrap";
import { getLocationByText } from "./../../services/arcgisService";
import { uuidv4 } from './../../services/utils';
export default class FormLocationMultiple extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: props.name,
            multiple: props.multiple,
            formData: props.formData ? props.formData : [{ geoPoint: { lat: 0, lon: 0 } }],
            formConfig: [{
                country: { required: false },
                departament: { required: false },
                city: { required: false }
            }],
            formReturn: [{ geoPoint: { lat: 0, lon: 0 } }],
            loaded: false,
        }
        this.outputLocation = this.outputLocation.bind(this);

    }

    componentDidUpdate(prevProps) {
        if (prevProps.formData !== this.props.formData && !this.state.loaded) {
            if (this.props.formData) {
                const formConfig = this.props.formData.map((fd) => {
                    return {
                        key: uuidv4(),
                        country: { required: false },
                        departament: { required: false },
                        city: { required: false }
                    }
                })
                this.setState({ formConfig: formConfig })
                this.setState({ loaded: true, formData: this.props.formData, formReturn: this.props.formData });
            }
        }
    }
    async outputLocation({ key, event }) {
        const locationReturn = [...this.state.formReturn];
        if(event.place) {
            if (event.place.length > 1) {
                let codeDivipola = '';
                codeDivipola += event.formData.country;
                codeDivipola += event.formData.departament ? `-${event.formData.departament}` : '';
                codeDivipola += event.formData.local ? `-${event.formData.local}` : '';
                const geoData = await getLocationByText(event.place.join(','));
                const { X, Y } = geoData.candidates[0].attributes;
                locationReturn[key] = {
                    code: codeDivipola,
                    name: event.place.join(','),
                    geoPoint: { lat: Y, lon: X },
                }
                this.setState({ formReturn: locationReturn });
                this.props.outputEvent({ [this.state.name]: locationReturn });
            } else {
                locationReturn[key] = {
                    code: event.formData.country,
                    name: event.place.join(','),
                    geoPoint: event.geoPoint
                }
                this.setState({ formReturn: locationReturn });
                this.props.outputEvent({ [this.state.name]: locationReturn });
            }
        } else {
            locationReturn[key] = {
                code: null,
                name: null,
                geoPoint: { lat: 0, lon: 0},
            }
            this.setState({ formReturn: locationReturn });
            this.props.outputEvent({ [this.state.name]: locationReturn });
        }
    }

    addField(key) {
        const locationConfig = [...this.state.formConfig];
        const formData = [...this.state.formData];
        const locationReturn = [...this.state.formReturn];
        locationConfig.push({
            country: { required: true },
            departament: { required: false },
            city: { required: false }
        });
        formData.push([{ code: '', name: '', geopoint: { lat: 0, lon: 0 } }]);
        this.setState({ formConfig: locationConfig, formReturn: locationReturn, formData: formData });
    }

    removeField(key) {
        const locationConfig = this.state.formConfig;
        const formData = [...this.state.formData];
        const locationReturn = [...this.state.formReturn];
        locationConfig.splice(key, 1);
        locationReturn.splice(key, 1);
        formData.splice(key, 1);
        this.props.outputEvent({ [this.state.name]: locationReturn });
        this.setState({ formData: formData, formConfig: locationConfig, formReturn: locationReturn });
    }
    openMap() {

    }

    render() {
        const { formConfig, formData, multiple } = this.state;
        return (
            <>
                {formConfig.map((value, key) => {
                    return (
                        <div className="row col-12" key={key}>
                            <div className={multiple ? "col-md-11" : "col-md-12"}>
                                <Form.Row>
                                    <div className="col-12">
                                        <FormLocation
                                            keyValue={key}
                                            formConfig={value}
                                            formData={formData[key]}
                                            outputEvent={(event) => { this.outputLocation({ key, event }) }}></FormLocation>
                                    </div>
                                </Form.Row>
                            </div>
                            {multiple && (
                                <div className="col-md-1 container-button">
                                    {(formConfig.length > 1) && (
                                        <Button variant="btn btn-secondary btn-sm btn-icon btn-circle ml-2"
                                            onClick={() => { this.removeField(key) }}>
                                            <i className="la la-trash"></i>
                                        </Button>
                                    )}
                                    {(formConfig.length - 1 === key) && (
                                        <Button variant="btn btn-primary btn-sm btn-icon btn-circle ml-2"
                                            onClick={() => { this.addField(key) }}>
                                            <i className="la la-plus"></i>
                                        </Button>
                                    )}
                                </div>
                            )}
                        </div>
                    )
                }
                )}
            </>
        )
    }
}
