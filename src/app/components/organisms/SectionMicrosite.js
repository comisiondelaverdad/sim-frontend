import React, { useState, useEffect } from "react";
import Accordion from "react-bootstrap/Accordion";
import { Form, Card, Col } from "react-bootstrap";
import * as CollectionService from "../../services/CollectionService";
import Select, { components } from "react-select";
import { v4 as uuidv4 } from "uuid";
import DataTable from "react-data-table-component";

const SectionMicrosite = (props) => {
  const styleCard = {
    header: { fontWeight: "900" },
    label: { fontWeight: "400 !important" },
  };
  const [loading, setLoading] = useState(true);
  const [title, setTitle] = useState(props.title || "Nueva sección");
  const [subtitle, setSubtitle] = useState(props.subtitle || "");
  const [color, setColor] = useState(props.color || "blanco");
  const [metadato, setMetadato] = useState(props.metadato || "");
  const [values, setValues] = useState(props.values || []);
  const [options, setOptions] = useState([]);
  const [show1, setShow1] = useState(false);
  const [collections, setCollections] = useState([]);
  const [relevants, setRelevants] = useState(props.relevants || []);
  const [id, setId] = useState(props.id || uuidv4());

  const deleteSection = () => {
    if (window.confirm("Está seguro que desea eliminar esta sección?")) {
      props.deleteElement(id);
    }
  };

  const saveElement = () => {
    if (!title) {
      alert("No es posible guardar una sección sin título");
      return false;
    }
    if (!metadato) {
      alert("No es posible guardar una sección sin metadato");
      return false;
    }
    if (!values || !values.length) {
      alert("No es posible guardar una sección sin valores de metadato");
      return false;
    }
    const element = {
      title,
      subtitle,
      metadato,
      color,
      values,
      relevants,
      id,
    };
    props.saveElement(element);
  };

  useEffect(() => {
    getValuesMetadato();
  }, [metadato]);

  useEffect(() => {
    getCollectionValues();
  }, [values]);

  const handleChange = (vals) => {
    setValues(vals);
    setRelevants([]);
    setCollections([]);
  };

  const getCollectionValues = async () => {
    if (metadato && values && values.length) {
      const modifYvalues = values.map((item) => item.value);
      const res = await CollectionService.collectionsByMetadata(
        metadato,
        modifYvalues
      );
      let collections = [];
      if (res && res.hits && res.hits.hits && res.hits.hits.length) {
        collections = res.hits.hits.map((item) => {
          if (item._source) {
            item._source._id = item._id;
            return item._source;
          }
        });
        setCollections(collections);
      }

      // if (res.length) setCollections(res);
    }
  };

  const getValuesMetadato = async () => {
    if (metadato) {
      let res = await CollectionService.getAggregation(metadato);
      if (res.length) {
        res = res.map((item) => {
          if (metadato == "user" && item._id.name)
            return { value: item._id._id, label: item._id.name };
          else return { value: item._id, label: item._id };
        });

        res = res.filter((item) => item.value != "");
        setOptions(res);
      }
    }
  };
  const setRelevant = (id) => {
    const index = relevants.indexOf(id);
    if (index != "-1") {
      setRelevants(relevants.filter((item) => item !== id));
    } else {
      if (relevants.length < 4) setRelevants((relevants) => [...relevants, id]);
      else {
        alert("Ya existen 4 Colecciones como relevantes");
      }
    }
  };

  const evaluateRelevant = (id) => {
    const index = relevants.indexOf(id);
    if (index != "-1") return true;
    else return false;
  };

  const saveSubtitle = (subtitle) => {
    setSubtitle(subtitle);
  };

  const saveTitle = (title) => {
    setTitle(title);
  };

  const columns = [
    {
      name: "Título",
      selector: (row) => row.title,
      sortable: true,
    },
    {
      name: "Relevante",
      width: "20%",
      cell: (row) => (
        <div className="form-check">
          <input
            onChange={() => setRelevant(row._id)}
            type="checkbox"
            className="form-check-input"
            id="exampleCheck1"
            value={row._id}
            checked={evaluateRelevant(row._id)}
          />
        </div>
      ),
    },
  ];

  return (
    <>
      <Accordion style={{ marginBottom: "6px", width: "100%" }}>
        <Card>
          <Card.Header style={styleCard.header}>
            <div className="d-flex justify-content-between align-items-center">
              <strong className="mx-4">{props.index + 1}</strong>
              <strong className="mx-auto">{title}</strong>
              <div>
                <button
                  onClick={(e) => {
                    setShow1(!show1);
                  }}
                  type="button"
                  className="btn btn-"
                >
                  <i className="fas fa-sort-down"></i>
                </button>
              </div>
            </div>
          </Card.Header>
          <Accordion.Collapse className={show1 ? "show" : ""} eventKey="1">
            <Card.Body>
              <Form>
                <Form.Group controlId="">
                  <Form.Label>Título *</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) => {
                      saveTitle(e.target.value);
                    }}
                    value={title}
                  />
                </Form.Group>
                <Form.Group controlId="">
                  <Form.Label>Subtítulo (opcional)</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) => saveSubtitle(e.target.value)}
                    value={subtitle}
                  />
                </Form.Group>
                <div className="form-group">
                  <Form.Label>Seleccione color</Form.Label>
                  <select
                    value={color}
                    className="form-control"
                    onChange={(e) => {
                      setColor(e.target.value);
                    }}
                  >
                    <option value="" disabled>
                      Seleccione...
                    </option>
                    <option value="blanco">Blanco</option>
                    <option value="azul">Azul</option>
                    <option value="rojo">Rojo</option>
                    <option value="verde">Verde</option>
                  </select>
                </div>
                <div className="form-group">
                  <Form.Label>Seleccione metadato *</Form.Label>
                  <select
                    value={metadato}
                    className="form-control"
                    id="exampleFormControlSelect1"
                    onChange={(e) => {
                      setValues([]);
                      setCollections([]);
                      setRelevants([]);
                      setMetadato(e.target.value);
                    }}
                  >
                    <option value="" disabled>
                      Seleccione...
                    </option>
                    <option value="title">Título</option>
                    <option value="keywords">Términos clave</option>
                    <option value="keywords_resources">Tema</option>
                    <option value="category">Categoría</option>
                    <option value="type">Tipo</option>
                    <option value="type_author">Autor</option>
                    <option value="user">Creador</option>
                    <option value="author">Curador</option>
                  </select>
                </div>
                <Form.Group controlId="my_multiselect_field">
                  <Form.Label>Selecciona valores del metadato *</Form.Label>

                  <Select
                    value={values}
                    onChange={handleChange}
                    options={options}
                    isMulti={true}
                    placeholder={<div>Seleccione</div>}
                  />
                </Form.Group>
              </Form>
              <hr />
              <Form.Label>
                Selecciona máximo 4 colecciones relevantes *
              </Form.Label>

              <DataTable
                columns={columns}
                data={collections}
                pagination
                noDataComponent={"No existen colecciones"}
                paginationComponentOptions={{
                  rowsPerPageText: "Filas por página",
                  rangeSeparatorText: "de",
                  selectAllRowsItem: true,
                  selectAllRowsItemText: "Todos",
                }}
              />

              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <button
                  onClick={deleteSection}
                  type="button"
                  className="btn btn-outline-danger"
                >
                  Eliminar sección
                </button>
                <button
                  onClick={() => saveElement()}
                  type="button"
                  className="btn btn-outline-success"
                >
                  Guardar sección
                </button>
              </div>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </>
  );
};

export default SectionMicrosite;
