import React, { useState, useEffect } from "react";

import AccordionMenu from "../molecules/AccordionMenu";

import Nestable from "react-nestable";
import "react-nestable/dist/styles/index.css";

function MenuEstructure(props) {
  const [list, setList] = useState(props.elements);
  const [elements, setElements] = useState([]);
  const [dragItem, setDragItem] = useState();

  useEffect(() => {
    if (props.elements) {
      setList(props.elements);
      const elementos = props.elements.map((item) => ({
        ...item,
      }));

      setElements(elementos);
    }
  }, [props.elements]);

  function replace(items) {
    console.log(items);
    props.updateElements(items);
  }

  const deleteItem = (id) => {
    props.deleteItem(id);
  };

  const renderItem = ({ item }) => (
    <AccordionMenu
      deleteItem={deleteItem}
      item={item}
      key={item.id}
      tag={item.tag}
      route={item.route}
      changeElement={props.changeElement}
      section = {props.section}
    />
  );

  return (
    <div>
      {elements.length ? (
        <Nestable
          onChange={(data) => replace(data.items)}
          idProp={"id"}
          items={elements}
          renderItem={renderItem}
        />
      ) : (
        <div className="alert alert-warning" role="alert">
          ¡Aún no existen items en la estructura de este menú!, puedes agregar rutas personalizadas o elementos...
        </div>
      )}
    </div>
  );
}
export default MenuEstructure;
