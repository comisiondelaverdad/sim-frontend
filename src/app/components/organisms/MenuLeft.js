import React, { useEffect } from "react";
import IconButton from "@material-ui/core/IconButton";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import { makeStyles } from "@material-ui/core";
import { menuBuscador, myMenu } from "../atoms/menuBuscador";
import ListSubheader from "@material-ui/core/ListSubheader";
import ItemMenu from "./ItemMenu";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { menuEvent$ } from "./../../services/MenusService";
const drawerWidth = 300;

const useStyles = makeStyles((theme) => ({
  cardMenu: {
    backgroundColor: "#2A5080",
    borderRadius: "unset",
    height: "64px",
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    // justifyContent: "flex-end",
    justifyContent: "space-between",
    color: "white",
  },
  divider: {
    backgroundColor: "#FFC258",
    height: "4px",
  },
  profileName: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: "flex-start",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    top: 60,
    backgroundColor: "#E6E6E6",
    width: drawerWidth,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  root: {
    width: "100%",
    maxWidth: 400,
    backgroundColor: "#E6E6E6",
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  media: {
    height: 140,
  },
  titleProfile:
    { marginLeft: "8px", marginBottom: '0px' }
}));

const MenuLeft = (props) => {

  const [open, setOpen] = React.useState(props.open);
  const [elements, setElements] = React.useState([]);
  const [active, setActive] = React.useState();
  const [user, setUser] = React.useState(props.user);

  useEffect(() => {
    loadItems(props.section);
  }, []);

  useEffect(() => {
    setOpen(props.open);
  }, [props.open, open]);

  const getName = () => {
    if (user.name) {
      let name = user.name.split(" ");
      if (name.length == 2)
        return name[0] + ' ' + name[1];
      else if (name.length >= 3)
        return name[0] + ' ' + name[2]
      else
        return name[0]
    }
    return 'Username';
  };

  const activate = (id) => setActive(id);

  const handleDrawerClose = () => {
    setOpen(false);
    props.handleDrawerClose(false);
  };
  const loadItems = async (section) => {
    let res = myMenu();
    if (res.elements) {
      setElements(res.elements);
    }
  };

  const classes = useStyles();
  const renderItems = elements.map((item, index) => (
    <ItemMenu activate={activate} active={active} key={item.id} item={item} />
  ));

  return (
    <>
      {
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <div>
            <Card className={classes.cardMenu}>
              <div className={classes.drawerHeader}>
                <Typography
                  className={classes.titleProfile}
                  gutterBottom
                  variant="h6"
                  component="h2"
                >
                  {getName()}
                </Typography>
                <IconButton
                  // style={{ marginBottom: "0px" }}
                  onClick={() => (handleDrawerClose())}
                >
                  <ArrowLeftIcon style={{ color: "white" }} fontSize="large" />
                </IconButton>
              </div>
            </Card>
          </div>

          <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
              <ListSubheader
                component="div"
                id="nested-list-subheader"
              ></ListSubheader>
            }
            className={classes.root}
          >
            {renderItems}
          </List>
        </Drawer>
      }
    </>
  );
};
const mapStateToProps = ({ auth: { user } }) => ({
  user,
});

export default withRouter(connect(mapStateToProps)(MenuLeft));

// export default MenuLeft;
