import React, { Component } from "react";
import Records from "../molecules/Records";

class RecordPreview extends Component {
  render() {
    return (
      <div className="cev-portlet__body">
        <div className="cev-widget4">
          <Records record={this.props.records}/>
        </div>
      </div>
    );
  }
}

export default RecordPreview;