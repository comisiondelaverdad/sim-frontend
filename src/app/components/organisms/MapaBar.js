import React, { Component } from 'react'
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import * as VizService from "../../services/VizService";
import MapaViz from '../molecules/MapaViz'
import NubeMapa from '../molecules/NubeMapa'
import Mapa3D from '../molecules/Mapa3D'
import Lottie from 'react-lottie';
import animationData from '../../../assets/viz-loading.json'


/**
 * Componente que se encarga de mostrar un mapa de leaflet y habilitar las herramientas de visualizacion para las entidades
 */
class MapaBar extends Component {
    constructor(props) {
        super(props)

        this.state = {
            loading: true,
            buckets: null,
            view: 'mapa'
        }

        this.updateMap = this.updateMap.bind(this)
        this.changeView = this.changeView.bind(this)
    }

    componentDidMount() {
        if (this.props.location) {
            let query = new URLSearchParams(this.props.location.search);

            if (query.toString() !== '') {
                this.updateMap(this.props.searchFilters);
            }
        } else if (this.props.bookmarks) {
            let idArray = []
            this.props.bookmarks.map(p => {
                idArray.push(p.path)
                return ''
            })

            let params = {
                idArray: idArray
            }

            this.updateMap(params)
        } else if (this.props.resource) {
            let params = {
                rg: this.props.resource[0]
              }
            this.updateMap(params)
        } else {
            this.updateDataFiltered()
        }
    }

    updateDataFiltered() {
        let params = {
            origin: "ModuloCaptura"
        }

        if (this.props.filtros !== undefined) {
            if (this.props.filtros[0].filtros.length > 0) params.ngramFilter = this.props.filtros[0].filtros
            if (this.props.filtros[1].filtros.length > 0) params.etiquetasFilter = this.props.filtros[1].filtros
            if (this.props.filtros[2].filtros.length > 0) params.entidadesFilter = this.props.filtros[2].filtros
        }

        this.updateMap(params)
    }

    updateMap(filters) {
        VizService.getMapaViz(filters)
            .then(data => {
                let response = []


                data.aggregations.tipo.cloud.total.buckets.map(d => {
                    if (d.centroid.count > 0) response.push(d)
                    return ''
                })

                VizService.getMapaVizEnlaces(filters)
                    .then(data_ => {
                        let response_ = []
                        let response__ = []
                        data_.aggregations.tipo.connections.total.buckets.map(d => {
                            if (d.centroid1.count > 0 && d.centroid2.count > 0) {
                                let obj = {
                                    'coor1': d.centroid1.location,
                                    'coor2': d.centroid2.location
                                }

                                response__.push(obj)
                            }
                            else if (d.centroid1.count > 0) {
                                let obj = {
                                    key: d.key.split('---')[1],
                                    centroid: d.centroid1.location,
                                    count: d.doc_count
                                }

                                response_.push(obj)
                            }
                            else if (d.centroid2.count > 0) {
                                let obj = {
                                    key: d.key.split('---')[0],
                                    centroid: d.centroid2.location,
                                    count: d.doc_count
                                }

                                response_.push(obj)
                            }
                            return ''
                        })

                        this.setState({
                            buckets: response,
                            buckets_entidades: response_,
                            buckets_lugares: response__,
                            loading: false
                        })

                        console.log(this.state)

                    })

                // this.setState({
                //     buckets: response
                // })
            })
    }

    changeView(event) {
        if (this.state.view !== event.target.getAttribute('value')) {
            this.setState({
                view: event.target.getAttribute('value')
            })
        }
    }

    render() {
        return (
            <>
                {this.state.loading &&
                    <div className="loading_viz">
                        <Lottie height={150} width={150} options={
                            {
                                loop: true,
                                autoplay: true,
                                animationData: animationData,
                                rendererSettings: {
                                    preserveAspectRatio: 'xMidYMid slice'
                                }
                            }
                        } />
                    </div>
                }
                {(this.state.buckets === null || this.state.buckets_entidades === null) && !this.state.loading &&
                    <>
                        <span class="badge badge-danger">Los resultados no tienen los datos suficientes para la visualización</span>
                    </>
                }
                {this.state.buckets && this.state.buckets_entidades &&
                    <div className="wordcloudBar mapa">
                        <div className="cloudContainer">
                            <div className="viewSelect">
                                <div value="mapa" className={this.state.view === 'mapa' ? 'item active' : 'item'} onClick={this.changeView}>Mapa de entidades</div>
                                {/* <div value="vista3d" className={this.state.view === 'vista3d' ? 'item active' : 'item'} onClick={this.changeView}>Mapa 3D</div> */}
                            </div>
                            {this.state.view === 'mapa' &&
                                <>
                                    <div className="map_inside">
                                        <MapaViz buckets={this.state.buckets} />
                                        <NubeMapa buckets={this.state.buckets_entidades} />
                                    </div>
                                </>
                            }
                            {this.state.view === 'vista3d' &&
                                <>
                                    <div className="map_inside">
                                        <Mapa3D bucketsd={this.state.buckets_lugares} />
                                    </div>
                                </>
                            }
                        </div>
                    </div>
                }
            </>
        )
    }
}

const mapStateToProps = store => ({
    searchFilters: store.app.filters
});

export default connect(
    mapStateToProps,
    app.actions
)(MapaBar);