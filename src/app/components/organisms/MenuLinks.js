import React, { useState, useEffect } from "react";
import { Button, Form, Card } from "react-bootstrap";
import Dropdown from "react-bootstrap/Dropdown";
import { v4 as uuidv4 } from "uuid";
import { toAbsoluteUrl } from "../../../theme/utils";
import Accordion from "react-bootstrap/Accordion";
import Tooltip from "react-bootstrap/Tooltip";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import { LABELS_MENU } from "../../../app/config/constLabels";
import { useTranslation } from "react-i18next";


const style = {
  width: "100%",
  backgroundColor: "white",
  color: "black",
};

const Icon = (props) => {
  return (
    <div>
      <svg className="cv-medium">
        <use
          xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#" + props.name)}
          className="flia-green"
        ></use>
      </svg>
      <span>{props.name}</span>
    </div>
  );
};

function MenuLinks(props) {
  const [t, i18n] = useTranslation("common");

  const [form, setForm] = useState({
    tag: "",
    route: "",
    tab: false,
    id: "",
    img: "",
    icon: null,
    only: false,
    material_icon: "",
    clases: "",
  });
  const [id_menu, setId] = useState(props.id_menu);
  const [show, setShow] = useState(false);
  const [toggleContents, setToggleContents] = useState(
    "Seleccione un icono..."
  );
  const [selectedItem, setSelectedItem] = useState();
  const [hover, setHover] = useState(false);

  function handleChangeTag(event) {
    setForm({
      ...form,
      tag: event.target.value,
      id: uuidv4(),
    });
  }
  function handleChangeRoute(event) {
    let { value } = event.target;
    let conditions = [" ", ","];
    if (conditions.some((el) => value.includes(el))) {
      alert("La ruta no debe llevar espacios, comas ni tildes!");
      return false;
    }
    value = value.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    setForm({
      ...form,
      route: event.target.value,
    });
  }
  function handleChangeTab(event) {
    setForm({
      ...form,
      tab: event.target.checked,
    });
  }
  function handleChangeOnlyIcon(event) {
    setForm({
      ...form,
      only: event.target.checked,
    });
  }

  function handleChangeImg(event) {
    setForm({
      ...form,
      img: event.target.value,
    });
  }
  function handleChangeIcon(icon) {
    setForm({
      ...form,
      icon: icon,
    });
  }
  function handleChangeMaterialIcon(event) {
    setForm({
      ...form,
      material_icon: event.target.value,
    });
  }
  function handleChangeColor(event) {
    setForm({
      ...form,
      clases: event.target.value,
    });
  }

  function clear() {
    setForm({
      ...form,
      tab: false,
      tag: "",
      route: "",
      id: "",
      img: "",
    });
  }

  return (
    <>
      {/* <Card style={{ width: "18rem", marginLeft: "10px" }}> */}
      <Accordion style={{ marginBottom: "6px", width: "100%" }}>
        <Card className="mb-2">
          <Card.Header style={{ fontWeight: "900" }}>
            <div className="d-flex justify-content-between align-items-center">
              <strong className="mx-auto">{t("micrositios.menus.customRoutes")}</strong>
              <div>
                <button
                  onClick={(e) => {
                    setShow(!show);
                  }}
                  type="button"
                  className="btn btn-"
                >
                  <i className="fas fa-sort-down"></i>
                </button>
              </div>
            </div>
          </Card.Header>
          <Accordion.Collapse className={show ? "show" : ""} eventKey="1">
            <Card.Body>
              <Form
                onSubmit={(e) => {
                  e.preventDefault();
                }}
              >
                <Form.Group controlId="">
                  <div className="d-flex justify-content-between">
                    <label>{t("micrositios.menus.navigationLabel")} *</label>
                    <OverlayTrigger
                      delay={{ hide: 1000, show: 200 }}
                      overlay={(props) => (
                        <Tooltip className="help_menu" {...props}>
                          Ingresa el texto a visualizar en el ítem del menú
                        </Tooltip>
                      )}
                      placement="bottom"
                    >
                      <i className="fas fa-question-circle"></i>
                    </OverlayTrigger>
                  </div>

                  <Form.Control
                    type="text"
                    placeholder=""
                    onChange={handleChangeTag}
                    value={form.tag}
                  />
                </Form.Group>
                <Form.Group controlId="">
                  <div className="d-flex justify-content-between">
                    <label>{t("micrositios.menus.route")} </label>
                    <OverlayTrigger
                      delay={{ hide: 1000, show: 200 }}
                      overlay={(props) => (
                        <Tooltip className="help_menu" {...props}>
                          Ingresa la ruta, Url o enlace interno o externo de la
                          aplicación. Ej: museo/conoce
                        </Tooltip>
                      )}
                      placement="bottom"
                    >
                      <i className="fas fa-question-circle"></i>
                    </OverlayTrigger>
                  </div>
                  <Form.Control
                    type="text"
                    placeholder=""
                    onChange={handleChangeRoute}
                    value={form.route}
                  />
                </Form.Group>

                <Form.Group controlId="">
                  <div className="d-flex justify-content-between">
                    <label>{t("micrositios.menus.image")} </label>
                    <OverlayTrigger
                      delay={{ hide: 1000, show: 200 }}
                      overlay={(props) => (
                        <Tooltip className="help_menu" {...props}>
                          Ingresa la ruta, Url de una imagen interna o externa.
                          Ej: assets/img/logo.jpg
                        </Tooltip>
                      )}
                      placement="bottom"
                    >
                      <i className="fas fa-question-circle"></i>
                    </OverlayTrigger>
                  </div>

                  <Form.Control
                    type="text"
                    placeholder=""
                    onChange={handleChangeImg}
                    value={form.img}
                  />
                </Form.Group>
                {props.section && props.section == "1" ? (
                  <Form.Group>
                    <div className="d-flex justify-content-between">
                      <label>{t("micrositios.menus.icon")} </label>
                      <OverlayTrigger
                        delay={{ hide: 1000, show: 200 }}
                        overlay={(props) => (
                          <Tooltip className="help_menu" {...props}>
                            Selecciona un ícono del listado
                          </Tooltip>
                        )}
                        placement="bottom"
                      >
                        <i className="fas fa-question-circle"></i>
                      </OverlayTrigger>
                    </div>

                    <Dropdown
                      className="text-left  dropdown-menu-right"
                      align="end"
                      style={{}}
                      size="lg"
                      onSelect={(eventKey) => {
                        handleChangeIcon(eventKey);
                        if (eventKey)
                          setToggleContents(
                            <>
                              <Icon name={eventKey} />
                            </>
                          );
                        else setToggleContents("Seleccione un icono...");
                      }}
                    >
                      <Dropdown.Toggle
                        align="end"
                        variant="primary"
                        id="dropdown-flags"
                        className="text-left border dropdown-menu-right"
                        style={hover ? style : style}
                        onMouseLeave={() => setHover(false)}
                        onMouseEnter={() => setHover(true)}
                      >
                        {toggleContents}
                      </Dropdown.Toggle>

                      <Dropdown.Menu
                        align="end"
                        className="dropdown-menu-right"
                        style={{ width: "100%" }}
                      >
                        {
                          <>
                            <Dropdown.Item eventKey={null}>
                              Seleccione un ícono...
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-localizacion"}>
                              <Icon name="icono-localizacion" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-entrevista"}>
                              <Icon name="icono-entrevista" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-audio"}>
                              <Icon name="icono-audio" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-calendar"}>
                              <Icon name="icono-calendar" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-informe"}>
                              <Icon name="icono-informe" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-fichacorta"}>
                              <Icon name="icono-fichacorta" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-concepto"}>
                              <Icon name="icono-concepto" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-metadata"}>
                              <Icon name="icono-metadata" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-casos"}>
                              <Icon name="icono-casos" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-acceso"}>
                              <Icon name="icono-acceso" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-acta"}>
                              <Icon name="icono-acta" />
                            </Dropdown.Item>
                            <Dropdown.Item eventKey={"icono-fichalarga"}>
                              <Icon name="icono-fichalarga" />
                            </Dropdown.Item>
                          </>
                        }
                      </Dropdown.Menu>
                    </Dropdown>
                  </Form.Group>
                ) : (
                  <Form.Group controlId="">
                    <div className="d-flex justify-content-between">
                      <label>{t("micrositios.menus.materialIcon")} </label>
                      <OverlayTrigger
                        delay={{ hide: 1000, show: 200 }}
                        overlay={(props) => (
                          <Tooltip className="help_menu" {...props}>
                            Ingresa el nombre del ícono que aparece en la
                            librería de{" "}
                            <a
                              target="_blanck"
                              href="https://material-ui.com/components/material-icons/"
                            >
                              {" "}
                              Material Design{" "}
                            </a>{" "}
                            Ej: CreateTwoTone
                          </Tooltip>
                        )}
                        placement="bottom"
                      >
                        <i className="fas fa-question-circle"></i>
                      </OverlayTrigger>
                    </div>
                    <Form.Control
                      type="text"
                      placeholder=""
                      onChange={handleChangeMaterialIcon}
                      value={form.material_icon}
                    />
                  </Form.Group>
                )}

                {props.section && props.section == "3" ? (
                  <Form.Group controlId="">
                    <div className="d-flex justify-content-between">
                      <label>{t("micrositios.menus.classIcon")} </label>
                      <OverlayTrigger
                        delay={{ hide: 1000, show: 200 }}
                        overlay={(props) => (
                          <Tooltip className="help_menu" {...props}>
                            Ingresa las clases de CSS definidas en tus archivos
                            css para dar un estilo personalizado a tu ícono
                          </Tooltip>
                        )}
                        placement="bottom"
                      >
                        <i className="fas fa-question-circle"></i>
                      </OverlayTrigger>
                    </div>
                    <Form.Control
                      type="text"
                      placeholder=""
                      onChange={handleChangeColor}
                      value={form.clases}
                    />
                  </Form.Group>
                ) : (
                  ""
                )}

                <Form.Group controlId="">
                  <div className="d-flex justify-content-between">
                    <Form.Check
                      onChange={handleChangeOnlyIcon}
                      type="checkbox"
                      label={t("micrositios.menus.onlyIcon")}
                      checked={form.only}
                    />
                    <OverlayTrigger
                      delay={{ hide: 1000, show: 200 }}
                      overlay={(props) => (
                        <Tooltip className="help_menu" {...props}>
                          Selecciona si deseas que sólo se muestre el ícono y no
                          la etiqueta
                        </Tooltip>
                      )}
                      placement="bottom"
                    >
                      <i className="fas fa-question-circle"></i>
                    </OverlayTrigger>
                  </div>
                </Form.Group>
                <Form.Group controlId="">
                  <div className="d-flex justify-content-between">
                    <Form.Check
                      onChange={handleChangeTab}
                      type="checkbox"
                      label={t("micrositios.menus.newTab")}
                      checked={form.tab}
                    />
                    <OverlayTrigger
                      delay={{ hide: 1000, show: 200 }}
                      overlay={(props) => (
                        <Tooltip className="help_menu" {...props}>
                          Selecciona si deseas que este ítem se abra en una
                          nueva pestaña al dar clic desde el menú
                        </Tooltip>
                      )}
                      placement="bottom"
                    >
                      <i className="fas fa-question-circle"></i>
                    </OverlayTrigger>
                  </div>
                </Form.Group>
              </Form>
              {form.tag && (
                <Card.Link
                  onClick={(e) => {
                    e.preventDefault();
                    props.addElement(form);
                    clear();
                  }}
                  href="#"
                >
                  {LABELS_MENU.add_to_menu}
                </Card.Link>
              )}
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </>
  );
}
export default MenuLinks;
