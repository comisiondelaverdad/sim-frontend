import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Formik } from 'formik';
import * as app from "../../store/ducks/app.duck";
import * as ResourceGroupService from "../../services/ResourceGroupService";
import * as FormService from "../../services/FormService";
import {Typeahead} from 'react-bootstrap-typeahead';
import CustomTextField from "../molecules/CustomTextField"
import AdvancedSearch from "./AdvancedSearch";
import { toAbsoluteUrl } from "../../../theme/utils";
import { errorAlert } from './../../services/utils'
import SearchIcon from '@material-ui/icons/Search';
class FormSearch extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ResourceGroup: [],
      open: false,
      advancedSearchConfig: undefined,
      mapQuery: undefined,
      filters: [],
      currentPosition: 0,
      showTime: false,
      currentKey: null,
      query: '',
      mapPlace: undefined
    };

    this.TypeaheadRef = React.createRef();
    this.ResourceGroup = this.ResourceGroup.bind(this);
    this.deleteTag = this.deleteTag.bind(this);
  }

  componentDidMount() {
    this.ResourceGroup();
    this.AdvancedSearhConfig();
  }

  AdvancedSearhConfig(){
    FormService.getAdvancedSearchConfig()
      .then(
        (data) => {
          this.setState({advancedSearchConfig: data})
        },
        (error) => {
          console.log(error)
        }
      )
  }

  ResourceGroup(){
    ResourceGroupService.serviceListFirstLevel("")
      .then(
        (data) => {
          let resourceGroup = [];
          data.forEach(node=>{
            if(node.children.length>0){
              node.children.forEach(item =>{
                item.sublevel = node.text;
                resourceGroup.push(item);
              })
            }
            else{
              resourceGroup.push(node);
            }
          });
          this.setState({ResourceGroup : resourceGroup});
        },
        (error) => {
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar la información',
            confirmButtonText: 'Aceptar',
            action: ()=> {}
          })
        }
      )
  }

  getAllChildren(node){
    let id = []
    let getAll = function(node){
      if(node.children.length > 0){
        id.push(...node.resourceGroup);
        node.children.forEach((c)=>{
          getAll(c);
        });
      }
      else{
        id.push(...node.resourceGroup);
      }
    }

    getAll(node);

    return id;
  }

  _renderItems = (option, props, index) => {
    let item = "";
    if(option.parent !== null){
      item =
            <div key={option.id} className="d-flex flex-column flex-grow-1 font-weight-bold">
                <span className="text-dark text-hover-primary mb-1 font-size-lg">{option.text}</span>
                <small className="text-muted">{option.sublevel}</small>
            </div>;
    }
    else{
      item =
            <div key={option.id} className="d-flex flex-column flex-grow-1 font-weight-bold">
                <span href="#" className="text-dark text-hover-primary mb-1 font-size-lg">{option.text}</span>
            </div>;
            //
    }
    return [
      item
    ];
  }

  addQuery(q, values, setFieldValue){
    let currentPosition = this.state.currentPosition;
    let filters = this.state.filters;
    // Evito agreguen AND y OR seguidos.
    if(q === "AND" || q === "OR"){
      if(currentPosition === 0){
        q = "";
      }
      else{
        if((filters[currentPosition] === "AND" || filters[currentPosition] === "OR") || (filters[currentPosition-1] === "AND" || filters[currentPosition-1] === "OR")){
          q = "";
        }
      }
    }
    if(q){
      if(currentPosition === filters.length){
        filters.push(q);
      }
      else{
        filters.splice(currentPosition, 0, q);
      }
      let newQuery = filters.join(" ");

      setFieldValue("searchValue", newQuery);
      
      this.setState({filters: filters, currentPosition: filters.length });
    }
  }

  
  addMap(geojson, q, setFieldValue, name){
    this.setState({"mapQuery": geojson, "mapPlace": name})
    setFieldValue("map", q)
  }

  removeMap(){
    this.setState({"mapQuery": undefined})
  }

  handleChangeInput(evt){
    this.setState({query:evt.target.value});
  }

  handleKeyUP(evt, values, setFieldValue){
    if(evt.which === 13){
      if(evt.target.value.length !== 0){
        this.setState({query:''});
        this.addQuery(evt.target.value,values,setFieldValue);
      }
    }
    if(evt.key === "\""){
      if(this.state.currentKey === "\""){
        this.setState({currentKey: null});
      }
      else{
        this.setState({currentKey: evt.key})
      }
    }
    if(evt.which === 32){
      if(this.state.currentKey !== "\""){
        evt.preventDefault();
        this.addQuery(evt.target.value,values,setFieldValue);
        this.setState({query:""});
      }
    }
    if([8,37,39,46].indexOf(evt.which) !== -1 && evt.target.value.length === 0){
      let currentPosition = this.state.currentPosition;
      if(evt.which === 8){
        if(currentPosition > 0){
          this.deleteTag(currentPosition-1,values,setFieldValue);
        }
      }
      if(evt.which === 37){ 
        this.setState({currentPosition: (currentPosition === 0 ? 0 : currentPosition - 1) });
      }
      if(evt.which === 39){
        this.setState({currentPosition: (currentPosition === this.state.filters.length ? this.state.filters.length : currentPosition + 1) });
      }
      if(evt.which === 46){
        if(currentPosition < this.state.filters.length){
          this.deleteTag(currentPosition,values,setFieldValue);
        }
      }
    }
  }

  deleteTag(tagPosition, values, setFieldValue){
    let filters = this.state.filters;
    filters.splice(tagPosition,1);
    let searchValue = filters.join(" ");
    setFieldValue("searchValue",searchValue);
    this.setState({filters: filters, currentPosition: filters.length });
  }

  toggleTime(){
    this.setState({"showTime":!this.state.showTime})
  }

  render() {
    return (
      <>
      {this.state.ResourceGroup.length > 0 &&
        <Formik
          enableReinitialize
          initialValues = {{
            searchValue: '',
            searchResourceGroup: [],
            map: ''
          }}
          validate = {values => {
            const errors = {};
            if(this.state.query === ''){
              if(!values.searchValue && values.searchResourceGroup.length === 0 && !values.map){
                errors.validate = "Debe ingresar un parámetro de búsqueda, seleccionar un fondo o seleccionar un area georreferenciada"
              }
              else{
                /*if (
                  !/^[A-Z0-9áéíóú\s]+$/ig.test(values.searchValue) && values.searchValue
                ) {
                  errors.validate = "No se permiten caracteres especiales"
                }*/
              }
            }
            return errors;
          }}

          onSubmit={values => {
            let resourceGroup = values.searchResourceGroup.children && values.searchResourceGroup.children.length > 0 ? values.searchResourceGroup.children.join(",") : "";
            let resourceGroupText = values.searchResourceGroup.text && values.searchResourceGroup.text.length > 0 ? values.searchResourceGroup.text.join(",") : "";
            let from = "from=" + encodeURIComponent(1);
            let searchValue = "";
            if(values.searchValue || this.state.query !== ''){
              searchValue = values.searchValue.split(" ");
              if(searchValue[searchValue.length-1] === "OR" || searchValue[searchValue.length-1] === "AND"){
                searchValue.pop();
              }
              values.searchValue = searchValue.join(" ") + " " + this.state.query;
              searchValue = "q="+encodeURIComponent(values.searchValue);
            }
            
            this.props.fromComponent("search");
            this.props.filters({
              "filters":{
                "keyword": values.searchValue,
                "resourceGroup": {
                  "resourceGroupText": resourceGroupText,
                  "resourceGroupIDS": resourceGroup
                },
                "mapPolygon": values.map ? values.map : ""
              }
            })
            if(searchValue){
              this.props.history.push("/search?" + searchValue + "&" + from);
            }
            else{
              this.props.history.push("/search?" + from);
            }
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue
          }) => (
          <div>
            <form  onSubmit={handleSubmit}>
              <div className="input-group">
                <div className="input-group-prepend">
                </div>
                <CustomTextField
                  tags={this.state.filters}
                  currentPosition={this.state.currentPosition}
                  events={{
                    onKeyDown: (evt) => {this.handleKeyUP(evt, values, setFieldValue)},
                    deleteTag: (tagPosition) => {this.deleteTag(tagPosition, values, setFieldValue)},
                    onChange: (evt) => {this.handleChangeInput(evt) }
                  }}
                />
                <button
                  type="submit"
                  className="btn-small ml-2 btn btn-primary"
                  disabled={false}
                >
                  <SearchIcon></SearchIcon> Buscar
                </button>
              </div>
              {this.state.mapPlace &&
                <div className="mapPlace">
                  <i className="flaticon2-map"></i>
                  Mapa seleccionado: {this.state.mapPlace}
                </div>
              }
              <br/>
            </form>
            <div className="resources-group-search resources-group-zindex">
              {this.state.advancedSearchConfig &&
                <AdvancedSearch
                      dashboard={true}
                      className="advanced-search-zindex"
                      config={this.state.advancedSearchConfig}
                      addQuery={(q)=>this.addQuery(q, values, setFieldValue)}
                      addMap={(geojson, q, name)=>this.addMap(geojson, q, setFieldValue, name)}/>
              }
              <div className="resources-group-list">
                <Typeahead
                  id="resources-group"
                  labelKey="text"
                  clearButton
                  multiple
                  options={this.state.ResourceGroup.filter(x=>!["001-ERR", "000-ERR"].includes(x.parent))}
                  renderMenuItemChildren={this._renderItems}
                  placeholder="Selecciona un fondo"
                  searchText="Cargando fondos"
                  promptText="Buscar fondos"
                  emptyLabel="No se encontraron fondos"
                  onChange={(selected) => {
                    let children = selected.map((node,x)=>{
                      return this.getAllChildren(node);
                    });
                    let text = selected.map((node,x)=>{
                      return node.text;
                    });
                    setFieldValue('searchResourceGroup', {children:children,text:text});
                  }}
                  open={this.state.open}
                  onBlur={()=>this.setState({open:false})}
                  onFocus={()=>this.setState({open:true})}
                  ref={this.TypeaheadRef}
                />
              </div>
            </div>
            <br />
            <div className="text-center">
              <span className="badge badge-danger">{errors.validate && (touched.searchValue || touched.searchResourceGroup ) && errors.validate}</span>
            </div>
          </div>
        )}
      </Formik>
      }
      </>
    );
  }
}

export default withRouter(connect(
  null,
  app.actions
)(FormSearch));
