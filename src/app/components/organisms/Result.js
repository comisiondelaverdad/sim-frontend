import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import ResultExtra from "../molecules/ResultExtra";
import BookmarksMenu from "./BookmarksMenu";
import ResultSubHead from "../molecules/ResultSubHead";
import { toAbsoluteUrl } from "../../../theme/utils";
import _ from 'lodash';
import striptags from "striptags";
import { TITLE_SIZE } from "../../config/const";
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import { Card, OverlayTrigger, Tooltip } from "react-bootstrap";
import FormAccessRequest from "./FormAccessRequest";
import { Link } from "react-router-dom";
import { is_array } from './../../services/utils';
import LinkIcon from '@material-ui/icons/Link';

const ConditionalWrapper = ({
  condition,
  wrapper,
  children,
}) => (condition ? wrapper(children) : children);

class Result extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeAccessRequests: props.extra.activeAccessRequests ? props.extra.activeAccessRequests : [],
      pendingAccessRequests: props.extra.pendingAccessRequests ? props.extra.pendingAccessRequests : [],
      showModal: null
    };
    this.showAccessRequestModal = this.showAccessRequestModal.bind(this);
    this.toggleAccessRequestModal = this.toggleAccessRequestModal.bind(this);
    this.renderTooltip = this.renderTooltip.bind(this);
    this.goToTranscription = this.goToTranscription.bind(this);
  }

  tags(tags) {
    let description = "";
    if (Array.isArray(tags)) {
      tags.forEach((tag) => {
        tag = striptags(tag, ['span', 'i', 'br']);
        tag = tag.replace(/\xA0/g, '');
        tag = tag.replace(/\\n/g, '<br/>');
        tag = "<span class='badge cev-tags'>" + tag + "</span><br/>";
        description += tag;
      });
    }
    else {
      let tag = "<span class='badge cev-tags'>" + tags + "</span><br/>";
      description += tag;
    }
    return description.trim();
  }

  limpiar(description) {
    description = striptags(description, ['span', 'i']);
    description = description.replace(/\xA0/g, '');
    description = description.replace(/\\n/g, '<br/>');
    description = description.replace(/ENT+[\s0-9:]*/g, "<br /><span class='badge badge-primary'>ENT: </span>");
    description = description.replace(/TEST+[\s0-9:]*/g, "<br /><span class='badge badge-success'>TEST: </span>");
    return description.trim();
  }

  checkAccessRequested(ident) {
    return this.state.pendingAccessRequests.map(r => r.resourceIdent).some(item => item === ident);
  }

  checkPermission(ident, accessLevel) {
    if (parseInt(accessLevel) >= this.props.user.accessLevel) {
      return true;
    } else {
      return this.state.activeAccessRequests.map(r => r.resourceIdent).some(item => item === ident);
    }
  }

  showAccessRequestModal() {
    this.setState({ showModal: this.props.record.ident });
  }

  toggleAccessRequestModal(showModal) {
    const pendingAccessRequests = this.state.pendingAccessRequests.map(r => r.resourceIdent);
    if (showModal && showModal.resource && showModal.resource.ident) {
      pendingAccessRequests.push({ accessRequestIdent: showModal.ident, resourceIdent: showModal.resource.ident });
    }
    this.setState({ pendingAccessRequests: pendingAccessRequests, showModal: null });
  }

  renderTooltip(props) {
    return (
      <Tooltip {...props} show={props.show.toString()}>
        Usted tiene ya una solicitud de accesso en curso a este recurso
      </Tooltip>
    );
  }

  goToTranscription(evt, label, ident) {
    this.props.display({ display: "cev-annotated-transcription", label: label });
    this.props.history.push("/detail/" + ident);
  }

  goTodetail(evt, ident, found) {
    if (found && found.ident && found.type) {
      this.props.display({ display: "cev-files$" + found.ident, type: found.type });
    }
    else {
      this.props.display({ display: "" });
    }
    this.props.history.push("/detail/" + ident);
  }

  render() {
    const {
      ident: resourceIdent,
      type,
      resourceGroupId,
    } = this.props.record;

    const score = this.props.extra.score ? this.props.extra.score : 0;

    let temporalCoverage = !this.props.record.metadata ? this.props.record.temporalCoverage : this.props.record.metadata.firstLevel.temporalCoverage,
      geographicCoverage = !!this.props.record.metadata ? is_array(this.props.record.metadata.firstLevel.geographicCoverage) ? this.props.record.metadata.firstLevel.geographicCoverage[0] : { lat: "", lon: "" } : this.props.record.geographicCoverage && is_array(this.props.record.geographicCoverage) ? this.props.record.geographicCoverage[0] : { lat: "", lon: "" },
      geoPoint = !!this.props.record.metadata ? is_array(this.props.record.metadata.firstLevel.geographicCoverage) ? this.props.record.metadata.firstLevel.geographicCoverage[0].geoPoint : { lat: "", lon: "" } : this.props.record.geographicCoverage && is_array(this.props.record.geographicCoverage) ? this.props.record.geographicCoverage[0].location : { lat: "", lon: "" },
      accessLevel = !this.props.record.metadata ? this.props.record.accessLevel : this.props.record.metadata.firstLevel.accessLevel,
      originalAccessLevel = this.props.record.metadata ? this.props.record.metadata.firstLevel.originalAccessLevel ? this.props.record.metadata.firstLevel.originalAccessLevel : this.props.record.metadata.firstLevel.accessLevel : this.props.record.accessLevel,
      date = !!this.props.record.metadata ? this.props.record.metadata.firstLevel.creationDate ? this.props.record.metadata.firstLevel.creationDate : '' : this.props.record.creationDate ? this.props.record.creationDate : '',
      title = !this.props.record.metadata ? this.props.record.title : this.props.record.metadata.firstLevel.title,
      description = !this.props.record.metadata ? this.props.record.description : this.props.record.metadata.firstLevel.description,
      encodedImage = this.props.record.origin ? this.props.record.records.find(x => x.extra.encodedImage != null) : this.props.extra.extra.encodedImage ? this.props.extra : undefined,
      url = this.props.record.origin ? this.props.record.records.find(x => x.extra.url != null) : this.props.extra.extra.url ? this.props.extra : undefined,
      found = this.props.extra.found ? this.props.extra.found : true

    const {
      highlight,
      bookmarks,
      views,
      highlightMode,
      labelled
    } = this.props.extra;

    const accessRequests = this.state.activeAccessRequests.concat(this.state.pendingAccessRequests);
    const accessRequestIdent = accessRequests.find(r => r.resourceIdent === resourceIdent)?.accessRequestIdent;

    if (accessLevel === null) {
      accessLevel = "1";
    }
    var tags = [];
    const pattern = /etiquetado:".*?"/g;
    let current;
    while ((current = pattern.exec(this.props.searchFilters.filters ? this.props.searchFilters.filters.keyword ? this.props.searchFilters.filters.keyword : "" : ""))) {
      tags.push(current[0].split("etiquetado:")[1].replace(/"/g, ""));
    }

    // Filter labels
    let fragments = []
    if (labelled && highlightMode === "2") {
      if (tags.length === 0) {
        if (labelled.annotation) {
          fragments = labelled.annotation.filter(r => !(Array.isArray(r.label) ? r.label.join(' ').startsWith("Entidades") : r.standard_label.join(' ').startsWith("Entidades")));
        }
      }
      else {
        if (labelled.annotation) {
          fragments = labelled.annotation.filter((r) => {
            return tags.filter(x => Array.isArray(r.label) ? r.label.includes(x) : r.standard_label.includes(x)).length !== 0
          });
        }
      }
    }

    let icon = 'documento';
    let origin = this.props.record.origin ? this.props.record.origin : this.props.extra.origin;
    switch (origin) {
      case 'ModuloCaptura':
        icon = 'entrevista'
        break;

      case 'VisualizacionesGeograficas':
        icon = 'vizgeo'
        break;

      case 'CatalogacionFuentesInternas':
        icon = 'omekai'
        break;

      case 'CatalogacionFuentesExternas':
        icon = 'omekae'
        break;

      case 'FileServer':
        icon = 'fileserver'
        break;

      case 'Microdato':
        icon = 'microdato'
        break;

      case 'ModuloCapturaCasosInformes':
        icon = 'casos_comision'
        break;

      case 'Visualizaciones':
        icon = 'viz'
        break;

      default:
        icon = 'documento'
        break;
    }

    return (
      <>
        <FormAccessRequest showModal={this.state.showModal} toggleModal={this.toggleAccessRequestModal} accessLevel={accessLevel} />
        <div className="container-result">
          <div className="header">
            <ResultExtra className="item" title="Tipo" description={type} />
            <ResultExtra className="item" title="Nivel de Acceso" description={originalAccessLevel} />
            {score > 0 &&
              <ResultExtra className="item" title="Relevancia" description={score} />
            }
            {views &&
              <ResultExtra className="item" title="Vistas" description={views[resourceIdent] ? "#" + new Intl.NumberFormat("de-DE").format(views[resourceIdent]) : "#0"} />
            }
            {!(this.props.user.roles.includes('catalogador') || this.props.user.roles.includes('catalogador_gestor')) && this.checkPermission(resourceIdent, accessLevel) && (
              <div className="item">
                <div className="label">Editar Recurso</div>
                <Link
                  to={`/resources/resource/${resourceGroupId ? resourceGroupId : '0'}/${resourceIdent}`} >
                  <svg className="cv-medium">
                    <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-documento")} className="flia-green"></use>
                  </svg>
                </Link>
              </div>
            )}
            {date &&
              <ResultExtra className="item" title="Fecha" description={date} type="Date" />
            }
            {temporalCoverage && !is_array(temporalCoverage.start) && !is_array(temporalCoverage.end) && temporalCoverage.start && temporalCoverage.end &&
              <ResultExtra title="Cobertura temporal" description={[temporalCoverage.start, temporalCoverage.end]} type="DateRange" />
            }
            {this.checkPermission(resourceIdent, accessLevel) && bookmarks &&
              <div className="item">
                <div className="label">Bookmark</div>
                <div>
                  <BookmarksMenu extra={{ ident: resourceIdent, bookmarks, highlight, title, temporalCoverage, geographicCoverage }} />
                </div>
              </div>
            }
          </div>
          <div className="content">
            <div className="label link-cev">
              <LinkIcon></LinkIcon>
              {!this.checkPermission(resourceIdent, accessLevel) &&
                <>
                  {!this.checkAccessRequested(resourceIdent) &&
                    <span style={{ pointerEvents: 'none' }}>
                      {resourceIdent} - {title && title.split(" ", TITLE_SIZE).join(" ") + (found.type ? " - " + found.type : "")}
                      <br />
                    </span>
                  }
                  {this.checkAccessRequested(resourceIdent) &&
                    <Link to={`/access-requests/${accessRequestIdent}`} >
                      {resourceIdent} - {title && title.split(" ", TITLE_SIZE).join(" ") + (found.type ? " - " + found.type : "")}
                      <br />
                    </Link>
                  }
                </>
              }
              {this.checkPermission(resourceIdent, accessLevel) && found &&
                <span onClick={(evt) => this.goTodetail(evt, resourceIdent, found)} style={{ cursor: "pointer" }} className="cev-widget__username">
                  {resourceIdent} -{title && title.split(" ", TITLE_SIZE).join(" ") + (found.type ? " - " + found.type : "")}
                  <i className="flaticon2-correct cev-font-success" />
                  <br />
                </span>
              }
              <div className="cev-widget__action">

              </div>
            </div>
            {highlightMode === "1" &&
              <div className="cev-widget__info">
                <div className="cev-widget__desc">
                  {description}
                  {highlight && _.map(highlight, (v, k) => (
                    <React.Fragment key={k}>
                      {_.map(v, (v2, k2) => (
                        <div key={k + k2}
                          dangerouslySetInnerHTML={{
                            __html: this.limpiar(v2)
                          }}
                        />
                      ))}
                    </React.Fragment>
                  ))}
                </div>
              </div>
            }
            {fragments && fragments.length > 0 && highlightMode === "2" && this.checkPermission(resourceIdent, accessLevel) &&
              <div className="cev-widget__info">
                <div className="cev-widget__desc" style={{ textAlign: "justify" }}>
                  {_.map(fragments, (v, k) => (
                    <React.Fragment key={k}>
                      {_.map(v.points, (v2, k2) => (
                        <React.Fragment key={k + k2}>
                          <Card>
                            <Card.Body>
                              < div className="row">
                                <div className="col-5">
                                  <span className="font-weight-bold border-bottom">Etiquetas</span>
                                </div>
                                <div className="col-7">
                                  <span className="font-weight-bold border-bottom">Fragmento</span>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-5" key={k + k2 + "e"} onClick={(evt) => this.goToTranscription(evt, v2, resourceIdent)}
                                  dangerouslySetInnerHTML={{
                                    __html: this.tags(v.label)
                                  }}
                                />
                                <div className="col-7" key={k + k2 + "f"} onClick={(evt) => this.goToTranscription(evt, v2, resourceIdent)}
                                  dangerouslySetInnerHTML={{
                                    __html: this.limpiar(v2.text)
                                  }}
                                />
                              </div>
                            </Card.Body>
                          </Card>
                          <br />
                        </React.Fragment>
                      ))}
                    </React.Fragment>
                  ))}
                </div>
              </div>
            }
            {!this.checkPermission(resourceIdent, accessLevel) &&
              <div>
                <ConditionalWrapper
                  condition={this.checkAccessRequested(resourceIdent)}
                  wrapper={children => (
                    <OverlayTrigger placement="left" delay={{ show: 250, hide: 400 }}
                      overlay={this.renderTooltip}>
                      {children}
                    </OverlayTrigger>
                  )} resourceIdent
                >
                  <div style={{ display: 'inline-block', cursor: 'not-allowed' }}>
                    <button className="btn btn-danger" disabled={this.checkAccessRequested(resourceIdent)}
                      onClick={this.showAccessRequestModal}
                      style={{ pointerEvents: this.checkAccessRequested(resourceIdent) ? 'none' : 'all' }}>
                      Solicitar Acceso
                    </button>
                  </div>
                </ConditionalWrapper>
              </div>
            }

          </div>
        </div>
        <div className="hidden">
          {!encodedImage &&
            <svg style={{ height: "7rem", width: "6rem" }}>
              <use xlinkHref={toAbsoluteUrl("/media/cev/icons/icons.svg#icono-" + icon)} className="flia-green"></use>
            </svg>
          }
          {encodedImage &&
            <img src={`data:image/png;base64,${encodedImage.extra.encodedImage}`} alt="1" />
          }
        </div>
      </>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user,
  searchFilters: store.app.filters
});

export default withRouter(
  connect(
    mapStateToProps,
    app.actions
  )(Result));