import React from "react";
import { Dropdown } from "react-bootstrap";
import DropDownIcon from "../molecules/DropDownIcon";
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import * as ResourceGroupService from "../../services/ResourceGroupService"
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { errorAlert } from './../../services/utils'
import { OverlayTrigger, Tooltip } from "react-bootstrap";

class ResourceGroupSelect extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
      ResourceGroup: [],
      show: false,
      selected:[]
    };

    this.loadResourceGroup = this.loadResourceGroup.bind(this);
    this.loadSelected = this.loadSelected.bind(this);
    this.addSelected = this.addSelected.bind(this);
  }

  componentDidMount(){
    this.loadResourceGroup();
  }

  loadResourceGroup(){
    ResourceGroupService.serviceListFirstLevel("")
      .then(
        (data) => {
          let resourceGroup = [];
          data.forEach(node=>{ 
            if(node.children.length>0){
              node.children.forEach(item =>{
                item.sublevel = node.text;
                resourceGroup.push(item);
              })
            }
            else{
              resourceGroup.push(node);  
            }
          });
          this.setState({ResourceGroup : resourceGroup},()=>{
            if(this.props.from === "search"){
              this.loadSelected();
            }
          });
        },
        (error) => {
          errorAlert({
            error: error,
            text: 'Se ha presentado un error al cargar la información',
            confirmButtonText: 'Aceptar',
            action: ()=> {}
          })
        }
      )
  }

  _renderItems = (option, props, index) => {
    let item = "";
    if(option.parent !== null){
      item = 
            <div key={option.id} className="d-flex flex-column flex-grow-1 font-weight-bold">
                <span className="text-dark text-hover-primary mb-1 font-size-lg">{option.text}</span>
                <small className="text-muted">{option.sublevel}</small>
            </div>;
    }
    else{
      item = 
            <div key={option.id} className="d-flex flex-column flex-grow-1 font-weight-bold">
                <span href="#" className="text-dark text-hover-primary mb-1 font-size-lg">{option.text}</span>
            </div>;
    }
    return [
      item
    ];
  }

  loadSelected(){
    let selected = [];
    this.state.ResourceGroup.forEach((resourceGroup,key)=>{
      if(this.props.searchFilters && this.props.searchFilters.filters.resourceGroup.resourceGroupText.split(",").includes(resourceGroup.text) ){
        selected.push(resourceGroup);
      }
    });

    this.addSelected(selected);
  }

  addSelected(selected){
    let children = selected.map((node,x)=>{
      return this.getAllChildren(node);
    });
    let text = selected.map((node,x)=>{
      return node.text;
    });

    this.props.addResourceGroup({children:children,text:text});
    this.setState({selected : selected});
  }

  getAllChildren(node){
    let id = []
    let getAll = function(node){
      if(node.children.length > 0){
        id.push(...node.resourceGroup);
        node.children.forEach((c)=>{
          getAll(c);
        });
      }
      else{
        id.push(...node.resourceGroup)
      }
    }

    getAll(node);

    return id;
  }

  render(){
    
    return(
      <Dropdown
        alignRight
        drop="down"
        show={this.state.show}
        onToggle={()=>{this.setState({show:false})}}
      >
        <Dropdown.Toggle 
          as={DropDownIcon}
          onClick={(evt)=>{this.setState({show:true})}}
        >
          <OverlayTrigger
            key="bottom3"
            placement="bottom"
            overlay={
              <Tooltip>
                Filtrar por Fondos
              </Tooltip>
            }
          >
            <button type="button" className="btn btn-outline-success add">
              <i className="fa fa-folder cev-font-warning"></i>Fondos
            </button>
          </OverlayTrigger>
        </Dropdown.Toggle>
        <Dropdown.Menu className="dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl cev-bookmarks">
          <div className="cev-portlet">
            <div className="cev-portlet__head">
              <div className="cev-portlet__head-label">
                <h3 className="cev-portlet__head-title cev-font-primary">
                  Fondos
                </h3>
              </div>
              <div className="cev-portlet__head-toolbar">
                <div className="cev-portlet__head-actions">
                  <button type="button" className="btn btn-outline-secondary btn-icon btn-sm" onClick={()=>{this.setState({show:false})}}>
                    <i className="flaticon2-cross"></i>
                  </button>
                </div>
              </div>
            </div>
            <div className="cev-portlet__body">
              <div className="">
                <Typeahead
                  className="cev-typehead-resource-group"
                  id="resources-group"
                  labelKey="text"
                  clearButton
                  multiple
                  options={this.state.ResourceGroup}
                  selected={this.state.selected}
                  renderMenuItemChildren={this._renderItems}
                  placeholder="Selecciona un fondo"
                  selectHintOnEnter
                  searchText="Cargando fondos"
                  promptText="Buscar fondos"
                  emptyLabel="No se encontraron fondos"
                  onChange={(selected) => {this.addSelected(selected)}}
                />
              </div>
            </div>
            <div className="cev-portlet__foot"></div>
          </div>
        </Dropdown.Menu>
      </Dropdown>
    )
  }
}

const mapStateToProps = store => ({
  searchFilters: store.app.filters,
  from: store.app.fromComponent
});

export default withRouter(connect(
  mapStateToProps,
  null
)(ResourceGroupSelect));