import React, { Component } from "react";
import PDFViewer from "../molecules/PDFViewer";
import FilesViewer from "../molecules/FilesViewer";
import * as RecordsService from "../../services/RecordsService"


class Omeka extends Component {
  state = {
    ver : false,
    file: ''
  }

  componentDidMount(){
    const types = ["doc","docx","csv","pdf"];
    const type = this.props.records[0].filename.split('.').pop();
    if(types.indexOf(type)!== -1){
      RecordsService.serviceFile(this.props.records[0].ident)
        .then(
          (data)=>{
            this.setState({
              ver:!this.state.ver,
              file:data
            });
          },
          (error)=>{
            this.setState({file: false, ver:false});
            console.log(error)
          }
        )
    }
    else{
      this.setState({file: false, ver:false});
    }
  }

  render() {
    const types = ["doc","docx","csv"];
    const type = this.props.records[0].filename.split('.').pop();
    return (
      <>
        <div className="cev-portlet__body">
          <div className="cev-widget4" >
            {this.props.records && type === "pdf" && this.state.ver &&
              <PDFViewer file={this.state.file} id={this.props.records[0].ident} type={type}></PDFViewer>
            }
            {this.props.records && types.indexOf(type)!== -1 && this.state.ver &&
              <FilesViewer file={this.state.file} id={this.props.records[0].ident} type={type}></FilesViewer>
            }
            {this.state.file === false && this.state.ver === false &&
              <div className="cev-section">
                <div className="cev-section__content text-center">
                  <h2 className="badge-danger">Archivo no encontrado</h2>
                </div>
              </div>
            }
          </div>
        </div>
      </>
    );
  }
}

export default Omeka;