import React, { Component } from 'react'
import Dona from '../molecules/Dona'

/**
 * Componente que se encarga de mostrar una dona con los datos ya procesados
 */
class DonaBar extends Component {
    constructor(props) {
        super(props)

        this.state = {
            buckets: null
        }

        this.update = this.update.bind(this)
    }

    componentDidMount() {

    }

    update() {
        if (this.props.data !== undefined) {

        }
    }

    render() {
        return (
            <>
                <div className="dona">
                    <Dona data={this.state.buckets} />
                </div>
            </>
        )
    }
}

export default DonaBar