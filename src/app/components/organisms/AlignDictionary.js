import React, { Component, useEffect, useState } from "react";
import * as TermService from "../../services/TermService";
import ReactPlayer from 'react-player'
import { Alert, Button, Col, Container, Collapse, Form, Modal, Row, Table } from "react-bootstrap";

class AlignDictionary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recordId: this.props.recordId,
            record: {},
            createObjectURL:'',
            segundo:0,
            formsubtitle:'',
            subtitle:[]
        };
    }

    componentDidMount() {

        //Consultamos el record quye esta asociado al termino
        TermService.findByidRecord(this.state.recordId).then((data) => {
                this.setState({ record: data });                

                //Guardo el contenido del audio
                if( data.subtitle !== "")
                {
                    this.setState({ subtitle: eval(data.subtitle) });                                
                }

                //Genero el audio para reproducción
                TermService.audioRecord(this.state.recordId).then((aud) => {
                    this.setState({ createObjectURL: URL.createObjectURL(aud) });                   
                });        

                //Craeo las filas de cada frangmento del contenido del audio
                this.pintarFilas();
            })

        



    }

    handleProgress = state => {
        this.setState({ segundo: state.playedSeconds });
    }

    ajustartiempo (numero,tipo,value){
        if(value==="")
        {
            this.state.subtitle[numero][tipo]=this.state.segundo; 
        }
        else
        {            
            this.state.subtitle[numero][tipo]=value; 
        }
        
        //Craeo las filas de cada frangmento del contenido del audio
        this.pintarFilas();
    }

    pintarFilas(){
        this.setState({formsubtitle: this.state.subtitle.map((item,index) => <tr><td>{item[0]}<Button variant="outline-primary" onClick={() => { this.ajustartiempo(index,0,'');}}><i className="flaticon-time"/></Button></td><td>{item[1]}<Button variant="outline-primary" onClick={() => { this.ajustartiempo(index,1,'');}}><i className="flaticon-time"/></Button></td><td><textarea onKeyUp={(e) => { this.ajustartiempo(index,2,e.target.value);}}>{item[2]}</textarea></td><td><Button variant="danger" onClick={() => { this.eliminarFila(index);}}>X</Button></td></tr>)});        
    }

    eliminarFila( item ) {
        this.state.subtitle.splice( item, 1 );        
        this.pintarFilas();
    };
    
    agregarFila( item ) {
        this.state.subtitle.push(['','','']);        
        this.pintarFilas();
    };

    guardarSubtitle() {
        this.state.record.subtitle=this.state.subtitle;         
        TermService.updateRecord(this.state.recordId,JSON.stringify(this.state.subtitle)).then((data) => {alert('Se guardó el texto, con éxito!!!');this.props.history.push(this.props.pagehistory);});        
    };


    render() {
        return (
            <>
            {this.state.record &&
            <div>
                <Container>
                <Row>
                    <Col xs={12} md={8}>
                        <ReactPlayer
                        url={this.state.createObjectURL}
                        width="400px"
                        height="50px"
                        playing={false}
                        controls={true}                                    
                        onProgress={this.handleProgress}
                        />
                    </Col>
                    <Col xs={6} md={4}>
                    <h3>Tiempo de reproducción actual: {this.state.segundo}</h3>
                    </Col>
                </Row> 
                <Row>
                    <Col xs={12} md={12} style={{ textAlign: "right" }}>
                        <Button variant="success" onClick={() => { this.agregarFila();}}>+</Button>
                    </Col>
                </Row>               
                </Container>
                
                <Table striped bordered hover size="sm">
                    <thead>
                        <tr>
                        <th>Tiempo de inicio</th>
                        <th>Tiempo final</th>
                        <th>Texto</th>                        
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.formsubtitle}
                    </tbody>
                </Table>
                <Row>
                    <Col xs={12} md={12} style={{ textAlign: "right" }}>
                        <Button variant="warning" onClick={() => { this.guardarSubtitle();}}>Guardar</Button>
                    </Col>
                </Row> 
            </div>
            }
            </>
        );
    }
}

export default AlignDictionary;
