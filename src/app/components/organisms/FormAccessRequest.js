import React, {Component} from "react";
import PropTypes from 'prop-types';
import {Alert, Button, Col, Collapse, Form, Modal, Row, SafeAnchor} from "react-bootstrap";
import {connect} from "react-redux";
import * as ResourcesService from "../../services/ResourcesService";
import * as AccessRequestService from "../../services/AccessRequestService";
import * as AccessRequestHistoryService from "../../services/AccessRequestHistoryService";
import * as app from "../../store/ducks/app.duck";
import moment from "moment";
import Swal from "sweetalert2";

class FormAccessRequest extends Component {

    static propTypes = {
        user: PropTypes.object,
        toggleModal: PropTypes.func.isRequired,
        //showModal: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            user: props.user._id,
            description: '',
            userAgreement: false,
            requestDate: moment().local().format(),
            requestedFrom: null,
            requestedTo: null,
            status: 'pending',
            resource: null,
            formValidated: false,
            accessLevel: null,
            showTermsOfService: false
        };
        this.handleClose = this.handleClose.bind(this);
        this.handleEnter = this.handleEnter.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleClose() {
        this.props.toggleModal(null);
    }

    handleEnter() {
        ResourcesService.getResource(this.props.showModal).then((data) => {
            this.setState({
                resource: data._id,
                accessLevel: data.metadata.firstLevel.accessLevel
            });
        }).catch((error) => {
            console.error("An unexpected error occurred while retrieving resource:  %s", error);
            this.props.toggleModal(null);
            alert("Debe volver a iniciar la sesión")
            //this.props.history.push("/logout");
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({formValidated: true});
        if (!e.target.checkValidity()) {
            return;
        }

        Swal.showLoading()
        AccessRequestHistoryService.createAccessRequestHistory({
            user: this.props.user._id,
            date: moment().local().format(),
            status: 'created',
            description: this.state.description+" "+this.state.contact
        }).then((data) => {
            return AccessRequestService.createAccessRequest({
                ...this.state,
                history: [data._id]
            });
        }).then((data) => {
            this.setState({resource: data._id});
            Swal.close();
            this.props.toggleModal(data);
        }).catch((error) => {
            console.error("An unexpected error occurred while creating an access request:  %s", error);
            this.props.toggleModal(null);
        });
    }

    render() {
        return (
            <Modal
                show={this.props.showModal != null}
                onEnter={this.handleEnter}
                onHide={this.handleClose}
                size="lg"
            >
                {this.props.accessLevel && this.props.accessLevel === '3' &&
                  <>
                      <Modal.Header closeButton>
                          <Modal.Title>Solicitud de acceso</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        El usuario no esta autorizado para solicitar acceso a este recurso.<br/>
                        Por favor solicitar son su coordinador que sea asignado a nivel 3 si debe tener acceso.
                      </Modal.Body>
                  </>
                }
                {this.props.accessLevel && this.props.accessLevel !== '3' &&
                    <Form noValidate validated={this.state.formValidated} onSubmit={this.handleSubmit}>
                        <Modal.Header closeButton>
                            <Modal.Title>Solicitud de acceso</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Alert key={0} variant={'secondary'}>
                                <div className="alert-icon">
                                    <i className="flaticon-warning cev-font-brand"/>
                                </div>
                                <div className="alert-text">
                                    Solicitará acceso temporal a un recurso clasificado, justifique su solicitud.
                                </div>
                            </Alert>
                            <Form.Row>
                                <Form.Group as={Col} sm="6">
                                    <Row style={{marginLeft: '0px'}}>
                                        <Form.Label column="false" sm={{span: 6}}>
                                            <span>Fecha inicial de acceso</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={{span: 6}}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                min={moment().local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({requestedFrom: moment(e.target.value, 'YYYY-MM-DD').local().format()})
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es
                                                requerido.</Form.Control.Feedback>
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group as={Col} sm="6">
                                    <Row style={{marginLeft: '0px'}}>
                                        <Form.Label column="false" sm={{span: 6}}>
                                            <span>Fecha final de acceso</span>
                                            <span className="text-danger">*</span>
                                        </Form.Label>
                                        <Col sm={{span: 6}}>
                                            <Form.Control
                                                required
                                                as="input"
                                                type="date"
                                                min={moment(this.state.requestedFrom ? this.state.requestedFrom : undefined).local().format('YYYY-MM-DD')}
                                                onChange={(e) => {
                                                    this.setState({requestedTo: moment(e.target.value, 'YYYY-MM-DD').local().endOf('day').format()})
                                                }}
                                            />
                                            <Form.Control.Feedback type="invalid">Este campo es
                                                requerido.</Form.Control.Feedback>
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Form.Row>
                            <Form.Group controlId="accessRequestForm.Textarea">
                                <Form.Label column="false">
                                    <span>Justificación</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Form.Control
                                    required
                                    as="textarea"
                                    rows="3"
                                    onChange={(e) =>{this.setState({description: e.target.value})}}
                                />
                                <Form.Control.Feedback type="invalid">Este campo es requerido.</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group controlId="accessRequestForm.contact">
                                <Form.Label column="false">
                                    <span>Teléfono de contacto</span>
                                    <span className="text-danger">*</span>
                                </Form.Label>
                                <Form.Control
                                    required
                                    as="input"
                                    type="number"
                                    rows="3"
                                    onChange={(e) =>{this.setState({contact: e.target.value})}}
                                />
                                <Form.Control.Feedback type="invalid">Este campo es requerido y deben ser solo números.</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group controlId="accessRequestForm.Checkbox">
                                <Collapse in={this.state.showTermsOfService}>
                                    <div style={{textAlign: 'justify', wordBreak: 'break-word'}}>
                                        {this.props.accessLevel === null &&
                                        <p>La información solicitada es información RESERVADA, en los términos de la Ley
                                            1448 de 2011, 1712 de 2014, Ley 1581 de 2012, Ley 599 de 2000, Ley 600 de
                                            2000, Ley 1922 de 2018, Ley Estatutaria 1957 de 2019 y demás normas
                                            aplicables. El acceso indebido a ella pone en riesgo derechos a la intimidad,
                                            vida y seguridad, derechos de infancia y adolescencia, derechos de las víctimas,
                                            entre otros, y a intereses públicos de defensa y seguridad nacional, seguridad
                                            pública, investigación y persecución de delitos, debido proceso, reserva
                                            judicial, administración de justicia, investigación y esclarecimiento de graves
                                            violaciones a los derechos humanos, reconstrucción de la verdad acerca de lo
                                            ocurrido durante el conflicto armado, entre otros. Por lo tanto, la persona que
                                            incumpla el presente COMPROMISO DE RESERVA y sin autorización brinde acceso,
                                            revele, comparta, filtre, divulgue, dé a conocer, envíe, entregue, filtre,
                                            ofrezca, intercambie, comercialice, emplee o permita que alguien emplee esta
                                            información incurre en conductas que conllevan sanciones disciplinarias,
                                            fiscales y penales.</p>
                                        }
                                        {this.props.accessLevel && this.state.accessLevel === '1' &&
                                        <p>La información solicitada es información RESERVADA, en los términos de la Ley
                                            1448 de 2011, 1712 de 2014, Ley 1581 de 2012, Ley 599 de 2000, Ley 600 de
                                            2000, Ley 1922 de 2018, Ley Estatutaria 1957 de 2019 y demás normas
                                            aplicables. El acceso indebido a ella pone en riesgo derechos a la intimidad,
                                            vida y seguridad, derechos de infancia y adolescencia, derechos de las víctimas,
                                            entre otros, y a intereses públicos de defensa y seguridad nacional, seguridad
                                            pública, investigación y persecución de delitos, debido proceso, reserva
                                            judicial, administración de justicia, investigación y esclarecimiento de graves
                                            violaciones a los derechos humanos, reconstrucción de la verdad acerca de lo
                                            ocurrido durante el conflicto armado, entre otros. Por lo tanto, la persona que
                                            incumpla el presente COMPROMISO DE RESERVA y sin autorización brinde acceso,
                                            revele, comparta, filtre, divulgue, dé a conocer, envíe, entregue, filtre,
                                            ofrezca, intercambie, comercialice, emplee o permita que alguien emplee esta
                                            información incurre en conductas que conllevan sanciones disciplinarias,
                                            fiscales y penales.</p>
                                        }
                                        {this.props.accessLevel && this.state.accessLevel === '2' &&
                                        <p>La información solicitada es información RESERVADA, en los términos de la Ley
                                            1448 de 2011, 1712 de 2014, Ley 1581 de 2012, Ley 599 de 2000, Ley 600 de
                                            2000, Ley 1922 de 2018, Ley Estatutaria 1957 de 2019 y demás normas
                                            aplicables. El acceso indebido a ella pone en riesgo derechos a la intimidad,
                                            vida y seguridad, derechos de infancia y adolescencia, derechos de las víctimas,
                                            entre otros, y a intereses públicos de defensa y seguridad nacional, seguridad
                                            pública, investigación y persecución de delitos, debido proceso, reserva
                                            judicial, administración de justicia, investigación y esclarecimiento de graves
                                            violaciones a los derechos humanos, reconstrucción de la verdad acerca de lo
                                            ocurrido durante el conflicto armado, entre otros. Por lo tanto, la persona que
                                            incumpla el presente COMPROMISO DE RESERVA y sin autorización brinde acceso,
                                            revele, comparta, filtre, divulgue, dé a conocer, envíe, entregue, filtre,
                                            ofrezca, intercambie, comercialice, emplee o permita que alguien emplee esta
                                            información incurre en conductas que conllevan sanciones disciplinarias,
                                            fiscales y penales.</p>
                                        }
                                        {this.props.accessLevel && this.state.accessLevel === '3' &&
                                        <p>La información solicitada es información CONFIDENCIAL, de acceso y circulación
                                            restringida, y tratamiento exclusivo por parte de servidores autorizados, en los
                                            términos del Decreto Ley 588 de 2017, ley 1581 de 2013, ley 1721 de 2014 y el
                                            Art. 269f de la Ley 599 de 2000. El acceso no autorizado a ella puede causar
                                            daño a los derechos a la intimidad, vida y seguridad, derechos de infancia y
                                            adolescencia, derechos de las víctimas, entre otros, y a intereses públicos de
                                            defensa y seguridad nacional, seguridad pública, investigación y persecución de
                                            delitos, debido proceso, reserva judicial, administración de justicia,
                                            investigación y esclarecimiento de graves violaciones a los derechos humanos,
                                            reconstrucción de la verdad acerca de lo ocurrido durante el conflicto armado,
                                            entre otros. Incumplir el presente COMPROMISO DE CONFIDENCIALIDAD y, sin
                                            autorización, brindar acceso, revelar, compartir, filtrar, divulgar, dar a
                                            conocer, enviar, entregar, filtrar, ofrecer, intercambiar, comercializar,
                                            emplear o permitir que alguien emplee esta información, acarrea sanciones
                                            laborales, disciplinarias, fiscales y penales.</p>
                                        }
                                    </div>
                                </Collapse>
                                <Form.Check custom>
                                    <Form.Check.Input
                                        required
                                        type="checkbox"
                                        onChange={(e) => {
                                            this.setState({userAgreement: e.target.value === "on"})
                                        }}/>
                                    <Form.Check.Label>
                                        <span>Entiendo y acepto el </span>
                                        <SafeAnchor
                                            href="#"
                                            onClick={(e) => {
                                                this.setState({showTermsOfService: !this.state.showTermsOfService})
                                            }}
                                        >
                                            {((this.props.accessLevel && this.props.accessLevel === '') || this.props.accessLevel === null) &&
                                            <>compromiso de reserva</>
                                            }
                                            {this.props.accessLevel && this.props.accessLevel === '1' &&
                                            <>compromiso de reserva</>
                                            }
                                            {this.props.accessLevel && this.props.accessLevel === '2' &&
                                            <>compromiso de reserva</>
                                            }
                                            {this.props.accessLevel && this.props.accessLevel === '3' &&
                                            <>compromiso de confidencialidad</>
                                            }
                                        </SafeAnchor>
                                    </Form.Check.Label>
                                    <Form.Control.Feedback type="invalid">Debe aceptar el compromiso legal para
                                        continuar</Form.Control.Feedback>
                                </Form.Check>
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className="pull-left" variant="primary" type="submit">
                                Enviar solicitud
                            </Button>
                            <Button className="pull-left" variant="secondary" onClick={this.handleClose}>
                                Cancelar
                            </Button>
                        </Modal.Footer>
                    </Form>
                }
            </Modal>
        );
    }
}

const mapStateToProps = store => ({
    user: store.auth.user,
});

export default connect(
    mapStateToProps,
    app.actions
)(FormAccessRequest);
