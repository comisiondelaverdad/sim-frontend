import React, { useEffect, useState } from "react";

import * as CollectionService from "../../services/CollectionService";
import Carousel from "react-multi-carousel";
import TarjetaResumenColeccion from "../../sim-ui/organisms/TarjetaResumenColeccion";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Lottie from "react-lottie";
import animationData from "../../../assets/loading_cev_explora.json";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
  },
  colecciones: {
    padding: "10px",
  },
}));

const CollectionKeyword = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [element, setElement] = useState(props.element);
  const [propierty, setPropierty] = useState(props.propierty);
  const [collections, setCollections] = useState([]);

  useEffect(() => {
    loadColletions();
  }, []);
  const handlerTarjetaResumenColeccion = (tipo, id) => {
    const ruta = "/museo/conoce/" + id;
    history.push(ruta);
  };

  const loadColletions = async () => {
    let res = null;
    if (propierty == "keywords" || propierty == 'keywords_resources')
      res = await CollectionService.getCollectionsByKeywords(propierty, element);
    else res = await CollectionService.getCollectionsByCategory(element);

    if (res.length) setCollections(res);
  };

  const colecciones = () => {
    return collections.map((item) => (
      <TarjetaResumenColeccion
        titulo={item.title}
        descripcion={item.description}
        id={item._id}
        handlerTarjetaResumen={handlerTarjetaResumenColeccion}
        conoce={true}
      />
    ));
  };

  const renderCollections = () => {
    if (collections.length) {
      return (
        <div>
          <h3>{element}</h3>
          <ul></ul>
          <Carousel
            className={classes.colecciones}
            additionalTransfrom={0}
            arrows
            autoPlaySpeed={3000}
            centerMode={false}
            containerClass="container"
            dotListClass=""
            draggable
            focusOnSelect={false}
            infinite={false}
            itemClass=""
            keyBoardControl
            minimumTouchDrag={80}
            renderButtonGroupOutside={false}
            renderDotsOutside={false}
            responsive={{
              desktop: {
                breakpoint: {
                  max: 3000,
                  min: 1024,
                },
                items: 5,
                partialVisibilityGutter: 40,
              },
              mobile: {
                breakpoint: {
                  max: 500,
                  min: 0,
                },
                items: 2,
                partialVisibilityGutter: 30,
              },
              tablet: {
                breakpoint: {
                  max: 1024,
                  min: 500,
                },
                items: 3,
                partialVisibilityGutter: 30,
              },
            }}
            showDots={false}
            sliderClass=""
            slidesToSlide={1}
            swipeable
          >
            {colecciones()}
          </Carousel>
          <hr />
        </div>
      );
    }
  };

  return (
    <>
      {collections.length ? (
        renderCollections()
      ) : (
        <div className="loading_viz">
          <Lottie
            height={150}
            width={150}
            options={{
              loop: true,
              autoplay: true,
              animationData: animationData,
              rendererSettings: {
                preserveAspectRatio: "xMidYMid slice",
              },
            }}
          />
        </div>
      )}
    </>
  );
};

export default CollectionKeyword;
