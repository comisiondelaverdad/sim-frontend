import React, { Component } from 'react'
import WordCloudBar from '../../components/organisms/WordCloudBar';
import GrafoBar from '../../components/organisms/GrafoBar';
import EtiquetasBar from '../../components/organisms/EtiquetasBar';
import TimeLineBar from '../../components/organisms/TimeLineBar';
import MapaBar from '../../components/organisms/MapaBar';
import NGram from '../../components/organisms/NGram';

export default class VizSelect extends Component {
    constructor(props) {
        super(props)
        this.state = {
            viz: ''
        }

        this.changeViz = this.changeViz.bind(this);
    }

    componentDidMount() {
        if (this.props.bookmarks !== undefined) {
            let arrayPath = []
            this.props.bookmarks.map(b => {
                arrayPath.push(b.path)
                return ''
            })
        }
    }

    changeViz(event) {
        if (this.state.viz !== event.target.getAttribute('value')) {
            this.setState({
                viz: event.target.getAttribute('value')
            })
        } else {
            this.setState({
                viz: ''
            })
        }
    }

    render() {
        return (
            <>
                <div className="vizNav mt-10">
                    <span>Explorar patrones de texto: </span>
                    <span value="cloud" className={this.state.viz === 'cloud' ? 'btn active' : 'btn'} onClick={this.changeViz}>Nube de entidades</span>
                    <span value="grafo" className={this.state.viz === 'grafo' ? 'btn active' : 'btn'} onClick={this.changeViz}>Grafo de entidades</span>
                    <span value="etiquetas" className={this.state.viz === 'etiquetas' ? 'btn active' : 'btn'} onClick={this.changeViz}>Etiquetas analíticas</span>
                    <span value="linea_tiempo" className={this.state.viz === 'linea_tiempo' ? 'btn active' : 'btn'} onClick={this.changeViz}>Linea de tiempo de entidades</span>
                    <span value="mapa" className={this.state.viz === 'mapa' ? 'btn active' : 'btn'} onClick={this.changeViz}>Mapa de entidades</span>
                    <span value="ngram" className={this.state.viz === 'ngram' ? 'btn active' : 'btn'} onClick={this.changeViz}>Arbol de Ngram</span>
                </div>

                {this.props.total && this.props.location && this.state.viz === 'cloud' &&
                    <WordCloudBar
                        total={this.props.total}
                        location={this.props.location}
                    />
                }

                {this.props.bookmarks && this.state.viz === 'cloud' &&
                    <WordCloudBar
                        bookmarks={this.props.bookmarks}
                    />
                }

                {this.props.resource !== undefined && this.state.viz === 'cloud' &&
                    <>
                        {this.props.resource.selected !== undefined &&
                            <WordCloudBar
                                resource={this.props.resource.selected}
                            />
                        }
                    </>
                }

                {this.props.total && this.props.location && this.state.viz === 'grafo' &&
                    <GrafoBar
                        total={this.props.total}
                        location={this.props.location}
                        info="de los resultados tienen asignados algunas de las siguientes entidades"
                    />
                }

                {this.props.bookmarks && this.state.viz === 'grafo' &&
                    <GrafoBar
                        bookmarks={this.props.bookmarks}
                    />
                }

                {this.props.resource !== undefined && this.state.viz === 'grafo' &&
                    <>
                        {this.props.resource.selected !== undefined &&
                            <GrafoBar
                                resource={this.props.resource.selected}
                            />
                        }
                    </>
                }

                {this.props.total && this.props.location && this.state.viz === 'etiquetas' &&
                    <EtiquetasBar
                        total={this.props.total}
                        location={this.props.location}
                    />
                }

                {this.props.bookmarks && this.state.viz === 'etiquetas' &&
                    <EtiquetasBar
                        bookmarks={this.props.bookmarks}
                    />
                }

                {this.props.resource !== undefined && this.state.viz === 'etiquetas' &&
                    <>
                        {this.props.resource.selected !== undefined &&
                            <EtiquetasBar
                                resource={this.props.resource.selected}
                            />
                        }
                    </>
                }

                {this.props.total && this.props.location && this.state.viz === 'linea_tiempo' &&
                    <TimeLineBar
                        total={this.props.total}
                        location={this.props.location}
                    />
                }

                {this.props.bookmarks && this.state.viz === 'linea_tiempo' &&
                    <TimeLineBar
                        bookmarks={this.props.bookmarks}
                    />
                }

                {this.props.resource !== undefined && this.state.viz === 'linea_tiempo' &&
                    <>
                        {this.props.resource.selected !== undefined &&
                            <TimeLineBar
                                resource={this.props.resource.selected}
                            />
                        }
                    </>
                }

                {this.props.total && this.props.location && this.state.viz === 'mapa' &&
                    <MapaBar
                        total={this.props.total}
                        location={this.props.location}
                    />
                }

                {this.props.bookmarks && this.state.viz === 'mapa' &&
                    <MapaBar
                        bookmarks={this.props.bookmarks}
                    />
                }

                {this.props.resource !== undefined && this.state.viz === 'mapa' &&
                    <>
                        {this.props.resource.selected !== undefined &&
                            <MapaBar
                                resource={this.props.resource.selected}
                            />
                        }
                    </>
                }

                {this.props.total && this.props.location && this.state.viz === 'ngram' &&
                    <>
                        <NGram
                            total={this.props.total}
                            location={this.props.location}
                        />
                    </>
                }

                {this.props.bookmarks && this.state.viz === 'ngram' &&
                    <NGram
                        bookmarks={this.props.bookmarks}
                    />
                }

                {this.props.resource !== undefined && this.state.viz === 'ngram' &&
                    <>
                        {this.props.resource.selected !== undefined &&
                            <NGram
                                resource={this.props.resource.selected}
                            />
                        }
                    </>
                }


            </>
        )
    }
}