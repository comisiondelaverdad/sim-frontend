import React, { Component, useEffect, useState, useRef } from "react";
import Lottie from 'react-lottie';
import animationData from '../../../assets/loading_cev_explora.json'
import * as CollectionService from "../../services/CollectionService";
import CollectionKeyword from "../../components/organisms/CollectionKeyword";
import { makeStyles } from "@material-ui/core/styles";

const CarrouselCollections = (props) => {
  const [elements, setElements] = useState([]);
  const [propierty, setPropierty] = useState(props.propierty);
  const useStyles = makeStyles((theme) => ({
    root: {
      width: "100%",
      "& > * + *": {
        marginTop: theme.spacing(2),
      },
    },
  }));

  useEffect(() => {
    // get_keywords();
    // get_categories();
    get_elements(propierty);
  }, []);
  const get_elements = async (propierty) => {
    const res = await CollectionService.getAggregation(propierty);
    if (res.length) setElements(res);
  };

  const render_elements = () => {
    let list = "";
    if (elements.length)
      return elements.map((item) => {
        return (
          <CollectionKeyword
            propierty={propierty}
            element={propierty == "category" ? item._id : item._id}
          />
        );
      });
    return <h3>No hay términos clave</h3>;
  };

  return (
    <div>
      <div>
        {elements.length ? (
          render_elements()
        ) : (
          <div className="loading_viz">
            <Lottie
              height={150}
              width={150}
              options={{
                loop: true,
                autoplay: true,
                animationData: animationData,
                rendererSettings: {
                  preserveAspectRatio: "xMidYMid slice",
                },
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (store) => ({});

export default CarrouselCollections;
