import React, { Component,useEffect, useState } from "react";
import {Alert, Button, Col, Collapse, Form, Modal, Row, SafeAnchor} from "react-bootstrap";

class TermsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {        
      show: false,
      terms: this.props.terms
    };          
}

onOpenModal() {
  this.setState({show: true});  
}

onCloseModal() {
  this.setState({show: false});  
}

onCkeckBox(id) {
  alert(id);
}

  render() {
    
    const listTerms = this.state.terms.map((number) =>  <Col sm><label><input type='checkbox' value={number[0].id} onClick={() => {
      this.onCkeckBox(number[0].id);
    }}/>{number[0].name}</label></Col>);


    return (
      <div>
      <Button variant="success" onClick={() => {
                                this.onOpenModal();
                              }}>
        Seleccionar los términos
      </Button>
      <ul>
        
      </ul>

      <Modal show={this.state.show} >
        <Modal.Header>
          <Modal.Title>Términos</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Row>
          {listTerms}
          <Col><Form.Check.Input type="checkbox" checked="checked"/></Col>
        </Row>

        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => {
                                this.onCloseModal();
                              }}>
            Cerrar
          </Button>          
        </Modal.Footer>
      </Modal>        
      </div>
    );
  }
}

export default TermsModal;
