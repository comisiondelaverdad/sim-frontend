import React, { Component } from 'react'
import Wordcloud from '../molecules/Wordcloud'
import { connect } from "react-redux";
import * as app from "../../store/ducks/app.duck";
import * as VizService from "../../services/VizService";
import PropTypes from 'prop-types';
import * as d3 from 'd3'
import Lottie from 'react-lottie';
import animationData from '../../../assets/viz-loading.json'
import LocalOfferIcon from '@material-ui/icons/LocalOffer';

/**
 * Bloque que muestra dos nubes de palabras. Una apunta a **api/search/labelledcloud** y la otra a **api/search/tagcloud** con el parámetro _tipoTagCloud_ =_conflictActors_.
 * @version 0.1
 */
class WordCloudBar extends Component {
  static propTypes = {
    /** URL string location */
    location: PropTypes.object,
    /** Total de hits para la búsqueda realizada */
    total: PropTypes.number,
    /** Bookmarks */
    bookmarks: PropTypes.array
  }

  constructor(props) {
    super(props)
    this.state = {
      tagcloud1: [],
      tagcloud: [],
      total: 0,
      grupoSelected: null,
      selected: [''],
      loading: true
    };

    this.grupos = []
    this.changeSelected = this.changeSelected.bind(this)
  }

  componentDidMount() {
    this.color = d3.scaleOrdinal(d3.schemeTableau10)
    if (this.props.location) {
      let query = new URLSearchParams(this.props.location.search);

      if (query.toString() !== '') {
        this.updateSearchTagClouds(this.props.searchFilters);
      }
    } else if (this.props.bookmarks) {
      this.updateBookmarksTagClouds()
    } else if (this.props.resource) {
      this.updateRGTagClouds()
    } else {
      this.updateDataFiltered()
    }

  }

  updateDataFiltered() {
    let params = {
      origin: "ModuloCaptura"
    }

    if (this.props.filtros !== undefined) {
      if (this.props.filtros[0].filtros.length > 0) params.ngramFilter = this.props.filtros[0].filtros
      if (this.props.filtros[1].filtros.length > 0) params.etiquetasFilter = this.props.filtros[1].filtros
      if (this.props.filtros[2].filtros.length > 0) params.entidadesFilter = this.props.filtros[2].filtros
    }

    this.updateSearchTagClouds(params)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.location) {
      let oldQuery = new URLSearchParams(prevProps.location.search).toString();
      let query = new URLSearchParams(this.props.location.search);

      if (query.toString() !== '') {
        if (query.toString() !== oldQuery.toString()) {
          this.updateSearchTagClouds(this.props.searchFilters);
        }
      }
    } else if (this.props.bookmarks && !this.state.tagcloud) {
      this.updateBookmarksTagClouds()
    }

    if (prevState.selected[0] !== this.state.selected['0']) this.updateSearchTagClouds(this.props.searchFilters)
  }

  /**
   * Servicio para actualizar el estado cuando es una búsqueda
   * 
   * @param {string} search 
   * @param {string} resourceGroup 
   */
  updateSearchTagClouds(filters) {
    VizService.tagCloud(filters, true, this.state.selected)
      .then(
        (data) => {
          var resp = false
          if (data.hits) {
            resp = {
              total: data.hits.total.value,
              buckets: data.aggregations.tipo.cloud.total.buckets
            }
          }

          let newArray = []
          resp.buckets.map(m => {
            if (m.key.split('|').length > 1) {
              const grupo = this.grupos.find(d => d === m.key.split('|')[0])
              if (!grupo) this.grupos.push(m.key.split('|')[0])

              if (this.state.selected.length > 0) {
                const grupo = this.state.selected.find(d => d === m.key.split('|')[0])
                if (grupo) newArray.push(m)
                else if (this.state.selected[0] === '') newArray.push(m)
              } else {
                newArray.push(m)
              }
            }
            return ''
          })

          resp.buckets = newArray

          this.setState({
            tagcloud: resp,
            loading: false
          })

        },
        (error) => {
          console.log(error)
          //this.props.history.push("/logout");
        }
      )



  }

  /**
   * Servicio para actualizar el estado cuando son bookmarks
   */
  updateBookmarksTagClouds() {
    let idArray = []
    this.props.bookmarks.map(p => {
      idArray.push(p.path)
      return ''
    })

    let json = {
      idArray: idArray,
      type: true
    }

    VizService.tagCloud(json)
      .then(
        (data) => {
          let resp = {}
          if (data.hits) {
            resp = {
              total: data.hits.total.value,
              buckets: data.aggregations.tipo.cloud.total.buckets
            }
          }
          this.setState({
            tagcloud: resp,
            loading: false
          })
        },
        (error) => {

        }
      )
  }

  updateRGTagClouds() {
    let json = {
      rg: this.props.resource[0],
      type: true
    }

    VizService.tagCloud(json)
      .then(
        (data) => {
          let resp = {}
          if (data.hits) {
            resp = {
              total: data.hits.total.value,
              buckets: data.aggregations.tipo.cloud.total.buckets
            }
          }
          this.setState({
            tagcloud: resp,
            loading: false
          })
        },
        (error) => {

        }
      )
  }

  changeSelected(elem) {
    if (this.state.selected[0] !== elem) {
      this.setState({
        selected: [elem]
      })
    } else {
      this.setState({
        selected: ['']
      })
    }
  }

  render() {
    // console.log(this.grupos)
    return (
      <>
        {this.state.loading &&
          <div className="loading_viz">
            <Lottie height={150} width={150} options={
              {
                loop: true,
                autoplay: true,
                animationData: animationData,
                rendererSettings: {
                  preserveAspectRatio: 'xMidYMid slice'
                }
              }
            } />
          </div>
        }
        {(this.state.tagcloud.total === 0 || this.state.tagcloud.length === 0 || !this.state.tagcloud.total || this.state.tagcloud.buckets.length === 0) &&
          <>
            <span className="badge badge-danger">Los resultados no tienen los datos suficientes para la visualización</span>
          </>
        }
        {((this.state.tagcloud1.total > 0 || this.state.tagcloud.total > 0) && this.state.tagcloud.buckets.length > 0) &&
          <div className="wordcloudBar">

            {/* {this.state.tagcloud.total > 0 && this.state.total > 0 &&
              <Wordcloud
                width={1000}
                color={this.color}
                height={300}
                info="de los resultados tienen asignados algunas de las siguientes entidades"
                descripcion="La identificación de entidades recupera la información que responde a las preguntas ¿quién/es?, ¿dónde? y ¿cuándo? de las entrevistas transcritas. Esta nube de entidades visualiza las más usados en los resultados."
                list={this.state.tagcloud}
                total={this.state.total}
              />
            } */}

            {this.state.tagcloud.total > 0 && !this.state.total && this.state.tagcloud.buckets.length > 0 &&
              <Wordcloud
                width={1000}
                color={this.color}
                callBack={this.props.changeFilter}
                height={300}
                info="de los resultados tienen asignados algunas de las siguientes entidades"
                descripcion="La identificación de entidades recupera la información que responde a las preguntas ¿quién/es?, ¿dónde? y ¿cuándo? de las entrevistas transcritas. Esta nube de entidades visualiza las más usados en los resultados."
                list={this.state.tagcloud}
              />
            }

            <div className="groups">
              {this.grupos.map((n, i) => {
                let style = {
                  color: this.color(n)
                }
                return (
                  <div key={i} className={(this.state.selected[0] !== '' && this.state.selected[0] !== n) ? 'inactive item' : 'active item'} onClick={() => this.changeSelected(n)} >
                    <LocalOfferIcon style={style}></LocalOfferIcon>
                    {n}
                  </div>
                )
              })}
            </div>
          </div>
        }
      </>
    )
  }
}

const mapStateToProps = store => ({
  searchFilters: store.app.filters
});

export default connect(
  mapStateToProps,
  app.actions
)(WordCloudBar);