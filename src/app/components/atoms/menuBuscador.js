import store from "../../store/store";
import * as _ from "lodash";

const allRoles = [
    "user", "admin", "reviewer", "approver", "catalogador", "catalogador_gestor",
    "escucha", "descarga", "admin_diccionario", "invitado", "tesauro", "comisionado", "revision_bibliografica"];


export const menuBuscador = {
    "elements": [

        {
            "tag": "Administración del sistema",
            "route": "",
            "tab": false,
            "id": "d05fbe5a-8674-4ad9-c4fa-60e529b35216",
            "img": "",
            "icon": null,
            "only": false,
            "material_icon": "SettingsApplications",
            "clases": "",
            "orden": 1,
            "roles": ["admin"],
            "children": [
                {
                    "tag": "Usuarios",
                    "route": "admin/users",
                    "tab": false,
                    "id": "e87b8667-fad3-4ee3-8a5e-60e529b35211",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "GroupAddTwoTone",
                    "clases": "",
                    "orden": 5,
                    "roles": ["admin"] ,
                    "children": [

                    ]
                },
                {
                    "tag": "Ubicaciones",
                    "route": "admin/locations",
                    "tab": false,
                    "id": "e87b8667-fad3-4ee3-8a5e-60e529b35212",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "EditLocation",
                    "clases": "",
                    "orden": 5,
                    "roles": ["admin"],
                    "children": [

                    ]
                },
                {
                    "tag": "Historial de búsqueda",
                    "route": "admin/history",
                    "tab": false,
                    "id": "e87b8667-fad3-4ee3-8a5e-60e529b35213",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "History",
                    "clases": "",
                    "orden": 5,
                    "roles": ["admin"],
                    "children": [

                    ]
                },
                {
                    "tag": "Micrositios",
                    "route": "admin/microsites",
                    "tab": false,
                    "id": "e87b8667-fad3-4ee3-8a5e-60e529b35214",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "AccountBalance",
                    "clases": "",
                    "orden": 5,
                    "roles": ["admin"],
                    "children": [

                    ]
                },
                {
                    "tag": "Menús",
                    "route": "admin/menus",
                    "tab": false,
                    "id": "e87b8667-fad3-4ee3-8a5e-60e529b35215",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "Menu",
                    "clases": "",
                    "orden": 5,
                    "roles": allRoles,
                    "children": [

                    ]
                }
            ]
        },
        {
            "tag": "Explora",
            "route": "dashboard",
            "tab": false,
            "id": "d05fbe5a-8674-4ad9-b4fa-60e529b35216",
            "img": "",
            "icon": null,
            "only": false,
            "material_icon": "ExploreTwoTone",
            "clases": "",
            "orden": 1,
            "roles": allRoles,
            "children": [
                {
                    "tag": "Explora por fondos",
                    "route": "resourcegroup/all",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-60e529b35211",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "AccountTree",
                    "clases": "",
                    "orden": 5,
                    "roles": allRoles,
                    "children": [

                    ]
                },
                {
                    "tag": "Explora los metadatos",
                    "route": "visualmetadata",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-60e529b35212",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "FindInPage",
                    "clases": "",
                    "orden": 5,
                    "roles": allRoles,
                    "children": [

                    ]
                },
                {
                    "tag": "Explora por tesauro",
                    "route": "tesauro",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-60e529b35213",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "Textsms",
                    "clases": "",
                    "orden": 5,
                    "roles": allRoles,
                    "children": [

                    ]
                },
                {
                    "tag": "Explora por NGram",
                    "route": "visualcontent",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-60e529b35214",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "Timeline",
                    "clases": "",
                    "orden": 5,
                    "roles": allRoles,
                    "children": [

                    ]
                },
                {
                    "tag": "Explora por entrevistas",
                    "route": "visualexplorer",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-60e529b35215",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "Mic",
                    "clases": "",
                    "orden": 5,
                    "roles": allRoles,
                    "children": [

                    ]
                }
            ]
        },
        {
            "tag": "Catalogación",
            "route": "admin-cataloging/main/all",
            "tab": false,
            "id": "143a41b1-100f-4ee7-ac87-bcbc608e75a8",
            "img": "",
            "icon": null,
            "only": false,
            "material_icon": "MenuBook",
            "clases": "",
            "orden": 2,
            "roles": ["catalogador", "catalogador_gestor", "tesauro", "admin"],
            "children": [
                {
                    "tag": "Catalogador gestor",
                    "route": "admin-cataloging/main/all",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-7d7079132041",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "ViewModule",
                    "clases": "",
                    "orden": 5,
                    "roles": ["catalogador_gestor"],
                    "children": [

                    ]
                }, {
                    "tag": "Módulo de catalogación",
                    "route": "admin-cataloging/main/all",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-7d7079132042",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "ViewModule",
                    "clases": "",
                    "orden": 5,
                    "roles": ["catalogador"],
                    "children": [

                    ]
                },
                {
                    "tag": "Gestor de listas",
                    "route": "list",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-7d7079132043",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "ListAlt",
                    "clases": "",
                    "orden": 5,
                    "roles": ["catalogador", "catalogador_gestor", "tesauro"],
                    "children": [

                    ]
                },
                {
                    "tag": "Actualización masiva de recursos y fondos",
                    "route": "resources/masive",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-7d7079132044",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "GridOn",
                    "clases": "",
                    "orden": 5,
                    "roles": ["catalogador_gestor"],
                    "children": [

                    ]
                },
                {
                    "tag": "Homologación de campos",
                    "route": "admin-cataloging/homologateTerm",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-7f7079132041",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "ViewModule",
                    "clases": "",
                    "orden": 5,
                    "roles": ["tesauro"],
                    "children": [

                    ]
                },
                {
                    "tag": "Gestor de formularios",
                    "route": "forms",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-7d7079132045",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "Code",
                    "clases": "",
                    "orden": 5,
                    "roles": ["catalogador_gestor", "admin"],
                    "children": [

                    ]
                }
            ]
        },
        {
            "tag": "Solicitudes de acceso",
            "route": "my-access-requests",
            "tab": false,
            "id": "b6e82cc1-bb5e-4fb0-a4d4-c7fe50d73a4e",
            "img": "",
            "icon": null,
            "only": false,
            "material_icon": "LockOpen",
            "clases": "",
            "orden": 3,
            "roles":["user", "approver", "reviewer"],
            "children": [
                {
                    "tag": "Mis solicitudes de acceso",
                    "route": "my-access-requests",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-717079132941",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "Update",
                    "clases": "",
                    "orden": 5,
                    "roles":["user"],
                    "children": [

                    ]
                },
                {
                    "tag": "Pre solicitudes de acceso",
                    "route": "review-access-requests",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-7d70791329412",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "SupervisorAccount",
                    "clases": "",
                    "orden": 5,
                    "roles":["reviewer"],
                    "children": [

                    ]
                },
                {
                    "tag": "Solicitudes de acceso",
                    "route": "approve-access-requests",
                    "tab": false,
                    "id": "e87b8667-cad3-4ee3-8a5e-7d7079132sd1",
                    "img": "",
                    "icon": null,
                    "only": false,
                    "material_icon": "SupervisorAccount",
                    "clases": "",
                    "orden": 5,
                    "roles":["approver"],
                    "children": [

                    ]
                },

            ]
        },
        {
            "tag": "Bookmarks",
            "route": "bookmarks",
            "tab": false,
            "id": "b6e82cc1-bb5e-4fb0-a4d4-c7fe50d73a5e",
            "img": "",
            "icon": null,
            "only": false,
            "material_icon": "Bookmarks",
            "clases": "",
            "orden": 3,
            "children": [
            ]
        },
        {
            "tag": "Salir",
            "route": "logout",
            "tab": false,
            "id": "b44be534-76ac-45e6-9d5b-a60f0b9788f2",
            "img": "",
            "icon": null,
            "only": false,
            "material_icon": "ExitToAppTwoTone",
            "clases": "",
            "orden": 4,
            "roles": allRoles,
            "children": [

            ]
        }
    ],
    "_id": "626013358d54b911aafc6220",
    "nombreMenu": "Menú de archivo",
    "estado": 1,
    "section": 2,
    "__v": 0
}


export const myMenu = () => {
    const misRoles = store.getState().auth.user.roles;
    const newMenu = {
        elements: menuBuscador.elements.map((item) => {
            if (item.children.length > 0) {
                return {
                    ...item,
                    ...{ children: item.children.filter((subitem) => ((_.intersection(subitem.roles, misRoles)).length > 0)) }
                }
            } else {
                return item;
            }
        })
            .filter((items) => ((_.intersection(items.roles, misRoles)).length > 0))
    }
    return newMenu;
}