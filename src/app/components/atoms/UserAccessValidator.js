import React, { Component } from "react";
import { Redirect } from 'react-router-dom'
import {connect} from "react-redux";
import * as app from "../../store/ducks/app.duck";

class UserAccessValidator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inicio: false,
    };
  }

  componentDidMount() {
    const { user, rol } = this.props;
    if (!user.roles.includes(rol)){
      //alert("Acceso no permitido")
      this.setState({inicio:true})
    }
  }

  render() {
    return (
      <span>
        { this.state.inicio && <Redirect to="/" /> }
      </span>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(UserAccessValidator);
