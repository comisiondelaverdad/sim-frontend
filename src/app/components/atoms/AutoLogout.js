import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from 'react-router-dom'
import * as app from "../../store/ducks/app.duck";
import store from "../../store/store"
import { delay } from 'rxjs/operators';
import { of } from 'rxjs';
import Swal from "sweetalert2";

class AutoLogout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logout: false,
            timeAlert: 300000 // alert in miliseconds
        };
    }

    componentDidMount() {
        const expiresInSeg = (JSON.parse(atob(((store.getState().auth.authToken).split('.'))[1])).exp);
        let expiresIn = new Date(expiresInSeg * 1000); //
        let timerDelay = 10;
        if (expiresIn > new Date()) {
            timerDelay = expiresIn - new Date();
            console.log(`%cFecha expiración: %c${new Date(expiresIn)}`, "color: blue", "color: green")
        }
        // of(null).pipe(delay(10000)).subscribe((data) => {
        of(null).pipe(delay(timerDelay)).subscribe((data) => {
            this.setState({ logout: true });
        });
        if (this.state.timeAlert < timerDelay) {
            of(null).pipe(delay(timerDelay - this.state.timeAlert)).subscribe((data) => {
                Swal.fire({
                    type: 'info',
                    title: `Su sesión se cerrará en ${this.state.timeAlert / 60000} minutos`,
                    timer: 3000
                })
            });
        }
    }

    render() {
        return (
            <>
                {this.state.logout && (
                    <Redirect to="/logout" />
                )}
            </>
        );
    }
}

const mapStateToProps = store => ({
    search: store.app.keyword,
    user: store.auth.user
});

export default connect(
    mapStateToProps,
    app.actions
)(AutoLogout);