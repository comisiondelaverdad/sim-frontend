import React, { Component } from "react";
import {connect} from "react-redux";
import * as app from "../../store/ducks/app.duck";

class UserAccessShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inicio: false,
    };
  }

  componentDidMount() {
    const { user, rol } = this.props;
    if (user.roles.includes(rol)){
      this.setState({inicio:true})
    }
  }

  render() {
    return (
      <>
        { this.state.inicio && this.props.children }
      </>
    );
  }
}

const mapStateToProps = store => ({
  search: store.app.keyword,
  user: store.auth.user
});

export default connect(
  mapStateToProps,
  app.actions
)(UserAccessShow);
