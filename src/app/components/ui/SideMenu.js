import React from 'react'

import Informe from './Iconos/Informe'
// import GeoPortal from './Iconos/GeoPortal'
// import Nube from './Iconos/Nube'
import Casos from './Iconos/Casos'
import Microdatos from './Iconos/Microdatos'
// import Grafo from './Iconos/Grafo'

import MenuIcon from './MenuIcon'

const SideMenu = props => {
    return (
        <>
            <nav className="sideMenu">
                <MenuIcon title="Patrones de texto" onClick={props.callback}>
                    <Informe />
                </MenuIcon>
                <MenuIcon title="Información de fichas" onClick={props.callback}>
                    <Microdatos />
                </MenuIcon>
                <MenuIcon title="Resultados" onClick={props.callback}>
                    <Casos />
                </MenuIcon>
            </nav>
        </>
    )
}

export default SideMenu