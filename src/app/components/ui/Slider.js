import React, { useState } from 'react'

const Slider = props => {
    const [active, changeState] = useState(false)

    function changeSlideState() {
        props.toggle()
        active ? changeState(false) : changeState(true)
    }

    return (
        <>
            <div className={active ? "slider_container active" : "slider_container"}>
                <div className="slider" onClick={changeSlideState}>
                    <div className="slide"></div>
                </div>

                <div className="label">{props.label}</div>
                {props.active &&
                    <div className="selected" onClick={props.removeFilter}>{props.active.name}</div>
                }
            </div>
        </>
    )
}

export default Slider