import React from 'react'

const MenuIcon = props => {
    return (
        <>
            <div className="item" onClick={() => props.onClick(props.title)}>
                <div className="icon">
                    {props.children}
                </div>
                <div className="tooltipUi">
                    {props.title}
                </div>
            </div>
        </>
    )
}

export default MenuIcon