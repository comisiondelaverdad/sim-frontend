import React from 'react'

const Paginacion = props => {
    const showNumberPages = 10; 
    const totalPages = Math.ceil(props.total / props.step)
    const pages = [];
    const min = props.page > Math.ceil(showNumberPages/2)? props.page-Math.ceil(showNumberPages/2): 1;
    const max = (min + showNumberPages) > totalPages ? totalPages: min + showNumberPages -1;
    for (let index = min; index <= max; index++) {
        if(index === props.page) {
            pages.push({class: 'pag active', number: index});
        }else{
            pages.push({class: 'pag', number: index});
        }
    }
    if(pages[0].number !== 1){
        pages.unshift({class:'pag first', number: 1});
    }
    if(pages[(pages.length) -1 ].number !== totalPages){
        pages.push({class:'pag last', number: totalPages});
    }
    return (
        <nav className="paginacion">

            {props.page > 1 &&
                <div className="prev" onClick={() => props.callback(props.page - 1)}>{"<"}</div>
            }
            {pages.map((p, key) => {
                return <div key={key} className={ p.class  } onClick={() => props.callback(p.number)}>{p.number}</div>
            })
            }
            {props.page < totalPages &&
                <div className="next" onClick={() => props.callback(props.page + 1)}>{">"}</div>
            }
        </nav>
    )
}

export default Paginacion