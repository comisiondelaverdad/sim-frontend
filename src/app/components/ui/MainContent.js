import React from 'react'

const MainContent = ({children}) => {
    return(
        <>
            <section className="mainContent">
                {children}
            </section>
        </>
    )
}

export default MainContent