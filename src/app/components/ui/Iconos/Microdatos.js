import React from 'react'

const Microdatos = () => {
    return (
        <>
            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" x="0" y="0" version="1.1" viewBox="0 0 53.2 53.2">
                <defs />
                <path className="back" d="M11.3,46.4c0,2.4,1.9,4.3,4.3,4.3h23.3c-2.4,0-4.3-1.9-4.3-4.3H11.3z" />
                <path className="back" d="M8.7,41.6l-2.5,7.7l-2.6-7.8v-31c0-1,0.8-1.7,1.7-1.7H7c1,0,1.7,0.8,1.7,1.7V41.6z" />
                <line className="front" fill="currentColor" x1="3.6" y1="40.7" x2="8.7" y2="40.7" />
                <path className="front" fill="currentColor" d="M11.3,9.7v2.6H9.6v-1.7c0-0.7-0.3-1.4-0.8-1.8V6.2c0-1.4-1.2-2.6-2.6-2.6c-1.4,0-2.6,1.2-2.6,2.6v2.5
    c-0.5,0.5-0.9,1.2-0.9,1.9v31L6.2,52l3.4-10.2V14h1.7v8.6H13V9.7H11.3z M4.4,10.5c0-0.4,0.2-0.7,0.5-0.8h2.4
    c0.3,0.1,0.5,0.4,0.5,0.8v1.8H4.4V10.5z M5.3,6.2c0-0.5,0.3-0.9,0.8-0.9S7,5.7,7,6.2v1.7H5.3V6.2z M6.1,46.6l-1.7-5.1h3.3L6.1,46.6z
     M7.8,39.8H4.4V14h3.4V39.8z"/>
                <g>
                    <g>
                        <g>
                            <rect x="21.7" y="19.1" className="front" fill="currentColor" width="15.5" height="1.7" />
                        </g>
                        <g>
                            <rect x="21.7" y="22.6" className="front" fill="currentColor" width="15.5" height="1.7" />
                        </g>
                        <g>
                            <rect x="21.7" y="26" className="front" fill="currentColor" width="15.5" height="1.7" />
                        </g>
                        <g>
                            <rect x="21.7" y="29.5" className="front" fill="currentColor" width="9.5" height="1.7" />
                        </g>
                    </g>
                    <path className="front" fill="currentColor" d="M47.6,10.6L47.6,10.6l-0.1-0.1H20.8c-2.9,0-5.2,2.3-5.2,5.2v30.1h-5.2v0.9c0,2.9,2.3,5.2,5.2,5.2h23.2h0.1
        c2.9,0,5.2-2.3,5.2-5.2v-31c0-1.9,1.5-3.4,3.4-3.4h0.1c1.9,0,3.4,1.5,3.4,3.4h-5.2v1.7h6.9h0.1v-1.6C52.8,12.9,50.5,10.6,47.6,10.6
        z M17.7,50.2h-2.1c-1.6,0-2.9-1.1-3.3-2.6h21.5c0.1,1,0.6,1.9,1.2,2.6H17.7z M42.4,15.8v31c0,1.9-1.5,3.4-3.4,3.4h-0.2v-0.1
        c-1.9,0-3.4-1.5-3.4-3.4v-0.9h-18v-30c0-1.9,1.5-3.4,3.4-3.4h22.9C42.9,13.3,42.4,14.5,42.4,15.8z"/>
                </g>
            </svg>
        </>
    )
}

export default Microdatos