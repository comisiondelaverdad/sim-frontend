/**
 * ETIQUETAS MUSEO CONOCE
 */
export const LABELS_CONOCE = {
  "colPorTipologia": "Colecciones por tipología",
  "colPorTerminosClave": "Colecciones por términos clave"
};

/**
 * ETIQUETAS MUSEO CREA
 */
export const LABELS_CREA = {
  "gestorNarrativasTitle": "Gestor de narrativas",
  "gestorNarrativasDescription": "En este espacio podras crear una narrativa a partir de una serie de elementos del acervo dispuesto por la Comisión de la Verdad",
  "miBibliotecaTitle": "Mi Biblioteca",
  "miBibliotecaDescription": "En este espacio podrás consultar los diferentes recursos que has seleccionado desde la exploración del acervo documental de la Comisión de la Verdad para la elaboración de tus colecciones"
};

/**
 * ETIQUETAS MUSEO EXPLORA
 */
 export const LABELS_EXPLORA = {
  "title": "Explora",
};
/**
 * ETIQUETAS MICROSITIOS MENÚ
 */
 export const LABELS_MENU = {
  "microsites": "Micrositios",
  "microsites_subtitle": "Gestión de Menús", 
  "create_menu": "Gestión de Menús",
  "administration_menu": "Menú Administración",
  "menu_name": "Nombre menú",
  "actions": "Acciones",
  "edit": "Editar",
  "delete": "Eliminar",
  "delete_confirmation": "¿Está seguro que desea eliminar el menú?",
  "menu_create": "Crear menú",
  "menu_config": "Configuración de menú",
  "menu_save": "Guardar menú",
  "basic_info": "Datos básicos",
  "section": "Sección",
  "custom_routes": "Rutas personalizadas",
  "navigation_label": "Etiqueta de navegación",
  "route": "Ruta / Url",
  "image": "Imagen",
  "icon": "Ícono",
  "material_design_icon": "Ícono de material design",
  "css_class_icon": "Clases css para ícono",
  "only_icon": "Sólo ícono",
  "new_tab": "Abrir en nueva pestaña",
  "add_elements": "Agregar elementos",
  "add_to_menu": "Agregar al Menú",
  "collections": "Colecciones",
  "delete_element": "Eliminar elemento",
  "search_collections_by_name": "Buscar colección por nombre",

};