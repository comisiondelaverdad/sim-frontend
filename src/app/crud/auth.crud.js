import fetch from 'isomorphic-fetch'
import * as fetchSync from 'sync-fetch';
import { URL_API } from '../config/const'
import store from "../store/store"

export function http(type, data) {
  let config = {
    method: type,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': getToken()
    },
    body: data
  }
  return config;
}


export function acceptCompromise() {
  return fetch(`${URL_API}/api/users/acceptcompromise`, http('GET')).then((response) => {
    return response.json();
  }).then((data) => {
    if (data._id) {
      return data;
    }
  }).catch(err => {
    return err;
  });
}

export function compromise() {
  return fetch(`${URL_API}/api/users/compromise`, http('GET')).then((response) => {
    return response.json();
  }).then((data) => {
    if (data._id) {
      return data;
    }
  }).catch(err => {
    return err;
  });
}

export function me() {
  return fetch(`${URL_API}/api/users/me`, http('GET')).then((response) => {
    return response.json();
  }).then((data) => {
    if (data._id) {
      return data;
    }
  }).catch(err => {
    return err;
  });
}

export function localStoreToken(){
  return fetchSync(`${URL_API}/auth/token`, {method: 'GET'}).text();
}

export function getToken() {
  const token = "Bearer " + store.getState().auth.authToken;
  return token;
}

export function getUserId() {
  const userId = store.getState().auth.user._id;
  return userId;
}