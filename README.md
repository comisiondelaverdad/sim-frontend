
![](https://archivo.comisiondelaverdad.co/static/media/logo-informe.bcefeab5413ab4f7f70e96825a5b5e32.svg)

## Metabuscador

| Repositorio   | Tecnología|
|---------------|------------|
|[*backend* ](https://gitlab.com/comisiondelaverdad/sim-backend-nest/-/tree/master)|*nestjs* v7.0.11|
|[*frontend*](https://gitlab.com/comisiondelaverdad/sim-frontend/-/tree/master)|*reactjs* 16.13.1|
|[*sim-ui submódulo frontend*](https://gitlab.com/visualizaciones-de-datos-sim/sim-ui/-/tree/feature/material)|*react* 16.13.1|

## Metabuscador frontend

El componente frontend del proyecto ***Metabuscador*** está escrito en ***react v16.13.1***. Para su instalación hay dos opciones que se plantean a continuación. Si elije la **opción 1** de ejecución  debe instalar previamente en su máquina ***node v14.19***, Si elije la **opción 2** de ejecución, debe tener previamente instalado ***docker***.

### Opción 1: Ejecutar el proyecto

1. Cree el archivo ***.env*** en la raíz del proyecto
2. Añada al archivo ***.env*** las variables REACT_APP_URL_API y PORT con su respectivo valor, ejemplo
   <pre><code>REACT_APP_URL_API=http://localhost:4000
   PORT=3000</code></pre>
3. Ejecute para añadir submódulo
   <pre><code>git submodule init
   git submodule update
   cd src/app/sim-ui && git checkout develop</code></pre>
4. Desde la raíz del proyecto ejecute para lanzar el proyecto
<code>npm start</code>

### Opción. Ejecutar el proyecto con docker

1. Cree el archivo ***.env*** en la raíz del proyecto
2. Añada al archivo ***.env*** la variable REACT_APP_URL_API y PORT con su respectivo valor, ejemplo
   <pre><code>REACT_APP_URL_API=http://localhost:4000
   PORT=3000</code></pre>
3. ejecute para añadir submódulo
   <pre><code>git submodule init
   git submodule update
   cd src/app/sim-ui && git checkout develop</code></pre>
4. Desde la raíz del proyecto ejecute para crear la imágen
   <code>docker build --build-arg URL_API_ARG=$URL_API -t buscador-front .</code>
5. Ejecute para crear el contenedor
   <code>docker run --name buscador-front_container -dp $PORT:80 buscador-front</code>
