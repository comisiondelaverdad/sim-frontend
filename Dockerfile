FROM node:14.19 AS builder

ARG IP_BACK_ARG
ARG PORT_BACK_ARG

WORKDIR /frontend

ENV REACT_APP_URL_API=$URL_API_ARG

COPY package.json package.json

RUN npm install

COPY public public/

COPY src src/

RUN npm run-script build

FROM nginx:latest

WORKDIR /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf

COPY nginx.conf /etc/nginx/conf.d

COPY --from=builder /frontend/build .
